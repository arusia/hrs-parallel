!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                                                                     >
!>           TFx_AllocateMem.f95: Coode unit including the             >
!>         routines allocating memory to all TFx simulations           >
!>                                                                     >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
! 
! 
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!


!
      MODULE EOS_Parameters
! 
         SAVE
! 
! ----------
! ...... Integer parameters
! ----------
! 
         INTEGER :: MaxNum_MComp   ! Maximum number of mass components
         INTEGER :: MaxNum_Equat   ! Maximum number of equations
         INTEGER :: MaxNum_Phase   ! Maximum number of phases
         INTEGER :: MaxNum_MobPh   ! Maximum number of mobile phases
! 
! 
      END MODULE EOS_Parameters
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      MODULE Basic_Param
! 
         SAVE
! 
! ----------
! ...... Double precision parameters
! ----------
! 
         REAL(KIND = 8), PARAMETER :: ZERO = 1.0d-6
         REAL(KIND = 8), PARAMETER :: ZA   = 1.0d00
! 
! ----------
! ...... Common integer variables      
! ----------
! 
         INTEGER :: NEQ, NK, NPH, M_BinDif, M_add, Num_MobPhs   
! 
         INTEGER :: No_CEN
! 
         INTEGER :: NEQ1,NK1,NFLUX
! 
         INTEGER :: MaxNum_Elem,NEL,NELA
! 
         INTEGER :: MaxNum_Conx,NCON
		 
  	     INTEGER :: MaxNum_PVar,Num_SecPar
! 
         INTEGER :: NREDM,MNZ,NZ,LIRN,LICN,NCONUP,NCONDN,MNZP1
         INTEGER :: NRWORK,NIWORK,lenw,leniw
! 
         INTEGER :: MaxNum_SS,NOGN
! 
         INTEGER :: MaxNum_Media,N_PFmedia
! 
         INTEGER :: nactdi,nactd2
! 
         INTEGER :: MaxNum_GasComp  ! Maximum number of gas components
! 

!----------
!......  For parallel solvers
!----------
	
		 INTEGER :: N_SUB=1,overlap_loc=1,overlap=1,reorder=1
		 INTEGER :: MAAS
! ----------
! ...... Character parameters
! ----------
! 
         CHARACTER(LEN = 6), ALLOCATABLE,   & 
                         DIMENSION(:) :: GPhase_Com ! Names of the gas components  
! 
! ----------
! ...... Common character variables
! ----------
! 
         CHARACTER(LEN = 6)  :: FLG_con
         CHARACTER(LEN = 15) :: EOS_Name
         CHARACTER(LEN = 80) :: TITLE
! 
! ----------
! ...... Common Logical variables
! ----------
! 
         LOGICAL :: EX_FileVERS
! 
! 
      END MODULE Basic_Param
!
!
!

!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      MODULE Universal_Param
! 
! ----------
! ...... Double precision parameters
! ----------
! 
         REAL(KIND = 8), PARAMETER :: GRPI  = 3.14159265358979324D0  
         REAL(KIND = 8), PARAMETER :: R_gas = 8.31456d3
! 
         REAL(KIND = 8), PARAMETER :: Stef_Boltz = 5.6687d-8
! 
! 
      END MODULE Universal_Param
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      MODULE GenControl_Param
! 
         SAVE
! 
! ----------
! ...... Double precision arrays     
! ----------
! 
         REAL(KIND = 8), DIMENSION(100) :: DLT, TIS
! 
! ----------
! ...... Integer arrays     
! ----------
! 
         INTEGER, DIMENSION(24) :: MOP
! 
! ----------
! ...... Common double precision variables     
! ----------
! 
         REAL(KIND = 8) :: U_p
         REAL(KIND = 8) :: TZERO,T_mat_solv,T_jac_setup=0
         REAL(KIND = 8) :: TIMP1,TIMIN,TIMAX,REDLT,SUMTIM
         REAL(KIND = 8) :: TSTART,DELAF,DELTMX,TIMOUT,GF
         REAL(KIND = 8) :: DELT,DELTEN,DELTEX,FOR,FORD
         REAL(KIND = 8) :: RE1,RE2,RERM,DFAC
         REAL(KIND = 8) :: SCALE,WUP,WNR
! 
! ----------
! ...... Common integer variables     
! ----------
! 
         INTEGER :: NDLT,ITI,ITPR,ITCO
         INTEGER :: KC,KCYC,ITER,ITERC
         INTEGER :: INUM,IPRINT,MCYC,MCYPR,MSEC
         INTEGER :: IRPD,ICPD
         INTEGER :: GenStIndx
! 
         INTEGER :: KDATA,NER,KER,NOITE
         INTEGER :: IGOOD,NST,IHLV
         INTEGER :: MM
         INTEGER :: IHALVE
! 
! ----------
! ...... Common character variables     
! ----------
! 
         CHARACTER(LEN = 4) :: PermModif
         CHARACTER(LEN = 8) :: ELST
! 
! ----------
! ...... Common logical variables     
! ----------
! 
         LOGICAL :: CoordNeed,WellTabData,NoFloSimul,NoVersion
         LOGICAL :: PrintOut_Flag,Converge_Flag,RandmOrdrIC 
         LOGICAL :: RadiHeat_Flag,CondHeat_Flag 
! 
! 
      END MODULE GenControl_Param
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      MODULE Solver_Param
! 
         SAVE
! 
! ----------
! ...... Double precision parameters
! ----------
! 
         REAL(KIND = 8), PARAMETER :: seed = 1.0d-25
! 
! ----------
! ...... Double precision variables
! ----------
! 
         REAL(KIND = 8) :: ritmax,closur
! 
! ----------
! ...... Integer parameters
! ----------
! 
         INTEGER, PARAMETER :: iunit = 699, nvectr = 30
! 
! ----------
! ...... Common integer variables     
! ----------
! 
         INTEGER :: matslv,nmaxit,nnvvcc,iiuunn
! 
! ----------
! ...... Common character variables     
! ----------
! 
         CHARACTER(LEN = 2) :: oprocs,zprocs
         CHARACTER(LEN = 4) :: coord
! 
! ----------
! ...... Common logical variables     
! ----------
! 
         LOGICAL :: SolverBlok
! 
! 

		  
      END MODULE Solver_Param
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      MODULE TimeSeriesP_Param
! 
         SAVE
! 
! ----------
! ...... Derived types    
! ----------
! 
         TYPE Observation_Elem
! 
            INTEGER, DIMENSION(100)            :: num   ! Element # of observation elements 
            CHARACTER(LEN = 8), DIMENSION(100) :: name  ! Names of observation elements
! 
         END TYPE Observation_Elem
! 
! 
! 
         TYPE Observation_Conx
! 
            INTEGER , DIMENSION(100)            :: num   ! Connection # of observation connections
            CHARACTER(LEN = 16), DIMENSION(100) :: name  ! Names of observation connections
! 
         END TYPE Observation_Conx
! 
! 
! 
         TYPE Observation_SS
! 
            INTEGER, DIMENSION(100)            :: num   ! Source/sink # of observation sources/sinks
            CHARACTER(LEN = 5), DIMENSION(100) :: name  ! Names of observation sources/sinks 
! 
         END TYPE Observation_SS
! 
! ----------
! ...... Derived-type variables     
! ----------
! 
         TYPE(Observation_Elem) :: obs_elem   ! The observation element list
         TYPE(Observation_Conx) :: obs_conx   ! The observation connection list
         TYPE(Observation_SS)   :: obs_SS     ! The observation SS list
! 
! ----------
! ...... Integer variables
! ----------
! 
         INTEGER :: N_obs_elem, &   ! The # of elements in the observation element list
                    N_obs_conx,  &  ! The # connections in the observation connection list
  	                N_obs_SS       ! The # of sources/sinks in the observation SS list
! 
! ----------
! ...... Logical variables
! ----------
! 
         LOGICAL :: obs_elem_Flag,&  ! Flag indicating that the observation element list is not empty
                   obs_conx_Flag,&  ! Flag indicating that the observation connection list is not empty
                   obs_SS_Flag     ! Flag indicating that the observation SS list is not empty
! 
! 
      END MODULE TimeSeriesP_Param
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      MODULE Diffusion_Param
! 
         SAVE
! 
! ----------
! ...... Double precision arrays     
! ----------
! 
         REAL(KIND = 8), ALLOCATABLE, DIMENSION(:) :: VBC
         REAL(KIND = 8), ALLOCATABLE, DIMENSION(:) :: FDIF
! 
         REAL(KIND = 8), DIMENSION(10,5) :: diffusivity
! 
! ----------
! ...... Common double precision variables     
! ----------
! 
         REAL(KIND = 8) :: DIFF0,TEXP,BE
! 
! ----------
! ...... Common integer variables     
! ----------
! 
         INTEGER :: iddiag
! 
! 
      END MODULE Diffusion_Param
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      MODULE SELEC_Param
! 
         SAVE
! 
! ----------
! ...... Double precision allocatable arrays     
! ----------
! 
         REAL(KIND = 8), ALLOCATABLE, DIMENSION(:) :: FE
! 
! ----------
! ...... Common integer arrays    
! ----------
! 
         INTEGER, DIMENSION(16) :: IE
! 
! 
      END MODULE SELEC_Param
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      MODULE Input_Comments
! 
         SAVE
! 
! ----------
! ...... Double precision arrays     
! ----------
! 
         CHARACTER(LEN = 80), ALLOCATABLE, DIMENSION(:) :: comm
! 
! 
      END MODULE Input_Comments
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      MODULE Element_Arrays
! 
         SAVE
! 
! ----------
! ...... Derived-type variables     
! ----------
! 
         TYPE Element
! 
            INTEGER(KIND = 2) :: MatNo       ! Element material #
            INTEGER(KIND = 1) :: StateIndex  ! Element phase state index
            INTEGER(KIND = 1) :: StatePoint  ! Element phase state point
!
            CHARACTER(LEN = 8) :: name       ! Element name
! 
            REAL(KIND = 8) :: vol            ! Element volume
            REAL(KIND = 8) :: phi            ! Element porosity
! 
            REAL(KIND = 8) :: P              ! Element pressure
            REAL(KIND = 8) :: T              ! Element temperature
! 
         END TYPE Element
! 
! ----------
! ...... Double precision allocatable arrays     
! ----------
! 
         REAL(KIND = 8), ALLOCATABLE, DIMENSION(:) :: AI,AHT,pm
         REAL(KIND = 8), ALLOCATABLE, DIMENSION(:) :: X_coord
         REAL(KIND = 8), ALLOCATABLE, DIMENSION(:) :: Y_coord
         REAL(KIND = 8), ALLOCATABLE, DIMENSION(:) :: Z_coord
! 
! ----------
! ...... Derived-type arrays     
! ----------
! 
         TYPE(Element), ALLOCATABLE, DIMENSION(:) :: elem
! 
! 
      END MODULE Element_Arrays
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      MODULE Connection_Arrays
! 
         SAVE
! 
! ----------
! ...... Derived-type variables: Connection     
! ----------
! 
         TYPE Connection
! 
            INTEGER :: n1,n2                                 ! Numbers of elements in the connection
            INTEGER :: ki                                    ! Number of the permeability index
! 
            CHARACTER(LEN = 8) :: nam1,nam2                  ! Element names in the connection
! 
            REAL(KIND = 8) :: d1,d2                          ! Distance from interface (m)
            REAL(KIND = 8) :: area                           ! Interface area (m^2)
            REAL(KIND = 8) :: beta                           ! Cos(angle) with vertical
! 
            REAL(KIND = 8) :: FluxH                          ! Heat flux (W)
! 
            REAL(KIND = 8), DIMENSION(:), POINTER :: FluxF   ! Phase fluxes (kg/s)
            REAL(KIND = 8), DIMENSION(:), POINTER :: VelPh   ! Phase velocities (m/s)
! 
         END TYPE Connection
! 
! ----------
! ...... Derived-type variables: VaporFlux     
! ----------
! 
         TYPE VaporFlux
! 
            REAL(KIND = 8) :: FluxVD                ! Diffusive vapor flux (W)
! 
            REAL(KIND = 8), DIMENSION(2) :: FluxVF  ! Vapor flux in the phases (kg/s)
! 
         END TYPE VaporFlux
! 
! ----------
! ...... Double precision allocatable arrays     
! ----------
! 
         REAL(KIND = 8), ALLOCATABLE, DIMENSION(:) :: sig
! 
! ----------
! ...... Integer allocatable arrays     
! ----------
! 
         INTEGER, ALLOCATABLE, DIMENSION(:) :: NEX1_a,NEX2_a,IFL_con
! 
! ----------
! ...... Derived-type arrays     
! ----------
! 
         TYPE(Connection), ALLOCATABLE, DIMENSION(:) :: conx
! 
         TYPE(VaporFlux),  ALLOCATABLE, DIMENSION(:) :: conV
! 
! 
      END MODULE Connection_Arrays
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      MODULE Variable_Arrays
! 
         SAVE
! 
! ----------
! ...... Double precision allocatable arrays     
! ----------
! 
         REAL(KIND = 8), ALLOCATABLE, DIMENSION(:) :: X,DX,DELX
         REAL(KIND = 8), ALLOCATABLE, DIMENSION(:) :: R,DOLD
! 
! ----------
! ...... Double precision arrays     
! ----------
! 
         REAL(KIND = 8), DIMENSION(10) :: DEP
         REAL(KIND = 8), DIMENSION(10) :: XX
! 
! ----------
! ...... Derived-type variables     
! ----------
! 
         TYPE SecParam
            REAL(KIND = 8), DIMENSION(4):: p_Satr ! Phase saturation
            REAL(KIND = 8), DIMENSION(4):: p_KRel ! Phase relative permeability
            REAL(KIND = 8), DIMENSION(4):: p_Visc ! Phase viscosity (Pa.s)
            REAL(KIND = 8), DIMENSION(4):: p_Dens ! Phase density (kg/m^3)
            REAL(KIND = 8), DIMENSION(4):: p_Enth ! Phase specific enthalpy (J/Kg) 
            REAL(KIND = 8), DIMENSION(4):: p_PCap ! Phase capillary pressure (Pa)
            REAL(KIND = 8), DIMENSION(4,4):: p_MasF ! Mass fraction of components in the phases
            REAL(KIND = 8), DIMENSION(0):: p_DiFA ! Coefficient 1 for multicomponent bin. diffusion
            REAL(KIND = 8), DIMENSION(0):: p_DiFB ! Coefficient 2 for multicomponent bin. diffusion
            REAL(KIND = 8), DIMENSION(4):: p_AddD ! Additional data 
            REAL(KIND = 8)                          :: TemC   ! Temperature (C)
         END TYPE SecParam
! 
! ----------
! ...... Derived-type arrays     
! ----------
! 
         TYPE(SecParam), ALLOCATABLE, DIMENSION(:,:) :: Cell_V
! 
! 
      END MODULE Variable_Arrays
!
!

MODULE MPI_ARRAYS
	 
	 USE Element_arrays
	 USE Variable_Arrays
	 
	 IMPLICIT NONE

	 !ARRAYS NEEDED BY MASTER PROCESS
	 
	 INTEGER, ALLOCATABLE ,dimension(:) :: procnum, locnum, global0
	 
	 INTEGER, ALLOCATABLE :: COUNTI(:)
	
	 !ARRAYS NEEDED LOCALLY
	 INTEGER,ALLOCATABLE :: globalconx(:),globalgener(:)
	 
	 INTEGER, ALLOCATABLE, dimension (:) :: update_p,update_l,update_g, update_ps,update_ls,update_gs
	 
	 
	 !Number array for communication
	 INTEGER, ALLOCATABLE,DIMENSION(:) :: NUM_SEND,NUM_RECV
	 INTEGER , ALLOCATABLE,DIMENSION(:,:) :: EXT,update_recv
	 
	 !Other Arrays needed for communication
	 TYPE ELEMPA
		TYPE(Element), pointer, DIMENSION(:) :: ELEM_SEND,ELEM_RECV
		TYPE(SecParam), pointer, DIMENSION(:,:) :: CELL_V_SEND,CELL_V_RECV
		REAL(KIND=8),allocatable, DIMENSION(:) :: REAL_SEND,REAL_RECV      !For communicating real arrays X, DX and DELX
	 END TYPE 
	 
	 
	 TYPE(ELEMPA), allocatable, dimension(:) :: E_array	 
	 
	 
	 !More MPI types for communication
	 integer :: elem_type,cell_v_type
	 
	 END MODULE MPI_ARRAYS
	

!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      MODULE SolMatrix_Arrays
! 
         SAVE
! 
! ----------
! ...... Double precision allocatable arrays     
! ----------
! 
         REAL(KIND = 8), ALLOCATABLE, DIMENSION(:) :: CO
         REAL(KIND = 8), ALLOCATABLE, DIMENSION(:) :: WKAREA
         REAL(KIND = 8), ALLOCATABLE, DIMENSION(:) :: RWORK
! 
         REAL(KIND = 8), ALLOCATABLE, DIMENSION(:,:) :: AB
! 
! ----------
! ...... Integer allocatable arrays     
! ----------
! 
         INTEGER, ALLOCATABLE, DIMENSION(:) :: IRN,ICN
         INTEGER, ALLOCATABLE, DIMENSION(:) :: JVECT
! 
         INTEGER, ALLOCATABLE, DIMENSION(:) :: IIJJKK,NO
         INTEGER, ALLOCATABLE, DIMENSION(:) :: IJKMM,IJKPP
         INTEGER, ALLOCATABLE, DIMENSION(:) :: ISUMMM,ISUMPP
! 
         INTEGER, ALLOCATABLE, DIMENSION(:,:)   :: NX0
         INTEGER, ALLOCATABLE, DIMENSION(:,:,:) :: NA0,NB0,NC0
! 
! 
      END MODULE SolMatrix_Arrays
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      MODULE Q_Arrays
! 
         SAVE
! 
! ----------
! ...... Derived-type variables     
! ----------
! 
         TYPE SourceSink
!
            CHARACTER(LEN = 5) :: name
            CHARACTER(LEN = 8) :: name_el
            CHARACTER(LEN = 5) :: type,dlv_file_name
!
            INTEGER            :: el_num
            INTEGER            :: n_TableP
            INTEGER(KIND = 1)  :: typ_indx
!
            REAL(KIND = 8) :: rate_m,rate_v,rate_res,enth
            REAL(KIND = 8) :: pi,bhp,z_layer,grad
!
            REAL(KIND = 8), DIMENSION(:), POINTER :: frac_flo
            REAL(KIND = 8), DIMENSION(:), POINTER :: rate_phs
!
         END TYPE SourceSink
! 
! 
! 
         TYPE Tabular
            INTEGER :: N_points
            REAL(KIND = 8), DIMENSION(:), POINTER :: p_TList,p_QList
            REAL(KIND = 8), DIMENSION(:), POINTER :: p_HList
         END TYPE Tabular
! 
! 
! 
         TYPE Tab_BHP
            INTEGER                               :: idTab,NumG,NumH
            REAL(KIND = 8), DIMENSION(:), POINTER :: p_bhp
         END TYPE Tab_BHP
! 
! ----------
! ...... Double precision allocatable arrays     
! ----------
! 
         REAL(KIND = 8), ALLOCATABLE, DIMENSION(:) :: F1,F2,F3
! 
! ----------
! ...... Derived-type arrays     
! ----------
! 
         TYPE(SourceSink), ALLOCATABLE, DIMENSION(:) :: SS
! 
         TYPE(Tabular),    ALLOCATABLE, DIMENSION(:) :: Well_TData
! 
         TYPE(Tab_BHP),    ALLOCATABLE, DIMENSION(:) :: WDel
! 
! 
      END MODULE Q_Arrays
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      MODULE TempStoFlo_Arrays
! 
         SAVE
! 
! ----------
! ...... Double precision arrays     
! ----------
! 
         REAL(KIND = 8), DIMENSION(10)    :: DEPU
! 
         REAL(KIND = 8), DIMENSION(11,12) :: D
! 
         REAL(KIND = 8), DIMENSION(11,23) :: F
! 
! 
      END MODULE TempStoFlo_Arrays
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      MODULE PFMedProp_Arrays
! 
         SAVE
! 
! ----------
! ...... Derived-type variables     
! ----------
! 
         TYPE PFMedium
! 
            INTEGER :: NumF_RP,   &    ! The number of the relative permeability function
                      NumF_CP        ! The number of the capillary function
! 
            REAL(KIND = 8) :: DensG,&  ! The PF medium grain density (kg/m3)
                             Poros, & ! The PF medium porosity
                             SpcHt,  &! The PF medium specific heat (J/kg/C)
                             KThrW,  &! The thermal conductivity of the saturated PF medium (W/m/C)
                             KThrD   ! The thermal conductivity of the dry PF medium (W/m/C)
! 
            REAL(KIND = 8) :: Compr,&  ! The pore compressibility (1/Pa)
                             Expan, & ! The pore expansivity (1/C)
                             Tortu, & ! The PF medium tortuosity factor
                             Klink   ! The Kinkenberg parameter (1/Pa)
! 
            REAL(KIND = 8) :: RGrain,& ! The average radius of the medium grain (m)
                             RSpace, &! The average radius of the interstitial space (m)
                             SArea,  &! The surface are per unit volume of the PM (m^2/m^3)
                             NVoid,  &! The number of voids per unit volume of the PM (1/m^3)
                             VVoid   ! The volume of each interstitial void (m^3)
! 
            REAL(KIND = 8), DIMENSION(3) :: Perm   ! Intrinsic permeabilities along principal axes (m^2)
! 
            REAL(KIND = 8), DIMENSION(7) :: ParRP  ! Parameters of the relative permeability functions
! 
            REAL(KIND = 8), DIMENSION(7) :: ParCP   ! Parameters of the capillary pressure functions
! 
         END TYPE PFMedium
! 
! 
! 
         TYPE PFMedium1
! 
            REAL(KIND = 8) :: RGrain,& ! The average radius of the medium grain (m)
                             SArea,  &! The surface area per unit volume of the PM (m^2/m^3)
                             RVoid,  &! The average radius of the interstitial space (m)
                             NVoid,  &! The number of voids per unit volume of the PM (1/m^3)
                             VVoid   ! The volume of each void (m^3)
! 
         END TYPE PFMedium1
! 
! ----------
! ...... Derived-type arrays     
! ----------
! 
         TYPE(PFMedium),  ALLOCATABLE, DIMENSION(:) :: media
! 
         TYPE(PFMedium1), ALLOCATABLE, DIMENSION(:) :: PoMed
! 
! ----------
! ...... Double precision allocatable arrays     
! ----------
! 
         REAL(KIND = 8), ALLOCATABLE, DIMENSION(:,:) :: XIN,YIN
! 
! ----------
! ...... Double precision arrays     
! ----------
! 
         REAL(KIND = 8), DIMENSION(7) :: RPD,CPD
! 
! ----------
! ...... Integer allocatable arrays     
! ----------
! 
         INTEGER(KIND = 1), ALLOCATABLE, DIMENSION(:) :: StateI
         INTEGER(KIND = 1), ALLOCATABLE, DIMENSION(:) :: StateIndex
! 
! ----------
! ...... Character allocatable arrays     
! ----------
! 
         CHARACTER(LEN = 5), ALLOCATABLE, DIMENSION(:) :: MAT,Rk_name
! 
! 
      END MODULE PFMedProp_Arrays
!
!=====================================================================

!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE Allocate_MemGen
! 
         USE EOS_Parameters
         USE EOS_DefaultParam
! 
         USE Basic_Param
         USE GenControl_Param
         USE Diffusion_Param
! 
         USE Element_Arrays
         USE Connection_Arrays
         USE Variable_Arrays
         USE SolMatrix_Arrays
         USE Q_Arrays
         USE PFMedProp_Arrays
		 USE MPI_param
		 USE MPI_SUBROUTINES
! 
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*     MAIN ROUTINE FOR ALLOCATING MEMORY TO MOST TOUGH-Fx ARRAYS      *
!*                                                                     *
!***********************************************************************
!***********************************************************************
! 
         IMPLICIT NONE
! 
! ----------
! ...... Integer Arrays    
! ----------
! 
         INTEGER, DIMENSION(0:100) :: ierror = 99999
! 
! ----------
! ...... Integer variables     
! ----------
! 
         INTEGER :: NREDM_t,MPRIM_t,ii
         INTEGER :: i,j,ierr
! 
! ----------
! ...... Character variables     
! ----------
! 
         CHARACTER(LEN = 15) :: B_name = '               '
         CHARACTER(LEN = 26) :: HEADR
! 
! ----------
! ...... Logical variables     
! ----------
! 
         LOGICAL EX
! 
         SAVE 
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of Allocate_MemGen
!
!
!write(*,*) ' i am here in allloc 896'
		if(MPI_rank==0) then
         WRITE(11,6000)
		end if
! 
! 
!***********************************************************************
!*                                                                     *
!*              OPEN FILE TO DOCUMENT MEMORY ALLOCATION                *
!*                                                                     *
!***********************************************************************
! 
! 
       OPEN(50,FILE='ALLOC')
! 
! 
!***********************************************************************
!*                                                                     *
!*              READ NUMBERS OF ELEMENTS AND CONNECTIONS               *
!*                                                                     *
!***********************************************************************
! 
! 
!write(*,*) ' i am here in allloc 896'

         if(MPI_rank==0) then
			READ(100,5000), HEADR 
!write(*,*) ' i am here in allloc 896'    
!
			IF_Head: IF(HEADR /= 'TOUGH-Fx MEMORY ALLOCATION') THEN
						 WRITE(6, 6002)
						 WRITE(50,6002)
						 call MPI_ABORT(MPI_COMM_WORLD,0,ierr)
					END IF IF_Head
!
			READ(100,5001), B_name
			EOS_Name = TRIM(ADJUSTL(B_name))
			FORALL (i=1:15) TEMP_CHAR(i) = EOS_Name(i:i)
		 end if
		 call MPI_BROADCAST(CHAR_ARRAY=TEMP_CHAR(1:15),char_size=15,root=0)
		 write(EOS_Name,'(15A1)'), TEMP_CHAR(1:15)
		 
! 
!
!write(*,*) ' i am here in allloc 896'
!***********************************************************************
!*                                                                     *
!*  READ THE NUMBER OF COMPONENTS, EQUATIONS, PHASES, SEC. VARIABLES   *
!*                                                                     *
!***********************************************************************
! 
!
         if(MPI_rank==0) READ(100,5002),TEMP_INT(1:5)
		 call MPI_BROADCAST(INT_ARRAY=TEMP_INT(1:5),int_size=5,root=0)
		 NK=TEMP_INT(1)
		 NEQ=TEMP_INT(2)
		 NPH=TEMP_INT(3)
		 M_BinDif=TEMP_INT(4)
		 M_add=TEMP_INT(5)
		 
!	
! ...... Number of supplementary parameters 
!
         IF(M_add <= 0) M_Add = 1             
! 
! 
!***********************************************************************
!*                                                                     *
!*   DETERMINE DEFAULT PARAMETERS FOR THE VARIOUS EQUATIONS OF STATE   *
!*                                                                     *
!***********************************************************************
! 
! 
        CALL EOS_DefaultNum(ADJUSTR(EOS_Name),&
                           MaxNum_MComp,MaxNum_Equat,&
                           MaxNum_Phase,MaxNum_MobPh,M_Add)
!
! ...... Determine the number of mobile phases 
!
         Num_MobPhs = MaxNum_MobPh
         NPH        = MaxNum_Phase
!
! ...... For hydrates, ensure that NEQ = NK+1 
!
         IF(EOS_Name(1:7) == 'HYDRATE' .AND. NEQ /= (NK+1) .AND. MPI_RANK==0) THEN
				WRITE(6, 6004) NEQ,NK
				STOP
         END IF
!
! ----------
! ...... Ensure that NK,NEQ,NPH do not exceed the maximum numbers 
! ----------
!
         IF(((NK  > MaxNum_MComp) .OR. &
           (NEQ > MaxNum_Equat) .OR. &
           (NPH > MaxNum_Phase) &
          ) .AND. MPI_RANK==0) THEN
            PRINT 6010, NK,NEQ,NPH,&
                       MaxNum_MComp,MaxNum_Equat,MaxNum_Phase  
            STOP
         END IF
!
! ...... Number of secondary parameters (to be stored in the DP array)
!
         IF(M_BinDif == 0) THEN
            Num_SecPar = 8*MaxNum_Phase+1+M_add
         ELSE
            Num_SecPar = 10*MaxNum_Phase+1+M_add
         END IF
!
! ----------
! ...... Read the number of elements, connections and 
! ...... number of characters in the element name
! ----------
!
         IF(MPI_RANK==0) READ(100, 5004), TEMP_INT(1:3),TEMP_CHAR(1:6)
		 call MPI_BROADCAST(INT_ARRAY=TEMP_INT(1:3),int_size=3,CHAR_ARRAY=TEMP_CHAR(1:6),&
							char_size=6,root=0)
		 MaxNum_Elem=TEMP_INT(1)
		 MaxNum_Conx=TEMP_INT(2)
		 No_CEN=TEMP_INT(3)
		 write(FLG_con,'(6A1)'),TEMP_CHAR(1:6)
		 
!
!
!
!***********************************************************************
!*                                                                     *
!*            MEMORY ALLOCATION TO ELEMENT-RELATED ARRAYS              *
!*                                                                     *
!***********************************************************************
!
!
         IF(No_CEN /= 8) No_CEN = 5
! 
! ----------
! ...... Allocate memory to derived-type arrays  
! ----------
! 
		 if(MPI_rank==0) then
			ALLOCATE(elem(MaxNum_Elem), STAT=ierror(1))
! 
! ----------
! ...... Allocate memory to double precision allocatable arrays     
! ----------
	! 
			 ALLOCATE(AI(MaxNum_Elem),      STAT=ierror(2))
			 ALLOCATE(AHT(MaxNum_Elem),     STAT=ierror(3))
	! 
			 ALLOCATE(Pm(MaxNum_Elem),      STAT=ierror(4))
			 ALLOCATE(X_Coord(MaxNum_Elem),&
					 Y_Coord(MaxNum_Elem),&
					 Z_coord(MaxNum_Elem), STAT=ierror(5))
		
!
!
!***********************************************************************
!*                                                                     *
!*          MEMORY ALLOCATION TO CONNECTION-RELATED ARRAYS             *
!*                                                                     *
!***********************************************************************
! 
! ----------
! ...... Allocate memory to derived-type arrays  
! ----------
	! 
			 ALLOCATE(conx(MaxNum_Conx), STAT=ierror(6))
	! 
			 IF(MaxNum_Phase > 1) THEN
				ALLOCATE(conV(MaxNum_Conx), STAT=ierror(7))
			 END IF
	! 
	! ----------
	! ...... Allocate memory to arrays within the derived type: Conx  
	! ----------
	! 
			 DO_Conx1 : DO i = 1,MaxNum_Conx
	! 
				ALLOCATE(Conx(i)%FluxF(Num_MobPhs), &
						Conx(i)%VelPh(Num_MobPhs), &
						STAT = ierror(8))
	! 
			 END DO DO_Conx1
	! 
	! ----------
	! ...... Allocate memory to double precision allocatable arrays     
	! ----------
	! 
			 ALLOCATE(sig(MaxNum_Conx), STAT=ierror(9))
		end if
!
!
!
!***********************************************************************
!*                                                                     *
!*          MEMORY ALLOCATION TO SOURCE/SINK-RELATED ARRAYS            *
!*                                                                     *
!***********************************************************************
!
!
         if(MPI_RANK==0) READ(100, 5002), TEMP_INT(1)
		 call MPI_BROADCAST(INT_ARRAY=TEMP_INT(1),int_size=1,root=0)
		 MaxNum_SS=TEMP_INT(1)
! 
! ----------
! ...... Allocate memory to derived-type     
! ----------
! 
         ALLOCATE(SS(MaxNum_SS), STAT = ierror(20))
! 
         IF(MaxNum_Phase > 1) THEN
            DO_SS1 : DO i = 1,MaxNum_SS
               ALLOCATE(SS(i)%frac_flo(Num_MobPhs), &
                       SS(i)%rate_phs(Num_MobPhs),&
                       STAT = ierror(21))
            END DO DO_SS1
         END IF
! 
         ALLOCATE(Well_TData(MaxNum_SS), STAT = ierror(21))
! 
         ALLOCATE(WDel(MaxNum_SS),       STAT = ierror(22))
!
!
!***********************************************************************
!*                                                                     *
!*           MEMORY ALLOCATION TO ARRAYS OF ROCK PROPERTIES            *
!*                                                                     *
!***********************************************************************
!
!
         if(MPI_RANK==0) READ(100, 5002), TEMP_INT(1)
		 call MPI_BROADCAST(INT_ARRAY=TEMP_INT(1),int_size=1,root=0)
		 MaxNum_Media=TEMP_INT(1)
! 
! ----------
! ...... Allocate memory to derived-type allocatable arrays     
! ----------
! 
         ALLOCATE(PoMed(MaxNum_Media), STAT=ierror(23))
! 
         ALLOCATE(media(MaxNum_Media), STAT=ierror(24))
! 
! 
!***********************************************************************
!*                                                                     *
!*         ENSURE PROPER MEMORY ALLOCATION - STOP IF PROBEMS           *
!*                                                                     *
!***********************************************************************
! 
! 
         DO_InitCheck: DO ii=0,24
! 
            IF(ierror(ii) == 0) THEN
               WRITE(50,6015) ii,MPI_RANK
            ELSE IF(ierror(ii) == 99999) THEN
               CONTINUE
            ELSE
               PRINT 6020, ii,MPI_RANK
               WRITE(50,6020) ii,MPI_RANK
               STOP
            END IF
! 
         END DO DO_InitCheck
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 5000 FORMAT(A26)
 5001 FORMAT(A15)
 5002 FORMAT(5(I5))
 5004 FORMAT(2(I10),I2,2x,6A1)
! 
 6000 FORMAT(/,'Allocate_MemGen   1.0   29 September 2003',6X, &
              'Allocate memory to most arrays based on size ',&
              'provided by input parameters')
! 
 6002 FORMAT(//,20('ERROR-'),//,T33, &
                  '       S I M U L A T I O N   A B O R T E D',/,&
              T20,"The header 'TOUGH-Fx MEMORY ALLOCATION' is ",&
                  'missing at the top of the input file'&
            /,T32,'               CORRECT AND TRY AGAIN',&     
            //,20('ERROR-'))
 6003 FORMAT(//,20('ERROR-'),//,T33, &
                  '       S I M U L A T I O N   A B O R T E D',/,&
              T20,'The equation of state specified by EOS_Name = ',&
              A15,' is unavailable'&
            /,T32,'               CORRECT AND TRY AGAIN',     &
            //,20('ERROR-'))
 6004 FORMAT(//,20('ERROR-'),//,T33,&
                  '       S I M U L A T I O N   A B O R T E D',//,&
              T08,'The number of equation NEQ =',i2,&
                  ' <= the number of mass components NK =',i2,&
                  ' : In hydrate simulation, NEQ = NK+1 '&
            /,T32,'               CORRECT AND TRY AGAIN',/,     &
            //,20('ERROR-'))
 6010 FORMAT(//,20('ERROR-'),//,T33,&
                  '       S I M U L A T I O N   A B O R T E D',//,&
              T10,'One or more of the parameters NK,NEQ,NPH = (',&
                 i2,',',i2,',',i2,')',/,&
              T10,'exceed the maximum values ',&
                  '"MaxNum_MComp", "MaxNum_Equat", ',&
                  '"MaxNum_Phase" = (',i2,',',i2,',',i2,')',/,&
            /,T32,'               CORRECT AND TRY AGAIN',     &
            //,20('ERROR-'))
 6015 FORMAT(T2,' Memory allocation at point ',i3,&
               ' in subroutine "Allocate_MemGen" was successful at process rank',i3)
 6020 FORMAT(//,20('ERROR-'),//,&
            T2,' Memory allocation at point ',i3,&
               ' in subroutine "Allocate_MemGen" was unsuccessful',//,&
			&'at process rank=',i3,//,&
            T2,'!!!!!!!    THE EXECUTION WAS STOPPED    !!!!!!!',&
            //,20('ERROR-'))
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of Allocate_MemGen
!
!
      END SUBROUTINE Allocate_MemGen
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE Allocate_MemMatrx
! 
! ... Modules to be used 
! 
         USE Basic_Param
         USE SolMatrix_Arrays
         USE GenControl_Param
		 USE MPI_PARAM
		 USE MPI_ARRAYS
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*  ROUTINE FOR MEMORY ALLOCATION TO SOLUTION MATRIX AND WORK ARRAYS   *
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
         IMPLICIT NONE
! 
! ----------
! ...... Integer Arrays    
! ----------
! 
         INTEGER, DIMENSION(25:32) :: ierror = 99999
! 
! ----------
! ...... Integer Variables    
! ----------
! 
! 
         INTEGER :: ii
! 
         SAVE 
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of Allocate_MemMatrx
!
!
		 if(MPI_RANK==0) THEN		
			 WRITE(11,6000)
			 WRITE(50,6005)         
		 end if
!
! ....Account for the solver memory needs
!
         MNZ   = (NTL+2*NCONT)*NEQ*NEQ
         MNZP1 =  MNZ+1
		 MAAS = MNZ*2*(overlap+1)
!
! ....Account for the solver needs
!
    
         IF_Solver: IF(MOP(21) == 4) THEN
                       NRWORK = max( 1000+2*mnz+38*NREDM, &
                                    9*MaxNum_Conx+1000&
                                  )
                       NIWORK = 100+mnz+5*NREDM
                    ELSE  
                       NRWORK = max( 1000+mnz+10*NREDM +N_sub*(2*MAAS+NREDM),&
                                    9*MaxNum_Conx+1000 +N_sub*(2*MAAS+NREDM)&
                                  )
                       NIWORK = 100+mnz+5*NREDM +N_sub*(4*MAAS+NREDM)
                    END IF IF_Solver 
!
! ....Account for connection reordering
!
         IF_ActCon: IF(FLG_con == 'ACTIVE') THEN
                       MNZ    = 1
                       NRWORK = 5*MaxNum_Elem
                       NIWORK = 1
                    END IF IF_ActCon
!
!***********************************************************************
!*                                                                     *
!*        MEMORY ALLOCATION TO SOLUTION MATRIX AND WORK ARRAYS         *
!*                                                                     *
!***********************************************************************
!
         LIRN = MNZ
         LICN = MNZ+1
! 
         lenw  = NRWORK
         leniw = NIWORK
! 
! ----------
! ...... Allocate memory to double precision allocatable arrays     
! ----------
! 
         ALLOCATE(CO(mnz+1),      STAT=ierror(25))
         ALLOCATE(RWORK(NRWORK),  STAT=ierror(26))
! 
! ----------
! ...... Allocate memory to integer allocatable arrays     
! ----------
! 
         ALLOCATE(IRN(mnz+1),    STAT=ierror(27))
         ALLOCATE(ICN(mnz+1),    STAT=ierror(28))
!
         ALLOCATE(JVECT(niwork), STAT=ierror(29))
! 
		 ALLOCATE(update_ps(MAAS), STAT = ierror(30))
		 ALLOCATE(update_ls(MAAS),STAT = ierror(31))
		 ALLOCATE(update_gs(MAAS),STAT = ierror(32))
		
		
! 
!***********************************************************************
!*                                                                     *
!*         ENSURE PROPER MEMORY ALLOCATION - STOP IF PROBEMS           *
!*                                                                     *
!***********************************************************************
! 
! 
         DO_InitCheck: DO ii=25,32
! 
            IF(ierror(ii) == 0) THEN
               WRITE(50,6001) ii
            ELSE IF(ierror(ii) == 99999) THEN
               CONTINUE
            ELSE
               PRINT 6002, ii,MPI_RANK
               WRITE(50,6002) ii,MPI_RANK
               STOP
            END IF
! 
         END DO DO_InitCheck
! 
! ----------
! ...... Print information on the size of the arrays    
! ----------
! 
         PRINT 6010, MNZ,LIRN,LICN,NRWORK,NIWORK
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'Allocate_MemMatrx 1.0   27 April     2003',6X,&
              'Allocate memory to the Jacobian matrix arrays')
! 
 6001 FORMAT(T2,' Memory allocation at point ',i3,&
               ' in subroutine "Allocate_MemMatrx" was successful')
 6002 FORMAT(//,20('ERROR-'),//,&
            T2,' Memory allocation at point ',i3,&
               ' in subroutine "Allocate_MemMatrx" was unsuccessful in mpi rank',i3,//,&
            T2,'!!!!!!!    THE EXECUTION WAS STOPPED    !!!!!!!',&
            //,20('ERROR-'))
 6005 FORMAT(/)
! 
 6010 FORMAT(' ',131('='),//, &
            ' Maximum number of Jacobian matrix elements:',16X,&
            ' MNZ  =',I9,//,&
            ' Large linear equation arrays: Length of IRN is',13X,&
            ' LIRN =',I9,/,&
            '                               Length of ICN',&
            ' and CO is',7X,'LICN =',I9,//,&
            ' Maximum length of the Conjugate Gradient double',&
            ' precision work array RWORK is NRWORK = ',i10,/,&
            ' Maximum length of the Conjugate Gradient integer',&
            ' array JVECT is NIWORK               = ',i10,//,&
            ' ',131('='))
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of Allocate_MemMatrx
!
!
      END SUBROUTINE Allocate_MemMatrx
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE Allocate_MemPrPro(ix)
! 
! ... Modules to be used 
! 
         USE Basic_Param
         USE SolMatrix_Arrays
         USE GenControl_Param
		 USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*    ROUTINE FOR MEMORY ALLOCATION TO MATRIX PRE-PROCESSING ARRAYS    *
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
         IMPLICIT NONE
! 
! ----------
! ...... Integer Arrays    
! ----------
! 
         INTEGER, DIMENSION(30:39) :: ierror = 99999
! 
! ----------
! ...... Integer Variables    
! ----------
! 
! 
         INTEGER, INTENT(IN) :: ix
! 
         INTEGER :: ii
! 
         SAVE 
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of Allocate_MemPrPro
!
!
		 if(MPI_RANK==0) THEN
			 WRITE(11,6000)
			 WRITE(50,6005)         
		 end if
! 
         IF_Pepro: IF(ix == 0) THEN
!
!
! ...... NCONUP: The total number of connections of elements with 
! ......         other elements of larger element numbers 
! ...... NCONDN: The total number of connections of elements with
! ......         other elements of smaller element numbers 
!
         NCONUP = 11*NCONT/10
         NCONDN = NCONUP
!***********************************************************************
!*                                                                     *
!*      MEMORY ALLOCATION TO INTEGER MATRIX PRE-PROCESSING ARRAYS      *
!*         Integer allocatable arrays, iterative solvers only          *
!*                                                                     *
!***********************************************************************
!
!
            ALLOCATE(NX0(NEQ,NTL+1),           STAT=ierror(30))
            ALLOCATE(NB0(NEQ,NEQ,NTL+1),       STAT=ierror(31))
            ALLOCATE(NA0(NCONDN,NEQ,NEQ),              STAT=ierror(32))
            ALLOCATE(NC0(NCONUP,NEQ,NEQ),              STAT=ierror(33))
!
            ALLOCATE(IIJJKK(NTL+1),            STAT=ierror(34))
            ALLOCATE(NO(NTL+2*NCONT+10), STAT=ierror(35))
            ALLOCATE(IJKMM(NTL+1),             STAT=ierror(36))
            ALLOCATE(IJKPP(NTL+1),             STAT=ierror(37))
            ALLOCATE(ISUMMM(0:NTL+1),          STAT=ierror(38))
            ALLOCATE(ISUMPP(0:NTL+1),          STAT=ierror(39))
!
         ELSE
!
!
!***********************************************************************
!*                                                                     *
!*       DUMMY MEMORY ALLOCATION TO MATRIX PRE-PROCESSING ARRAYS       *
!*         Integer allocatable arrays, iterative solvers only          *
!*                                                                     *
!***********************************************************************
!
!
            ALLOCATE(NX0(1,1),   STAT=ierror(30))
            ALLOCATE(NB0(1,1,1), STAT=ierror(31))
            ALLOCATE(NA0(1,1,1), STAT=ierror(32))
            ALLOCATE(NC0(1,1,1), STAT=ierror(33))
!
            ALLOCATE(IIJJKK(1),  STAT=ierror(34))
            ALLOCATE(NO(1),      STAT=ierror(35))
            ALLOCATE(IJKMM(1),   STAT=ierror(36))
            ALLOCATE(IJKPP(1),   STAT=ierror(37))
            ALLOCATE(ISUMMM(1),  STAT=ierror(38))
            ALLOCATE(ISUMPP(1),  STAT=ierror(39))
!
         END IF IF_Pepro
! 
! 
!***********************************************************************
!*                                                                     *
!*         ENSURE PROPER MEMORY ALLOCATION - STOP IF PROBEMS           *
!*                                                                     *
!***********************************************************************
! 
! 
         DO_InitCheck: DO ii=30,39
! 
            IF(ierror(ii) == 0) THEN
               WRITE(50,6001) ii
            ELSE IF(ierror(ii) == 99999) THEN
               CONTINUE
            ELSE
               PRINT 6002, ii
               WRITE(50,6002) ii
               STOP
            END IF
! 
         END DO DO_InitCheck
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'Allocate_MemPrPro 1.0   27 April     2003',6X,&
              'Allocate memory to arrays for matrix preprocessing')
! 
 6001 FORMAT(T2,' Memory allocation at point ',i3,&
               ' in subroutine "Allocate_MemPrePro" was successful')
 6002 FORMAT(//,20('ERROR-'),//,&
            T2,' Memory allocation at point ',i3,&
               ' in subroutine "Allocate_MemPrePro" was unsuccessful',&
            //,T2,'!!!!!!!    THE EXECUTION WAS STOPPED    !!!!!!!',&
            //,20('ERROR-'))
 6005 FORMAT(/)
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of Allocate_MemPrPro
!
!
      END SUBROUTINE Allocate_MemPrPro
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE Allocate_Input
! 
! ... Modules to be used 
! 
         USE Input_Comments
         USE Basic_Param
! 
         USE GenControl_Param
         USE TimeSeriesP_Param
! 
         USE PFMedProp_Arrays
		 USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*       ROUTINE FOR MEMORY ALLOCATION TO TEMPORARY INPUT ARRAYS       *
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
         IMPLICIT NONE
! 
! ----------
! ...... Integer Arrays    
! ----------
! 
         INTEGER, DIMENSION(40:44) :: ierror = 99999
! 
! ----------
! ...... Integer Variables    
! ----------
! 
! 
         INTEGER :: ii
! 
         SAVE 
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of Allocate_Input
!
!
         if(MPI_rank==0) WRITE(11,6000)
         if(MPI_rank==0) WRITE(50,6005)         
! 
         ALLOCATE(Rk_name(MaxNum_Media),   &
                 MAT(MaxNum_Media),        STAT=ierror(40))
! 
         ALLOCATE(StateI(MaxNum_Media), &     
                 StateIndex(MaxNum_Media), STAT=ierror(41))
!
         ALLOCATE(comm(50),                 STAT=ierror(42))
!  
         ALLOCATE(XIN(NEQ,MaxNum_Media),    STAT=ierror(43))
         ALLOCATE(YIN(NEQ,MaxNum_Media),    STAT=ierror(44))
! 
! 
!***********************************************************************
!*                                                                     *
!*         ENSURE PROPER MEMORY ALLOCATION - STOP IF PROBEMS           *
!*                                                                     *
!***********************************************************************
! 
! 
         DO_InitCheck: DO ii=40,44
! 
            IF(ierror(ii) == 0) THEN
               WRITE(50,6001) ii,MPI_RANK
            ELSE IF(ierror(ii) == 99999) THEN
               CONTINUE
            ELSE
               PRINT 6002, ii,MPI_RANK
               WRITE(50,6002) ii,MPI_RANK
               STOP
            END IF
! 
         END DO DO_InitCheck
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'Allocate_Input    1.0   27 April     2003',6X,&
              'Memory allocation to temporary input arrays')
! 
 6001 FORMAT(T2,' Memory allocation at point ',i3,&
               ' in subroutine "Allocate_Input" was successful in process rank',i3)
 6002 FORMAT(//,20('ERROR-'),//,&
            T2,' Memory allocation at point ',i3,&
               ' in subroutine "Allocate_Input" was unsuccessful',&
			   &' in process rank',i3,&
            //,T2,'!!!!!!!    THE EXECUTION WAS STOPPED    !!!!!!!',&
            //,20('ERROR-'))
 6005 FORMAT(/)
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of Allocate_Input
!
!
      END SUBROUTINE Allocate_Input
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE DeAllocate_Input
! 
! ... Modules to be used 
! 
         USE Input_Comments
! 
         USE Basic_Param
         USE GenControl_Param
         USE Hydrate_Param
! 
         USE Element_Arrays
         USE PFMedProp_Arrays
         USE Q_Arrays
		 USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*     ROUTINE FOR MEMORY DEALLOCATION FROM TEMPORARY INPUT ARRAYS     *
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
         IMPLICIT NONE
! 
! ----------
! ...... Integer Arrays    
! ----------
! 
         INTEGER, DIMENSION(40:44) :: ierror = 99999
! 
! ----------
! ...... Integer Variables    
! ----------
! 
! 
         INTEGER :: ii,ierG = 0
! 
         SAVE 
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DeAllocate_Input
!
!
		 IF(MPI_RANK == 0) THEN
			 WRITE(11,6000)
			 WRITE(50,6005)         
		 END IF
! 
         DEALLOCATE(Rk_name,MAT,        STAT=ierror(40))  ! PF medium name
         DEALLOCATE(StateI,StateIndex,  STAT=ierror(41))  ! PF state index
!
         DEALLOCATE(comm,    STAT=ierror(42))  ! Comments in input file
!
         DEALLOCATE(XIN,     STAT=ierror(43))  ! PF medium domain-wise initial conditions
         DEALLOCATE(YIN,     STAT=ierror(44))  ! PF medium domain-wise initial conditions
! 
! 
!***********************************************************************
!*                                                                     *
!*        ENSURE PROPER MEMORY DEALLOCATION - STOP IF PROBEMS          *
!*                                                                     *
!***********************************************************************
! 
! 
         DO_InitCheck: DO ii=40,44
! 
            IF(ierror(ii) == 0) THEN
               WRITE(50,6001) ii
            ELSE IF(ierror(ii) == 99999) THEN
               CONTINUE
            ELSE
               PRINT 6002, ii,MPI_RANK           ! Unsuccesful deallocation
               WRITE(50,6002) ii,MPI_RANK
               STOP
            END IF
! 
         END DO DO_InitCheck
! 
! ----------
! ...... Interface area for heat exchange with confining beds (semi-analytical solution)   
! ----------
! 
         IF(MOP(15) <= 1 ) DEALLOCATE(AI, STAT=ierG)   
! 
! ...... Check if properly deallocated 
! 
         IF(ierG == 0) THEN
            WRITE(50,6001) 2        ! Succesful deallocation 
         ELSE
            PRINT 6002, 2           ! Unsuccesful deallocation
            WRITE(50,6002) 2        ! Unsuccesful deallocation
            STOP
         END IF
! 
! ----------
! ...... Interface area for heat exchange with confining beds (semi-analytical solution)   
! ----------
! 
         IF(MOP(15) <= 1) DEALLOCATE(AHT, STAT=ierG)   
! 
! ...... Check if properly deallocated 
! 
         IF(ierG == 0) THEN
            WRITE(50,6001) 3        ! Succesful deallocation 
         ELSE
            PRINT 6002, 3           ! Unsuccesful deallocation
            WRITE(50,6002) 3        ! Unsuccesful deallocation
            STOP
         END IF
! 
! ----------
! ...... X,Y,Z coordinates (used only when dispersion is considered)   
! ----------
! 
         IF(CoordNeed .EQV. .FALSE.) THEN
            DEALLOCATE(X_Coord,Y_Coord,Z_Coord, STAT=ierG)
         END IF
! 
! ...... Check if properly deallocated 
! 
         IF(ierG == 0) THEN
            WRITE(50,6001) 5        ! Succesful deallocation 
         ELSE
            PRINT 6002, 5           ! Unsuccesful deallocation
            WRITE(50,6002) 5        ! Unsuccesful deallocation
            STOP
         END IF
! 
! ----------
! ...... Tabular well data  
! ----------
! 
         IF(WellTabData .EQV. .FALSE.) DEALLOCATE(Well_TData, STAT=ierG)
! 
! ...... Check if properly deallocated 
! 
         IF(ierG == 0) THEN
            WRITE(50,6001) 22       ! Succesful deallocation 
         ELSE
            PRINT 6002,    22,MPI_RANK       ! Unsuccesful deallocation
            WRITE(50,6002) 22,MPI_RANK       ! Unsuccesful deallocation
            STOP
         END IF
! 
! ----------
! ...... Pore void parameters for kinetic hydrate dissociation   
! ----------
! 
         IF((EOS_Name(1:7) /= 'HYDRATE') .OR. &
           ( EOS_Name(1:7) == 'HYDRATE' .AND. &
            (F_Kinetic .EQV. .FALSE.)))&
        THEN 
            DEALLOCATE(PoMed, STAT=ierG)
         END IF
! 
! ...... Check if properly deallocated 
! 
         IF(ierG == 0) THEN
            WRITE(50,6001) 23      ! Succesful deallocation 
         ELSE
            PRINT 6002,    23      ! Unsuccesful deallocation
            WRITE(50,6002) 23      ! Unsuccesful deallocation
            STOP
         END IF
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'DeAllocate_Input  1.0   27 April     2003',6X,&
              'Deallocation of memory from all temporary and ',&
              'unnecessary arrays')
! 
 6001 FORMAT(T2,' Memory deallocation at point ',i3,&
               ' in subroutine "DeAllocate_Input" was successful')
 6002 FORMAT(//,20('ERROR-'),//,&
            T2,' Memory deallocation at point ',i3,&
               ' in subroutine "DeAllocate_Input" was unsuccessful in processor',I3,&
            //,T2,'!!!!!!!    THE EXECUTION WAS STOPPED    !!!!!!!',&
            //,20('ERROR-'))
 6005 FORMAT(/)
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DeAllocate_Input
!
!
      END SUBROUTINE DeAllocate_Input
!
!
!
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

   SUBROUTINE ALLOCATELOC
! 
         USE EOS_Parameters
         USE EOS_DefaultParam
! 
         USE Basic_Param
         USE GenControl_Param
         USE Diffusion_Param
! 
         USE Element_Arrays
         USE Connection_Arrays
         USE Variable_Arrays
         USE SolMatrix_Arrays
         USE Q_Arrays
         USE PFMedProp_Arrays
		 USE MPI_param
		 USE MPI_SUBROUTINES
		 USE MPI_ARRAYS
! 
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*     ROUTINE FOR ALLOCATING LOCAL MEMORY TO MOST TOUGH-Fx ARRAYS      *
!*                                                                     *
!***********************************************************************
!***********************************************************************
! 
         IMPLICIT NONE
! 
! ----------
! ...... Integer Arrays    
! ----------
! 
         INTEGER, DIMENSION(0:100) :: ierror = 99999
! 
! ----------
! ...... Integer variables     
! ----------
! 
         INTEGER :: NREDM_t,MPRIM_t,ii
         INTEGER :: i,j,ierr
! 
! ----------
! ...... Character variables     
! ----------
! 
         CHARACTER(LEN = 15) :: B_name = '               '
         CHARACTER(LEN = 26) :: HEADR
! 
! ----------
! ...... Logical variables     
! ----------
! 
         LOGICAL EX
! 
         SAVE 
!
!
!***********************************************************************
!*                                                                     *
!*            MEMORY ALLOCATION TO ELEMENT-RELATED ARRAYS              *
!*                                                                     *
!***********************************************************************
!
! ...... Allocate memory to derived-type arrays  
! ----------
! 
		 if(MPI_rank==0) then
			DEALLOCATE(elem)
			DEALLOCATE(AHT)
			DEALLOCATE(AI)
			DEALLOCATE(Pm)
			DEALLOCATE(X_COORD)
			DEALLOCATE(Y_COORD)
			DEALLOCATE(Z_COORD)
		 end if
		 
			ALLOCATE(elem(NTL), STAT=ierror(1))
! 
! ----------
! ...... Allocate memory to double precision allocatable arrays     
! ----------
	! 
			 ALLOCATE(AHT(NTL),     STAT=ierror(2))
			 ALLOCATE(AI(NTL),     STAT=ierror(3))
	! 
			 ALLOCATE(Pm(NTL),      STAT=ierror(4))
			 ALLOCATE(X_Coord(NTL),&
					 Y_Coord(NTL),&
					 Z_coord(NTL), STAT=ierror(5))	
!
!
!***********************************************************************
!*                                                                     *
!*          MEMORY ALLOCATION TO CONNECTION-RELATED ARRAYS             *
!*                                                                     *
!***********************************************************************
! 
! ----------
! ...... Allocate memory to derived-type arrays  
! ----------
	! 
			IF(MPI_RANK==0) THEN
				IF(MaxNum_Phase > 1) THEN
					DEALLOCATE(conV)
				END IF
				DEALLOCATE(sig)
			END IF
	! 
			 IF(MaxNum_Phase > 1) THEN
				ALLOCATE(conV(NCONT), STAT=ierror(7))
			 END IF
	! 
	! ----------
	! ...... Allocate memory to arrays within the derived type: Conx  
	! ----------
	! 
			 DO_Conx1 : DO i = 1,NCONT
	! 
				ALLOCATE(Conx(i)%FluxF(Num_MobPhs), &
						Conx(i)%VelPh(Num_MobPhs), &
						STAT = ierror(8))
	! 
			 END DO DO_Conx1
	! 
	! ----------
	! ...... Allocate memory to double precision allocatable arrays     
	! ----------
	! 
			 ALLOCATE(sig(NCONT), STAT=ierror(9))
		
!
!
!***********************************************************************
!*                                                                     *
!*      MEMORY ALLOCATION TO PRIMARY & SECONDARY VARIABLE ARRAYS       *
!*                                                                     *
!***********************************************************************
!
!
         MaxNum_PVar = (MaxNum_MComp+1)*NTL
         NREDM       =  NEQ*NTL
! 
         IF(FLG_con == 'ACTIVE') THEN
            NREDM_t = 1
            MPRIM_t = 1
         ELSE
            NREDM_t = NEQ*NTL
            MPRIM_t = MaxNum_PVar
         END IF
! 
! ----------
! ...... Allocate memory to double precision allocatable arrays     
! ----------
!  
         ALLOCATE(X(MaxNum_PVar),       STAT=ierror(10))
         ALLOCATE(DX(MPRIM_t),          STAT=ierror(11))
         ALLOCATE(DELX(MPRIM_t),        STAT=ierror(12))
!
         ALLOCATE(R(NEQ*NTL+1), STAT=ierror(13))
! 
         ALLOCATE(DOLD(NREDM_t),        STAT=ierror(14))
! 
! ----------
! ...... Allocate memory to derived-type arrays - Secondary variables  
! ----------
! 
         ALLOCATE(Cell_V(NTL,0:NEQ), STAT=ierror(15))
! 
! ----------
! 
!
!***********************************************************************
!*                                                                     *
!*                  MEMORY ALLOCATION TO WORK ARRAYS                   *
!*                                                                     *
!***********************************************************************
!
!
         ALLOCATE(WKAREA(NREDM+10), STAT=ierror(19))
		 

!***********************************************************************
!*                                                                     *
!*         ENSURE PROPER MEMORY ALLOCATION - STOP IF PROBEMS           *
!*                                                                     *
!***********************************************************************
! 
! 
         DO_InitCheck: DO ii=0,24
! 
            IF(ierror(ii) == 0) THEN
               WRITE(50,6015) ii,MPI_RANK
            ELSE IF(ierror(ii) == 99999) THEN
               CONTINUE
            ELSE
               PRINT 6020, ii,MPI_RANK
               WRITE(50,6020) ii,MPI_RANK
               STOP
            END IF
! 
         END DO DO_InitCheck
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 5000 FORMAT(A26)
 5001 FORMAT(A15)
 5002 FORMAT(5(I5))
 5004 FORMAT(2(I10),I2,2x,6A1)
! 
 6000 FORMAT(/,'Allocate_MemGen   1.0   29 September 2003',6X, &
              'Allocate memory to most arrays based on size ',&
              'provided by input parameters')
! 
 6002 FORMAT(//,20('ERROR-'),//,T33, &
                  '       S I M U L A T I O N   A B O R T E D',/,&
              T20,"The header 'TOUGH-Fx MEMORY ALLOCATION' is ",&
                  'missing at the top of the input file'&
            /,T32,'               CORRECT AND TRY AGAIN',&     
            //,20('ERROR-'))
 6003 FORMAT(//,20('ERROR-'),//,T33, &
                  '       S I M U L A T I O N   A B O R T E D',/,&
              T20,'The equation of state specified by EOS_Name = ',&
              A15,' is unavailable'&
            /,T32,'               CORRECT AND TRY AGAIN',     &
            //,20('ERROR-'))
 6004 FORMAT(//,20('ERROR-'),//,T33,&
                  '       S I M U L A T I O N   A B O R T E D',//,&
              T08,'The number of equation NEQ =',i2,&
                  ' <= the number of mass components NK =',i2,&
                  ' : In hydrate simulation, NEQ = NK+1 '&
            /,T32,'               CORRECT AND TRY AGAIN',/,     &
            //,20('ERROR-'))
 6010 FORMAT(//,20('ERROR-'),//,T33,&
                  '       S I M U L A T I O N   A B O R T E D',//,&
              T10,'One or more of the parameters NK,NEQ,NPH = (',&
                 i2,',',i2,',',i2,')',/,&
              T10,'exceed the maximum values ',&
                  '"MaxNum_MComp", "MaxNum_Equat", ',&
                  '"MaxNum_Phase" = (',i2,',',i2,',',i2,')',/,&
            /,T32,'               CORRECT AND TRY AGAIN',     &
            //,20('ERROR-'))
 6015 FORMAT(T2,' Memory allocation at point ',i3,&
               ' in subroutine "Allocate_MemGen" was successful at process rank',i3)
 6020 FORMAT(//,20('ERROR-'),//,&
            T2,' Memory allocation at point ',i3,&
               ' in subroutine "Allocate_MemGen" was unsuccessful',//,&
			&'at process rank=',i3,//,&
            T2,'!!!!!!!    THE EXECUTION WAS STOPPED    !!!!!!!',&
            //,20('ERROR-'))
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of Allocate_MemGen
!
!
      END SUBROUTINE ALLOCATELOC
!
!
!
!=======================================================================
!
!               GLOSSARY OF MAJOR ARRAYS, IN ORDER OF APPEARANCE BELOW
!
!=======================================================================
!
!     ELEMENTS:
!                  *ELEM* HOLDS ELEMENT CODE NAMES (IDENTIFIERS)
!                  *MATX* HOLDS MATERIAL DOMAIN IDENTIFIER
!                  *EVOL* HOLDS VOLUME
!                  *PHI* IS POROSITY
!                  *P* IS PRESSURE AT LAST CONVERGED TIME STEP
!                  *T* IS TEMPERATURE AT LAST CONVERGED TIME STEP
!                  *AI* IS FOR HEAT EXCHANGE WITH CONFINING LAYERS
!                  *AHT* ARE HEAT TRANSFER AREAS
!
!
!     PRIMARY VARIABLES:
!                  *X* LATEST CONVERGED VALUES OF PRIMARY VARIABLES
!                  *DX* LATEST INCREMENTS OF PRIMARY VARIABLES
!                  *DELX* SMALL INCREMENTS OF PRIMARY VARIABLES
!                         FOR NUMERICALLY COMPUTING DERIVATIVES
!                  *R* LATEST RESIDUALS
!                  *DOLD* ACCUMULATION TERMS AT BEGINNING OF TIME STEP
!
!
!     CONNECTIONS (INTERFACES):
!                  *NEX1* INDICES OF FIRST ELEMENTS IN A CONNECTION
!                  *NEX2* INDICES OF SECOND ELEMENTS IN A CONNECTION
!                  *DEL1* FIRST ELEMENT NODAL DISTANCES
!                  *DEL2* SECOND ELEMENT NODAL DISTANCES
!                  *AREA* INTERFACE AREAS
!                  *BETA* COSINE OF ANGLE BETWEEN NODAL LINE AND
!                         THE VERTICAL
!                  *ISOX* ISOTROPY INDICES
!                  *GLO* HEAT FLOW RATES
!                  *ELEM1* CODE NAMES OF FIRST ELEMENT IN A CONNECTION
!                  *ELEM2* CODE NAMES OF SECOND ELEMENT
!
!
!     LINEAR EQUATION SOLVER:
!                  *IRN* ROW INDICES OF NON-ZERO MATRIX ELEMENTS
!                  *ICN* COLUMN INDICES OF NON-ZERO MATRIX ELEMENTS
!                  *WKAREA* WORKSPACE USED IN MA28
!                  *JVECT* COLUMN INDICES OF NON-ZERO MATRIX ELEMENTS
!
!
!     SINKS AND SOURCES:
!                  *F1* TABLE OF GENERATION TIMES
!                  *F2* TABLE OF GENERATION RATES
!                  *F3* TABLE OF INJECTION ENTHALPIES
!
!
!     OTHER LARGE ARRAYS:
!                  *FLO* RATES OF GAS AND LIQUID PHASE FLOWS ACROSS
!                        ALL INTERFACES
!                  *VEL* PORE VELOCITIES OF GAS AND LIQUID PHASE FLOW
!
!
!=======================================================================



SUBROUTINE MPI_Template
		
	USE MPI_PARAM
	USE Basic_Param
	USE Element_arrays
	USE Connection_arrays
	USE Variable_Arrays
	USE MPI_ARRAYS
	USE EOS_Parameters
	USE MPI_SUBROUTINES
	
	IMPLICIT NONE
	
	INTEGER :: I,J,K,L
	
	INTEGER :: ierr
	
	!Allocate objects arrays containing pointer arrays
	allocate(E_array(0:MPI_NP-1))
	
	allocate(NUM_SEND(0:MPI_NP-1))
	allocate(NUM_RECV(0:MPI_NP-1))
	
	NUM_SEND=0
	NUM_RECV=0
	
	Do i=1,NUL
		J=update_p(i)
		Num_recv(J)=Num_recv(J)+1
	end do
	Do i=NCONI+1,NCONT
		J = conx(i)%n1
		K = conx(i)%n2
		if(K>NELL) then  !J is to be sent
			L = update_p(K-NELL)
			Num_send(L) = Num_send(L)+1
		else			 !K is to be sent
			L = update_p(J-NELL)
			Num_send(L) = Num_send(L)+1
		end if
	end do
	
	!Allocate to the array to count elements to send
	allocate(EXT(maxval(Num_send),0:MPI_NP-1))
	allocate(update_recv(maxval(Num_recv),0:MPI_NP-1))

	Num_send=0
	!Insert in external set EXT elements to send.
	DO I=NCONI+1,NCONT
		J = conx(i)%n1
		K = conx(i)%n2
		if(K>NELL) then  !J is to be sent
			L = update_p(K-NELL)
			call insert_ext(EXT(:,L),Num_send(L),J)
		else			 !K is to be sent
			L = update_p(J-NELL)
			call insert_ext(EXT(:,L),Num_send(L),K)
		end if
	END DO
	
	!Allocate the pointer arrays
	Do i=0,MPI_NP-1
		allocate(E_array(i)%Elem_send(Num_send(I)))
		allocate(E_array(i)%Elem_recv(Num_recv(I)))
		allocate(E_array(i)%CELL_V_SEND(Num_send(I),0:NEQ))
		allocate(E_array(i)%CELL_V_RECV(Num_recv(I),0:NEQ))
		allocate(E_array(i)%REAL_SEND((MaxNum_MComp+1)*3*Num_send(I)))
		allocate(E_array(i)%REAL_RECV((MaxNum_MComp+1)*3*Num_recv(I)))
	end do	
	
	
	!Make MPI structures to send recieve array of objects like ELement and CELL_V
	call MPI_type_create
	
END SUBROUTINE MPI_Template

Subroutine MPI_TYPE_CREATE


	USE MPI_PARAM
	USE Basic_Param
	USE Element_arrays
	USE Variable_Arrays
	USE MPI_ARRAYS
	
	implicit none
	
	integer(kind=MPI_ADDRESS_KIND) :: ad1,ad2,disp(100)
	integer :: oldtype(100)
	integer :: oldnumblocks(100)
	integer :: i,j,k

	INTEGER :: ierr
		
	!Do for Element object
	oldtype(1:6)=(/MPI_INTEGER1,MPI_INTEGER1,MPI_REAL8,MPI_REAL8,&
					MPI_REAL8,MPI_REAL8/)
	oldnumblocks(1:6)=(/(1, i=1,6)/)				
	call MPI_GET_ADDRESS(elem(1),ad1,ierr)
	
	call MPI_GET_ADDRESS(elem(1)%StateIndex,ad2,ierr)
	disp(1)=ad2-ad1
	call MPI_GET_ADDRESS(elem(1)%StatePoint,ad2,ierr)
	disp(2)=ad2-ad1
	call MPI_GET_ADDRESS(elem(1)%vol,ad2,ierr)
	disp(3)=ad2-ad1
	call MPI_GET_ADDRESS(elem(1)%phi,ad2,ierr)
	disp(4)=ad2-ad1
	call MPI_GET_ADDRESS(elem(1)%P,ad2,ierr)
	disp(5)=ad2-ad1
	call MPI_GET_ADDRESS(elem(1)%T,ad2,ierr)
	disp(6)=ad2-ad1

	call MPI_TYPE_CREATE_STRUCT(6,oldnumblocks,disp,oldtype,elem_type,ierr)
	call MPI_TYPE_COMMIT(elem_type,ierr)
	
	
	!Do for CELL_V array
	forall(i=1:(neq+1)*9) oldtype(i) = MPI_REAL8
	forall(i=1:6) oldnumblocks(i) = 4
	oldnumblocks(7) = 16
	oldnumblocks(8) = 4
	oldnumblocks(9) = 1
	
	call MPI_GET_ADDRESS(CELL_V(1,0),ad1,ierr)
	
	call MPI_GET_ADDRESS(CELL_V(1,0)%p_satr,ad2,ierr)
	disp(1)=ad2-ad1	
	call MPI_GET_ADDRESS(CELL_V(1,0)%p_KRel,ad2,ierr)
	disp(2)=ad2-ad1	
	call MPI_GET_ADDRESS(CELL_V(1,0)%p_Visc,ad2,ierr)
	disp(3)=ad2-ad1	
	call MPI_GET_ADDRESS(CELL_V(1,0)%p_Dens,ad2,ierr)
	disp(4)=ad2-ad1	
	call MPI_GET_ADDRESS(CELL_V(1,0)%p_Enth,ad2,ierr)
	disp(5)=ad2-ad1	
	call MPI_GET_ADDRESS(CELL_V(1,0)%p_PCap,ad2,ierr)
	disp(6)=ad2-ad1	
	call MPI_GET_ADDRESS(CELL_V(1,0)%p_MasF,ad2,ierr)
	disp(7)=ad2-ad1	
	call MPI_GET_ADDRESS(CELL_V(1,0)%p_AddD,ad2,ierr)
	disp(8)=ad2-ad1	
	call MPI_GET_ADDRESS(CELL_V(1,0)%TemC,ad2,ierr)
	disp(9)=ad2-ad1	
	
	! do i=1,neq
	! !For next column
		! call MPI_GET_ADDRESS(CELL_V(1,i),ad2,ierr)
		! do j=1,9
			! disp(i*9+j) = ad2 - ad1 + disp(j)
			! oldnumblocks(i*9+j) = oldnumblocks(j)
		! end do
	! end do
	
	call MPI_TYPE_CREATE_STRUCT(9,oldnumblocks,disp,&
					oldtype,cell_v_type,ierr)
	call MPI_TYPE_COMMIT(cell_v_type,ierr)
	
END SUBROUTINE MPI_TYPE_CREATE



SUBROUTINE MPI_COMM_ELEM
!Subroutine to communicate arrays of ELEM and CELL_V belonging to update set

	USE MPI_PARAM
	USE Basic_Param
	USE MPI_ARRAYS
	USE Element_arrays
	USE Variable_Arrays
	
	IMPLICIT NONE

	INTEGER :: i,j,l,k
	INTEGER :: XLOC, SSIZE, loc
	INTEGER,allocatable,dimension(:) :: STATUSS(:,:),request

	INTEGER :: ierr
!

	allocate(STATUSS(MPI_STATUS_SIZE,8*MPI_NP))
	allocate(request(8*MPI_NP))
	
!***********************************************************************
!*                                                                     *
!*             Copy element and cell_v into arrays and send            *
!*                      Also post recieve request                      *
!***********************************************************************
	l=1
	!Process loop
	Do i=0,MPI_NP-1
		if(Num_send(i)/=0)  THEN
			DO j=1,Num_send(i)
				E_array(i)%ELEM_SEND(j) = elem(ext(j,i))
				E_array(i)%CELL_V_SEND(j,:) = CELL_V(ext(j,i),:)
				
				!For X, DX and DELX
				XLOC = (ext(j,i)-1)*NK1 
				SSIZE = 3*NK1
				E_array(i)%REAL_SEND((j-1)*SSIZE+1:j*SSIZE) &
					& = (/ X(XLOC+1:XLOC+NK1) ,DX(XLOC+1:XLOC+NK1) ,DELX(XLOC+1:XLOC+NK1)   /)
			END DO
			!Send element array
			call MPI_ISEND(E_array(i)%ELEM_SEND,Num_send(i),&
				elem_type,i,0,MPI_COMM_WORLD,request(l), ierr)
			if(ierr/=0) THEN
				print *, "MPI SEND ERROR AT 9992A"
				stop
			end if
			l=l+1
			!Send CELL_V array
			call MPI_ISEND(E_array(i)%CELL_V_SEND,Num_send(i)*(NEQ+1),&
				cell_v_type,i,1,MPI_COMM_WORLD,request(l), ierr)
			if(ierr/=0) THEN
				print *, "MPI SEND ERROR AT 9993A"
				stop
			end if
			l=l+1
			
			!Send REAL array
			call MPI_ISEND(E_array(i)%REAL_SEND,Num_send(i)*3*NK1,&
				MPI_REAL8,i,2,MPI_COMM_WORLD,request(l), ierr)
			if(ierr/=0) THEN
				print *, "MPI SEND ERROR AT 9993A"
				stop
			end if
			l=l+1
			
			!Send ext array containing local position of elements which are sent
			call MPI_ISEND(ext(:,i),Num_send(i),&
				MPI_int,i,3,MPI_COMM_WORLD,request(l), ierr)
			if(ierr/=0) THEN
				print *, "MPI SEND ERROR AT 9993A"
				stop
			end if
			l=l+1
		END IF
		
		IF(Num_recv(i)/=0) THEN
			!Receive element array
			call MPI_IRECV(E_array(i)%ELEM_RECV,Num_recv(i),&
						elem_type,i,0,MPI_COMM_WORLD,request(l), ierr )
			if(ierr/=0) THEN
				print *, "MPI RECEIVE ERROR AT 9992B"
				stop
			end if
			 l=l+1
			 
			!Receive CELL_V array
			call MPI_IRECV(E_array(i)%CELL_V_RECV,Num_recv(i)*(NEQ+1),&
						cell_v_type,i,1,MPI_COMM_WORLD,request(l), ierr )
			if(ierr/=0) THEN
				print *, "MPI RECEIVE ERROR AT 9993B"
				stop
			end if
			 l=l+1
			 
			!Receive REAL array
			call MPI_IRECV(E_array(i)%REAL_RECV,Num_recv(i)*3*NK1,&
						MPI_REAL8,i,2,MPI_COMM_WORLD,request(l), ierr )
			if(ierr/=0) THEN
				print *, "MPI RECEIVE ERROR AT 9993B"
				stop
			end if
			 l=l+1
			 
			!Receive update_recv array containing local numbers of the recieved element
			call MPI_IRECV(update_recv(1:Num_recv(i),i),Num_recv(i),&
						MPI_INT,i,3,MPI_COMM_WORLD,request(l), ierr )
			if(ierr/=0) THEN
				print *, "MPI RECEIVE ERROR AT 9993B"
				stop
			end if
			 l=l+1
		 END IF
	END DO
	ierr = 0
	if(l>1) call MPI_Waitall(l-1,request,STATUSS,ierr)
				
	if(ierr/=0) THEN
		print *, "MPI WAIT ERROR AT 9994"
		stop
	end if
	
!***********************************************************************
!*                                                                     *
!*             Assign to the update set newly recieved values          *
!***********************************************************************
	
	Do i=0,MPI_NP-1
		IF(Num_recv(i)==0) CYCLE
		
		do k=1,Num_recv(i)
			j = update_recv(k,i)
			do l=1,NUL
				if(update_p(l)==i .and. update_l(l)==j) exit
			end do
			if(l>NUL) then		!We have encountered an error
				print *, "Received element",j,"from process",i," not found in update set of process",&
						MPI_rank,":ERROR: ABORTING!!!!!"
				stop
			end if
			
			
			!Do the copying
			!For ELEMENT
			elem(NELL+l)%StateIndex = E_array(i)%ELEM_RECV(k)%StateIndex
			elem(NELL+l)%StatePoint = E_array(i)%ELEM_RECV(k)%StatePoint
			elem(NELL+l)%vol = E_array(i)%ELEM_RECV(k)%vol
			elem(NELL+l)%phi = E_array(i)%ELEM_RECV(k)%phi
			elem(NELL+l)%P = E_array(i)%ELEM_RECV(k)%P
			elem(NELL+l)%T = E_array(i)%ELEM_RECV(k)%T
			
			!For SecParam CELL_V
			CELL_V(NELL+l,:) = E_array(i)%CELL_V_RECV(k,:)
			!For X, DX and DELX
			XLOC = (NELL+l-1)*NK1
			SSIZE = 3*NK1
			X(XLOC+1:XLOC+NK1) = E_array(i)%REAL_RECV( (k-1)*SSIZE+1 : (k-1)*SSIZE+NK1)
			DX(XLOC+1:XLOC+NK1) = E_array(i)%REAL_RECV( (k-1)*SSIZE+NK1+1 : (k-1)*SSIZE+2*NK1)
			DELX(XLOC+1:XLOC+NK1) = E_array(i)%REAL_RECV( (k-1)*SSIZE+2*NK1+1 : (k-1)*SSIZE+3*NK1)
		end do
	END DO
END SUBROUTINE MPI_COMM_ELEM

	 