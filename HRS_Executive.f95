!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                                                                     >
!>        TFx_Executive.f95: Executive code unit including the         >
!>          executive routines common to all TFx simulations           >
!>                                                                     >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!

      SUBROUTINE SIMULATION_Cycle
! 
! ... Modules to be used 
! 
      USE Basic_Param
      USE GenControl_Param
! 
      USE Element_Arrays
      USE Variable_Arrays
	  USE MPI_PARAM
	  USE MPI_ARRAYS
	  USE MPI_SUBROUTINES
	  !
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*          EXECUTIVE ROUTINE THAT PERFORMS THE SIMULATION             * 
!*                     WHILE ADVANCING IN TIME                         *
!*                                                                     *
!*                  Version 1.0 - January 9, 2004                      *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: PNIT,TCUR,Dt_solv,T_dd
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: n,NSTL,KIT,IGOOD_SEND
      INTEGER :: n_t,n_t0,i_n,NIT,NIT1,ierr
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: ConvFail_Flag,DtCut_Flag,PrintNow_Flag
      integer :: i ! added by rps
! 
      SAVE ICALL

	
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of SIMULATION_Cycle
!
!
      ICALL = ICALL+1
      IF(ICALL == 1 .AND. MPI_RANK==0) WRITE(11,6000)
! 
! 
!***********************************************************************
!*                                                                     *
!*                           INITIALIZATIONS                           *
!*                                                                     *
!***********************************************************************
! 
! 
	if(MPI_RANK==0) print *, "************ ENTERED SIMULATION_CYCLE ***********"
      KC    = 0
      IGOOD = 0
! 
      SUMTIM = TIMIN
      TIMOUT = SUMTIM
      DELT   = DELTEN
! 
      NSTL = (NST-1)*NK1
! 
      ITER = 0
      IHLV = 0
      KIT  = 0
! 
      Converge_Flag = .FALSE.    ! Flag denoting convergence of the Newton-Raphson ...
                                 ! ... iteration - Simulation continuing 
! 
      ConvFail_Flag = .FALSE.    ! Flag denoting convergence failure ...
                                 ! ... leading to abrupt simulation end
! 
! ----------
! ... Initialize thermophysical properties        
! ----------
! 
!write(*,*) delx
!write(*,*) elem(2)%P,elem(2)%T
!write(*,*) x(4),x(5),x(6),x(7),x(8)
!write(*,*) cell_v(2,0)%p_addD(1),cell_v(2,0)%p_addD(2)
!write(*,*) cell_v(2,0)%p_satr(1),cell_v(2,1)%p_satr(1)
!write(*,*) cell_v(2,2)%p_satr(1),cell_v(2,3)%p_satr(1),cell_V(2,4)%p_satr(1)
      CALL EOS          ! Calling the Equation-of-State routine 
	 
	  if(MPI_RANK==0) Print *, "****************CALL TO EOS ENDED******************"
      !do i = 1,nel
       !  elem(i)%p = 6.0e6
      !end do
!write(*,*) elem(2)%P,elem(2)%T
!write(*,*) x(4),x(5),x(6),x(7),x(8)
!write(*,*) cell_v(2,0)%p_addD(1),cell_v(2,0)%p_addD(2)
!write(*,*) cell_v(2,0)%p_satr(1),cell_v(2,1)%p_satr(1)
!write(*,*) cell_v(2,2)%p_satr(1),cell_v(2,3)%p_satr(1),cell_V(2,4)%p_satr(1)
! 
!write(*,*) delx
      CALL VME_Balance  ! Compute mass and heat balance
	  if(MPI_RANK==0) Print *, "****************CALL TO VME_Balance******************"
!write(*,*) delx
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                       MAIN TIME STEP LOOP                           >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
	OPEN(99,FILE='Time_Evolution') !BY RPS 27/03/2016
!
      n_t0 = KCYC+1     ! Beginning value of the cumulative number ...
!                       ! ... of time steps (including restarts)
!
      DO_TimeSteps: DO n_t = n_t0,MCYC
!
         KCYC = n_t     ! Update KCYC
! 
         KC   = KC+1    ! Update the number of time steps (new time step)
!
         IF(MOP(2) /= 0 .and. MPI_RANK==0) THEN
            PRINT 6001, KC,KCYC,DELT,SUMTIM  ! Print explanatory messsage
         END IF
! 
         IHALVE = 0   ! Initializing the time reduction counter 
! 
! 
!***********************************************************************
!*                                                                     *
!*                 INITIALIZATIONS FOR NEW TIME STEP                   *
!*                                                                     *
!***********************************************************************
! 
! 
!write(*,*) delx
 2000    ITER   = 0                ! The counter of the Newtonian iterations ...
!                                  ! ... in this time step
!  
         PrintOut_Flag = .FALSE.   ! Flag indicating print-out at a requested time
! 
         PrintNow_Flag = .FALSE.   ! Flag indicating immediate print-out 
! 
! -------------
! ...... Set the time-step (Dt) size & adjust related variables
! -------------
! 
         IF(ITI /= 0) CALL Adjust_DtPrint  ! Adjust Dt to coincide with desired print-out times
! 
         DELTEX = DELT                     ! Set Dt for the ensueing computations
! 
         Converge_Flag = .FALSE.           ! Set the convergence flag
! 
! 
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*                   THE NEWTONIAN ITERATION LOOP                      *
!*                                                                     *
!***********************************************************************
!***********************************************************************
! 
! 
         DO_NewtIt: DO i_n = 1,NOITE+1
! 
! ----------------
! ......... Initialize changes in the primary variables for this iteration
! ----------------
! 
            IF(ITER == 0) DX = 0.0d0  ! CAREFUL! Whole array operation
! 
! ----------------
! ......... Update counters
! ----------------
! 
            ITER  = ITER+1    ! Update the Newtonian iteration counter 
!                             !    in this time step
            ITERC = ITERC+1   ! Update the cumulative Newtonian iteration 
                              !    counter since t = 0
            FORD = FOR*DELTEX ! Compute the factor used in the 
                              !    accumulation terms
! 
! 
!***********************************************************************
!*                                                                     *
!*        Compute the accumulation, source/sink and flow terms         *
!*                    SET UP THE JACOBIAN EQUATIONS                    *
!*                                                                     *
!***********************************************************************
! 
!
  ! write(*,*) dx(1),dx(2),dx(3),dx(4),dx(5),dx(6),dx(7) 
!write(*,*) delx
			
	!  Print *, "****************CALL TO JACOBIAN_SetUp******************"
	   CALL CPU_ElapsedTime(0,Dt_solv,T_dd)
            CALL JACOBIAN_SetUp
		 CALL CPU_ElapsedTime(1,Dt_solv,T_dd)   ! Determine execution time
            T_jac_setup = T_jac_setup + Dt_solv
!write(*,*) delx
!		
			IGOOD_SEND = IGOOD
			CALL MPI_ALLREDUCE(IGOOD_SEND,IGOOD,1,MPI_INT,MPI_MAX,MPI_COMM_WORLD,IERR)
            IF(IGOOD  == 3) THEN
!
               CALL Dt_CutBack(KIT,DtCut_Flag,ConvFail_Flag)  ! Cut Dt if necessary
               IF(DtCut_Flag .EQV. .TRUE.) GO TO 2000         ! If so, restart the ... 
                                                              ! ... Newtonian iteration loop
            END IF
! 
! 
!***********************************************************************
!*                                                                     *
!*                        UPON CONVERGENCE ...                         *
!*                                                                     *
!***********************************************************************
! 
! 
            IF_Convrg: IF(RERM <= RE1) THEN

		if(MPI_RANK==0) WRITE(99,*) KC, x(3),x(9)
! 
               CALL NI_Converged(NSTL,KIT,ConvFail_Flag,PrintNow_Flag)
! 

               IF(PrintNow_Flag .EQV. .TRUE.) THEN  ! If it the print-out flag is set, ...
                  CALL PRINT_Output                 ! ...  produce a print-out and ...
                  CALL VME_Balance                  ! ...  compute mass and heat balances
				  
               END IF
! 
               EXIT               ! Continue timestepping
! 
            END IF IF_Convrg
! 
! 
!***********************************************************************
!*                                                                     *
!*               IF CONVERGENCE IS NOT YET ATTAINED ...                *
!*                                                                     *
!***********************************************************************
! 
! 
            IF(MOP(1) /= 0) THEN
! 
! ............ Print basic information for MOP(1) /= 0
! 
            if(NER>0)  PRINT 6006, KCYC,ITER,DELTEX,RERM,elem(NER)%name,KER
            END IF
! 
! ----------------
! ......... Print extra information for MOP(1) > 2
! ----------------
! 
            IF_Mop1: IF(MOP(1) >= 2) THEN
! 
               IF(NST >0) THEN
                  NIT = NST
               ELSE IF (NST/=-1) THEN
                  NIT = NER
               END IF
! 
			   IF(NIT>0) THEN
				   NIT1 = (NIT-1)*NK1
				   PNIT = X(NIT1+1)+DX(NIT1+1)
	! 
				   PRINT 6008, elem(nit)%name,DX(NIT1+1),DX(NIT1+2),&
		 &                     Cell_V(nit,0)%TemC,PNIT,&
		 &                     Cell_V(nit,0)%p_Satr(1) 
! 				
			   END IF
            END IF IF_Mop1
! 
! ----------------
! ......... Check if the maximum number of Newtonian iterations has been reached
! ----------------
! 
            IF(ITER > NOITE) THEN
!
               CALL Dt_CutBack(KIT,DtCut_Flag,ConvFail_Flag)  ! Cut Dt if necessary
               IF(DtCut_Flag .EQV. .TRUE.) GO TO 2000         ! If so, restart the ...
                                                              ! ... Newtonian iteration loop
            END IF
! 
! 
!***********************************************************************
!*                                                                     *
!*                         SOLVE THE JACOBIAN                          *
!*                                                                     *
!***********************************************************************
! 
! 
            CALL CPU_ElapsedTime(0,Dt_solv,T_dd)   ! Set timer 
!
            CALL SOLVE_JacobianME
  ! write(*,*) dx(1),dx(2),dx(3),dx(4),dx(5),dx(6),dx(7) 
!write(*,*)cell_v(1,1)%p_pcap
!
            CALL CPU_ElapsedTime(1,Dt_solv,T_dd)   ! Determine execution time 
            T_mat_solv = T_mat_solv + Dt_solv
!
			IGOOD_SEND = IGOOD
			CALL MPI_ALLREDUCE(IGOOD_SEND,IGOOD,1,MPI_INT,MPI_MAX,MPI_COMM_WORLD,IERR)
            IF(IGOOD  /= 0) THEN
!
               CALL Dt_CutBack(KIT,DtCut_Flag,ConvFail_Flag)  ! Cut Dt if necessary
               IF(DtCut_Flag .EQV. .TRUE.) GO TO 2000         ! If so, restart the ... 
                                                              ! ... Newtonian iteration loop
            END IF
! 
! ----------------
! ......... Update the thermophysical properties
! ----------------
!
	!write(*,*) elem(2)%P,elem(2)%T
!write(*,*) cell_v(2,0)%p_addD(1),cell_v(2,0)%p_addD(2)
!	write(*,*) x(4),x(5),x(6),x(7),x(8) 
            CALL EOS
!	write(*,*) elem(2)%P,elem(2)%T
!write(*,*) cell_v(2,0)%p_addD(1),cell_v(2,0)%p_addD(2)
!	write(*,*) x(4),x(5),x(6),x(7),x(8) 
! 
! ----------------
! ......... If outside the range, reduce time step
! ----------------
! 
			IGOOD_SEND = IGOOD
			CALL MPI_ALLREDUCE(IGOOD_SEND,IGOOD,1,MPI_INT,MPI_MAX,MPI_COMM_WORLD,IERR)
            IF(IGOOD /= 0) THEN
!
               CALL Dt_CutBack(KIT,DtCut_Flag,ConvFail_Flag)  ! Cut Dt if necessary
               IF(DtCut_Flag .EQV. .TRUE.) GO TO 2000         ! If so, restart the ... 
                                                              ! ... Newtonian iteration loop
            ELSE
! 
! ----------------
! ......... If within the range, check if print-out is needed
! ----------------
! 
               IF(KDATA >= 10) THEN
                  CALL PRINT_Output
               END IF
! 
            END IF
!
! <<<<<<                          
! <<<<<< End of the NEWTONIAN ITERATION LOOP         
! <<<<<<    
!
         END DO DO_NewtIt
! 
! 
!***********************************************************************
!*                                                                     *
!*          CHECK IF ANY OF THE VARIOUS TERMINATION CRITERIA           *
!*                         HAVE BEEN FULFILLED                         *
!*                                                                     *
!***********************************************************************
! 
! 
         CALL CPU_TimeF(Temp_REAL(1))     ! Check the CPU time up to this point 
	    	CALL MPI_BROADCAST(REAL_ARRAY=Temp_REAL,REAL_SIZE=1,ROOT=0)
			TCUR = Temp_REAL(1)
! 
         IF(  (ConvFail_Flag .EQV. .TRUE.) .OR.  &         ! The termination flag is invoked
     &        (TIMAX /= 0.0d0 .AND. SUMTIM == TIMAX) .OR.& ! The simulation time is exceeded
     &        (KC >= MCYC) .OR.                     &      ! The number of time steps is exceeded
     &        (MSEC /= 0.0d0 .AND. (TCUR-Tzero) > MSEC) &  ! The alloted CPU time is exceeded
     &     ) THEN 
! 
            CALL WRITE_FileSAVE                ! Write results into file SAVE
! 
            IF(MOP(15) /= 0) THEN              ! When the semi-analytical heat exchange solution is used
               REWIND 8                        !    Open the file TABLE  
               WRITE(6,6002)                   !    Print a heading
			   
			   do i=1,NELA
				if(MPI_RANK==procnum(i)) then
					n=locnum(i)
					WRITE(8,6005) AI(n)  ! Write the heat exchange coefficients into TABLE
					call MPI_BARRIER(MPI_COMM_WORLD,IERR)
				else
					call MPI_BARRIER(MPI_COMM_WORLD,IERR)
				end if
			   end do
            END IF
! 
            EXIT                               ! Done!  Exit the subroutine                
! 
         END IF
!
! <<<                      
! <<< End of the TIME STEP LOOP         
! <<<
!
      END DO DO_TimeSteps
   close(99)
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'SIMULATION_Cycle  1.0   18 December  2003',6X,&
     &         'Executive routine that performs the simulation while ',&
     &         'marching in time')
 6001 FORMAT(/,' CYCIT ..........  KC = ',I4,'  KCYC =',I4,&
     &         '  DELT = ',1pE13.6,'  SUMTIM = ',1pE13.6,/)
 6002 FORMAT(' Write coefficients for semi-analytical heat loss', &
     &       ' calculation onto file "TABLE"')
!
 6005 FORMAT(4E20.13)
 6006 FORMAT(' ...ITERATING...  AT [',I5,',',I2,&
     &       ']: DELTEX = ',1pE13.6,&
     &       '   MAX{Residual} = ',1pE13.6,'  at element "',A8,&
     &       '", equation ',I2)
 6008 FORMAT(31X,'     AT "',A8,'" ...   DX1= ',1pE12.5,&
     &           ' DX2= ',1pE12.5,' T = ',F7.3,' P = ',F9.0,&
     &           ' S = ',1pE12.5)
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of SIMULATION_Cycle
!
!
      RETURN
! 
      END SUBROUTINE SIMULATION_Cycle
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE Dt_CutBack(k_it,DtCut_Flag,ConvFail_Flag)
! 
! ... Modules to be used 
! 
      USE Basic_Param
      USE GenControl_Param
! 
      USE Element_Arrays
	  USE MPI_Param
	  USE MPI_ARRAYS
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*               ROUTINE FOR REDUCING THE TIME-STEP SIZE               *
!*                                                                     *
!*                  Version 1.00, January 11, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: k_it
! 
      INTEGER :: ICALL  = 0
! 
      INTEGER :: n,i,ierr
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL, INTENT(OUT) :: ConvFail_Flag,DtCut_Flag
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of Dt_CutBack
!
!
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)  
! 
! -------
! ... Reduce time-step size  
! -------
! 
      DELT   = DELTEX/REDLT
!
      IGOOD  = 0               ! Reset the IGOOD flag
!
      DtCut_Flag = .FALSE.     ! Initialize the flag for transfer of control on exit
!
!***********************************************************************
!*                                                                     *
!*  Check on number of previous time steps that converged on ITER = 1  *
!*                                                                     *
!***********************************************************************
!
      IF(k_it > 2 .AND. MOP(16) /= 0) THEN
         if(MPI_RANK==0) PRINT 6001, KCYC,DELTEX          ! Print explanatory comment
         ConvFail_Flag = .TRUE.           ! Set flag to stop execution after following Dt
      END IF
! 
      if(MPI_RANK==0) PRINT 6002, KCYC,ITER,DELT          ! Print explanatory comment 
! 
! -------
! ... Determine the number of Dt cutbacks  
! -------
! 
      IHALVE = IHALVE+1
!
!***********************************************************************
!*                                                                     *
!*             STOP AFTER 25 ATTEMPTS TO REDUCE TIME STEP              *
!*                                                                     *
!***********************************************************************
!
      IF_IhafMax: IF(IHALVE > 25) THEN
! 
         if(MPI_RANK==0) PRINT 6003, DELTEX     ! Print warning message
         CALL PRINT_Output      ! Write output before stopping
         CALL WRITE_FileSAVE    ! Write the SAVE file before stopping
! 
! ----------
! ...... For semianalytical estimation of heat loss to bounding layers 
! ----------
! 
         IF(MOP(15) == 0) THEN  
! 
            REWIND 8                        ! Open file TABLE
            if(MPI_RANK==0) WRITE(6,6004)                   ! Print a warning message
			
			 do i=1,NELA
				if(MPI_RANK==procnum(i)) then
					n=locnum(i)
					WRITE(8,6005) AI(n)  ! Write the heat exchange coefficients into TABLE
					call MPI_BARRIER(MPI_COMM_WORLD,IERR)
				else
					call MPI_BARRIER(MPI_COMM_WORLD,IERR)
				end if
		     end do
!                                           ! ... exchange parameters into TABLE
         END IF                      
! 
         STOP                               ! Stop the simulation
! 
      ELSE
! 
         ITER = 0              ! Reset the counter of Newtonian iterations for new Dt
         IHLV = 1              ! Set the cutback counter
!
         CALL EOS              ! Re-estimate thermophysical properties
!
         IHLV = 0              ! Reset the cutback counter
!
         DtCut_Flag = .TRUE.   ! Set the flag for a new NI iteration ...
!                              ! ... with a shorter Dt on exit
      END IF IF_IhafMax
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'Dt_CutBack        1.0   18 December  2003',6X,&
     &         'Reduce time-step size') 
!
 6001 FORMAT(/,' >>>>>>>>  CONVERGENCE FAILURE on time step #',I5,&
     &         ' with Dt = ',1pE13.6,' seconds, following two ',&
     &         '  steps that converged on ITER = 1',/,&
     &         '      ==>  STOP EXECUTION AFTER NEXT TIME STEP')
 6002 FORMAT(' >>>>>  REDUCE TIME STEP AT (',I5,',',I2,&
     &       ') ==>  NEW DELT = ',1pE13.6)
!
 6003 FORMAT(//,' NO CONVERGENCE AFTER 25 REDUCTIONS OF', &
     &          ' TIME STEP SIZE',/,&
     &          ' LAST TIME STEP WAS ',1pE13.6,' SECONDS ',//,&
     &          ' !!!!!!!!!!  S T O P   E X E C U T I O N  !!!!!!!!!!')
 6004 FORMAT(' Write coefficients for semi-analytical heat loss', &
     &       ' calculation onto file \D2TABLE\D3')
!
 6005 FORMAT(4E20.13)
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>    End of Dt_CutBack
!
!
      RETURN
! 
      END SUBROUTINE Dt_CutBack
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
      SUBROUTINE NI_Converged(NSTL,KIT,ConvFail_Flag,PrintNow_Flag)
! 
! ... Modules to be used 
! 
      USE Basic_Param
      USE GenControl_Param
      USE TimeSeriesP_Param
! 
      USE Hydrate_Param
! 
      USE Element_Arrays
      USE Connection_Arrays
      USE Variable_Arrays
      USE PFMedProp_Arrays
      USE SolMatrix_Arrays
	  USE MPI_PARAM
	  USE MPI_SUBROUTINES
	  USE MPI_ARRAYS
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*        ROUTINE FOR UPDATING PRIMARY VARIABLES AND SELECTING         *
!*          AFTER SUCCESSFUL CONVERGENCE OF THE NI ITERATIONS          *
!*                                                                     *
!*                 Version 1.00 - September 29, 2004                   *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: PHIN,DPHI,T_n,P_n
      REAL(KIND = 8) :: CumCH4_RelRate,CumCH4_MassD,CumCH4_MassL
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN)  :: NSTL
      INTEGER, INTENT(OUT) :: KIT
! 
      INTEGER :: ICALL  = 0, IERR
! 
      INTEGER :: n,NLOC,NMAT
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL, INTENT(INOUT) :: ConvFail_Flag
      LOGICAL, INTENT(OUT)   :: PrintNow_Flag
! 
      SAVE ICALL,CumCH4_MassL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of NI_Converged
!
!
      ICALL = ICALL+1                                                     
      IF(ICALL == 1) THEN
         if(MPI_RANK==0) WRITE(11,6000)
         CumCH4_MassL = 0.0d0
      END IF
! 
! -------
! ... Initializations
! -------
! 
      CumCH4_RelRate = 0.0d0
      CumCH4_MassD   = 0.0d0
! 
! -------
! ... Reset the convergence flag to indicate convergence 
! -------
! 
      Converge_Flag = .TRUE.           
! 
! -------
! ... Compute semianalytical heat losses through bounding layers  
! -------
! 
      IF(MOP(15) >= 1) CALL HeatExch_An
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                       ACTIVE ELEMENT LOOP                           >
!>       Compute Changes In Properties, Parameters And Variables       >
!>                        After Convergence                            >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_NumEleAct: DO n = 1,NELAL
! 
! ...... Identify locations in the parameter arrays 
! 
         NLOC  = (n-1)*NK1
! 
! -------------
! ...... Compute changes in porosity      
! -------------
! 
         PHIN = elem(n)%phi
         nmat = elem(n)%matNo
         T_n  = Cell_V(n,0)%TemC
! 
! -------------
! ...... Update element pressures and pressure-related parameters   
! -------------
! 
         IF( (F_Kinetic .EQV. .FALSE.) .AND. & 
     &       (elem(n)%StateIndex == 7_1  .OR. &  
     &        elem(n)%StateIndex == 10_1 .OR. &
     &        elem(n)%StateIndex == 11_1) )    &
     &   THEN
!
! ......... Update porosity
!
            P_n         = Cell_V(n,0)%p_AddD(2)
            DPHI        = PHIN*( media(nmat)%Compr*(P_n-elem(n)%P) &
     &                          +media(nmat)%Expan*(T_n-elem(n)%T))
            elem(n)%phi = PHIN+DPHI      
!
! ......... Update pressure
!
            elem(n)%P = P_n
!
         ELSE
!
! ......... Update porosity
!
            P_n         = X(NLOC+1)+DX(NLOC+1)-elem(n)%P
            DPHI        = PHIN*( media(nmat)%Compr*P_n &
     &                          +media(nmat)%Expan*(T_n-elem(n)%T))
            elem(n)%phi = PHIN+DPHI                            
!
! ......... Update pressure
!
            elem(n)%P = X(NLOC+1)+DX(NLOC+1)
!
         END IF
!
! <<<                      
! <<< End of the ELEMENT LOOP         
! <<<
!
      END DO DO_NumEleAct
! 
! ----------
! ... Update element temperatures: CAREFUL! Array operations!      
! ----------
! 
      elem(1:NELAL)%T = Cell_V(1:NELAL,0)%TemC 
! 
! ----------
! ... Increment primary variables: CAREFUL! Array operations!   
! ----------
! 
      X(1:NELAL*NEQ) = X(1:NELAL*NEQ)+DX(1:NELAL*NEQ)
! 
! ----------
! ... Compute total hydrate mass: CAREFUL! Array operations!     
! ----------
! 
      FORALL(n=1:NELAL) CO(n) = Cell_V(n,0)%p_Satr(3)&
     &                        *Cell_V(n,0)%p_Dens(3)
! 
      CumCH4_MassD = SUM(elem(1:NELAL)%vol*elem(1:NELAL)%phi*CO(1:NELAL))
	  
! 
! ----------
! ... Compute total hydrate reaction rate: CAREFUL! Array operations!      
! ----------
! 
      IF(F_Kinetic .EQV. .TRUE.) THEN
         FORALL(n=1:NELAL) CO(n) = elem(n)%vol*Cell_V(n,0)%p_AddD(2)
         CumCH4_RelRate = SUM(CO(1:NELAL))
      END IF  
!
!
	  TEMP_REAL(1:2) = (/CumCH4_MassD, CumCH4_RelRate /)
	  call MPI_ALLREDUCE((/CumCH4_MassD, CumCH4_RelRate /), TEMP_REAL,2,MPI_REAL8,&
						   MPI_SUM,MPI_COMM_WORLD,IERR)
	  CumCH4_MassD = TEMP_REAL(1)
	  CumCH4_RelRate = TEMP_REAL(2)
	  
!***********************************************************************
!*                                                                     *
!*             Store the state index in case of Dt cutback             *
!*                                                                     *
!***********************************************************************
! 
      elem%StatePoint = elem%StateIndex   ! CAREFUL! Whole array operation
!
! -------
! ... Udpate current time
! -------
!
      SUMTIM = SUMTIM+DELTEX    ! The new current time
! 
! 
!***********************************************************************
!*                                                                     *
!*        WRITE HYDRATE-RELATED DATA INTO FILE "Hydrate_Info"          *
!*                                                                     *
!***********************************************************************
! 
! 
      CumCH4_MassD = (Initial_HydMass-CumCH4_MassD)*HCom%GasMF(1)
!
      IF(F_Kinetic .EQV. .TRUE.) THEN   ! Kinetic conditions
! 
         CumCH4_RelRate =  -CumCH4_RelRate*HCom%GasMF(1)
! 
      ELSE                              ! Equilibrium conditions
! 
         CumCH4_RelRate = (CumCH4_MassD-CumCH4_MassL)/DELTEX
         CumCH4_MassL   =  CumCH4_MassD
! 
      END IF
!
     if(MPI_RANK==0)  WRITE(21,6200)  SUMTIM/8.64d4,   &          ! Time in days
     &                CumCH4_RelRate,   &         ! Cumulative CH4 mass release rate (kg/s)
     &                CumCH4_RelRate*1.47724d0,&  ! Cumulative CH4 volume release rate (ST m^3/s)
     &                CumCH4_MassD,    &          ! Cumulative CH4 released/reacted mass (kg)
     &                CumCH4_MassD*1.47724d0     ! Cumulative CH4 released/reacted volume (ST m^3)
! 
! 
!***********************************************************************
!*                                                                     *
!*             SET NEW TIMESTEP; UPDATE OTHER PARAMETERS               *
!*                                                                     *
!***********************************************************************
! 
! 
      IF(TIMAX /= 0.0d0 .AND. TIMAX == SUMTIM) THEN
         PrintOut_Flag = .TRUE.                      ! Set flag for printout at SUMTIM
      END IF
! 
! ----------
! ... For internally-determined time step sizes      
! ----------
! 
      IF((NDLT == 0) .OR. (KC+1 > 8*NDLT .OR. DLT(KC+1) == 0.0d0)) THEN
! 
         IF_DtDouble: IF(ITER <= MOP(16)) THEN  ! If convergence within MOP(16) iterations, ...
            DELT = 2.0d0*DELTEX                 !    double time step, ...
         ELSE                                   ! otherwise
            DELT = DELTEX                       !    use old time step 
         END IF IF_DtDouble
! 
! ----------
! ... For given (predetermined) time step sizes                   
! ----------
! 
      ELSE
         DELT = DLT(KC+1)                       ! Use specified time step
      END IF
! 
! ----------
! ... Adjust Dt to ensure conformance to the maximum simulation period TIMAX                  
! ----------
! 
      IF(TIMAX  > 0.0d0) DELT = MIN(DELT,TIMAX-SUMTIM)
! 
! ----------
! ... Adjust Dt to ensure conformance with max{Dt} = DELTMAX                
! ----------
! 
      IF(DELTMX > 0.0d0) DELT = MIN(DELT,DELTMX)
!
!***********************************************************************
!*                                                                     *
!*            Determine flags and counters for print-outs              *
!*                                                                     *
!***********************************************************************
!
      IF(ITER == 1) THEN    ! Count number of consecutive time steps 
         KIT = KIT+1        !    that converge on ITER = 1 
      ELSE 
         KIT = 0            ! For ITER > 1, do not consider
      END IF
! 
! -------
! ... Print messages if KIT >= 10  
! -------
! 
      IF(KIT >= 100) THEN  
         if(MPI_RANK==0) PRINT 6001
         ConvFail_Flag = .TRUE.   ! Set the flag indicating convergence failure
      END IF
!
!***********************************************************************
!*                                                                     *
!*             Print out important data upon convergence               *
!*                                                                     *
!***********************************************************************
!
      IF_Conver: IF(Converge_Flag .EQV. .TRUE.) THEN
! 
         IF_NST: IF(NST > 0) THEN
! 
            IF( (F_Kinetic .EQV. .FALSE.) .AND. & 
     &          (elem(nst)%StateIndex == 7_1  .OR.&   
     &           elem(nst)%StateIndex == 10_1 .OR. &
     &           elem(nst)%StateIndex == 11_1) ) &   
     &      THEN
               P_n  = Cell_V(nst,0)%p_AddD(2)
            ELSE
               P_n  = X(NSTL+1)
            END IF
! 
            PRINT 6002, elem(nst)%name,KCYC,ITER,SUMTIM,DELTEX,&
     &                  DX(NSTL+1),DX(NSTL+2),&
     &                  Cell_V(nst,0)%TemC,P_n,&
     &                  Cell_V(nst,0)%p_Satr(1)
! 
         ELSE IF (ner>0 .and. NST/=-1) THEN
! 
            IF( (F_Kinetic .EQV. .FALSE.) .AND.  &
     &          (elem(ner)%StateIndex == 7_1  .OR.   &
     &           elem(ner)%StateIndex == 10_1 .OR. &
     &           elem(ner)%StateIndex == 11_1) )     &
     &      THEN
               P_n  = Cell_V(ner,0)%p_AddD(2)
            ELSE
               P_n  = X((NER-1)*NK1+1)
            END IF
! 
            PRINT 6002, elem(NER)%name,KCYC,ITER,SUMTIM,DELTEX,&
     &                  DX((NER-1)*NK1+1),&
     &                  DX((NER-1)*NK1+2), &
     &                  Cell_V(ner,0)%TemC,P_n,&
     &                  Cell_V(ner,0)%p_Satr(1)
! 
         END IF IF_NST
!
!***********************************************************************
!*                                                                     *
!*            Print out time-series data upon convergence              *
!*                                                                     *
!***********************************************************************
!
         IF_TimeS: IF((obs_elem_Flag .EQV. .TRUE.) .OR. &
     &                (obs_conx_Flag .EQV. .TRUE.) .OR. &
     &                (obs_SS_Flag   .EQV. .TRUE.)) &
     &   THEN
             CALL TABLE_TimSerD
         END IF IF_TimeS
! 
      END IF IF_Conver
!
!***********************************************************************
!*                                                                     *
!*              Determine whether printout is required                 *
!*                                                                     *
!***********************************************************************
!
      IF((ConvFail_Flag .EQV. .TRUE.) .OR.&
     &   ((PrintOut_Flag .EQV. .TRUE.) .AND. &
     &    (Converge_Flag .EQV. .TRUE.)&
     &   ) .OR. &
     &   ((Converge_Flag .EQV. .TRUE.) .AND. MOD(KCYC,MCYPR) == 0&
     &   ) .OR.  &
     &   (KCYC >= MCYC) .OR.  &
     &   (KDATA>= 10)&
     &  ) PrintNow_Flag = .TRUE.
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'NI_Converged      1.0   29 September 2003',6X,&
     &         'Update primary variables and select timestep after ',&
     &         'convergence of the NI iterations is achieved') 
!
 6001 FORMAT(' !!!!!!!!!  FOR 10 CONSECUTIVE TIME STEPS HAVE',&
     &       ' CONVERGENCE ON ITER = 1',/,&
     &       '       ==>  WRITE OUT CURRENT DATA,',&
     &       ' THEN STOP EXECUTION')
!
 6002 FORMAT(1X,A8,'(',I5,',',I2,')',' ST = ',1pE12.5,' DT = ',1pE12.5,&
     &             ' DX1= ',1pE12.5,' DX2= ',1pE12.5,' T = ',1pE11.4,&
     &             ' P = ',1pE13.6,' S = ',1pE12.5)
!
 6200 FORMAT(8(1pe15.8,1x))
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of NI_Converged
!
!
      RETURN
! 
      END SUBROUTINE NI_Converged
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE Adjust_DtPrint
! 
! ... Modules to be used 
! 
      USE Basic_Param
      USE GenControl_Param
	  USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*        ROUTINE MODIFYING THE TIME-STEP SIZE TO CONFORM WITH         *
!*     USER-SUPPLIED INPUT TIMES AT WHICH PRINT-OUTS ARE DESIRED       *
!*                                                                     *
!*                   Version 1.0 - January 4, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: i
! 
      SAVE ICALL
!
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of Adjust_DtPrint
!
!
      ICALL = ICALL+1
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)
! 
! -------
! ... Check if the current time exceeds the maximum print-out time 
! -------
! 
      IF_On: IF(TIS(ITI) <= SUMTIM) THEN
                ITCO = ITI                 ! Reset ITCO
                RETURN                     ! Done ! Exit the routine
             END IF IF_On
! 
! 
!***********************************************************************
!*                                                                     *
!*                        PRINT-OUT TIME LOOP                          *
!*                                                                     *
!***********************************************************************
! 
! 
      DO_NPTimes: DO  i=1,ITI
! 
! ----------
! ...... If the desired print-out time is reached
! ----------
! 
         IF_ChkTIS: IF(TIS(i) == SUMTIM) THEN
! 
            ITCO = i                                  ! Reset the ITCO flag
            IF(DELAF > 0.0d0) DELT = MIN(DELT,DELAF)  ! Obtain an estimate for the new Dt
!
! ......... Check the Dt estimate against the next print-out time
!
            IF(TIS(I+1) > TIS(i)+DELT) THEN       ! If Dt < TIS(I+1)-TIS(I)
               RETURN                             !    Done ! Exit the routine
            ELSE                                  ! Otherwise
               DELT = MIN(DELT,TIS(i+1)-TIS(i))   !    Readjust Dt
            END IF
!
            PrintOut_Flag = .TRUE.                ! Reset the print-out flag                     
!
            RETURN                                ! Done ! Exit the routine
! 
! ----------
! ...... If the desired print-out time has already been passsed
! ----------
! 
         ELSE IF(TIS(i) < SUMTIM) THEN 
! 
            CYCLE                                 ! Continue the loop                       
! 
! ----------
! ...... If the desired print-out time has not been passsed
! ----------
! 
         ELSE 
!
            IF(SUMTIM+DELT >= TIS(i)) THEN   ! If Dt results in current t > TIS(I), ...
               DELT   = TIS(i)-SUMTIM        ! ... Dt is adjusted and
               PrintOut_Flag = .TRUE.        ! ... the print-out flag is reset
            END IF
!
            ITCO = i-1                       ! Reset the print-out flag
            RETURN                           ! Done ! Exit the routine
! 
         END IF IF_ChkTIS
!
! <<<                      
! <<< End of the PRINT-OUT TIME LOOP         
! <<<
!
      END DO DO_NPTimes
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'Adjust_DtPrint    1.0   14 November  2003',6X,&
     &         'Adjust time steps to conform with user-defined ',&
     &         'times for a print-out')
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Adjust_DtPrint
!
!
      RETURN
! 
      END SUBROUTINE Adjust_DtPrint
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE SOLVE_JacobianME
! 
! ...... Modules to be used 
! 
         USE Basic_Param
         USE GenControl_Param
         USE Solver_Param
! 
         USE Element_Arrays
         USE Variable_Arrays
         USE SolMatrix_Arrays
! 
         USE Matrix_Solvers_par
		 USE MPI_PARAM
		 USE MPI_ARRAYS
		 USE schur
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*     ROUTINE CALLING THE SOLVERS FOR THE JACOBIAN MATRIX EQUATION    *
!*                                                                     *
!*                   Version 1.0 - August 24, 2003                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: zertio,TS,TT,err,TSS
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0, iteruc = 0, iprpro =0, izero0 = 0, totiter = 0
! 
      INTEGER :: NI,NT,nn,k,NLOC,NLOCP,I,J
      INTEGER :: izerod,N_Eq,ierG,info,iteru,IERR,izerop
      INTEGER :: iddfup,iddfdn,matord,nsupdg,nsubdg,ntotd
! 
      SAVE ICALL,NI,NT,iteruc,iprpro,izero0
      SAVE matord,nsupdg,nsubdg,ntotd,totiter
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of SOLVE_JacobianME
!
!
      ICALL = ICALL+1
      IF(ICALL == 1) THEN
         IF(MPI_RANK==0) WRITE(11,6000)
         NI = NEQ*NELAL
		 NT = NEQ*(NELAL+NUL)
		!Also assigning two arrays to send to iterative solvers
		do i=1,NUL
			update_ps(NEQ*(i-1)+1:NEQ*i) = (/( update_p(i), j=1,NEQ) /)
			update_ls(NEQ*(i-1)+1:NEQ*i) = (/( (update_l(i)-1)*NEQ+j, j=1,NEQ ) /) 
			update_gs(NEQ*(i-1)+1:NEQ*i) = (/( (update_g(i)-1)*NEQ+j, j=1,NEQ ) /) 
		end do
		j=0
		do i=1,NELAL
			do while (j<=NELA)
				j=j+1
				if(procnum(j)==MPI_RANK) EXIT
			end do
			if(J==NELA+1) then
				print *, "ERROR at 9900XZ"
				stop
			end if
			global0(NEQ*(i-1)+1:NEQ*i) = (/ ((J-1)*NEQ+k,k=1,NEQ)/)
		end do
      END IF
!
!
!***********************************************************************
!*                                                                     *
!*              ACCOUNTING FOR ZEROs ON THE MAIN DIAGONAL              *
!*                                                                     *
!***********************************************************************
!
!
! 
! ... Initializations
!
      izerod = 0
      iprpro = 0
	  izerop = 0
!
!
!***********************************************************************
!*                                                                     *
!*                       FOR ITERATIVE SOLVERS                         *
!*                                                                     *
!***********************************************************************
!
!
      IF_Iter: IF(MATSLV /= 1) THEN
!
         N_Eq = NEQ*NEQ*NELAL			!Number of elements in all the diagonal blocks
!
! ...... Initialize: Whole array operations, CAREFUL!!!
!
         RWORK(1:N_Eq) = 0.0d0
! 
! ----------
! ...... Determine non-zero mail diagonal elements: 
! ...... Whole array operations - CAREFUL!!!
! ----------
! 
         WHERE(IRN(1:N_Eq)==ICN(1:N_Eq) .AND. ABS(CO(1:N_Eq))==0.0d0)
            RWORK(1:N_Eq) = 1.0d0
         END WHERE
!
         izerop = INT(SUM(RWORK(1:N_Eq)))
!....... Sum izerop among all processes to get izerod
		 call MPI_ALLREDUCE(izerop,izerod,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD,ierr)
		 
! ...... Re-Initialize: Whole array operations, CAREFUL!!!
!
         RWORK(1:N_Eq) = 0.0d0
!
! ...... Print a comment if different from that in the previous solution
!
         IF(izerod /= izero0 ) THEN
            IF(MPI_RANK==0) WRITE(15,6003) kcyc,iter,&
					izerod,zprocs,oprocs
            izero0 = izerod
         END IF
! 
! ----------
! ...... Clarify Z-preprocessing
! ----------
! 
         zertio = 1.0d2*izerod/(neq*nela)
! 
         IF_Zero: IF(izerod > 0) THEN
!
            CASE_zprocs: SELECT CASE(zprocs)
                         CASE('Z1')
                            iprpro = 1
                         CASE('Z2')
                            iprpro = 2
                         CASE('Z3')
                            iprpro = 3
                         CASE('Z4')
                            iprpro = 4
                         CASE DEFAULT 
                            iprpro = 0
                         END SELECT CASE_zprocs
! 
            IF_O0: IF(oprocs /= 'O0') THEN
               IF(zprocs == 'Z0' .OR. zprocs == 'Z1') THEN
                  IF(zertio > 2.0d1) THEN
                     if(MPI_RANK==0) PRINT 6004, zertio,oprocs
                     oprocs = 'O0'
                  END IF
               END IF
            END IF IF_O0
! 
         END IF IF_Zero
!
! ...... Print solver data for MOP(6) > 0
!
         IF(MOP(6) /= 0 .and. MPI_RANK==0) PRINT 6005, kcyc,iter,icall,NI,NZ,izerod,zertio
!
!
!
      ELSE
!
! ...... DIRECT SOLVERS: Print solver data for MOP(6) > 0
!
         IF(MOP(6) /= 0 .and. MPI_RANK==0) PRINT 6006, kcyc,iter,icall,NI
!
!
!
      END IF IF_Iter
! 
! -------
! ... Print matrix when MOP(6) >= 7 
! ... Whole array operations - CAREFUL!!!
! -------
! 
      IF(MOP(6) >= 7) THEN
         IF(MPI_RANK==0) PRINT 6015
		 DO I=0,MPI_NP-1
			IF(I==MPI_RANK) THEN
				PRINT 6010,(IRN(NN),ICN(NN),CO(NN),NN=1,NZ)
				CALL MPI_BARRIER(MPI_COMM_WORLD,IERR)
			ELSE
				CALL MPI_BARRIER(MPI_COMM_WORLD,iERR)
			END IF
		 END DO
      END IF
!
!
!***********************************************************************
!*                                                                     *
!*       DETERMINATION OF THE BANDWIDTHS OF THE U AND L MATRICES       *
!*                IN THE DIRECT LU DECOMPOSITION SOLVER                *
!*                                                                     *
!***********************************************************************
!
      IF(icall == 1) THEN   
         CO(mnz+1) = 0.0d0        
!         R(NREDM) = 0.0d0
      END IF
! 
! -------
! ... Determine bandwidths and memory requirements for direct solver
! -------
! 
      ! IF_Direct: IF(MATSLV == 1 .AND. icall == 1) THEN
! ! 
         ! iddfup = MAXVAL(icn(1:NZ)-irn(1:NZ))
         ! iddfdn = MAXVAL(irn(1:NZ)-icn(1:NZ))
! ! 
         ! matord = neq*nela
         ! nsupdg = iddfup
         ! nsubdg = iddfdn
         ! ntotd  = 2*nsubdg+nsupdg+1
! ! 
! ! ...... Deallocate memory for iterative solver 
! ! 
         ! DEALLOCATE(RWORK, STAT = ierG)
! ! 
! ! ...... Check if properly deallocated 
! ! 
         ! IF(ierG == 0) THEN
            ! WRITE(50,6101) 102      ! Succesful deallocation 
         ! ELSE
            ! PRINT 6102, 102         ! Unsuccesful deallocation
            ! WRITE(50,6102) 102      
            ! STOP
         ! END IF
! ! 
! ! ...... Allocate memory for direct solver 
! ! 
         ! ALLOCATE(AB(ntotd,matord), STAT = ierG)
! ! 
! ! ...... Check if properly allocated 
! ! 
         ! IF(ierG == 0) THEN
            ! WRITE(50,6103) 102      ! Succesful deallocation 
         ! ELSE
            ! PRINT 6104, 104         ! Unsuccesful deallocation
            ! WRITE(50,6104) 102      
            ! STOP
         ! END IF
! ! 
      ! END IF IF_Direct
!
!
!***********************************************************************
!*                                                                     *
!*       DETERMINATION OF THE POINTERS FOR MATRIX PREPROCESSING        *
!*            ALLOCATION OF MEMORY TO CORRESPONDING ARRAYS             *
!*                                                                     *
!***********************************************************************
!
       IF_1stCall: IF(icall == 1) THEN
 ! 
          IF_IterG: IF(NEQ > 1 .AND. MATSLV /= 1) THEN
 ! 
             IF(oprocs /= 'O0' .OR. (izerod /= 0 .AND. iprpro > 1)) THEN
! ! 				
			    
                CALL Allocate_MemPrPro(0) ! Allocate memory for preprocessing
! ! 
                CALL INDEX_Neighbor       ! Determine cell neighbors
! ! 
                CALL JSparse_Pattern      ! Determine the sparsity pattern of ... 
! !                                        ! ... the Jacobian for Z-preprocessing
             ELSE
                CALL Allocate_MemPrPro(1)
             END IF
          ELSE
             CALL Allocate_MemPrPro(1)
! ! 
          END IF IF_IterG
! ! 
       END IF IF_1stCall
!
!
!***********************************************************************
!*                                                                     *
!*                          MATRIX SOLUTION                            *
!*                                                                     *
!***********************************************************************
!
      IF_MatrixSolution: IF(MATSLV == 1) THEN
! 
! ----------
! ...... Direct solver
! ----------
! 
		 PRINT *, "DIRECT SOLVER NOT AVAILABLE IN PARALLEL.&
					CHANGE to MATSLV>1. STOPPING!"
		 STOP
    !     CALL LUD_DirectSolver(matord,nz,nsubdg,nsupdg,ntotd,&
    ! &                         matord,info)
!
      ELSE
! 
! ----------
! ...... Iterative solvers
! ----------
! 
          IF(izerod /= 0 .AND. NEQ > 1 .AND. zprocs /= 'Z0') THEN
             CALL Z_PreProcess(iprpro)
          END IF
! 
! ...... O-Pre-processing options
! 
         CASE_OPrePro: SELECT CASE (oprocs)
! 
                       CASE('O1')
                          IF(NEQ > 1) CALL O_PreProcess(1)
                       CASE('O2')
                          IF(NEQ > 1) CALL O_PreProcess(2)
                       CASE('O3')
                          IF(NEQ > 1) CALL O_PreProcess(3)
                       CASE('O4')
                          IF(NEQ > 1) CALL O_PreProcess(4)
! 
                       END SELECT CASE_OPrePro
! 
! ...... Initialization - CAREFUL! Whole array operation
! 
         wkarea(1:NT) = 0.0d0
!    
         IF(MOP(6) /= 0) CALL CPU_ElapsedTime(0,TS,TT)
! 
! ----------
! ...... Select conjugate gradient solver
! ----------
! 
!         print 8601
!         print 8603, R(1:N)
!         print 8602
!         print 8603, CO(1:NZ)
 8601 format(/,t2,'R Values')
 8602 format(/,t2,'CO Values')
 8603 format(8(1pe15.8,1x))
!
		call MPI_BARRIER(MPI_COMM_WORLD,IERR)!REMOVE
         IF_CGSolver: IF(MATSLV == 3) THEN
            call DSLUCS_SW_PAR(NI,NT,R,wkarea,&
							NZ,irn,icn,CO,CLOSUR,NMAXIT,ITERU,ERR,&
    &                  IERR,IUNIT,RWORK,LENW,jvect,LENIW,UPDATE_PS,UPDATE_LS,&
					&overlap,MAAS,&
	&					update_gs,global0,reorder)			!ARRAYS FOR REORDERING
			!Calculating total iterations, and finding average number of iterations
			totiter = totiter + ITERU
			if(MPI_RANK==0) write(IUNIT,*), "Average iterations upto this point is",totiter/ICALL," in total iterations",ICALL
		 
                ELSE IF(MATSLV == 6) THEN
		 
            call DSLUCS_WP_PAR(NI,NT,R,wkarea,NZ,&
								irn,icn,CO,CLOSUR,NMAXIT,ITERU,ERR,&
     &                  IERR,IUNIT,RWORK,LENW,jvect,LENIW,UPDATE_P,UPDATE_L)
	 
	     ELSE IF(MATSLV == 7) THEN
           CALL DSLUCS_SC_PAR(NI,NT,R,wkarea,NZ,irn,icn,CO,CLOSUR,NMAXIT,ITERU,ERR,&
     &                  IERR,IUNIT,RWORK,LENW,jvect,LENIW,UPDATE_PS,UPDATE_LS)
	 	totiter = totiter + ITERU
			if(MPI_RANK==0) write(IUNIT,*), "Average iterations upto this point is",totiter/ICALL," in total iterations",ICALL
		 
        ELSE 
			 PRINT *, "MATSLV = 3,6 and 7 only supported right now in parallel&
						Please choose one of them. Thank you. Aborting!"
			 stop
         END IF IF_CGSolver
! 
! ----------
! ...... Retrieve solution - CAREFUL! Whole array operation 
! ---------- 
! 
	
         R(1:NI) = wkarea(1:NI)
!    
         IF(MOP(6) /= 0) THEN
            CALL CPU_ElapsedTime(1,TSS,TT)
            IF(MPI_RANK==0) PRINT 6040,TSS
         END IF
! 
! ----------
! ...... Update and print iteration statistics 
! ----------
! 
         iteruc = iteruc+iteru
! 
         IF(MPI_RANK==0) &
		 & WRITE(15,6045) kcyc,iter,deltex,ierr,err,iteru,iteruc
! 
! 
      END IF IF_MatrixSolution
! 
! -------
! ... Print-out for MOP(6) >= 5
! -------
! 
      IF_MOP6: IF(MOP(6) >= 5) THEN
         if(MPI_RANK==0) PRINT 6050
! 
         DO_NumEle: DO i=1,NELA
			if(procnum(i)==MPI_RANK) THEN	
				NN = locnum(i)
				PRINT 6055, elem(nn)%name,&
					(R((NN-1)*NEQ+K),K=1,NEQ)
				CALL MPI_BARRIER(MPI_COMM_WORLD,IERR)
			ELSE
				CALL MPI_BARRIER(MPI_COMM_WORLD,IERR)
			END IF
         END DO DO_NumEle
! 
      END IF IF_MOP6
!
!
!*********************************************************************
!*                                                                   *
!*            UPDATE CHANGES - Whole array operations                *
!*                                                                   *
!*********************************************************************
!
!
!
      DO_NumEl: DO nn=1,NELAL
! 
         NLOC  = (nn-1)*NEQ
         NLOCP = (nn-1)*NK1
! 
         DX(NLOCP+1:NLOCP+NEQ) = DX(NLOCP+1:NLOCP+NEQ)&
     &                          +WNR*R(NLOC+1:NLOC+NEQ)  
! 
      END DO DO_NumEl
!
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'SOLVE_JacobianME  1.0   24 August    2004',6X,&
     &         'Solves the Jacobian matrix equation through an ',&
     &         'interface with linear eq. solvers ',/,47X,&
     &         'Can call a direct solver or a package of ',&
     &         'conjugate gradient solvers')
! 
 6003 FORMAT(1X,'At KCYC=',i5,' and ITER=',i5,', IZEROD=',I5, &
     &              ', ZPROCS = ',a2,' and OPROCS = ',a2)         
 6004 FORMAT(//,' ',15('WARNING-'),//T14,F5.2,'% of the elements on ',&
     &          'the main diagonal of the Jacobian matrix are zeros',/,&
     &       T14,'The matrix preprocessor OPROCS = ',A2,&
     &           ' cannot be used',/,&
     &       T14,'Action taken: reset OPROCS to *O0* (no ',&
     &           'O-preprocessing); continue execution')
 6005 FORMAT(' !!!!! L I N E Q !!!!!  at [KCYC, ITER] = [',I5,',',I3,&
     &       ']','   ICALL =',I5,'   N =',I4,'   NZ =',I8,&
     &       '   IZEROD =',I7,'   or ',F6.2,' % zeros')
 6006 FORMAT(' !!!!! L I N E Q !!!!!  at [KCYC, ITER] = [',I5,',',I3,&
     &       ']','   ICALL =',I5,'   N =',I4,'   (Direct solver)')
! 
 6010 FORMAT(5(1X,2I5,1pE14.7))
 6015 FORMAT(/,' MATRIX OF COEFFICIENTS',/)
! 
 6020 FORMAT(//,20('ERROR-'),//,T33,&
     &             '       S I M U L A T I O N   A B O R T E D',&
     &       /,T40,'DECLARED BANDWIDTH SMALLER THAN NEEDED',&
     &       /,T33,'        PLEASE CORRECT AND TRY AGAIN',     &
     &       //,T20,'THE NUMBER OF NEEDED DIAGONALS IS', I4,&
     &             ' WHILE THE AVAILABLE NUMBER IS ', I4,&
     &       //,T27,'THE PARAMETER nrwork MUST BE INCREASED TO ',&
     &              'AT LEAST ',I10,&
     &       //,20('ERROR-'))         
 6030 FORMAT('  EPS = ',1pE11.4,  '  RMIN = ',1pE11.4, &                       
     &       '  RESID = ',1pE11.4,'  IRNCP = ',I6,                &                 
     &       '  MINIRN = ',I6,  '  MINICN = ',I6,'IRANK = ',I4)           
! 
 6040 FORMAT('      SOLUTION TIME = ',1PE12.5,'  SECONDS')
 6045 FORMAT(T5,' At [',I4,',',I3,']',' DELT=',1pE12.5,&
     &       ' IERR=',I1,'& ERR=',1PE12.6,' IT=',I5,' ITC=',I10)
!
 6050 FORMAT(/,' ===== INCREMENTS ====   in order of primary',&
     &         ' variables',/)
 6055 FORMAT('       AT ELEMENT *',A5,'*   ',8(1X,1pE15.8))
! 
 6101 FORMAT(/,T3,'Memory deallocation at point ',i3,&
     &            ' in "SOLVE_JacobianME" was successful')
 6102 FORMAT(//,20('ERROR-'),//,&
     &       T3,'Memory deallocation at point ',i3,&
     &          ' in "SOLVE_JacobianME" was unsuccessful',//,&
     &       T2,'!!!!!!!    THE EXECUTION WAS STOPPED    !!!!!!!',&
     &       //,20('ERROR-'))
! 
 6103 FORMAT(T3,'Memory allocation at point ',i3,&
     &          ' in "SOLVE_JacobianME" was successful')
 6104 FORMAT(//,20('ERROR-'),//,&
     &       T3,'Memory allocation at point ',i3,&
     &          ' in "SOLVE_JacobianME" was unsuccessful',//,&
     &       T2,'!!!!!!!    THE EXECUTION WAS STOPPED    !!!!!!!',&
     &       //,20('ERROR-'))
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of SOLVE_JacobianME
!
!
      RETURN
! 
      END SUBROUTINE SOLVE_JacobianME
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE O_PreProcess(ilevel)
! 
! ...... Modules to be used 
! 
         USE EOS_Parameters
! 
         USE Basic_Param
         USE GenControl_Param
         USE Solver_Param
! 
         USE Variable_Arrays
         USE SolMatrix_Arrays
		 USE MPI_PARAM
! 
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*             ROUTINE FOR O-PREPROCESSING OF THE JACOBIAN             * 
!*                                                                     *
!*                   Version 1.00 - December 9, 2003                   *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: QUOT,QQQ
! 
! -------
! ... Integer arrays
! -------
! 
      INTEGER, DIMENSION(MaxNum_Equat*10) :: Index_CO1,Index_CO2
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: ilevel
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: MXMYMZ,IPHASE,ijk,LB,LB1,LA,N_1,N_2,N_3,L,NDM 
      INTEGER :: NA0LOC,NC0LOC

! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of O_PreProcess
!
!
      ICALL = ICALL+1
      IF(ICALL == 1 .and. MPI_RANK==0) WRITE(11,6000)
!
! ... Some definitions
!
      MXMYMZ = NELAL
      IPHASE = NEQ
!
! ... Provision for single-equation systems (NEQ = 1)
!
      IF(IPHASE == 1) GO TO 1000
!
!***********************************************************************
!*                                                                     *
!*               ELIMINATION OF LEFT-OFF-M.BAND ELEMENTS               *
!*                                                                     *
!***********************************************************************
!
      DO_NumEle: DO IJK=1,MXMYMZ
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... OUTER EQUATION LOOP  
! ...... 
! >>>>>>>>>>>>>>>>>>
! 
! 
         DO_Outer1: DO LB=1,IPHASE-1
            LB1=LB+1
! 
! 
! >>>>>>>>>>>>>>>>>>>>>
! .........
! ......... INTERMEDIATE EQUATION LOOP   
! ......... 
! >>>>>>>>>>>>>>>>>>>>>
! 
! 
            DO_Medium1: DO LA=LB1,IPHASE
! 
               QUOT = CO(NB0(LB,LA,IJK))/CO(NB0(LB,LB,IJK))    ! Pivoting
!
! ............ Adjusting the right-hand side
!
               R(NX0(LA,IJK)) = R(NX0(LA,IJK))-QUOT*R(NX0(LB,IJK))
!
! ............ Adjusting the elements of the main diagonal submatrix
!
               CO(NB0(LB1:IPHASE,LA,IJK)) = CO(NB0(LB1:IPHASE,LA,IJK))&
     &                                     -QUOT&
     &                                     *CO(NB0(LB1:IPHASE,LB,IJK))
!
! ............ Adjusting the elements of the submatrix to 
! ............ the left of the main diagonal submatrix
!
               IF_Left1: IF(IJKMM(ijk) >= 1) THEN
                  N_1 = 1+ISUMMM(ijk-1)
                  N_2 = IJKMM(ijk)+ISUMMM(ijk-1)
                  N_3 = IPHASE*IJKMM(ijk)

!
                  Index_CO1(1:N_3) &
     &              = PACK(       NA0(N_1:N_2,1:IPHASE,LA), &
     &                     MASK = NA0(N_1:N_2,1:IPHASE,LA) /= 0)
                  Index_CO2(1:N_3) &
     &              = PACK(       NA0(N_1:N_2,1:IPHASE,LB), &
     &                     MASK = NA0(N_1:N_2,1:IPHASE,LB) /= 0)
!
                  CO(Index_CO1(1:N_3)) = CO(Index_CO1(1:N_3))&
     &                                  -CO(Index_CO2(1:N_3))*QUOT
! 
               END IF IF_Left1

!
! ............ Adjusting the elements of the submatrix to 
! ............ the right of the main diagonal submatrix
!
               IF_Right1: IF(IJKPP(ijk) >= 1) THEN
                  N_1 = 1+ISUMPP(ijk-1)
                  N_2 = IJKPP(ijk)+ISUMPP(ijk-1)
                  N_3 = IPHASE*IJKPP(ijk)

!
                  Index_CO1(1:N_3)  &
     &              = PACK(       NC0(N_1:N_2,1:IPHASE,LA),  &
     &                     MASK = NC0(N_1:N_2,1:IPHASE,LA) /= 0)
                  Index_CO2(1:N_3)  &
     &              = PACK(       NC0(N_1:N_2,1:IPHASE,LB),  &
     &                     MASK = NC0(N_1:N_2,1:IPHASE,LB) /= 0)
!
                  CO(Index_CO1(1:N_3)) = CO(Index_CO1(1:N_3)) &
     &                                  -CO(Index_CO2(1:N_3))*QUOT
! 
               END IF IF_Right1

! 
! <<<<<<<<<                          
! <<<<<<<<< End of the INTERMEDIATE EQUATION LOOP        
! <<<<<<<<<    
! 
            END DO DO_Medium1  
! 
            CO(NB0(LB,LB1:IPHASE,IJK)) = 0.0d0
! 
! <<<<<<                          
! <<<<<< End of the OUTER EQUATION LOOP        
! <<<<<<    
! 
         END DO DO_Outer1  
!
!
! 
         IF(ilevel == 1) CYCLE
!
!***********************************************************************
!*                                                                     *
!*              ELIMINATION OF RIGHT-OFF-M.BAND ELEMENTS               *
!*                                                                     *
!***********************************************************************
!
         DO_Outer2: DO LB=IPHASE,2,-1
            LB1=LB-1
! 
! 
! >>>>>>>>>>>>>>>>>>>>>
! .........
! ......... INTERMEDIATE EQUATION LOOP   
! ......... 
! >>>>>>>>>>>>>>>>>>>>>
! 
! 
            DO_Medium2: DO LA=LB1,1,-1
! 
               QUOT           = CO(NB0(LB,LA,IJK))/CO(NB0(LB,LB,IJK))
!
! ............ Adjusting the right-hand side
!
               R(NX0(LA,IJK)) = R(NX0(LA,IJK))-QUOT*R(NX0(LB,IJK))
!
! ............ Adjusting the elements of the submatrix to 
! ............ the left of the main diagonal submatrix
!
               IF_Left2: IF(IJKMM(ijk) >= 1) THEN
                  N_1 = 1+ISUMMM(ijk-1)
                  N_2 = IJKMM(ijk)+ISUMMM(ijk-1)
                  N_3 = IPHASE*IJKMM(ijk)

!
                  Index_CO1(1:N_3)  &
     &              = PACK(       NA0(N_1:N_2,1:IPHASE,LA),  &
     &                     MASK = NA0(N_1:N_2,1:IPHASE,LA) /= 0)
                  Index_CO2(1:N_3)  &
     &              = PACK(       NA0(N_1:N_2,1:IPHASE,LB),  &
     &                     MASK = NA0(N_1:N_2,1:IPHASE,LB) /= 0)
!
                  CO(Index_CO1(1:N_3)) = CO(Index_CO1(1:N_3)) &
     &                                  -CO(Index_CO2(1:N_3))*QUOT
! 
               END IF IF_Left2

!
! ............ Adjusting the elements of the submatrix to 
! ............ the right of the main diagonal submatrix
!
               IF_Right2: IF(IJKPP(ijk) >= 1) THEN
                  N_1 = 1+ISUMPP(ijk-1)
                  N_2 = IJKPP(ijk)+ISUMPP(ijk-1)
                  N_3 = IPHASE*IJKPP(ijk)

!
                  Index_CO1(1:N_3)  &
     &              = PACK(       NC0(N_1:N_2,1:IPHASE,LA),  &
     &                     MASK = NC0(N_1:N_2,1:IPHASE,LA) /= 0)
                  Index_CO2(1:N_3)  &
     &              = PACK(       NC0(N_1:N_2,1:IPHASE,LB),  &
     &                     MASK = NC0(N_1:N_2,1:IPHASE,LB) /= 0)
!
                  CO(Index_CO1(1:N_3)) = CO(Index_CO1(1:N_3)) &
     &                                  -CO(Index_CO2(1:N_3))*QUOT
! 
               END IF IF_Right2

! 
! <<<<<<<<<                          
! <<<<<<<<< End of the INTERMEDIATE EQUATION LOOP        
! <<<<<<<<<    
! 
            END DO DO_Medium2  
!
            CO(NB0(LB,LB1:1:-1,IJK)) = 0.0d0
! 
! <<<<<<                          
! <<<<<< End of the OUTER EQUATION LOOP        
! <<<<<<    
! 
         END DO DO_Outer2  
!
!
!
         IF(ilevel == 2) CYCLE
!
!***********************************************************************
!*                                                                     *
!*                     N O R M A L I Z A T I O N                       *
!*                                                                     *
!***********************************************************************
!
         DO_Outer3: DO LB=1,IPHASE
            QQQ                =-1.0d0/CO(NB0(LB,LB,IJK))
            CO(NB0(LB,LB,IJK)) =-1.0d0
            R(NX0(LB,IJK))     = R(NX0(LB,IJK))*QQQ
! 
! -------------
! ......... Inner equation loop           
! -------------
! 
            DO_Inner3: DO L=1,IPHASE
! 
               IF_Left3: IF(IJKMM(ijk) >= 1) THEN
                 N_1                   = 1+ISUMMM(ijk-1)
                 N_2                   = IJKMM(ijk)+ISUMMM(ijk-1)
                 CO(NA0(N_1:N_2,L,LB)) = CO(NA0(N_1:N_2,L,LB))*QQQ
               END IF IF_Left3
! 
               IF_Right3: IF(IJKPP(ijk) >= 1) THEN
                 N_1                   = 1+ISUMPP(ijk-1)
                 N_2                   = IJKPP(ijk)+ISUMPP(ijk-1)
                 CO(NC0(N_1:N_2,L,LB)) = CO(NC0(N_1:N_2,L,LB))*QQQ
               END IF IF_Right3
! 
! ............ End of equation inner loop 
! 
            END DO DO_Inner3  
! 
! <<<<<<<<<                          
! <<<<<<<<< End of the OUTER EQUATION LOOP        
! <<<<<<<<<    
! 
         END DO DO_Outer3  
!
!
!
      END DO DO_NumEle 
!
      RETURN
!
!***********************************************************************
!*                                                                     *
!*                            THE NEQ=1 CASE                           *
!*                                                                     *
!***********************************************************************
!
 1000 DO_NumEle2: DO IJK=1,MXMYMZ
         QQQ =-1.0e0/CO(NB0(1,1,IJK))
! 
         CO(NB0(1,1,IJK)) =-1.0d0
         R(NX0(1,IJK))    = R(NX0(1,IJK))*QQQ
!
         IF_MinRange: IF(IJKMM(ijk) >= 1) THEN
            DO_Left: DO NDM=1,IJKMM(ijk)
               NA0LOC              = NDM+ISUMMM(ijk-1)
               CO(NA0(NA0LOC,1,1)) = CO(NA0(NA0LOC,1,1))*QQQ
            END DO DO_Left
         END IF IF_MinRange
! 
         IF_MaxRange: IF(IJKPP(ijk) >= 1) THEN
            DO_Right: DO NDM=1,IJKPP(ijk)
               NC0LOC              = NDM+ISUMPP(ijk-1)
               CO(NC0(NC0LOC,1,1)) = CO(NC0(NC0LOC,1,1))*QQQ
            END DO DO_Right
         END IF IF_MaxRange
! 
      END DO DO_NumEle2
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'O_PreProcess      1.0    9 December  2003',6X, &
     &         'Routine for O-preprocessing ', &
     &         'of the Jacobian')
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of O_PreProcess
!
!
      RETURN
!
      END SUBROUTINE O_PreProcess
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE Z_PreProcess(IOPT)
! 
! ...... Modules to be used 
! 
         USE EOS_Parameters
! 
         USE Basic_Param
         USE GenControl_Param
         USE Solver_Param
! 
         USE Variable_Arrays
         USE SolMatrix_Arrays
		 USE MPI_PARAM
! 
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*             ROUTINE FOR Z-PREPROCESSING OF THE JACOBIAN             * 
!*                                                                     *
!*                  Version 1.00 - December 16, 2003                   *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(MaxNum_Equat) :: CO_Piv,TR0
! 
      REAL(KIND = 8), DIMENSION(MaxNum_Equat,MaxNum_Equat) :: B0INV
      REAL(KIND = 8), DIMENSION(MaxNum_Equat,MaxNum_Equat) :: TA0,TC0
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: ROWMAX,QUOT,DETR,DETI,DET1,DET2,DET3
! 
! -------
! ... Integer arrays
! -------
! 
      INTEGER, DIMENSION(MaxNum_Equat*10) :: Index_CO1,Index_CO2
      INTEGER, DIMENSION(MaxNum_Equat)    :: Index_CO3
      INTEGER, DIMENSION(1)               :: Index_CO
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: IOPT
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: MXMYMZ,IPHASE,ijk,LA,LB,N_1,N_2,N_3,L1
      INTEGER :: ndm,NA0LOC,NC0LOC
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of Z_PreProcess
!
!
      ICALL = ICALL+1
      IF(ICALL == 1 .and. MPI_RANK==0) WRITE(11,6000)
!
! ... Some local definitions
!
      MXMYMZ = NELAL
      IPHASE = NEQ
!
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*            REPLACEMENT OF 0 BY SEED ON THE MAIN DIAGONAL            *
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
!
!
      IF_Iopt1: IF(IOPT <= 1) THEN
!
         WHERE(IRN(1:NZ) == ICN(1:NZ) .AND. ABS(CO(1:NZ)) == 0.0d0) 
            CO(1:NZ) = SEED
         END WHERE
!
         RETURN
!
      END IF IF_Iopt1
!
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*     ADDITION OF EQUATIONS TO PREVENT ZEROS ON THE MAIN DIAGONAL     *
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
!
      IF(IOPT > 2) GO TO 101
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                         1st ELEMENT LOOP                            >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_NumEle1: DO IJK=1,MXMYMZ
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... 1st OUTER EQUATIONS LOOP 
! ...... 
! >>>>>>>>>>>>>>>>>>
! 
! 
        DO_OuterE1: DO LA=1,IPHASE
! 
! -----------
! ....... For zero main diagonal elements ...          
! -----------
! 
          IF_MainD1: IF(CO(NB0(LA,LA,IJK)) == 0.0d0) THEN
! 
! 
! >>>>>>>>>>>>>>>>>>>>>
! .........
! ......... 1st MIDDLE EQUATIONS LOOP 
! ......... 
! >>>>>>>>>>>>>>>>>>>>>
! 
! 
             DO_MiddleE1: DO LB=1,IPHASE
               IF(LB == LA) CYCLE
! 
! ----------------
! ............ Determining a non-zero element to add           
! ----------------
! 
               IF_NonZ1: IF(CO(NB0(LA,LB,IJK)) /= 0.0d0) THEN
!
! ............... Adjusting the right-hand side
!
                  R(NX0(LA,IJK)) = R(NX0(LA,IJK))+1.75d0*R(NX0(LB,IJK))
!
! ............... Adjusting the column of the main diagonal submatrix
!
                  CO(NB0(1:IPHASE,LA,IJK)) = CO(NB0(1:IPHASE,LA,IJK)) &
     &                                      +CO(NB0(1:IPHASE,LB,IJK)) &
     &                                      *1.75d0
!
! ............... Adjusting the remaining elements of the column to 
! ............... the left of the main diagonal submatrix
!
                  IF_Left1: IF(IJKMM(ijk) >= 1) THEN
                     N_1 = 1+ISUMMM(ijk-1)
                     N_2 = IJKMM(ijk)+ISUMMM(ijk-1)
                     N_3 = IPHASE*IJKMM(ijk)
!
                     Index_CO1(1:N_3)  &
     &                 = PACK(       NA0(N_1:N_2,1:IPHASE,LA),  &
     &                        MASK = NA0(N_1:N_2,1:IPHASE,LA) /= 0)
                     Index_CO2(1:N_3)  &
     &                 = PACK(       NA0(N_1:N_2,1:IPHASE,LB),  &
     &                        MASK = NA0(N_1:N_2,1:IPHASE,LB) /= 0)
!
                     CO(Index_CO1(1:N_3)) = CO(Index_CO1(1:N_3)) &
     &                                     +CO(Index_CO2(1:N_3))*1.75d0
                  END IF IF_Left1
!
! ............... Adjusting the remaining elements of the column to 
! ............... the right of the main diagonal submatrix
!
                  IF_Right1: IF(IJKPP(ijk) >= 1) THEN
                     N_1 = 1+ISUMPP(ijk-1)
                     N_2 = IJKPP(ijk)+ISUMPP(ijk-1)
                     N_3 = IPHASE*IJKPP(ijk)
!
                     Index_CO1(1:N_3)  &
     &                 = PACK(       NC0(N_1:N_2,1:IPHASE,LA),  &
     &                        MASK = NC0(N_1:N_2,1:IPHASE,LA) /= 0)
                     Index_CO2(1:N_3)  &
     &                 = PACK(       NC0(N_1:N_2,1:IPHASE,LB),  &
     &                        MASK = NC0(N_1:N_2,1:IPHASE,LB) /= 0)
!
                     CO(Index_CO1(1:N_3)) = CO(Index_CO1(1:N_3)) &
     &                                     +CO(Index_CO2(1:N_3))*1.75d0
                  END IF IF_Right1
!
                  EXIT  
!
                END IF IF_NonZ1
!                          
! <<<<<<<<<<<                          
! <<<<<<<<<<< End of the 1st MIDDLE EQUATIONS LOOP          
! <<<<<<<<<<<   
!                          
              END DO DO_MiddleE1
!
            END IF IF_MainD1
!
! <<<<<                          
! <<<<< End of the 1st OUTER EQUATIONS LOOP         
! <<<<<    
!
        END DO DO_OuterE1
!
! <<<                      
! <<< End of the 1st ELEMENT LOOP         
! <<<
!
      END DO DO_NumEle1
!
      RETURN
!
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*                NORMALIZATION + ADDITION OF EQUATIONS                *
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
!
  101 IF(IOPT > 4) GO TO 301
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>  PART I: NORMALIZATION USING THE LARGEST ELEMENT OF SUBMATRIX ROW   >
!>                         2nd ELEMENT LOOP                            >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_NumEle2: DO IJK=1,MXMYMZ
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... 2nd OUTER EQUATIONS LOOP  
! ...... 
! >>>>>>>>>>>>>>>>>>
! 
! 
        DO_OuterE2: DO LB=1,IPHASE
!
! ........ Finding the largest element on the row of 
! ........ the main diagonal submatrix
!
           Index_CO3        = NB0(1:IPHASE,LB,IJK)
           CO_Piv(1:IPHASE) = ABS(CO(Index_CO3(1:IPHASE)))
!
           ROWMAX   = MAXVAL(CO_Piv(1:IPHASE))
           Index_CO = Index_CO3(MAXLOC(CO_Piv(1:IPHASE)))
!
! ........ Computing the multiplier for the whole row
!
           QUOT    = 1.0d0/CO(Index_CO(1))
!
! ........ Adjusting the right-hand side
!
           R(NX0(LB,IJK)) = R(NX0(LB,IJK))*QUOT
!
! ........ Adjusting the row of the main diagonal submatrix
!
           CO(NB0(1:IPHASE,LB,IJK)) = CO(NB0(1:IPHASE,LB,IJK))*QUOT
!
! ........ Adjusting the remaining elements of the row to 
! ........ the left of the main diagonal submatrix
!
           IF_Left2: IF(IJKMM(ijk) >= 1) THEN
              N_1 = 1+ISUMMM(ijk-1)
              N_2 = IJKMM(ijk)+ISUMMM(ijk-1)
              N_3 = IPHASE*IJKMM(ijk)
!
              Index_CO1(1:N_3)  &
     &          = PACK(       NA0(N_1:N_2,1:IPHASE,LB),  &
     &                 MASK = NA0(N_1:N_2,1:IPHASE,LB) /= 0)
!
              CO(Index_CO1(1:N_3)) = CO(Index_CO1(1:N_3))*QUOT
           END IF IF_Left2
!
! ........ Adjusting the remaining elements of the row to 
! ........ the right of the main diagonal submatrix
!
           IF_Right2: IF(IJKPP(ijk) >= 1) THEN
              N_1 = 1+ISUMPP(ijk-1)
              N_2 = IJKPP(ijk)+ISUMPP(ijk-1)
              N_3 = IPHASE*IJKPP(ijk)
!
              Index_CO1(1:N_3)  &
     &          = PACK(       NC0(N_1:N_2,1:IPHASE,LB),  &
     &                 MASK = NC0(N_1:N_2,1:IPHASE,LB) /= 0)
!
              CO(Index_CO1(1:N_3)) = CO(Index_CO1(1:N_3))*QUOT
           END IF IF_Right2
!
! <<<<<                          
! <<<<< End of the 2nd OUTER EQUATIONS LOOP         
! <<<<<    
!
        END DO DO_OuterE2
!
! <<<                      
! <<< End of the 2nd ELEMENT LOOP         
! <<<
!
      END DO DO_NumEle2
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                   PART II: ADDITION OF EQUATIONS                    >
!>                         3rd ELEMENT LOOP                            >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_NumEle3: DO IJK=1,MXMYMZ
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... 3rd OUTER EQUATIONS LOOP 
! ...... 
! >>>>>>>>>>>>>>>>>>
! 
! 
        DO_OuterE3: DO LA=1,IPHASE
! 
! -----------
! ....... For zero main diagonal elements ...          
! -----------
! 
          IF_MainD3: IF(CO(NB0(LA,LA,IJK)) == 0.0d0) THEN
! 
! 
! >>>>>>>>>>>>>>>>>>>>>
! .........
! ......... 1st MIDDLE EQUATIONS LOOP 
! ......... 
! >>>>>>>>>>>>>>>>>>>>>
! 
! 
             DO_MiddleE3: DO LB=1,IPHASE
               IF(LB == LA) CYCLE
! 
! ----------------
! ............ Determining a non-zero element to add           
! ----------------
! 
               IF_NonZ3: IF(CO(NB0(LA,LB,IJK)) /= 0.0d0) THEN
!
! ............... Adjusting the right-hand side
!
                  R(NX0(LA,IJK)) = R(NX0(LA,IJK))+1.75d0*R(NX0(LB,IJK))
!
! ............... Adjusting the column of the main diagonal submatrix
!
                  CO(NB0(1:IPHASE,LA,IJK)) = CO(NB0(1:IPHASE,LA,IJK)) &
     &                                      +CO(NB0(1:IPHASE,LB,IJK)) &
     &                                      *1.75d0
!
! ............... Adjusting the remaining elements of the column to 
! ............... the left of the main diagonal submatrix
!
                  IF_Left3: IF(IJKMM(ijk) >= 1) THEN
                     N_1 = 1+ISUMMM(ijk-1)
                     N_2 = IJKMM(ijk)+ISUMMM(ijk-1)
                     N_3 = IPHASE*IJKMM(ijk)
!
                     Index_CO1(1:N_3)  &
     &                 = PACK(       NA0(N_1:N_2,1:IPHASE,LA),  &
     &                        MASK = NA0(N_1:N_2,1:IPHASE,LA) /= 0)
                     Index_CO2(1:N_3)  &
     &                 = PACK(       NA0(N_1:N_2,1:IPHASE,LB),  &
     &                        MASK = NA0(N_1:N_2,1:IPHASE,LB) /= 0)
!
                     CO(Index_CO1(1:N_3)) = CO(Index_CO1(1:N_3)) &
     &                                     +CO(Index_CO2(1:N_3))*1.75d0
                  END IF IF_Left3
!
! ............... Adjusting the remaining elements of the column to 
! ............... the right of the main diagonal submatrix
!
                  IF_Right3: IF(IJKPP(ijk) >= 1) THEN
                     N_1 = 1+ISUMPP(ijk-1)
                     N_2 = IJKPP(ijk)+ISUMPP(ijk-1)
                     N_3 = IPHASE*IJKPP(ijk)
!
                     Index_CO1(1:N_3)  &
     &                 = PACK(       NC0(N_1:N_2,1:IPHASE,LA),  &
     &                        MASK = NC0(N_1:N_2,1:IPHASE,LA) /= 0)
                     Index_CO2(1:N_3)  &
     &                 = PACK(       NC0(N_1:N_2,1:IPHASE,LB),  &
     &                        MASK = NC0(N_1:N_2,1:IPHASE,LB) /= 0)
!
                     CO(Index_CO1(1:N_3)) = CO(Index_CO1(1:N_3)) &
     &                                     +CO(Index_CO2(1:N_3))*1.75d0
                  END IF IF_Right3
!
                  EXIT  
!
                END IF IF_NonZ3
!                          
! <<<<<<<<<<<                          
! <<<<<<<<<<< End of the 3rd MIDDLE EQUATIONS LOOP          
! <<<<<<<<<<<   
!                          
              END DO DO_MiddleE3
!
            END IF IF_MainD3
!
! <<<<<                          
! <<<<< End of the 3rd OUTER EQUATIONS LOOP         
! <<<<<    
!
        END DO DO_OuterE3
!
! <<<                      
! <<< End of the 3rd ELEMENT LOOP         
! <<<
!
      END DO DO_NumEle3
!
      RETURN
!
!
!***********************************************************************
!*                                                                     *
!*    MULTIPLICATION BY THE INVERSE OF THE MAIN-DIAGONAL SUBMATRIX     *
!*                                                                     *
!***********************************************************************
!
!
  301 IF(NEQ == 3) GO TO 401
!
!
!
      DO 400 IJK=1,MXMYMZ
!
        DETR = CO(NB0(1,1,IJK))*CO(NB0(2,2,IJK))- &
     &         CO(NB0(1,2,IJK))*CO(NB0(2,1,IJK))
        IF(ABS(DETR).LE.SEED) THEN
           PRINT 6001
           STOP
        END IF
        DETI = 1.0e0/DETR
!
        B0INV(1,1) = CO(NB0(2,2,IJK))*DETI
        B0INV(1,2) =-CO(NB0(1,2,IJK))*DETI
        B0INV(2,1) =-CO(NB0(2,1,IJK))*DETI
        B0INV(2,2) = CO(NB0(1,1,IJK))*DETI
!
        CO(NB0(1,1,IJK)) = 1.0e0
        CO(NB0(2,2,IJK)) = 1.0e0
        CO(NB0(1,2,IJK)) = 0.0e0
        CO(NB0(2,1,IJK)) = 0.0e0
!
        DO 390 LB=1,IPHASE
          TR0(LB) = 0.0e0
          DO 355 L1=1,IPHASE
            TR0(LB) = TR0(LB)+B0INV(L1,LB)*R(NX0(L1,IJK))
  355     CONTINUE 
          R(NX0(LB,IJK)) = TR0(LB)
! 
          DO 380 LA=1,IPHASE             
            TA0(LA,LB) = 0.0e0
            TC0(LA,LB) = 0.0e0
! 
            IF(IJKMM(ijk).LT.1) GO TO 371
            DO 370 NDM=1,IJKMM(ijk)
              NA0LOC = NDM+ISUMMM(ijk-1)
              DO 360 L1=1,IPHASE
                 TA0(LA,LB) = TA0(LA,LB) &
     &                       +CO(NA0(NA0LOC,L1,LB))*B0INV(LA,L1)
  360         CONTINUE
              CO(NA0(NA0LOC,LA,LB)) = TA0(LA,LB)         
  370       CONTINUE
! 
  371       IF(IJKPP(ijk).LT.1) GO TO 380
            DO 375 NDM=1,IJKPP(ijk)
              NC0LOC = NDM+ISUMPP(ijk-1)
              DO 372 L1=1,IPHASE
                 TC0(LA,LB) = TC0(LA,LB) &
     &                       +CO(NC0(NC0LOC,L1,LB))*B0INV(LA,L1)
  372         CONTINUE
              CO(NC0(NC0LOC,LA,LB)) = TC0(LA,LB)         
  375       CONTINUE
! 
  380     CONTINUE
  390   CONTINUE
  400 CONTINUE
      GO TO 999
!
!
!
  401 DO 500 IJK=1,MXMYMZ
!
        DET1 = CO(NB0(2,2,IJK))*CO(NB0(3,3,IJK)) &
     &        -CO(NB0(2,3,IJK))*CO(NB0(3,2,IJK))
        DET2 = CO(NB0(2,1,IJK))*CO(NB0(3,3,IJK)) &
     &        -CO(NB0(2,3,IJK))*CO(NB0(3,1,IJK))
        DET3 = CO(NB0(2,1,IJK))*CO(NB0(3,2,IJK)) &
     &        -CO(NB0(2,2,IJK))*CO(NB0(3,1,IJK))
        DETR = CO(NB0(1,1,IJK))*DET1 &
     &        +CO(NB0(1,2,IJK))*DET2 &
     &        +CO(NB0(1,3,IJK))*DET3
!
        IF(ABS(DETR).LE.SEED) THEN
           PRINT 6001
           STOP
        END IF
        DETI = 1.0d0/DETR
!
        B0INV(1,1) = DET1*DETI
        B0INV(1,2) =(CO(NB0(1,3,IJK))*CO(NB0(3,2,IJK)) &
     &              -CO(NB0(1,2,IJK))*CO(NB0(3,3,IJK)))*DETI
        B0INV(1,3) =(CO(NB0(1,2,IJK))*CO(NB0(2,3,IJK)) &
     &              -CO(NB0(1,3,IJK))*CO(NB0(2,2,IJK)))*DETI
!
        B0INV(2,1) =-DET2*DETI
        B0INV(2,2) =(CO(NB0(1,1,IJK))*CO(NB0(3,3,IJK)) &
     &              -CO(NB0(1,3,IJK))*CO(NB0(3,1,IJK)))*DETI
        B0INV(2,3) =(CO(NB0(1,3,IJK))*CO(NB0(2,1,IJK)) &
     &              -CO(NB0(1,1,IJK))*CO(NB0(2,3,IJK)))*DETI
!
        B0INV(3,1) = DET3*DETI
        B0INV(3,2) =(CO(NB0(1,2,IJK))*CO(NB0(3,1,IJK)) &
     &              -CO(NB0(1,1,IJK))*CO(NB0(3,2,IJK)))*DETI
        B0INV(3,3) =(CO(NB0(1,1,IJK))*CO(NB0(2,2,IJK)) &
     &              -CO(NB0(1,2,IJK))*CO(NB0(2,1,IJK)))*DETI
!
        CO(NB0(1,1,IJK)) =-1.0d0
        CO(NB0(2,2,IJK)) =-1.0d0
        CO(NB0(3,3,IJK)) =-1.0d0
        CO(NB0(1,2,IJK)) = 0.0d0
        CO(NB0(1,3,IJK)) = 0.0d0
        CO(NB0(2,1,IJK)) = 0.0d0
        CO(NB0(2,3,IJK)) = 0.0d0
        CO(NB0(3,1,IJK)) = 0.0d0
        CO(NB0(3,2,IJK)) = 0.0d0
!
        DO 440 LB=1,IPHASE
          TR0(LB) = 0.0d0
          DO 405 L1=1,IPHASE
            TR0(LB) = TR0(LB)+B0INV(L1,LB)*R(NX0(L1,IJK))
  405     CONTINUE 
          R(NX0(LB,IJK)) =-TR0(LB)
! 
          DO 430 LA=1,IPHASE             
            TA0(LA,LB) = 0.0d0
            TC0(LA,LB) = 0.0d0
! 
            IF(IJKMM(ijk).LT.1) GO TO 421
            DO 420 NDM=1,IJKMM(ijk)
              NA0LOC = NDM+ISUMMM(ijk-1)
              DO 410 L1=1,IPHASE
                 TA0(LA,LB) = TA0(LA,LB) &
     &                       +CO(NA0(NA0LOC,L1,LB))*B0INV(LA,L1)
  410         CONTINUE
              CO(NA0(NA0LOC,LA,LB)) =-TA0(LA,LB)         
  420       CONTINUE
! 
  421       IF(IJKPP(ijk).LT.1) GO TO 430
            DO 425 NDM=1,IJKPP(ijk)
              NC0LOC = NDM+ISUMPP(ijk-1)
              DO 422 L1=1,IPHASE
                 TC0(LA,LB) = TC0(LA,LB) &
     &                       +CO(NC0(NC0LOC,L1,LB))*B0INV(LA,L1)
  422         CONTINUE
              CO(NC0(NC0LOC,LA,LB)) =-TC0(LA,LB)         
  425       CONTINUE
! 
  430     CONTINUE
  440   CONTINUE
  500 CONTINUE
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'Z_PreProcess      1.0   16 December  2003',6X, &
     &         'Routine for Z-preprocessing ', &
     &         'of the Jacobian')
 6001 FORMAT(//,20('ERROR-'),//,T40, &
     &             '      S I M U L A T I O N   H A L T E D', &
     &       /,T35,'THE DETERMINANT OF THE MAIN DIAGONAL SUBMATRIX ', &
     &             'IS ZERO',/, &
     &         T31,'Preprocessors ZPROCS = Z4 and OPROCS = O4 cannot ', &
     &             'be used! ',/,      &
     &         T40,'        PLEASE CORRECT AND TRY AGAIN',/,      &
     &       //,20('ERROR-'))
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of Z_PreProcess
!
!
  999 RETURN
!
      END SUBROUTINE Z_PreProcess
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE JSparse_Pattern
! 
! ...... Modules to be used 
! 
         USE Basic_Param
         USE GenControl_Param
         USE Solver_Param
! 
         USE SolMatrix_Arrays
		 USE MPI_PARAM
! 
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*             ROUTINE ESTABLISHING THE SPARSITY PATTERN OF            * 
!*                THE JACOBIAN FOR THE Z-PREPROCESSORS                 * 
!*                                                                     *
!*                  Version 1.00 - December 17, 2003                   *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: iee,IJKR,IMODR,IJKMEL,IPH1,indxx
      INTEGER :: n,izz,IPH2,i8,ijkc,IMODC,IJKCON,nabov,id,jjj,nbelo
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of JSparse_Pattern
!
!
      ICALL = ICALL+1
      IF(ICALL == 1 .and. MPI_RANK==0) WRITE(11,6000)
!
!
!
!***********************************************************************
!*                                                                     *
!*            NUMBERING TO MAP CO TO THE A0,C0,B0,X0 SYSTEM            *
!*                                                                     *
!***********************************************************************
!
!
      DO_NumEq1: DO iee=1,neq*nelal
! 
         IJKR  = iee/neq
         IMODR = MOD(iee,neq)
! 
         IF(IMODR == 0) THEN
            IJKMEL = IJKR
            IPH1   = neq
         ELSE
            IJKMEL = IJKR+1
            IPH1   = IMODR
         END IF
! 
         NX0(IPH1,IJKMEL) = iee
! 
      END DO DO_NumEq1
!
!
!
!***********************************************************************
!*                                                                     *
!*                           INITIALIZATION                            *
!*                     CAREFUL!!! Array operations                     *
!*                                                                     *
!***********************************************************************
!
!
      IJKPP  = 0
      ISUMPP = 0
!
      IJKMM  = 0
      ISUMMM = 0
! 
      NB0 = mnzp1
! 
      NA0 = mnzp1
! 
      NC0 = mnzp1
!
!
!
!***********************************************************************
!*                                                                     *
!*      DETERMINING NEIGBORING ELEMENT SPECIFICS ALONG SAME ROW        *
!*                                                                     *
!***********************************************************************
!
!
      DO_NumEle: DO n=1,nelal     
! 
! -------------
! ...... For elements at left boundaries (X=0, Y=0, or Z=0)
! -------------
! 
         IF_LeftB: IF(no(iijjkk(n)+1) > n) THEN
            IJKPP(n)      = iijjkk(n+1)-iijjkk(n)-1
            jvect(n)      = iijjkk(n)+1      
            jvect(nelal+n) = iijjkk(n+1)-1    
! 
            IJKMM(n)        = 0
            jvect(2*nelal+n) = 0     
            jvect(3*nelal+n) = 0   
            CYCLE
         END IF IF_LeftB
! 
! -------------
! ...... For elements at right boundaries (X=Xmax, Y=Ymax, or Z=Zmax)
! -------------
! 
         IF_RightB: IF(no(iijjkk(n+1)-1) < n) THEN
            IJKPP(n)      = 0
            jvect(n)      = 0      
            jvect(nelal+n) = 0    
! 
            IJKMM(n)        = iijjkk(n+1)-iijjkk(n)-1
            jvect(2*nelal+n) = iijjkk(n)+1    
            jvect(3*nelal+n) = iijjkk(n+1)-1   
            CYCLE
         END IF IF_RightB
! 
! -------------
! ...... For elements not at boundaries
! -------------
! 
         DO_ElRange: DO indxx = iijjkk(n)+1, iijjkk(n+1)-1
! 
            IF_ElRange: IF(n > no(indxx) .AND. n < no(indxx+1)) THEN
               IJKPP(n)      = iijjkk(n+1)-1-indxx
               jvect(n)      = indxx+1    
               jvect(nelal+n) = iijjkk(n+1)-1   
! 
               IJKMM(n)        = indxx-iijjkk(n)
               jvect(2*nelal+n) = iijjkk(n)+1   
               jvect(3*nelal+n) = indxx  
               EXIT
            END IF IF_ElRange
! 
         END DO DO_ElRange
! 
      END DO DO_NumEle
! 
! 
! 
      DO_NumEle1: DO n=1,nelal      
         ISUMPP(n)  = ISUMPP(n-1)+IJKPP(n)
         ISUMMM(n)  = ISUMMM(n-1)+IJKMM(n)
      END DO DO_NumEle1
! 
      IF(ISUMPP(nelal) > NCONUP) THEN
         PRINT 6001, ISUMPP(nela)
         STOP
      END IF
! 
      IF(ISUMMM(nelal) > NCONDN) THEN
         PRINT 6002, ISUMMM(nela)
         STOP
      END IF
!
!
!
!***********************************************************************
!*                                                                     *
!* MAPPING THE GLOBAL MATRIX ELEMENT NUMBER TO CELL-SPECIFIC POINTERS  *
!*                                                                     *
!***********************************************************************
!
!
      DO 400 izz=1,nz
         IJKR  = irn(izz)/neq
         IMODR = MOD(irn(izz),neq)
         IF(IMODR == 0) THEN
            IJKMEL = IJKR
            IPH2   = neq
         ELSE
            IJKMEL = IJKR+1
            IPH2   = IMODR
         END IF
         i8 = ijkmel
! 
! 
! 
         IJKC  = icn(izz)/neq
         IMODC = MOD(icn(izz),neq)
         IF(IMODC == 0) THEN
            IJKCON = IJKC
            IPH1   = neq
         ELSE
            IJKCON = IJKC+1
            IPH1   = IMODC
         END IF
! 
! -------------
! ...... For elements on the main diagonal submatrix
! -------------
! 
         IF_Pointer: IF(IJKMEL == IJKCON) THEN
            NB0(IPH1,IPH2,i8) = izz
         ELSE
! 
! ----------------
! ......... For elements to the right of the main diagonal submatrix
! ----------------
! 
            IF_RelLoc: IF(i8 < IJKCON) THEN
! 
               IF(jvect(i8) == 0) CYCLE 
               nabov = 0
! 
               DO_RightE: DO indxx = jvect(i8),jvect(nelal+i8)
                  id    = no(indxx)
                  nabov = nabov+1
                  IF(id == ijkcon) THEN
                     jjj = nabov
                     EXIT
                  END IF
               END DO DO_RightE
! 
               NC0(jjj+ISUMPP(i8-1),IPH1,IPH2) = izz
! 
! ----------------
! ......... For elements to the left of the main diagonal submatrix
! ----------------
! 
            ELSE 
! 
               IF(jvect(2*nelal+i8) == 0) CYCLE 
               nbelo = 0
! 
               DO_LeftE: DO indxx = jvect(2*nelal+i8),jvect(3*nelal+i8)
                  id    = no(indxx)
                  nbelo = nbelo+1
                  IF(id == ijkcon) THEN
                     jjj = nbelo
                     EXIT
                  END IF
               END DO DO_LeftE
! 
               NA0(jjj+ISUMMM(i8-1),IPH1,IPH2) = izz
! 
            END IF IF_RelLoc
! 
         END IF IF_Pointer
!
!
! 
  400 CONTINUE
! 
! ----------
! ... Initialization - CAREFUL! Whole array operations
! ----------
! 
      jvect(1:leniw) = 0       
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'JSparse_Pattern   1.0   17 December  2003',6X, &
     &         'Establish the sparsity pattern ', &
     &         'of the Jacobian for the Z preprocessors')
! 
 6001 FORMAT(//,20('ERROR-'),//,T33, &
     &             '       S I M U L A T I O N   A B O R T E D', &
     &       /,T24,'THE FIRST DIMENSION OF THE isumpp ARRAY ', &
     &             'IS INSUFFICIENT FOR THIS PROBLEM', &
     &       /,T12,'THE PARAMETER nconup MUST BE AT LEAST ',I10, &
     &       //,T33,'               CORRECT AND TRY AGAIN',      &
     &       //,20('ERROR-'))         
 6002 FORMAT(//,20('ERROR-'),//,T33, &
     &             '       S I M U L A T I O N   A B O R T E D', &
     &       /,T24,'THE FIRST DIMENSION OF THE isummm ARRAY ', &
     &             'IS INSUFFICIENT FOR THIS PROBLEM', &
     &       /,T12,'THE PARAMETER ncondn MUST BE AT LEAST ',I10, &
     &       //,T33,'               CORRECT AND TRY AGAIN',      &
     &       //,20('ERROR-'))         
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of JSparse_Pattern
!
!
  999 RETURN
! 
      END SUBROUTINE JSparse_Pattern
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE INDEX_Neighbor
! 
! ...... Modules to be used 
! 
         USE Basic_Param
         USE GenControl_Param
! 
         USE Connection_Arrays
         USE Variable_Arrays
         USE SolMatrix_Arrays
		 USE MPI_PARAM
!
!*********************************************************************
!*********************************************************************
!*                                                                   *
!*            DETERMINE THE POSITIONING ARRAYS ia AND ja             *     
!*          WHICH IDENTIFY THE LOCATIONS OF THE NEIGHBORS            *     
!*                                                                   *
!*                 Version 1.00 - December 9, 2003                   *     
!*                                                                   *
!*********************************************************************
!*********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: n,n1,n2,mshift,isum,ii,inx1,inx2
      INTEGER :: itemp,istart,numsrt,i5
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of INDEX_Neighbor
!
!
      ICALL = ICALL+1
      IF(ICALL == 1 .and. MPI_RANK==0) WRITE(11,6000)
!
!***********************************************************************
!*                                                                     *
!*                   I N I T I A L I Z A T I O N S                     *
!*                                                                     *
!***********************************************************************
!
      iijjkk(1:NTL)                   = 1   ! CAREFUL - Whole array operations    
!
      no(1:NCONT+NTL+1) = 0       
!
      jvect(1:leniw)                  = 0       
!
!***********************************************************************
!*                                                                     *
!*       DETERMINE NUMBER OF NEIGHBORING ELEMENTS AND STORE IN iw      *
!*                                                                     *
!***********************************************************************
!
      DO_NumCon1: DO n = 1,NCONT
! 	 
         n1 = conx(n)%n1         
         n2 = conx(n)%n2        
! 	 
         IF(n1 == 0 .OR. n2 == 0) CYCLE
! 	 
         IF(n1.LE.nelal) iijjkk(n1) = iijjkk(n1)+1         
         IF(n2.LE.nelal) iijjkk(n2) = iijjkk(n2)+1         
! 	 
      END DO DO_NumCon1
!
!***********************************************************************
!*                                                                     *
!*     STORE TEMPORARILY THE # OF NEIGHBORS PER ELEMENT ii IN jvect    *
!*                    (FIRST nela ELEMENTS OF jvect)                   *
!*                                                                     *
!***********************************************************************
!
      jvect(1:NTL) = iijjkk(1:NTL)
!
!***********************************************************************
!*                                                                     *
!*        DETERMINE THE CUMULATIVE NUMBER OF NEIGBORING ELEMENTS       *
!*       STORE IN jvect - FROM jvect(mshift) TO jvect(mshift+nel)      *
!*                                                                     *
!***********************************************************************
!
      mshift     = NTL+1
      no(mshift) = 0
! 
      isum = 0
      DO_NumEle1: DO ii = 1,NTL
         isum             = isum+iijjkk(ii)
         jvect(mshift+ii) = isum
      END DO DO_NumEle1
!
!***********************************************************************
!*                                                                     *
!*     DETERMINE THE iw AND ja ARRAYS WITH THE NEIGHBOR INFORMATION    *
!*                                                                     *
!***********************************************************************
!
      DO_NumEle2: DO ii = 1,NTL
         iijjkk(ii)               = 1 
         no(jvect(mshift+ii-1)+1) = ii
      END DO DO_NumEle2
!
!
!
      DO_NumCon2: DO n = 1,ncont
!
         n1 = conx(n)%n1       
         n2 = conx(n)%n2         
!
         IF(n1 > nelal .AND. n2 > nelal) CYCLE		!Both either inactive or update
         IF(n1 == 0 .OR. n2 == 0)      CYCLE
!
         iijjkk(n1) = iijjkk(n1)+1         
         inx1       = jvect(mshift+n1-1)+iijjkk(n1)
         no(inx1)   = n2
!
         iijjkk(n2) = iijjkk(n2)+1
         inx2       = jvect(mshift+n2-1)+iijjkk(n2)
         no(inx2)   = n1
!
      END DO DO_NumCon2
!
!
!
      iijjkk(1) = 1
      no(1)     = 1
      DO_NumEle3: DO ii = 1,NTL
!
         itemp            = 0
         iijjkk(ii+1)     = iijjkk(1)+jvect(mshift+ii)
         no(iijjkk(ii+1)) = ii+1
!  
! ------------
! ...... Sort the subarrays ja(ii), ii=iijjkk(ii),...,iijjkk(ii+1)-1
! ------------
!  
         istart = iijjkk(ii)
         numsrt = iijjkk(ii+1)-iijjkk(ii)
!  
         CALL SORT(no(istart),numsrt)
!  
! ------------
! ...... Put the diagonal element first,  
! ...... swap with element in sorted subarray 
! ------------
!  
         DO_Inner: DO i5 = iijjkk(ii),iijjkk(ii+1)-1
            IF(no(i5).EQ.ii) THEN
               itemp = i5
               EXIT
            END IF
         END DO DO_Inner
!  
         no(itemp)      = no(iijjkk(ii))
         no(iijjkk(ii)) = ii
!  
      END DO DO_NumEle3
!
! ---------
! ... Reinitialization - CAREFUL! Array operations
! ---------
!
      jvect(ii) = 0       
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'INDEX_Neighbor    1.00   9 December  2003',6X, &
     &         'Routine for neighbor element indexing')
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of INDEX_Neighbor
!
!
      RETURN
!
      END SUBROUTINE INDEX_Neighbor
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE SORT(iar,n)
!
!***********************************************************************
!*                                                                     *
!*             SORTING THE VECTOR iar IN ASCENDING ORDER               *     
!*                  Version 1.00 - January 14, 1998                    *     
!*                                                                     *
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: k,j,m,n,i5,maxd,iert,itemp
! 
! -------
! ... Integer arrays
! -------
! 
      INTEGER, DIMENSION(n) :: IAR
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of SORT
!
!
      m = n
! 
      DO_Outer: DO i5 = 1,n
! 
         m = m/2
         IF(m == 0) EXIT DO_Outer
! 
         maxd = n-m
! 
         DO_jLoop: DO j = 1,maxd
! 
            DO_kLoop: DO k = j,1,-m
! 
               iert = iar(k+m)
               IF(iert >= iar(k)) EXIT DO_kLoop
! 
               itemp    = iar(k+m)
               iar(k+m) = iar(k)
               iar(k)   = itemp
! 
            END DO DO_kLoop
! 
         END DO DO_jLoop
! 
      END DO DO_Outer
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of SORT
!
!
      RETURN 
!
      END SUBROUTINE SORT
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE WRITE_FileSAVE
! 
! ...... Modules to be used 
! 
         USE EOS_DefaultParam, ONLY: EOS_state
         USE Basic_Param
         USE GenControl_Param
! 
         USE Element_Arrays
         USE Variable_Arrays
         USE PFMedProp_Arrays
		 USE MPI_PARAM
		 USE MPI_ARRAYS
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*     ROUTINE GENERATING A FILE \D2SAVE\D3 TO BE USED FOR RESTARTING      *
!*                                                                     *
!*                   Version 1.0 - January 5, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0,ierr
      INTEGER :: nn
! 
      INTEGER :: n,i,j
! 
      SAVE ICALL
	  CHARACTER(LEN=117) :: buff
	  
	  INTEGER (KIND=MPI_OFFSET_KIND)::endoff
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of WRITE_FileSAVE
!
!
      ICALL = ICALL+1
      IF(ICALL == 1 .and. MPI_RANK==0) WRITE(11,6000)
! 
      if(MPI_RANK==0) PRINT 6001, KCYC,SUMTIM      ! Write time and timestep into main output file
! 
! -------
! ... Associate and print data into file SAVE
! -------

! 
! -------
! ... Print primary variables into file SAVE
! -------
! 
      DO_NumEle: DO i=1,NEL
		 if( (procnum(i) == MPI_RANK .and. i<=NELA) .OR. &
						(i>NELA .and. MPI_RANK==0)) THEN
			 n = locnum(i)
			 nn = elem(n)%StateIndex
			 WRITE(buff,6003)  elem(n)%name, elem(n)%phi,  &
		 &                  EOS_state(nn),NEW_LINE(NLINE),(X((n-1)*NK1+j),j=1,NK1),NEW_LINE(NLINE)
			
			call MPI_FILE_WRITE(SAVE_file,buff,1,SAVE_linetype,MPI_STATUS_IGNORE,ierr)
		 end if
      END DO DO_NumEle
! 
! -------
! ... Print continuation data into file SAVE
! -------
! 

	!To write heading, set new view with zero displacement and MPI_CHARACTER file type
	!NOTE: This HAS to be collective operation
	call MPI_FILE_SET_VIEW(SAVE_file,ZeroOffset,MPI_CHARACTER,MPI_CHARACTER,'internal',MPI_INFO_NULL,ierr)
	
	if(MPI_RANK==0) THEN
		if(ierr/=0) then
			print *, 'SOME ERROR OCCURED WHILE WRITING FILE SAVE at 561ZZA'
		end if
		
		WRITE(buff,6002) NEL,SUMTIM,NEW_LINE(NLINE)
		call MPI_FILE_WRITE(SAVE_file,buff,68,MPI_CHARACTER,MPI_STATUS_IGNORE,ierr)     ! Print headings
		
		endoff = (68+NEL*117)
		!To print footers, seek the pointer
		call MPI_FILE_SEEK(SAVE_file,endoff,MPI_SEEK_SET,ierr)
		WRITE(buff,6004),NEW_LINE(NLINE)                                     ! Print continuation keyword \D4+++\D5
		call MPI_FILE_WRITE(SAVE_file,buff,6,MPI_CHARACTER,MPI_STATUS_IGNORE,ierr)     ! Print headings
	! 
		WRITE(buff,6005),KCYC,ITERC,N_PFmedia,TSTART,SUMTIM,NEW_LINE(NLINE)     
		call MPI_FILE_WRITE(SAVE_file,buff,46,MPI_CHARACTER,MPI_STATUS_IGNORE,ierr)  ! Print time, timestep and iteration data
	! 
	!
	 end if
	 call MPI_TYPE_FREE(SAVE_linetype,ierr)
	 call MPI_FILE_CLOSE(SAVE_file,ierr)
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'WRITE_FileSAVE    1.0    9 January   2004',6X, &
     &         'At the completion of a TOUGH-Fx run, write primary ', &
     &         'variables into file "SAVE"')
!
 6001 FORMAT(/,' Write file "SAVE" after',i5,' time steps  --->  The', &
     &         ' time is ',1pe13.6,' seconds'/)
 6002 FORMAT('INCON -- INITIAL CONDITIONS FOR',I5, &
     &       ' ELEMENTS AT TIME',1pE14.7,A1)
!
 6003 FORMAT(A5,10X,1pE15.8,2x,a3,A1,4(1pE20.13),A1)
 6004 FORMAT('+++  ',1A1)
 6005 FORMAT(3I5,2(1pE15.8),1A1)
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of WRITE_FileSAVE
!
!
      RETURN
!C
      END SUBROUTINE WRITE_FileSAVE
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE OUT_DifusFlow
! 
! ...... Modules to be used 
! 
         USE Basic_Param
         USE GenControl_Param
         USE Diffusion_Param
! 
         USE Element_Arrays
         USE Connection_Arrays
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*       ROUTINE GENERATING A PRINTOUT OF DIFFUSIVE FLOW RATES         *
!*                                                                     *
!*                    Version 1.0 - June 22, 2003                      *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: i,j,n,kk,np
! 
! -------
! ... Character arrays
! -------
! 
      CHARACTER(LEN = 3), PARAMETER,  &
     &      DIMENSION(5) :: IJ = (/'-1-','-2-','-3-','-4-','-5-'/)
! 
! -------
! ... Character parameters
! -------
! 
      CHARACTER(LEN = 3), PARAMETER  :: ijnph = 'all'
! 
      CHARACTER(LEN = 12), PARAMETER :: PHAC  = '  PHASE COMP'
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of OUT_DifusFlux
!
!
      ICALL = ICALL+1
      IF(ICALL ==1) WRITE(11,6000)
! 
! -------
! ... Print a short header
! -------
! 
      PRINT 6002, ' ',TITLE,KCYC,ITER,SUMTIM
! 
! 
!***********************************************************************
!*                                                                     *
!*               For MOP(24) = 1, print diffusive fluxes               *
!*                       (Phases and components)                       *
!*                                                                     *
!***********************************************************************
! 
! 
      IF_DifPrint: IF(mop(24) == 1) THEN
! 
! ----------
! ...... Print table caption
! ----------
! 
         PRINT 6004, ' ',((PHAC,i=1,NK),j=1,NPH)
         PRINT 6005, ((IJ(j),IJ(i),i=1,NK),j=1,NPH)
! 
! ----------
! ...... Print diffusive fluxes (components and phases)
! ----------
! 
         DO_NumCon1: DO n=1,NCON
!
            IF_ActCon1: IF(conx(n)%n1 /= 0 .AND. conx(n)%n2 /= 0) THEN
!
               PRINT 6006,   conx(n)%nam1,conx(n)%nam2, &
     &                     ((FDIF(II(kk,np,n)),kk=1,NK),np=1,NPH)
!   
            END IF IF_ActCon1
!
         END DO DO_NumCon1
!
         PRINT 6010
! 
! 
!***********************************************************************
!*                                                                     *
!*      For MOP(24) = 0, print diffusive fluxes (components only)      *
!*                                                                     *
!***********************************************************************
! 
! 
      ELSE IF(mop(24) == 0) THEN
! 
! ----------
! ...... Print table caption
! ----------
! 
         PRINT 6004, ' ',(PHAC,i=1,NK)
         PRINT 6005, (IJnph,IJ(i),i=1,NK)
! 
! ----------
! ...... Print diffusive fluxes (components only)
! ----------
! 
         DO_NumCon2: DO n=1,NCON
            IF_ActCon2: IF(conx(n)%n1 /= 0 .AND. conx(n)%n2 /= 0) THEN
!
               PRINT 6006,  conx(n)%nam1,conx(n)%nam2, &
     &                     (FDIF(II(kk,NPH,n)),kk=1,NK)
!   
            END IF IF_ActCon2
!
         END DO DO_NumCon2
!
         PRINT 6010
!
!
!
      END IF IF_DifPrint
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'OUT_DifusFlow     1.0   22 June      2003',6X, &
     &         'Print out interblock diffusive flow rates')
!
 6002 FORMAT(A1,/,10X,A80,//,80X,'KCYC =',I5,'  -  ITER =',I5, &
     &       '  -  TIME =',1pE13.6,/, &
     &       ' MASS FLOW RATES (KG/S) FROM DIFFUSION'/)
!
 6004 FORMAT(A1,'ELEM1 ELEM2',10A12)
 6005 FORMAT(12X,10('   ',A3,2X,A3,' ')/)
!
 6006 FORMAT(1X,A5,1X,A5,(10(1X,1pE12.5)))
 6010 FORMAT(' ',131('@'))
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of OUT_DifusFlow
!
!
      RETURN
!
!
!***********************************************************************
!*                                                                     *
!*                        INTERNAL PROCEDURES                          *
!*                                                                     *
!***********************************************************************
!
!
      CONTAINS
! 
! ----------
! ...... INTEGER function II
! ----------
! 
         INTEGER(KIND = 4) FUNCTION II(IKK,INP,IN)
! 
            INTEGER(KIND = 4) :: IKK,INP,IN  
!
            II = (IN-1)*NPH*NK+(INP-1)*NK+IKK
!
         END FUNCTION II
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of the II Internal Function 
! 
! 
      END SUBROUTINE OUT_DifusFlow
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE HeatExch_An
! 
! ... Modules to be used 
! 
      USE Basic_Param
      USE GenControl_Param
!
      USE Element_Arrays
      USE Connection_Arrays
! 
      USE Variable_Arrays
      USE SolMatrix_Arrays,  ONLY : CO
! 
      USE PFMedProp_Arrays
      USE TempStoFlo_Arrays
	  USE MPI_PARAM
	  USE MPI_ARRAYS
!
!*********************************************************************
!*********************************************************************
!*                                                                   *
!*    COMPUTES A SEMI-ANALYTICAL APPROXIMATION FOR HEAT EXCHANGE     *     
!*      WITH CONFINING LAYERS WITH UNIFORM INITIAL TEMPERATURE       *
!*                                                                   *
!*   Uses the method of Vinsome & Westerveld, J. Can. Petr. Tech.,   *     
!*                  July-September 1980, pp. 87-90                   *     
!*                                                                   *
!*                 Version 1.00, September 02, 2003                  *     
!*                                                                   *
!*********************************************************************
!*********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: HTL,HTLC,T00,DIF,D_i,DIFDT
      REAL(KIND = 8) :: TCUR,THETA,THETAK,PNUM,PDEN,PP,QNK1,QQ,FLOH
      REAL(KIND = 8) :: DPPDT,DQK1DT
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0, IERR
      INTEGER :: N,n_m,NLOC,NLK1,N1KL,i,j
! 
      SAVE ICALL,n_m,T00,DIF
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of HeatExch_An
!
!
      ICALL = ICALL+1
!
!
!***********************************************************************
!*                                                                     *
!*       In the 1st call of QLOSS, compute the thermal diffusivity     *
!*                                                                     *
!***********************************************************************
!
!
      IF_1stCall: IF(ICALL == 1) THEN
!
         IF(MPI_RANK==0) WRITE(11,6000)
!
         HTL  = 0.0d0
         HTLC = 0.0d0
! 
! ----------
! ...... Whole array operation: CAREFUL !
! 
! ...... Array 'AI' holds one parameter per element which characterizes the
! ......   temperature distribution in the confining layer. On initialization
! ......   all AI=0. At the end of a run the parameters 'AI' are written onto
! ......   the disk file 'TABLE' (needed for a restart)
! ----------
! 
         AI(1:NELAL) = 0.0d0
!
! ...... Read from files
!
         REWIND 8
		 
		 J=1
		 DO I=1,NELA
			READ(8,5001) AI(J)
			IF(procnum(I)==MPI_RANK) J=J+1
		 END DO
! 
! ----------
! ...... Assign initial temperature and diffusivity of confining layers
! ----------
! 
         T00 = elem(NELL)%T
         n_m = elem(NELL)%MatNo
		 
		 call MPI_BCAST(T00,1,MPI_REAL8,procnum(NEL),MPI_COMM_WORLD,ierr)
		 call MPI_BCAST(n_m,1,MPI_INT,procnum(NEL),MPI_COMM_WORLD,ierr)
! 
         DIF = media(n_m)%KThrW/( media(n_m)%DensG &
     &                           *media(n_m)%SpcHt &
     &                          )
!
         PRINT 6002, T00,media(n_m)%KThrW, &
     &                   media(n_m)%DensG, &
     &                   media(n_m)%SpcHt
!
      END IF IF_1stCall
! 
! ----------
! ...... Some print-out for MOP(15)>2 ...
! ----------
! 
      IF(MOP(15) >= 2 .and. MPI_RANK==0) PRINT 6005, KCYC,ITER
! 
! -------
! ... Assign conduction distance
! -------
! 
      HTL   = 0.0d0
      D_i   = SQRT(DIF*(SUMTIM+DELTEX))/2.0d0
      DIFDT = DIF*DELTEX
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                           ELEMENT LOOP                              >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_NumEle: DO n=1,NELAL
!
!
!
         IF_AHTPos: IF(AHT(n) /= 0.0d0) THEN
!
! ......... Determine locations in the X array
!
            NLOC = (n-1)*NEQ
            NLK1 = NLOC+NK1
!
! ...... Compute intermediate variables
!
            TCUR   = Cell_V(n,0)%TemC 
!      
            THETA  = TCUR-T00
            THETAK = elem(n)%T-T00
            PNUM   = DIFDT*THETA/D_i-(THETA-THETAK)*D_i*D_i*D_i/DIFDT
!
            IF(KCYC >= 2) PNUM = PNUM+AI(n)
!
            PDEN = 3.0d0*D_i*D_i+DIFDT
            PP   = PNUM/PDEN
!
            QNK1 = media(n_m)%KThrW*(THETA/D_i-PP)*DELTEX
!
            QQ   = ( (THETA-THETAK)/DIFDT &
     &              - THETA/(D_i*D_i) + 2.0d0*PP/D_i &
     &             )/2.0d0
!
            IF_Converge: IF(Converge_Flag .EQV. .TRUE.) THEN 
               AI(N) = THETA*D_i+PP*D_i*D_i+2.0d0*QQ*D_i*D_i*D_i
               FLOH  = -QNK1*AHT(n)/DELTEX
            ELSE
!
               FLOH    = -QNK1*AHT(n)/DELTEX
               R(NLK1) = R(NLK1)+QNK1*AHT(n)/elem(n)%vol
!
! ............ Determine locations of matrix elements corresponding
! ............ to the energy equation in the Jacobian 
!
               N1KL = (n-1)*NEQ*NEQ+NEQ*NK
!
! ............ Compute derivative of lateral heat flux 
! ............ with respect to temperature 
!
               DPPDT  = (DIFDT/D_i-D_i*D_i*D_i/DIFDT) &
     &                 /(3.0d0*D_i*D_i+DIFDT)
               DQK1DT = media(n_m)%KThrW*DELTEX*(1.0d0/D_i-DPPDT)
!
! ............ Adjust the Jacobian matrix element CO  
!
               CO(N1KL+nk1) = CO(N1KL+nk1)-DQK1DT*AHT(n)/elem(n)%vol
!
            END IF IF_Converge
!
! ......... Some print-out for MOP(15)>2 ...
!
            IF(MOP(15) >= 2) THEN
               PRINT 6010, elem(n)%name,FLOH,QNK1,AI(n),TCUR,DQK1DT
            END IF 
!
            HTL = HTL-FLOH
!
!
!
         END IF IF_AHTPos
!
! <<<                      
! <<< End of the ELEMENT LOOP         
! <<<
!
      END DO DO_NumEle
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 5001 FORMAT(1E20.13)
!
 6000 FORMAT(/,'HeatExch_An       1.0    2 September 2003',6X, &
     &         'Perform semi-analytical calculation for heat ', &
     &         'exchange with confining beds')
 6002 FORMAT(//,' ',131('='),/, &
     &       ' PERFORM SEMI-ANALYTICAL HEAT EXCHANGECALCULATION',/, &
     &       '      THERMAL PARAMETERS ARE:',/, &
     &       '      TEMPERATURE = ',1pE12.5, &
     &       '   HEAT CONDUCTIVITY = ',1pE12.5, &
     &       '   DENSITY = ',1pE12.5,'   SPECIFIC HEAT = ',1pE12.5,/, &
     &       '      DIFFUSIVITY = ',1pE12.5/' ',131('='),//)
 6005 FORMAT('SUBROUTINE QLOSS   -----   (KCYC,ITER) = (', &
     &       I4,',',I3,')')
 6010 FORMAT(' ELEMENT *',A8,'*   ---   FLOH= ',1pE12.5, &
     &       '   QNK1= ',1pE12.5,'   AI(N)= ',1pE12.5, &
     &       '   TCUR= ',1pE12.5,'   DQDT= ',1pE12.5)
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of HeatExch_An
!
!
      RETURN
!
      END SUBROUTINE HeatExch_An
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE Q_SourceSink
! 
! ...... Modules to be used 
! 
         USE Basic_Param
         USE GenControl_Param
         USE Solver_Param
         USE Hydrate_Param, ONLY: Inh_WMasF
! 
         USE Element_Arrays
         USE Variable_Arrays
         USE SolMatrix_Arrays
         USE TempStoFlo_Arrays
         USE Q_Arrays
		 USE MPI_PARAM
		 USE MPI_ARRAYS
! 
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*     ROUTINE COMPUTING ALL TERMS ARISING FROM SINKS AND SOURCES      * 
!*                                                                     *
!*                  Version 1.00 - November 18, 2003                   *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: FAC,Gn,EGn
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: n,j,k,m,lnk,jkm,np
      INTEGER :: JLOC,JLOCP,MN,JMN,JNK1,J2LOC,LTABA
! 
! -------
! ... Character variables
! -------
! 
      CHARACTER(LEN = 1) :: ITABA
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of Q_SourceSink
!
!
      ICALL = ICALL+1
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)
!
      IF(MOP(4) >= 1 .and. mpi_rank==0) PRINT 6002, KCYC,ITER   ! Print additional info
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                         SOURCE/SINK LOOP                            >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_NumSS: DO n=1,NOGN
!
		 if(procnum(SS(n)%el_num) /=MPI_RANK) CYCLE
         j = locnum(SS(n)%el_num)
		 
         IF(j == 0 .OR. j > NELAL) GO TO 1100
         FAC = FORD/elem(j)%vol
!
         JLOC  = (j-1)*NEQ
         JLOCP = (j-1)*NK1
!
         MN   = SS(n)%typ_indx
         JMN  = JLOC+MN
         JNK1 = JLOC+NK1
!
         LTABA = ABS(SS(n)%n_TableP)
         ITABA = SS(n)%type(5:5)
! 
! 
!***********************************************************************
!*                                                                     *
!*             PRODUCTION OR INJECTION OF A MASS COMPONENT             *
!*  THE EFFECTS OF HEAT OF INJECTED/PRODUCED COMPONENT ARE CONSIDERED  *
!*                                                                     *
!***********************************************************************
! 
! 
         IF_MassC: IF(mn <= nk) THEN     ! Mass components only
! 
! 
! >>>>>>>>>>>>>>>>>>
! .........
! ......... Injection/production at a prescribed fixed rate (LTABA <= 1) 
! ......... 
! >>>>>>>>>>>>>>>>>>
! 
! 
            IF_QFixed: IF(ltaba <= 1) THEN
!
               Gn = SS(n)%rate_m
! 
! -------------------
! ............ Mass injection (GN > 0), or 
! ............    production (GN < 0) with prescribed enthalpy
! -------------------
! 
               IF_Enth1: IF(Gn >= 0.0d0 .OR.  &
     &                     (GN <  0.0d0 .AND. itaba /= ' '))  &
     &         THEN
! 
! ............... Provision for inhibitor injected with the water stream
! 
                  IF(EOS_name(1:7) == 'HYDRATE' .AND.  &
     &               Inh_WMasF(n) > 0.0d0 .AND. Gn >= 0.0d0)  &
     &            THEN
                     R(jmn)   = R(jmn)-fac*(1.0d0-Inh_WMasF(n))*Gn    ! Water
                     R(jmn+2) = R(jmn+2)-fac*Inh_WMasF(n)*Gn          ! Inhibitor
! 
! ............... Standard case
! 
                  ELSE
                     R(jmn) = R(jmn)-fac*Gn
                  END IF
! 
                  IF(neq == nk1) R(JNK1) = R(JNK1) &
     &                                    -FAC*SS(n)%rate_m*SS(n)%enth
! 
                  GO TO 1100
! 
! -------------------
! ............ Mass production (GN < 0), with enthalpy to be determined 
! ............    from conditions in producing block
! -------------------
! 
               ELSE
! 
                  CALL PhaseXH_Source(n,j,gn,fac) ! Calculate phase composition and flowing ...
!                                                 ! ... enthalpy of multiphase source fluid
! 
                  GO TO 1000                      ! Go to modify the Jacobian
!
               END IF IF_Enth1
! 
! 
! >>>>>>>>>>>>>>>>>>
! .........
! ......... Time-dependent injection/production at a rate determined from  
! ......... tabular data of time, and corresponding rate and enthalpy
! ......... 
! >>>>>>>>>>>>>>>>>>
! 
! 
            ELSE
!
               CALL QTable_Interp(n,ltaba,itaba,Gn,EGn)  ! Determine Gn,Egn from table
               IF(igood /= 0) RETURN                     ! Exit the routine if a ...
!                                                        ! ... problem is encountered
               SS(n)%rate_m = Gn
! 
! -------------------
! ............ Mass injection (GN > 0), or 
! ............    production (GN < 0) with prescribed enthalpy
! ----------------
! 
               IF_Enth2: IF(itaba /= ' ') THEN
! 
                  SS(n)%enth = egn
                  r(jmn) = r(jmn)-fac*gn
                  IF(neq == nk1) R(JNK1) = R(JNK1) &
     &                                    -FAC*SS(n)%rate_m*SS(n)%enth
! 
                  GO TO 1100
! 
               ELSE
! 
                  CALL PhaseXH_Source(n,j2loc,gn,fac)  ! Calculate phase composition and flowing ...
!                                                      ! ... enthalpy of multiphase source fluid
! 
                  GO TO 1000                           ! Go to modify the Jacobian
!
               END IF IF_Enth2
!                          
! <<<<<<<<<                          
! <<<<<<<<< End of the Fixed Rate IF         
! <<<<<<<<<   
!                          
            END IF IF_QFixed
!
! <<<<<<                          
! <<<<<< End of the Mass Component IF        
! <<<<<<    
!
         END IF IF_MassC
! 
! 
!***********************************************************************
!*                                                                     *
!*                 DIRECT HEAT INJECTION/PRODUCTION                    *
!*                                                                     *
!***********************************************************************
! 
! 
         IF_Heat: IF(mn == nk1) THEN
! 
! 
! >>>>>>>>>>>>>>>>>>
! .........
! ......... Heat injection/production at a prescribed fixed rate 
! ......... 
! >>>>>>>>>>>>>>>>>>
! 
! 
            IF(neq == nk1) THEN                   ! Ignore heat sinks/sources when not solving ...
!                                                 ! ... the energy equation
               r(jmn) = r(jmn)-fac*SS(n)%rate_m   ! Adjusting the RHS
! 
            END IF
!
            GO TO 1100
!
! <<<<<<                          
! <<<<<< End of the Mass Component IF        
! <<<<<<    
!
         END IF IF_Heat
! 
! 
!***********************************************************************
!*                                                                     *
!*                           OTHER SCENARIOS                           *
!*                                                                     *
!***********************************************************************
! 
! 
         lnk = mn-nk1
!
!
!
         CASE_SSOption: SELECT CASE(lnk)
! 
! >>>>>>>>>>>>>
! >>>>>>>>>>>>>
! ......
! ...... Deliverability option: Production well with specified downhole pressure      
! ......
! >>>>>>>>>>>>>
! >>>>>>>>>>>>>
! 
         CASE (1)
!
! ......... Optionally perform simple gravity correction for flowing
! ......... downhole pressure of multi-feed-zone wells 
!
            IF(iter == 1 .AND. ltaba > 1) CALL GRAVITY_correct(n,ltaba)
!
! ......... Obtain source rate, phase composition, flowing enthalpy
!
            CALL PhaseXH_WellDelv(n,jlocp,j,fac)
!
!            Gpo(n) = G(n)
!
            GO TO 1000
! 
! >>>>>>>>>>>>>
! >>>>>>>>>>>>>
! ......
! ...... Production well with specified wellhead pressure, and      
! ...... flowing wellbore pressure correction       
! ......
! >>>>>>>>>>>>>
! >>>>>>>>>>>>>
! 
         CASE (2)
!
! ......... On 1st call, initialize starting guess for well rate
!
            IF(icall == 1) SS(n)%rate_res = 10.0d0
!
! ......... Obtain source rate, phase composition, flowing enthalpy
!
            CALL PXH_WelHedP(n,jlocp,j,fac)
!
!            Gpo(n) = G(n)
!
            GO TO 1000
! 
! >>>>>>>>>>>>>
! >>>>>>>>>>>>>
! ......
! ...... DEFAULT
! ......
! >>>>>>>>>>>>>
! >>>>>>>>>>>>>
! 
         CASE DEFAULT 
!
            CONTINUE
!
         END SELECT CASE_SSOption
! 
! 
!***********************************************************************
!*                                                                     *
!*                MODIFY THE JACOBIAN MATRIX EQUATION                  *
!*                                                                     *
!***********************************************************************
! 
! 
 1000    DO_NeqOuter: DO k=1,NEQ
!
            R(JLOC+k) = R(JLOC+k)+D(k,1)                 ! Adjusting the RHS
!
            DO_NeqInner: DO m=1,NEQ
               jkm     = (j-1)*NEQ*NEQ+(k-1)*NEQ+m       ! Locating the CO index
               CO(jkm) = CO(jkm)   &                       ! Adjusting CO
     &                  -(D(k,m+1)-D(k,1))/DELX(JLOCP+m)
            END DO DO_NeqInner
!
         END DO DO_NeqOuter
! 
! -------
! ... Print supporting information (when MOP(4) >=2 only)
! -------
! 
 1100    IF(MOP(4) >= 2) PRINT 6004, SS(n)%name_el,SS(n)%name, &
     &                               SS(n)%rate_m,SS(n)%enth, &
     &                              (SS(n)%frac_flo(np),np=1,Num_MobPhs)
!
! <<<                      
! <<<                      
! <<< End of the SOURCE/SINK LOOP         
! <<<
! <<<                      
!
      END DO DO_NumSS
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'Q_SourceSink      1.0   18 November  2003',6X, &
     &         'Assembling all source and sink terms',/,47X, &
     &         '"Rigorous" step rate capability for MOP(12) = 2,',/,47X, &
     &         'and capability for flowing wellbore pressure ', &
     &         'corrections')
 6002 FORMAT(/,' ==============> SUBROUTINE QU <==============', &
     &         ' --- [KCYC,ITER] = [',I4,',',I3,']')
 6004 FORMAT(' ELEMENT ',A8,'   SOURCE ',A5, &
     &       '   ---   FLOW RATE = ',1pE13.6, &
     &       '   SPECIFIC ENTHALPY = ',1pE13.6,/, &
     &       ' FNP1 = ',1pE13.6,'   FNP2 = ',1pE13.6, &
     &       '   FNP3 = ',1pE13.6,'   FNP4 = ',1pE13.6)
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of Q_SourceSink
!
!
      RETURN
! 
!
      END SUBROUTINE Q_SourceSink
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE QTable_Interp(n,ltaba,itaba,Gx,EGx)
! 
! ... Modules to be used 
! 
      USE Basic_Param
      USE GenControl_Param
! 
      USE Q_Arrays
	  USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*       ROUTINE INTERPOLATING SINK/SOURCE RATES AND ENTHALPIES        *
!*                        FROM TABULAR DATA                            *
!*                                                                     *
!*                   Version 1.0 - January 4, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(OUT) :: Gx,EGx
! 
      REAL(KIND = 8) :: t_i,t_f,V_dn,V_up,Q1,Q2,qdt,hqdt
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: n,ltaba
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: N_up,i_Lo,i_Hi,i_Lop,i_Him,m
! 
! -------
! ... Character variables
! -------
! 
      CHARACTER(LEN = 1), INTENT(IN) :: itaba
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of QTable_Interp
!
!
      ICALL = ICALL+1
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)
! 
! 
!***********************************************************************
!*                                                                     *
!*                           INITIALIZATIONS                           *
!*                                                                     *
!***********************************************************************
! 
! 
      t_i  = sumtim                       ! Time at beginning of time step
      t_f  = sumtim+deltex                ! Time at end of time step
!
      N_up = Well_TData(n)%N_points        ! # of points in the table
      V_dn = Well_TData(n)%p_TList(1)      ! First time value in table
      V_up = Well_TData(n)%p_TList(N_up)   ! Last time value in table
! 
!
!***********************************************************************
!*                                                                     *
!*      Determine the time interval in the generation time table:      *
!*          First the time at the beginning of the time step           *
!*                                                                     *
!***********************************************************************
! 
! 
      IF_RangeD: IF(t_i < V_dn .OR. t_i > V_up) THEN          ! When outside the table range, ...
                    PRINT 6001, t_i,V_dn,V_up                 ! ... print warning, ... 
                    PRINT 6002, KCYC,SS(n)%name_el,SS(n)%name ! ... print additional info, ... 
                    IGOOD = 3                                 ! ... set up for Dt cutback
                    RETURN                                    ! Done ! Exit routine
                 ELSE
                    i_Lo = COUNT(Well_TData(n)%p_TList-t_i <= 0.0d0) ! Determine the interval #
                 END IF IF_RangeD
! 
! 
!***********************************************************************
!*                                                                     *
!*      Determine the time interval in the generation time table:      *
!*                The time at the end of the time step                 *
!*                                                                     *
!***********************************************************************
! 
! 
      IF_RangeU: IF(t_f < V_dn .OR. t_f > V_up) THEN
                    PRINT 6001, t_f,V_dn,V_up
                    PRINT 6002, KCYC,SS(n)%name_el,SS(n)%name
                    IGOOD = 3
                    RETURN
                 ELSE
                    i_Hi = COUNT(Well_TData(n)%p_TList-t_f <= 0.0d0) 
                 END IF IF_RangeU
! 
! 
!***********************************************************************
!*                                                                     *
!*              DETERMINATION OF WELL RATE AND ENTHALPY                *
!*                                                                     *
!***********************************************************************
! 
! 
      SELECT_Int: SELECT CASE(MOP(12))
! 
! ----------
! ... 1st Option: Q,H are computed through linear interpolation           
! ----------
! 
      CASE(0)
!      
         Q1 =  Well_TData(n)%p_QList(i_Lo)     &    ! Q at the begining of the 
     &       +(t_i-Well_TData(n)%p_TList(i_Lo)   &   !    current time-step
     &        )                                  &   !    is obtained from 
     &         *( Well_TData(n)%p_QList(i_Lo+1)  &   !    table interpolation
     &           -Well_TData(n)%p_QList(i_Lo) &
     &          ) &
     &         /( Well_TData(n)%p_TList(i_Lo+1) &
     &           -Well_TData(n)%p_TList(i_Lo) &
     &          )
!      
         Q2 =  Well_TData(n)%p_QList(i_Hi)       &   ! Q at the end of the  
     &       +(t_f-Well_TData(n)%p_TList(i_Hi)     & !    current time-step
     &        )                                    & !    is obtained from
     &         *( Well_TData(n)%p_QList(i_Hi+1)    & !    table interpolation
     &           -Well_TData(n)%p_QList(i_Hi) &
     &          ) &
     &         /( Well_TData(n)%p_TList(i_Hi+1) &
     &           -Well_TData(n)%p_TList(i_Hi) &
     &          )
!      
         Gx = 5.0d-1*(Q1+Q2)                      ! Average Q
!      
         IF(ITABA /= ' ') THEN 
            Q1 =  Well_TData(n)%p_HList(i_Lo)     &  ! H at the begining of the 
     &          +(t_f-Well_TData(n)%p_TList(i_Lo)  & !    current time-step
     &           )                                &  !    is obtained from 
     &            *( Well_TData(n)%p_HList(i_Lo+1)  &!    table interpolation
     &              -Well_TData(n)%p_HList(i_Lo) &
     &             ) &
     &            /( Well_TData(n)%p_TList(i_Lo+1) &
     &              -Well_TData(n)%p_TList(i_Lo) &
     &             )
!      
            Q2 =  Well_TData(n)%p_HList(i_Hi)   &    ! H at the end of the
     &          +(t_f-Well_TData(n)%p_TList(i_Hi)   &!    current time-step
     &           )                                 & !    is obtained from
     &            *( Well_TData(n)%p_HList(i_Hi+1)  &!    table interpolation
     &              -Well_TData(n)%p_HList(i_Hi) &
     &             ) &
     &            /( Well_TData(n)%p_TList(i_Hi+1) &
     &              -Well_TData(n)%p_TList(i_Hi) &
     &             )
!      
            EGx = 5.0d-1*(Q1+Q2)                  ! Average H
         ELSE
            EGx = 0.0d0
         END IF
! 
! ----------
! ... 2nd Option: Q,H are computed assuming a step function distribution of tabular data       
! ----------
! 
      CASE(1)
!      
         Gx = 5.0d-1*(  Well_TData(n)%p_QList(i_Lo)  &     ! Q averaged at the beginning and 
     &                + Well_TData(n)%p_QList(i_Hi)   &    !    end of the current time-step
     &               )   
!      
         IF(ITABA /= ' ') THEN
            EGx = 5.0d-1*(  Well_TData(n)%p_HList(i_Lo) &  ! H averaged at the beginning and
     &                    + Well_TData(n)%p_HList(i_Hi)  & !    end of the current time-step
     &                   )
         ELSE
            EGx = 0.0d0
         END IF
! 
! ----------
! ... Q,H are computed assuming a rigorous adherence to a step function distribution        
! ----------
! 
      CASE(2)
!
! ...... When the current time-step boundaries fall within a single table interval
!
         IF_Rig: IF(i_Lo == i_Hi) THEN
            Gx = Well_TData(n)%p_QList(i_Lo)
            IF(ITABA /= ' ') EGx = Well_TData(n)%p_HList(i_Lo)
!
! ...... When the current time-step boundaries span across several table intervals
!
         ELSE
            qdt =   Well_TData(n)%p_QList(i_Lo) &
     &           *( Well_TData(n)%p_TList(i_Lo+1) &
     &             -t_i &
     &            ) &
     &           +  Well_TData(n)%p_QList(i_Hi) &
     &           *( t_f &
     &             -Well_TData(n)%p_TList(i_Hi)  &
     &            )
!      
            IF(ITABA /= ' ') THEN
               hqdt =   Well_TData(n)%p_HList(i_Lo) &
     &                 *Well_TData(n)%p_QList(i_Lo) &
     &                 *( Well_TData(n)%p_TList(i_Lo+1) &
     &                   -t_i &
     &                  ) &
     &               +  Well_TData(n)%p_HList(i_Hi) &
     &                 *Well_TData(n)%p_QList(i_Hi) &
     &                 *( t_f &
     &                   -Well_TData(n)%p_TList(i_Hi)  &
     &                  )
            END IF
!
! ......... Compute intermediate parameters
!
            IF_Rig2: IF(i_Hi > i_Lo+1) THEN
               i_Lop = i_Lo+1
               i_Him = i_Hi-1
!      
               DO_mLoop: DO m = i_Lop,i_Him
                  qdt =   qdt &
     &                  + Well_TData(n)%p_QList(m) &
     &                 *( Well_TData(n)%p_TList(m+1) &
     &                   -Well_TData(n)%p_TList(m) &
     &                  )
                  IF(ITABA /= ' ') THEN
                     hqdt =   hqdt &
     &                      + Well_TData(n)%p_HList(m) &
     &                       *Well_TData(n)%p_QList(m) &
     &                       *( Well_TData(n)%p_TList(m+1) &
     &                         -Well_TData(n)%p_TList(m) &
     &                        )
                  END IF
!      
               END DO DO_mLoop
!      
            END IF IF_Rig2
!
! ......... Finally, compute Q,H
!
            Gx = qdt/deltex                           ! Determine Q
!
            IF(ITABA /= ' ' .AND. qdt /= 0.0d0) THEN  ! Determine H
               EGx = hqdt/qdt
            ELSE
               EGx = 0.0d0
            END IF
!      
         END IF IF_Rig
!      
      END SELECT SELECT_Int
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'QTable_Interp     1.0    4 January   2004',6X, &
     &         'Interpolate sink/source rates and enthalpies from ', &
     &         'tabular data')
!      
 6001 FORMAT(T2,'In \D2QTable_Interp\D2, the time value ',1pE14.7, &
     &          ' is outside the range  (',1pE14.7,',',1pE14.7,')')
 6002 FORMAT(T2,'At KCYC = ',I4,' the time exceeds the maximum value ', &
     &          'in the generation time table at element',A5,  &
     &          ' (source ',A5,') -- will reduce DELT')
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of QTable_Interp
!
!
      RETURN
! 
      END SUBROUTINE QTable_Interp
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE WELL_Delivrblty(i_n,kf)
! 
! ...... Modules to be used 
! 
         USE Basic_Param
         USE GenControl_Param
! 
         USE Q_Arrays
		 USE MPI_param
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*    ROUTINE FOR READING FROM F-FILES DATA ON WELL DELIVERABILITY     *
!*                                                                     *
!*                  Version 1.00, January 19, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN)    :: i_n
      INTEGER, INTENT(INOUT) :: kf
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: i,j,k,nG,nH,ier
! 
! -------
! ... Character variables
! -------
! 
      CHARACTER(LEN = 5)  :: DLV_File
      CHARACTER(LEN = 10) :: Header
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: EX
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of WELL_Delivrblty
!
!
      ICALL = ICALL+1
      IF(ICALL == 1 .and. MPI_RANK==0) WRITE(11,6000)
! 
! 
!***********************************************************************
!*                                                                     *
!*        DETERMINE THE FILE NAMES WITH THE DELIVERABILITY DATA        *
!*                                                                     *
!***********************************************************************
! 
! 
      DO_NumCh: DO i = 1,5
! 
         IF(SS(i_n)%type(i:i) == ' ') THEN
            DLV_File = SS(i_n)%type(1:i-1)     ! The file where the data are stored
            EXIT
         ELSE
            IF(i == 5) DLV_File = SS(i_n)%type ! The file where the data are stored
         END IF
! 
      END DO DO_NumCh
! 
! ----------
! ... Determine if such a file is available      
! ----------
! 
      INQUIRE(FILE=DLV_File,EXIST=EX)
! 
      IF(EX) THEN                               ! If the file exists ...
         PRINT 6001, DLV_File                   !    print info 
         OPEN(9,FILE=DLV_File,STATUS='OLD')     !    open as an old file
      ELSE                                      ! If the file does not exist ...
         PRINT 6002, DLV_File                   !    print warning and ignore the item
         SS(i_n)%typ_indx  = 0                  ! Reset the index describing well type
         RETURN                                 ! Done ! Exit the routine
      END IF    
! 
! 
!***********************************************************************
!*                                                                     *
!*        For a valid F-type file with deliverability data, ...        *
!*                                                                     *
!***********************************************************************
! 
! 
      SS(i_n)%typ_indx = nk1+2             ! Set index describing well type
! 
      SS(i_n)%pi       = SS(i_n)%rate_m    ! Set the well productivity index
! 
! ----------
! ... If the current wellbore pressure file has already been read      
! ----------
! 
      IF_ExPF: IF(kf >= 1) THEN
! 
         DO k=1,kf
! 
            IF(DLV_File == SS(k)%dlv_file_name) THEN
! 
               WDel(i_n)%idTab = k                   ! Assign index of previously read wellbore 
                                                     !    pressure file to current GENER item
               RETURN                                ! Done ! Exit the routine
! 
            END IF       
! 
         END DO       
! 
      END IF IF_ExPF 
! 
! ----------
! ... Read new wellbore pressure file                 
! ----------
! 
      kf                   = kf+1          ! Set the new variables
      SS(kf)%dlv_file_name = DLV_File
! 
      WDel(i_n)%idTab = kf
! 
      READ(9,5001) Header        ! Read the title
! 
      READ(9,5002) nG,nH         ! Read the number of data points
! 
      WDel(kf)%NumG = nG         ! Read the number of generation curves
      WDel(kf)%NumH = nH         ! Read the number of enthalpy curves
! 
! ----------
! ... Allocate memory to the array including the bottomhole pressure data                
! ----------
! 
      ALLOCATE(WDel(i_n)%p_bhp(nG+nH+nG*nH), STAT = ier)
! 
! ... Print explanatory comments  
! 
         IF(ier == 0) THEN
            WRITE(50,6101) 
         ELSE
            PRINT 6102
            WRITE(50,6102) 
            STOP
         END IF
! 
      READ(9,5003)  (WDel(kf)%p_bhp(i),i=1,nG)     ! Read the generation data (row headings)
! 
      READ(9,5003)  (WDel(kf)%p_bhp(nG+j),j=1,nH)  ! Read the enthalpy data (column headings)
! 
      READ(9,5003)  ((WDel(kf)%p_bhp(nG+nH+(i-1)*nH+j),j=1,nH),i=1,nG) ! Read the pressure data
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 5001 FORMAT(A10)
 5002 FORMAT(2I5)
 5003 FORMAT(8E10.4)
! 
 6000 FORMAT(/,'WELL_Delivrblty   1.0   19 January   2004',6X, &
     &         'Read from F-files data on well deliverability ') 
! 
 6001 FORMAT(' FILE "',a5,'" EXISTS --- OPEN AS AN OLD FILE') 
 6002 FORMAT(T2,'Invalid F-type GENER item:  File "',A5,'" is ', &
     &          'not available')
! 
 6101 FORMAT(T2,'Memory allocation of "WDel(i_n)%p_bhp" in subroutine ', &
     &          '"WELL_Delivrblty" was successful')
  6102 FORMAT(//,20('ERROR-'),//, &
     &       T2,'Memory allocation of "WDel(i_n)%p_bhpt" in ', &
     &          'subroutine "WELL_Delivrblty" was unsuccessful',//, &
     &       T2,'!!!!!!!    THE EXECUTION WAS STOPPED    !!!!!!!', &
     &       //,20('ERROR-'))
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  WELL_Delivrblty
!
!
      RETURN
! 
      END SUBROUTINE WELL_Delivrblty
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE GRAVITY_correct(n,ltaba)
! 
! ...... Modules to be used 
! 
         USE Basic_Param
         USE GenControl_Param
! 
         USE Variable_Arrays
         USE Q_Arrays
		 USE MPI_Param
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*      ROUTINE PROVIDING A SIMPLE GRAVITY CORRECTION TO FLOWING       *
!*   BOTTOMHOLE PRESSURE OF MULTI-FEED ZONE WELLS ON DELIVERABILITY    *
!*                                                                     *
!*                   Version 1.00, February 4, 2004                    *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: pind
      REAL(KIND = 8) :: Mob_Gas,Mob_Aqu,QVG,QVW
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: n,ltaba
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: NLTAB1,ND,JD,JDLOCP
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of GRAVITY_correct
!
!
      ICALL = ICALL+1
      IF(ICALL == 1 .and. MPI_RANK==0) WRITE(11,6000)     ! Write version information
!
      NLTAB1 = n+LTABA-1                ! Determine index
!
!***********************************************************************
!*                                                                     *
!*                 LOOP OVER ALL LAYERS FOR THIS WELL                  *
!*                                                                     *
!***********************************************************************
!
      DO_NumLayrs: DO nd=n,NLTAB1      ! ND is the generation index
!
         jd     =  SS(nd)%el_num       ! Determine positions in the PAR array
         JDLOCP = (jd-1)*NK1
!
         pind   = SS(nd)%pi            ! The well productivity index
! 
! -------------
! ...... Compute gas phase mobilities
! -------------
! 
         IF(Cell_V(jd,0)%p_Visc(1) /= 0.0d0) THEN
            Mob_Gas = Cell_V(jd,0)%p_KRel(1)/Cell_V(jd,0)%p_Visc(1)
         ELSE
            Mob_Gas = 0.0d0
         END IF
! 
! -------------
! ...... Compute aqueous phase mobilities
! -------------
! 
         IF(NPH >= 2 .AND. Cell_V(jd,0)%p_Visc(2) /= 0.0d0) THEN
            Mob_Aqu = Cell_V(jd,0)%p_KRel(2)/Cell_V(jd,0)%p_Visc(2)
         ELSE
            Mob_Aqu = 0.0d0
         END IF
! 
! -------------
! ...... Compute volumetric production rates for individual phases
! -------------
! 
         QVG = pind*Mob_Gas*X(JDLOCP+1)    ! Gas
         QVW = pind*Mob_Aqu*X(JDLOCP+1)    ! Aqueous
! 
! -------------
! ...... Assign values to arrays for cumulative phase rates
! -------------
! 
         IF(nd == n) THEN
            SS(nd)%rate_phs(1) = QVG
            SS(nd)%rate_phs(2) = QVW
         ELSE
            SS(nd)%rate_phs(1) = SS(nd-1)%rate_phs(1)+QVG
            SS(nd)%rate_phs(2) = SS(nd-1)%rate_phs(2)+QVW
         END IF
! 
! -------------
! ...... Assign values to array of gradients
! -------------
! 
         SS(nd)%grad = ( SS(nd)%rate_phs(1)*Cell_V(jd,0)%p_Dens(1) &
     &                  +SS(nd)%rate_phs(2)*Cell_V(jd,0)%p_Dens(2) &
     &                 )*9.80665d0 &
     &                /(SS(nd)%rate_phs(1)+SS(nd)%rate_phs(2))
! 
! 
! 
      END DO DO_NumLayrs
! 
! 
!***********************************************************************
!*                                                                     *
!*   Perform loop going from top layer down: starting from the layer   *
!*  just below the top (index N+LTABA-2) to the bottom layer (index N) *
!*                                                                     *
!*                  CAREFUL! Whole array operations                    *
!*                                                                     *
!***********************************************************************
! 
! 
      SS(NLTAB1-1:N:-1)%bhp =    SS(NLTAB1:N+1:-1)%bhp &
     &                       +(  SS(NLTAB1:N+1:-1)%z_layer &
     &                          *SS(NLTAB1:N+1:-1)%grad &
     &                         + SS(NLTAB1-1:N:-1)%z_layer &
     &                          *SS(NLTAB1-1:N:-1)%grad &
     &                        )*5.0d-1
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'GRAVITY_correct   1.0    4 February  2004',6X, &
     &         'Perform simple gravity correction for flowing ', &
     &         'bottomhole pressure')
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of GRAVITY_correct
!
!
      RETURN
! 
      END SUBROUTINE GRAVITY_correct
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE PhaseXH_Source(n,j,Gn,fac)
! 
! ...... Modules to be used 
! 
         USE Basic_Param
         USE GenControl_Param
! 
         USE Element_Arrays
         USE Connection_Arrays
         USE Variable_Arrays
         USE Q_Arrays
         USE TempStoFlo_Arrays
		 USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*  ROUTINE DETERMINING PHASE COMPOSITION & ENTHALPY OF SOURCE FLUID   *
!*                                                                     *
!*                   Version 1.00 - April 27, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN) :: Gn,fac
! 
      REAL(KIND = 8) :: FFS
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: n,j
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: ic,m,np
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of PhaseXH_Source
!
!
      ICALL = ICALL+1
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)
! 
! -------
! ... Assignment of parameter values
! -------
! 
      ic = mop(9)+1
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                LOOP OVER NEQ+1 SETS OF PARAMETERS                   >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_Neq1: DO m=1,NEQ1
! 
! ----------
! ...... Initialize the block D(j,j)
! ----------
! 
         D(1:NK1,m) = 0.0d0  ! CAREFUL - Whole array operation
! 
! ----------
! ...... Initialization
! ----------
! 
         FFS        = 0.0d0   ! = SUM(mobility*density) in all phases
         SS(n)%enth = 0.0d0   ! Flow enthalpy  
! 
! 
!***********************************************************************
!*                                                                     *
!*           COMPUTE FRACTIONAL FLOWS IN THE VARIOUS PHASES            *
!*                                                                     *
!***********************************************************************
! 
! 
         DO_NumPhase1: DO np=1,NPH
!      
            SS(n)%frac_flo(np) = 0.0d0   ! Initialization of fractional flows
!
            IF(ic == 1 .AND. Cell_V(j,m-1)%p_Visc(np) /= 0.0d0) THEN
               SS(n)%frac_flo(np) = Cell_V(j,m-1)%p_KRel(np) &
     &                             *Cell_V(j,m-1)%p_Dens(np) &
     &                             /Cell_V(j,m-1)%p_Visc(np)
            END IF
!
!
            IF(ic == 2) THEN
               SS(n)%frac_flo(np) = Cell_V(j,m-1)%p_Satr(np) &
     &                             *Cell_V(j,m-1)%p_Dens(np)
            END IF
! 
! -------------
! ......... Adjsust the sum in FFS
! -------------
! 
            FFS = FFS+SS(n)%frac_flo(np)
! 
! -------------
! ......... For MOP(4) >= 5, print info
! -------------
! 
            IF(MOP(4) >= 5) PRINT 6002, m,np, &
     &                                  Cell_V(j,m-1)%p_KRel(np),  &
     &                                  Cell_V(j,m-1)%p_Visc(np), &
     &                                  Cell_V(j,m-1)%p_Dens(np), &
     &                                  SS(n)%frac_flo(np),FFS
!
         END DO DO_NumPhase1
! 
! 
!***********************************************************************
!*                                                                     *
!*           RENORMALIZE FRACTIONAL FLOWS TO 1, AND COMPUTE            *
!*               CONTRIBUTIONS OF COMPONENTS IN PHASES                 *
!*                                                                     *
!***********************************************************************
! 
! 
         DO_NumPhase2: DO np=1,NPH
!      
! ......... Normalization of fractional flows      
!      
            IF(FFS /= 0.0d0) SS(n)%frac_flo(np) = SS(n)%frac_flo(np)/FFS
!      
! ......... Contribution of components to phase enthalpy     
!      
            SS(n)%enth = SS(n)%enth &
     &                  +SS(n)%frac_flo(np)*Cell_V(j,m-1)%p_Enth(np)
!      
! ......... Adjustment of the Jacobian submatrices    
! ......... CAREFUL! Whole array operation     
!      
            D(1:NK,M) = D(1:NK,M) &
     &                 -FAC*GN*SS(n)%frac_flo(np) &
     &                        *Cell_V(j,m-1)%p_MasF(1:NK,np)
!
         END DO DO_NumPhase2
! 
! ----------
! ...... Adjust the heat-related Jacobian submatrices
! ----------
! 
         D(NK1,m) = D(NK1,m)-FAC*GN*SS(n)%enth
!
! <<<                      
! <<< End of the NEQ1 LOOP         
! <<<
!
      END DO DO_Neq1
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'PhaseXH_Source    1.0   27 April     2004',6X, &
     &         'Determine phase composition and enthalpy of ', &
     &         'source fluid')
! 
 6002 FORMAT(' M=',I2,' NP=',I1,' K=',1pE13.6,' VIS=',1pE13.6, &
     &       ' D=',1pE13.6,' FF=',1pE13.6,' FFS=',1pE13.6)
!c
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of PhaseXH_Source
!
!
      RETURN
! 
!
      END SUBROUTINE PhaseXH_Source
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE PhaseXH_WellDelv(n,jlocp,j,fac)
! 
! ...... Modules to be used 
! 
         USE Basic_Param
         USE GenControl_Param
! 
         USE Element_Arrays
         USE Connection_Arrays
         USE Variable_Arrays
         USE Q_Arrays
         USE TempStoFlo_Arrays
		 USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*  ROUTINE DETERMINING PHASE COMPOSITION & ENTHALPY OF SOURCE FLUID   *
!*                    FOR WELLS ON DELIVERABILITY                      *
!*                                                                     *
!*                   Version 1.00 - April 27, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN) :: fac
! 
      REAL(KIND = 8) :: DELP,pin,Gn,FFS,DPRES
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: n,j,jlocp
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: ic,m,np
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of PhaseXH_WellDelv
!
!
      ICALL = ICALL+1
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)
! 
! -------
! ... Assignment of parameter values
! -------
! 
      ic  = mop(9)+1
      pin = SS(n)%pi
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                LOOP OVER NEQ+1 SETS OF PARAMETERS                   >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_Neq1: DO m=1,NEQ1
! 
! ----------
! ...... Initialize the block D(j,j)
! ----------
! 
         D(1:NK1,M) = 0.0d0  ! CAREFUL - Whole array operation
! 
! ----------
! ...... Initializations
! ----------
! 
         FFS          = 0.0d0   ! = SUM(mobility*density) in all phases
         SS(n)%rate_m = 0.0d0   ! Mass flow rate 
         SS(n)%enth   = 0.0d0   ! Enthalpy rate
! 
! 
!***********************************************************************
!*                                                                     *
!*           COMPUTE FRACTIONAL FLOWS IN THE VARIOUS PHASES            *
!*                                                                     *
!***********************************************************************
! 
! 
         DO_NumPhase1: DO np=1,NPH
!      
            SS(n)%frac_flo(np) = 0.0d0   ! Initialization of fractional flows
! 
! 
            IF(ic == 1 .AND. Cell_V(j,m-1)%p_Visc(np) /= 0.0d0) THEN
               SS(n)%frac_flo(np) = Cell_V(j,m-1)%p_KRel(np) &
     &                             *Cell_V(j,m-1)%p_Dens(np) &
     &                             /Cell_V(j,m-1)%p_Visc(np)
            END IF
!
!
            IF(ic == 2) THEN
               SS(n)%frac_flo(np) = Cell_V(j,m-1)%p_Satr(np) &
     &                             *Cell_V(j,m-1)%p_Dens(np)
            END IF
! 
! 
!***********************************************************************
!*                                                                     *
!*         COMPUTE FLOW RATES FOR EACH PHASE, AND TOTAL RATE           *
!*                    FOR WELLS ON DELIVERABILITY                      *
!*                                                                     *
!***********************************************************************
! 
! 
            IF(m == 2) THEN
               DPRES = DELX(JLOCP+1)
            ELSE 
               DPRES = 0.0d0
            END IF
!
            DELP = MAX(0.0d0, (  X(JLOCP+1) + DX(JLOCP+1) + DPRES &
     &                         + Cell_V(j,m-1)%p_PCap(np) - SS(n)%bhp) &
     &                )
! 
! -------------
! ......... Compute fractional flows (adjustment)
! -------------
! 
            SS(n)%frac_flo(np) = SS(n)%frac_flo(np)*pin*DELP
! 
! -------------
! ......... Compute mass flow rate
! -------------
! 
            SS(n)%rate_m = SS(n)%rate_m-SS(n)%frac_flo(np)
! 
! -------------
! ......... Augment the sum in FFS
! -------------
! 
            FFS = FFS+SS(n)%frac_flo(np)
! 
! -------------
! ......... For MOP(4) >= 5, print info
! -------------
! 
            IF(MOP(4) >= 5) PRINT 6002, m,np, &
     &                                  Cell_V(j,m-1)%p_KRel(np),  &
     &                                  Cell_V(j,m-1)%p_Visc(np), &
     &                                  Cell_V(j,m-1)%p_Dens(np), &
     &                                  SS(n)%frac_flo(np),FFS
!
         END DO DO_NumPhase1
! 
! 
!***********************************************************************
!*                                                                     *
!*           RENORMALIZE FRACTIONAL FLOWS TO 1, AND COMPUTE            *
!*               CONTRIBUTIONS OF COMPONENTS IN PHASES                 *
!*                                                                     *
!***********************************************************************
! 
! 
         Gn = SS(n)%rate_m    ! Initialization
!
!
!
         DO_NumPhase2: DO np=1,NPH
!      
! ......... Normalization of fractional flows        
!      
            IF(FFS /= 0.0d0) SS(n)%frac_flo(np) = SS(n)%frac_flo(np)/FFS
!      
! ......... Contribution of components to phase enthalpy     
!      
            SS(n)%enth = SS(n)%enth &
     &                  +SS(n)%frac_flo(np)*Cell_V(j,m-1)%p_Enth(np)
!      
! ......... Adjustment of the Jacobian submatrices    
! ......... CAREFUL! Whole array operation     
!      
            D(1:NK,M) = D(1:NK,M) &
     &                 -FAC*GN*SS(n)%frac_flo(np) &
     &                        *Cell_V(j,m-1)%p_MasF(1:NK,np)
!
         END DO DO_NumPhase2
! 
! ----------
! ...... Adjust the heat-related Jacobian submatrices
! ----------
! 
         D(NK1,m) = D(NK1,m)-FAC*GN*SS(n)%enth
!
! <<<                      
! <<< End of the NEQ1 LOOP         
! <<<
!
      END DO DO_Neq1
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'PhaseXH_WellDelv  1.0   27 April     2004',6X, &
     &         'Determine phase composition and enthalpy of ', &
     &         'source fluid for wells on deliverability')
! 
 6002 FORMAT(' M=',I2,' NP=',I1,' K=',1pE13.6,' VIS=',1pE13.6, &
     &       ' D=',1pE13.6,' FF=',1pE13.6,' FFS=',1pE13.6)
!
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of PhaseXH_WellDelv
!
!
      RETURN
! 
!
      END SUBROUTINE PhaseXH_WellDelv
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE PXH_WelHedP(n,jlocp,j,fac)
! 
! ...... Modules to be used 
! 
         USE Basic_Param
         USE GenControl_Param
! 
         USE Element_Arrays
         USE Connection_Arrays
         USE Variable_Arrays
         USE Q_Arrays
         USE TempStoFlo_Arrays
		 USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*   ROUTINE DETERMINING BOTTOMHOLE PRESSURE, PHASE COMPOSITION AND    *
!*              ENTHALPY OF SOURCE FLUID FOR PRODUCTION                *
!*                 WITH SPECIFIED WELLHEAD PRESSURE                    *
!*                                                                     *
!*                   Version 1.00 - April 26, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN) :: fac
! 
      REAL(KIND = 8) :: gw,gw0,pin,Gn,FFS,pwbn,gres,dpdg,rg,pj,drgdg
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: n,j,jlocp
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: ic,m,np,ig
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: NRI_Convergence
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of PXH_WelHedP
!
!
      ICALL = ICALL+1
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)
! 
! -------
! ... Assignment of parameter values
! -------
! 
      ic  = mop(9)+1
      pin = SS(n)%pi
      gw0 = SS(n)%rate_res
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                LOOP OVER NEQ+1 SETS OF PARAMETERS                   >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_Neq1: DO m=1,NEQ1
! 
! ----------
! ...... Initialize the block D(j,j)
! ----------
! 
         D(1:NK1,M) = 0.0d0  ! CAREFUL - Whole array operation
! 
! ----------
! ...... Initializations
! ----------
! 
         FFS        = 0.0d0   ! = SUM(mobility*density) in all phases
         SS(n)%enth = 0.0d0   ! Enthalpy rate
! 
! 
!***********************************************************************
!*                                                                     *
!*           COMPUTE FRACTIONAL FLOWS IN THE VARIOUS PHASES            *
!*                                                                     *
!***********************************************************************
! 
! 
         DO_NumPhase1: DO np=1,NPH
!      
            SS(n)%frac_flo(np) = 0.0d0   ! Initialization of fractional flows
! 
! 
            IF(ic == 1 .AND. Cell_V(j,m-1)%p_Visc(np) /= 0.0d0) THEN
               SS(n)%frac_flo(np) = Cell_V(j,m-1)%p_KRel(np) &
     &                             *Cell_V(j,m-1)%p_Dens(np) &
     &                             /Cell_V(j,m-1)%p_Visc(np)
            END IF
!
!
            IF(ic == 2) THEN
               SS(n)%frac_flo(np) = Cell_V(j,m-1)%p_Satr(np) &
     &                             *Cell_V(j,m-1)%p_Dens(np)
            END IF
! 
! -------------
! ......... Augment the sum in FFS
! -------------
! 
            FFS = FFS+SS(n)%frac_flo(np)
! 
! -------------
! ......... Contribution of components to phase enthalpy     
! -------------
!      
            SS(n)%enth = SS(n)%enth &
     &                  +SS(n)%frac_flo(np)*Cell_V(j,m-1)%p_Enth(np)
! 
! -------------
! ......... For MOP(4) >= 5, print info
! -------------
! 
            IF(MOP(4) >= 5) PRINT 6002, m,np, &
     &                                  Cell_V(j,m-1)%p_KRel(np),  &
     &                                  Cell_V(j,m-1)%p_Visc(np), &
     &                                  Cell_V(j,m-1)%p_Dens(np), &
     &                                  SS(n)%frac_flo(np),FFS
!
         END DO DO_NumPhase1
! 
! ----------
! ...... Normalize EG(n)     
! ----------
!      
         IF(FFS /= 0.0d0) SS(n)%enth = SS(n)%enth/FFS
! 
! 
!***********************************************************************
!*                                                                     *
!*          ITERATE ON FLOW RATE TO FIND BOTTOMHOLE PRESSURE           *
!*                                                                     *
!***********************************************************************
! 
! 
         IF(m == 2) THEN
            pj = pj+delx(jlocp+1)
         ELSE
            pj = x(jlocp+1)+dx(jlocp+1)
         END IF
! 
! ...... Initializations 
! 
         gw              = gw0      ! Well flow rate (positive for production)
         NRI_Convergence = .FALSE.  ! Flag indicating convergence of the NR iteration 
! 
! >>>>>>>>>>
! >>>>>>>>>>
! ...... 
! ...... Newton/Raphson iteration
! ...... 
! >>>>>>>>>>
! >>>>>>>>>>
! 
         DO_NewtRaphLoop: DO ig=1,20
! 
            CALL BotmHol_Pres(n,gw,SS(n)%enth,pwbn,dpdg) ! Find bottomhole pressure 
! 
            IF(igood /= 0) THEN                          ! If unable to do so, ...
               PRINT 6004, kcyc,SS(n)%name_el,SS(n)%name ! ... print warning message, and ...
               RETURN                                    ! ... exit the routine
            END IF
! 
! ......... Set reservoir rate (negative for production)
! 
            gres = -pin*FFS*(pj-pwbn)
            rg   =  gres+gw
! 
! ......... Print info for MOP(4) >= 5
! 
            IF(mop(4) >= 5) print 6006, ig,pwbn,gw,gres,dpdg,rg
! 
! -------------
! ......... Upon convergence
! -------------
! 
            IF(abs(rg/gw) <= 1.0d-10) THEN
               NRI_Convergence = .TRUE.      ! Reset the convergence flag
               EXIT DO_NewtRaphLoop          ! Exit the NR loop
            END IF
! 
! -------------
! ......... Otherwise, update variables and continue the iteration
! -------------
! 
            drgdg = pin*ffs*dpdg+1.0d0
            gw    = gw-rg/drgdg
! 
         END DO DO_NewtRaphLoop
! 
! 
!***********************************************************************
!*                                                                     *
!*          UPON COMPLETION OF THE NEWTON-RAPHSON ITERATION            *
!*                                                                     *
!***********************************************************************
! 
! 
         IF(NRI_Convergence .EQV. .TRUE.) THEN  ! Upon convergence
! 
! -------------
! ......... Finalize parameter values
! -------------
! 
            Gn = gres
! 
            IF(m == 1) THEN
               SS(n)%bhp      =  pwbn   ! Bottomhole pressure
               SS(n)%rate_m   =  gres   ! Flow rate
               SS(n)%rate_res = -gres   ! Store current well rate for next initialization
            END IF
! 
         ELSE                   
! 
! -------------
! ......... Upon unsucessful NR iteration
! -------------
! 
            igood = 3                                               ! Set flag
            PRINT 6008, kcyc,SS(n)%name_el,SS(n)%name,gres,gn,pwbn  ! Print warning
            RETURN                                                  ! Exit the routine
!
         END IF
! 
! 
!***********************************************************************
!*                                                                     *
!*           RENORMALIZE FRACTIONAL FLOWS TO 1, AND COMPUTE            *
!*               CONTRIBUTIONS OF COMPONENTS IN PHASES                 *
!*                                                                     *
!***********************************************************************
! 
! 
         DO_NumPhase2: DO np=1,NPH
!      
! ......... Normalization of fractional flows     
!      
            IF(FFS /= 0.0d0) SS(n)%frac_flo(np) = SS(n)%frac_flo(np)/FFS
!      
! ......... Adjustment of the Jacobian submatrices    
! ......... CAREFUL! Whole array operation     
!      
            D(1:NK,M) = D(1:NK,M) &
     &                 -FAC*GN*SS(n)%frac_flo(np) &
     &                        *Cell_V(j,m-1)%p_MasF(1:NK,np)
!
         END DO DO_NumPhase2
! 
! ----------
! ...... Adjust the heat-related Jacobian submatrices
! ----------
! 
         D(NK1,m) = D(NK1,m)-FAC*Gn*SS(n)%enth
!
! <<<                      
! <<< End of the NEQ1 LOOP         
! <<<
!
      END DO DO_Neq1
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'PXH_WelHedP       1.0   26 April     2004',6X, &
     &         'Determine bottomhole pressure, phase composition ', &
     &         'and enthalpy for production with specified ', &
     &         'wellhead pressure')
! 
 6002 FORMAT(' M=',I2,' NP=',I1,' K=',1pE13.6,' VIS=',1pE13.6, &
     &       ' D=',1pE13.6,' FF=',1pE13.6,' FFS=',1pE13.6)
 6004 FORMAT(' At KCYC = ',I5,' exceed WFLO table data at element ', &
     &       A5,' (source ',A5,') --> will reduce DELTEX')
 6006 FORMAT(' IG=',I2,'   Pwb=',E12.6,'   Gwell=',E12.6, &
     &       '   Gres=',1pE13.6,'   dPwb/dG =',1pE13.6, &
     &       '   RG =',1pE13.6)
 6008 format(' At KCYC = ',I4,', element ',A5,' (source ',A5,') no', &
     &       ' convergence for Pwb = ',1pE13.6,'   Gres = ',1pE13.6, &
     &       '   Gwel = ',1pE13.6)
!
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of PXH_WelHedP
!
!
      RETURN
! 
!
      END SUBROUTINE PXH_WelHedP
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE BotmHol_Pres(n,G,H,pp,dpdg)
! 
! ...... Modules to be used 
! 
         USE Basic_Param
         USE GenControl_Param
! 
         USE Element_Arrays
         USE Connection_Arrays
         USE Variable_Arrays
         USE Q_Arrays, ONLY: WDel
 
         USE TempStoFlo_Arrays
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*     ROUTINE DETERMINING BOTTOMHOLE PRESSURE THROUGH BIVARIATE       *
!*         INTERPOLATION FROM FLOWING WELLBORE PRESSURE TABLE          *
!*                                                                     *
!*                   Version 1.00 - April 27, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN)  :: g,h
      REAL(KIND = 8), INTENT(OUT) :: pp,dpdg
! 
      REAL(KIND = 8) :: QQ,RR,DQDg,DpDQ
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: n
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: nn,nG,nH,KL,KR,LL,LR
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of BotmHol_Pres
!
!
      ICALL = ICALL+1
      IF(ICALL == 1) WRITE(11,6000)
! 
! -------
! ... Assignment of parameter values
! -------
! 
      nn  = WDel(n)%idTab
      nG  = WDel(nn)%NumG
      nH  = WDel(nn)%NumH
!      kf  = nftab(n)
!      nG  = iftit(kf)
!      nH  = jftit(kf)
! 
! 
!***********************************************************************
!*                                                                     *
!*   Determine from the table the interval for the production rate     *
!*                                                                     *
!***********************************************************************
! 
! 
      IF_RangeG: IF((G < WDel(nn)%p_bhp(1)) .OR.  &
     &              (G > WDel(nn)%p_bhp(nG)) &
     &             ) THEN                                    ! When outside the table range, ...
         PRINT 6002, G,WDel(nn)%p_bhp(1),WDel(nn)%p_bhp(nG)  ! ... print warning
         IF(KC == 0) STOP                                    ! ... abort on first iteration
         IGOOD = 3                                           ! ... set the completion flag 
         RETURN                                              ! ... Done ! Exit routine
      ELSE
         KL = COUNT(G - WDel(nn)%p_bhp(1:nG) <= 0.0d0)  ! Determine the # of the left interval boundary
         KR = MIN(KL+1,ng)                              ! Determine the # of the right interval boundary
      END IF IF_RangeG
! 
! 
!***********************************************************************
!*                                                                     *
!*       Determine from the table the interval for the enthalpy        *
!*                                                                     *
!***********************************************************************
! 
! 
      IF_RangeH: IF((H < WDel(nn)%p_bhp(nG+1)) .OR.  &
     &              (H > WDel(nn)%p_bhp(nG+nH)) &
     &             ) THEN                                          ! When outside the table range, ...
         PRINT 6004, H,WDel(nn)%p_bhp(nG+1),WDel(nn)%p_bhp(nG+nH)  ! ... print warning
         IF(KC == 0) STOP                                          ! ... abort on first iteration
         IGOOD = 3                                                 ! ... set the completion flag
         RETURN                                                    ! ... Done ! Exit routine
      ELSE
         LL = COUNT(H - WDel(nn)%p_bhp(nG+1:nG+nH) <= 0.0d0)  ! Determine the # of the left interval boundary
         LR = MIN(LL+1,nH)                                    ! Determine the # of the right interval boundary
      END IF IF_RangeH
! 
! 
!***********************************************************************
!*                                                                     *
!*                    COMPUTE BOTTOMHOLE PRESSURE                      *
!*                                                                     *
!***********************************************************************
! 
! 
      QQ =  (G                  - WDel(nn)%p_bhp(KL)) &
     &     /(WDel(nn)%p_bhp(KR) - WDel(nn)%p_bhp(KL))
!
      RR =  (H                   - WDel(nn)%p_bhp(LL)) &
     &     /(WDel(nn)%p_bhp(LR) - WDel(nn)%p_bhp(LL))
!
      pp =  (1.0d0-QQ)*(1.0d0-RR)*WDel(nn)%p_bhp(nG+nH+(KL-1)*nH+LL) &
     &     +        (1.0d0-RR)*QQ*WDel(nn)%p_bhp(nG+nH+(KR-1)*nH+LL) &
     &     +        (1.0d0-QQ)*RR*WDel(nn)%p_bhp(nG+nH+(KL-1)*nH+LR) &
     &     +                QQ*RR*WDel(nn)%p_bhp(nG+nH+(KR-1)*nH+LR)
! 
! 
!***********************************************************************
!*                                                                     *
!*      COMPUTE DERIVATIVE OF PRESSURE WITH RESPECT TO FLOW RATE       *
!*                                                                     *
!***********************************************************************
! 
! 
      DQDg = 1.0d0/( WDel(nn)%p_bhp(KR)   &                     ! dQ/dG
     &              -WDel(nn)%p_bhp(KL) &
     &             )    
!
      DpDQ = (1.0d0-RR)*( WDel(nn)%p_bhp(nG+nH+(KR-1)*nH+LL) &  ! dP/dQ
     &                   -WDel(nn)%p_bhp(nG+nH+(KL-1)*nH+LL) &
     &                  ) &
     &      +        RR*( WDel(nn)%p_bhp(nG+nH+(KR-1)*nH+LR) &
     &                   -WDel(nn)%p_bhp(nG+nH+(KL-1)*nH+LR) &
     &                  )
!
      DpDg = DpDQ*DQDg                                        ! dP/dG = (dP/dQ)*(dQ/dG)
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'BotmHol_Pres      1.0   27 April     2004',6X, &
     &         'Determine bottomhole pressure through bivariate ', &
     &         'interpolation from flowing wellbore pressure table')
! 
 6002 FORMAT('===> In subroutine "BotmHol_Pres", the rate G = ',1pE15.8, &
     &       ' is outside the table range (', 1pE15.8,',',1pE15.8, &
     &       ')   <=== ')
 6004 FORMAT('===> In subroutine "BotmHol_Pres", ', &
     &       'the enthalpy H = ',1pE15.8, &
     &       ' is outside the table range (', 1pE15.8,',',1pE15.8, &
     &       ')   <=== ')
      IF(KC.EQ.0) STOP
!
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of BotmHol_Pres
!
!
      RETURN
! 
!
      END SUBROUTINE BotmHol_Pres
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE CPU_ElapsedTime(IFLAG,DT,TOTAL)  
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*                 ROUTINE COMPUTING ELAPSED CPU TIME                  *
!*                                                                     *
!*                  Version 1.00 - February 15, 2004                   *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(OUT) :: DT,TOTAL
! 
      REAL(KIND = 8) :: T1,T2
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: IFLAG
! 
      INTEGER :: ICALL = 0
! 
      SAVE ICALL,T1
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of CPU_ElapsedTime
!
!
      ICALL = ICALL+1
      IF(ICALL == 1) WRITE(11,6000)
! 
! -------
! ... Call the CPU timing routine
! -------
! 
      CALL CPU_TimeF(T2)
! 
! -------
! ... Compute elapsed time
! -------
! 
      IF(IFLAG /= 0) THEN
         DT    = T2-T1
         TOTAL = T2
      ELSE         
         T1 = T2
      END IF        
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'CPU_ElapsedTime   1.0   15 February  2004',6X, &
     &         'Compute elapsed CPU time')
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of CPU_ElapsedTime
!
!
      RETURN
! 
!
      END SUBROUTINE CPU_ElapsedTime
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE TABLE_TimSerD
! 
! ...... Modules to be used 
! 
         USE Basic_Param
         USE GenControl_Param
         USE TimeSeriesP_Param
! 
         USE Hydrate_Param
! 
         USE Element_Arrays
         USE Connection_Arrays
         USE Variable_Arrays
         USE Q_Arrays
		 USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*     ROUTINE TABULATING ELEMENT, CONNECTION AND GENERATION TIME      *
!*                SERIES DATA FOR SUBSEQUENT PLOTTING                  *
!*                                                                     *
!*                   Version 1.00 - April 27, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(100) :: xd
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: pco2,cgn,fft
! 
      REAL(KIND = 8) :: S_CH4inGas,S_CH4inAqu,S_CH4inTot 
      REAL(KIND = 8) :: S_H2OinGas,S_H2OinAqu,S_H2OinTot
! 
      REAL(KIND = 8) :: SumM_CH4 = 0.0d0,SumV_CH4 = 0.0d0
      REAL(KIND = 8) :: SumM_H2O = 0.0d0
! 
      REAL(KIND = 8) :: TGas_MRate,TWat_MRate 
      REAL(KIND = 8) :: TGas_Vol,TWat_Mass 
! 
      REAL(KIND = 8) :: Cum_GasV = 0.0d0
      REAL(KIND = 8) :: Cum_H2OM = 0.0d0, REALARRAY(2)
! 
! -------
! ... Integer arrays
! -------
! 
      INTEGER, DIMENSION(100) :: ind
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0, nogu = 0, IERR
! 
      INTEGER :: i,j,k,l,n,nloc,nnp
! 
      SAVE ICALL,nogu
      SAVE Cum_GasV,Cum_H2OM
      SAVE SumM_CH4,SumV_CH4,SumM_H2O
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of TABLE_TimSerD
!
!
      ICALL = ICALL+1
      IF(ICALL == 1 .and. MPI_RANK==0) WRITE(11,6000)
! 
! 
!***********************************************************************
!*                                                                     *
!*             Tabulate element-related times series data              *
!*                                                                     *
!***********************************************************************
! 
! 
      IF_ElemFOFT: IF(obs_elem_Flag .EQV. .TRUE.) THEN
! 
         pco2 = 0.0d0   ! Initializations
         i    = 0
! 
! ----------
! ...... Go through the element list in file "Elem_Time_Series"
! ----------
! 
         DO_NinFOFT: DO n=1,N_obs_elem
! 
            IF_EleNum: IF(obs_elem%num(n) > 0) THEN
! 
               i      =  i+1
               nloc   = (obs_elem%num(n)-1)*nk1
               ind(i) =  obs_elem%num(n)
!    
! -------------    
! ......... Check is pressure is the 1st primary variable 
! -------------    
!    
                  IF( (F_Kinetic .EQV. .FALSE.) .AND.   &
     &                (elem(ind(i))%StateIndex == 7_1  .OR.    &
     &                 elem(ind(i))%StateIndex == 10_1 .OR.  &
     &                 elem(ind(i))%StateIndex == 11_1) )      &
     &            THEN
                  xd((i-1)*4+1) = Cell_V(n,0)%p_AddD(2)
               ELSE
                  xd((i-1)*4+1) = x(nloc+1)
               END IF
! 
               xd((i-1)*4+2) = Cell_V(ind(i),0)%TemC       ! Temperature
               xd((i-1)*4+3) = Cell_V(ind(i),0)%p_Satr(1)  ! Gas saturation
               xd((i-1)*4+4) = Cell_V(ind(i),0)%p_Satr(2)  ! Water saturation
               xd((i-1)*4+5) = Cell_V(ind(i),0)%p_Satr(3)  ! Hydrate saturation
! 
            END IF IF_EleNum
! 
         END DO DO_NinFOFT
! 
      END IF IF_ElemFOFT
! 
! -------
! ... Write time series data for elements specified in file "Elem_Time_Series"
! -------
! 
      IF(obs_elem_Flag .EQV. .TRUE.) write(12,6002)  kcyc,sumtim, &
     &                     (ind(i),(xd((i-1)*4+k),k=1,5),i=1,N_obs_elem)
! 
! 
!***********************************************************************
!*                                                                     *
!*           Tabulate connection-related times series data             *
!*                                                                     *
!***********************************************************************
! 
! 
      IF_ConxCOFT: IF(obs_conx_Flag .EQV. .TRUE.) THEN
! 
         i = 0   ! Counter initialization
! 
! -------------
! ......... Go through the connection list in file "Conx_Time_Series"
! -------------
! 
         TGas_MRate = 0.0d0
         TWat_MRate = 0.0d0
! 
         DO_NinCOFT: DO j=1,N_obs_conx
! 
            n = obs_conx%num(j)
! 
            IF_ConNum: IF(n > 0) THEN
! 
               i      =  i+1
               ind(i) =  n
! 
               xd((i-1)*3+1) = conx(n)%FluxF(1)     ! Flux of phase 1
! 
               xd((i-1)*3+2) = conx(n)%FluxF(2)     ! Flux of phase 2
! 
               xd((i-1)*3+3) = conx(n)%FluxH        ! Heat flux
! 
               TGas_MRate = TGas_MRate+ABS(conx(n)%FluxF(1)) ! Total gas production rate
               TWat_MRate = TWat_MRate+ABS(conx(n)%FluxF(2)) ! Total H2O production rate
! 
            END IF IF_ConNum
! 
         END DO DO_NinCOFT
! 
         TGas_Vol  = TGas_MRate*DELTEX*1.47724d0      ! Total gas produced mass
         TWat_Mass = TWat_MRate*DELTEX                ! Total H2O produced mass
! 
		 CALL MPI_ALLREDUCE((/ TGas_Vol, TWat_Mass /),REALARRAY,2,MPI_REAL8,&
							MPI_SUM,MPI_COMM_WORLD,IERR)
		 TGas_Vol = REALARRAY(1)
		 TWat_Mass = REALARRAY(2)
		  
         Cum_GasV = Cum_GasV+TGas_Vol                 ! Total H2O produced mass
         Cum_H2OM = Cum_H2OM+TWat_Mass                ! Total H2O produced mass
! 
      END IF IF_ConxCOFT
! 
! -------
! ... Write time series data for connections specified in file "Conx_Time_Series"
! -------
! 
!      IF(icofu > 0) write(14,6004)  kcyc,sumtim,(ind(i),
!     &                             (xd((i-1)*3+k),k=1,3),i=1,icofu)

	
		
      IF(obs_conx_Flag .EQV. .TRUE.) write(14,6010)  sumtim/8.64d4, &
     &                               TGas_MRate*1.47724d0,TWat_MRate, &
     &                               Cum_GasV,Cum_H2OM
! 
! 
!***********************************************************************
!*                                                                     *
!*           Tabulate source/sink-related times series data            *
!*                                                                     *
!***********************************************************************
! 
! 
      IF_SS_All: IF(nogn > 0 .AND. N_obs_SS == -1) THEN
! 
! ----------
! ...... When generation data vs. time for all sinks/source
! ...... are to be written on unit 13
! ----------
! 
         i = 0   ! Counter initialization
! 
         DO_NumSS: DO n=1,nogn
! 
            j = SS(n)%el_num
! 
            IF_SSNum: IF(j > 0) THEN     
! 
               i = i+1
               IF(icall == 1) nogu = nogu+1
! 
! ............ Compute flowing CO2 mass fraction
! 
               cgn = 0.0d0
               fft = SS(n)%frac_flo(1)+SS(n)%frac_flo(2)
! 
               IF(nk == 2 .AND. fft /= 0.0d0) THEN
                  cgn= ( SS(n)%frac_flo(1)*Cell_V(j,0)%p_MasF(2,1) &
     &                  +SS(n)%frac_flo(2)*Cell_V(j,0)%p_MasF(2,2))/fft
               END IF
! 
               ind(i) = SS(n)%el_num
! 
               xd((i-1)*5+1) = SS(n)%rate_m
               xd((i-1)*5+2) = SS(n)%enth
               xd((i-1)*5+3) = cgn
               xd((i-1)*5+4) = SS(n)%frac_flo(1)
               xd((i-1)*5+5) = SS(n)%bhp
! 
            END IF IF_SSNum
! 
         END DO DO_NumSS
! 
      END IF IF_SS_All
! 
! ----------
! ...... When generation data vs. time for the sinks/source listed 
! ...... in data block "SS_Time_Series" are to be written on unit 13
! ----------
! 
      IF_SS_GOFT: IF(nogn > 0 .AND. (obs_SS_Flag .EQV. .TRUE.)) THEN
! 
         i = 0   ! Counter initialization
! 
         S_CH4inGas = 0.0d0
         S_CH4inAqu = 0.0d0
         S_CH4inTot = 0.0d0
! 
         S_H2OinGas = 0.0d0
         S_H2OinAqu = 0.0d0
         S_H2OinTot = 0.0d0
! 
         DO_NumGSS: DO l=1,N_obs_SS
! 
            n = obs_SS%num(l)                ! Determine number of corresponding source/sink
! 
            IF_SSGNum: IF(n > 0) THEN        ! Check whether the element listed in GOFT is valid
! 
               j = SS(n)%el_num              ! Assign element index      
               i = i+1
               IF(icall == 1) nogu = nogu+1
! 
! ............ Compute flowing CO2 mass fraction
! 
               nnp = (n-1)*nph
! 
               S_CH4inGas = S_CH4inGas+ABS(SS(n)%rate_m) &
     &                                 *SS(n)%frac_flo(1) &
     &                                 *Cell_V(j,0)%p_MasF(2,1)
               S_CH4inAqu = S_CH4inAqu+ABS(SS(n)%rate_m) &
     &                                 *SS(n)%frac_flo(2) &
     &                                 *Cell_V(j,0)%p_MasF(2,2)
               S_CH4inTot = S_CH4inGas+S_CH4inAqu
! 
               S_H2OinGas = S_H2OinGas+ABS(SS(n)%rate_m) &
     &                                 *SS(n)%frac_flo(1) &
     &                                 *Cell_V(j,0)%p_MasF(1,1)
               S_H2OinAqu = S_H2OinAqu+ABS(SS(n)%rate_m) &
     &                                 *SS(n)%frac_flo(2) &
     &                                 *Cell_V(j,0)%p_MasF(1,2)
               S_H2OinTot = S_H2OinGas+S_H2OinAqu
! 
            END IF IF_SSGNum
! 
		 CALL MPI_ALLREDUCE((/ S_CH4inTot, S_H2OinTot /),REALARRAY,2,MPI_REAL8,&
							MPI_SUM,MPI_COMM_WORLD,IERR)
		 S_CH4inTot = REALARRAY(1)
		 S_H2OinTot = REALARRAY(2)
		  
         END DO DO_NumGSS
! 

		
         SumM_CH4 = SumM_CH4+S_CH4inTot*DELTEX
         SumV_CH4 = SumM_CH4*1.47724d0
         SumM_H2O = SumM_H2O+S_H2OinTot*DELTEX
! 
      END IF IF_SS_GOFT
! 
! -------
! ... Write time series data for sources/sinks
! -------
! 
!      IF(nogu > 0) WRITE(13,6006)  kcyc,sumtim,(ind(n),
!     &                            (xd((n-1)*5+k),k=1,5),n=1,nogu)
      IF(nogu > 0) write(13,6010)  sumtim/8.64d4, &
     &                             S_CH4inTot*1.47724d0, &
     &                             SumM_CH4,SumV_CH4, &
     &                             S_H2OinTot,SumM_H2O
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'TABLE_TimSerD     1.0   27 April     2004',6X, &
     &         'Tabulate element, connection, and generation ', &
     &         'data vs. time for plotting')
! 
 6002 FORMAT(I5,' , ',1pE12.5,100(' , ',I5,5(' , ',1pE12.5)))
 6004 FORMAT(I5,' , ',1pE12.5,100(' , ',I5,3(' , ',1pE12.5)))
 6006 FORMAT(I5,' , ',1pE12.5,100(' , ',I5,5(' , ',1pE12.5)))
! 
 6010 FORMAT(1pE12.5,2x,7(1pE14.7,1x))
!
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of TABLE_TimSerD
!
!
      RETURN
! 
!
      END SUBROUTINE TABLE_TimSerD
!
!
!
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
