!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                                                                     >
!>                 PROPERTIES AND PROCESSES OF WATER                   >
!>                                                                     >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      MODULE H2O_Param
! 
! ----------
! ...... Double precision parameters
! ----------
! 
! ...... Molecular weight
! 
         REAL(KIND = 8), PARAMETER :: MW_H2O = 18.016d0
! 
! ...... Critical pressure (Pa)
! 
         REAL(KIND = 8), PARAMETER :: P_H2O_crit = 22.064d+06
! 
! ...... Critical temperature (C and K)
! 
         REAL(KIND = 8), PARAMETER :: Tc_H2O_crit = 373.946d+00
         REAL(KIND = 8), PARAMETER :: Tk_H2O_crit = 647.096d+00
! 
! ...... Critical density (kg/m^3)
! 
         REAL(KIND = 8), PARAMETER :: D_H2O_crit = 322.00d+00
! 
! ...... Triple point pressure (Pa)
! 
         REAL(KIND = 8), PARAMETER :: P_H2O_3p = 611.657d+00
! 
! ...... Triple point temperature (C and K)
! 
         REAL(KIND = 8), PARAMETER :: Tc_H2O_3p = 1.0d-02
         REAL(KIND = 8), PARAMETER :: Tk_H2O_3p = 273.16d+00
! 
! ...... Ice density at triple point 
! 
         REAL(KIND = 8), PARAMETER :: Ice_rho_3p = 9.168167856685283d+02
! 
! 
      END MODULE H2O_Param
!
!
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! 
!
      MODULE H2O_Properties
! 
		 USE MPI_Param
		 
         PRIVATE
! 
         PUBLIC :: P_Sat_H2O_S,T_Sat_H2O_S,P_Sublm_H2O_S,T_Sublm_H2O_S,&
     &             H_Sublm_H2O_S,P_Fusion_H2O_S,T_Fusion_H2O_S,&
     &             H_Fusion_H2O_S,SIGMA,&
     &             ViscLV_H2O,ViscLV_H2Ox,VISC_VapH2O,VISC_LiqH2O,&
     &             UHRho_LiqH2O,UHRho_VapH2O,ThrmCond_LiqH2O,&
     &             ThrmCond_VapH2O,KThermal_H2O,&
     &             HRhoL_H2OFusion,HRhoV_H2OSublim,&
     &             Ice_Enthalpy,Ice_Density,ThrmCond_Ice
!
!
! 
         CONTAINS
!
!
!
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! 
!
!
         INTEGER FUNCTION P_Sat_H2O_S(T_c,P_eq)
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*       Routine for calculating the saturation and sublimation        *
!*            pressure of H2O as a function of temperature             *
!*                                                                     *
!*                   Version 1.0 - October 18, 2004                    *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
            IMPLICIT NONE
! 
! -------------
! ......... Double precision parameters
! -------------
! 
            REAL(KIND=8), PARAMETER :: B00 = -7.3993388900833090d+00,   &
     &                                 BN1 =  7.2669349039428524d-02,    &
     &                                 BN2 = -1.5553228847117661d-04,   &
     &                                 BN3 =  4.8212464336377295d-07,   &
     &                                 BN4 = -1.1198866718381969d-09,   &
     &                                 BD1 =  1.9864351977571261d-03,   &
     &                                 BD2 = -1.1661892292039663d-06,   &
     &                                 BD3 =  9.1309450737729051d-09,   &
     &                                 BD4 = -6.2620767308454070d-11
! 
            REAL(KIND=8), PARAMETER :: A00 =  3.0884536677807830d+00,&   
     &                                 A01 =  1.1893263883467915d-02, &   
     &                                 A02 = -6.2744922204896418d-06,  & 
     &                                 A03 =  1.0392834188808607d-09 
! 
! -------------
! ......... Double precision input/output variables
! -------------
! 
            REAL(KIND=8), INTENT(IN)  :: T_c
! 
            REAL(KIND=8), INTENT(OUT) :: P_eq
! 
! -------------
! ......... Double precision arrays
! -------------
! 
            REAL(KIND=8) :: T_d
! 
! -------------
! ......... Logical variables
! -------------
! 
            LOGICAL :: First_call = .TRUE.
! 
! -------------
! ......... Saving variables
! -------------
! 
            SAVE First_call
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of P_Sat_H2O_S
!
!
            IF(First_call) THEN
               First_call = .FALSE.
               IF(MPI_RANK==0) WRITE(11,6000)
            END IF
! 
!***********************************************************************      
!*                                                                     *
!*              Compute the saturation pressure (in Pa)                *
!*                                                                     *
!***********************************************************************      
!
            IF(T_c >= 1.0d-2 .AND. T_c <= 3.735d2) THEN  
! 
               T_d = T_c - 1.0d-2   ! Making the triple-point temperature the reference T
! 
               P_eq =  1.0d6*EXP( B00 + ( ( T_d*(BN1 + T_d*(BN2 &
     &                                               + T_d*(BN3 &
     &                                               + T_d* BN4))))   &
     &                                   /( 1.0d0 + T_d*(BD1 &
     &                                            + T_d*(BD2 &
     &                                            + T_d*(BD3 &
     &                                            + T_d* BD4)))) ))
! 
               P_Sat_H2O_S = 0
! 
!***********************************************************************      
!*                                                                     *
!*           Above the critical point, compute a ficticious            *
!*                     saturation pressure (in Pa)                     *
!*                                                                     *
!***********************************************************************      
!
            ELSE IF(T_c > 3.735d2 .AND. T_c <= 2.50d3) THEN  
! 
               T_d = T_c - 3.735d2   ! Making the critical-point temperature the reference T
! 
               P_eq =  1.0d6*EXP( A00 + T_d*(A01 + T_d*(A02 + T_d*A03)))
! 
               P_Sat_H2O_S = 0
! 
! 
!***********************************************************************      
!*                                                                     *
!*    For T a little below the triple point, get the sublimation P     *
!*                                                                     *
!***********************************************************************      
!
!
            ELSE IF (T_c < 1.0d-2 .AND. T_c >= -180.0d0) THEN
! 
               P_Sat_H2O_S = P_Sublm_H2O_S(T_c,P_eq)
! 
! 
!***********************************************************************      
!*                                                                     *
!*             For T outside the range, set a flag value               *
!*                                                                     *
!***********************************************************************      
!
            ELSE             

! 
               P_Sat_H2O_S = 2
               WRITE(6,6001) T_c
! 
            END IF
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'P_Sat_H2O_S       1.0   18 October   2004',6X, &   
     &         'Water saturation pressure as a function ',&
     &         'of temperature (scalar)')
!
 6001 FORMAT(' : : : : : Function "P_Sat_H2O_S": The temperature ',&
     &       1pe15.8,' oC is out of range')
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of P_Sat_H2O_S
!
!
            RETURN
!
         END FUNCTION P_Sat_H2O_S
!
!
!
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! 
!
!
         INTEGER FUNCTION T_Sat_H2O_S(P_eq,T_eq)
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*      Routine for calculating the saturation temperature of H2O      *
!*                     as a function of pressure                       *
!*                                                                     *
!*                   Version 1.0 - October 22, 2004                    *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
            IMPLICIT NONE
! 
! -------------
! ......... Double precision parameters
! -------------
! 
            REAL(KIND=8), PARAMETER :: B00 = -7.3993388900833090d+00, & 
     &                                 BN1 =  7.2669349039428524d-02,  & 
     &                                 BN2 = -1.5553228847117661d-04,  &
     &                                 BN3 =  4.8212464336377295d-07,  &
     &                                 BN4 = -1.1198866718381969d-09,  &
     &                                 BD1 =  1.9864351977571261d-03,  &
     &                                 BD2 = -1.1661892292039663d-06,  &
     &                                 BD3 =  9.1309450737729051d-09,  &
     &                                 BD4 = -6.2620767308454070d-11
! 
            REAL(KIND=8), PARAMETER :: E1 =  1.418878351854323d+01,   &
     &                                 E2 =  1.989502481303133d-01,  &
     &                                 E3 =  3.139547314838871d-01,  &
     &                                 E4 = -5.090673357146564d-02,  &
     &                                 E5 =  5.049671320250361d-03,  &
     &                                 E6 = -1.381410414116421d-04
! 
            REAL(KIND = 8), PARAMETER :: Log_P3 = B00
            REAL(KIND = 8), PARAMETER :: Small  = 1.0d-20
            REAL(KIND = 8), PARAMETER :: Delta  = 1.0d-10
! 
! -------------
! ......... Double precision input/output variables
! -------------
! 
            REAL(KIND=8), INTENT(IN)  :: P_eq  ! The pressure (Pa)
            REAL(KIND=8), INTENT(OUT) :: T_eq  ! The saturation temperature (oC)
! 
! -------------
! ......... Double precision variables
! -------------
! 
            REAL(KIND = 8) :: X_2,X_1,X_0,Y_2,Y_1,Y_0
            REAL(KIND = 8) :: dX,dYdX
! 
! -------------
! ......... Integer patameters
! -------------
! 
            INTEGER, PARAMETER :: MaxIte = 20
! 
! -------------
! ......... Integer variables
! -------------
! 
            INTEGER :: i
! 
! -------------
! ......... Logical variables
! -------------
! 
            LOGICAL :: Converge_Flag
! 
            LOGICAL :: First_call = .TRUE.
! 
! -------------
! ......... Saving variables
! -------------
! 
            SAVE First_call
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of T_Sat_H2O_S
!
!
            IF(First_call) THEN
               First_call = .FALSE.
               IF(MPI_RANK==0) WRITE(11,6000)
            END IF
!
! ------------
! ......... Initialization 
! ------------
! 
            Converge_Flag = .FALSE.
!
! ------------
! ......... First, obtain a very good initial approximation 
! ------------
! 
            Y_1 = LOG(1.0d-6*P_eq) - Log_P3    ! Y_1 stores temporarily the transformed pressure
! 
! ......... Compute two inital temperature estimates
!
            X_1 = Y_1*(E1 + Y_1*(E2 &
     &                    + Y_1*(E3 &
     &                    + Y_1*(E4 &
     &                    + Y_1*(E5 &
     &                   + Y_1*E6)))))
            X_0 = X_1 + 1.0d-2
!
! ------------
! ......... Compute the corresponding pressures 
! ------------
! 
            IF (X_1 >= 0.0d0 .AND. X_1 <= 500.0d0) THEN
               Y_0 =  1.0d6*EXP( B00 +( ( X_0*(BN1 + X_0*(BN2 &
     &                                             + X_0*(BN3 &
     &                                             + X_0* BN4))))   &
     &                                 /( 1.0d0 + X_0*(BD1 &
     &                                          + X_0*(BD2 &
     &                                          + X_0*(BD3 &
     &                                          + X_0*BD4)))))) - P_eq
! 
               Y_1 =  1.0d6*EXP( B00 +( ( X_1*(BN1 + X_1*(BN2 &
     &                                             + X_1*(BN3 &
     &                                             + X_1* BN4))))   &
     &                                 /( 1.0d0 + X_1*(BD1 &
     &                                          + X_1*(BD2 &
     &                                          + X_1*(BD3 &
     &                                          + X_1*BD4)))))) - P_eq
            ELSE 
! 
               T_Sat_H2O_S = 2
               WRITE(6,6001) X_1
               RETURN
! 
            END IF
! 
! **********************************************************************
! *                                                                    *
! *                 CONVERGENCE LOOP - SECANT METHOD                   *
! *                                                                    *
! **********************************************************************
!
            DO_NRSLoop: DO i=1,MaxIte
!
!
!
                  dYdX = (Y_1 - Y_0)/(X_1 - X_0 + Small)
!
!
! -------------------
! ............... Compute F(X_0), dF/dx(X_0), and the equation to solve F(X_0)-V = 0.0d0
! -------------------
!
!
                  IF (ABS(dYdX) <= 1.0d-20) THEN
                     Converge_Flag = .TRUE.
                     X_2 = X_1
                     EXIT DO_NRSLoop
                  ELSE
                     dX  = Y_1/dYdX
                     X_2 = X_1 - dX
                  END IF
!
! -------------------
! ............... Compute updated function value
! -------------------
!
                  IF (X_2 >= 0.0d0 .AND. X_2 <= 500.0d0) THEN
                     Y_2 =  1.0d6*EXP( B00 +( &
     &                      (X_2*(BN1 + X_2*(BN2 + X_2*(BN3 + X_2*BN4&
     &                                                 )&
     &                                      )&
     &                           )&
     &                      )   &
     &                     /(  1.0d0 &
     &                       + X_2*(BD1 + X_2*(BD2 + X_2*(BD3 + X_2*BD4&
     &                                                   )&
     &                                        )&
     &                             )&
     &                      ))) - P_eq
                  ELSE
                     T_Sat_H2O_S = 2
                     WRITE(6,6001) X_2
                     RETURN
                  END IF
!
! -------------------
! .............. Compute the relative error; reset the convergence flag
! -------------------
!
                  IF(ABS(dX)/(ABS(X_2)+Small) < Delta) THEN
                     Converge_Flag = .TRUE.
                     EXIT DO_NRSLoop
                  END IF
!
! -------------------
! ............... Reset X_0, X_1 and Y_0, Y_1
! -------------------
!
                  X_0 = X_1
                  X_1 = X_2
                  Y_0 = Y_1
                  Y_1 = Y_2
!
! <<<<<<<<<
! <<<<<<<<< End of the convergence loop
! <<<<<<<<<
!
            END DO DO_NRSLoop
!
! -------------
! ......... Assign the final saturation temperature
! -------------
!
            T_eq = X_2 + 1.0d-2  
!
! ......... Print a message if convergence has NOT been achieved within MaxIte
!
            IF(Converge_Flag .EQV. .FALSE.) WRITE(6,6002)
! 
! **********************************************************************
! *                                                                    *
! *                    FUNCTION VALUE ASSIGNMENT                       *
! *                                                                    *
! **********************************************************************
!
            T_Sat_H2O_S = 0   ! The function value indicates success status
! 
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'T_Sat_H2O_S       1.0   22 October   2004',6X,   &    
     &         'Water sublimation temperature as a function of ',&
     &         'pressure (scalar)')  
!
 6001 FORMAT(' : : : : : Function "T_Sat_H2O_S": The temperature ',&
     &       1pe15.8,' oC is out of range')
!
 6002 FORMAT(/,' ===> WARNING :',   &
     &      ' No convergence within "MaxIte" in function "T_Sat_H2O_S"')
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of T_Sat_H2O_S
!
!
            RETURN
!
         END FUNCTION T_Sat_H2O_S
!
!
!
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! 
!
!
         INTEGER FUNCTION P_Sublm_H2O_S(T_c,P_eq)
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*       Routine for calculating the sublimation pressure of H2O       *
!*                    as a function of temperature                     *
!*                                                                     *
!*                   Version 1.0 - October 18, 2004                    *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
            IMPLICIT NONE
! 
! -------------
! ......... Double precision parameters
! -------------
! 
            REAL(KIND=8), PARAMETER :: S1  = -1.39281690d+01,   & 
     &                                 S2  =  3.47078238d+01
! 
! -------------
! ......... Double precision input/output variables
! -------------
! 
            REAL(KIND=8), INTENT(IN)  :: T_c   ! The temperature in oC
! 
            REAL(KIND=8), INTENT(OUT) :: P_eq  ! The sublimation P in Pa
! 
! -------------
! ......... Double precision arrays
! -------------
! 
            REAL(KIND=8) :: T_d
! 
! -------------
! ......... Logical variables
! -------------
! 
            LOGICAL :: First_call = .TRUE.
! 
! -------------
! ......... Saving variables
! -------------
! 
            SAVE First_call
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of P_Sublm_H2O_S
!
!
            IF(First_call) THEN
               First_call = .FALSE.
               IF(MPI_RANK==0) WRITE(11,6000)
            END IF
!
! ------------
! ......... Check range
! ------------
! 

            T_d = (T_c + 273.15d0)/273.16d0   ! Reduced T with respect to T3
! 
!***********************************************************************      
!*                                                                     *
!*              Compute the sublimation pressure (in Pa)               *
!*                                                                     *
!***********************************************************************      
!
            IF (T_c <= 1.0d-2 .AND. T_c >= -180.0d0) THEN
!
               P_eq = 611.657d0*EXP(S1*(  1.0d0 - T_d**(-1.50d0)) &
     &                                  + S2*(1.0d0 - T_d**(-1.25d0)))
!
               P_Sublm_H2O_S = 0
! 
! 
!***********************************************************************      
!*                                                                     *
!*    For T a little above the triple point, get the sublimation P     *
!*                                                                     *
!***********************************************************************      
!
!
            ELSE IF(T_d > 1.0d-2 .AND. T_d <= 500.0d0) THEN  
!
               P_Sublm_H2O_S = P_Sat_H2O_S(T_c,P_eq)
! 
! 
!***********************************************************************      
!*                                                                     *
!*             For T outside the range, set a flag value               *
!*                                                                     *
!***********************************************************************      
!
            ELSE
!
! ............ Out of range
!
               P_Sublm_H2O_S = 2
               WRITE(6,6001) T_c
! 
            END IF
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'P_Sublm_H2O_S     1.0   18 October   2004',6X, &  
     &         'Water sublimation pressure as a function of ',&
     &         'temperature (scalar)')
!
 6001 FORMAT(' : : : : : Function "P_Sublm_H2O_S": The temperature ',&
     &       1pe12.5,' oC is out of range')
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of P_Sublm_H2O_S
!
!
            RETURN
!
         END FUNCTION P_Sublm_H2O_S
!
!
!
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! 
!
!
         INTEGER FUNCTION T_Sublm_H2O_S(P_eq,T_eq)
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*     Routine for calculating the sublimation temperature of H2O      *
!*                     as a function of pressure                       *
!*                                                                     *
!*                   Version 1.0 - October 22, 2004                    *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
            IMPLICIT NONE
! 
! -------------
! ......... Double precision parameters
! -------------
! 
            REAL(KIND=8), PARAMETER :: S1  = -1.39281690d+01, &   
     &                                 S2  =  3.47078238d+01
! 
            REAL(KIND=8), PARAMETER :: C1 =  1.214426043765534D+01,    &
     &                                 C2 =  5.443065233003680d-01,   &
     &                                 C3 =  2.465692259519613d-02,   &
     &                                 C4 =  1.056890168593228d-03,   &
     &                                 C5 =  3.597346500755430d-05,   &
     &                                 C6 =  6.587778126010190d-07
! 
            REAL(KIND = 8), PARAMETER :: Log_P3 = -7.3993388900833090d0
            REAL(KIND = 8), PARAMETER :: Small  =  1.0d-20
            REAL(KIND = 8), PARAMETER :: Delta  =  1.0d-10
! 
! -------------
! ......... Double precision input/output variables
! -------------
! 
            REAL(KIND=8), INTENT(IN)  :: P_eq   ! The pressure in Pa
            REAL(KIND=8), INTENT(OUT) :: T_eq   ! The sublimation temperature in oC
! 
! -------------
! ......... Double precision variables
! -------------
! 
            REAL(KIND = 8) :: X_2,X_1,X_0,Y_2,Y_1,Y_0
            REAL(KIND = 8) :: dX,dYdX
! 
! -------------
! ......... Integer patameters
! -------------
! 
            INTEGER, PARAMETER :: MaxIte = 20
! 
! -------------
! ......... Integer variables
! -------------
! 
            INTEGER :: i
! 
! -------------
! ......... Logical variables
! -------------
! 
            LOGICAL :: Converge_Flag
! 
            LOGICAL :: First_call = .TRUE.
! 
! -------------
! ......... Saving variables
! -------------
! 
            SAVE First_call
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of T_Sublm_H2O_S
!
!
            IF(First_call) THEN
               First_call = .FALSE.
               IF(MPI_RANK==0) WRITE(11,6000)
            END IF
!
! ------------
! ......... Initialization 
! ------------
! 
            Converge_Flag = .FALSE.
!
! ------------
! ......... First, obtain a very good initial approximation 
! ------------
! 
            Y_1 = LOG(1.0d-6*P_eq) - Log_P3    ! Y_1 stores temporarily the transformed pressure
! 
! ......... Compute an inital temperature estimate 
! 
            X_1 = Y_1*(C1 + Y_1*(C2 + Y_1*(C3 + Y_1*(C4 &
     &                    + Y_1*(C5 + Y_1*C6)))))
!
! ------------
! ......... Compute the corresponding pressures 
! ------------
! 
            IF (X_1 < 1.1d-2 .AND. X_1 >= -180.0d0) THEN
! 
! ............ Compute the reduced temperatures with respect to T_3
! 
               X_1 = (X_1 + 273.15d0)/273.16d0
               X_0 = (X_1 + 273.14d0)/273.16d0   ! The second approximate solution
! 
! ............ Compute the corresponding Y(X)-Val
! 
               Y_0 = 611.657d0*EXP(  S1*(1.0d0 - X_0**(-1.50d0)) &
     &                             + S2*(1.0d0 - X_0**(-1.25d0))) - P_eq
               Y_1 = 611.657d0*EXP(  S1*(1.0d0 - X_1**(-1.50d0)) &
     &                             + S2*(1.0d0 - X_1**(-1.25d0))) - P_eq
! 
            ELSE 
! 
               T_Sublm_H2O_S = 2
               WRITE(6,6001) X_1
               RETURN
! 
            END IF
! 
! **********************************************************************
! *                                                                    *
! *                 CONVERGENCE LOOP - SECANT METHOD                   *
! *                                                                    *
! **********************************************************************
!
            DO_NRSLoop: DO i=1,MaxIte
!
                  dYdX = (Y_1 - Y_0)/(X_1 - X_0 + Small)
!
!
! -------------------
! ............... Compute F(X_0), dF/dx(X_0), and the equation to solve F(X_0)-V = 0.0d0
! -------------------
!
!
                  IF (ABS(dYdX) <= 1.0d-20) THEN
                     Converge_Flag = .TRUE.
                     X_2 = X_1
                     EXIT DO_NRSLoop
                  ELSE
                     dX  = Y_1/dYdX
                     X_2 = X_1 - dX
                  END IF
!
! -------------------
! ............... Compute updated function value
! -------------------
!
                  IF (X_2 < 1.0d0 .AND. X_2 >= 3.41d-1) THEN
                     Y_2 = 611.657d0*EXP(  S1*(1.0d0 - X_2**(-1.50d0)) &
     &                                   + S2*(1.0d0 - X_2**(-1.25d0))&
     &                                  ) - P_eq
                  ELSE
                     T_Sublm_H2O_S = 2
                     WRITE(6,6001) X_2
                     RETURN
                  END IF
!
! -------------------
! .............. Compute the relative error; reset the convergence flag
! -------------------
!
                  IF(ABS(dX)/(ABS(X_2)+Small) < Delta) THEN
                     Converge_Flag = .TRUE.
                     EXIT DO_NRSLoop
                  END IF
!
! -------------------
! ............... Reset X_0, X_1 and Y_0, Y_1
! -------------------
!
                  X_0 = X_1
                  X_1 = X_2
                  Y_0 = Y_1
                  Y_1 = Y_2
!
! <<<<<<<<<<<<
! <<<<<<<<<<<< End of the convergence loop
! <<<<<<<<<<<<
!
               END DO DO_NRSLoop
!
! -------------
! ......... Assign the final sublimation temperature
! -------------
!
            T_eq = X_2*273.16d0-273.15d0
!
! ......... Print a message if convergence has NOT been achieved within MaxIte
!
            IF(Converge_Flag .EQV. .FALSE.) WRITE(6,6002)
! 
! **********************************************************************
! *                                                                    *
! *                    FUNCTION VALUE ASSIGNMENT                       *
! *                                                                    *
! **********************************************************************
!
            T_Sublm_H2O_S = 0   ! The function value indicates success status
! 
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'T_Sublm_H2O_S     1.0   22 October   2004',6X, &       
     &         'Water sublimation temperature as a function ',&
     &         'of pressure')  
!
 6001 FORMAT(' : : : : : Function "T_Sublm_H2O_S": The temperature ',&
     &       1pe15.8,' oC is out of range')
!
 6002 FORMAT(/,' ===> WARNING : No convergence within',  &  
     &         ' "MaxIte" in function "T_Sublm_H2O_S"')
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of T_Sublm_H2O_S
!
!
            RETURN
!
         END FUNCTION T_Sublm_H2O_S
!
!
!
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! 
!
!
         INTEGER FUNCTION H_Sublm_H2O_S(T_c,H_sub)
! 
! ......... Modules to be used 
! 
            USE H2O_Param
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*       Routine for calculating the sublimation enthalpy of H2O       *
!*                    as a function of temperature                     *
!*                                                                     *
!*                   Version 1.0 - October 18, 2004                    *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
            IMPLICIT NONE
! 
! -------------
! ......... Double precision parameters
! -------------
! 
            REAL(KIND=8), PARAMETER :: S1  = -9.642370863481d+03,  &
     &                                 S2  =  2.0023313242865d+04
! 
! -------------
! ......... Double precision input/output variables
! -------------
! 
            REAL(KIND=8), INTENT(IN)  :: T_c    ! The temperature in oC
! 
            REAL(KIND=8), INTENT(OUT) :: H_sub  ! The sublimation H in J/Kg
! 
! -------------
! ......... Double precision arrays
! -------------
! 
            REAL(KIND=8) :: T_k,T_d
! 
! -------------
! ......... Logical variables
! -------------
! 
            LOGICAL :: First_call = .TRUE.
! 
! -------------
! ......... Saving variables
! -------------
! 
            SAVE First_call
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of H_Sublm_H2O_S
!
!
            IF(First_call) THEN
               First_call = .FALSE.
               IF(MPI_RANK==0) WRITE(11,6000)
            END IF
!
! ------------
! ......... Check range
! ------------
! 
            T_k = T_c + 273.15d0   ! Kelvin T 
            T_d = T_k/273.16d0     ! Reduced T with respect to T3
! 
!***********************************************************************      
!*                                                                     *
!*         Compute the specific sublimation enthalpy (in J/kg)         *
!*                                                                     *
!***********************************************************************      
!
            IF (T_c <= 1.1d-2 .AND. T_c >= -180.0d0) THEN
!
               H_Sub = T_k*(S1*(T_d**(-1.50d0)) + S2*(T_d**(-1.25d0)) )
!
               H_Sublm_H2O_S = 0
! 
! 
!***********************************************************************      
!*                                                                     *
!*             For T outside the range, set a flag value               *
!*                                                                     *
!***********************************************************************      
!
            ELSE
!
! ............ Out of range
!
               H_Sublm_H2O_S = 2
               WRITE(6,6001) T_c
! 
            END IF
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'H_Sublm_H2O_S     1.0   18 October   2004',6X,  &  
     &         'Specific heat of water sublimation as a function of ',&
     &         'temperature (scalar)')
!
 6001 FORMAT(' : : : : : Function "H_Sublm_H2O_S": The temperature ',&
     &       1pe15.8,' oC is out of range')
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of H_Sublm_H2O_S
!
!
            RETURN
!
         END FUNCTION H_Sublm_H2O_S
!
!
!
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! 
!
!
         INTEGER FUNCTION P_Fusion_H2O_S(T_c,P_eq)
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*         Routine for calculating the fusion pressure of H2O          *
!*                    as a function of temperature                     *
!*                                                                     *
!*                   Version 1.0 - October 25, 2004                    *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
            IMPLICIT NONE
! 
! -------------
! ......... Double precision parameters
! -------------
! 
            REAL(KIND=8), PARAMETER :: F1 = -6.26D+05, F2 = 1.97135d+05
! 
! -------------
! ......... Double precision input/output variables
! -------------
! 
            REAL(KIND=8), INTENT(IN)  :: T_c   ! The temperature in oC
! 
            REAL(KIND=8), INTENT(OUT) :: P_eq  ! The sublimation P in Pa
! 
! -------------
! ......... Double precision arrays
! -------------
! 
            REAL(KIND=8) :: T_d
! 
! -------------
! ......... Logical variables
! -------------
! 
            LOGICAL :: First_call = .TRUE.
! 
! -------------
! ......... Saving variables
! -------------
! 
            SAVE First_call
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of P_Fusion_H2O_S
!
!
            IF(First_call) THEN
               First_call = .FALSE.
               IF(MPI_RANK==0) WRITE(11,6000)
            END IF
!
! ------------
! ......... Check range
! ------------
! 

            T_d = (T_c + 273.15d0)/273.16d0   ! Reduced T with respect to T3
! 
!***********************************************************************      
!*                                                                     *
!*                Compute the fusion pressure (in Pa)                  *
!*                                                                     *
!***********************************************************************      
!
            IF (T_c <= 1.1d-2 .AND. T_c >= -23.0d0) THEN
!
               P_eq = 611.657d0*(1.0d0 + F1*(1.0d0 - T_d**(-3.00d0)) &
     &                                 + F2*(1.0d0 - T_d**(21.00d0)))
!
               P_Fusion_H2O_S = 0
! 
! 
!***********************************************************************      
!*                                                                     *
!*             For T outside the range, set a flag value               *
!*                                                                     *
!***********************************************************************      
!
            ELSE
! 
               P_Fusion_H2O_S = 2
               WRITE(6,6001) T_c
! 
            END IF
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'P_Fusion_H2O_S    1.0   25 October   2004',6X,   & 
     &         'Water fusion pressure as a function of ',&
     &         'temperature (scalar)')
!
 6001 FORMAT(' : : : : : Function "P_Fusion_H2O_S": The temperature ',&
     &       1pe15.8,' oC is out of range')
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of P_Fusion_H2O_S
!
!
            RETURN
!
         END FUNCTION P_Fusion_H2O_S
!
!
!
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! 
!
!
         INTEGER FUNCTION T_Fusion_H2O_S(P_eq,T_eq)
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*       Routine for calculating the fusion temperature of H2O         *
!*                     as a function of pressure                       *
!*                                                                     *
!*                   Version 1.0 - October 25, 2004                    *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
            IMPLICIT NONE
! 
! -------------
! ......... Double precision parameters
! -------------
! 
            REAL(KIND=8), PARAMETER :: F1 = -6.26D+05, F2 = 1.97135d+05
! 
            REAL(KIND=8), PARAMETER :: CN1 = -7.3729611943945894d-02, &   
     &                                 CN2 =  2.6629015377742257d-04,  & 
     &                                 CN3 = -4.9096971912843264d-07,   &
     &                                 CD1 = -5.3238860766769243d-03,   &
     &                                 CD2 =  1.3204367441317687d-05,   &
     &                                 CD3 = -9.4779298870961774d-09
! 
            REAL(KIND = 8), PARAMETER :: Small  =  1.0d-20
            REAL(KIND = 8), PARAMETER :: Delta  =  1.0d-10
! 
! -------------
! ......... Double precision input/output variables
! -------------
! 
            REAL(KIND=8), INTENT(IN)  :: P_eq   ! The pressure in Pa
            REAL(KIND=8), INTENT(OUT) :: T_eq   ! The sublimation temperature in oC
! 
! -------------
! ......... Double precision variables
! -------------
! 
            REAL(KIND = 8) :: X_2,X_1,X_0,Y_2,Y_1,Y_0
            REAL(KIND = 8) :: dX,dYdX
! 
! -------------
! ......... Integer patameters
! -------------
! 
            INTEGER, PARAMETER :: MaxIte = 50
! 
! -------------
! ......... Integer variables
! -------------
! 
            INTEGER :: i
! 
! -------------
! ......... Logical variables
! -------------
! 
            LOGICAL :: Converge_Flag
! 
            LOGICAL :: First_call = .TRUE.
! 
! -------------
! ......... Saving variables
! -------------
! 
            SAVE First_call
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of T_Fusion_H2O_S
!
!
            IF(First_call) THEN
               First_call = .FALSE.
               IF(MPI_RANK==0) WRITE(11,6000)
            END IF
!
! ------------
! ......... Initialization 
! ------------
! 
            Converge_Flag = .FALSE.
!
! ------------
! ......... First, obtain a very good initial approximation of T
! ------------
! 
            Y_1 = 1.0d-6*P_eq - 611.657d-6    ! Y_1 stores temporarily the transformed pressure
! 
! ......... Compute the inital temperature estimate 
! 
            X_1 =  (         Y_1*(CN1 + Y_1*(CN2 + Y_1*CN3)))  &  
     &            /( 1.0d0 + Y_1*(CD1 + Y_1*(CD2 + Y_1*CD3)))
!
! ------------
! ......... Compute the corresponding pressures 
! ------------
! 
            IF (X_1 < 1.1d-2 .AND. X_1 >= -23.0d0) THEN
! 
! ............ Compute the reduced temperatures with respect to T_3
! 
               X_1 = (X_1 + 273.150d0)/273.16d0
               X_0 = (X_1 + 273.149d0)/273.16d0   ! The second approximate solution
! 
! ............ Compute the corresponding Y(X)-Val
! 
               Y_0 = 611.657d0*(  1.0d0 &
     &                          + F1*(1.0d0 - X_0**(-3.0d0)) &
     &                          + F2*(1.0d0 - X_0**(21.0d0))&
     &                         ) - P_eq
! 
               Y_1 = 611.657d0*(  1.0d0 &
     &                          + F1*(1.0d0 - X_1**(-3.0d0)) &
     &                          + F2*(1.0d0 - X_1**(21.0d0))&
     &                         ) - P_eq
! 
            ELSE 
! 
               T_Fusion_H2O_S = 2
               WRITE(6,6001) X_1
               RETURN
! 
            END IF
! 
! **********************************************************************
! *                                                                    *
! *                 CONVERGENCE LOOP - SECANT METHOD                   *
! *                                                                    *
! **********************************************************************
!
            DO_NRSLoop: DO i=1,MaxIte
!
!
!
                  dYdX = (Y_1 - Y_0)/(X_1 - X_0 + Small)
!
!
! -------------------
! ............... Compute F(X_0), dF/dx(X_0), and the equation to solve F(X_0)-V = 0.0d0
! -------------------
!
!
                  IF (ABS(dYdX) <= 1.0d-20) THEN
                     Converge_Flag = .TRUE.
                     X_2 = X_1
                     EXIT DO_NRSLoop
                  ELSE
                     dX  = Y_1/dYdX
                     X_2 = X_1 - dX
                  END IF
!
! -------------------
! ............... Compute updated function value
! -------------------
!
                  IF (X_2 < 1.0d0 .AND. X_2 >= 9.158d-1) THEN
                     Y_2 = 611.657d0*(  1.0d0 &
     &                                + F1*(1.0d0 - X_2**(-3.0d0)) &
     &                                + F2*(1.0d0 - X_2**(21.0d0))&
     &                               ) - P_eq
                  ELSE
                     T_Fusion_H2O_S = 2
                     WRITE(6,6001) X_2
                     RETURN
                  END IF
!
! -------------------
! .............. Compute the relative error; reset the convergence flag
! -------------------
!
                  IF(ABS(dX)/(ABS(X_2)+Small) < Delta) THEN
                     Converge_Flag = .TRUE.
                     EXIT DO_NRSLoop
                  END IF
!
! -------------------
! ............... Reset X_0, X_1 and Y_0, Y_1
! -------------------
!
                  X_0 = X_1
                  X_1 = X_2
                  Y_0 = Y_1
                  Y_1 = Y_2
!
! <<<<<<<<<
! <<<<<<<<< End of the convergence loop
! <<<<<<<<<
!
            END DO DO_NRSLoop
!
! -------------
! ......... Assign the final sublimation temperature
! -------------
!
            T_eq = X_2*273.16d0-273.15d0
!
! ......... Print a message if convergence has NOT been achieved within MaxIte
!
            IF(Converge_Flag .EQV. .FALSE.) WRITE(6,6002)
! 
! **********************************************************************
! *                                                                    *
! *                    FUNCTION VALUE ASSIGNMENT                       *
! *                                                                    *
! **********************************************************************
!
            T_Fusion_H2O_S = 0   ! The function value indicates success status
! 
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'T_Fusion_H2O_S    1.0   25 October   2004',6X,  &      
     &         'Water sublimation temperature as a function of ',&
     &         'pressure (scalar)')  
!
 6001 FORMAT(' : : : : : Function "T_Fusion_H2O_S": The temperature ',&
     &       1pe15.8,' oC is out of range')
!
 6002 FORMAT(/,' ===> WARNING : No convergence within "MaxIte" in', & 
     &         ' function "T_Fusion_H2O_S"')
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of T_Fusion_H2O_S
!
!
            RETURN
!
         END FUNCTION T_Fusion_H2O_S
!
!
!
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! 
!
!
         INTEGER FUNCTION H_Fusion_H2O_S(T_c,H_fus)
! 
! ......... Modules to be used 
! 
            USE H2O_Param
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*         Routine for calculating the fusion enthalpy of H2O          *
!*                    as a function of temperature                     *
!*                                                                     *
!*                   Version 1.0 - October 25, 2004                    *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
            IMPLICIT NONE
! 
! -------------
! ......... Double precision parameters
! -------------
! 
            REAL(KIND=8), PARAMETER :: F1 = -1.148691846d+09, &
     &                                 F2 = -2.532159056595d+09
! 
! -------------
! ......... Double precision input/output variables
! -------------
! 
            REAL(KIND=8), INTENT(IN)  :: T_c    ! The temperature in oC
! 
            REAL(KIND=8), INTENT(OUT) :: H_fus  ! The sublimation H in J/kg
! 
! -------------
! ......... Double precision arrays
! -------------
! 
            REAL(KIND=8) :: T_d,P_fus,rho_i,rho_L,U_wat,H_wat
! 
! -------------
! ......... Integer variables
! -------------
! 
            INTEGER :: IGOOD
! 
! -------------
! ......... Logical variables
! -------------
! 
            LOGICAL :: First_call = .TRUE.
! 
! -------------
! ......... Saving variables
! -------------
! 
            SAVE First_call
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of H_Fusion_H2O_S
!
!
            IF(First_call) THEN
               First_call = .FALSE.
               IF(MPI_RANK==0) WRITE(11,6000)
            END IF
!
! ------------
! ......... Check range
! ------------
! 

            T_d = (T_c + 273.15d0)/273.16d0   ! Reduced T with respect to T3
! 
!***********************************************************************      
!*                                                                     *
!*                Compute the fusion pressure (in Pa)                  *
!*                                                                     *
!***********************************************************************      
!
            IF (T_c <= 1.1d-2 .AND. T_c >= -23.0d0) THEN
!
! ............ Compute the fusion pressure at T_c
!
               IGOOD = P_Fusion_H2O_S(T_c,P_fus)   
!
! ............ Compute the liquid water density at P_fusion, T_c
!
               CALL UHRho_LiqH2O(T_c,P_fus,rho_L,U_wat,H_wat,IGOOD)
!
! ............ Compute the ice density at P_fusion, T_c
!
               rho_i = Ice_Density(T_c,P_fus)    
!
! ............ Compute the specific enthalpy of fusion
!
               H_fus = (F1*(T_d**(-3))+ F2*(T_d**(21.0d0))) &
     &                *(1.0d0/rho_L - 1.0d0/rho_i)
!
! ............ Set the sucess flag
!
               H_Fusion_H2O_S = 0
! 
! 
!***********************************************************************      
!*                                                                     *
!*             For T outside the range, set a flag value               *
!*                                                                     *
!***********************************************************************      
!
            ELSE
! 
               H_Fusion_H2O_S = 2
               WRITE(6,6001) T_c
! 
            END IF
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'H_Fusion_H2O_S    1.0   25 October   2004',6X, &   
     &         'Specific enthalpy of water fusion as a function of ',&
     &         'temperature (scalar)')
!
 6001 FORMAT(' : : : : : Function "H_Fusion_H2O_S": The temperature ',&
     &       1pe15.8,' oC is out of range')
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of H_Fusion_H2O_S
!
!
            RETURN
!
         END FUNCTION H_Fusion_H2O_S
!
!
!
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
!
!
!
         SUBROUTINE SIGMA(T,ST)
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*          Routine for calculating the surface tension of H2O         *
!*                                                                     *
!*                     Version 1.0 - April 9, 2003                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
            IMPLICIT NONE
! 
! -------------
! ......... Double precision variables
! -------------
! 
            REAL(KIND=8), INTENT(IN)  :: T
            REAL(KIND=8), INTENT(OUT) :: ST
! 
            REAL(KIND=8) :: T1
! 
! -------------
! ......... Logical variables
! -------------
! 
            LOGICAL :: First_call = .TRUE.
! 
! -------------
! ......... Saving variables
! -------------
! 
            SAVE First_call
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of SIGMA
!
!
            IF(First_call) THEN
               First_call = .FALSE.
               IF(MPI_RANK==0) WRITE(11,6000)
            END IF
!
! ------------
! ......... Check T in relation to the critical temperature of H2O
! ------------
! 
            IF_TCrit: IF(T >= 3.7415d2) THEN
                         ST = 0.0d0
                      ELSE
                         T1 = (374.15d0-T)/647.3d0
                         ST = 0.2358d0*(1.0d0-0.625d0*T1)*(T1**1.256d0)
                      END IF IF_TCrit
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'SIGMA             1.0    9 April     2003',6X, &  
     &         'Surface tension of water as function of temperature')
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of SIGMA
!
!
            RETURN
!
         END SUBROUTINE SIGMA
!
!
!
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
!
!
!
         SUBROUTINE ViscLV_H2O(T,P,D,VW,VS,PS)
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*  Routine for calculating the viscosity of liquid H2O and H2O vapor  *
!*                                                                     *
!*                     Version 1.0 - April 9, 2003                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
            IMPLICIT NONE
! 
! -------------
! ......... Double precision variables
! -------------
! 
            REAL(KIND=8), INTENT(IN)  :: T,P,D,PS
            REAL(KIND=8), INTENT(OUT) :: VW,VS
! 
            REAL(KIND=8) :: EX,PHI,AM,V1
! 
! -------------
! ......... Logical variables
! -------------
! 
            LOGICAL :: First_call = .TRUE.
! 
! -------------
! ......... Saving variables
! -------------
! 
            SAVE First_call
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of ViscLV_H2O 
!
!
            IF(First_call) THEN
               First_call = .FALSE.
               IF(MPI_RANK==0) WRITE(11,6000)
            END IF
!
! ------------
! ......... Viscosity of liquid water
! ------------
! 
            EX  = 2.478d2/(T+1.3315d2)
            PHI = 1.0467d0*(T-3.185d1)
            AM  = 1.0d0+PHI*(P-PS)*1.0d-11
! 
            VW  = 1.0d-7*AM*2.414d2*(1.0d1**EX)
!
! ------------
! ......... Viscosity of water vapor (steam)
! ------------
! 
            V1 = 4.07d-1*T+8.04d1
! 
            IF_TRange: IF(T <= 3.5d2) THEN
               VS = 1.d-7*(V1-D*(1.858d3-5.9d0*T)*1.0d-3)
            ELSE
               VS = 1.d-7*(V1+3.53d-1*D+6.765d-4*D*D+1.021d-7*D*D*D)
            END IF IF_TRange
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'ViscLV_H2O        1.0    9 April     2003',6X, & 
     &         'Viscosity of liquid water and vapor as a ',     & 
     &         'function of temperature and pressure')
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of ViscLV_H2O
!
!
            RETURN
!
         END SUBROUTINE ViscLV_H2O
!
!
!
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
!
!
!
         SUBROUTINE VISC_VapH2O(T,P,D,VS)
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*      Routine for calculating the viscosity of H2O vapor (steam)     *
!*                                                                     *
!*                     Version 1.0 - April 9, 2003                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
            IMPLICIT NONE
! 
! -------------
! ......... Double precision variables
! -------------
! 
            REAL(KIND=8), INTENT(IN)  :: T,P,D
            REAL(KIND=8), INTENT(OUT) :: VS
! 
            REAL(KIND=8) :: V1
! 
! -------------
! ......... Logical variables
! -------------
! 
            LOGICAL :: First_call = .TRUE.
! 
! -------------
! ......... Saving variables
! -------------
! 
            SAVE First_call
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of VISC_VapH2O
!
!
            IF(First_call) THEN
               First_call = .FALSE.
               IF(MPI_RANK==0) WRITE(11,6000)
            END IF
!
! ------------
! ......... Viscosity of water vapor (steam)
! ------------
! 
            V1 = 4.07d-1*T+8.04d1
! 
            IF_TRange: IF(T <= 3.5d2) THEN
               VS = 1.d-7*(V1-D*(1.858d3-5.9d0*T)*1.0d-3)
            ELSE
               VS = 1.d-7*(V1+3.53d-1*D+6.765d-4*D*D+1.021d-7*D*D*D)
            END IF IF_TRange
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'VISC_VapH2O       1.0    9 April     2003',6X, & 
     &         'Viscosity of water vapor (steam) as a ',        & 
     &         'function of temperature and pressure')
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of VISC_VapH2O
!
!
            RETURN
!
         END SUBROUTINE VISC_VapH2O
!
!
!
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
!
!
!
         SUBROUTINE VISC_LiqH2O(T,P,PS,VW)
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*         Routine for calculating the viscosity of liquid H2O         *
!*                                                                     *
!*                     Version 1.0 - April 9, 2003                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
            IMPLICIT NONE
! 
! -------------
! ......... Double precision variables
! -------------
! 
            REAL(KIND=8), INTENT(IN)  :: T,P,PS
            REAL(KIND=8), INTENT(OUT) :: VW
! 
            REAL(KIND=8) :: EX,PHI,AM
! 
! -------------
! ......... Logical variables
! -------------
! 
            LOGICAL :: First_call = .TRUE.
! 
! -------------
! ......... Saving variables
! -------------
! 
            SAVE First_call
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of VISC_LiqH2O
!
!
            IF(First_call) THEN
               First_call = .FALSE.
               IF(MPI_RANK==0) WRITE(11,6000)
            END IF
!
! ------------
! ......... Viscosity of liquid water
! ------------
! 
            EX  = 2.478d2/(T+1.3315d2)
            PHI = 1.0467d0*(T-3.185d1)
            AM  = 1.0d0+PHI*(P-PS)*1.0d-11
! 
            VW  = 1.0d-7*AM*2.414d2*(1.0d1**EX)
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'VISC_LiqH2O       1.0    9 April     2003',6X, & 
     &         'Viscosity of liquid water as a ',               & 
     &         'function of temperature and pressure')
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of VISC_LiqH2O
!
!
            RETURN
!
         END SUBROUTINE VISC_LiqH2O
!
!
!
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
!
!
!
         REAL(KIND = 8) FUNCTION ViscLV_H2Ox(T_c,DX)
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*    Routine for calculating the viscosity of liquid or vapor H2O     *
!*              Extended range: 0<TX<800\BFC, 0<PX<1000 bar              *
!*     ASME [1977] + Corrections of Haar, Gallagher & Kell [1984]      *
!*                                                                     *
!*                     Version 1.0 - April 9, 2003                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
			
            IMPLICIT NONE
! 
! -------------
! ......... Double precision parameters
! -------------
! 
            REAL(KIND=8), PARAMETER :: A0 = 0.0181583d0,  &  
     &                                 A1 = 0.0177624d0,   &   
     &                                 A2 = 0.0105287d0,    & 
     &                                 A3 =-0.0036744d0 
! 
            REAL(KIND=8), PARAMETER :: B00 = 0.5019380d0,   &
     &                                 B01 = 0.2356220d0,    & 
     &                                 B02 =-0.2746370d0,    &
     &                                 B03 = 0.1458310d0,    &
     &                                 B04 =-0.0270448d0 
! 
            REAL(KIND=8), PARAMETER :: B10 = 0.1628880d0, &    
     &                                 B11 = 0.7893930d0,  & 
     &                                 B12 =-0.7435390d0,   & 
     &                                 B13 = 0.2631290d0,    &
     &                                 B14 =-0.0253093d0 
! 
            REAL(KIND=8), PARAMETER :: B20 =-0.1303560d0,   &  
     &                                 B21 = 0.6736650d0,    & 
     &                                 B22 =-0.9594560d0,    &
     &                                 B23 = 0.3472470d0,    &
     &                                 B24 =-0.0267758d0 
! 
            REAL(KIND=8), PARAMETER :: B30 = 0.9079190d0,   &  
     &                                 B31 = 1.2075520d0,    &
     &                                 B32 =-0.6873430d0,    &
     &                                 B33 = 0.2134860d0,    &
     &                                 B34 =-0.0822904d0 
! 
            REAL(KIND=8), PARAMETER :: B40 =-0.5511190d0,   &  
     &                                 B41 = 0.0670665d0,    & 
     &                                 B42 =-0.4970890d0,    &
     &                                 B43 = 0.1007540d0,    &
     &                                 B44 = 0.0602253d0 
! 
            REAL(KIND=8), PARAMETER :: B50 = 0.1465430d0,   &  
     &                                 B51 =-0.0843370d0,    & 
     &                                 B52 = 0.1952860d0,   &
     &                                 B53 =-0.0329320d0,   &
     &                                 B54 =-0.0202595d0 
! -------------
! ......... Double precision variables
! -------------
! 
            REAL(KIND=8), INTENT(IN)  :: T_c, &    ! T in \BFC
     &                                   DX       ! Density in kg/m^3
! 
            REAL(KIND=8) :: TK,TR,T2R,TRI
            REAL(KIND=8) :: TR1,TR2,TR3,TR4,TR5
            REAL(KIND=8) :: DR,DR1,DR2,DR3,DR4
            REAL(KIND=8) :: AMU0,SUM
! 
! -------------
! ......... Logical variables
! -------------
! 
            LOGICAL :: First_call = .TRUE.
! 
! -------------
! ......... Saving variables
! -------------
! 
            SAVE First_call
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of ViscLV_H2Ox
!
!
            IF(First_call) THEN
               First_call = .FALSE.
               IF(MPI_RANK==0) WRITE(11,6000)
            END IF
!
! ------------
! ........ Temperature computations
! ------------
! 
           TK  = T_c+273.15d0
           TR  = 647.27d0/TK
           T2R = TR*TR
           TRI = 1.0d0/TR
!
           TR1 = TR-1.0d0
           TR2 = TR1*TR1
           TR3 = TR2*TR1
           TR4 = TR3*TR1
           TR5 = TR4*TR1
!
! ------------
! ........ Density computations
! ------------
! 
           DR  = DX/317.763d0
! 
           DR1 = DR-1.0d0
           DR2 = DR1*DR1
           DR3 = DR2*DR1
           DR4 = DR3*DR1
!
! ------------
! ........ Computation of the T-dependent quantity
! ------------
! 
           AMU0 = 1.0d-6*(SQRT(TRI)/(A0+A1*TR+A2*T2R+A3*TR*T2R))
!
! ------------
! ........ Computation of the T- and D-dependent quantity
! ------------
! 
           SUM =  B00+B01*DR1+B02*DR2+B03*DR3+B04*DR4  &      
     &          +(B10+B11*DR1+B12*DR2+B13*DR3+B14*DR4)*TR1   &
     &          +(B20+B21*DR1+B22*DR2+B23*DR3+B24*DR4)*TR2   &
     &          +(B30+B31*DR1+B32*DR2+B33*DR3+B34*DR4)*TR3   &
     &          +(B40+B41*DR1+B42*DR2+B43*DR3+B44*DR4)*TR4   &
     &          +(B50+B51*DR1+B52*DR2+B53*DR3+B54*DR4)*TR5
!
! ------------
! ........ Computation of the viscosity
! ------------
! 
           ViscLV_H2Ox = AMU0*EXP(DR*SUM)
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'ViscLV_H2Ox       1.0    9 April     2003',6X,  &        
     &         'Viscosity of water (liquid or vapor) as a function ',   &
     &         'of temperature and density (extended P-T range)')
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of ViscLV_H2Ox
!
!
            RETURN
!
         END FUNCTION ViscLV_H2Ox
!
!
!
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
!
!
!
         SUBROUTINE UHRho_LiqH2O(TF,PP,D,U,H,IGOOD)
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*       Routine for calculating the density and internal energy       *
!*           of liquid H2O - Based on M.J.O'Sullivan routine           *
!*                                                                     *
!*                      Version 1.0 - May 9, 2003                      *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
            IMPLICIT NONE
! 
! -------------
! ......... Double precision parameters
! -------------
! 
            REAL(KIND=8), PARAMETER ::                             & 
     &         A1  =   6.824687741d+03,  A2  =  -5.422063673d+02,   &
     &         A3  =  -2.096666205d+04,  A4  =   3.941286787d+04,   &
     &         A5  = -13.466555478d+04,  A6  =  29.707143084d+04,   &
     &         A7  =  -4.375647096d+05,  A8  =  42.954208335d+04,   &
     &         A9  = -27.067012452d+04,  A10 =   9.926972482d+04,   &
     &         A11 = -16.138168904d+03,  A12 =   7.982692717d+00,   &
     &         A13 =  -2.616571843d-02,  A14 =   1.522411790d-03,   &
     &         A15 =   2.284279054d-02,  A16 =   2.421647003d+02,   &
     &         A17 =   1.269716088d-10,  A18 =   2.074838328d-07,  &
     &         A19 =   2.174020350d-08,  A20 =   1.105710498d-09,   &
     &         A21 =   1.293441934d+01,  A22 =   1.308119072d-05,   &
     &         A23 =   6.047626338d-14
! 
            REAL(KIND=8), PARAMETER ::  &                             
     &         SA1  =  8.438375405d-01,  SA2  =  5.362162162d-04,  &
     &         SA3  =  1.720000000d+00,  SA4  =  7.342278489d-02,   &
     &         SA5  =  4.975858870d-02,  SA6  =  6.537154300d-01,   &
     &         SA7  =        1.150d-06,  SA8  =      1.51080d-05,   &
     &         SA9  =      1.41880d-01,  SA10 =  7.002753165d+00,   &
     &         SA11 =  2.995284926d-04,  SA12 =        2.040d-01   
! 
! -------------
! ......... Double precision variables
! -------------
! 
            REAL(KIND=8), INTENT(IN)  :: TF,PP
            REAL(KIND=8), INTENT(OUT) :: D,U
!
            REAL(KIND=8) :: TKR,TKR2,TKR3,TKR4,TKR5,TKR6,TKR7
            REAL(KIND=8) :: TKR8,TKR9,TKR10,TKR11,TKR18,TKR19,TKR20
!
            REAL(KIND=8) :: Y,ZP,Z,CZ,VMKR,V,YD,SNUM
            REAL(KIND=8) :: CC1,CC2,CC4,CC8,CC10,DD1,DD2,DD4
            REAL(KIND=8) :: PNMR,PNMR2,PNMR3,PNMR4
            REAL(KIND=8) :: PRT1,PRT2,PRT3,PRT4,PRT5
            REAL(KIND=8) :: PAR1,PAR2,PAR3,PAR4,PAR5
!
            REAL(KIND=8) :: AA1,BB1,BB2,EE1,EE3,ENTR,H
! 
! -------------
! ......... Integer variables
! -------------
! 
            INTEGER :: IGOOD
! 
! -------------
! ......... Logical variables
! -------------
! 
            LOGICAL :: First_call = .TRUE.
! 
! -------------
! ......... Saving variables
! -------------
! 
            SAVE First_call
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of UHRho_LiqH2O 
!
!
            IF(First_call ) THEN
               First_call = .FALSE.
               IF( MPI_Rank==0) WRITE(11,6000)
            END IF
!
! ------------
! ......... Estimation of intermediate parameters 
! ------------
! 
            TKR   = (TF+2.7315d2)/6.473d2
! 
            TKR2  = TKR*TKR
            TKR3  = TKR*TKR2
            TKR4  = TKR2*TKR2
            TKR5  = TKR2*TKR3
            TKR6  = TKR4*TKR2
            TKR7  = TKR4*TKR3
            TKR8  = TKR4*TKR4
            TKR9  = TKR4*TKR5
! 
            TKR10 = TKR4*TKR6
            TKR11 = TKR*TKR10
            TKR18 = TKR8*TKR10
            TKR19 = TKR8*TKR11
            TKR20 = TKR10*TKR10
!
            PNMR  = PP/2.212d7
            PNMR2 = PNMR*PNMR
            PNMR3 = PNMR*PNMR2
            PNMR4 = PNMR*PNMR3
!
! ------------
! ......... Check temperature range
! ------------
! 
            Y  = 1.0d0-SA1*TKR2-SA2/TKR6
            ZP = SA3*Y*Y-2.0d0*SA4*TKR+2.0d0*SA5*PNMR
! 
            IF_TRange: IF(ZP < 0.0d0) THEN
! 
                          IGOOD = 2
! 
                          WRITE(6,6002) TF
! 
                          RETURN
! 
                       END IF IF_TRange
!
! ------------
! ......... For T in range, then compute density first 
! ------------
! 
            IGOOD = 0
! 
            Z    = Y+SQRT(ZP)
            CZ   = Z**(5.0d0/17.0d0)
            PAR1 = A12*SA5/CZ
! 
            CC1  = SA6-TKR
            CC2  = CC1*CC1
            CC4  = CC2*CC2
            CC8  = CC4*CC4
            CC10 = CC2*CC8
! 
            AA1  = SA7+TKR19
            PAR2 = A13+A14*TKR+A15*TKR2+A16*CC10+A17/AA1
            PAR3 = (A18+2.0d0*A19*PNMR+3.0d0*A20*PNMR2)/(SA8+TKR11)
! 
            DD1 = SA10+PNMR
            DD2 = DD1*DD1
            DD4 = DD2*DD2
! 
            PAR4 = A21*TKR18*(SA9+TKR2)*(-3.0d0/DD4+SA11)
            PAR5 = 3.0d0*A22*(SA12-TKR)*PNMR2+4.0d0*A23/TKR20*PNMR3
! 
            VMKR = PAR1+PAR2-PAR3-PAR4+PAR5
!
! ......... The density of liquid water is computed 
! 
            V = VMKR*3.17d-3
            D = 1.0d0/V
!
! ------------
! ......... Continue by computing the enthalpy and internal energy 
! ------------
! 
            YD   =-2.0d0*SA1*TKR+6.0d0*SA2/TKR7
!
            SNUM = (((((((A10+A11*TKR &  
     &                   )*TKR+A9   &
     &                  )*TKR+A8   &
     &                 )*TKR+A7   &
     &                )*TKR+A6   &
     &               )*TKR+A5   &
     &              )*TKR+A4   &
     &             )*TKR2-A2   
!
            PRT1 =  A12*(Z*(  17.0d0*( Z/29.0d0-Y/12.0d0 ) &
     &                      + 5.0d0*TKR*YD/12.0d0   &
     &                     )+SA4*TKR-(SA3-1.0d0)*TKR*Y*YD       &
     &                  )/CZ
!
            PRT2 = PNMR*( A13-A15*TKR2    &                   
     &                   +A16*(9.0d0*TKR+SA6)*CC8*CC1        &
     &                   +A17*(19.0d0*TKR19+AA1)/(AA1*AA1)   &
     &                  )
!
            BB1  = SA8+TKR11
            BB2  = BB1*BB1
!
            EE1  = SA10+PNMR
            EE3  = EE1*EE1*EE1
!
            PRT3 = ( 11.0d0*TKR11 + BB1 )/BB2  & 
     &            *( A18*PNMR + A19*PNMR2 + A20*PNMR3 )
!
            PRT4 = A21*TKR18*( 1.7d1*SA9 + 1.9d1*TKR2 )&
     &                      *( 1.0d0/EE3 + SA11*PNMR )
!
            PRT5 = A22*SA12*PNMR3 + 2.1d1*A23/TKR20*PNMR4
!
            ENTR = A1*TKR - SNUM + PRT1 + PRT2 - PRT3 + PRT4 + PRT5
!
! ......... The enthalpy and internal energy of liquid water are computed 
! 
            H = ENTR*7.01204d4
            U = H-PP*V
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'UHRho_LiqH2O      1.0    9 May       2003',6X, &  
     &         'Liquid water density and internal energy ',     & 
     &         'versus temperature and pressure')
! 
 6002 FORMAT(' The temperature = ',1pE12.6,&
     &       ' oC is out of range in "UHRho_LiqH2O"')
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of UHRho_LiqH2O
!
!
            RETURN
!
         END SUBROUTINE UHRho_LiqH2O
!
!
!
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
!
!
!
         SUBROUTINE UHRho_VapH2O(T,P,D,U,H)
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*       Routine for calculating the density, internal energy and      *
!*  specific enthalpy of H2O vapor - Based on M.J.O'Sullivan routine   *
!*                                                                     *
!*                      Version 1.0 - May 9, 2003                      *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
            IMPLICIT NONE
! 
! -------------
! ......... Double precision parameters
! -------------
! 
            REAL(KIND=8), PARAMETER :: B0 =    16.83599274d+00, &  
     &         B01 =  28.56067796d+00, B02 =  -54.38923329d+00,  & 
     &         B03 = 0.4330662834d+00, B04 = -0.6547711697d+00,   &
     &         B05 =  8.565182058d-02
! 
            REAL(KIND=8), PARAMETER :: &                          
     &         B11 =  6.670375918d-02, B12 =  1.388983801d+00, &   
     &         B21 =  8.390104328d-02, B22 =  2.614670893d-02,  & 
     &         B23 = -3.373439453d-02, B31 =  4.520918904d-01,   & 
     &         B32 =  1.069036614d-01, B41 = -5.975336707d-01,    &
     &         B42 = -8.847535804d-02
! 
            REAL(KIND=8), PARAMETER ::  &                            
     &         B51 =  5.958051609d-01, B52 = -5.159303373d-01, &   
     &         B53 =  2.075021122d-01, B61 =  1.190610271d-01,  &  
     &         B62 = -9.867174132d-02, B71 =  1.683998803d-01,   & 
     &         B72 = -5.809438001d-02, B81 =  6.552390126d-03,    &  
     &         B82 =  5.710218649d-04
! 
            REAL(KIND=8), PARAMETER :: B90 =  1.936587558d+02,&     
     &         B91 = -1.388522425d+03, B92 =  4.126607219d+03, &   
     &         B93 = -6.508211677d+03, B94 =  5.745984054d+03,  &  
     &         B95 = -2.693088365d+03, B96 =  5.235718623d+02    
! 
            REAL(KIND=8), PARAMETER ::  SB   = 7.633333333d-01, & 
     &         SB61 =  4.006073948d-01, SB71 = 8.636081627d-02,  & 
     &         SB81 = -8.532322921d-01, SB82 = 3.460208861d-01    
! 
            REAL(KIND=8), PARAMETER ::  I1 = 4.260321148d0
! 
! -------------
! ......... Double precision variables
! -------------
! 
            REAL(KIND=8), INTENT(IN)  :: T,P
            REAL(KIND=8), INTENT(OUT) :: D,U,H
!
            REAL(KIND=8) :: THETA,THETA2,THETA3,THETA4
            REAL(KIND=8) :: BETA,BETA2,BETA3,BETA4,BETA5,BETA6,BETA7
            REAL(KIND=8) :: BETAL,DBETAL
!
            REAL(KIND=8) :: X,X2,X3,X4,X5,X6,X8
            REAL(KIND=8) :: X10,X11,X14,X18,X19,X24,X27
            REAL(KIND=8) :: R,R2,R4,R6,R10,CHI2,SC,SD1,SD2,SD3
!
            REAL(KIND=8) :: V,OS1,OS2,OS5,OS6,OS7,EPS2,SN,SN6,SN7,SN8
! 
! -------------
! ......... Logical variables
! -------------
! 
            LOGICAL :: First_call = .TRUE.
! 
! -------------
! ......... Saving variables
! -------------
! 
            SAVE First_call
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of UHRho_VapH2O 
!
!
            IF(First_call) THEN
               First_call = .FALSE.
               IF(MPI_RANK==0) WRITE(11,6000)
            END IF
!
! ------------
! ......... Estimation of intermediate parameters 
! ------------
! 
            THETA = (T+273.15d0)/6.473d2
            BETA  = P/2.212d7
            X     = EXP(SB*(1.0d0-THETA))
! 
            X2  = X*X
            X3  = X2*X
            X4  = X3*X
            X5  = X4*X
            X6  = X5*X
            X8  = X6*X2
            X10 = X6*X4
            X11 = X10*X
            X14 = X10*X4
            X18 = X14*X4
            X19 = X18*X
            X24 = X18*X6
            X27 = X24*X3
! 
            THETA2 = THETA*THETA
            THETA3 = THETA2*THETA
            THETA4 = THETA3*THETA
!
            BETA2 = BETA*BETA
            BETA3 = BETA2*BETA
            BETA4 = BETA3*BETA
            BETA5 = BETA4*BETA
            BETA6 = BETA5*BETA
            BETA7 = BETA6*BETA
!
            BETAL  = 15.74373327d0 - 34.17061978d0*THETA &
     &                             + 19.31380707d0*THETA2
!
            DBETAL = -34.17061978d0+38.62761414d0*THETA
!
            R   = BETA/BETAL
            R2  = R*R
            R4  = R2*R2
            R6  = R4*R2
            R10 = R6*R4
!
            CHI2 = I1*THETA/BETA
            SC   = (B11*X10+B12)*X3
            CHI2 = CHI2-SC
            SC   = B21*X18+B22*X2+B23*X
            CHI2 = CHI2-2*BETA*SC
            SC   = (B31*X8+B32)*X10
            CHI2 = CHI2-3*BETA2*SC
            SC   = (B41*X11+B42)*X14
            CHI2 = CHI2-4*BETA3*SC
            SC   = (B51*X8+B52*X4+B53)*X24
            CHI2 = CHI2-5*BETA4*SC
!
            SD1 = 1.0d0/BETA4+SB61*X14
            SD2 = 1.0d0/BETA5+SB71*X19
            SD3 = 1.0d0/BETA6+(SB81*X27+SB82)*X27
!
            SN   = (B61*X+B62)*X11
            chi2 = chi2-(sn/sd1*4.0d0/beta5)/sd1
            SN   = (B71*X6+B72)*X18
            chi2 = chi2-(sn/sd2*5.0d0/beta6)/sd2
            SN   = (B81*X10+B82)*X14
            chi2 = chi2-(sn/sd3*6.0d0/beta7)/sd3
!
            SC = (((((B96*X+B95)*X+B94)*X+B93)*X+B92)*X+B91)*X+B90
!
! ------------
! ......... The H2O vapor density is computed first 
! ------------
! 
            CHI2 = CHI2+1.1d1*R10*SC
            V    = CHI2*3.17d-3
            D    = 1.0d0/V
!
! ------------
! ......... Intermediate parameters for computing enthalpy and internal energy
! ------------
! 
            OS1  = SB*THETA
            EPS2 = 0.0d0+B0*THETA-(-       B01 &          
     &                             +       B03*THETA2    &
     &                             + 2.0d0*B04*THETA3    &
     &                             + 3.0d0*B05*THETA4    &
     &                            )
!
            SC   = (  B11*(1.d0+13.0d0*OS1)*X10 &
     &              + B12*(1.d0+3.00d0*OS1) )*X3
            EPS2 = EPS2-BETA*SC
!
            SC   =   B21*(1.d0+18.0d0*OS1)*X18 &
     &             + B22*(1.d0+2.00d0*OS1)*X2 &
     &             + B23*(1.d0+OS1)*X
            EPS2 = EPS2-BETA2*SC
!
            SC   = (  B31*(1.d0+18.0d0*OS1)*X8 &
     &              + B32*(1.d0+10.0d0*OS1) )*X10
            EPS2 = EPS2-BETA3*SC
!
            SC   = (  B41*(1.d0+25.0d0*OS1)*X11 &
     &              + B42*(1.d0+14.0d0*OS1) )*X14
            EPS2 = EPS2-BETA4*SC
!
            SC   = (  B51*(1.d0+32.0d0*OS1)*X8 &
     &              + B52*(1.d0+28.0d0*OS1)*X4 &
     &              + B53*(1.d0+24.0d0*OS1) )*X24
            EPS2 = EPS2-BETA5*SC
!
            SN6 = 14.0d0*SB61*X14
            SN7 = 19.0d0*SB71*X19
            SN8 = (54.0d0*SB81*X27+27.0d0*SB82)*X27
!
            OS5  = 1.0d0+11.d0*OS1-OS1*SN6/SD1
            SC   = ( B61*X*(OS1+OS5) + B62*OS5 )*(X11/SD1)
            EPS2 = EPS2-SC
!
            OS6  = 1.d0+24.d0*OS1-OS1*SN7/SD2
            SC   = (B71*X6*OS6+B72*(OS6-6.d0*OS1))*(X18/SD2)
            EPS2 = EPS2-SC
!
            OS7  = 1.d0+24.d0*OS1-OS1*SN8/SD3
            SC   = ( B81*X10*OS7 + B82*(OS7-10.0d0*OS1) )*(X14/SD3)
            EPS2 = EPS2-SC
!
            OS2  = 1.0d0+THETA*10.0d0*DBETAL/BETAL
!
            SC = (((((((OS2+6.0d0*OS1  &          
     &                 )*B96            &         
     &                )*X+(OS2+5.0d0*OS1)*B95    &
     &               )*X+(OS2+4.0d0*OS1)*B94     &
     &              )*X+(OS2+3.0d0*OS1)*B93      &
     &             )*X+(OS2+2.0d0*OS1)*B92       &
     &            )*X+(OS2+OS1)*B91              &
     &           )*X+OS2*B90
!
! ......... The enthalpy and internal energy of H2O vapor are computed 
! 
            EPS2 = EPS2+BETA*R10*SC
            H    = EPS2*70120.4d0
            U    = H-P*V
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'UHRho_VapH2O      1.0    9 May       2003',6X,   &      
     &         'Water vapor density, internal energy and specific ',  &
     &         'enthalpy as a ',                                       &
     &       /,T48,'function of temperature and pressure')
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of UHRho_VapH2O
!
!
            RETURN
!
         END SUBROUTINE UHRho_VapH2O
!
!
!
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
!
!
!
         REAL(KIND = 8) FUNCTION ThrmCond_LiqH2O(T,P,D,PS)
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*          Routine for calculating the thermal conductivity of        *
!*                     of liquid H2O and H2O vapor                     *
!*                                                                     *
!*                      Version 1.0 - May 9, 2003                      *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
            IMPLICIT NONE
! 
! -------------
! ......... Double precision parameters
! -------------
! 
            REAL(KIND=8), PARAMETER :: A0 = -922.47d+00, &   
     &         A1 = 2839.5d+00,        A2 = -1800.7d+00,  &  
     &         A3 = 525.77d+00,        A4 = -73.440d+00
! 
            REAL(KIND=8), PARAMETER :: B0 = -0.94730d+00,  & 
     &         B1 =  2.5186d+00,       B2 =  -2.0012d+00,   &
     &         B3 = 0.51536d+00
! 
            REAL(KIND=8), PARAMETER :: C0 = 1.6563d-03,&    
     &         C1 = -3.8929d-03,       C2 = 2.9323d-03, &    
     &         C3 = -7.1693d-04
! 
! -------------
! ......... Double precision variables
! -------------
! 
            REAL(KIND=8), INTENT(IN)  :: T,P,D,PS
!
            REAL(KIND=8) :: T1,T2,T3,T4
            REAL(KIND=8) :: CON1,CON2,CON3
! 
! -------------
! ......... Logical variables
! -------------
! 
            LOGICAL :: First_call = .TRUE.
! 
! -------------
! ......... Saving variables
! -------------
! 
            SAVE First_call 
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of ThrmCond_LiqH2O
!
!
            IF(First_call) THEN
               First_call = .FALSE.
               IF(MPI_RANK==0) WRITE(11,6000)
            END IF
!
! ------------
! ......... Estimation of intermediate parameters 
! ------------
! 
            T1 = (T+273.15d0)/273.15d0
            T2 = T1*T1
            T3 = T2*T1
            T4 = T3*T1
!
! ------------
! ......... The thermal conductivity of liquid H2O 
! ------------
! 
            CON1 = A0+A1*T1+A2*T2+A3*T3+A4*T4
            CON2 = (P-PS)*(B0+B1*T1+B2*T2+B3*T3)*1.d-5
            CON3 = (P-PS)*(P-PS)*(C0+C1*T1+C2*T2+C3*T3)*1.0d-10
! 
            ThrmCond_LiqH2O = (CON1+CON2+CON3)*1.0d-3
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'ThrmCond_LiqH2O   1.0    9 May       2003',6X,            &
     &         'Thermal conductivity of liquid water as a function of ',  &
     &         'temperature, pressure, density and vapor pressure(T)')
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of ThrmCond_LiqH2O
!
!
            RETURN
!
         END FUNCTION ThrmCond_LiqH2O
!
!
!
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
!
!
!
         REAL(KIND = 8) FUNCTION ThrmCond_VapH2O(T,P,D)
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*          Routine for calculating the thermal conductivity of        *
!*                     of liquid H2O and H2O vapor                     *
!*                                                                     *
!*                      Version 1.0 - May 9, 2003                      *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
            IMPLICIT NONE
! 
! -------------
! ......... Double precision parameters
! -------------
! 
            REAL(KIND=8), PARAMETER :: A0 = -922.47d+00, &  
     &         A1 = 2839.5d+00,        A2 = -1800.7d+00,  & 
     &         A3 = 525.77d+00,        A4 = -73.440d+00 
! 
            REAL(KIND=8), PARAMETER :: B0 = -0.94730d+00,  & 
     &         B1 =  2.5186d+00,       B2 =  -2.0012d+00,   &
     &         B3 = 0.51536d+00
! 
            REAL(KIND=8), PARAMETER :: C0 = 1.6563d-03,     &
     &         C1 = -3.8929d-03,       C2 = 2.9323d-03,    &
     &         C3 = -7.1693d-04
! 
! -------------
! ......... Double precision variables
! -------------
! 
            REAL(KIND=8), INTENT(IN)  :: T,P,D
!
            REAL(KIND=8) :: T1,T2,T3
            REAL(KIND=8) :: CON1,CON2,CON3
! 
! -------------
! ......... Logical variables
! -------------
! 
            LOGICAL :: First_call = .TRUE.
! 
! -------------
! ......... Saving variables
! -------------
! 
            SAVE First_call
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of ThrmCond_VapH2O
!
!
            IF(First_call) THEN
               First_call = .FALSE.
               IF(MPI_RANK==0) WRITE(11,6000)
            END IF
!
! ------------
! ......... Estimation of intermediate parameters 
! ------------
! 
            T1 = (T+273.15d0)/273.15d0
            T2 = T1*T1
            T3 = T2*T1
!
! ------------
! ......... The thermal conductivity of the H2O vapor is ...
! ------------
! 
            CON1 = 17.6d0+5.87d-2*T
            CON2 = 1.04d-4*T2
            CON3 = 4.51d-8*T3
! 
            ThrmCond_VapH2O =  1.0d-3*( CON1 + CON2 - CON3 )  &                   
     &                       + 1.0d-6*(  1.0351d2 &
     &                                 + 4.198d-1*T& 
     &                                 - 2.771d-5*T2 )*D   &
     &                       + 2.1482d5*D*D/(T**4.2d0)
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'ThrmCond_VapH2O   1.0    9 May       2003',6X,  &         
     &         'Thermal conductivity of water vapor as a function of ',   &
     &         'temperature, pressure, and density')
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of ThrmCond_VapH2O
!
!
            RETURN
!
         END FUNCTION ThrmCond_VapH2O
!
!
!
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
!
!
!
         SUBROUTINE KThermal_H2O(T,P,D,CONW,CONS,PS)
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*          Routine for calculating the thermal conductivity of        *
!*                     of liquid H2O and H2O vapor                     *
!*                                                                     *
!*                      Version 1.0 - May 9, 2003                      *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
            IMPLICIT NONE
! 
! -------------
! ......... Double precision parameters
! -------------
! 
            REAL(KIND=8), PARAMETER :: A0 = -922.47d+00,  & 
     &         A1 = 2839.5d+00,        A2 = -1800.7d+00,   & 
     &         A3 = 525.77d+00,        A4 = -73.440d+00
! 
            REAL(KIND=8), PARAMETER :: B0 = -0.94730d+00,   &
     &         B1 =  2.5186d+00,       B2 =  -2.0012d+00,   &
     &         B3 = 0.51536d+00
! 
            REAL(KIND=8), PARAMETER :: C0 = 1.6563d-03,  &  
     &         C1 = -3.8929d-03,       C2 = 2.9323d-03,   &  
     &         C3 = -7.1693d-04 
! 
            REAL(KIND=8), PARAMETER :: T0 = 273.15d+00
! 
! -------------
! ......... Double precision variables
! -------------
! 
            REAL(KIND=8), INTENT(IN)  :: T,P,D,PS
            REAL(KIND=8), INTENT(OUT) :: CONW,CONS
!
            REAL(KIND=8) :: T1,T2,T3,T4
            REAL(KIND=8) :: CON1,CON2,CON3
! 
! -------------
! ......... Logical variables
! -------------
! 
            LOGICAL :: First_call = .TRUE.
! 
! -------------
! ......... Saving variables
! -------------
! 
            SAVE First_call
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of KThermal_H2O 
!
!
            IF(First_call) THEN
               First_call = .FALSE.
               IF(MPI_RANK==0) WRITE(11,6000)
            END IF
!
! ------------
! ......... Estimation of intermediate parameters 
! ------------
! 
            T1 = (T+273.15d0)/T0
            T2 = T1*T1
            T3 = T2*T1
            T4 = T3*T1
!
! ------------
! ......... The thermal conductivity of liquid H2O is computed first 
! ------------
! 
            CON1 = A0+A1*T1+A2*T2+A3*T3+A4*T4
            CON2 = (P-PS)*(B0+B1*T1+B2*T2+B3*T3)*1.d-5
            CON3 = (P-PS)*(P-PS)*(C0+C1*T1+C2*T2+C3*T3)*1.0d-10
            CONW = (CON1+CON2+CON3)*1.0d-3
!
! ------------
! ......... The thermal conductivity of the H2O vapor is computed next 
! ------------
! 
            CON1 = 17.6d0+5.87d-2*T
            CON2 = 1.04d-4*T2
            CON3 = 4.51d-8*T3
! 
            CONS  =  1.0d-3*( CON1 + CON2 - CON3 )  &                    
     &             + 1.0d-6*( 1.0351d2 + 4.198d-1*T - 2.771d-5*T2 )*D  &
     &             + 2.1482d5*D*D/(T**4.2d0)
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'KThermal_H2O      1.0    9 May       2003',6X,   &         
     &         'Thermal conductivity of water and vapor as a function ',   &
     &         'of T, P, density and H2O vapor pressure (at T)')
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of KThermal_H2O
!
!
            RETURN
!
         END SUBROUTINE KThermal_H2O
!
!
!
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
!
!
!
         SUBROUTINE HRhoL_H2OFusion(T_c,H_LiqH2O,D_LiqH2O)
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*        Routine for calculating the enthalpy and density of          *
!*                 liquid H2O during melting or fusion                 *
!*                                                                     *
!*                     Version 1.0 - May 12, 2004                      *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
            IMPLICIT NONE
! 
! -------------
! ......... Double precision parameters
! -------------
! 
            REAL(KIND=8), PARAMETER :: A0 = -6.534708488711557D-03,  & 
     &                                 A1 = -6.600724702334037D+00,   &
     &                                 A2 = -1.676923826823394D-01,    &  
     &                                 A3 = -2.628921749547960D-03     
! 
            REAL(KIND=8), PARAMETER :: B0 = +2.886639333211162D-01,   &
     &                                 B1 = -9.264947091333974D+00,    &  
     &                                 B2 = -3.157580892539609D-01,   &
     &                                 B3 = -5.745992597191567D-03   
! 
! -------------
! ......... Double precision variables
! -------------
! 
            REAL(KIND=8), INTENT(IN)  :: T_c
! 
            REAL(KIND=8), INTENT(OUT) :: H_LiqH2O,D_LiqH2O
! 
! -------------
! ......... Logical variables
! -------------
! 
            LOGICAL :: First_call = .TRUE.
! 
! -------------
! ......... Saving variables
! -------------
! 
            SAVE First_call
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of HRhoL_H2OFusion
!
!
            IF(First_call) THEN
               First_call = .FALSE.
               IF(MPI_RANK==0) WRITE(11,6000)
            END IF
!
! ------------
! ......... The density of the liquid H2O during melting/fusion is ...
! ------------
! 
! 
            D_LiqH2O =  A0+T_c*    &       ! In Kg/m^3
     &                 (A1+T_c*     &  
     &                 (A2+T_c*      & 
     &                  A3))+1.0d3 
!
! ------------
! ......... The enthalpy of the liquid H2O during melting/fusion is ...
! ------------
! 
! 
            H_LiqH2O = 1.0d3*( B0+T_c*  &  ! In J/Kg
     &                        (B1+T_c*   &
     &                        (B2+T_c*   &
     &                         B3)))
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'HRhoL_H2OFusion   1.0   12 May       2004',6X,   &       
     &         'Liquid H2O enthalpy and density during ice melting/',   &
     &         'fusion as a function of temperature')
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of HRhoL_H2OFusion
!
!
            RETURN
!
         END SUBROUTINE HRhoL_H2OFusion
!
!
!
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
!
!
!
         SUBROUTINE HRhoV_H2OSublim(T_c,H_VapH2O,D_VapH2O)
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*        Routine for calculating the enthalpy and density of          *
!*                    H2O vapor during sublimation                     *
!*                                                                     *
!*                     Version 1.0 - May 12, 2004                      *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
            IMPLICIT NONE
! 
! -------------
! ......... Double precision parameters
! -------------
! 
            REAL(KIND=8), PARAMETER :: A0 = -5.32343811973481D+00, & 
     &                                 A1 = +7.99783777361478D-02,  & 
     &                                 A2 = -2.32428885339912D-04,   &   
     &                                 A3 = +2.13649543395573D-06
! 
            REAL(KIND=8), PARAMETER :: B0 = +9.71831861487123D-01,   &
     &                                 B1 = +1.84154344362045D+00,    &  
     &                                 B2 = -8.67800173335483D-05   
! 
! -------------
! ......... Double precision variables
! -------------
! 
            REAL(KIND=8), INTENT(IN)  :: T_c
! 
            REAL(KIND=8), INTENT(OUT) :: H_VapH2O,D_VapH2O
! 
! -------------
! ......... Logical variables
! -------------
! 
            LOGICAL :: First_call = .TRUE.
! 
! -------------
! ......... Saving variables
! -------------
! 
            SAVE First_call
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of HRhoV_H2OSublim
!
!
            IF(First_call) THEN
               First_call = .FALSE.
               IF(MPI_RANK==0) WRITE(11,6000)
            END IF
!
! ------------
! ......... The density of the H2O vapor during melting/fusion is ...
! ------------
! 
! 
            D_VapH2O =  EXP( A0+T_c*  &     ! In Kg/m^3
     &                      (A1+T_c*   &  
     &                      (A2+T_c*    & 
     &                       A3))        &
     &                     )
!
! ------------
! ......... The enthalpy of the H2O vapor during melting/fusion is ...
! ------------
! 
! 
            H_VapH2O = 1.0d3*( EXP( B0+T_c*   &          ! In J/Kg
     &                             (B1+T_c*    & 
     &                              B2)         &
     &                            )             &
     &                        +2.5d3            &
     &                       )
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'HRhoV_H2OSublim   1.0   12 May       2004',6X,   &         
     &         'H2O vapor enthalpy and density during ice sublimation',   &
     &         'as a function of temperature')
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of HRhoV_H2OSublim
!
!
            RETURN
!
         END SUBROUTINE HRhoV_H2OSublim
!
!
!
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
!
!
!
         REAL(KIND = 8) FUNCTION Ice_Enthalpy(P_Pa,T_c)
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*             Routine for calculating the enthalpy of ice             *
!*                                                                     *
!*                   Version 1.0 - November 9, 2004                    *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
            IMPLICIT NONE
! 
! -------------
! ......... Double precision parameters
! -------------
! 
            REAL(KIND=8), PARAMETER :: A1 = +5.3813277d+02,  &
     &                                 A2 = +2.0802342d+00,   &  
     &                                 A3 = -2.8833629d-04
! 
            REAL(KIND=8), PARAMETER :: T_cut = -8.2d0
! 
! -------------
! ......... Double precision variables
! -------------
! 
            REAL(KIND=8), INTENT(IN)  :: T_c,P_Pa
!
            REAL(KIND=8) :: P_Fusion,H_Fusion,P_Sublim,H_Sublim
            REAL(KIND=8) :: U_LiqH2O,H_LiqH2O,D_LiqH2O,H_IceFusion
            REAL(KIND=8) :: U_vap,H_vap,D_vap,H_IceSublim
            REAL(KIND=8) :: P_F00,H_F00,H_Liq00,H_IceFus00,Ice_DH
            REAL(KIND=8) :: T0_1,T0_2,T0_3,T_1,T_2,T_3
! 
! -------------
! ......... Integer variables
! -------------
! 
            INTEGER :: IGOOD
! 
! -------------
! ......... Logical variables
! -------------
! 
            LOGICAL :: First_call = .TRUE.
! 
! -------------
! ......... Saving variables
! -------------
! 
            SAVE First_call,T0_1,T0_2,T0_3,P_F00,H_F00
            SAVE H_Liq00,H_IceFus00
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of Ice_Enthalpy
!
!
            IF(First_call) THEN
! 
               First_call = .FALSE.
!
! ............ Compute some parameters, just in case
! 
               T0_1 = 273.15d0 + T_cut
               T0_2 = T0_1*T0_1
               T0_3 = T0_2*T0_1
!
! ............ Fusion at T_cut 
! 
               IGOOD = P_Fusion_H2O_S(T_cut,P_F00)
               IGOOD = H_Fusion_H2O_S(T_cut,H_F00)
! 
               CALL UHRho_LiqH2O(T_cut,P_F00,D_LiqH2O,&
     &                           U_LiqH2O,H_Liq00,IGOOD)
! 
               H_IceFus00 = H_Liq00 - H_F00
!
! ............ Print routine identifier
! 
               WRITE(11,6000)
! 
            END IF
! 
!
!***********************************************************************
!*                                                                     *
!*        FIRST, THE ICE ENTHALPY AT THE FUSION PRESSURE => T_c        *
!*                                                                     *
!***********************************************************************
!
            IF( T_c >= T_cut) THEN 
!
! ............ Determine the fusion pressure P_Fusion at T_c
! 
               IGOOD = P_Fusion_H2O_S(T_c,P_Fusion)
!
! ............ Determine the corresponding specific fusion enthalpy
! 
               IGOOD = H_Fusion_H2O_S(T_c,H_Fusion)
!
! ............ Determine the liquid H2O enthalpy at the fusion point (T_c,P_Fusion)
! 
               CALL UHRho_LiqH2O(T_c,P_Fusion,D_LiqH2O,&
     &                           U_LiqH2O,H_LiqH2O,IGOOD)
!
! ............ Compute the specific enthalpy of ice at the fusion point (T_c,P_Fusion)
! 
               H_IceFusion = H_LiqH2O - H_Fusion  
! 
            ELSE
!
! ----------------
! ............ Determine the enthalpy change from T_cut to T_c 
! ----------------
! 
               T_1 = T_c + 273.15d0
               T_2 = T_1*T_1
               T_3 = T_2*T_1
!
               Ice_DH = A1*(T_1 - T0_1) + A2*(T_2 - T0_2) &
     &                                  + A3*(T_3 - T0_3)
!
! ............ Compute the specific enthalpy of ice at the fusion point (T_c,P_Fusion20)
! 
               H_IceFusion = H_IceFus00 + Ice_DH
! 
            END IF
!
! -------------
! ......... If at the triple point, the computation is complete
! -------------
! 
            IF(T_c == 1.0d-2) THEN
               Ice_Enthalpy =  H_IceFusion
               RETURN
            END IF
! 
!
!***********************************************************************
!*                                                                     *
!*     SECOND, THE ICE ENTHALPY AT THE SUBLIMATION PRESSURE => T_c     *
!*                                                                     *
!***********************************************************************
!
! ......... Determine the sublimation pressure P_Sublm at T_c
! 
            IGOOD = P_Sublm_H2O_S(T_c,P_Sublim)
!
! ......... Determine the corresponding specific sublimation enthalpy
! 
            IGOOD = H_Sublm_H2O_S(T_c,H_Sublim)
!
! ......... Determine the H2O vapor enthalpy at the sublimation point (T_c,P_sublm)
! 
            CALL UHRho_VapH2O(T_c,P_Sublim,D_vap,U_vap,H_vap)
!
! ......... Compute the specific enthalpy of ice at the sublimation point (T_c,P_sublm)
! 
            H_IceSublim = H_vap - H_Sublim 
! 
!
!***********************************************************************
!*                                                                     *
!*          FINALLY, INTERPOLATE BETWEEN THE TWO ENTHALPIES            *
!*                                                                     *
!***********************************************************************
!
            IF( T_c >= T_cut) THEN            
               Ice_Enthalpy =   H_IceSublim      &
     &                       + (H_IceFusion - H_IceSublim)&
     &                        *((P_pa - P_Sublim)/(P_Fusion - P_Sublim))
            ELSE
               Ice_Enthalpy =   H_IceSublim    & 
     &                       + (H_IceFusion - H_IceSublim)&
     &                        *((P_pa - P_Sublim)/(P_F00 - P_Sublim))
            END IF
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'Ice_Enthalpy      1.0    9 November  2004',6X, &  
     &         'Enthalpy of ice as a function of ',&
     &         'temperature based on ',/,T48,  &
     &         'interpolation between fusion and sublimation points')                                                 
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of Ice_Enthalpy
!
!
            RETURN
!
         END FUNCTION Ice_Enthalpy
!
!
!
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
!
!
!
         REAL(KIND = 8) FUNCTION Ice_Density(T_c,P_Pa)
!
! ......... Modules used
!
            USE H2O_Param, ONLY: Ice_rho_3p
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*             Routine for calculating the density of ice              *
!*                                                                     *
!*                     Version 1.0 - May 12, 2004                      *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
            IMPLICIT NONE
! 
! -------------
! ......... Double precision parameters for ice molal volume
! ......... From Marion and Jakubowski: Cold Regions Science and 
! .........    Technology, 38, 211-218, (2004) 
! -------------
! 
            REAL(KIND=8), PARAMETER :: V0 =  1.071518095026643d-03, &  
     &                                 V1 = -4.434097468916519d-08,  & 
     &                                 V2 =  4.198079484902309d-10   
! 
            REAL(KIND=8), PARAMETER :: B0 =  1.548680062166963d-11,   &
     &                                 B1 = -1.240808170515098d-13,  &
     &                                 B2 =  2.496520315275311d-16   
! 
! -------------
! ......... Double precision parameters for thermal expansivity of ice   
! ......... From data in Tanaka: Journal of Molecular Structure, V461-462, 561-567, (1999) 
! -------------
! 
            REAL(KIND=8), PARAMETER :: C0 = -1.72176258645362d-05,  &  
     &                                 C1 =  3.56011844951369d-07,   & 
     &                                 C2 = -4.65623949357508d-10    
! 
! -------------
! ......... Double precision variables
! -------------
! 
            REAL(KIND=8), INTENT(IN)  :: T_c,P_Pa
! 
            REAL(KIND=8)  :: T_k,T_k2,T_k3,F_dT
! 
! -------------
! ......... Logical variables
! -------------
! 
            LOGICAL :: First_call = .TRUE.
! 
! -------------
! ......... Saving variables
! -------------
! 
            SAVE First_call
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of Ice_Density
!
!
            IF(First_call) THEN
               First_call = .FALSE.
               IF(MPI_RANK==0) WRITE(11,6000)
            END IF
!
! ------------
! ......... The temperature (in degrees Kelvin) is ...
! ------------
! 
            T_k  = T_c + 273.15d0
            T_k2 = T_k*T_k
            T_k3 = T_k*T_k2
!
! ------
! ... The expansivity contribution to the volume change is ...
! ------
! 
            F_dT =   C0*(T_k  - 273.16d0)   &            
     &             + C1*(T_k2 - 7.4616385d+04)/2.0d0       &
     &             + C2*(T_k3 - 2.0382211d+07)/3.0d0       
!
! ------------
! ......... The ice density (in kg/m^3) is ...
! ------------
! 
            Ice_Density = 1.0d0    			&
     &                   /(  V0+T_k*(V1 + T_k*V2)        &               ! The molal volume at T_k and P = 1 atm
     &                     -(B0+T_k*(B1 + T_k*B2))*(P_Pa - 1.013d5)&     ! The effect of compressibility
     &                     + F_dT/Ice_rho_3p                        &    ! The effect of compressibility
     &                    ) 
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'Ice_Density       1.0   12 May       2004',6X,   &
     &         'Density of ice as a function of temperature')
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of Ice_Density
!
!
            RETURN
!
         END FUNCTION Ice_Density
!
!
!
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
!
!
!
         REAL(KIND = 8) FUNCTION ThrmCond_Ice(T_c)
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*       Routine for calculating the thermal conductivity of ice       *
!*                                                                     *
!*                     Version 1.0 - May 12, 2004                      *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
            IMPLICIT NONE
! 
! -------------
! ......... Double precision parameters
! -------------
! 
            REAL(KIND=8), PARAMETER :: A0 = +2.21735244021574d+00,  & 
     &                                 A1 = -6.91686028517579d-03,   &
     &                                 A2 = +1.01672116667213d-04,    & 
     &                                 A3 = +4.45674285580074d-07
! 
! -------------
! ......... Double precision variables
! -------------
! 
            REAL(KIND=8), INTENT(IN)  :: T_c
! 
! -------------
! ......... Logical variables
! -------------
! 
            LOGICAL :: First_call = .TRUE.
! 
! -------------
! ......... Saving variables
! -------------
! 
            SAVE First_call
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of ThrmCond_Ice
!
!
            IF(First_call) THEN
               First_call = .FALSE.
               IF(MPI_RANK==0) WRITE(11,6000)
            END IF
!
! ------------
! ......... The thermal conductivity of ice is ...
! ------------
! 
! 
!            ThrmCond_Ice =  A0+T_c*    &    ! In W/m/s
!     &                     (A1+T_c*    &
!     &                     (A2+T_c*    &
!     &                      A3))
            ThrmCond_Ice =  488.19d0/(T_c+273.15d0)+0.4685d0    ! In W/m/K
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'ThrmCond_Ice      1.0   12 May       2004',6X,  & 
     &         'Thermal conductivity of ice as a function ',     &
     &         'of temperature')
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of ThrmCond_Ice
!
!
            RETURN
!
         END FUNCTION ThrmCond_Ice
!
!
!
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
!
!
!
      END MODULE H2O_Properties
!
!
!
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

