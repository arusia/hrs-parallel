!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                                                                     >
!>    TFx_HydrateEOS.f95: Code unit of hydrate EOS-related routines,   > 
!>                      including the hydrate EOS,                     >
!>       the corresponding Jacobian, and associated procedures         >
!>                                                                     >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

!
!
!
      SUBROUTINE READ_HydrateData
! 
! ...... Modules to be used 
! 
         USE Basic_Param
! 
         USE RefRGas_Param
         USE Hydrate_Param
! 
         USE RealGas_Properties
		 USE MPI_PARAM
		 USE MPI_SUBROUTINES
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*           ROUTINE FOR READING THE HYDRATE PROPERTY DATA             *
!*             DIRECTLY FROM THE TOUGH90 INPUT DATA BLOCK              *
!*                                                                     *
!*                  Version 1.0 - September 30, 2004                   *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8) :: Sum_H
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0, n,m,ier,ierr
! 
! -------
! ... Character parameters
! -------
! 
      CHARACTER(LEN = 12) :: Reaction_Type  ! Indicates the dissociation type
! 
! -------
! ... Logical parameters
! -------
! 
      LOGICAL :: EX
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of READ_HydrateData
!
!
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .AND. MPI_RANK==0) WRITE(11,6000)                                      
! 
!***********************************************************************
!*                                                                     *
!*        OPENING A FILE FOR STORING HYDRATE DISSOCIATION DATA         *
!*                                                                     *
!***********************************************************************
! 	
	  IF(MPI_RANK==0) THEN
		   OPEN(21,FILE='Hydrate_Info')
	  
! 
!***********************************************************************
!*                                                                     *
!*     READING THE BASIC DATA IDENTIFYING THE HYDRATE COMPOSITION      *
!*                                                                     *
!***********************************************************************
! 
		  READ(100,*) ,TEMP_INT(1)     ! The number of hydrate-producing gases
	! 
		  WRITE(6, 6001)        ! Write a heading into the output file
	  END IF
	  CALL MPI_BROADCAST(INT_ARRAY=TEMP_INT,INT_SIZE=1,root=0)
! 
	  HCom%NCom = TEMP_INT(1)
!***********************************************************************
!*                                                                     *
!*      INITIALIZE THE MAXIMUM # OF COMPONENTS IN THE GAS PHASE        *
!*                                                                     *
!***********************************************************************
! 
         MaxNum_GasComp = HCom%NCom+1   ! Set equal to the number ...
!                                       ! ... of hydrate gases + 1 (H2O) 
!
         ALLOCATE(GPhase_Com(MaxNum_GasComp), STAT=ier)  ! Allocate memory to the array 
! 
! ...... Print explanatory comments  
! 		
         IF(ier == 0) THEN
            WRITE(50,6203),MPI_RANK
         ELSE
            WRITE(6, 6204),MPI_RANK 
            WRITE(50,6204),MPI_RANK
			call MPI_BARRIER(MPI_COMM_WORLD,IER)
            STOP
         END IF
! 
!-------- 
! ... Ensure that this is a simple or a binary hydrate  
!-------- 
! 
      IF(HCom%NCom < 1 .OR. HCom%NCom > 2 .AND. MPI_RANK==0) THEN  
         WRITE(6,6102) HCom%NCom                      
         STOP
      END IF
! 
      IF(MPI_RANK==0) WRITE(6,6002) HCom%NCom
! 
!-------- 
! ... Identify the hydrate components  
!-------- 
! 
      DO n=1,HCom%NCom
! 
      IF(MPI_RANK==0)   READ(100,*), HCom%nameG(n),TEMP_REAL(1:2)
	  CALL MPI_BROADCAST(CHAR_ARRAY=HCom%nameG(n),REAL_ARRAY=TEMP_REAL(1:2),&
						char_size=6,real_size=2,root=0)

	  !HCom%nameG(n) is the name of the hydrate-forming gas 
      HCom%hydrN(n) = TEMP_REAL(1)   ! The corresponding hydration number
      HCom%moleF(n) = TEMP_REAL(2)   ! The mole fraction in the composite hydrate
! 
! ...... Check if available in the data base  
! 
         SELECT CASE(TRIM(ADJUSTL(HCom%nameG(n))))
         CASE('CH4','C2H6','C3H8','H2S','CO2','N2')  ! Acceptable gas options
! 
            GPhase_Com(n) = HCom%nameG(n)            ! Make the compound a component in the gas phase
! 
         CASE DEFAULT
! 
			IF(MPI_RANK==0) THEN
				WRITE(6,6104) HCom%nameG(n)     ! Unavailable gas compound option                    
				STOP                            ! Print a warning and stop
			END IF
! 
         END SELECT
! 
      END DO 
! 
!-------- 
! ... Ensure that the mole fractions add up to one  
!-------- 
! 
      Sum_H = SUM(HCom%moleF)               ! CAREFUL! Whole array operations
! 
      IF(ABS(1.0d0 - Sum_H) > 1.0d-6 .AND. MPI_RANK==0) THEN
         WRITE(6,6105) Sum_H                ! Print a message                   
         STOP                               ! Stop the simulation
      END IF         
! 
! ... Make water the last component of the gas phase  
! 
      GPhase_Com(HCom%NCom+1) = 'H2O'
! 
!***********************************************************************
!*                                                                     *
!*     Set up the basic parameters for gas behavior description        *
!*                                                                     *
!***********************************************************************
! 
      CALL RGasSetup(MaxNum_GasComp,&
     &               GPhase_Com(1:MaxNum_GasComp),'PR ','G')
! 
!***********************************************************************
!*                                                                     *
!*          Compute the component hydrate molecular weights            *
!*                                                                     *
!***********************************************************************
! 
      DO_HydComp: DO n=1,HCom%NCom   ! The hydrate component loop 
! 
         HCom%MolWt(n)  &             ! Molecular weight of component hydrate (kg/mol)
     &   =    GasR(ID(n))%MolWt&      ! Gas molecular weight
     &     +( GasR(8)%MolWt     &     ! H2O molecular weight 
     &       *HCom%hydrN(n))         ! Hydration number          
! 
         HCom%GasMF(n)   &            ! Mass fraction of gas in the component hydrate
     &   =  GasR(ID(n))%MolWt &       ! Gas molecular weight
     &     /HCom%MolWt(n)            ! Molecular weight of component hydrate (kg/mol)
! 
         HCom%H2OMF(n)  &             ! Mass fraction of H2O in the component hydrate
     &   =  1.0d0 - HCom%GasMF(n)
! 
! ...... Convert to g/mol   
! 
         HCom%MolWt(n) = 1.0d3*HCom%MolWt(n)    
! 
!----------- 
! ...... Print out the data   
!----------- 
! 
         IF(MPI_RANK==0) WRITE(6,6004) n,TRIM(ADJUSTL(HCom%nameG(n))), &  ! GH-component name
     &                   HCom%hydrN(n),                 & ! GH-hydration number
     &                   HCom%moleF(n),                 & ! GH-component mole fraction
     &                   HCom%MolWt(n)                   ! GH-component molecular weight   
! 
! 
      END DO DO_HydComp
! 
!-------- 
! ... Compute the molecular weight of the composite hydrate  
!-------- 
! 
      MW_Hydrate = SUM( HCom%MolWt(1:HCom%NCom) &  ! Molecular weight of component hydrate
     &                 *HCom%moleF(1:HCom%NCom)  & ! Mole fraction of component hydrate
     &                )
! 
! 
      IF(MPI_RANK==0) WRITE(6,6005) MW_Hydrate   ! Write a heading in the output file        
! 
!***********************************************************************
!*                                                                     *
!*            READING THE HYDRATE THERMAL CONDUCTIVITY DATA            *
!*                                                                     *
!***********************************************************************
! 
      IF(MPI_RANK==0) READ(100,*) TEMP_INT(1)   ! The number of coefficients in the thermal conductivity ...
                           ! ... polynomial F = A0+A1*T+A2*T^2+...+Am*T^m
	  CALL MPI_BROADCAST(INT_ARRAY=TEMP_INT,int_size=1,root=0)
	  GH%N_ThC = TEMP_INT(1)
! 
!-------- 
! ... Allocate memory to the corresponding coefficient array  
!-------- 
! 

      ALLOCATE(GH%p_ThC(GH%N_ThC), STAT = ier)
! 
! ... Print explanatory comments  
! 
      IF(ier == 0) THEN
         WRITE(50,6114),MPI_RANK
      ELSE
         WRITE(6, 6115),MPI_RANK
         WRITE(50,6115),MPI_RANK
		 CALL MPI_BARRIER(MPI_COMM_WORLD,IER)
         STOP
      END IF
! 
!-------- 
! ... Read the coefficients   
!-------- 
! 
      IF(MPI_RANK==0) READ(100,*) (GH%p_ThC(m), m=1,GH%N_ThC)        ! Coefficients
	  CALL MPI_BROADCAST(REAL_ARRAY=GH%p_ThC(1:GH%N_ThC),real_size=GH%N_ThC,root=0)
	  
! 
!-------- 
! ... Print out the data   
!-------- 
! 
      if(MPI_RANK==0) WRITE(6,6008)     GH%N_ThC,  &                 ! # of coefficients
     &             (m-1,GH%p_ThC(m), m=1,GH%N_ThC)  ! Coefficients       

! 
!***********************************************************************
!*                                                                     *
!*              READING THE HYDRATE SPECIFIC HEAT DATA                 *
!*                                                                     *
!***********************************************************************
! 
      IF(MPI_RANK==0) READ(100,*) TEMP_INT(1)  ! The number of coefficients in the specific heat ...
                          ! ... polynomial F = A0+A1*T+A2*T^2+...+Am*T^m
	  CALL MPI_BROADCAST(INT_ARRAY=TEMP_INT,int_size=1,root=0)
! 
	  GH%N_SpH = TEMP_INT(1)
!-------- 
! ... Allocate memory to the corresponding coefficient array  
!-------- 
! 
      ALLOCATE(GH%p_SpH(GH%N_SpH), STAT = ier)
! 
! ... Print explanatory comments  
! 
      IF(ier == 0) THEN
         WRITE(50,6116),MPI_RANK
      ELSE
         WRITE(6, 6117),MPI_RANK
         WRITE(50,6117),MPI_RANK
		 call MPI_BARRIER(MPI_COMM_WORLD,ierr)
         STOP
      END IF
! 
!-------- 
! ... Read the coefficients   
!-------- 
! 
      IF(MPI_RANK==0) READ(100,*) (GH%p_SpH(m), m=1,GH%N_SpH)    ! Coefficients
	  CALL MPI_BROADCAST(REAL_ARRAY=GH%p_SpH,real_size=GH%N_SpH,root=0)
! 
!-------- 
! ... Print out the data   
!-------- 
! 
      IF(MPI_RANK==0) WRITE(6,6010)     GH%N_SpH,   &               ! # of coefficients
     &             (m-1,GH%p_SpH(m), m=1,GH%N_SpH) ! Coefficients       
! 
!***********************************************************************
!*                                                                     *
!*                    READING THE HYDRATE DENSITY                      *
!*                                                                     *
!***********************************************************************
! 
      IF(MPI_RANK==0) READ(100,*) TEMP_INT(1)  ! The number of coefficients in the density ...
                          ! ... polynomial F = A0+A1*T+A2*T^2+...+Am*T^m
	  CALL MPI_BROADCAST(INT_ARRAY=TEMP_INT,int_size=1,root=0)
! 
	  GH%N_Rho = TEMP_INT(1)
!-------- 
! ... Allocate memory to the corresponding coefficient array  
!-------- 
! 
      ALLOCATE(GH%p_Rho(GH%N_Rho), STAT = ier)
! 
! ... Print explanatory comments  
! 
      IF(ier == 0) THEN
         WRITE(50,6118),MPI_RANK
      ELSE
         WRITE(6, 6119),MPI_RANK  
         WRITE(50,6119),MPI_RANK 
		 call MPI_BARRIER(MPI_COMM_WORLD,ierr)
         STOP
      END IF
! 
!-------- 
! ... Read the coefficients   
!-------- 
! 
      IF(MPI_RANK==0) READ(100,*) (GH%p_Rho(m), m=1,GH%N_Rho)   ! Coefficients
	  CALL MPI_BROADCAST(REAL_ARRAY=GH%p_Rho,real_size=GH%N_Rho,root=0)
! 
!-------- 
! ... Print out the data   
!-------- 
! 
      IF(MPI_RANK==0) WRITE(6,6011)     GH%N_Rho,    &               ! # of coefficients
     &             (m-1,GH%p_Rho(m), m=1,GH%N_Rho)  ! Coefficients       
! 
!***********************************************************************
!*                                                                     *
!*     READING THE TEMPERATURE SHIFTS DUE TO SALT AND INHIBITORS       *
!*                                                                     *
!***********************************************************************
! 
! ... Inhibitor-related temperature shift in the hydrate equilibrium curve
! 
      IF(MPI_RANK==0) READ(100,*),TEMP_REAL(1:6)
	  CALL MPI_BROADCAST(REAL_ARRAY=TEMP_REAL,REAL_SIZE=6,ROOT=0)
	  T_MaxOff = TEMP_REAL(1) ! Maximum T-shift (degrees C)
      C_MaxOff = TEMP_REAL(2) ! Mole fraction of salt or alcohol at T_MaxOff 
      MW_Inhib = TEMP_REAL(3) ! Molecular weight of the inhibitor (g/mole)
      D_Inhib  = TEMP_REAL(4) ! Density of the inhibitor (kg/m^3)
      H_InhSol = TEMP_REAL(5) ! Enthalpy of inhibitor dissolution (J/kg)
      DifCo_Inh= TEMP_REAL(6) ! Diffusion coefficient of inhibitor (m^2/s)
! 
!-------- 
! ... Print out the data   
!-------- 
! 
     IF(MPI_RANK==0) WRITE(6,6012) T_MaxOff,C_MaxOff,MW_Inhib,D_Inhib,&
     &              H_InhSol,DifCo_Inh
! 
!***********************************************************************
!*                                                                     *
!*            READING THE HYDRATE EQUATION OPTION NUMBER               *
!*                                                                     *
!***********************************************************************
! 
      IF(MPI_RANK==0) READ(100,*) TEMP_INT(1)   ! Flag for selecting the hydrate P-T and H-T functions
!                            ! = 0: Moridis [2003]; = 1: Kamath (1984)
	  CALL MPI_BROADCAST(INT_ARRAY=TEMP_INT,int_size=1,root=0)
	  F_EqOption = TEMP_INT(1)
!-------- 
! ... Print out the data   
!-------- 
! 
      IF(MPI_RANK==0) WRITE(6,6014) F_EqOption
! 
! 
!***********************************************************************
!*                                                                     *
!*              READING THE TYPE OF HYDRATE DISSOCIATION               *
!*                                                                     *
!***********************************************************************
! 
      IF(MPI_RANK==0) THEN
		READ(100,*) Reaction_Type
	  
	! 
	! ... For kinetic dissociation 
	! 
		  IF(Reaction_Type(1:5) == 'KINET') THEN         
			 F_Kinetic = .TRUE.
			 WRITE(6,6022)
	! 
	! ... For equilibrium dissociation 
	! 
		  ELSE IF(Reaction_Type(1:5) == 'EQUIL') THEN
			 F_Kinetic = .FALSE.
			 WRITE(6,6023)
	! 
	! ... On error, print a message and stop 
	! 
		  ELSE
			 WRITE(6,6106)
			 STOP
		  END IF
	  END IF
	  TEMP_LOGICAL(1) = F_Kinetic
	  call MPI_BROADCAST(LOGICAL_ARRAY=TEMP_LOGICAL,logical_size=1,root=0)
	  F_Kinetic = TEMP_LOGICAL(1)
! 
!----------- 
! ...... Ensure correct dimensioning   
!----------- 
! 
      IF_KinDim: IF(F_Kinetic .EQV. .TRUE.) THEN
!         
         IF(NK < 3 .AND. MPI_RANK==0) THEN
            WRITE(6,6108) NK
            STOP
         END IF
!         
         IF(NEQ < 4 .AND. MPI_RANK==0) THEN
            WRITE(6,6110) NEQ
            STOP
         END IF
!         
      END IF IF_KinDim
! 
!***********************************************************************
!*                                                                     *
!*                     FOR KINETIC HYDRATION ONLY                      *
!*                                                                     *
!***********************************************************************
! 
      IF_Kinetic: IF(F_Kinetic .EQV. .TRUE.) THEN
! 
!----------- 
! ...... Read and write the kinetic parameters   
!----------- 
! 
    IF(MPI_RANK==0) READ(100,*), TEMP_REAL(1:3)
	CALL MPI_BROADCAST(REAL_ARRAY=TEMP_REAL,real_size=3,root=0)
	Activ_En  = TEMP_REAL(1)     ! Activation energy for n-GH dissociation [J/mol]
    InRate_k0 = TEMP_REAL(2)     ! Intrinsic dissoc. rate constant [mol/(m^2.Pa.s)]
    Area_Factor = TEMP_REAL(3)   ! Adjustment factor for reaction area = ratio of ...
!                                  ! ... actual grain surface area/spherical surface area
	
	if(MPI_RANK==0)	WRITE(6,6024) Activ_En,  & 
     &                 InRate_k0,  & 
     &                 Area_Factor   
! 
! ...... Convert the intrinsic dissociation energy to [kg/(m^2.Pa.s)] 
! 
!         InRate_k0 = 1.0d3*InRate_k0/MW_Hydrate
         InRate_k0 = InRate_k0*MW_Hydrate/1.0d3
! 
!----------- 
! ...... Allocate memory to arrays related to kinetic dissociation  
!----------- 
! 
         ALLOCATE(SArea_Hyd(MaxNum_Elem), STAT = ier)
! 
! ...... Print explanatory comments  
! 
         IF(ier == 0) THEN
            WRITE(50,6121), MPI_RANK
         ELSE
            WRITE(6, 6122), MPI_RANK
            WRITE(50,6122), MPI_RANK
			CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)
            STOP
         END IF
! 
! 
! 
      END IF IF_Kinetic
! 
!***********************************************************************
!*                                                                     *
!*            Allocate memory to double precision array of             *
!*         inhibitor mass fraction at source (injection well)          *
!*                                                                     *
!***********************************************************************
! 
! 
      ALLOCATE(Inh_WMasF(MaxNum_SS), STAT=ier)
! 
! ... Print explanatory comments  
! 
      IF(ier == 0) THEN
         WRITE(50,6123),MPI_RANK 
      ELSE
         WRITE(6, 6124),MPI_RANK
         WRITE(50,6124),MPI_RANK
		 CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)
         STOP
      END IF
! 
! ... Print dividers for improved legibility     
! 
      IF(MPI_RANK==0) WRITE(6, 6030)
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 5002 FORMAT(8E10.4)
!
 6000 FORMAT(/,'READ_HydrateData  1.0   30 September 2004',6X,&
     &         'Read the hydrate data ',&
     &         'from the HYDRATE block of the input data file') 
!
 6001 FORMAT(//,130('*'),/,130('*'),/,'*',T130,'*',/,'*',&
     &       T39,'H Y D R A T E - R E L A T E D   ',&
     &           'I N F O R M A T I O N',T130,'*',&
     &       /,'*',T130,'*',/,130('*'),/,130('*'),/)
!
 6002 FORMAT(//,T5, 'HYDRATE COMPOSITION',//,&
     &          T10,'Number of Components of the Composite ',&
     &              'Gas Hydrate = ',i1,//,&
     &          T10,'The components are:',/)
 6004 FORMAT(T15,'Component #',i1,':  ',a4,'-Hydrate', /,&
     &       T20,'Hydration number                       =',1pe10.3,/,&
     &       T20,'Mole Fraction in the composite hydrate =',1pe12.5,/,&
     &       T20,'Molecular weight of component hydrate  =',1pe12.5,&
     &             ' g/mol',/)
!
 6005 FORMAT(/,T5, 'PROPERTIES OF THE COMPOSITE HYDRATE',//,&
     &         T10,'MOLECULAR WEIGHT =',1pe12.5,' g/mol')
!
 6008 FORMAT( /,T10,'THERMAL CONDUCTIVITY FUNCTION F_ThC = F(T)',/,&
     &          T15,'Number of coefficients "GH%N_ThC" in the ',&
     &              'F(T) = A0+A1*T+A2*T^2+...+Am*T^m polynomial = ',i2,&
     &       //,T15,'Coefficients Am = "GH%p_ThC" [ m = 0,...,',&
     &              'GH%N_ThC - 1 ] => Units: W/m/C^(m+1) ',/,&
     &          T15,8('A',i1,' = ',1pe12.5,';',2x))
!
 6010 FORMAT( /,T10,'SPECIFIC HEAT FUNCTION F_SpH = F(T)',/,&
     &          T15,'Number of coefficients "GH%N_SpH" in the ',&
     &              'F(T) = A0+A1*T+A2*T^2+...+Am*T^m polynomial = ',i2,&
     &       //,T15,'Coefficients Am = "GH%p_SpH" [ m = 0,...,',&
     &              'GH%N_SpH - 1 ] => Units: J/kg/C^(m+1) ',/,&
     &          T15,8('A',i1,' = ',1pe12.5,';',2x))
!
 6011 FORMAT( /,T10,'DENSITY FUNCTION F_Rho = F(T) ',/,&
     &          T15,'Number of coefficients "GH%N_Rho" in the ',&
     &              'F(T) = A0+A1*T+A2*T^2+...+Am*T^m polynomial = ',i2,&
     &       //,T15,'Coefficients Am = "GH%p_Rho" [ m = 0,...,',&
     &              'GH%N_Rho - 1] => Units: kg/m^3/C^m ',/,&
     &          T15,8('A',i1,' = ',1pe12.5,';',2x))
!
 6012 FORMAT( /,T10,'INHIBITOR-INDUCED TEMPERATURE SHIFT IN THE ',&
     &              'EQUILIBRIUM HYDRATION CURVE (Salt or Alcohol)',/,&
     &          T15,'Maximum temperature shift "T_MaxOff" ',&
     &              '                        = ',&
     &               1pe12.5,' (degrees C)',/, &
     &          T15,'Mole fraction "C_MaxOff" at which the maximum ',&
     &              'T-shift occurs = ',1pe12.5,/,&
     &          T15,'Molecular weight of the inhibitor "MW_Inhib"   ',&
     &              '              = ',1pe12.5,' (g/mol)',/,&
     &          T15,'Density of the inhibitor "D_Inhib"             ',&
     &              '              = ',1pe12.5,' (kg/m^3)',/,&
     &          T15,'Enthalpy of inhibitor dissolution "D_InhSol"   ',&
     &              '              = ',1pe12.5,' (J/kg)',/,&
     &          T15,'Diffusion coefficient "DifCo_Inh" of inhibitor ',&
     &              'in H2O        = ',1pe12.5,' (m^2/s)')
!
 6014 FORMAT( /,T10,'HYDRATION EQUATIONS OPTION NUMBER ',&
     &              '"F_EqOption" = ',i1,/,&
     &          T15,'Flag for selecting the equations used for (a) ',&
     &              'the hydration P-T and (b) H-T relationhips',/,&
     &          T15,' = 1: The Kamath [1984] equations are used',/, &
     &          T15,'/= 1: Extended forms of the Moridis [2003] ',&
     &              'equations are used (DEFAULT)')
!
 6022 FORMAT(///,T5,'TYPE OF HYDRATE FORMATION OR DISSOCIATION : ',&
     &              'K I N E T I C ',//,&
     &          T10,'PARAMETERS OF KINETIC DISSOCIATION')
!
 6023 FORMAT(///,T5,'TYPE OF HYDRATE FORMATION OR DISSOCIATION : ',&
     &              'E Q U I L I B R I U M',/)
!
 6024 FORMAT(/,T15,'Activation energy       = ', 1pe12.5,' J/mol',/,&
     &         T15,'Intrinsic rate constant = ', 1pe12.5,&
     &             ' mol/(m^2.Pa.s)',/,&
     &         T15,'Area adjustment factor  = ', 1pe12.5)
!
 6030 FORMAT(//,130('*'),/,130('*'),/)
!
 6102 FORMAT(//,20('ERROR-'),//,&
     &       T5,'The number of hydrate-producing ',&
     &          'gases "Num_HydrComp" = ',i1,//,&
     &       T5,'"Num_HydrComp" can be either 1 ',&
     &          '(a simple hydrate) or ',&
     &          '"Num_HydrComp" = 2 (binary hydrates),',//,&
     &       T40,'SIMULATION ABORTED - CORRECT AND TRY AGAIN',//,&
     &       20('ERROR-')) 
 6104 FORMAT(//,20('ERROR-'),//,&
     &       T5,'The substance ',a6,' is not among the ',&
     &          'hydrate-producing gases in the TOUGH-Fx/',&
     &          'HYDRATE database',//,&
     &       T40,'SIMULATION ABORTED - CORRECT AND TRY AGAIN',//,&
     &       20('ERROR-')) 
 6105 FORMAT(//,20('ERROR-'),//,&
     &       T5,'The sum of the mole fractions of the components ',&
     &          'of the hydrate ( =',1pe12.5,') do not add up ',&
     &          'to one',//,&
     &       T40,'SIMULATION ABORTED - CORRECT AND TRY AGAIN',//,&
     &       20('ERROR-')) 
 6106 FORMAT(//,20('ERROR-'),//,&
     &       T5,'The variable "Reaction_Type" =',a5,&
     &          ' is neither "EQUIL" or "KINET" - ',&
     &          'Unable to determine type of hydrate reaction',//,&
     &       T40,'SIMULATION ABORTED - CORRECT AND TRY AGAIN',//,&
     &       20('ERROR-')) 
!
 6108 FORMAT(//,20('ERROR-'),//,&
     &       T5,'The number of mass components "NK" =',i2,&
     &          ' is in conflict with the kinetic type',&
     &          ' of hydrate reaction ',/,&
     &       T5,'For a kinetic reaction, "NK" must be either 3 ',&
     &          '(without salt) or 4 (with salt)',//,&
     &       T40,'SIMULATION ABORTED - CORRECT AND TRY AGAIN',//,&
     &       20('ERROR-')) 
 6110 FORMAT(//,20('ERROR-'),//,&
     &       T5,'The number of equations "NEQ" =',i2,&
     &          ' is in conflict with the kinetic type',&
     &          ' of hydrate reaction ',/,&
     &       T5,'For a kinetic reaction, "NEQ" must be either 4 ',&
     &          '(without salt) or 5 (with salt)',//,&
     &       T40,'SIMULATION ABORTED - CORRECT AND TRY AGAIN',//,&
     &       20('ERROR-')) 
!
 6114 FORMAT(T3,'Memory allocation of "GH%p_ThC" in subroutine',&
     &          ' "READ_HydrateData" was successful in process rank',i3)
 6115 FORMAT(//,20('ERROR-'),//,&
     &       T3,'Memory allocation of "GH%p_ThC" in subroutine',&
     &          ' "READ_HydrateData" was unsuccessful in process rank',i3,//,&
     &       T3,'!!!!!!!    THE EXECUTION WAS STOPPED    !!!!!!!',&
     &       //,20('ERROR-'))
!
 6116 FORMAT(T3,'Memory allocation of "GH%p_SpH" in subroutine',&
     &          ' "READ_HydrateData" was successful in process rank',i3)
 6117 FORMAT(//,20('ERROR-'),//,&
     &       T3,'Memory allocation of "GH%p_SpH" in subroutine',&
     &          ' "READ_HydrateData" was unsuccessful in process rank',i3,//,&
     &       T3,'!!!!!!!    THE EXECUTION WAS STOPPED    !!!!!!!',&
     &       //,20('ERROR-'))
!
 6118 FORMAT(T3,'Memory allocation of "GH%p_Rho" in subroutine',&
     &          ' "READ_HydrateData" was successful in process rank',i3)
 6119 FORMAT(//,20('ERROR-'),//,&
     &       T3,'Memory allocation of "GH%p_Rho" in subroutine',&
     &          ' "READ_HydrateData" was unsuccessful in process rank',i3,//,&
     &       T3,'!!!!!!!    THE EXECUTION WAS STOPPED    !!!!!!!',&
     &       //,20('ERROR-'))
!
 6121 FORMAT(T3,'Memory allocation of "SArea_Hyd" in subroutine',&
     &          ' "READ_HydrateData" was successful in process rank',i3)
 6122 FORMAT(//,20('ERROR-'),//,&
     &       T3,'Memory allocation of "SArea_Hyd" in subroutine',&
     &          ' "READ_HydrateData" was unsuccessful in process rank',i3,//,&
     &       T3,'!!!!!!!    THE EXECUTION WAS STOPPED    !!!!!!!',&
     &       //,20('ERROR-'))
!
 6123 FORMAT(T3,'Memory allocation of "Inh_WMasF" in subroutine',&
     &          ' "READ_HydrateData" was successful in process rank',i3)
 6124 FORMAT(//,20('ERROR-'),//,&
     &       T3,'Memory allocation of "Inh_WMasF" in subroutine',&
     &          ' "READ_HydrateData" was unsuccessful in process rank',i3,//,&
     &       T3,'!!!!!!!    THE EXECUTION WAS STOPPED    !!!!!!!',&
     &       //,20('ERROR-'))
!
 6201 FORMAT(' FILE "Hydrate_Info"  EXISTS ==> OPEN AS AN OLD FILE')
 6202 FORMAT(' FILE "Hydrate_Info" DOES NOT EXIST ==>',&
     &       ' OPEN AS A NEW FILE')
!
 6203 FORMAT(T3,'Memory allocation of "GPhase_Com" in subroutine',&
     &          ' "READ_HydrateData" was successful by process rank',i3)
 6204 FORMAT(//,20('ERROR-'),//,&
     &       T3,'Memory allocation of "GPhase_Com" in subroutine',&
     &          ' "READ_HydrateData" was unsuccessful by process rank',i3,//,&
     &       T3,'!!!!!!!    THE EXECUTION WAS STOPPED    !!!!!!!',&
     &       //,20('ERROR-'))
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of READ_HydrateData
!
!
      RETURN
! 
      END SUBROUTINE READ_HydrateData
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE EOS
! 
! ... Modules to be used 
! 
      USE Hydrate_Param
      USE PFMedProp_Arrays
	  USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*    SELECTS THE ACTUAL EOS SUBROUTINE FOR THE HYDRATE SIMULATION     *     
!*                                                                     *
!*                    Version 1.0 - May 26, 2004                       *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of EOS
!
!
      IF(First_call ) THEN
!
         First_call = .FALSE.
         if(MPI_RANK==0) WRITE(11,6000)
!
      END IF
! 
!*********************************************************************
!*                                                                   *
!*    FOR KINETIC DISSOCIATION, USE THE "EOS_Kinetic" SUBROUTINE     *
!*                                                                   *
!*********************************************************************
! 
      IF(F_Kinetic .EQV. .TRUE.) THEN
	  
         CALL EOS_Kinetic
		 
! 
!*********************************************************************
!*                                                                   *
!* FOR EQULIBRIUM DISSOCIATION, USE THE "EOS_Equilibrium" SUBROUTINE *
!*                                                                   *
!*********************************************************************
! 
      ELSE 
         CALL EOS_Equilibrium
      END IF
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'EOS               1.0    6 January   2004',6X,  & 
     &         'Selects the equation of state for kinetic ',   &
     &         'or equilibrium hydrate reaction') 
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of EOS
!
!
      RETURN
!
      END SUBROUTINE EOS                                                            
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE EOS_Equilibrium
! 
! ... Modules to be used 
! 
      USE Universal_Param
      USE Basic_Param
      USE GenControl_Param
!
      USE Variable_Arrays
      USE Element_Arrays
      USE PFMedProp_Arrays
	  
	  USE MPI_PARAM
	  USE MPI_ARRAYS
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*     DETERMINES PHASE TRANSITIONS AND COMPUTES ALL THERMOPHYSICAL    *     
!*       PROPERTIES OF THREE COMPONENTS (Water, Air, HYDRATE IN 1,     *     
!*         2 OR 3 PHASES.  THE AQUEOUS PHASE DOES NOT DISAPPEAR        *     
!*              Kinetic hydrate dissociation or formation              *     
!*                                                                     *
!*                  Version 1.0 - September 28, 2004                   *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: XINCR
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: N,M,NLOC,N1,NMAT
! 
      INTEGER(KIND = 1) :: State_X
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of EOS_Equilibrium
!
!
      IF(First_call) THEN
         First_call = .FALSE.
         IF(MPI_RANK==0) WRITE(11,6000)
      END IF
!                                                                       
      IF (MOP(5) >= 7 .and. MPI_RANK==0) WRITE(6,6001) KCYC,ITER
! 
!***********************************************************************
!*                                                                     *
!*                      SET UP INITIAL CONDITIONS                      *
!*                                                                     *
!***********************************************************************
! 
      IF (KC == 0) CALL EOS_InitAssignEql
! 
      IF (MOP(5) >= 7) WRITE(6,6101)                                     
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>      Main loop determining conditions and assigning properties      >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      IF_N1Val: IF(KC > 0) THEN
                   N1 = NELAL
                ELSE
                   N1 = NELL
                END IF IF_N1Val  
! 
!***********************************************************************
!*                                                                     *
!*        Set updated primary variables after last NR-iteration        *
!*                                                                     *
!***********************************************************************
! 
      DO_NumEle: DO n=1,N1                                                       
!
         NMAT = elem(n)%MatNo  
		 
         NLOC = (N-1)*NK1                                                  
! 
! ----------
! ...... Regular variable update in active gridblocks 
! ----------
! 
         DO_UpdatePV: DO m=1,NK1
! 
            XINCR = 0.0d0                                                      
            IF(ITER /= 0) XINCR = DX(NLOC+m)                               
            XX(m) = X(NLOC+m)+XINCR                                         
! 
         END DO DO_UpdatePV   
! 
! 
! >>>>>>>>>>>>>>>>>>
! .....
! ..... Choice of phase conditions after time step reduction 
! .....
! >>>>>>>>>>>>>>>>>>
! 
! 
         IF_Ihaf_NE_0: IF(IHALVE /= 0 .AND. ITER == 0) THEN                                       
            State_X = elem(n)%StatePoint
         ELSE
            State_X = elem(n)%StateIndex
         END IF IF_Ihaf_NE_0
! 
! ...... Ensure realistic pressures
! 
         IF ( ( ABS(XX(1)) > 1.0d8 ) .OR.  &
     &        ( State_X /= 7_1 .AND. State_X /= 10_1 .AND. &
     &          State_X /= 11_1 .AND. XX(1) < 0.0d0 ) .OR.  &
     &        ( XX(1) /= XX(1) ) ) IGOOD = 5                                 
!                                                                       
         IF (MOP(5) >= 7) WRITE(6,6102) elem(n)%name,(XX(M),M=1,NK1)
!                                                                       
         IF_IGood: IF(IGOOD /= 0) THEN
            CALL WARN_RF(elem(n)%name,XX,NK1,KC)
            RETURN
         END IF IF_IGood                                      
! 
! 
! >>>>>>>>>>>>>>>>>>
! .....
! ..... Choice of phase conditions in a \D2normal\D3 case 
! .....
! >>>>>>>>>>>>>>>>>>
! 
! 
         Case_State: SELECT CASE(State_X)
! 
! ---------- 
! ...... Single phase: Gas           
! ---------- 
!    
         CASE( 1_1)
!    
            CALL PHS1_Gas_Eql(N,NLOC,NMAT)
!    
            IF(IGOOD /= 0) THEN
              RETURN
            ELSE                                      
               CYCLE DO_NumEle  
            END IF                                       
! 
! ---------- 
! ...... Single phase: Aqueous           
! ---------- 
!    
         CASE( 2_1)
!    
            CALL PHS1_Aqu_Eql(N,NLOC,NMAT)
!    
            IF(IGOOD /= 0) THEN
              RETURN
            ELSE                                      
               CYCLE DO_NumEle  
            END IF                                       
! 
! ---------- 
! ...... Two phases: Aqueous+Gas   
! ---------- 
!    
         CASE( 3_1)
!    
            CALL PHS2_AqG_Eql(N,NLOC,NMAT)
!    
            IF(IGOOD /= 0) THEN
               RETURN
            ELSE                                      
               CYCLE DO_NumEle  
            END IF                                       
! 
! ---------- 
! ...... Two phases: Ice+Gas   
! ---------- 
!    
         CASE( 4_1)
!    
            CALL PHS2_IcG_Eql(N,NLOC,NMAT)
!    
            IF(IGOOD /= 0) THEN
               RETURN
            ELSE                                      
               CYCLE DO_NumEle  
            END IF                                       
! 
! ---------- 
! ...... Two phases: Aqueous+Hydrate    
! ---------- 
!    
         CASE( 5_1)
!    
            CALL PHS2_AqH_Eql(N,NLOC,NMAT)
!    
            IF(IGOOD /= 0) THEN
               RETURN
            ELSE                                      
               CYCLE DO_NumEle  
            END IF                                       
! 
! ---------- 
! ...... Two phases: Ice+Hydrate    
! ---------- 
!    
         CASE( 6_1)
!    
            CALL PHS2_IcH_Eql(N,NLOC,NMAT)
!    
            IF(IGOOD /= 0) THEN
               RETURN
            ELSE                                      
               CYCLE DO_NumEle  
            END IF                                       
! 
! ---------- 
! ...... Three phases: Aqueous+Hydrate+Gas    
! ---------- 
!    
         CASE( 7_1)
!    
            CALL PHS3_AGH_Eql(N,NLOC,NMAT)
!    
            IF(IGOOD /= 0) THEN
               RETURN
            ELSE                                      
               CYCLE DO_NumEle  
            END IF                                       
! 
! ---------- 
! ...... Three phases: Aqueous+Ice+Gas    
! ---------- 
!    
         CASE( 8_1)
!    
            CALL PHS3_AIG_Eql(N,NLOC,NMAT)
!    
            IF(IGOOD /= 0) THEN
               RETURN
            ELSE                                      
               CYCLE DO_NumEle  
            END IF                                       
! 
! ---------- 
! ...... Three phases: Aqueous+Ice+Hydrate    
! ---------- 
!    
         CASE( 9_1)
!    
            CALL PHS3_AIH_Eql(N,NLOC,NMAT)
!    
            IF(IGOOD /= 0) THEN
               RETURN
            ELSE                                      
               CYCLE DO_NumEle  
            END IF                                       
! 
! ---------- 
! ...... Three phases: Ice+Hydrate+Gas    
! ---------- 
!    
         CASE(10_1)
!    
            CALL PHS3_IGH_Eql(N,NLOC,NMAT)
!    
            IF(IGOOD /= 0) THEN
               RETURN
            ELSE                                      
               CYCLE DO_NumEle  
            END IF                                       
! 
! ---------- 
! ...... Quadruple point: Aqueous+Hydrate+Gas+Ice    
! ---------- 
!    
         CASE(11_1)
!    
            CALL PHS4_QuP_Eql(N,NLOC,NMAT)
!    
            IF(IGOOD /= 0) THEN
               RETURN
            ELSE                                      
               CYCLE DO_NumEle  
            END IF                                       
!    
         END SELECT Case_State
!                                                                       
!                                                                       
      END DO DO_NumEle                                                        
! 
! 
!***********************************************************************
!*                                                                     *
!*           PRINT SECONDARY PARAMETERS (When MOP(5) >= 8)             *
!*                                                                     *
!***********************************************************************
! 
! 
      IF(MOP(5) >= 8) CALL PRINT_SecPar                                                       
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'EOS_Equilibrium   1.0   28 September 2004',6X, &  
     &         'Thermophysical properties of the Water+CH4+',   &
     &         'Hydrate system - Equilibrium reaction') 
!                                                                       
 6001 FORMAT(/,' %%%%%%%%%% E O S %%%%%%%%%% E O S %%%%%%%%%%',  &  
     &          '  [KCYC,ITER] = [',I4,',',I3,']'/) 
!                                                                       
 6101 FORMAT(/,' PRIMARY VARIABLES',/)                                    
 6102 FORMAT(' AT ELEMENT "',A8,'" --- ',(4(2X,1pE20.13)))              
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of EOS_Equilibrium
!
!
      RETURN
!
      END SUBROUTINE EOS_Equilibrium                                                             
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE EOS_InitAssignEql
! 
! ... Modules to be used 
! 
      USE Universal_Param
      USE Basic_Param
      USE GenControl_Param
!
      USE H2O_Param
      USE Hydrate_Param
!
      USE Variable_Arrays
      USE Element_Arrays
      USE PFMedProp_Arrays
!
      USE H2O_Properties
      USE Hydrate_Properties
      USE RealGas_Properties
	  
	  USE MPI_PARAM
	  USE MPI_ARRAYS
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*     DETERMINES PHASE TRANSITIONS AND COMPUTES ALL THERMOPHYSICAL    *     
!*       PROPERTIES OF THREE COMPONENTS (Water, Air, HYDRATE IN 1,     *     
!*         2 OR 3 PHASES.  THE AQUEOUS PHASE DOES NOT DISAPPEAR        *     
!*            Equilibrium hydrate dissociation or formation            *     
!*                                                                     *
!*                  Version 1.0 - September 29, 2004                   *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: TX,PX,Peq_Hydr,Teq_Hydr,P_CH4,Psat
      REAL(KIND = 8) :: X_mA,Xmol_mA,D_Hydr,H_Hydr,HK_GinH2O
      REAL(KIND = 8) :: T_dev,X_iA,Xmol_iA,Xmol_mA_old,Xmol_iA_old
! 
      REAL(KIND = 8) :: S_aqu,S_gas,S_hyd,S_ice,P_frz
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: n,NLOC
! 
      INTEGER :: HPeq_Paramtrs,PTXS_Paramtrs  ! Function name
! 
! -------
! ... Character variables
! -------
! 
      CHARACTER(LEN = 1) :: H1 = ' '
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call, H1
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of EOS_InitAssignEql
!
!
!    
      IF(First_call) THEN
!
         IF(MPI_RANK==0) WRITE(11,6000)
!
         First_call = .FALSE.
         P_quad_hyd = EqPres_HydrCH4(273.16d0,0.0d0,F_EqOption,IGOOD)
!
      END IF
!                                                                       
      IF (MOP(5) >= 7 .and. MPI_RANK==0) WRITE(6,6001) KCYC,ITER
! 
! -------
! ... Initialization of T-depression variables        
! -------
! 
      T_dev    = 0.0d0  
      Xmol_mA  = 0.0d0
      Xmol_iA  = 0.0d0
! 
      Xmol_mA_old  = 0.0d0
      Xmol_iA_old  = 0.0d0
! 
! ... Initialize the T-shift 
! 
      FORALL (n=1:NELL) Cell_V(n,1)%p_AddD(1) = 0.0d0
! 
!***********************************************************************
!*                                                                     *
!*                      SET UP INITIAL CONDITIONS                      *
!*                                                                     *
!***********************************************************************
! 
      DO_InConSt: DO n=1,NELL                                                     
!
         NLOC = (n-1)*NK1                                                  
!
         IF(NK == 2) THEN
            X_iA = 0.0d0  
            TX   = X(NLOC+3)  
         ELSE
            X_iA = X(NLOC+3)  
            TX   = X(NLOC+4)  
         END IF
!
!***********************************************************************
!*                                                                     *
!*         Determine iteratively the mole fractions at Peq_Hyd         *
!*                                                                     *
!***********************************************************************
!
         IGOOD = HPeq_Paramtrs(1,TX,X_iA,.FALSE., &                         ! In only
     &                           Xmol_iA,Xmol_mA_old,Xmol_iA_old,T_dev,  &  ! In and out
     &                           Peq_Hydr,HK_GinH2O,Xmol_mA,X_mA)          ! Out only
!
         IF(IGOOD /= 0) THEN
            CALL WARN_RF(elem(n)%name,XX,NK1,KC)
            RETURN
         END IF                                       
! 
! 
! >>>>>>>>>>>>>>>>>>
! .....
! ..... Choice of phase conditions 
! .....
! >>>>>>>>>>>>>>>>>>
! 
! 
!write(*,*) elem(n)%StateIndex
         Case_State: SELECT CASE(elem(n)%StateIndex)
!
!***********************************************************************
!*                                                                     *
!*     Check if the element is under single-phase conditions: Gas      *
!*                                                                     *
!***********************************************************************
! 
         CASE(1_1)
! 
! ......... Erroneous 1-phase conditions are specified           
! 
            IF (X(NLOC+2) > 1.0d0) THEN            
               WRITE(6,6053) elem(n)%name,TX,X(NLOC+2)                     
               X(NLOC+2) = 1.00d0                                                   
            END IF 
! 
! ........ Initializing the pressure, temperature, saturations
! 
            elem(n)%P = X(NLOC+1)
            elem(n)%T = TX
! 
            Cell_V(n,0)%p_Satr(1) = 1.0d0
            Cell_V(n,0)%p_Satr(2) = 0.0d0
            Cell_V(n,0)%p_Satr(3) = 0.0d0
            Cell_V(n,0)%p_Satr(4) = 0.0d0
!                                                                       
            Cell_V(n,3)%p_AddD(1) = 0.0d0
!
!***********************************************************************
!*                                                                     *
!*   Check if the element is under single-phase conditions: Aqueous    *
!*                                                                     *
!***********************************************************************
! 
         CASE(2_1)
! 
! ......... Erroneous 1-phase conditions are specified           
! 
            IF (X(NLOC+2) > X_mA) THEN            
               WRITE(6,6054) elem(n)%name,TX,X(NLOC+2),X_mA                     
               X(NLOC+2) = X_mA                                                   
            END IF 
! 
! ........ Initializing the pressure, temperature, saturations
! 
            elem(n)%P = X(NLOC+1)
            elem(n)%T = TX
! 
            Cell_V(n,0)%p_Satr(1) = 0.0d0
            Cell_V(n,0)%p_Satr(2) = 1.0d0
            Cell_V(n,0)%p_Satr(3) = 0.0d0
            Cell_V(n,0)%p_Satr(4) = 0.0d0
!                                                                       
            Cell_V(n,3)%p_AddD(1) = 0.0d0
!
!***********************************************************************
!*                                                                     *
!*   Check if the element is under 2-phase conditions: Aqueous+Gas     *
!*                                                                     *
!***********************************************************************
! 
         CASE(3_1)
! 
! ......... Erroneous 2-phase conditions are specified           
! 
            IF (X(NLOC+1) > Peq_Hydr) THEN            
               WRITE(6,6055) elem(n)%name,TX,X(NLOC+1),Peq_Hydr                        
               X(NLOC+1) = 9.0d-1*Peq_Hydr                                                  
            END IF 
! 
! ------------- 
! ......... Check the water saturation          
! ------------- 
! 
            S_aqu = X(NLOC+2)
! 
            IF(S_aqu > 1.0d0 .OR. S_aqu < 0.0d0) THEN
               WRITE(6,6505) elem(n)%name,S_aqu     ! Print a clarifying message about the error
               STOP                                 ! Stop the simulation                                                
            END IF
! 
! ........ Initializing the pressure, temperature, saturations 
! 
            elem(n)%P = X(NLOC+1)
            elem(n)%T = TX
! 
            Cell_V(n,0)%p_Satr(1) = 1.0d0 - S_aqu
            Cell_V(n,0)%p_Satr(2) = S_aqu
            Cell_V(n,0)%p_Satr(3) = 0.0d0
            Cell_V(n,0)%p_Satr(4) = 0.0d0
!                                                                       
            Cell_V(n,3)%p_AddD(1) = 0.0d0  ! Initial hydrate mass
!
!***********************************************************************
!*                                                                     *
!*     Check if the element is under 2-phase conditions: Ice+Gas       *
!*                                                                     *
!***********************************************************************
! 
         CASE(4_1)
! 
! ......... Erroneous 2-phase conditions are specified           
! 
            IF (X(NLOC+1) > Peq_Hydr) THEN            
               WRITE(6,6055) elem(n)%name,TX,X(NLOC+1),Peq_Hydr                      
               X(NLOC+1) = 9.0d-1*Peq_Hydr                                                  
            END IF 
! 
! ------------- 
! ......... Check the ice saturation          
! ------------- 
! 
            S_ice = X(NLOC+2)
! 
            IF(S_ice > 1.0d0 .OR. S_ice < 0.0d0) THEN
               WRITE(6,6507) elem(n)%name,S_ice     ! Print a clarifying message about the error
               STOP                                 ! Stop the simulation                                                
            END IF
! 
! ........ Initializing the pressure, temperature, saturations 
! 
            elem(n)%P = X(NLOC+1)
            elem(n)%T = TX
! 
            Cell_V(n,0)%p_Satr(1) = 1.0d0 - S_ice
            Cell_V(n,0)%p_Satr(2) = 0.0d0
            Cell_V(n,0)%p_Satr(3) = 0.0d0
            Cell_V(n,0)%p_Satr(4) = S_ice
!                                                                       
            Cell_V(n,3)%p_AddD(1) = 0.0d0  ! Initial hydrate mass
!
!***********************************************************************
!*                                                                     *
!*  Check if the element is under 2-phase conditions: Aqueous+Hydrate  *
!*                                                                     *
!***********************************************************************
! 
         CASE(5_1)
! 
! ......... Determine Peq_Hydr: the equilibrium hydration temperature at PX     
! 
            Teq_Hydr = EqTemp_HydrCH4(X(NLOC+1),T_dev,&
     &                                F_EqOption,1,IGOOD)-273.15d0
!
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
! 
! ......... Erroneous 1-phase conditions are specified           
! 
            IF (TX > Teq_Hydr) THEN            
               WRITE(6,6056) elem(n)%name,X(NLOC+1),TX,Teq_Hydr                   
               X(NLOC+3) = Teq_Hydr*(1.0d0-ZERO)                                                  
            END IF 
! 
! ------------- 
! ......... Check the water saturation          
! ------------- 
! 
            S_aqu = X(NLOC+2)
! 
            IF(S_aqu > 1.0d0 .OR. S_aqu < 0.0d0) THEN
               WRITE(6,6505) elem(n)%name,S_aqu     ! Print a clarifying message about the error
               STOP                                 ! Stop the simulation                                                
            END IF
! 
! ........ Initializing the pressure, temperature, saturations
! 
            elem(n)%P = X(NLOC+1)
            elem(n)%T = X(NLOC+3)
! 
            Cell_V(n,0)%p_Satr(1) = 0.0d0
            Cell_V(n,0)%p_Satr(2) = S_aqu
            Cell_V(n,0)%p_Satr(3) = 1.0d0 - S_aqu
            Cell_V(n,0)%p_Satr(4) = 0.0d0
!                                                                       
! ......... Obtain density of the solid hydrate at TX                                                                 
!                                                                       
            CALL HRho_Hydrate(TX,X(NLOC+1),D_Hydr,H_Hydr)
! 
            Cell_V(n,3)%p_AddD(1) = D_Hydr*Cell_V(n,0)%p_Satr(3)
!
!***********************************************************************
!*                                                                     *
!*    Check if the element is under 2-phase conditions: Ice+Hydrate    *
!*                                                                     *
!***********************************************************************
! 
         CASE(6_1)
! 
! ......... Pressure   
! 
            PX = X(NLOC+1)
! 
! ......... Ice saturation   
! 
            S_ice = X(NLOC+2)
! 
            IF(S_ice > 1.0d0 .OR. S_ice < 0.0d0) THEN
               WRITE(6,6507) elem(n)%name,S_ice      ! Print a clarifying message about the error
               STOP                                  ! Stop the simulation                                                
            END IF
! 
! ......... Determine P_frz: the fusion pressure at TX     
! 
            IGOOD = P_Fusion_H2O_S(TX,P_frz)
!
            IF(IGOOD /= 0) THEN
               P_frz = 1.0d10
            ELSE
               P_frz = P_frz + P_quad_hyd - P_H2O_3p
            END IF                                       
! 
! ......... Determine Peq_Hydr: the hydration equilibrium pressure at TX     
! 
            Peq_Hydr = EqPres_HydrCH4((TX+273.15d0),0.0d0,&
     &                                 F_EqOption,IGOOD)
! 
! ......... Erroneous 1-phase conditions are specified           
! 
            IF (PX > P_frz .OR. PX < Peq_Hydr) THEN            
               WRITE(6,6060) elem(n)%name,PX,P_frz,Peq_Hydr,TX                  
               STOP                                                  
            END IF 
! 
! ........ Initializing the pressure, temperature, saturations
! 
            elem(n)%P = PX
            elem(n)%T = TX
! 
            Cell_V(n,0)%p_Satr(1) = 0.0d0
            Cell_V(n,0)%p_Satr(2) = 0.0d0
            Cell_V(n,0)%p_Satr(3) = 1.0d0 - S_ice
            Cell_V(n,0)%p_Satr(4) = S_ice
!                                                                       
! ......... Obtain density of the solid hydrate at TX                                                                 
!                                                                       
            CALL HRho_Hydrate(TX,PX,D_Hydr,H_Hydr)
! 
            Cell_V(n,3)%p_AddD(1) = D_Hydr*Cell_V(n,0)%p_Satr(3)
!
!***********************************************************************
!*                                                                     *
!*         Check if the element is under 3-phase conditions:           *
!*                      Aqueous + Gas + Hydrate                        *
!*                                                                     *
!***********************************************************************
! 
         CASE(7_1)
! 
! ------------- 
! ......... Check the gas saturation          
! ------------- 
! 
            S_gas = X(NLOC+1)
! 
            IF(S_gas > 1.0d0 .OR. S_gas < 0.0d0) THEN
               WRITE(6,6506) elem(n)%name,S_gas     ! Print a clarifying message about the error
               STOP                                 ! Stop the simulation                                                
            END IF
! 
! ------------- 
! ......... Check the water saturation          
! ------------- 
! 
            S_aqu = X(NLOC+2)
! 
            IF(S_aqu > 1.0d0 .OR. S_aqu < 0.0d0) THEN
               WRITE(6,6505) elem(n)%name,S_aqu     ! Print a clarifying message about the error
               STOP                                 ! Stop the simulation                                                
            END IF
! 
! ------------- 
! ......... The hydrate saturation 
! ------------- 
! 
            S_hyd = 1.0d0 - S_gas - S_aqu
! 
! ......... Erroneous 3-phase conditions are specidfied           
! 
            IF (S_hyd < 0.0d0) THEN            
               WRITE(6,6068) elem(n)%name,(S_gas + S_aqu)                        
               STOP
            END IF 
! 
! ------------- 
! ......... Assigning initial saturations         
! ------------- 
! 
            Cell_V(n,0)%p_Satr(1) = S_gas 
            Cell_V(n,0)%p_Satr(2) = S_aqu
            Cell_V(n,0)%p_Satr(3) = S_hyd
            Cell_V(n,0)%p_Satr(4) = 0.0d0
!                                                                       
            CALL HRho_Hydrate(TX,Peq_Hydr,D_Hydr,H_Hydr)
! 
            Cell_V(n,3)%p_AddD(1) = D_Hydr*Cell_V(n,0)%p_Satr(3)  ! Initial hydrate mass
! 
! ......... Initializing the pressure
! 
            elem(n)%P = Peq_Hydr
            elem(n)%T = TX
!
!***********************************************************************
!*                                                                     *
!*         Check if the element is under 3-phase conditions:           *
!*                        Aqueous + Ice + Gas                          *
!*                                                                     *
!***********************************************************************
! 
         CASE(8_1)
! 
! ------------- 
! ......... Check the gas saturation          
! ------------- 
! 
            IF(NK == 2) THEN 
               S_gas = X(NLOC+3)
            ELSE
               S_gas = X(NLOC+4)
            END IF
! 
            IF(S_gas > 1.0d0 .OR. S_gas < 0.0d0) THEN
               WRITE(6,6506) elem(n)%name,S_gas      ! Print a clarifying message about the error
               STOP                                  ! Stop the simulation                                                
            END IF
! 
! ------------- 
! ......... Check the water saturation          
! ------------- 
! 
            S_aqu = X(NLOC+2)
! 
            IF(S_aqu > 1.0d0 .OR. S_aqu < 0.0d0) THEN
               WRITE(6,6505) elem(n)%name,S_aqu      ! Print a clarifying message about the error
               STOP                                  ! Stop the simulation                                                
            END IF
! 
! ------------- 
! ......... The hydrate saturation 
! ------------- 
! 
            S_ice = 1.0d0 - S_gas - S_aqu
! 
! ......... Erroneous 3-phase conditions are specified           
! 
            IF (S_ice < 0.0d0) THEN            
               WRITE(6,6071) elem(n)%name,(S_gas + S_ice)                        
               STOP
            END IF 
! 
! ------------- 
! ......... Erroneous pressure conditions are specified           
! ------------- 
! 
            IF(X(NLOC+1) > P_quad_hyd) THEN
               !WRITE(6,6057) elem(n)%name,P_quad_hyd,X(NLOC+1)                        
               X(NLOC+1) = P_quad_hyd                                                  
            END IF 
! 
! ......... Initializing the pressure
! 
            elem(n)%P = X(NLOC+1)
! 
! ------------- 
! ......... Assigning initial saturations         
! ------------- 
! 
            Cell_V(n,0)%p_Satr(1) = S_gas 
            Cell_V(n,0)%p_Satr(2) = S_aqu
            Cell_V(n,0)%p_Satr(3) = 0.0d0
            Cell_V(n,0)%p_Satr(4) = S_ice
! 
            Cell_V(n,3)%p_AddD(1) = 0.0d0  ! Initial hydrate mass
! 
! ......... A starting T estimate
! 
            TX = Tc_quad_hyd   ! Initial temperature estimate  
!
! ------------- 
! ......... Determine iteratively partial P, T, T-dev and CH4 solubility   
! ------------- 
!
            IGOOD = PTXS_Paramtrs(1,X(NLOC+1),X_iA,  &                             ! In only
     &                              TX,Xmol_iA,Xmol_mA_old,     &                  ! In and out 
     &                              Xmol_iA_old,T_dev,           &                 ! In and out 
     &                              P_CH4,Psat,HK_GinH2O,X_mA,Xmol_mA)            ! Out only
! 
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
! 
! ......... Initializing the temperature
! 
            elem(n)%T = TX
!
!***********************************************************************
!*                                                                     *
!*         Check if the element is under 3-phase conditions:           *
!*                      Aqueous + Ice + Hydrate                        *
!*                                                                     *
!***********************************************************************
! 
         CASE(9_1)
! 
! ------------- 
! ......... Check the gas saturation          
! ------------- 
! 
            IF(NK == 2) THEN 
               S_ice = X(NLOC+3)
            ELSE
               S_ice = X(NLOC+4)
            END IF
! 
            IF(S_ice > 1.0d0 .OR. S_ice < 0.0d0) THEN
               WRITE(6,6507) elem(n)%name,S_ice      ! Print a clarifying message about the error
               STOP                                  ! Stop the simulation                                                
            END IF
! 
! ------------- 
! ......... Check the water saturation          
! ------------- 
! 
            S_aqu = X(NLOC+2)
! 
            IF(S_aqu > 1.0d0 .OR. S_aqu < 0.0d0) THEN
               WRITE(6,6505) elem(n)%name,S_aqu      ! Print a clarifying message about the error
               STOP                                  ! Stop the simulation                                                
            END IF
! 
! ------------- 
! ......... The hydrate saturation 
! ------------- 
! 
            S_hyd = 1.0d0 - S_ice - S_aqu
! 
! ......... Erroneous 3-phase conditions are specidfied           
! 
            IF (S_hyd < 0.0d0) THEN            
               WRITE(6,6069) elem(n)%name,(S_ice + S_aqu)                        
               STOP
            END IF 
! 
! ------------- 
! ......... Assigning initial saturations         
! ------------- 
! 
            Cell_V(n,0)%p_Satr(1) = 0.0d0
            Cell_V(n,0)%p_Satr(2) = S_aqu
            Cell_V(n,0)%p_Satr(3) = S_hyd
            Cell_V(n,0)%p_Satr(4) = S_ice
!                                                                       
            CALL HRho_Hydrate(TX,Peq_Hydr,D_Hydr,H_Hydr)
! 
            Cell_V(n,3)%p_AddD(1) = D_Hydr*Cell_V(n,0)%p_Satr(3)  ! Initial hydrate mass
! 
! ------------- 
! ......... Erroneous pressure conditions are specified           
! ------------- 
! 
            IF(X(NLOC+1) < P_quad_hyd) THEN
               WRITE(6,6058) elem(n)%name,P_quad_hyd,X(NLOC+1)                        
               X(NLOC+1) = P_quad_hyd                                                  
            END IF 
! 
! -------------
! ......... Initializing the pressure
! -------------
! 
            elem(n)%P = X(NLOC+1)
!
! -------------
! ......... Determine the fusion temperature at the prevailing pressure 
! -------------
!
            IGOOD = T_Fusion_H2O_S((X(NLOC+1)-P_quad_hyd+P_H2O_3p),TX)
! 
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
! 
! -------------
! ......... Initializing the temperature
! -------------
! 
            elem(n)%T = TX                          
!
!***********************************************************************
!*                                                                     *
!*         Check if the element is under 3-phase conditions:           *
!*                       Ice + Gas + Hydrate                           *
!*                                                                     *
!***********************************************************************
! 
         CASE(10_1)
! 
! ------------- 
! ......... Check the gas saturation          
! ------------- 
! 
            S_gas = X(NLOC+1)
! 
            IF(S_gas > 1.0d0 .OR. S_gas < 0.0d0) THEN
               WRITE(6,6506) elem(n)%name,S_gas     ! Print a clarifying message about the error
               STOP                                 ! Stop the simulation                                                
            END IF
! 
! ------------- 
! ......... Check the water saturation          
! ------------- 
! 
            S_ice = X(NLOC+2)
! 
            IF(S_ice > 1.0d0 .OR. S_ice < 0.0d0) THEN
               WRITE(6,6507) elem(n)%name,S_ice     ! Print a clarifying message about the error
               STOP                                 ! Stop the simulation                                                
            END IF
! 
! ------------- 
! ......... The hydrate saturation 
! ------------- 
! 
            S_hyd = 1.0d0 - S_gas - S_ice
! 
! ......... Erroneous 3-phase conditions are specidfied           
! 
            IF (S_hyd < 0.0d0) THEN            
               WRITE(6,6071) elem(n)%name,(S_gas + S_ice)                        
               STOP
            END IF 
! 
! ------------- 
! ......... Assigning initial saturations         
! ------------- 
! 
            Cell_V(n,0)%p_Satr(1) = S_gas 
            Cell_V(n,0)%p_Satr(2) = 0.0d0
            Cell_V(n,0)%p_Satr(3) = S_hyd
            Cell_V(n,0)%p_Satr(4) = S_ice
!                                                                       
            CALL HRho_Hydrate(TX,Peq_Hydr,D_Hydr,H_Hydr)
! 
            Cell_V(n,3)%p_AddD(1) = D_Hydr*Cell_V(n,0)%p_Satr(3)  ! Initial hydrate mass
! 
! ......... Initializing the pressure
! 
            elem(n)%P = Peq_Hydr
            elem(n)%T = TX
!
!***********************************************************************
!*                                                                     *
!*         Check if the element is under 4-phase conditions:           *
!*          Quadruple Point: Aqueous + Ice + Hydrate + Gas             *
!*                                                                     *
!***********************************************************************
! 
         CASE(11_1)
! 
! ------------- 
! ......... Check the gas saturation          
! ------------- 
! 
            S_gas = X(NLOC+1)
! 
            IF(S_gas > 1.0d0 .OR. S_gas < 0.0d0) THEN
               WRITE(6,6506) elem(n)%name,S_gas     ! Print a clarifying message about the error
               STOP                                 ! Stop the simulation                                                
            END IF
! 
! ------------- 
! ......... Check the water saturation          
! ------------- 
! 
            S_aqu = X(NLOC+2)
! 
            IF(S_aqu > 1.0d0 .OR. S_aqu < 0.0d0) THEN
               WRITE(6,6505) elem(n)%name,S_aqu     ! Print a clarifying message about the error
               STOP                                 ! Stop the simulation                                                
            END IF
! 
! ------------- 
! ......... Check the ice saturation          
! ------------- 
! 
            IF(NK == 2) THEN 
               S_ice = X(NLOC+3)
            ELSE
               S_ice = X(NLOC+4)
            END IF
! 
            IF(S_ice > 1.0d0 .OR. S_ice < 0.0d0) THEN
               WRITE(6,6507) elem(n)%name,S_ice     ! Print a clarifying message about the error
               STOP                                 ! Stop the simulation                                                
            END IF
! 
! ------------- 
! ......... The hydrate saturation 
! ------------- 
! 
            S_hyd = 1.0d0 - S_gas - S_aqu - S_ice
! 
! ......... Erroneous 4-phase conditions are specidfied           
! 
            IF (S_hyd < 0.0d0) THEN            
               WRITE(6,6070) elem(n)%name,(S_gas + S_aqu + S_ice)                        
               STOP
            END IF 
! 
! ------------- 
! ......... Assigning initial saturations         
! ------------- 
! 
            Cell_V(n,0)%p_Satr(1) = S_gas
            Cell_V(n,0)%p_Satr(2) = S_aqu
            Cell_V(n,0)%p_Satr(3) = S_hyd
            Cell_V(n,0)%p_Satr(4) = S_ice
! 
! ........ Initializing the pressure
! 
            elem(n)%P = P_quad_hyd
            elem(n)%T = Tc_quad_hyd - T_dev
!                                                                       
! ......... Obtain density of the solid hydrate at TX                                                                 
!                                                                       
            CALL HRho_Hydrate(elem(n)%T,P_quad_hyd,D_Hydr,H_Hydr)
! 
            Cell_V(n,3)%p_AddD(1) = D_Hydr*Cell_V(n,0)%p_Satr(3)       ! Initial hydrate mass
!
!***********************************************************************
!*                                                                     *
!*              Unrealistic/incorrect initial conditions               *
!*                                                                     *
!***********************************************************************
! 
        CASE DEFAULT
! 
           CALL WARN_RF(elem(n)%name,XX,NK1,KC)
! 
        END SELECT Case_State 
!                                                                       
!                                                                       
      END DO DO_InConSt                                                        
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'EOS_InitAssignEql 1.0   29 September 2004',6X, &  
     &         'Initial assignment of thermophysical properties in a ',   &
     &         'hydrate system - Equilibrium reaction') 
!                                                                       
 6001 FORMAT(/,' %%%%%%%%%% E O S %%%%%%%%%% E O S %%%%%%%%%%',  &  
     &          '  [KCYC,ITER] = [',I4,',',I3,']'/) 
!                                                                       
 6505 FORMAT(//,20('ERROR-'),//,   &
     &         T20,'R U N   A B O R T E D',/,   &
     &         T20,'The conditions specified in ',   &
     &             'element "',a8,'" are erroneous !!!',/,   &
     &         T20,'The aqueous phase saturation ',   &
     &             'S_aqu =',1pe11.4,' is outside the possible ',&
     &             'range (0 <= S_aqu <= 1)',//,   &
     &         T20,'CORRECT AND TRY AGAIN',//,   &
     &          20('ERROR-'))
 6506 FORMAT(//,20('ERROR-'),//, &  
     &         T20,'R U N   A B O R T E D',/,  &
     &         T20,'The conditions specified in ',   &
     &             'element "',a8,'" are erroneous !!!',/,  &
     &         T20,'The gas phase saturation ',  &
     &             'S_gas =',1pe11.4,' is outside the possible ',&
     &             'range (0 <= S_gas <= 1)',//, &
     &         T20,'CORRECT AND TRY AGAIN',//,   &
     &          20('ERROR-'))
 6507 FORMAT(//,20('ERROR-'),//,  & 
     &         T20,'R U N   A B O R T E D',/,   &
     &         T20,'The conditions specified in ',   &
     &             'element "',a8,'" are erroneous !!!',/,   &
     &         T20,'The ice phase saturation ',   &
     &             'S_ice =',1pe11.4,' is outside the possible ',&
     &             'range (0 <= S_gas <= 1)',//,  &
     &         T20,'CORRECT AND TRY AGAIN',//,   &
     &          20('ERROR-'))
!                                                                       
 6053 FORMAT(' THE SINGLE-PHASE CONDITIONS SPECIFIED AT ELEMENT "',A8,  &      
     &         '" ARE ERRONEOUS',/,   &
     &         ' At T = ',1pE12.5,' C, the CH4 mole fraction',       &
     &         'in the gas phase "Xmol_mG" =',1pE12.5,   &
     &         ' exceeds the maximum = 1',/,   &
     &         ' !!! ACTION TAKEN: SET Xmol_mG = 1   ==>   EXECUTION',   &
     &         ' CONTINUES',/)   
!                                                                       
 6054 FORMAT(' THE SINGLE-PHASE CONDITIONS SPECIFIED AT ELEMENT "',A8,   &     
     &         '" ARE ERRONEOUS',/,   &
     &         ' At T = ',1pE12.5,' C, the CH4 mass fraction',       &
     &         'in H2O "X_mA" =',1pE12.5,   &
     &         ' exceeds the maximum = ',1pE12.5,' (T-dependent)',/,   &
     &         ' !!! ACTION TAKEN: SET X_mA = Max   ==>   EXECUTION',   &
     &         ' CONTINUES',/)   
!                                                                       
 6055   FORMAT(' THE TWO-PHASE CONDITIONS SPECIFIED AT ELEMENT "',A8,   &   
     &         '" ARE ERRONEOUS',/,   &
     &         ' At T = ',1pE12.5,' C, the pressure P =',1pE15.8,   &
     &         ' Pa exceeds the equilibrium pressure "Peq_Hydr" = ',&
     &         1pE15.8,' Pa',/,   &
     &         ' !!! ACTION TAKEN: SET P = 0.9*Peq_Hydr   ==>',&
     &         '   EXECUTION CONTINUES',/)   
!                                                                       
 6056   FORMAT(' THE TWO-PHASE CONDITIONS SPECIFIED AT ELEMENT "',A8,  &      
     &         '" ARE ERRONEOUS',/,   &
     &         ' At P =',1pE12.5,&
     &         ' Pa, the input temperature T =',1pE12.5,      &
     &         ' oC exceeds the hydrate equilibrium "Teq_Hydr"  = ',&
     &           1pE15.8,' C',/,   &
     &         ' !!! ACTION TAKEN: SET T = Teq_Hydr   ==>   ',&
     &         'EXECUTION CONTINUES',/)   
!                                                                       
 6057   FORMAT(' THE TWO-PHASE CONDITIONS SPECIFIED AT ELEMENT "',A8,    &    
     &         '" ARE ERRONEOUS',/,   &
     &         ' The quadruple point pressure "P_quad_hyd" = ',1pE15.8,   &
     &         ' Pa is less than the total pressure P = ',&
     &           1pE15.8,' Pa',/,   &
     &         ' !!! ACTION TAKEN: SET P = P_quad_hyd   ==>',&
     &         '   EXECUTION CONTINUES',/)   
!                                                                       
 6058   FORMAT(' THE TWO-PHASE CONDITIONS SPECIFIED AT ELEMENT "',A8,   &     
     &         '" ARE ERRONEOUS',/,   &
     &         ' The quadruple point pressure "P_quad_hyd" = ',1pE15.8,   &
     &         ' oC exceeds the total pressure P = ',1pE15.8,' oC',/,   &
     &         ' !!! ACTION TAKEN: SET P = P_quad_hyd   ==>   ',&
     &         'EXECUTION CONTINUES',/)   
!                                                                       
 6060   FORMAT(//,20('ERROR-'),//,   &
     &         T20,'R U N   A B O R T E D',/,   &
     &         ' THE TWO-PHASE CONDITIONS SPECIFIED AT ELEMENT "',A8,&
     &         '" ARE ERRONEOUS',/,   &
     &         ' The pressure "P" = ',1pE15.8,   &
     &         ' P is either > the fusion pressure P_frz = ',1pE15.8,&
     &         ' Pa',/,  &
     &         ' or P < the hydration equilibrium pressure P_eq = ', &
     &           1pE15.8,' Pa corresponding to TX =',1pE12.5,' oC',//,& 
     &         T20,'CORRECT AND TRY AGAIN',//,   &
     &          20('ERROR-'))
!                                                                       
 6068 FORMAT(//,20('ERROR-'),//,  &
     &         T20,'R U N   A B O R T E D',/,   &
     &         T20,'The three-phase conditions specified in ',  &
     &             'element "',a8,'" are erroneous !!!',/,  &
     &         T20,'The sum of the aqueous and gas phase saturations ', &
     &             'S_aqu + S_gas =',1pe11.4,' -> exceeds 1',//,   &
     &         T20,'CORRECT AND TRY AGAIN',//,  &
     &          20('ERROR-'))
!                                                                       
 6069 FORMAT(//,20('ERROR-'),//,  &
     &         T20,'R U N   A B O R T E D',/,  &
     &         T20,'The three-phase conditions specified in ',   &
     &             'element "',a8,'" are erroneous !!!',/,   &
     &         T20,'The sum of the aqueous and gas phase saturations ', &
     &             'S_aqu + S_ice =',1pe11.4,' -> exceeds 1',//,   &
     &         T20,'CORRECT AND TRY AGAIN',//,  &
     &          20('ERROR-'))
!                                                                       
 6070 FORMAT(//,20('ERROR-'),//,   &
     &         T20,'R U N   A B O R T E D',/,   &
     &         T20,'The quadruple point conditions specified in ', &
     &             'element "',a8,'" are erroneous !!!',/,   &
     &         T20,'The sum of the aqueous, gas and ',&
     &             'ice phase saturations ', &
     &             'S_aqu + S_gas + S_ice =',1pe11.4,&
     &             ' -> exceeds 1',//,  &
     &         T20,'CORRECT AND TRY AGAIN',//,  &
     &          20('ERROR-'))
!                                                                       
 6071 FORMAT(//,20('ERROR-'),//, &
     &         T20,'R U N   A B O R T E D',/,  &
     &         T20,'The three-phase conditions specified in ',   &
     &             'element "',a8,'" are erroneous !!!',/,  &
     &         T20,'The sum of the ice and gas phase saturations ',  &
     &             'S_ice + S_gas =',1pe11.4,' -> exceeds 1',//,   &
     &         T20,'CORRECT AND TRY AGAIN',//, &
     &          20('ERROR-'))
!                                                                       
 6100 FORMAT(' !!!!! WARNING: Primary variables with "NaN" values are', &
     &       ' encountered in "EOS".',     &
     &       ' Dt is reduced and the simulation continues <===')                                 
!                                                                       
 6101 FORMAT(/,' PRIMARY VARIABLES',/)                                    
 6102 FORMAT(' AT ELEMENT "',A8,'":',(4(2X,1pE20.13)))              
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of EOS_InitAssignEql
!
!
      RETURN
!
      END SUBROUTINE EOS_InitAssignEql                                                            
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE PHS1_Gas_Eql(N,NLOC,NMAT)
! 
! ... Modules to be used 
! 
      USE Universal_Param
      USE Basic_Param
      USE GenControl_Param
      USE Diffusion_Param
!
      USE H2O_Param
      USE Hydrate_Param
!
      USE Variable_Arrays
      USE Element_Arrays
      USE PFMedProp_Arrays
!
      USE H2O_Properties
      USE Hydrate_Properties
      USE RealGas_Properties
	  
	  USE MPI_Param
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*       ROUTINE FOR COMPUTING AND ASSIGNING ALL THE SECONDARY         *
!*    VARIABLES IN A SINGLE-PHASE (Gas) SYSTEM - Extended T-scale      *
!*                                                                     *
!*                  EQUILIBRIUM HYDRATION REACTION                     *
!*                                                                     *
!*                  Version 1.0 - January 29, 2005                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!                                                                       
      IMPLICIT NONE
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(2) :: Y_inG
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: PX,TX,Psat,ZLiq,ZGas,D_gas,rho_L
      REAL(KIND = 8) :: Hi_Gas,Ui_Gas,Hd_Gas,Ud_Gas,Sd_Gas,H_Gas,U_Gas
! 
      REAL(KIND = 8) :: V_gas,T_k,P_cap
! 
      REAL(KIND = 8) :: Y_mG,X_mG,X_wG,X_iA
      REAL(KIND = 8) :: Peq_Hydr,P_CH4,P_H2O,H_Heat
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN)  :: N,NLOC,NMAT
! 
      INTEGER :: K,ixx
! 
! -------
! ... Character variables     
! -------
! 
      CHARACTER(LEN = 8) :: PhTran 
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of PHS1_Gas_Eql
!
!
      IF(First_call) THEN
         First_call = .FALSE.
         IF(MPI_RANK==0) WRITE(11,6000)
      END IF
! 
! ... Some initializations 
! 
      PhTran = '        '
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>      Main loop determining conditions and assigning properties      >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_NumEqs: DO K=1,NEQ1
!
! ----------
! ...... Initial assignments of primary variable values        
! ...... Primary variables: Px, Y_mG, Tx        
! ----------
! 
         PX   = XX(1)                                                      
         Y_mG = XX(2)
! 
         IF(NK == 2) THEN
            X_iA = 0.0d0
            TX   = XX(3)
         ELSE IF(NK == 3) THEN
            X_iA = XX(3)
            TX   = XX(4)
         END IF
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the first iteration - Unincremented variables
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         IF_1stEq: IF(K == 1) THEN
! 
! -------------
! ......... Determine PSAT: the H2O saturation pressure at TX                                                                       
! -------------
!                                                                       
            IF(TX > Tc_quad_hyd) THEN
               IGOOD = P_Sat_H2O_S(TX,Psat)
            ELSE
               IGOOD = P_Sublm_H2O_S(TX,Psat)
            END IF
!                                                                       
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
! 
! -------------
! ......... Actual CH4 and v-H2O partial pressure       
! -------------
! 
            P_CH4 = Y_mG*PX                                       
            P_H2O = PX - P_CH4                                       
!
!***********************************************************************
!*                                                                     *
!*                    Check phase transition from                      *
!*              1-phase (Gas) to 2-phase (Aqueous + Gas)               *
!*                                                                     *
!***********************************************************************
!
            IF_AquEvol: IF(P_H2O > Psat .AND. TX >= Tc_quad_hyd) THEN
! 
               PhTran = 'Gas->AqG'
               GO TO 1000
! 
            END IF IF_AquEvol
!
!***********************************************************************
!*                                                                     *
!*                    Check phase transition from                      *
!*                1-phase (Gas) to 2-phase (Ice + Gas)                 *
!*                                                                     *
!***********************************************************************
!
            IF_IceEvol: IF(P_H2O > Psat .AND. TX < Tc_quad_hyd) THEN
! 
               PhTran = 'Gas->IcG'
               GO TO 1000
! 
            ELSE
               GO TO 500 
            END IF IF_IceEvol
! 
         END IF IF_1stEq
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the remaining iterations - Incremented variables
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         CASE_Incr: SELECT CASE(K)
! 
! ...... Incrementing pressure 
! 
         CASE(2)                    
! 
            DELX(NLOC+1) = DFAC*XX(1)
            PX           = XX(1) + DELX(NLOC+1)
! 
! ...... Incrementing mole fraction of CH4 in the gas phase
! 
         CASE(3)                     
! 
            DELX(NLOC+2) = DFAC
            Y_mG         = XX(2) + DELX(NLOC+2)
! 
! ...... Incrementing temperature (NK = 2) or salt mass fraction (NK = 3) 
! 
         CASE(4) 
! 
            IF(NK == 2) THEN
               X_iA         = 0.0d0
               DELX(NLOC+3) = DFAC*(273.15d0 + XX(3))
               TX           = XX(3) + DELX(NLOC+3)
            ELSE IF(NK == 3) THEN
               DELX(NLOC+3) = DFAC
               X_iA         = 0.0d0
            END IF
! 
! ...... Incrementing temperature (NK = 3)
! 
         CASE(5) 
! 
            DELX(NLOC+4) = DFAC*(273.15d0 + XX(4))
            TX           = XX(4) + DELX(NLOC+4)
! 
         END SELECT CASE_Incr
!
!***********************************************************************
!*                                                                     *
!*         For augmented primary variables:                            *
!*             Determine the relevant parameters                       *
!*                                                                     *
!***********************************************************************
! 
         IF_GT1: IF(K > 1) THEN
!                                                                       
!.......... Determine the CH4 partial pressure
!                                                                       
            P_CH4 = Y_mG*PX                                       
! 
         END IF IF_GT1
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... Compute thermophysical properties 
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
  500    T_k = TX + 273.15d0  ! Temperature in K                                                    
! 
! ...... Determine Peq_Hydr: the equilibrium hydration pressure at TX       
! 
         Peq_Hydr = EqPres_HydrCH4(T_k,0.0d0,F_EqOption,IGOOD)
! 
! -------------
! ...... Mole fractions in the gas phase      
! -------------
! 
         Y_inG(1) = Y_mG             ! CH4
         Y_inG(2) = 1.0d0-Y_inG(1)   ! H2O
!                                                                      
! ...... Compute mass fractions in the gas phase
!                                                                      
         X_mG = MW_CH4*Y_inG(1)/( MW_CH4*Y_inG(1) + MW_H2O*Y_inG(2) )                                                  
!                                                                      
         X_wG = 1.0d0-X_mG                                                
!
!***********************************************************************
!*                                                                     *
!*             P R O P E R T I E S  +  P A R A M E T E R S             *
!*                                                                     *
!***********************************************************************
!
! ...... Determine the real gas density and compressibility
!                                                                      
         CALL Zcomp(PX,T_k,0.0d0,Y_inG,ZLiq,ZGas,D_gas,rho_L,ixx)
!                                                                      
! ...... Obtain the specific internal energy and enthalpy of CH4
!                                                                      
         CALL HU_IdealGas(0,2.7315d2,T_k,Hi_Gas,Ui_Gas)    ! Ideal enthalpy
         CALL HUS_Departure(T_k,ZGas,Hd_Gas,Ud_Gas,Sd_Gas) ! Departure enthalpy
!                                                                      
         H_gas = ( Hi_Gas - Hd_Gas )    &                 ! Convert to J/Kg/K from J/kgmol/K
     &          /( Y_inG(1)*MW_CH4 + Y_inG(2)*MW_H2O )  
!                                                                      
         U_gas = ( Ui_Gas - Ud_Gas )     &                ! Convert to J/Kg/K from J/kgmol/K
     &          /( Y_inG(1)*MW_CH4 + Y_inG(2)*MW_H2O )  
!                                                                      
! ...... Compute the viscosity of the multicomponent gas mixture
!                                                                      
         V_gas = Gas_Viscosity(PX,T_k,ZGas)                          
!                                                                      
! ...... Obtain capillary pressure
!                                                                      
         CALL CapPres_Hyd(TX,1.0d-6,(1.0d0-1.0d-6),1.0d3,P_cap,nmat)          
!                                                                      
! ----------
! ...... Compute the heat of hydrate dissociation/formation 
! ----------
! 
         H_Heat = 0.0d0
! 
! ----------
! ...... Assign value to state index  
! ----------
! 
         IF(k == 1) elem(n)%StateIndex = 1_1
! 
! ----------
! ...... Assign values of secondary parameters - Gas phase  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(1)   = 1.0d0
         Cell_V(n,k-1)%p_KRel(1)   = 1.0d0
         Cell_V(n,k-1)%p_Visc(1)   = V_gas
         Cell_V(n,k-1)%p_Dens(1)   = D_gas 
         Cell_V(n,k-1)%p_Enth(1)   = H_Gas                 
         Cell_V(n,k-1)%p_PCap(1)   = 0.0d0
         Cell_V(n,k-1)%p_MasF(1,1) = X_wG
         Cell_V(n,k-1)%p_MasF(2,1) = X_mG
         Cell_V(n,k-1)%p_MasF(3,1) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Aqueous phase 
! ----------
! 
         Cell_V(n,k-1)%p_Satr(2)   = 0.0d0
         Cell_V(n,k-1)%p_KRel(2)   = 0.0d0
         Cell_V(n,k-1)%p_Visc(2)   = 1.0d0
         Cell_V(n,k-1)%p_Dens(2)   = 0.0d0
         Cell_V(n,k-1)%p_Enth(2)   = 0.0d0
         Cell_V(n,k-1)%p_PCap(2)   = P_cap
         Cell_V(n,k-1)%p_MasF(1,2) = 0.0d0
         Cell_V(n,k-1)%p_MasF(2,2) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,2) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Hydrate phase  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(3)   = 0.0d0
         Cell_V(n,k-1)%p_Dens(3)   = 0.0d0
         Cell_V(n,k-1)%p_Enth(3)   = 0.0d0
         Cell_V(n,k-1)%p_MasF(1,3) = 0.0d0
         Cell_V(n,k-1)%p_MasF(2,3) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,3) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Ice phase 
! ----------
! 
         Cell_V(n,k-1)%p_Satr(4)   = 0.0d0
         Cell_V(n,k-1)%p_Dens(4)   = 0.0d0
         Cell_V(n,k-1)%p_Enth(4)   = 0.0d0
         Cell_V(n,k-1)%p_MasF(1,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(2,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,4) = 0.0d0
! 
! ----------
! ...... Store the temperature 
! ----------
! 
         Cell_V(n,k-1)%TemC = TX
! 
! ----------
! ...... Store other important variables  
! ----------
! 
         IF(K == 1) THEN
! 
! ......... Store equilibrium hydration pressure at TX 
! 
            Cell_V(n,0)%p_AddD(1) = Peq_Hydr
! 
! ......... Store the hydrate temperature depression  
! 
            Cell_V(n,1)%p_AddD(1) = 0.0d0
! 
         END IF
! 
! ----------
! ...... Store heat of dissociation/formation 
! ----------
! 
         Cell_V(n,k-1)%p_AddD(3) = H_Heat
! 
! 
! 
      END DO DO_NumEqs
! 
! 
! 
      RETURN     ! Task completed - Exit the routine
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                         PHASE TRANSITIONS                           >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
 1000 CONTINUE
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Gas (1-phase)  
! .............. to Aqueous + Gas (2-phase)  
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
      IF_PhTran: IF(PhTran == 'Gas->AqG') THEN
!                                                                       
                    IF(MOP(5) >= 3) WRITE(6,6002) elem(n)%name,&
     &                                            P_H2O,Psat
                    IF(KC == 0) THEN
                       WRITE(6,6101) 
                       STOP                                             
                    END IF
! 
! ................. Reset 2nd variable as aqueous phase saturation
! 
                    XX(2)      = ZERO
                    DX(NLOC+2) = XX(2) - X(NLOC+2)
! 
! ................. Call the single-phase (H2O) subroutine 
! 
                    CALL PHS2_AqG_Eql(N,NLOC,NMAT)
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Gas (1-phase)  
! .............. to Ice + Gas (2-phase)  
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
                 ELSE IF(PhTran == 'Gas->IcG') THEN
! 
                    IF(MOP(5) >= 3) WRITE(6,6003) elem(n)%name,&
     &                                            P_H2O,Psat
                    IF(KC == 0) THEN
                       WRITE(6,6101)                                              
                       STOP                                             
                    END IF
! 
! ................. Reset 2nd variable as ice saturation
! 
                    XX(2)      = ZERO
                    DX(NLOC+2) = XX(2) - X(NLOC+2)
! 
! ................. Call the 3-phase (H2O+Hydrate+Gas) routine 
! 
                    CALL PHS2_IcG_Eql(N,NLOC,NMAT)
! 
                 ELSE 
! 
                    WRITE(6,6102)
                    STOP
! 
                 END IF IF_PhTran
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'PHS1_Gas_Eql      1.0   29 January   2005',6X,   &
     &         'Routine for assigning the secondary parameters of ',   &
     &         'the 1-phase (Gas) ',   &
     &       /,T48,'system - Equilibrium hydrate reaction')

!
 6002 FORMAT(' !!!!!! "PHS1_Gas_Eql": Aqueous phase appears at ',   &
     &       'element "',A8,'" ==> P_H2O = ',1pE15.8,   &
     &       '  P_sat = ',1pE15.8) 
 6003 FORMAT(' !!!!!! "PHS1_Gas_Eql": Ice evolves at ',  &          
     &       'element "',A8,'" ==> P_H2O = ',1pE15.8,  &
     &       '  P_subl = ',1pE15.8) 
! 
 6101 FORMAT(' !!!!!!!!!!   Routine PHS1_Gas_Eql: ', &
     &       'ERRONEOUS DATA INITIALIZATION   ',   &
     &       '==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
 6102 FORMAT(' !!!!!!!!!!   Routine PHS1_Gas_Eql: ',  & 
     &       'ERRONEOUS PHASE CHANGE   ==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of PHS1_Gas_Eql
! 
! 
      RETURN
!
      END SUBROUTINE PHS1_Gas_Eql
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE PHS1_Aqu_Eql(N,NLOC,NMAT)   
! 
! ... Modules to be used 
! 
      USE Universal_Param
      USE Basic_Param
      USE GenControl_Param
      USE Diffusion_Param
!
      USE H2O_Param
      USE Hydrate_Param
!
      USE Variable_Arrays
      USE Element_Arrays
      USE PFMedProp_Arrays
!
      USE H2O_Properties
      USE Hydrate_Properties
      USE RealGas_Properties
	  
	  USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*       ROUTINE FOR COMPUTING AND ASSIGNING ALL THE SECONDARY         *
!*         VARIABLES UNDER SINGLE-PHASE (AQUEOUS) CONDITIONS           *
!*               IN HYDRATE SYSTEMS - Extended T-scale                 *
!*                                                                     *
!*                   EQUILIBRIUM HYDRATION REACTION                    *
!*                                                                     *
!*                  Version 1.0 - September 29, 2004                   *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!                                                                       
      IMPLICIT NONE
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(2) :: Y_inG
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: PX,TX,PSAT,PBUB
      REAL(KIND = 8) :: DW,UW,HW,V_aqu,D_aqu,XDEN
! 
      REAL(KIND = 8) :: X_mA,X_iA,Xmol_mA,Xmol_iA,S_Hydr,Peq_Hydr
      REAL(KIND = 8) :: H_Sol,D_gas,dummy
      REAL(KIND = 8) :: P_CH4,HK_GinH2O,T_dev,H_Heat
! 
      REAL(KIND = 8) :: ZLiq,ZGas,rho_L,T_k
      REAL(KIND = 8) :: Hi_Gas,Ui_Gas,Hd_Gas,Ud_Gas,Sd_Gas,H_Gas,U_Gas
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN)  :: N,NLOC,NMAT
! 
      INTEGER :: K,ixx
! 
! -------
! ... Character variables     
! -------
! 
      CHARACTER(LEN = 8) :: PhTran 
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of PHS1_Aqu_Eql
!
!
      IF(First_call) THEN
         First_call = .FALSE.
         IF(MPI_RANK==0) WRITE(11,6000)
      END IF
! 
! ... Some initializations 
! 
      PhTran = '        '
!
      S_Hydr   = 0.0d0
      Peq_Hydr = 0.0d0
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>      Main loop determining conditions and assigning properties      >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_NumEqs: DO K=1,NEQ1
!
! ----------
! ...... Initial assignments of primary variable values        
! ...... Primary variables: Px, Xaw, Tx        
! ----------
! 
         PX   = XX(1)
         X_mA = XX(2)  ! Mass fraction of CH4 in the aqueous phase
! 
         IF(NK == 2) THEN
            X_iA = 0.0d0
            TX   = XX(3)
         ELSE IF(NK == 3) THEN
            X_iA = XX(3)
            TX   = XX(4)
         END IF
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the first iteration - Umincremented variables
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         IF_1stEq: IF(K == 1) THEN
! 
! -------------
! ......... Determine H2O vapor pressure        
! -------------
! 
            IGOOD = P_Sat_H2O_S(TX,Psat)
! 
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
! 
! -------------
! ......... Constraint on CH4 fraction in the aqueous phase        
! -------------
! 
            IF_CH4MassF: IF (X_mA < 0.0d0) THEN
! 
               IF(mop(5) >= 4) THEN
                  WRITE(6,6001) elem(n)%name,px,X_mA,S_Hydr,tx,psat
               END IF
!                                                                       
               X_mA       = 0.0d0
               XX(2)      = 0.0d0
               DX(NLOC+2) = XX(2)-X(NLOC+2)
!                                                                       
            END IF IF_CH4MassF
! 
! -------------
! ......... Constraint on inhibitor mass fraction in the aqueous phase        
! -------------
! 
            IF_SaltMassF: IF (X_iA < 0.0d0) THEN
               X_iA       =   0.0d0
               XX(3)      =   0.0d0
               DX(NLOC+3) = - X(NLOC+3)
            END IF IF_SaltMassF
! 
! -------------
! ......... Determine mole fractions      
! -------------
! 
            XDEN =(  X_mA/MW_CH4   &                       ! CH4
     &             + X_iA/MW_Inhib  &                      ! Inhibitor
     &             + ( 1.0d0 - X_mA - X_iA )/MW_H2O )     ! Water
 
            Xmol_mA = (X_mA/MW_CH4)/XDEN
!
            Xmol_iA = (X_iA/MW_Inhib)/XDEN
! 
! -------------
! ......... Determine the CH4 partial pressure corresponding to the dissolved CH4 fraction  
! -------------
! 
            P_CH4 = Xmol_mA*HenryS_K(1,TX,Xmol_iA,.TRUE.)
! 
! -------------
! ......... Determine the salt-induced suppression of the hydration T   
! -------------
! 
            T_dev = T_MaxOff*DLOG(1.0d0-Xmol_iA)  & 
     &                      /DLOG(1.0d0-C_MaxOff+1.0d-10)            
! 
! ......... Determine Peq_Hydr: the equilibrium hydration pressure at TX       
! 
            Peq_Hydr = EqPres_HydrCH4(TX+273.15d0,T_dev,&
     &                                F_EqOption,IGOOD)
!
!***********************************************************************
!*                                                                     *
!*                    Check phase transition from                      *
!*          1-phase (Aqueous) to 2-phase (Aqueous + Hydrate)           *
!*                                                                     *
!***********************************************************************
!
            IF_HydrEv: IF(P_CH4 > (Peq_Hydr - Psat)) THEN
! 
               PhTran = 'Aqu->AqH'
               GO TO 1000
! 
            END IF IF_HydrEv
!
!***********************************************************************
!*                                                                     *
!*                    Check phase transition from                      *
!*            1-phase (Aqueous) to 2-phase (Aqueous + Gas)             *
!*                                                                     *
!***********************************************************************
!
!......... Compute the sum of the H2O and the CH4 pressures: switching criterion 
! 
            PBUB = PSAT + P_CH4
! 
! ......... Gas evolution   
! 
            IF_GasEv: IF(PX < PBUB) THEN
! 
               PhTran = 'Aqu->AqG'
               GO TO 1000
! 
            ELSE
               GO TO 500 
            END IF IF_GasEv
! 
         END IF IF_1stEq
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the remaining iterations - Incremented variables
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         CASE_Incr: SELECT CASE(K)
! 
! ...... Incrementing pressure 
! 
         CASE(2)                    
! 
            DELX(NLOC+1) = DFAC*XX(1)
            PX           = XX(1) + DELX(NLOC+1)
! 
! ...... Incrementing mass fraction of dissolved CH4 in H2O 
! 
         CASE(3)                     
! 
            DELX(NLOC+2) = DFAC
            X_mA         = XX(2) + DELX(NLOC+2)
! 
! ...... Incrementing temperature (NK = 2) or salt mass fraction (NK = 3) 
! 
         CASE(4) 
! 
            IF(NK == 2) THEN
               X_iA         = 0.0d0
               DELX(NLOC+3) = DFAC*(273.15d0 + XX(3))
               TX           = XX(3) + DELX(NLOC+3)
            ELSE IF(NK == 3) THEN
               DELX(NLOC+3) = DFAC
               X_iA         = XX(3) + DELX(NLOC+3)
            END IF
! 
! ...... Incrementing temperature (NK = 3)
! 
         CASE(5) 
! 
            DELX(NLOC+4) = DFAC*(273.15d0 + XX(4))
            TX           = XX(4) + DELX(NLOC+4)
! 
         END SELECT CASE_Incr
!
!***********************************************************************
!*                                                                     *
!*         For augmented primary variables:                            *
!*             Determine the relevant parameters                       *
!*                                                                     *
!***********************************************************************
! 
         IF_KGT1: IF(K > 1) THEN
!                                                                       
            XDEN =(  X_mA/MW_CH4   &                       ! CH4
     &             + X_iA/MW_Inhib  &                      ! Inhibitor
     &             + ( 1.0d0 - X_mA - X_iA )/MW_H2O )     ! Water
! 
            Xmol_iA = (X_iA/MW_Inhib)/XDEN                ! Inhibitor
! 
! ......... Determine the salt-induced suppression of the hydration T   
! 
            T_dev = T_MaxOff*DLOG(1.0d0-Xmol_iA)   &
     &                      /DLOG(1.0d0-C_MaxOff-1.0d-10)            
!                                                                       
         END IF IF_KGT1
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... Compute thermophysical properties 
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
  500    T_k = TX + 273.15d0  ! ...... Temperature in K   
!
!***********************************************************************
!*                                                                     *
!*             P R O P E R T I E S  +  P A R A M E T E R S             *
!*                                                                     *
!***********************************************************************
!
! 
! -------------
! ...... Mole fractions in the dissolved gas      
! -------------
! 
         Y_inG(1) = 1.0d0   ! CH4
         Y_inG(2) = 0.0d0   ! H2O
!                                                                      
! -------------
! ...... Determine the real gas density and compressibility
! -------------
!                                                                      
         CALL Zcomp(PX,T_k,0.0d0,Y_inG,ZLiq,ZGas,D_gas,rho_L,ixx)
!                                                                     
! ----------
! ...... Internal energy and density of liquid H2O                                                                       
! ----------
!                                                                       
         CALL UHRho_LiqH2O(TX,PX,DW,UW,HW,IGOOD)
! 
! ----------
! ...... Mixture model for aqueous phase density: Assume volume  
! ...... conservation during CH4 and salt dissolution  
! ----------
! 
         D_aqu = X_iA*D_Inhib+X_mA*D_gas+(1.0d0-X_iA-X_mA)*DW    
!                                                                       
! ----------
! ...... Viscosity of liquid H2O                                                                        
! ----------
!                                                                       
         V_aqu = ViscLV_H2Ox(TX,D_aqu)            ! Aqueous phase
! 
! ----------
! ...... Compute the enthalpy of the dissolved CH4 
! ----------
! 
         HK_GinH2O = HenryS_K(1,TX,Xmol_iA,.TRUE.) !is it wrong?.........RPS
!
! ...... Gas enthalpy and internal energy
!
         CALL HU_IdealGas(1,0.0d0,T_k,Hi_Gas,Ui_Gas)                                  
         CALL HUS_Departure(T_k,ZGas,Hd_Gas,Ud_Gas,Sd_Gas) ! Departure enthalpy
!                                                                      
         H_Gas = (Hi_Gas - Hd_Gas)/MW_CH4          ! Convert to J/Kg/K from J/kgmol/K
         U_Gas = (Ui_Gas - Ud_Gas)/MW_CH4          
!
! ...... Enthalpy of CH4 dissolution
!
         H_Sol = HEAT_GasSolution(1,TX,Xmol_iA,HK_GinH2O,dummy)                                     
! 
! ----------
! ...... Compute the heat of hydrate dissociation/formation 
! ----------
! 
         H_Heat = HeatDiss_HydrCH4(T_k,&
     &                             F_EqOption, &  ! H-equation
     &                             IGOOD)
! 
! ----------
! ...... Assign value to state index  
! ----------
! 
         IF(k == 1) elem(n)%StateIndex = 2_1
! 
! ----------
! ...... Assign values of secondary parameters - Gas phase
! ----------
! 
         Cell_V(n,k-1)%p_Satr(1)   = 0.0d0
         Cell_V(n,k-1)%p_KRel(1)   = 0.0d0
         Cell_V(n,k-1)%p_Visc(1)   = 1.0d0
         Cell_V(n,k-1)%p_Dens(1)   = 0.0d0
         Cell_V(n,k-1)%p_Enth(1)   = 0.0d0
         Cell_V(n,k-1)%p_PCap(1)   = 0.0d0
         Cell_V(n,k-1)%p_MasF(1,1) = 0.0d0
         Cell_V(n,k-1)%p_MasF(2,1) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,1) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Aqueous phase 
! ----------
! 
         Cell_V(n,k-1)%p_Satr(2)   = 1.0d0
         Cell_V(n,k-1)%p_KRel(2)   = 1.0d0
         Cell_V(n,k-1)%p_Visc(2)   = V_aqu
         Cell_V(n,k-1)%p_Dens(2)   = D_aqu
         Cell_V(n,k-1)%p_Enth(2)   = (1.0d0 - X_mA - X_iA)*HW &
     &                              + X_mA*(H_Gas + H_Sol) &
     &                              + X_iA*H_InhSol
         Cell_V(n,k-1)%p_PCap(2)   = 0.0d0
         Cell_V(n,k-1)%p_MasF(1,2) = 1.0d0 - X_mA - X_iA
         Cell_V(n,k-1)%p_MasF(2,2) = X_mA
         Cell_V(n,k-1)%p_MasF(3,2) = X_iA
! 
! ----------
! ...... Assign values of secondary parameters - Hydrate phase 
! ----------
! 
         Cell_V(n,k-1)%p_Satr(3)   = 0.0d0
         Cell_V(n,k-1)%p_Dens(3)   = 0.0d0
         Cell_V(n,k-1)%p_Enth(3)   = 0.0d0
         Cell_V(n,k-1)%p_MasF(1,3) = 0.0d0
         Cell_V(n,k-1)%p_MasF(2,3) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,3) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Ice phase 
! ----------
! 
         Cell_V(n,k-1)%p_Satr(4)   = 0.0d0
         Cell_V(n,k-1)%p_Dens(4)   = 0.0d0
         Cell_V(n,k-1)%p_Enth(4)   = 0.0d0
         Cell_V(n,k-1)%p_MasF(1,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(2,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,4) = 0.0d0
! 
! ----------
! ...... Store the temperature 
! ----------
! 
         Cell_V(n,k-1)%TemC = TX
! 
! ----------
! ...... Store other important variables  
! ----------
! 
         IF(K == 1) THEN
! 
! ......... Store equilibrium hydration pressure at TX 
! 
            Cell_V(n,0)%p_AddD(1) = Peq_Hydr
! 
! ......... Store the hydrate temperature depression  
! 
            Cell_V(n,1)%p_AddD(1) = T_dev
! 
         END IF
! 
! ----------
! ...... Store heat of dissociation/formation 
! ----------
! 
         Cell_V(n,k-1)%p_AddD(3) = H_Heat
! 
! 
! 
      END DO DO_NumEqs
! 
! 
! 
      RETURN     ! Task completed - Exit the routine
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                         PHASE TRANSITIONS                           >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
 1000 CONTINUE
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Aqueous (1-phase)
! .............. to Aqueous + Hydrate (2-phase)    
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
      IF_PhTran: IF(PhTran == 'Aqu->AqH') THEN
! 
                    IF(MOP(5) >= 3) WRITE(6,6003) elem(n)%name,P_CH4,&
     &                                            Peq_Hydr,Xmol_mA
                    IF(KC == 0) THEN
                       WRITE(6,6101) 
                       STOP                                             
                    END IF
! 
! ................. Reset the 2nd variable as aqueous phase saturation 
! 
                    XX(2)      = 1.0d0-ZERO
                    DX(NLOC+2) = XX(2)-X(NLOC+2)
! 
! ................. Call the 2-phase (H2O+Hydrate) routine 
! 
                    CALL PHS2_AqH_Eql(N,NLOC,NMAT)
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Aqueous (1-phase)
! .............. to Aqueous + Gas (2-phase)    
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
                 ELSE IF(PhTran == 'Aqu->AqG') THEN
! 
                    IF(MOP(5) >= 3) WRITE(6,6004) elem(n)%name,PX,PBUB
                    IF(KC == 0) THEN
                       WRITE(6,6101)                                              
                       STOP                                             
                    END IF
!                                                                       
                    XX(2)      = 1.0d0-ZERO
                    DX(NLOC+2) = XX(2)-X(NLOC+2)
! 
! ................. Call the 2-phase (H2O+Gas) routine 
! 
                    CALL PHS2_AqG_Eql(N,NLOC,NMAT)
! 
                 ELSE 
! 
                    WRITE(6,6102)
                    STOP
! 
                 END IF IF_PhTran
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'PHS1_Aqu_Eql      1.0   29 September 2004',6X, &  
     &         'Routine for assigning the secondary ',  &
     &         'parameters under 1-phase (aqueous) conditions ',/,T48,  &
     &         'in a hydrate system - ',   &
     &         'Equilibrium hydrate reaction')
!
 6001 FORMAT(' X_mA in "PHS1_Aqu_Eql" : Element "',A8,   &
     &       '" ==> Px =',1pE13.6,' X_mA =',1pE13.6,   &
     &       ' S_Hydr =',1pE13.6,' Tx =',1pE13.6,  &
     &       ' PSat_H2O =',1pE13.6)
 6002 FORMAT(' S_Hydr in "PHS1_Aqu_Eql" : Element "',A8,   &
     &       '" ==> Px =',1pE13.6,' X_mA =',1pE13.6,  &
     &       'S_Hydr =',1pE13.6,' Tx =',1pE13.6,  &
     &       ' PSat_H2O =',1pE13.6)
 6003 FORMAT(' !!!!!! "PHS1_Aqu_Eql": Hydrate evolves at element "',A8, &
     &       '" ==> P_CH4 = ',1pE15.8,'  Peq_Hydr = ',1pE15.8,   &
     &       '  X_mA = ',1pE15.8)
 6004 FORMAT(' !!!!!! "PHS1_Aqu_Eql": Gas phase evolves at ', &
     &       'element "',A8,'" ==> Px  = ',1pE15.8,'  Pbub = ',1pE15.8)
! 
 6101 FORMAT(' !!!!!!!!!!   Routine PHS1_Aqu_Eql: ',   &
     &       'ERRONEOUS DATA INITIALIZATION   ',       &
     &       '==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
 6102 FORMAT(' !!!!!!!!!!   Routine PHS1_Aqu_Eql: ',  &
     &       'ERRONEOUS PHASE CHANGE   ==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of PHS1_Aqu_Eql
! 
! 
      RETURN
!
      END SUBROUTINE PHS1_Aqu_Eql
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE PHS2_AqH_Eql(N,NLOC,NMAT)
! 
! ... Modules to be used 
! 
      USE Universal_Param
      USE Basic_Param
      USE GenControl_Param
      USE Diffusion_Param
!
      USE H2O_Param
      USE Hydrate_Param
!
      USE Variable_Arrays
      USE Element_Arrays
      USE PFMedProp_Arrays
!
      USE H2O_Properties
      USE Hydrate_Properties
      USE RealGas_Properties
	  USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*       ROUTINE FOR COMPUTING AND ASSIGNING ALL THE SECONDARY         *
!*            VARIABLES IN TWO-PHASE (H2O+HYDRATE) SYSTEMS             *
!*                                                                     *
!*                  EQUILIBRIUM HYDRATION REACTION                     *
!*                                                                     *
!*                 Version 1.0 - September 29, 2004                    *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!                                                                       
      IMPLICIT NONE
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(2) :: Y_inG
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: PX,S_aqu,TX,D_gas
      REAL(KIND = 8) :: DW,UW,HW,D_Hydr,H_Hydr
      REAL(KIND = 8) :: k_rel_aqu,k_rel_gas,D_aqu,V_aqu
! 
      REAL(KIND = 8) :: X_mA,Xmol_mA,S_Hydr,Peq_Hydr,dummy
      REAL(KIND = 8) :: H_Sol,HK_GinH2O,H_Heat
      REAL(KIND = 8) :: T_dev,X_iA,Xmol_iA,Xmol_mA_old,Xmol_iA_old
! 
      REAL(KIND = 8) :: ZLiq,ZGas,rho_L,T_k,T_frz
      REAL(KIND = 8) :: Hi_Gas,Ui_Gas,Hd_Gas,Ud_Gas,Sd_Gas,H_Gas,U_Gas
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN)  :: N,NLOC,NMAT
! 
      INTEGER :: K, ixx
! 
      INTEGER :: HPeq_Paramtrs  ! Integer function
! 
! -------
! ... Character variables     
! -------
! 
      CHARACTER(LEN = 8) :: PhTran 
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of PHS2_AqH_Eql
!
!
      IF(First_call) THEN
         First_call = .FALSE.
         IF(MPI_RANK==0) WRITE(11,6000)
      END IF
! 
! -------
! ... Initializations
! -------
! 
      T_dev   = Cell_V(n,1)%p_AddD(1)  
      Xmol_mA = 0.0d0
      Xmol_iA = 0.0d0
! 
      Xmol_mA_old = 0.0d0
      Xmol_iA_old = 0.0d0
! 
      PhTran = '        '
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>      Main loop determining conditions and assigning properties      >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_NumEqs: DO K=1,NEQ1
!
! ----------
! ...... Initial assignments of primary variable values        
! ...... Primary variables: Px, S_aqu, Tx        
! ----------
! 
         PX    = XX(1)
         S_aqu = XX(2)
! 
         IF(NK == 2) THEN
            X_iA = 0.0d0
            TX   = XX(3)
         ELSE IF(NK == 3) THEN
            X_iA = XX(3)
            TX   = XX(4)
         END IF
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the first iteration - Unincremented variables
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         IF_1stEq: IF(K == 1) THEN
! 
! -------------
! ......... Constraint on water saturation        
! -------------
! 
            IF_SwLim: IF (S_aqu <= 0.0d0) THEN
               S_aqu      = 1.0000d-4
               XX(2)      = S_aqu
               DX(NLOC+2) = XX(2) - X(NLOC+2)
            END IF IF_SwLim
! 
! -------------
! ......... Constraint on inhibitor mass fraction in the aqueous phase        
! -------------
! 
            IF_SaltMassF: IF (X_iA < 0.0d0) THEN
               X_iA       =   0.0d0
               XX(3)      =   0.0d0
               DX(NLOC+3) = - X(NLOC+3)
            END IF IF_SaltMassF
! 
! -------------
! ......... Determine Peq_Hydr and the corresponding mole fractions       
! -------------
! 
            IGOOD = HPeq_Paramtrs(1,TX,X_iA,.FALSE.,  &                        ! In only
     &                              Xmol_iA,Xmol_mA_old, &                     ! In and out
     &                              Xmol_iA_old,T_dev,    &                    ! In and out
     &                              Peq_Hydr,HK_GinH2O,Xmol_mA,X_mA)          ! Out only
! 
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
!
!***********************************************************************
!*                                                                     *
!*                    Check phase transition from                      *
!*          2-phase (Aqueous + Hydrate) to 1-phase (Aqueous)           *
!*                                                                     *
!***********************************************************************
!
            IF_HydDis: IF(S_aqu > 1.0d0) THEN    ! Hydrate disappears  
! 
               PhTran = 'AqH->Aqu'
               GO TO 1000
! 
            END IF IF_HydDis
!
!***********************************************************************
!*                                                                     *
!*       Check phase transition from 2-phase (Aqueous + Hydrate)       *
!*                to 3-phase (Aqueous + Ice + Hydrate)                 *
!*                                                                     *
!***********************************************************************
!

            IGOOD = T_Fusion_H2O_S((PX-P_quad_hyd+P_H2O_3p),T_frz)   ! The fusion temperature at PX
! 
! ......... Ice evolution  
! 
            IF_IceEvolv: IF( (IGOOD == 0) .AND. &
     &                       (TX < (T_frz-T_dev)) ) &
     &      THEN  
! 
               PhTran = 'AqH->AIH'
               GO TO 1000
! 
            END IF IF_IceEvolv
!
!***********************************************************************
!*                                                                     *
!*       Check phase transition from 2-phase (Aqueous + Hydrate)       *
!*                to 3-phase (Aqueous + Gas + Hydrate)                 *
!*                                                                     *
!***********************************************************************
!
! ......... Gas evolution  
! 
            IF_GasEv: IF(PX*(1.0d0+ZERO) < Peq_Hydr) THEN
! 
               PhTran = 'AqH->AGH'
               GO TO 1000
! 
            ELSE
               GO TO 500 
            END IF IF_GasEv
! 
         END IF IF_1stEq
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the remaining iterations - Incremented variables
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         CASE_Incr: SELECT CASE(K)
! 
! ...... Incrementing pressure 
! 
         CASE(2)                    
! 
            DELX(NLOC+1) = DFAC*XX(1)
            PX           = XX(1) + DELX(NLOC+1)
! 
! ...... Incrementing water saturation 
! 
         CASE(3)                     
! 
            DELX(NLOC+2) = DFAC
            S_aqu        = XX(2) + DELX(NLOC+2)
! 
! ...... Incrementing temperature (NK = 2) or salt mass fraction (NK = 3) 
! 
         CASE(4) 
! 
            IF(NK == 2) THEN
               X_iA         = 0.0d0
               DELX(NLOC+3) = DFAC*(273.15d0 + XX(3))
               TX           = XX(3) + DELX(NLOC+3)
            ELSE IF(NK == 3) THEN
               DELX(NLOC+3) = DFAC
               X_iA         = XX(3) + DELX(NLOC+3)
            END IF
! 
! ...... Incrementing temperature (NK = 3)
! 
         CASE(5) 
! 
            DELX(NLOC+4) = DFAC*(273.15d0 + XX(4))
            TX           = XX(4) + DELX(NLOC+4)
! 
         END SELECT CASE_Incr
!
!***********************************************************************
!*                                                                     *
!*         For augmented primary variables:                            *
!*             Determine the relevant parameters                       *
!*                                                                     *
!***********************************************************************
! 
         IF_KGT1: IF(K > 1) THEN
! 
            IGOOD = HPeq_Paramtrs(1,TX,X_iA,.FALSE.,   &                     ! In only
     &                            Xmol_iA,Xmol_mA_old,Xmol_iA_old,T_dev,  &  ! In and out
     &                            Peq_Hydr,HK_GinH2O,Xmol_mA,X_mA)          ! Out only
! 
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
! 
         END IF IF_KGT1                                     
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... Compute thermophysical properties 
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
  500    T_k = TX + 273.15d0   ! Temperature in K   
! 
! ----------
! ...... The hydrate saturation 
! ----------
! 
         S_Hydr = 1.0d0 - S_aqu   
!
!***********************************************************************
!*                                                                     *
!*             P R O P E R T I E S  +  P A R A M E T E R S             *
!*                                                                     *
!***********************************************************************
!
! 
! -------------
! ...... Mole fractions in the dissolved gas      
! -------------
! 
         Y_inG(1) = 1.0d0   ! CH4
         Y_inG(2) = 0.0d0   ! H2O
!                                                                      
! -------------
! ...... Determine the real gas density and compressibility
! -------------
!                                                                      
         CALL Zcomp(PX,T_k,0.0d0,Y_inG,ZLiq,ZGas,D_gas,rho_L,ixx)
!                                                                       
! ----------
! ...... Obtain density and internal energy of liquid H2O                                                                  
! ----------
!                                                                       
         CALL UHRho_LiqH2O(TX,PX,DW,UW,HW,IGOOD)
! 
! ----------
! ...... Mixture model for aqueous phase density: Assume volume  
! ...... conservation during Hydrate dissolution  
! ----------
! 
         D_aqu = X_iA*D_Inhib+X_mA*D_gas+(1.0d0-X_iA-X_mA)*DW    
!                                                                       
! ----------
! ...... Obtain liquid H2O viscosity                                                             
! ----------
!                                                                       
         V_aqu = ViscLV_H2Ox(TX,D_aqu) 
!                                                                       
! ----------
! ...... Obtain density and internal energy of the solid hydrate                                                                  
! ----------
!                                                                       
         CALL HRho_Hydrate(TX,PX,D_Hydr,H_Hydr)
!                                                                       
! ----------
! ...... Obtain relative permeabilities and capillary pressures                                                              
! ----------
!                                                                       
         CALL RelPerm_Hyd(S_aqu,0.0d0,k_rel_aqu,k_rel_gas,dummy,NMAT)
! 
! ----------
! ...... Compute the enthalpy and internal energy of the dissolved CH4 
! ----------
! 
         CALL HU_IdealGas(1,0.0d0,T_k,Hi_Gas,Ui_Gas)                                  
         CALL HUS_Departure(T_k,ZGas,Hd_Gas,Ud_Gas,Sd_Gas) ! Departure enthalpy
!                                                                      
         H_Gas = (Hi_Gas - Hd_Gas)/MW_CH4     ! Convert to J/Kg/K from J/kgmol/K
         U_Gas = (Ui_Gas - Ud_Gas)/MW_CH4     ! Convert to J/Kg/K from J/kgmol/K
!
! ...... Dissolution enthalpy of CH4 at TX (J/kkg)
!
         H_Sol = HEAT_GasSolution(1,TX,Xmol_iA,HK_GinH2O,dummy)                                     
! 
! ----------
! ...... Compute the heat of hydrate dissociation/formation 
! ----------
! 
         H_Heat = HeatDiss_HydrCH4(T_k,&
     &                             F_EqOption, &  ! H-equation
     &                             IGOOD)
! 
! ----------
! ...... Assign value to state index  
! ----------
! 
         IF(k == 1) elem(n)%StateIndex = 5_1
! 
! ----------
! ...... Assign values of secondary parameters - Gas phase  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(1)   = 0.0d0
         Cell_V(n,k-1)%p_KRel(1)   = 0.0d0
         Cell_V(n,k-1)%p_Visc(1)   = 1.0d0
         Cell_V(n,k-1)%p_Dens(1)   = 0.0d0
         Cell_V(n,k-1)%p_Enth(1)   = 0.0d0
         Cell_V(n,k-1)%p_PCap(1)   = 0.0d0
         Cell_V(n,k-1)%p_MasF(1,1) = 0.0d0
         Cell_V(n,k-1)%p_MasF(2,1) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,1) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Aqueous phase
! ----------
! 
         Cell_V(n,k-1)%p_Satr(2)   = S_aqu
         Cell_V(n,k-1)%p_KRel(2)   = k_rel_aqu    ! REPW
         Cell_V(n,k-1)%p_Visc(2)   = V_aqu
         Cell_V(n,k-1)%p_Dens(2)   = D_aqu
         Cell_V(n,k-1)%p_Enth(2)   = (1.0d0 - X_mA - X_iA)*HW &
     &                              + X_mA*(H_Gas + H_Sol) &
     &                              + X_iA*H_InhSol
         Cell_V(n,k-1)%p_PCap(2)   = 0.0d0           
         Cell_V(n,k-1)%p_MasF(1,2) = 1.0d0-X_mA-X_iA
         Cell_V(n,k-1)%p_MasF(2,2) = X_mA
         Cell_V(n,k-1)%p_MasF(3,2) = X_iA
! 
! ----------
! ...... Assign values of secondary parameters - Hydrate phase  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(3)   = S_Hydr
         Cell_V(n,k-1)%p_Dens(3)   = D_Hydr
         Cell_V(n,k-1)%p_Enth(3)   = H_Hydr
         Cell_V(n,k-1)%p_MasF(1,3) = HCom%H2OMF(1)
         Cell_V(n,k-1)%p_MasF(2,3) = HCom%GasMF(1)
         Cell_V(n,k-1)%p_MasF(3,3) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Ice phase 
! ----------
! 
         Cell_V(n,k-1)%p_Satr(4)   = 0.0d0
         Cell_V(n,k-1)%p_Dens(4)   = 0.0d0
         Cell_V(n,k-1)%p_Enth(4)   = 0.0d0
         Cell_V(n,k-1)%p_MasF(1,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(2,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,4) = 0.0d0
! 
! ----------
! ...... Store the temperature 
! ----------
! 
         Cell_V(n,k-1)%TemC = TX
! 
! ----------
! ...... Store other important variables  
! ----------
! 
         IF(K == 1) THEN
! 
! ......... Store equilibrium hydration pressure at TX 
! 
            Cell_V(n,0)%p_AddD(1) = Peq_Hydr
! 
! ......... Store the hydrate temperature depression  
! 
            Cell_V(n,1)%p_AddD(1) = T_dev
! 
         END IF
! 
! ----------
! ...... Store heat of dissociation/formation 
! ----------
! 
         Cell_V(n,k-1)%p_AddD(3) = H_Heat
! 
! 
! 
      END DO DO_NumEqs
! 
! 
! 
      RETURN     ! Task completed - Exit the routine
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                         PHASE TRANSITIONS                           >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
 1000 CONTINUE
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Aqueous + Hydrate (2-phase)
! .............. to Aqueous (1-phase)      
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
      IF_PhTran: IF(PhTran == 'AqH->Aqu') THEN
! 
                    IF(MOP(5) >= 3) WRITE(6,6001) elem(n)%name,&
     &                                           (1.0d0 - S_aqu)
                    IF(KC == 0) THEN
                       WRITE(6,6101) 
                       STOP                                             
                    END IF
!                                                                      
! ................. Reset the 2nd primary variable as mass fraction of dissolved CH4
!                                                                      
                    XX(2)      = X_mA - ZERO              
                    DX(NLOC+2) = XX(2) - X(NLOC+2)
! 
! ................. Call the single-phase (H2O) routine 
! 
                    CALL PHS1_Aqu_Eql(N,NLOC,NMAT)
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Aqueous + Hydrate (2-phase)
! .............. to Aqueous + Ice + Hydrate (3-phase)      
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
                 ELSE IF(PhTran == 'AqH->AIH') THEN
! 
                    IF(MOP(5) >= 3) WRITE(6,6002) elem(n)%name,TX
                    IF(KC == 0) THEN
                       WRITE(6,6101) 
                       STOP                                             
                    END IF
!                                                                      
! ................. Adjust the 1st primary variable as pressure
!                                                                      
                    XX(1)      = PX*(1.0d0 + ZERO)        
                    DX(NLOC+1) = XX(1) - X(NLOC+1)
!                                                                      
! ................. Reset the 2nd primary variable as aqueous phase saturation
!                                                                      
                    XX(2)      = S_aqu - ZERO              
                    DX(NLOC+2) = XX(2) - X(NLOC+2)
! 
! ................. Reset the 3nd or 4th variable as ice saturation 
! 
                    IF(NK == 2) THEN
                       XX(3)      = ZERO   
                       DX(NLOC+3) = XX(3) - X(NLOC+3)
                    ELSE
                       XX(4)      = ZERO   
                       DX(NLOC+4) = XX(4) - X(NLOC+4)
                    END IF
! 
! ................. Call the 3-phase (H2O+Hydrate+Gas) routine 
! 
                    CALL PHS3_AIH_Eql(N,NLOC,NMAT)
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Aqueous + Hydrate (2-phase)
! .............. to Aqueous + Gas + Hydrate (3-phase)      
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
                 ELSE IF(PhTran == 'AqH->AGH') THEN
! 
                    IF(MOP(5) >= 3) WRITE(6,6003) elem(n)%name,&
     &                                            PX,Peq_Hydr
                    IF(KC == 0) THEN
                       WRITE(6,6101)                                              
                       STOP                                             
                    END IF
!                                                                      
! ................. Reset the 1st primary variable as gas saturation
!                                                                      
                    XX(1)      = ZERO 
                    DX(NLOC+1) = XX(1) - X(NLOC+1)
! 
! ................. Call the 3-phase (H2O+Hydrate+Gas) routine 
! 
                    CALL PHS3_AGH_Eql(N,NLOC,NMAT)
! 
                 ELSE 
! 
                    WRITE(6,6102)
                    STOP
! 
                 END IF IF_PhTran
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'PHS2_AqH_Eql      1.0   29 September 2004',6X,   &
     &         'Routine for assigning the secondary ',   &
     &         'parameters of the 2-phase (Aqueous+Hydrate) ',   &
     &       /,T48,'system - Equilibrium hydrate reaction')
!
 6001 FORMAT(' !!!!!! "PHS2_AqH_Eql": Hydrate disappears at element "',&
     &       A8,'" ==> S_hydr = ',1pE15.8)
 6002 FORMAT(' !!!!!! "PHS2_AqH_Eql": Ice evolves at element "',A8,&
     &       '" ==> TX = ',1pE15.8)
 6003 FORMAT(' !!!!!! "PHS2_AqH_Eql": Gas phase evolves at element "',&
     &       A8,'" ==> Px  = ',1pE15.8,'  Peq = ',1pE15.8)
! 
 6101 FORMAT(' !!!!!!!!!!   Routine PHS2_AqH_Eql: ',   &
     &       'ERRONEOUS DATA INITIALIZATION   ',   &
     &       '==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
 6102 FORMAT(' !!!!!!!!!!   Routine PHS2_AqH_Eql: ',&   
     &       'ERRONEOUS PHASE CHANGE   ==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of PHS2_AqH_Eql
! 
! 
      RETURN
!
      END SUBROUTINE PHS2_AqH_Eql
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE PHS2_AqG_Eql(N,NLOC,NMAT)
! 
! ... Modules to be used 
! 
      USE Universal_Param
      USE Basic_Param
      USE GenControl_Param
      USE Diffusion_Param
!
      USE H2O_Param
      USE Hydrate_Param
!
      USE Variable_Arrays
      USE Element_Arrays
      USE PFMedProp_Arrays
!
      USE H2O_Properties
      USE Hydrate_Properties
      USE RealGas_Properties
!
	  USE MPI_PARAM
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*       ROUTINE FOR COMPUTING AND ASSIGNING ALL THE SECONDARY         *
!*    VARIABLES IN TWO-PHASE (H2O+Gas) SYSTEMS - Extended T-scale      *
!*                                                                     *
!*                  EQUILIBRIUM HYDRATION REACTION                     *
!*                                                                     *
!*                 Version 1.0 - September 29, 2004                    *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!                                                                       
      IMPLICIT NONE
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(2) :: Y_inG
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: PX,S_aqu,S_gas,TX,PS,P_evol,dummy
      REAL(KIND = 8) :: ZLiq,ZGas,D_gas,rho_L,H_Sol
      REAL(KIND = 8) :: Hi_Gas,Ui_Gas,Hd_Gas,Ud_Gas,Sd_Gas,H_Gas,U_Gas
! 
      REAL(KIND = 8) :: DW,UW,HW,k_rel_aqu,k_rel_gas,P_cap
      REAL(KIND = 8) :: V_gas,D_aqu,V_aqu,T_k
! 
      REAL(KIND = 8) :: Xmol_mG,X_mG,X_wG
      REAL(KIND = 8) :: Xmol_mA,X_mA,X_wA
      REAL(KIND = 8) :: Peq_Hydr,P_CH4,HK_GinH2O,H_Heat
      REAL(KIND = 8) :: T_dev,X_iA,Xmol_iA,Xmol_mA_old,Xmol_iA_old
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN)  :: N,NLOC,NMAT
! 
      INTEGER :: K,ixx
! 
      INTEGER :: HPeq_Paramtrs,PCH4_Paramtrs  ! Integer functions
! 
! -------
! ... Character variables     
! -------
! 
      CHARACTER(LEN = 8) :: PhTran 
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of PHS2_AqG_Eql
!
!
      IF(First_call) THEN
         First_call = .FALSE.
         IF(MPI_RANK==0) WRITE(11,6000)
      END IF
! 
! ... Some initializations 
! 
      PhTran = '        '
!
      Xmol_mG = 0.0d0
      P_CH4   = 0.0d0
! 
! -------
! ... Initialization of T-depression variables        
! -------
! 
      T_dev    = Cell_V(n,1)%p_AddD(1)  
      Xmol_mA  = 0.0d0
      Xmol_iA  = 0.0d0
! 
      Xmol_mA_old  = 0.0d0
      Xmol_iA_old  = 0.0d0
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>      Main loop determining conditions and assigning properties      >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_NumEqs: DO K=1,NEQ1
!
! ----------
! ...... Initial assignments of primary variable values        
! ...... Primary variables: Px, S_aqu, Tx        
! ----------
! 
         PX    = XX(1)                                                      
         S_aqu = XX(2)
! 
         IF(NK == 2) THEN
            X_iA = 0.0d0
            TX   = XX(3)
         ELSE IF(NK == 3) THEN
            X_iA = XX(3)
            TX   = XX(4)
         END IF
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the first iteration - Unincremented variables
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         IF_1stEq: IF(K == 1) THEN
! 
! ----------------
! ......... The gas saturation     
! ----------------
! 
            S_gas = 1.0d0 - S_aqu
! 
! -------------
! ......... Constraint on inhibitor mass fraction in the aqueous phase        
! -------------
! 
            IF_SaltMassF: IF (X_iA < 0.0d0) THEN
               X_iA       =   0.0d0
               XX(3)      =   0.0d0
               DX(NLOC+3) = - X(NLOC+3)
            END IF IF_SaltMassF
! 
! -------------
! ......... Determine PSAT: the H2O saturation pressure at TX                                                                       
! -------------
!                                                                       
            IGOOD = P_Sat_H2O_S(TX,PS)
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
! 
! -------------
! ......... Actual CH4 partial pressure       
! -------------
! 
            P_CH4 = PX - PS                                          
!
!***********************************************************************
!*                                                                     *
!*         Determine iteratively the mole fractions at Peq_Hyd         *
!*                                                                     *
!***********************************************************************
!
            IGOOD = HPeq_Paramtrs(2,TX,X_iA,.FALSE.,   &                     ! In only
     &                            Xmol_iA,Xmol_mA_old,Xmol_iA_old,T_dev,  &  ! In and out
     &                            Peq_Hydr,HK_GinH2O,Xmol_mA,X_mA)          ! Out only
! 
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
!
!***********************************************************************
!*                                                                     *
!*                    Check phase transition from                      *
!*            2-phase (Aqueous + Gas) to 1-phase (Aqueous)             *
!*                                                                     *
!***********************************************************************
!
            IF_IceEvol: IF(TX < 1.0d-2) THEN
! 
               PhTran = 'AqG->AIG'
               GO TO 1000
! 
            END IF IF_IceEvol
!
!***********************************************************************
!*                                                                     *
!*                    Check phase transition from                      *
!*            2-phase (Aqueous + Gas) to 1-phase (Aqueous)             *
!*                                                                     *
!***********************************************************************
!
            IF_GasDis: IF(S_aqu > 1.0d0) THEN
! 
               PhTran = 'AqG->Aqu'
               GO TO 1000
! 
            END IF IF_GasDis
!
!***********************************************************************
!*                                                                     *
!*                    Check phase transition from                      *
!*              2-phase (Aqueous + Gas) to 1-phase (Gas)               *
!*                                                                     *
!***********************************************************************
!
            IF_AquDis: IF(S_aqu < 0.0d0) THEN
! 
               PhTran = 'AqG->Gas'
               GO TO 1000
! 
            END IF IF_AquDis
!
!***********************************************************************
!*                                                                     *
!*                    Check phase transition from                      *
!*    2-phase (Aqueous + Gas) to 3-phase (Aqueous + Hydrate + Gas)     *
!*                                                                     *
!***********************************************************************
!
! ......... Determine the CH4 partial pressure switching criterion      
! 
            P_evol = Peq_Hydr - PS                                      
! 
! ......... Hydrate evolution  
! 
            IF_OrgEv: IF(P_CH4 > 1.000001d0*P_evol) THEN                                   
! 
               PhTran = 'AqG->AGH'
               GO TO 1000
! 
            ELSE
               GO TO 500 
            END IF IF_OrgEv
! 
         END IF IF_1stEq
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the remaining iterations - Incremented variables
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         CASE_Incr: SELECT CASE(K)
! 
! ...... Incrementing pressure 
! 
         CASE(2)                    
! 
            DELX(NLOC+1) = DFAC*XX(1)
            PX           = XX(1) + DELX(NLOC+1)
! 
! ...... Incrementing mass fraction of dissolved CH4 in H2O 
! 
         CASE(3)                     
! 
            DELX(NLOC+2) = DFAC
            S_aqu        = XX(2) + DELX(NLOC+2)
! 
! ...... Incrementing temperature (NK = 2) or salt mass fraction (NK = 3) 
! 
         CASE(4) 
! 
            IF(NK == 2) THEN
               X_iA         = 0.0d0
               DELX(NLOC+3) = DFAC*(273.15d0 + XX(3))
               TX           = XX(3) + DELX(NLOC+3)
            ELSE IF(NK == 3) THEN
               DELX(NLOC+3) = DFAC
               X_iA         = XX(3) + DELX(NLOC+3)
            END IF
! 
! ...... Incrementing temperature (NK = 3)
! 
         CASE(5) 
! 
            DELX(NLOC+4) = DFAC*(273.15d0 + XX(4))
            TX           = XX(4) + DELX(NLOC+4)
! 
         END SELECT CASE_Incr
!
!***********************************************************************
!*                                                                     *
!*         For augmented primary variables:                            *
!*             Determine the relevant parameters                       *
!*                                                                     *
!***********************************************************************
! 
         IF_GT1: IF(K > 1) THEN
! 
! ......... The gas saturation     
! 
            S_gas = 1.0d0 - S_aqu
! 
! ......... Determine the corresponding saturation pressure of H2O      
! 
            IGOOD = P_Sat_H2O_S(TX,PS)
! 
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
!                                                                       
!.......... Determine the CH4 partial pressure
!                                                                       
            P_CH4 = PX - PS
! 
         END IF IF_GT1
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... Compute thermophysical properties 
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
  500    T_k = TX + 273.15d0  ! Temperature in K                                                    
! 
! -------------
! ...... The gas saturation     
! -------------
! 
         S_gas = 1.0d0 - S_aqu
! 
! -------------
! ...... Determine iteratively the mole fractions in the aqueous phase      
! -------------
! 
         IGOOD = PCH4_Paramtrs(1,P_CH4,TX,X_iA,   &                         ! In only
     &                           Xmol_iA,Xmol_mA_old,Xmol_iA_old,T_dev, &   ! In and out 
     &                           HK_GinH2O,X_mA,Xmol_mA)                   ! Out only
! 
         IF(IGOOD /= 0) THEN
            CALL WARN_RF(elem(n)%name,XX,NK1,KC)
            RETURN
         END IF                                       
! 
! -------------
! ...... Mole fractions in the gas phase      
! -------------
! 
         Xmol_mG  = P_CH4/PX         ! Mole fraction of CH4 in the gas phase
! 
         Y_inG(1) = Xmol_mG          ! CH4
         Y_inG(2) = 1.0d0-Y_inG(1)   ! H2O
!                                                                      
! ...... Compute mass fractions in the gas phase
!                                                                      
         X_mG = MW_CH4*Y_inG(1)/( MW_CH4*Y_inG(1) + MW_H2O*Y_inG(2) )                                                  
!                                                                      
         X_wG = 1.0d0-X_mG                                                
!
!***********************************************************************
!*                                                                     *
!*             P R O P E R T I E S  +  P A R A M E T E R S             *
!*                                                                     *
!***********************************************************************
!
! ...... Determine the real gas density and compressibility
!                                                                      
         CALL Zcomp(PX,T_k,0.0d0,Y_inG,ZLiq,ZGas,D_gas,rho_L,ixx)
!                                                                      
! ...... Obtain the specific internal energy and enthalpy of CH4
!                                                                      
         CALL HU_IdealGas(0,2.7315d2,T_k,Hi_Gas,Ui_Gas)    ! Ideal enthalpy
         CALL HUS_Departure(T_k,ZGas,Hd_Gas,Ud_Gas,Sd_Gas) ! Departure enthalpy
!                                                                      
         H_gas = ( Hi_Gas - Hd_Gas ) &                      ! Convert to J/Kg/K from J/kgmol/K
     &          /( Y_inG(1)*MW_CH4 + Y_inG(2)*MW_H2O )  
!                                                                      
         U_gas = ( Ui_Gas - Ud_Gas ) &                     ! Convert to J/Kg/K from J/kgmol/K
     &          /( Y_inG(1)*MW_CH4 + Y_inG(2)*MW_H2O )  
!                                                                      
! ...... Compute the heat of CH4 solution
!                                                                      
         H_Sol = HEAT_GasSolution(1,TX,Xmol_iA,HK_GinH2O,dummy)                                     
!                                                                      
! ...... Compute H2O mass fraction in the aqueous phase
!                                                                      
         X_wA = 1.0d0-X_mA-X_iA                     
!                                                                      
! ...... Obtain the viscosity, density and internal energy of liquid H2O 
!                                                                      
         CALL UHRho_LiqH2O(TX,PX,DW,UW,HW,IGOOD)                                       
!                                                                      
! ...... Compute the viscosity of the multicomponent gas mixture
!                                                                      
         V_gas = Gas_Viscosity(PX,T_k,ZGas)                          
!                                                                      
! ...... Aqueous phase density and viscosity                                                                       
!                                                                      
         D_aqu = X_wA*DW+X_iA*D_Inhib+X_mA*D_gas
         V_aqu = ViscLV_H2Ox(TX,D_aqu)  
!                                                                      
! ----------
! ...... Obtain capillary pressures and relative permeabilities
! ----------
!                                                                      
         CALL CapPres_Hyd(TX,S_aqu,S_gas,D_aqu,P_cap,NMAT)          
         CALL RelPerm_Hyd(S_aqu,S_gas,k_rel_aqu,k_rel_gas,dummy,NMAT)         
! ----------
! ...... Compute the heat of hydrate dissociation/formation 
! ----------
! 
         H_Heat = HeatDiss_HydrCH4(T_k,&
     &                             F_EqOption,  & ! H-equation
     &                             IGOOD)
! 
! ----------
! ...... Assign value to state index  
! ----------
! 
         IF(k == 1) elem(n)%StateIndex = 3_1
! 
! ----------
! ...... Assign values of secondary parameters - Gas phase  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(1)   = S_gas
         Cell_V(n,k-1)%p_KRel(1)   = k_rel_gas
         Cell_V(n,k-1)%p_Visc(1)   = V_gas
         Cell_V(n,k-1)%p_Dens(1)   = D_gas 
         Cell_V(n,k-1)%p_Enth(1)   = H_Gas                 
         Cell_V(n,k-1)%p_PCap(1)   = 0.0d0
         Cell_V(n,k-1)%p_MasF(1,1) = X_wG
         Cell_V(n,k-1)%p_MasF(2,1) = X_mG
         Cell_V(n,k-1)%p_MasF(3,1) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Aqueous phase 
! ----------
! 
         Cell_V(n,k-1)%p_Satr(2)   = S_aqu
         Cell_V(n,k-1)%p_KRel(2)   = k_rel_aqu
         Cell_V(n,k-1)%p_Visc(2)   = V_aqu
         Cell_V(n,k-1)%p_Dens(2)   = D_aqu
         Cell_V(n,k-1)%p_Enth(2)   =  X_wA*HW &
     &                              + X_mA*(H_Gas + H_Sol) &
     &                              + X_iA*H_InhSol
         Cell_V(n,k-1)%p_PCap(2)   = P_cap
         Cell_V(n,k-1)%p_MasF(1,2) = X_wA
         Cell_V(n,k-1)%p_MasF(2,2) = X_mA
         Cell_V(n,k-1)%p_MasF(3,2) = X_iA
! 
! ----------
! ...... Assign values of secondary parameters - Hydrate phase  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(3)   = 0.0d0
         Cell_V(n,k-1)%p_Dens(3)   = 0.0d0
         Cell_V(n,k-1)%p_Enth(3)   = 0.0d0
         Cell_V(n,k-1)%p_MasF(1,3) = 0.0d0
         Cell_V(n,k-1)%p_MasF(2,3) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,3) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Ice phase 
! ----------
! 
         Cell_V(n,k-1)%p_Satr(4)   = 0.0d0
         Cell_V(n,k-1)%p_Dens(4)   = 0.0d0
         Cell_V(n,k-1)%p_Enth(4)   = 0.0d0
         Cell_V(n,k-1)%p_MasF(1,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(2,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,4) = 0.0d0
! 
! ----------
! ...... Store the temperature 
! ----------
! 
         Cell_V(n,k-1)%TemC = TX
! 
! ----------
! ...... Store other important variables  
! ----------
! 
         IF(K == 1) THEN
! 
! ......... Store equilibrium hydration pressure at TX 
! 
            Cell_V(n,0)%p_AddD(1) = Peq_Hydr
! 
! ......... Store the hydrate temperature depression  
! 
            Cell_V(n,1)%p_AddD(1) = T_dev
! 
         END IF
! 
! ----------
! ...... Store heat of dissociation/formation 
! ----------
! 
         Cell_V(n,k-1)%p_AddD(3) = H_Heat
! 
! 
! 
      END DO DO_NumEqs
! 
! 
! 
      RETURN     ! Task completed - Exit the routine
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                         PHASE TRANSITIONS                           >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
 1000 CONTINUE
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Aqueous + Gas (2-phase)
! .............. to Aqueous (1-phase)    
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
      IF_PhTran: IF(PhTran == 'AqG->AIG') THEN
!                                                                       
                    IF(MOP(5) >= 3) WRITE(6,6001) elem(n)%name,TX
                    IF(KC == 0) THEN
                       WRITE(6,6101) 
                       STOP                                             
                    END IF
! 
! ................. Adjust 2nd variable as the aqueus phase saturation 
! 
                    XX(2)      = S_aqu - 1.0d-4
                    DX(NLOC+2) = XX(2) - X(NLOC+2)
! 
! ................. Reset 3rd or 4th variable as the gas saturation
! 
                    IF(NK == 2) THEN
                       XX(3)      = S_gas
                       DX(NLOC+3) = XX(3) - X(NLOC+3)
                    ELSE IF(NK == 3) THEN
                       XX(4)      = S_gas
                       DX(NLOC+4) = XX(4) - X(NLOC+4)
                    END IF
! 
! ................. Call the single-phase (H2O) subroutine 
! 
                    CALL PHS3_AIG_Eql(N,NLOC,NMAT)
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Aqueous + Gas (2-phase)
! .............. to Aqueous (1-phase)    
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
                 ELSE IF(PhTran == 'AqG->Aqu') THEN
!                                                                       
                    IF(MOP(5) >= 3) WRITE(6,6002) elem(n)%name,S_aqu
                    IF(KC == 0) THEN
                       WRITE(6,6101) 
                       STOP                                             
                    END IF
! 
! ................. Reset 2nd variable as CH4 mass fraction in water 
! 
                    XX(2)      = X_mA
                    DX(NLOC+2) = XX(2) - X(NLOC+2)
! 
! ................. Call the single-phase (H2O) subroutine 
! 
                    CALL PHS1_Aqu_Eql(N,NLOC,NMAT)
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Aqueous + Gas (2-phase)
! .............. to Gas (1-phase)    
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
                 ELSE IF(PhTran == 'AqG->Gas') THEN
!                                                                       
                    IF(MOP(5) >= 3) WRITE(6,6004) elem(n)%name,S_aqu
                    IF(KC == 0) THEN
                       WRITE(6,6101) 
                       STOP                                             
                    END IF
! 
! ................. Reset 2nd variable as CH4 mole fraction in the gas phase
! 
                    XX(2)      = P_CH4/PX
                    DX(NLOC+2) = XX(2) - X(NLOC+2)
! 
! ................. Call the single-phase (H2O) subroutine 
! 
                    CALL PHS1_Gas_Eql(N,NLOC,NMAT)
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Aqueous + Gas (2-phase)
! .............. to Aqueous + Gas + Hydrate (3-phase)    
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
                 ELSE IF(PhTran == 'AqG->AGH') THEN
! 
                    IF(MOP(5) >= 3) WRITE(6,6003) elem(n)%name,&
     &                                            P_CH4,P_evol
                    IF(KC == 0) THEN
                       WRITE(6,6101)                                              
                       STOP                                             
                    END IF
! 
! ................. Reset the 1st primary variable as the gas phase saturation
! 
                    XX(1)      = S_gas*(1.0d0 - ZERO)                                       
                    DX(NLOC+1) = XX(1) - X(NLOC+1)                                    
! 
! ................. Call the 3-phase (H2O+Hydrate+Gas) routine 
! 
                    CALL PHS3_AGH_Eql(N,NLOC,NMAT)
! 
                 ELSE 
! 
                    WRITE(6,6102)
                    STOP
! 
                 END IF IF_PhTran
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'PHS2_AqG_Eql      1.0   29 September 2004',6X,  & 
     &         'Routine for assigning the secondary parameters of ',  &
     &         'the 2-phase (Aqueous+Gas) ',   &
     &       /,T48,'system - Equilibrium hydrate reaction')

!
 6001 FORMAT(' !!!!!! "PHS2_AqG_Eql": Ice phase disappears at ',   &
     &       'element "',A8,'" ==> TX = ',1pE15.8,' oC')
 6002 FORMAT(' !!!!!! "PHS2_AqG_Eql": Gas phase disappears at ',   &
     &       'element "',A8,'" ==> S_aqu = ',1pE15.8)
 6003 FORMAT(' !!!!!! "PHS2_AqG_Eql": Hydrate evolves at ',        &
     &       'element "',A8,'" ==> P_CH4 = ',1pE15.8,  &
     &       '  P_evol = ',1pE15.8) 
 6004 FORMAT(' !!!!!! "PHS2_AqG_Eql": Aqueous phase disappears at ',  &
     &       'element "',A8,'" ==> S_aqu = ',1pE15.8)
! 
 6101 FORMAT(' !!!!!!!!!!   Routine PHS2_AqG_Eql: ',&
     &       'ERRONEOUS DATA INITIALIZATION   ',  &
     &       '==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
 6102 FORMAT(' !!!!!!!!!!   Routine PHS2_AqG_Eql: ',   &
     &       'ERRONEOUS PHASE CHANGE   ==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of PHS2_AqG_Eql
! 
! 
      RETURN
!
      END SUBROUTINE PHS2_AqG_Eql
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE PHS2_IcG_Eql(N,NLOC,NMAT)
! 
! ... Modules to be used 
! 
      USE Universal_Param
      USE Basic_Param
      USE GenControl_Param
      USE Diffusion_Param
!
      USE H2O_Param
      USE Hydrate_Param
!
      USE Variable_Arrays
      USE Element_Arrays
      USE PFMedProp_Arrays
!
      USE H2O_Properties
      USE Hydrate_Properties
      USE RealGas_Properties
	  USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*       ROUTINE FOR COMPUTING AND ASSIGNING ALL THE SECONDARY         *
!*    VARIABLES IN TWO-PHASE (H2O+Gas) SYSTEMS - Extended T-scale      *
!*                                                                     *
!*                  EQUILIBRIUM HYDRATION REACTION                     *
!*                                                                     *
!*                 Version 1.0 - September 29, 2004                    *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!                                                                       
      IMPLICIT NONE
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(2) :: Y_inG
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: PX,S_ice,S_gas,TX,PS
      REAL(KIND = 8) :: ZLiq,ZGas,D_gas,rho_L
      REAL(KIND = 8) :: Hi_Gas,Ui_Gas,Hd_Gas,Ud_Gas,Sd_Gas,H_Gas,U_Gas
! 
      REAL(KIND = 8) :: k_rel_aqu,k_rel_gas,P_cap,dummy
      REAL(KIND = 8) :: V_gas,D_ice,H_ice,T_k
! 
      REAL(KIND = 8) :: Xmol_mG,X_mG,X_wG,X_iA
      REAL(KIND = 8) :: Peq_Hydr,P_CH4,H_Heat
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN)  :: N,NLOC,NMAT
! 
      INTEGER :: K,ixx
! 
! -------
! ... Character variables     
! -------
! 
      CHARACTER(LEN = 8) :: PhTran 
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of PHS2_IcG_Eql
!
!
      IF(First_call) THEN
         First_call = .FALSE.
         IF(MPI_RANK==0) WRITE(11,6000)
      END IF
! 
! ... Some initializations 
! 
      PhTran = '        '
!
      Xmol_mG = 0.0d0
      P_CH4   = 0.0d0
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>      Main loop determining conditions and assigning properties      >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_NumEqs: DO K=1,NEQ1
!
! ----------
! ...... Initial assignments of primary variable values        
! ...... Primary variables: Px, S_ice, Tx        
! ----------
! 
         PX    = XX(1)
         S_ice = XX(2)
! 
         IF(NK == 2) THEN
            X_iA = 0.0d0
            TX   = XX(3)
         ELSE IF(NK == 3) THEN
            X_iA = 0.0d0
            TX   = XX(4)
         END IF
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the first iteration - Unincremented variables
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         IF_1stEq: IF(K == 1) THEN
!
! ......... Gas saturation
!
            S_gas = 1.0d0 - S_ice
! 
! -------------
! ......... Constraint on gas saturation        
! -------------
! 
            IF_SgLim: IF (S_gas <= 0.0d0) THEN
               S_gas      = 1.0000d-3
               XX(2)      = 1.0d0 - S_gas
               DX(NLOC+2) = XX(2) - X(NLOC+2)
            END IF IF_SgLim
! 
! -------------
! ......... Determine PSAT: the H2O saturation pressure at TX                                                                       
! -------------
!                                                                       
            IGOOD = P_Sublm_H2O_S(TX,PS)
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
! 
! -------------
! ......... CH4 partial pressure and mole fraction in the gas phase    
! -------------
! 
            P_CH4    = PX - PS                                          
            Xmol_mG  = P_CH4/PX         
! 
! -------------
! ......... Equilibrium hydration pressure at TX                                                                       
! -------------
!                                                                       
            Peq_Hydr = EqPres_HydrCH4((TX+273.15d0),0.0d0,&
     &                                 F_EqOption,IGOOD)
!
!***********************************************************************
!*                                                                     *
!*                    Check phase transition from                      *
!*                2-phase (Ice + Gas) to 1-phase (Gas)                 *
!*                                                                     *
!***********************************************************************
!
            IF_GasDis: IF(S_ice < 0.0d0) THEN
! 
               PhTran = 'IcG->Gas'
               GO TO 1000
! 
            END IF IF_GasDis
!
!***********************************************************************
!*                                                                     *
!*                    Check phase transition from                      *
!*        2-phase (Ice + Gas) to 3-phase (Ice + Gas + Hydrate)         *
!*                                                                     *
!***********************************************************************
!
! ......... Aqueous phase evolution  
! 
            IF_HydrEv: IF(PX > Peq_Hydr) THEN                                   
! 
               PhTran = 'IcG->IGH'
               GO TO 1000
! 
            END IF IF_HydrEv
!
!***********************************************************************
!*                                                                     *
!*                    Check phase transition from                      *
!*        2-phase (Ice + Gas) to 3-phase (Aqueous + Ice + Gas)         *
!*                                                                     *
!***********************************************************************
!
! ......... Aqueous phase evolution  
! 
            IF_AquEv: IF(TX > Tc_quad_hyd) THEN                                   
! 
               PhTran = 'IcG->AIG'
               GO TO 1000
! 
            ELSE
               GO TO 500 
            END IF IF_AquEv
! 
         END IF IF_1stEq
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the remaining iterations - Incremented variables
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         CASE_Incr: SELECT CASE(K)
! 
! ...... Incrementing pressure 
! 
         CASE(2)                    
! 
            DELX(NLOC+1) = DFAC*XX(1)
            PX           = XX(1) + DELX(NLOC+1)
! 
! ...... Incrementing ice saturation 
! 
         CASE(3)                     
! 
            DELX(NLOC+2) = DFAC
            S_ice        = XX(2) + DELX(NLOC+2)
! 
! ...... Incrementing temperature (NK = 2) or salt mass fraction (NK = 3) 
! 
         CASE(4) 
! 
            IF(NK == 2) THEN
               X_iA         = 0.0d0
               DELX(NLOC+3) =-DFAC*(273.15d0 + XX(3))
               TX           = XX(3) + DELX(NLOC+3)
            ELSE IF(NK == 3) THEN
               DELX(NLOC+3) = DFAC
               X_iA         = 0.0d0
            END IF
! 
! ...... Incrementing temperature (NK = 3)
! 
         CASE(5) 
! 
            DELX(NLOC+4) =-DFAC*(273.15d0 + XX(4))
            TX           = XX(4) + DELX(NLOC+4)
! 
         END SELECT CASE_Incr
!
!***********************************************************************
!*                                                                     *
!*         For augmented primary variables:                            *
!*             Determine the relevant parameters                       *
!*                                                                     *
!***********************************************************************
! 
         IF_GT1: IF(K > 1) THEN
! 
! ......... Determine the corresponding sublimation pressure of H2O      
! 
            IGOOD = P_Sublm_H2O_S(TX,PS)
! 
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
!                                                                       
!.......... Determine the CH4 partial pressure and corresponding mole fraction
!                                                                       
            P_CH4    = PX - PS
            Xmol_mG  = P_CH4/PX         
!                                                                       
!.......... Augmented gas saturation
!                                                                       
            S_gas = 1.0d0 - S_ice
! 
         END IF IF_GT1
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... Compute thermophysical properties 
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
  500    T_k = TX + 273.15d0  ! Temperature in K                                                    
! 
! -------------
! ...... Mole fractions in the gas phase      
! -------------
! 
         Y_inG(1) = Xmol_mG          ! CH4
         Y_inG(2) = 1.0d0-Y_inG(1)   ! H2O
!                                                                      
! ...... Compute mass fractions in the gas phase
!                                                                      
         X_mG = MW_CH4*Y_inG(1)/( MW_CH4*Y_inG(1) + MW_H2O*Y_inG(2) )                                                  
!                                                                      
         X_wG = 1.0d0-X_mG                                                
!
!***********************************************************************
!*                                                                     *
!*             P R O P E R T I E S  +  P A R A M E T E R S             *
!*                                                                     *
!***********************************************************************
!
! ...... Determine the real gas density and compressibility
!                                                                      
         CALL Zcomp(PX,T_k,0.0d0,Y_inG,ZLiq,ZGas,D_gas,rho_L,ixx)
!                                                                      
! ----------
! ...... Obtain the specific internal energy and enthalpy of CH4
! ----------
!                                                                      
         CALL HU_IdealGas(0,2.7315d2,T_k,Hi_Gas,Ui_Gas)    ! Ideal enthalpy
         CALL HUS_Departure(T_k,ZGas,Hd_Gas,Ud_Gas,Sd_Gas) ! Departure enthalpy
!                                                                      
         H_gas = ( Hi_Gas - Hd_Gas )  &                   ! Convert to J/Kg/K from J/kgmol/K
     &          /( Y_inG(1)*MW_CH4 + Y_inG(2)*MW_H2O )  
!                                                                      
         U_gas = ( Ui_Gas - Ud_Gas )   &                  ! Convert to J/Kg/K from J/kgmol/K
     &          /( Y_inG(1)*MW_CH4 + Y_inG(2)*MW_H2O )  
!                                                                      
! ----------
! ...... Obtain capillary pressures and relative permeabilities
! ----------
!                                                                      
         CALL CapPres_Hyd(TX,1.0d-6,S_gas,1.0d3,P_cap,NMAT)          
         CALL RelPerm_Hyd(1.0d-6,S_gas,k_rel_aqu,k_rel_gas,dummy,NMAT)         
!                                                                      
! ----------
! ...... Compute the viscosity of the multicomponent gas mixture
! ----------
!                                                                      
         V_gas = Gas_Viscosity(PX,T_k,ZGas)                          
!
! ----------
! ...... Determine the ice density and enthalpy
! ----------
!                                                                      
         D_ice = Ice_Density(TX,PX)
         H_ice = Ice_Enthalpy(PX,TX)
! 
! ----------
! ...... Compute the heat of hydrate dissociation/formation 
! ----------
! 
         H_Heat = 0.0d0   
! 
! ----------
! ...... Assign value to state index  
! ----------
! 
         IF(k == 1) elem(n)%StateIndex = 4_1
! 
! ----------
! ...... Assign values of secondary parameters - Gas phase  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(1)   = S_gas
         Cell_V(n,k-1)%p_KRel(1)   = k_rel_gas
         Cell_V(n,k-1)%p_Visc(1)   = V_gas
         Cell_V(n,k-1)%p_Dens(1)   = D_gas 
         Cell_V(n,k-1)%p_Enth(1)   = H_Gas                 
         Cell_V(n,k-1)%p_PCap(1)   = 0.0d0
         Cell_V(n,k-1)%p_MasF(1,1) = X_wG
         Cell_V(n,k-1)%p_MasF(2,1) = X_mG
         Cell_V(n,k-1)%p_MasF(3,1) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Aqueous phase 
! ----------
! 
         Cell_V(n,k-1)%p_Satr(2)   = 0.0d0
         Cell_V(n,k-1)%p_KRel(2)   = 0.0d0
         Cell_V(n,k-1)%p_Visc(2)   = 1.0d0
         Cell_V(n,k-1)%p_Dens(2)   = 0.0d0
         Cell_V(n,k-1)%p_Enth(2)   = 0.0d0
         Cell_V(n,k-1)%p_PCap(2)   = P_cap
         Cell_V(n,k-1)%p_MasF(1,2) = 0.0d0
         Cell_V(n,k-1)%p_MasF(2,2) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,2) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Hydrate phase  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(3)   = 0.0d0
         Cell_V(n,k-1)%p_Dens(3)   = 0.0d0
         Cell_V(n,k-1)%p_Enth(3)   = 0.0d0
         Cell_V(n,k-1)%p_MasF(1,3) = 0.0d0
         Cell_V(n,k-1)%p_MasF(2,3) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,3) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Ice phase 
! ----------
! 
         Cell_V(n,k-1)%p_Satr(4)   = S_ice
         Cell_V(n,k-1)%p_Dens(4)   = D_ice
         Cell_V(n,k-1)%p_Enth(4)   = H_ice
         Cell_V(n,k-1)%p_MasF(1,4) = 1.0d0
         Cell_V(n,k-1)%p_MasF(2,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,4) = 0.0d0
! 
! ----------
! ...... Store the temperature 
! ----------
! 
         Cell_V(n,k-1)%TemC = TX
! 
! ----------
! ...... Store other important variables  
! ----------
! 
         IF(K == 1) THEN
! 
! ......... Store equilibrium hydration pressure at TX 
! 
            Cell_V(n,0)%p_AddD(1) = Peq_Hydr
! 
! ......... Store the hydrate temperature depression  
! 
            Cell_V(n,1)%p_AddD(1) = 0.0d0
! 
         END IF
! 
! ----------
! ...... Store heat of dissociation/formation 
! ----------
! 
         Cell_V(n,k-1)%p_AddD(3) = H_Heat
! 
! 
! 
      END DO DO_NumEqs
! 
! 
! 
      RETURN     ! Task completed - Exit the routine
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                         PHASE TRANSITIONS                           >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
 1000 CONTINUE
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Aqueous + Gas (2-phase)
! .............. to Aqueous (1-phase)    
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
      IF_PhTran: IF(PhTran == 'IcG->Gas') THEN
!                                                                       
                    IF(MOP(5) >= 3) WRITE(6,6002) elem(n)%name,S_ice
                    IF(KC == 0) THEN
                       WRITE(6,6101) 
                       STOP                                             
                    END IF
! 
! ................. Reset 2nd variable as CH4 mole fraction in the gas phase
! 
                    XX(2)      = Xmol_mG - ZERO   
                    DX(NLOC+2) = XX(2) - X(NLOC+2)
! 
! ................. Call the single-phase (H2O) subroutine 
! 
                    CALL PHS1_Gas_Eql(N,NLOC,NMAT)
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Aqueous + Gas (2-phase)
! .............. to Aqueous + Gas + Hydrate (3-phase)    
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
                 ELSE IF(PhTran == 'IcG->AIG') THEN
! 
                    IF(MOP(5) >= 3) WRITE(6,6003) elem(n)%name,TX
                    IF(KC == 0) THEN
                       WRITE(6,6101)                                              
                       STOP                                             
                    END IF
! 
! ................. Reset the 2nd primary variable as the aqueous phase
! 
                    XX(2)      = ZERO   ! 1.0d-3                                       
                    DX(NLOC+2) = XX(2) - X(NLOC+2)                                    
! 
! ................. Call the 3-phase (H2O+Hydrate+Gas) routine 
! 
                    CALL PHS3_AIG_Eql(N,NLOC,NMAT)
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Aqueous + Gas (2-phase)
! .............. to Ice + Gas + Hydrate (3-phase)    
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
                 ELSE IF(PhTran == 'IcG->IGH') THEN
! 
                    IF(MOP(5) >= 3) WRITE(6,6004) elem(n)%name,&
     &                                            PX,Peq_Hydr
                    IF(KC == 0) THEN
                       WRITE(6,6101)                                              
                       STOP                                             
                    END IF
! 
! ................. Reset the 1st primary variable as the gas phase
! 
                    XX(1)      = S_gas - ZERO   ! 1.0d-3                                    
                    DX(NLOC+1) = XX(1) - X(NLOC+1)                                    
! 
! ................. Reset the 2nd primary variable as the ice phase
! 
                    XX(2)      = S_ice - ZERO   ! 1.0d-4                                    
                    DX(NLOC+2) = XX(2) - X(NLOC+2)                                    
! 
! ................. Call the 3-phase (H2O+Hydrate+Gas) routine 
! 
                    CALL PHS3_IGH_Eql(N,NLOC,NMAT)
! 
                 ELSE 
! 
                    WRITE(6,6102)
                    STOP
! 
                 END IF IF_PhTran
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'PHS2_IcG_Eql      1.0   25 January   2005',6X,   &
     &         'Routine for assigning the secondary parameters',&
     &         ' of the 2-phase (Ice+Gas) ',  &
     &       /,T48,'system - Equilibrium hydrate reaction')

!
 6002 FORMAT(' !!!!!! "PHS2_IcG_Eql": Ice phase disappears at ',&
     &       'element "',A8,'" ==> S_ice = ',1pE15.8)
 6003 FORMAT(' !!!!!! "PHS2_IcG_Eql": Aqueous phase evolves at ',&
     &       'element "',A8,'" ==> TX = ',1pE15.8) 
 6004 FORMAT(' !!!!!! "PHS2_IcG_Eql": Hydrate evolves at element "',&
     &        A8,'" ==> P = ',1pE15.8,  &
     &       ' Pa,   Peq =',1pE15.8,' Pa') 
! 
 6101 FORMAT(' !!!!!!!!!!   Routine PHS2_IcG_Eql: ERRONEOUS ',   &
     &       'DATA INITIALIZATION   ==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
 6102 FORMAT(' !!!!!!!!!!   Routine PHS2_IcG_Eql: ERRONEOUS ',   &
     &       'PHASE CHANGE   ==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of PHS2_IcG_Eql
! 
! 
      RETURN
!
      END SUBROUTINE PHS2_IcG_Eql
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE PHS2_IcH_Eql(N,NLOC,NMAT)
! 
! ... Modules to be used 
! 
      USE Universal_Param
      USE Basic_Param
      USE GenControl_Param
      USE Diffusion_Param
!
      USE H2O_Param
      USE Hydrate_Param
!
      USE Variable_Arrays
      USE Element_Arrays
      USE PFMedProp_Arrays
!
      USE H2O_Properties
      USE Hydrate_Properties
      USE RealGas_Properties
	  USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*        ROUTINE FOR COMPUTING AND ASSIGNING ALL THE SECONDARY        *
!*             VARIABLES AT THE Hydrate+Ice 2-PHASE SYSTEM             *
!*                                                                     *
!*                  EQUILIBRIUM HYDRATION REACTION                     *
!*                                                                     *
!*                  Version 1.0 - January 24, 2005                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!                                                                       
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: PX,TX,P_frz,T_k
      REAL(KIND = 8) :: D_Hydr,H_Hydr
      REAL(KIND = 8) :: S_ice,D_ice,H_ice
! 
      REAL(KIND = 8) :: X_iA,T_dev,H_Heat,Peq_Hydr
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN)  :: N,NLOC,NMAT
! 
      INTEGER :: k
! 
! -------
! ... Character variables     
! -------
! 
      CHARACTER(LEN = 8) :: PhTran 
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of PHS2_IcH_Eql
!
!
      IF(First_call) THEN
         First_call = .FALSE.
         IF(MPI_RANK==0) WRITE(11,6000)
      END IF
! 
! ... Some initializations 
! 
      PhTran = '        '
! 
! -------
! ... Initialization of T-depression variables        
! -------
! 
      T_dev = 0.0d0  
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>      Main loop determining conditions and assigning properties      >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_NumEqs: DO K=1,NEQ1
!
! ----------
! ...... Initial assignments of primary variable values        
! ...... Primary variables: PX, S_ice, T        
! ----------
! 
         PX    = XX(1)                                                      
         S_ice = XX(2)                                     
! 
         IF(NK == 2) THEN
            X_iA = 0.0d0
            TX   = XX(3)
         ELSE IF(NK == 3) THEN
            X_iA = XX(3)
            TX   = XX(4)
         END IF
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the first iteration - Unincremented variables
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         IF_1stEq: IF(K == 1) THEN
! 
! -------------
! ......... Constraints on ice saturation        
! -------------
! 
            IF_SiULim: IF (S_ice >= 1.0d0) THEN
               S_ice      = 9.999d-1
               XX(2)      = S_ice
               DX(NLOC+2) = XX(2) - X(NLOC+2)
            END IF IF_SiULim
! 
            IF_SiLLim: IF (S_ice <= 0.0d0) THEN
               S_ice      = 1.0d-4
               XX(2)      = S_ice
               DX(NLOC+2) = XX(2) - X(NLOC+2)
            END IF IF_SiLLim
! 
! -------------
! ......... Constraint on inhibitor mass fraction in the aqueous phase        
! -------------
! 
            IF_SaltMassF: IF (X_iA /= 0.0d0) THEN
               X_iA       =   0.0d0
               XX(3)      =   0.0d0
               DX(NLOC+3) = - X(NLOC+3)
            END IF IF_SaltMassF
!
! -------------
! ......... Determine the fusion pressure at the prevailing temperature
! -------------
!
            IF(TX < -23.0d0) THEN
               IGOOD = P_Fusion_H2O_S(-23.0d0,P_frz)
            ELSE
! 
               IGOOD = P_Fusion_H2O_S(TX,P_frz)
! 
               IF(IGOOD /= 0) THEN
                  P_frz = - 1.0d10
               ELSE
                  P_frz = P_frz + P_quad_hyd - P_H2O_3p
               END IF                                       
! 
            END IF
!
!***********************************************************************
!*                                                                     *
!*         Check phase transition from Hydrate + Ice (2-phase)         *
!*                 to Aqueous + Hydrate + Ice (3-phase)                *
!*                                                                     *
!***********************************************************************
!
            IF_IceDis: IF(PX > P_frz) THEN
! 
               PhTran = 'IcH->AIH'
               GO TO 1000
!
            END IF IF_IceDis
!
! -------------
! ......... Determine the equilibrium hydration pressure at the prevailing temperature  
! -------------
!
            Peq_Hydr = EqPres_HydrCH4((TX+273.15d0),0.0d0,&
     &                                 F_EqOption,IGOOD)
!
!***********************************************************************
!*                                                                     *
!*    Check phase transition from Aqueous + Hydrate + Ice (3-phase)    *
!*     to Aqueous + Hydrate + Ice + Gas (4-phase, Quadruple point)     *
!*                                                                     *
!***********************************************************************
!
            IF_GasEvolv: IF(PX < Peq_Hydr) THEN
! 
               PhTran = 'IcH->IGH'
               GO TO 1000
!
            ELSE
! 
               GO TO 500 
! 
            END IF IF_GasEvolv
! 
         END IF IF_1stEq
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the remaining iterations - Incremented variables
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         CASE_Incr: SELECT CASE(k)
! 
! ...... Incrementing gas phase saturation 
! 
         CASE(2)                    
! 
            DELX(NLOC+1) = DFAC*XX(1)
            PX           = XX(1) + DELX(NLOC+1)
! 
! ...... Incrementing aqueous phase saturation  
! 
         CASE(3)                     
! 
            DELX(NLOC+2) = DFAC
            S_ice        = XX(2) + DELX(NLOC+2)
! 
! ...... Incrementing ice saturation (NK = 2) or salt mass fraction (NK = 3) 
! 
         CASE(4) 
! 
            IF(NK == 2) THEN
               X_iA         = 0.0d0
               DELX(NLOC+3) =-DFAC*(273.15d0 + XX(3))
               TX           = XX(3) + DELX(NLOC+3)
            ELSE IF(NK == 3) THEN
               DELX(NLOC+3) = DFAC
               X_iA         = 0.0d0
            END IF
! 
! ...... Incrementing hydrate saturation (NK = 3)
! 
         CASE(5) 
! 
            DELX(NLOC+4) =-DFAC*(273.15d0 + XX(4))
            TX           = XX(4) + DELX(NLOC+4)
! 
         END SELECT CASE_Incr
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... Compute thermophysical properties 
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
  500    T_k = TX + 273.15d0   ! Initial temperature estimate  
!
!***********************************************************************
!*                                                                     *
!*             P R O P E R T I E S  +  P A R A M E T E R S             *
!*                                                                     *
!***********************************************************************
!
! ...... Internal energy and density the solid hydrate
!
         CALL HRho_Hydrate(TX,PX,D_Hydr,H_Hydr)
!
! ----------
! ...... Determine the ice density and enthalpy
! ----------
!                                                                      
         D_ice = Ice_Density(TX,PX)
         H_ice = Ice_Enthalpy(PX,TX)
! 
! ----------
! ...... Compute the heat of hydrate dissociation/formation 
! ----------
! 
         H_Heat = 0.0d0 ! HeatDiss_HydrCH4(273.16d0,F_EqOption,IGOOD)
! 
! ----------
! ...... Assign value to state index  
! ----------
! 
         IF(k == 1) elem(n)%StateIndex = 6_1
! 
! ----------
! ...... Assign values of secondary parameters - Vapor  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(1)   = 0.0d0
         Cell_V(n,k-1)%p_KRel(1)   = 0.0d0
         Cell_V(n,k-1)%p_Visc(1)   = 1.0d0 
         Cell_V(n,k-1)%p_Dens(1)   = 0.0d0 
         Cell_V(n,k-1)%p_Enth(1)   = 0.0d0      
         Cell_V(n,k-1)%p_PCap(1)   = 0.0d0  
         Cell_V(n,k-1)%p_MasF(1,1) = 0.0d0
         Cell_V(n,k-1)%p_MasF(2,1) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,1) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Aqueous  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(2)   = 0.0d0
         Cell_V(n,k-1)%p_KRel(2)   = 0.0d0
         Cell_V(n,k-1)%p_Visc(2)   = 1.0d0
         Cell_V(n,k-1)%p_Dens(2)   = 0.0d0
         Cell_V(n,k-1)%p_Enth(2)   = 0.0d0
         Cell_V(n,k-1)%p_PCap(2)   = 0.0d0
         Cell_V(n,k-1)%p_MasF(1,2) = 0.0d0
         Cell_V(n,k-1)%p_MasF(2,2) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,2) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Hydrate phase  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(3)   = 1.0d0 - S_ice
         Cell_V(n,k-1)%p_Dens(3)   = D_Hydr
         Cell_V(n,k-1)%p_Enth(3)   = H_Hydr
         Cell_V(n,k-1)%p_MasF(1,3) = HCom%H2OMF(1)
         Cell_V(n,k-1)%p_MasF(2,3) = HCom%GasMF(1)
         Cell_V(n,k-1)%p_MasF(3,3) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Ice phase 
! ----------
! 
         Cell_V(n,k-1)%p_Satr(4)   = S_ice
         Cell_V(n,k-1)%p_Dens(4)   = D_ice
         Cell_V(n,k-1)%p_Enth(4)   = H_ice
         Cell_V(n,k-1)%p_MasF(1,4) = 1.0d0
         Cell_V(n,k-1)%p_MasF(2,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,4) = 0.0d0
! 
! ----------
! ...... Store the temperature 
! ----------
! 
         Cell_V(n,k-1)%TemC = TX
! 
! ----------
! ...... Store other important variables  
! ----------
! 
         IF(K == 1) THEN
! 
! ......... Store equilibrium hydration pressure at TX 
! 
            Cell_V(n,0)%p_AddD(1) = PX
! 
! ......... Store the hydrate temperature depression  
! 
            Cell_V(n,1)%p_AddD(1) = T_dev
! 
         END IF
! 
! ----------
! ...... Store heat of dissociation/formation 
! ----------
! 
         Cell_V(n,k-1)%p_AddD(3) = H_Heat
! 
! 
! 
      END DO DO_NumEqs
! 
! 
! 
      RETURN     ! Task completed - Exit the routine
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                         PHASE TRANSITIONS                           >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
 1000 CONTINUE
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Hydrate + Ice (2-phase) 
! .............. to Aqueous + Hydrate + Ice (3-phase)      
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
      IF_PhTran: IF(PhTran == 'IcH->AIH') THEN
!                                                                       
         IF(MOP(5) >= 3) WRITE(6,6001) elem(n)%name,PX,P_frz
         IF(KC == 0) THEN
            WRITE(6,6101) 
            STOP                                             
         END IF
! 
! ...... Reset 2nd variable as the aqueous phase
! 
         XX(2)      = ZERO   ! 1.0d-4
         DX(NLOC+2) = XX(2) - X(NLOC+2)
! 
! ...... Reset 3nd or 4th variable as the ice saturation
! 
         IF(NK == 2) THEN
            XX(3)      = S_ice - ZERO   ! 1.0d-4
            DX(NLOC+3) = XX(3) - X(NLOC+3)
         ELSE
            XX(4)      = S_ice - ZERO   ! 1.0d-4
            DX(NLOC+4) = XX(4) - X(NLOC+4)
         END IF
! 
! ...... Call the 3-phase (Aqueous + Ice + Hydrate) routine 
! 
         CALL PHS3_AIH_Eql(N,NLOC,NMAT)
! 
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Hydrate + Ice (2-phase) 
! .............. to Gas + Hydrate + Ice (3-phase)      
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! 
      ELSE IF(PhTran == 'IcH->IGH') THEN
!                                                                       
         IF(MOP(5) >= 3) WRITE(6,6002) elem(n)%name,PX,Peq_Hydr
         IF(KC == 0) THEN
            WRITE(6,6101) 
            STOP                                             
         END IF
! 
! ...... Reset 1st variable as the gas phase
! 
         XX(1)      = ZERO   ! 1.0d-4
         DX(NLOC+1) = XX(1) - X(NLOC+1)
! 
! ...... Reset 2nd variable as the ice phase
! 
         XX(2)      = S_ice
         DX(NLOC+2) = XX(2) - X(NLOC+2)
! 
! ...... Call the 3-phase (Ice+Hydrate+Gas) routine 
! 
         CALL PHS3_IGH_Eql(N,NLOC,NMAT)
! 
      ELSE 
! 
         WRITE(6,6102)
         STOP
! 
      END IF IF_PhTran
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'PHS2_IcH_Eql      1.0   24 January   2005',6X,  & 
     &         'Routine for assigning the secondary ',&
     &         'parameters for the 2-phase ', &
     &       /,T48,'(Hydrate + Ice) system - ',&
     &         'Equilibrium hydrate reaction')
!
 6001 FORMAT(' !!!!!! "PHS2_IcH_Eql": Aqueous phase ',&
     &       'evolves at element "',A8,  &
     &       '" ==> P = ',1pe15.8,'   P_frz = ',1pE15.8)
 6002 FORMAT(' !!!!!! "PHS2_IcH_Eql": Gas phase evolves at element "',&
     &       A8,'" ==> P = ',1pE15.8,'   P_eq = ',1pE15.8)
! 
 6101 FORMAT(' !!!!!!!!!!   Routine PHS2_IcH_Eql: ERRONEOUS ',  &
     &       'DATA INITIALIZATION   ==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
 6102 FORMAT(' !!!!!!!!!!   Routine PHS2_IcH_Eql: ERRONEOUS ',  &
     &       'PHASE CHANGE   ==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of PHS2_IcH_Eql
! 
! 
      RETURN
!
      END SUBROUTINE PHS2_IcH_Eql
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE PHS3_AGH_Eql(N,NLOC,NMAT)
! 
! ... Modules to be used 
! 
      USE Universal_Param
      USE Basic_Param
      USE GenControl_Param
      USE Diffusion_Param
!
      USE H2O_Param
      USE Hydrate_Param
!
      USE Variable_Arrays
      USE Element_Arrays
      USE PFMedProp_Arrays
!
      USE H2O_Properties
      USE Hydrate_Properties
      USE RealGas_Properties
	  USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*        ROUTINE FOR COMPUTING AND ASSIGNING ALL THE SECONDARY        *
!*          VARIABLES IN THREE-PHASE (H2O+Hydrate+Gas) SYSTEMS         *
!*                                                                     *
!*                   EQUILIBRIUM HYDRATION REACTION                    *
!*                                                                     *
!*                  Version 1.0 - September 29, 2004                   *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!                                                                       
      IMPLICIT NONE
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(2) :: Y_inG
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: PX,TX,Psat,S_gas,S_aqu
      REAL(KIND = 8) :: DW,UW,HW,D_Hydr,H_Hydr
! 
      REAL(KIND = 8) :: ZLiq,ZGas,D_gas,rho_L,H_Sol,D_aqu,dummy
      REAL(KIND = 8) :: Hi_Gas,Ui_Gas,Hd_Gas,Ud_Gas,Sd_Gas,H_Gas,U_Gas
! 
      REAL(KIND = 8) :: V_aqu,V_gas,T_k
! 
      REAL(KIND = 8) :: k_rel_aqu,k_rel_gas,P_cap
! 
      REAL(KIND = 8) :: X_mG,X_wG,Xmol_mA,X_mA,X_wA
      REAL(KIND = 8) :: S_Hydr,Peq_Hydr,HK_GinH2O,H_Heat
      REAL(KIND = 8) :: T_dev,X_iA,Xmol_iA,Xmol_mA_old,Xmol_iA_old
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN)  :: N,NLOC,NMAT
! 
      INTEGER :: K,ixx
! 
      INTEGER :: HPeq_Paramtrs  ! Integer function
! 
! -------
! ... Character variables     
! -------
! 
      CHARACTER(LEN = 8) :: PhTran 
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of PHS3_AGH_Eql
!
!
      IF(First_call) THEN
         IF(MPI_RANK==0) WRITE(11,6000)
         First_call = .FALSE.
      END IF
! 
! ... Some initializations 
! 
      PhTran = '        '
! 
! -------
! ... Initialization of T-depression variables        
! -------
! 
      T_dev    = Cell_V(n,1)%p_AddD(1) 
      Xmol_mA  = 0.0d0
      Xmol_iA  = 0.0d0
! 
      Xmol_mA_old  = 0.0d0
      Xmol_iA_old  = 0.0d0
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>      Main loop determining conditions and assigning properties      >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_NumEqs: DO K=1,NEQ1
!
! ----------
! ...... Initial assignments of primary variable values        
! ...... Primary variables: S_gas, S_aqu, Tx        
! ----------
! 
         S_gas = XX(1)                                                      
         S_aqu = XX(2)                                     
! 
         IF(NK == 2) THEN
            X_iA = 0.0d0
            TX   = XX(3)
         ELSE IF(NK == 3) THEN
            X_iA = XX(3)
            TX   = XX(4)
         END IF
! 
! ...... Ensure realistic saturation values
! 
         IF((S_gas > 1.0d0 .AND. S_aqu > 1.0d0) .OR. &
     &      (S_gas < 0.0d0 .AND. S_aqu < 0.0d0)) &
     &   THEN
            IGOOD = 2
            RETURN
         END IF
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the first iteration - Unincremented variables
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         IF_1stEq: IF(K == 1) THEN
!
! -------------
! ......... The hydrate saturation
! -------------
!
            S_Hydr = 1.0d0 - S_gas - S_aqu                               
! 
! -------------
! ......... Constraint on inhibitor mass fraction in the aqueous phase        
! -------------
! 
            IF_SaltMassF: IF (X_iA < 0.0d0) THEN
               X_iA       =   0.0d0
               XX(3)      =   0.0d0
               DX(NLOC+3) = - X(NLOC+3)
            END IF IF_SaltMassF
!
!***********************************************************************
!*                                                                     *
!*                  Determine iteratively the T_shift                  *
!*                                                                     *
!***********************************************************************
!
            IGOOD = HPeq_Paramtrs(1,TX,X_iA,.FALSE.,   &                     ! In only
     &                            Xmol_iA,Xmol_mA_old,Xmol_iA_old,T_dev, &   ! In and out
     &                            Peq_Hydr,HK_GinH2O,Xmol_mA,X_mA)          ! Out only
! 
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
!
!***********************************************************************
!*                                                                     *
!*    Check phase transition from Aqueous + Hydrate + Gas (3-phase)    *
!*                    to Aqueous + Hydrate (2-phase)                   *
!*                                                                     *
!***********************************************************************
!
            IF_GasDis: IF(S_gas < -ZERO) THEN
!
               PhTran = 'AGH->AqH'
               GO TO 1000
!
            END IF IF_GasDis
!
!***********************************************************************
!*                                                                     *
!*    Check phase transition from Aqueous + Hydrate + Gas (3-phase)    *
!*                      to Aqueous + Gas (2-phase)                     *
!*                                                                     *
!***********************************************************************
!
            IF_HydDis: IF(S_Hydr < -ZERO) THEN
! 
               PhTran = 'AGH->AqG'
               GO TO 1000
! 
            END IF IF_HydDis
!
!***********************************************************************
!*                                                                     *
!*    Check phase transition from Aqueous + Hydrate + Gas (3-phase)    *
!*     to Aqueous + Hydrate + Gas + Ice (4-phase: Quadruple point)     *
!*                                                                     *
!***********************************************************************
!
            IF_IceEvol: IF((TX+T_dev) < Tc_quad_hyd) THEN
! 
               PhTran = 'AGH->QuP'
               GO TO 1000
! 
            ELSE
! 
               GO TO 500 
! 
            END IF IF_IceEvol
! 
         END IF IF_1stEq
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the remaining iterations - Incremented variables
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         CASE_Incr: SELECT CASE(K)
! 
! ...... Incrementing gas phase saturation 
! 
         CASE(2)                    
! 
            DELX(NLOC+1) = DFAC
            S_gas        = XX(1) + DELX(NLOC+1)
! 
! ...... Incrementing aqueous phase saturation  
! 
         CASE(3)                     
! 
            DELX(NLOC+2) = DFAC
            S_aqu        = XX(2) + DELX(NLOC+2)
! 
! ...... Incrementing temperature (NK = 2) or salt mass fraction (NK = 3) 
! 
         CASE(4) 
! 
            IF(NK == 2) THEN
               X_iA         = 0.0d0
               DELX(NLOC+3) = DFAC*(273.15d0 + XX(3))
               TX           = XX(3) + DELX(NLOC+3)
            ELSE IF(NK == 3) THEN
               DELX(NLOC+3) = DFAC
               X_iA         = XX(3) + DELX(NLOC+3)
            END IF
! 
! ...... Incrementing temperature (NK = 3)
! 
         CASE(5) 
! 
            DELX(NLOC+4) = DFAC*(273.15d0 + XX(4))
            TX           = XX(4) + DELX(NLOC+4)
! 
         END SELECT CASE_Incr
!
!***********************************************************************
!*                                                                     *
!*         For augmented primary variables:                            *
!*             Determine the relevant parameters                       *
!*                                                                     *
!***********************************************************************
! 
         IF_KGT1: IF (K > 1) THEN 
!
! ......... The augmented hydrate saturation                    
! 
            S_Hydr = 1.0d0 - S_gas - S_aqu   
!
! ......... Determine hydration-related data                   
! 
            IGOOD = HPeq_Paramtrs(1,TX,X_iA,.FALSE.,   &                       
     &                            Xmol_iA,Xmol_mA_old,Xmol_iA_old,T_dev,   &
     &                            Peq_Hydr,HK_GinH2O,Xmol_mA,X_mA)          
! 
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
! 
         END IF IF_KGT1                                       
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... Compute thermophysical properties 
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
  500    T_k = TX + 273.15d0  ! The augmented temperature in K 
! 
! ----------
! ...... Determine the pressure in the system       
! ----------
! 
         PX = Peq_Hydr
! 
! ----------
! ...... Determine Psat: the saturation pressure of H2O at TX       
! ----------
! 
         IGOOD = P_Sat_H2O_S(TX,Psat)
! 
         IF(IGOOD /= 0) THEN
            CALL WARN_RF(elem(n)%name,XX,NK1,KC)
            RETURN
         END IF                                       
!
!***********************************************************************
!*                                                                     *
!*             P R O P E R T I E S  +  P A R A M E T E R S             *
!*                                                                     *
!***********************************************************************
!
! ...... Internal energy and density of liquid H2O 
!
         CALL UHRho_LiqH2O(TX,PX,DW,UW,HW,IGOOD)                         
!
! ...... Internal energy and density the solid hydrate
!
         CALL HRho_Hydrate(TX,PX,D_Hydr,H_Hydr)
! 
! -------------
! ...... Mole fractions in the gas phase      
! -------------
! 
         Y_inG(1) = (Peq_Hydr - Psat)/PX     ! CH4
         Y_inG(2) = 1.0d0-Y_inG(1)           ! H2O
!
! ...... Compute mass fractions in the gas phase
!
         X_mG =   MW_CH4*Y_inG(1)   &
     &         /( MW_CH4*Y_inG(1) + MW_H2O*Y_inG(2) )                                                  
!
         X_wG = 1.0d0-X_mG                                                
!
! ...... Determine the gas density and compressibility
!
         CALL Zcomp(PX,T_k,0.0d0,Y_inG,ZLiq,ZGas,D_gas,rho_L,ixx)
!                                                                      
! ...... Obtain the specific enthalpy of the gas phase
!                                                                      
         CALL HU_IdealGas(0,2.7315d2,T_k,Hi_Gas,Ui_Gas)    ! Ideal enthalpy
         CALL HUS_Departure(T_k,ZGas,Hd_Gas,Ud_Gas,Sd_Gas) ! Departure enthalpy
!                                                                      
         H_gas = ( Hi_Gas - Hd_Gas )     &            ! Convert to J/Kg/K from J/kgmol/K
     &          /( Y_inG(1)*MW_CH4 + Y_inG(2)*MW_H2O )  
!                                                                      
         U_gas = ( Ui_Gas - Ud_Gas )      &             ! Convert to J/Kg/K from J/kgmol/K
     &          /( Y_inG(1)*MW_CH4 + Y_inG(2)*MW_H2O )  
!
! ...... Mass fraction of H2O in the aqueous phase
!
         X_wA = 1.0d0-X_mA-X_iA                     
!                                                                      
! ...... Obtain the heat of CH4 solution in H2O
!                                                                      
         H_Sol = HEAT_GasSolution(1,TX,Xmol_iA,HK_GinH2O,dummy)                                     
!                                                                      
! ...... Aqueous phase density (assumed equal to water)                                                                      
!                                                                      
         D_aqu = X_wA*DW+X_iA*D_Inhib+X_mA*D_gas
!                                                                      
! ...... Liquid H2O viscosity                                                                      
!                                                                      
         V_aqu = ViscLV_H2Ox(TX,D_aqu)                         
!                                                                      
! ...... Multicomponent gas viscosity                                                                       
!                                                                      
         V_gas = Gas_Viscosity(PX,T_k,ZGas)                          
!                                                                      
! -------------
! ..... Relative permeability and capillary pressure 
! -------------
!                                                                      
         CALL RelPerm_Hyd(S_aqu,S_gas,k_rel_aqu,k_rel_gas,dummy,nmat)                          
         CALL CapPres_Hyd(TX,S_aqu,S_gas,D_aqu,P_cap,nmat)
! 
! ----------
! ...... Compute the heat of hydrate dissociation/formation 
! ----------
! 
         H_Heat = HeatDiss_HydrCH4(T_k,&
     &                             F_EqOption, &  ! H-equation
     &                             IGOOD)
! 
! ----------
! ...... Assign value to state index  
! ----------
! 
         IF(k == 1) elem(n)%StateIndex = 7_1
! 
! ----------
! ...... Assign values of secondary parameters - Vapor  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(1)   = S_gas
         Cell_V(n,k-1)%p_KRel(1)   = k_rel_gas
         Cell_V(n,k-1)%p_Visc(1)   = V_gas
         Cell_V(n,k-1)%p_Dens(1)   = D_gas
         Cell_V(n,k-1)%p_Enth(1)   = H_Gas      
         Cell_V(n,k-1)%p_PCap(1)   = 0.0d0  
         Cell_V(n,k-1)%p_MasF(1,1) = X_wG
         Cell_V(n,k-1)%p_MasF(2,1) = X_mG
         Cell_V(n,k-1)%p_MasF(3,1) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Aqueous  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(2)   = S_aqu
         Cell_V(n,k-1)%p_KRel(2)   = k_rel_aqu
         Cell_V(n,k-1)%p_Visc(2)   = V_aqu
         Cell_V(n,k-1)%p_Dens(2)   = D_aqu
         Cell_V(n,k-1)%p_Enth(2)   =  X_wA*HW &
     &                              + X_mA*(H_Gas + H_Sol) &
     &                              + X_iA*H_InhSol
         Cell_V(n,k-1)%p_PCap(2)   = P_cap
         Cell_V(n,k-1)%p_MasF(1,2) = X_wA
         Cell_V(n,k-1)%p_MasF(2,2) = X_mA
         Cell_V(n,k-1)%p_MasF(3,2) = X_iA
! 
! ----------
! ...... Assign values of secondary parameters - Hydrate phase  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(3)   = S_Hydr
         Cell_V(n,k-1)%p_Dens(3)   = D_Hydr
         Cell_V(n,k-1)%p_Enth(3)   = H_Hydr
         Cell_V(n,k-1)%p_MasF(1,3) = HCom%H2OMF(1)
         Cell_V(n,k-1)%p_MasF(2,3) = HCom%GasMF(1)
         Cell_V(n,k-1)%p_MasF(3,3) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Ice phase 
! ----------
! 
         Cell_V(n,k-1)%p_Satr(4)   = 0.0d0
         Cell_V(n,k-1)%p_Dens(4)   = 0.0d0
         Cell_V(n,k-1)%p_Enth(4)   = 0.0d0
         Cell_V(n,k-1)%p_MasF(1,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(2,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,4) = 0.0d0
! 
! ----------
! ...... Store the temperature 
! ----------
! 
         Cell_V(n,k-1)%TemC = TX
! 
! ----------
! ...... Store other important variables  
! ----------
! 
         IF(K == 1) THEN
! 
! ......... Store equilibrium hydration pressure at TX 
! 
            Cell_V(n,0)%p_AddD(1) = Peq_Hydr
! 
! ......... Store the hydrate temperature depression  
! 
            Cell_V(n,1)%p_AddD(1) = T_dev
! 
         END IF
! 
! ----------
! ...... Store equilibrium hydration pressure 
! ----------
! 
         Cell_V(n,k-1)%p_AddD(2) = Peq_Hydr
! 
! ----------
! ...... Store heat of dissociation/formation 
! ----------
! 
         Cell_V(n,k-1)%p_AddD(3) = H_Heat
! 
! 
! 
      END DO DO_NumEqs
! 
! 
! 
      RETURN     ! Task completed - Exit the routine
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                         PHASE TRANSITIONS                           >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
 1000 CONTINUE
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Aqueous+Hydrate+Gas (3-phase) 
! .............. to Aqueous+Hydrate (2-phase)      
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
      IF_PhTran: IF(PhTran == 'AGH->AqH') THEN
!                                                                       
         IF(MOP(5) >= 3) WRITE(6,6001) elem(n)%name,S_gas
         IF(KC == 0) THEN
            WRITE(6,6101) 
            STOP                                             
         END IF
! 
! ...... Reset 1st variable as pressure 
! 
         XX(1)      = Peq_Hydr*(1.0d0 + 1.0d-5)  !1.001d0
         DX(NLOC+1) = XX(1) - X(NLOC+1)
! 
! ...... Call the 2-phase (H2O+Hydrate) routine 
! 
         CALL PHS2_AqH_Eql(N,NLOC,NMAT)
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
!  .............. Transition from Aqueous+Hydrate+Gas (3-phase)       
!  .............. to Aqueous+Gas (2-phase)      
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! 
      ELSE IF(PhTran == 'AGH->AqG') THEN
!                                                                       
         IF(MOP(5) >= 3) WRITE(6,6002) elem(n)%name,S_hydr
         IF(KC == 0) THEN
            WRITE(6,6101) 
            STOP                                             
         END IF
! 
! ...... Reset 1st variable as pressure 
! 
         XX(1)      = Peq_Hydr*(1.0d0 - 1.0d-5)  
         DX(NLOC+1) = XX(1) - X(NLOC+1)
! 
! ...... Reset 2nd variable as aqueous saturation
! 
         XX(2)      = S_aqu + 1.0d-5 
         DX(NLOC+2) = XX(2) - X(NLOC+2)
! 
! ...... Call the 2-phase (H2O+Gas) routine 
! 
         CALL PHS2_AqG_Eql(N,NLOC,NMAT)
! 
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
!  .............. Transition from Aqueous+Hydrate+Gas (3-phase)       
!  .............. to Quadruple point (4-phases: Aqueous+Hydrate+Gas+Ice)      
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! 
      ELSE IF(PhTran == 'AGH->QuP') THEN
!                                                                       
         IF(MOP(5) >= 3) WRITE(6,6005) elem(n)%name,TX
         IF(KC == 0) THEN
            WRITE(6,6101) 
            STOP                                             
         END IF
! 
! ...... Reset 3nd or 4th variable as ice 
! 
         IF(NK == 2) THEN
            XX(3)      = ZERO
            DX(NLOC+3) = XX(3) - X(NLOC+3)
         ELSE
            XX(4)      = ZERO
            DX(NLOC+4) = XX(4) - X(NLOC+4)
         END IF
! 
! ...... Call the 4-phase (Aqueous+Hydrate+Gas+Ice) routine 
! 
         CALL PHS4_QuP_Eql(N,NLOC,NMAT)
! 
      ELSE 
! 
         WRITE(6,6102)
         STOP
! 
      END IF IF_PhTran
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'PHS3_AGH_Eql      1.0   29 September 2004',6X,   &
     &         'Routine for assigning the secondary ',   &
     &         'parameters of the 3-phase (Aqueous+Gas',  &
     &         '+Hydrate) ',  &
     &       /,T48,'system - Equilibrium hydrate reaction')
!
 6001 FORMAT(' !!!!!! "PHS3_AGH_Eql": Gas phase disappears at ',&
     &       'element "',A8,'" ==> S_gas = ',1pE15.8)
 6002 FORMAT(' !!!!!! "PHS3_AGH_Eql": Hydrate disappears at element "',&
     &       A8,'" ==> S_hydr = ',1pE15.8)
 6004 FORMAT(' !!!!!! "PHS3_AGH_Eql": Aqueous phase disappears at ',&
     &       'element "',A8,'" ==> S_aqu = ',1pE15.8)
 6005 FORMAT(' !!!!!! "PHS3_AGH_Eql": Ice evolves at element "',A8,&
     &       '" ==> TX = ',1pE15.8)
! 
 6101 FORMAT(' !!!!!!!!!!   Routine PHS3_AGH_Eql: ERRONEOUS ',   &
     &       'DATA INITIALIZATION   ==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
 6102 FORMAT(' !!!!!!!!!!   Routine PHS3_AGH_Eql: ERRONEOUS ',  &
     &       'PHASE CHANGE   ==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of PHS3_AGH_Eql
! 
! 
      RETURN
!
      END SUBROUTINE PHS3_AGH_Eql
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE PHS3_AIH_Eql(N,NLOC,NMAT)
! 
! ... Modules to be used 
! 
      USE Universal_Param
      USE Basic_Param
      USE GenControl_Param
      USE Diffusion_Param
!
      USE H2O_Param
      USE Hydrate_Param
!
      USE Variable_Arrays
      USE Element_Arrays
      USE PFMedProp_Arrays
!
      USE H2O_Properties
      USE Hydrate_Properties
      USE RealGas_Properties
	  USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*        ROUTINE FOR COMPUTING AND ASSIGNING ALL THE SECONDARY        *
!*         VARIABLES AT THE Aqueous+Hydrate+Ice 3-PHASE SYSTEM         *
!*                                                                     *
!*                  EQUILIBRIUM HYDRATION REACTION                     *
!*                                                                     *
!*                  Version 1.0 - January 21, 2005                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!                                                                       
      IMPLICIT NONE
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(2) :: Y_inG
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: PX,TX,S_aqu
      REAL(KIND = 8) :: DW,UW,HW,D_Hydr,H_Hydr
      REAL(KIND = 8) :: S_ice,D_ice,H_ice,k_rel_gas
! 
      REAL(KIND = 8) :: ZLiq,ZGas,D_gas,rho_L,H_Sol,D_aqu,dummy
      REAL(KIND = 8) :: Hi_Gas,Ui_Gas,Hd_Gas,Ud_Gas,Sd_Gas,H_Gas,U_Gas
! 
      REAL(KIND = 8) :: V_aqu,T_k
! 
      REAL(KIND = 8) :: k_rel_aqu
! 
      REAL(KIND = 8) :: Xmol_mA,X_mA,X_wA
      REAL(KIND = 8) :: S_Hydr,Peq_Hydr,HK_GinH2O,H_Heat
      REAL(KIND = 8) :: T_dev,X_iA,Xmol_iA,Xmol_mA_old,Xmol_iA_old
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN)  :: N,NLOC,NMAT
! 
      INTEGER :: K,ixx
! 
      INTEGER :: HPeq_Paramtrs  ! Integer function
! 
! -------
! ... Character variables     
! -------
! 
      CHARACTER(LEN = 8) :: PhTran 
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of PHS3_AIH_Eql
!
!
      IF(First_call) THEN
         First_call = .FALSE.
         IF(MPI_RANK==0) WRITE(11,6000)
      END IF
! 
! ... Some initializations 
! 
      PhTran = '        '
! 
! -------
! ... Initialization of T-depression variables        
! -------
! 
      T_dev    = Cell_V(n,1)%p_AddD(1)  
      Xmol_mA  = 0.0d0
      Xmol_iA  = 0.0d0
! 
      Xmol_mA_old  = 0.0d0
      Xmol_iA_old  = 0.0d0
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>      Main loop determining conditions and assigning properties      >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_NumEqs: DO K=1,NEQ1
!
! ----------
! ...... Initial assignments of primary variable values        
! ...... Primary variables: PX, S_aqu, S_ice        
! ----------
! 
         PX    = XX(1)                                                      
         S_aqu = XX(2)                                     
! 
         IF(NK == 2) THEN
            X_iA  = 0.0d0
            S_ice = XX(3)
         ELSE IF(NK == 3) THEN
            X_iA  = XX(3)
            S_ice = XX(4)
         END IF
! 
! ...... Ensure realistic saturation values
! 
         IF((S_ice > 1.0d0 .AND. S_aqu > 1.0d0) .OR. &
     &      (S_ice < 0.0d0 .AND. S_aqu < 0.0d0)) &
     &   THEN
            IGOOD = 2
            RETURN
         END IF
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the first iteration - Unincremented variables
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         IF_1stEq: IF(K == 1) THEN
!
! -------------
! ......... Hydrate saturation
! -------------
!
            S_Hydr = 1.0d0 - S_aqu - S_ice
! 
! -------------
! ......... Constraint on inhibitor mass fraction in the aqueous phase        
! -------------
! 
            IF_SaltMassF: IF (X_iA < 0.0d0) THEN
               X_iA       =   0.0d0
               XX(3)      =   0.0d0
               DX(NLOC+3) = - X(NLOC+3)
            END IF IF_SaltMassF
!
! -------------
! ......... Determine the fusion temperature at the prevailing pressure 
! -------------
!
            IGOOD = T_Fusion_H2O_S((PX - P_quad_hyd + P_H2O_3p),TX)
! 
! -------------
! ......... Determine Peq_Hydr and the corresponding mole fractions       
! -------------
! 
            IGOOD = HPeq_Paramtrs(1,TX,X_iA,.TRUE.,   &                      ! In only
     &                            Xmol_iA,Xmol_mA_old,Xmol_iA_old,T_dev, &   ! In and out
     &                            Peq_Hydr,HK_GinH2O,Xmol_mA,X_mA)          ! Out only
! 
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
!
!***********************************************************************
!*                                                                     *
!*    Check phase transition from Aqueous + Hydrate + Ice (3-phase)    *
!*                    to Aqueous + Hydrate (2-phase)                   *
!*                                                                     *
!***********************************************************************
!
            IF_IceDis: IF(S_ice < 0.0d0) THEN
! 
               PhTran = 'AIH->AqH'
               GO TO 1000
!
            END IF IF_IceDis
!
!***********************************************************************
!*                                                                     *
!*    Check phase transition from Aqueous + Hydrate + Ice (3-phase)    *
!*                      to Ice + Hydrate (2-phase)                     *
!*                                                                     *
!***********************************************************************
!
            IF_AquDis: IF(S_aqu < 0.0d0) THEN
! 
               PhTran = 'AIH->IcH'
               GO TO 1000
!
            END IF IF_AquDis
!
!***********************************************************************
!*                                                                     *
!*    Check phase transition from Aqueous + Hydrate + Ice (3-phase)    *
!*     to Aqueous + Hydrate + Ice + Gas (4-phase, Quadruple point)     *
!*                                                                     *
!***********************************************************************
!
            IF_GasEvolv: IF(PX < P_quad_hyd) THEN
! 
               PhTran = 'AIH->QuP'
               GO TO 1000
!
            ELSE
! 
               GO TO 500 
! 
            END IF IF_GasEvolv
! 
         END IF IF_1stEq
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the remaining iterations - Incremented variables
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         CASE_Incr: SELECT CASE(k)
! 
! ...... Incrementing gas phase saturation 
! 
         CASE(2)                    
! 
            DELX(NLOC+1) = DFAC*XX(1)
            PX           = XX(1) + DELX(NLOC+1)
! 
! ...... Incrementing aqueous phase saturation  
! 
         CASE(3)                     
! 
            DELX(NLOC+2) = DFAC
            S_aqu        = XX(2) + DELX(NLOC+2)
! 
! ...... Incrementing ice saturation (NK = 2) or salt mass fraction (NK = 3) 
! 
         CASE(4) 
! 
            IF(NK == 2) THEN
               X_iA         = 0.0d0
               DELX(NLOC+3) = DFAC
               S_ice        = XX(3) + DELX(NLOC+3)
            ELSE IF(NK == 3) THEN
               DELX(NLOC+3) = DFAC
               X_iA         = XX(3) + DELX(NLOC+3)
            END IF
! 
! ...... Incrementing hydrate saturation (NK = 3)
! 
         CASE(5) 
! 
            DELX(NLOC+4) = DFAC
            S_ice        = XX(4) + DELX(NLOC+4)
! 
         END SELECT CASE_Incr
!
!***********************************************************************
!*                                                                     *
!*         For augmented primary variables:                            *
!*             Determine the relevant parameters                       *
!*                                                                     *
!***********************************************************************
! 
         IF_KGT1: IF(k > 1) THEN
!
! ......... Augmented hydrate saturation
!
            S_Hydr = 1.0d0 - S_aqu - S_ice
!
! ......... Determine the fusion temperature at the prevailing pressure 
!
            IGOOD = T_Fusion_H2O_S((PX - P_quad_hyd + P_H2O_3p),TX)
! 
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
! 
! ......... Determine Peq_Hydr and the corresponding mole fractions       
! 
            IGOOD = HPeq_Paramtrs(1,TX,X_iA,.TRUE.,         &                  ! In only
     &                            Xmol_iA,Xmol_mA_old,Xmol_iA_old,T_dev, &     ! In and out
     &                            Peq_Hydr,HK_GinH2O,Xmol_mA,X_mA)            ! Out only
!
          END IF IF_KGT1 
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... Compute thermophysical properties 
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
  500    T_k = TX + 273.15d0   ! Initial temperature estimate  
!
!***********************************************************************
!*                                                                     *
!*             P R O P E R T I E S  +  P A R A M E T E R S             *
!*                                                                     *
!***********************************************************************
!
! ...... Internal energy and density of liquid H2O 
!
         CALL UHRho_LiqH2O(TX,PX,DW,UW,HW,IGOOD)                         
!
! ...... Internal energy and density the solid hydrate
!
         CALL HRho_Hydrate(TX,PX,D_Hydr,H_Hydr)
!
! ----------
! ...... Determine the ice density and enthalpy
! ----------
!                                                                      
         D_ice = Ice_Density(TX,PX)
         H_ice = Ice_Enthalpy(PX,TX)
! 
! -------------
! ...... Mole fractions in the gas phase      
! -------------
! 
         Y_inG(1) = 1.0d0    ! CH4
         Y_inG(2) = 0.0d0    ! H2O
!
! ...... Determine the gas density and compressibility
!
         CALL Zcomp(PX,T_k,0.0d0,Y_inG,ZLiq,ZGas,D_gas,rho_L,ixx)
!                                                                      
! ...... Obtain the specific enthalpy of the gas phase
!                                                                      
         CALL HU_IdealGas(0,2.7315d2,T_k,Hi_Gas,Ui_Gas)    ! Ideal enthalpy
         CALL HUS_Departure(T_k,ZGas,Hd_Gas,Ud_Gas,Sd_Gas) ! Departure enthalpy
!                                                                      
         H_gas = ( Hi_Gas - Hd_Gas )  &                 ! Convert to J/Kg/K from J/kgmol/K
     &          /( Y_inG(1)*MW_CH4 + Y_inG(2)*MW_H2O )  
!                                                                      
         U_gas = ( Ui_Gas - Ud_Gas )  &                 ! Convert to J/Kg/K from J/kgmol/K
     &          /( Y_inG(1)*MW_CH4 + Y_inG(2)*MW_H2O )  
!
! ...... Mass fraction of H2O in the aqueous phase
!
         X_wA = 1.0d0 - X_mA-X_iA                     
!                                                                      
! ...... Aqueous phase density (assumed equal to water)                                                                      
!                                                                      
         D_aqu = X_wA*DW + X_iA*D_Inhib + X_mA*D_gas
!                                                                      
! ...... Liquid H2O viscosity                                                                      
!                                                                      
         V_aqu = ViscLV_H2Ox(TX,D_aqu)            
!                                                                      
! ...... Obtain the heat of CH4 solution in H2O
!                                                                      
         H_Sol = HEAT_GasSolution(1,TX,Xmol_iA,HK_GinH2O,dummy)                                     
!                                                                      
! -------------
! ..... Relative permeability 
! -------------
!                                                                      
         CALL RelPerm_Hyd(S_aqu,0.0d0,k_rel_aqu,k_rel_gas,dummy,nmat)                          
! 
! ----------
! ...... Compute the heat of hydrate dissociation/formation 
! ----------
! 
         H_Heat = HeatDiss_HydrCH4(T_k, &
     &                             F_EqOption,  & ! H-equation
     &                             IGOOD)
! 
! ----------
! ...... Assign value to state index  
! ----------
! 
         IF(k == 1) elem(n)%StateIndex = 9_1
! 
! ----------
! ...... Assign values of secondary parameters - Vapor  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(1)   = 0.0d0
         Cell_V(n,k-1)%p_KRel(1)   = 0.0d0
         Cell_V(n,k-1)%p_Visc(1)   = 1.0d0 
         Cell_V(n,k-1)%p_Dens(1)   = 0.0d0 
         Cell_V(n,k-1)%p_Enth(1)   = 0.0d0      
         Cell_V(n,k-1)%p_PCap(1)   = 0.0d0  
         Cell_V(n,k-1)%p_MasF(1,1) = 0.0d0
         Cell_V(n,k-1)%p_MasF(2,1) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,1) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Aqueous  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(2)   = S_aqu
         Cell_V(n,k-1)%p_KRel(2)   = k_rel_aqu
         Cell_V(n,k-1)%p_Visc(2)   = V_aqu
         Cell_V(n,k-1)%p_Dens(2)   = D_aqu
         Cell_V(n,k-1)%p_Enth(2)   =  X_wA*HW &
     &                              + X_mA*(H_Gas + H_Sol) &
     &                              + X_iA*H_InhSol
         Cell_V(n,k-1)%p_PCap(2)   = 0.0d0
         Cell_V(n,k-1)%p_MasF(1,2) = X_wA
         Cell_V(n,k-1)%p_MasF(2,2) = X_mA
         Cell_V(n,k-1)%p_MasF(3,2) = X_iA
! 
! ----------
! ...... Assign values of secondary parameters - Hydrate phase  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(3)   = S_Hydr
         Cell_V(n,k-1)%p_KRel(3)   = 1.0d10
         Cell_V(n,k-1)%p_Dens(3)   = D_Hydr
         Cell_V(n,k-1)%p_Enth(3)   = H_Hydr
         Cell_V(n,k-1)%p_PCap(3)   = 0.0d0
         Cell_V(n,k-1)%p_MasF(1,3) = HCom%H2OMF(1)
         Cell_V(n,k-1)%p_MasF(2,3) = HCom%GasMF(1)
         Cell_V(n,k-1)%p_MasF(3,3) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Ice phase 
! ----------
! 
         Cell_V(n,k-1)%p_Satr(4)   = S_ice
         Cell_V(n,k-1)%p_KRel(4)   = 1.0d10
         Cell_V(n,k-1)%p_Dens(4)   = D_ice
         Cell_V(n,k-1)%p_Enth(4)   = H_ice
         Cell_V(n,k-1)%p_PCap(4)   = 0.0d0
         Cell_V(n,k-1)%p_MasF(1,4) = 1.0d0
         Cell_V(n,k-1)%p_MasF(2,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,4) = 0.0d0
! 
! ----------
! ...... Store the temperature 
! ----------
! 
         Cell_V(n,k-1)%TemC = TX
! 
! ----------
! ...... Store other important variables  
! ----------
! 
         IF(K == 1) THEN
! 
! ......... Store equilibrium hydration pressure at TX 
! 
            Cell_V(n,0)%p_AddD(1) = Peq_Hydr
! 
! ......... Store the hydrate temperature depression  
! 
            Cell_V(n,1)%p_AddD(1) = T_dev
! 
         END IF
! 
! ----------
! ...... Store heat of dissociation/formation 
! ----------
! 
         Cell_V(n,k-1)%p_AddD(3) = H_Heat
! 
! 
! 
      END DO DO_NumEqs
! 
! 
! 
      RETURN     ! Task completed - Exit the routine
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                         PHASE TRANSITIONS                           >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
 1000 CONTINUE
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Aqueous + Hydrate + Ice (3-phase) 
! .............. to Aqueous + Hydrate (2-phase)      
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
      IF_PhTran: IF(PhTran == 'AIH->AqH') THEN
!                                                                       
         IF(MOP(5) >= 3) WRITE(6,6001) elem(n)%name,S_ice
         IF(KC == 0) THEN
            WRITE(6,6101) 
            STOP                                             
         END IF
! 
! ...... Reset 2nd variable as aqueous phase saturation
! 
         XX(2)      = 1.0d0 - S_hydr
         DX(NLOC+2) = XX(2) - X(NLOC+2)
! 
! ...... Reset 3nd or 4th variable as temperature 
! 
         IF(NK == 2) THEN
            XX(3)      = TX + ZERO   ! 1.0d-4
            DX(NLOC+3) = XX(3) - X(NLOC+3)
         ELSE
            XX(4)      = TX + ZERO   ! 1.0d-4
            DX(NLOC+4) = XX(4) - X(NLOC+4)
         END IF
! 
! ...... Call the 2-phase (Aqueous + Hydrate) routine 
! 
         CALL PHS2_AqH_Eql(N,NLOC,NMAT)
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Aqueous + Hydrate + Ice (3-phase) 
! .............. to Ice + Hydrate (2-phase)      
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
      ELSE IF(PhTran == 'AIH->IcH') THEN
!                                                                       
         IF(MOP(5) >= 3) WRITE(6,6002) elem(n)%name,S_aqu
         IF(KC == 0) THEN
            WRITE(6,6101) 
            STOP                                             
         END IF
! 
! ...... Reset 2nd variable as ice phase saturation
! 
         XX(2)      = 1.0d0 - S_hydr
         DX(NLOC+2) = XX(2) - X(NLOC+2)
! 
! ...... Reset 3nd or 4th variable as temperature 
! 
         IF(NK == 2) THEN
            XX(3)      = TX - ZERO   ! 1.0d-3
            DX(NLOC+3) = XX(3) - X(NLOC+3)
         ELSE
            XX(4)      = TX - ZERO   ! 1.0d-3
            DX(NLOC+4) = XX(4) - X(NLOC+4)
         END IF
! 
! ...... Call the 2-phase (L-H2O + Hydrate) routine 
! 
         CALL PHS2_IcH_Eql(N,NLOC,NMAT)
! 
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Aqueous + Hydrate + Ice (3-phase) 
! .............. to Aqueous + Hydrate + Ice + Gas (Quadruple point, 4-phase) 
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! 
      ELSE IF(PhTran == 'AIH->QuP') THEN
!                                                                       
         IF(MOP(5) >= 3) WRITE(6,6003) elem(n)%name,TX
         IF(KC == 0) THEN
            WRITE(6,6101) 
            STOP                                             
         END IF
! 
! ...... Reset 1st variable as gas phase saturation
! 
         XX(1)      = ZERO   ! 1.0d-3
         DX(NLOC+1) = XX(1) - X(NLOC+1)
! 
! ...... Call the 4-phase (Ice+Hydrate+Gas+Aqueous) routine 
! 
         CALL PHS4_QuP_Eql(N,NLOC,NMAT)
! 
      ELSE 
! 
         WRITE(6,6102)
         STOP
! 
      END IF IF_PhTran
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'PHS3_AIH_Eql      1.0   21 January   2005',6X,  & 
     &         'Routine for assigning the secondary ',&
     &         'parameters for the 3-phase ',  &
     &       /,T48,'(Aqueous + Hydrate + Ice) system - ',&
     &         'Equilibrium hydrate reaction')
!
 6001 FORMAT(' !!!!!! "PHS3_AIH_Eql": Ice disappears at element "',A8,&
     &       '" ==> S_ice = ',1pE15.8)
 6002 FORMAT(' !!!!!! "PHS3_AIH_Eql": Aqueous phase disappears at ',&
     &       'element "',A8,'" ==> S_aqu = ',1pE15.8)
 6003 FORMAT(' !!!!!! "PHS3_AIH_Eql": Gas phase evolves at element "',&
     &       A8,'" ==> TX = ',1pE15.8)
! 
 6101 FORMAT(' !!!!!!!!!!   Routine PHS3_AIH_Eql: ERRONEOUS ',   &
     &       'DATA INITIALIZATION   ==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
 6102 FORMAT(' !!!!!!!!!!   Routine PHS3_AIH_Eql: ERRONEOUS ', & 
     &       'PHASE CHANGE   ==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of PHS3_AIH_Eql
! 
! 
      RETURN
!
      END SUBROUTINE PHS3_AIH_Eql
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE PHS3_AIG_Eql(N,NLOC,NMAT)
! 
! ... Modules to be used 
! 
      USE Universal_Param
      USE Basic_Param
      USE GenControl_Param
      USE Diffusion_Param
!
      USE H2O_Param
      USE Hydrate_Param
!
      USE Variable_Arrays
      USE Element_Arrays
      USE PFMedProp_Arrays
!
      USE H2O_Properties
      USE Hydrate_Properties
      USE RealGas_Properties
	  USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*        ROUTINE FOR COMPUTING AND ASSIGNING ALL THE SECONDARY        *
!*        VARIABLES IN THREE-PHASE (Aqueous+Ice+Hydrate) SYSTEMS       *
!*                                                                     *
!*                  EQUILIBRIUM HYDRATION REACTION                     *
!*                                                                     *
!*                  Version 1.0 - January 20, 2005                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!                                                                       
      IMPLICIT NONE
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(2) :: Y_inG
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: PX,TX,Psat,S_gas,S_aqu
      REAL(KIND = 8) :: DW,UW,HW,P_CH4
      REAL(KIND = 8) :: S_ice,D_ice,H_ice
! 
      REAL(KIND = 8) :: ZLiq,ZGas,D_gas,rho_L,H_Sol,D_aqu,dummy
      REAL(KIND = 8) :: Hi_Gas,Ui_Gas,Hd_Gas,Ud_Gas,Sd_Gas,H_Gas,U_Gas
! 
      REAL(KIND = 8) :: V_aqu,V_gas,T_k
! 
      REAL(KIND = 8) :: k_rel_aqu,k_rel_gas,P_cap
! 
      REAL(KIND = 8) :: X_mG,X_wG,Xmol_mA,X_mA,X_wA
      REAL(KIND = 8) :: HK_GinH2O,H_Heat
      REAL(KIND = 8) :: T_dev,X_iA,Xmol_iA,Xmol_mA_old,Xmol_iA_old
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN)  :: N,NLOC,NMAT
! 
      INTEGER :: K,ixx
! 
      INTEGER :: PTXS_Paramtrs  ! Integer function
! 
! -------
! ... Character variables     
! -------
! 
      CHARACTER(LEN = 8) :: PhTran 
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of PHS3_AIG_Eql
!
!
      IF(First_call) THEN
         IF(MPI_RANK==0) WRITE(11,6000)
         First_call = .FALSE.
      END IF
! 
! ... Some initializations 
! 
      PhTran = '        '
! 
! -------
! ... Initialization of T-depression variables        
! -------
! 
      T_dev    = Cell_V(n,1)%p_AddD(1)
      Xmol_mA  = 0.0d0
      Xmol_iA  = 0.0d0
! 
      Xmol_mA_old  = 0.0d0
      Xmol_iA_old  = 0.0d0
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>      Main loop determining conditions and assigning properties      >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_NumEqs: DO K=1,NEQ1
!
! ----------
! ...... Initial assignments of primary variable values        
! ...... Primary variables: PX, S_aqu, S_gas       
! ----------
! 
         PX    = XX(1)                                                      
         S_aqu = XX(2)                                     
! 
         IF(NK == 2) THEN
            X_iA  = 0.0d0
            S_gas = XX(3)
         ELSE IF(NK == 3) THEN
            X_iA  = XX(3)
            S_gas = XX(4)
         END IF
! 
! ...... Ensure realistic saturation values
! 
         IF((S_gas > 1.0d0 .AND. S_aqu > 1.0d0) .OR. &
     &      (S_gas < 0.0d0 .AND. S_aqu < 0.0d0)) &
     &   THEN
            IGOOD = 2
            RETURN
         END IF
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the first iteration - Unincremented variables
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         IF_1stEq: IF(K == 1) THEN
!
! -------------
! ......... Hydrate saturation
! -------------
!
            S_ice = 1.0d0 - S_aqu - S_gas
! 
! -------------
! ......... Constraint on inhibitor mass fraction in the aqueous phase        
! -------------
! 
            IF_SaltMassF: IF (X_iA < 0.0d0) THEN
               X_iA       =   0.0d0
               XX(3)      =   0.0d0
               DX(NLOC+3) = - X(NLOC+3)
            END IF IF_SaltMassF
!
! -------------
! ......... Temperature
! -------------
!
            TX = 1.0d-2   ! Initial temperature estimate  
!
!***********************************************************************
!*                                                                     *
!*    Determine iteratively partial P, T, T-dev and CH4 solubility     *
!*                                                                     *
!***********************************************************************
!
            IGOOD = PTXS_Paramtrs(1,PX,X_iA,TX, &                                   
     &                            Xmol_iA,Xmol_mA_old,Xmol_iA_old,T_dev,     &
     &                            P_CH4,Psat,HK_GinH2O,X_mA,Xmol_mA)            
! 
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
!
!***********************************************************************
!*                                                                     *
!*      Check phase transition from Aqueous + Ice + Gas (3-phase)      *
!*                       to Ice + Gas (2-phase)                        *
!*                                                                     *
!***********************************************************************
!
            IF_LiqDis: IF(S_aqu < 0.0d0) THEN
! 
               PhTran = 'AIG->IcG'
               GO TO 1000
! 
            END IF IF_LiqDis
!
!***********************************************************************
!*                                                                     *
!*      Check phase transition from Aqueous + Ice + Gas (3-phase)      *
!*                     to Aqueous + Gas (2-phase)                      *
!*                                                                     *
!***********************************************************************
!
            IF_IceDis: IF(S_ice < 0.0d0) THEN
! 
               PhTran = 'AIG->AqG'
               GO TO 1000
! 
            END IF IF_IceDis
!
!***********************************************************************
!*                                                                     *
!*      Check phase transition from Aqueous + Ice + Gas (3-phase)      *
!*     to Aqueous + Hydrate + Ice + Gas (4-phase, Quadruple point)     *
!*                                                                     *
!***********************************************************************
!
            IF_HydEv: IF(PX > P_quad_hyd) THEN
!
               PhTran = 'AIG->QuP'
               GO TO 1000
! 
            ELSE
! 
               GO TO 500 
! 
            END IF IF_HydEv
! 
         END IF IF_1stEq
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the remaining iterations - Incremented variables
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         CASE_Incr: SELECT CASE(k)
! 
! ...... Incrementing pressure 
! 
         CASE(2)                    
! 
            DELX(NLOC+1) =-DFAC*XX(1)
            PX           = XX(1) + DELX(NLOC+1)
! 
! ...... Incrementing aqueous phase saturation  
! 
         CASE(3)                     
! 
            DELX(NLOC+2) = DFAC
            S_aqu        = XX(2) + DELX(NLOC+2)
! 
! ...... Incrementing ice saturation (NK = 2) or salt mass fraction (NK = 3) 
! 
         CASE(4) 
! 
            IF(NK == 2) THEN
               X_iA         = 0.0d0
               DELX(NLOC+3) = DFAC
               S_gas        = XX(3) + DELX(NLOC+3)
            ELSE IF(NK == 3) THEN
               DELX(NLOC+3) = DFAC
               X_iA         = XX(3) + DELX(NLOC+3)
            END IF
! 
! ...... Incrementing hydrate saturation (NK = 3)
! 
         CASE(5) 
! 
            DELX(NLOC+4) = DFAC
            S_gas        = XX(4) + DELX(NLOC+4)
! 
         END SELECT CASE_Incr
!
!***********************************************************************
!*                                                                     *
!*         For augmented primary variables:                            *
!*             Determine the relevant parameters                       *
!*                                                                     *
!***********************************************************************
! 
         IF_KGT1: IF(k > 1) THEN
!
! ......... Augmented hydrate saturation
!
            S_ice = 1.0d0 - S_aqu - S_gas
! 
            TX = 1.0d-2   ! Initial temperature estimate  
!
! ......... Determine iteratively partial P, T, T-dev and CH4 solubility    
!
            IGOOD = PTXS_Paramtrs(1,PX,X_iA,TX,    &                            
     &                            Xmol_iA,Xmol_mA_old,Xmol_iA_old,T_dev,    & 
     &                            P_CH4,Psat,HK_GinH2O,X_mA,Xmol_mA)            
! 
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
!
          END IF IF_KGT1 
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... Compute thermophysical properties 
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
  500    T_k = TX + 273.15d0   ! Initial temperature estimate  
!
!***********************************************************************
!*                                                                     *
!*             P R O P E R T I E S  +  P A R A M E T E R S             *
!*                                                                     *
!***********************************************************************
!
! ...... Internal energy and density of liquid H2O 
!
         CALL UHRho_LiqH2O(TX,PX,DW,UW,HW,IGOOD)                         
!
! ----------
! ...... Determine the ice density and enthalpy
! ----------
!                                                                      
         D_ice = Ice_Density(TX,PX)
         H_ice = Ice_Enthalpy(PX,TX)
! 
! -------------
! ...... Mole fractions in the gas phase      
! -------------
! 
         Y_inG(1) = P_CH4/PX           ! CH4
         Y_inG(2) = 1.0d0 - Y_inG(1)   ! H2O
!
! ...... Compute mass fractions in the gas phase
!
         X_mG =   MW_CH4*Y_inG(1)  &
     &         /( MW_CH4*Y_inG(1) + MW_H2O*Y_inG(2) )                                                  
!
         X_wG = 1.0d0 - X_mG                                                
!
! ...... Determine the gas density and compressibility
!
         CALL Zcomp(PX,T_k,0.0d0,Y_inG,ZLiq,ZGas,D_gas,rho_L,ixx)
!                                                                      
! ...... Multicomponent gas viscosity                                                                       
!                                                                      
         V_gas = Gas_Viscosity(PX,T_k,ZGas)                          
!                                                                      
! ...... Obtain the specific enthalpy of the gas phase
!                                                                      
         CALL HU_IdealGas(0,2.7315d2,T_k,Hi_Gas,Ui_Gas)    ! Ideal enthalpy
         CALL HUS_Departure(T_k,ZGas,Hd_Gas,Ud_Gas,Sd_Gas) ! Departure enthalpy
!                                                                      
         H_gas = ( Hi_Gas - Hd_Gas )         &        ! Convert to J/Kg/K from J/kgmol/K
     &          /( Y_inG(1)*MW_CH4 + Y_inG(2)*MW_H2O )  
!                                                                      
         U_gas = ( Ui_Gas - Ud_Gas )     &            ! Convert to J/Kg/K from J/kgmol/K
     &          /( Y_inG(1)*MW_CH4 + Y_inG(2)*MW_H2O )  
!
! ...... Mass fraction of H2O in the aqueous phase
!
         X_wA = 1.0d0 - X_mA-X_iA                     
!                                                                      
! ...... Aqueous phase density (assumed equal to water)                                                                      
!                                                                      
         D_aqu = X_wA*DW + X_iA*D_Inhib + X_mA*D_gas
!                                                                      
! ...... Liquid H2O viscosity                                                                      
!                                                                      
         V_aqu = ViscLV_H2Ox(TX,D_aqu)            
!                                                                      
! ...... Obtain the heat of CH4 solution in H2O
!                                                                      
         H_Sol = HEAT_GasSolution(1,TX,Xmol_iA,HK_GinH2O,dummy)                                     
!                                                                      
! -------------
! ..... Compute relative permeability and capillary pressure
! -------------
!                                                                      
         CALL RelPerm_Hyd(S_aqu,S_gas,k_rel_aqu,k_rel_gas,dummy,nmat)                          
         CALL CapPres_Hyd(TX,S_aqu,S_gas,D_aqu,P_cap,nmat)
! 
! ----------
! ...... Compute the heat of hydrate dissociation/formation 
! ----------
! 
         H_Heat = 0.0d0      
! 
! ----------
! ...... Assign value to state index  
! ----------
! 
         IF(k == 1) elem(n)%StateIndex = 8_1
! 
! ----------
! ...... Assign values of secondary parameters - Vapor  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(1)   = S_gas
         Cell_V(n,k-1)%p_KRel(1)   = k_rel_gas
         Cell_V(n,k-1)%p_Visc(1)   = V_gas 
         Cell_V(n,k-1)%p_Dens(1)   = D_gas
         Cell_V(n,k-1)%p_Enth(1)   = H_Gas      
         Cell_V(n,k-1)%p_PCap(1)   = 0.0d0  
         Cell_V(n,k-1)%p_MasF(1,1) = X_wG
         Cell_V(n,k-1)%p_MasF(2,1) = X_mG
         Cell_V(n,k-1)%p_MasF(3,1) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Aqueous  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(2)   = S_aqu
         Cell_V(n,k-1)%p_KRel(2)   = k_rel_aqu
         Cell_V(n,k-1)%p_Visc(2)   = V_aqu
         Cell_V(n,k-1)%p_Dens(2)   = D_aqu
         Cell_V(n,k-1)%p_Enth(2)   =  X_wA*HW &
     &                              + X_mA*(H_Gas + H_Sol) &
     &                              + X_iA*H_InhSol
         Cell_V(n,k-1)%p_PCap(2)   = P_cap
         Cell_V(n,k-1)%p_MasF(1,2) = X_wA
         Cell_V(n,k-1)%p_MasF(2,2) = X_mA
         Cell_V(n,k-1)%p_MasF(3,2) = X_iA
! 
! ----------
! ...... Assign values of secondary parameters - Hydrate phase  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(3)   = 0.0d0
         Cell_V(n,k-1)%p_Dens(3)   = 0.0d0
         Cell_V(n,k-1)%p_Enth(3)   = 0.0d0
         Cell_V(n,k-1)%p_MasF(1,3) = 0.0d0
         Cell_V(n,k-1)%p_MasF(2,3) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,3) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Ice phase 
! ----------
! 
         Cell_V(n,k-1)%p_Satr(4)   = S_ice
         Cell_V(n,k-1)%p_Dens(4)   = D_ice
         Cell_V(n,k-1)%p_Enth(4)   = H_ice
         Cell_V(n,k-1)%p_MasF(1,4) = 1.0d0
         Cell_V(n,k-1)%p_MasF(2,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,4) = 0.0d0
! 
! ----------
! ...... Store the temperature 
! ----------
! 
         Cell_V(n,k-1)%TemC = TX
! 
! ----------
! ...... Store other important variables  
! ----------
! 
         IF(K == 1) THEN
! 
! ......... Store equilibrium hydration pressure at TX 
! 
            Cell_V(n,0)%p_AddD(1) = P_quad_hyd
! 
! ......... Store the hydrate temperature depression  
! 
            Cell_V(n,1)%p_AddD(1) = T_dev
! 
         END IF
! 
! ----------
! ...... Store heat of dissociation/formation 
! ----------
! 
         Cell_V(n,k-1)%p_AddD(3) = H_Heat
! 
! 
! 
      END DO DO_NumEqs
! 
! 
! 
      RETURN     ! Task completed - Exit the routine
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                         PHASE TRANSITIONS                           >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
 1000 CONTINUE
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from 3-phase (Aqueous + Gas + Ice)
! .............. to 2-phase (Hydrate + Ice)  
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
      IF_PhTran: IF(PhTran == 'AIG->IcG') THEN
!                                                                       
         IF(MOP(5) >= 3) WRITE(6,6001) elem(n)%name,S_aqu,TX
         IF(KC == 0) THEN
            WRITE(6,6101) 
            STOP                                             
         END IF
! 
! ...... Reset 2nd variable as ice saturation 
! 
         XX(2)      = S_ice + ZERO   ! 1.0d-3
         DX(NLOC+2) = XX(2) - X(NLOC+2)
! 
! ...... Reset 3nd or 4th variable as temperature 
! 
         IF(NK == 2) THEN
            XX(3)      = TX - ZERO   ! 1.0d-3
            DX(NLOC+3) = XX(3) - X(NLOC+3)
         ELSE
            XX(4)      = TX - ZERO   ! 1.0d-3
            DX(NLOC+4) = XX(4) - X(NLOC+4)
         END IF
! 
! ...... Call the two-phase (Ice+Gas) subroutine 
! 
         CALL PHS2_IcG_Eql(N,NLOC,NMAT)
! 
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from 3-phase (Aqueous + Gas + Ice)
! .............. to 2-phase (Aqueous + Hydrate)  
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! 
      ELSE IF(PhTran == 'AIG->AqG') THEN
!                                                                       
         IF(MOP(5) >= 3) WRITE(6,6002) elem(n)%name,S_ice
         IF(KC == 0) THEN
            WRITE(6,6101) 
            STOP                                             
         END IF
! 
! ...... Reset 1st variable as pressure 
! 
         XX(1)      = PX*(1.0d0-ZERO)  ! 9.999d-1
         DX(NLOC+1) = XX(1) - X(NLOC+1)
! 
! ...... Reset 2nd variable as aqueous saturation 
! 
         XX(2)      = S_aqu + ZERO   ! 1.0d-3
         DX(NLOC+2) = XX(2) - X(NLOC+2)
! 
! ...... Reset 3nd or 4th variable as temperature 
! 
         IF(NK == 2) THEN
            XX(3)      = TX + ZERO   ! 1.0d-3
            DX(NLOC+3) = XX(3) - X(NLOC+3)
         ELSE
            XX(4)      = TX + ZERO   ! 1.0d-3
            DX(NLOC+4) = XX(4) - X(NLOC+4)
         END IF
! 
! ...... Call the two-phase (Ice+Gas) subroutine 
! 
         CALL PHS2_AqG_Eql(N,NLOC,NMAT)
! 
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
!  .............. Transition from Aqueous + Gas + Ice (3-phase)       
!  .............. to Quadruple point (4-phases: Aqueous+Hydrate+Gas+Ice)      
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! 
      ELSE IF(PhTran == 'AIG->QuP') THEN
!                                                                       
         IF(MOP(5) >= 3) WRITE(6,6003) elem(n)%name,PX
         IF(KC == 0) THEN
            WRITE(6,6101) 
            STOP                                             
         END IF
! 
! ...... Reset 1st variable as gas saturation 
! 
         XX(1)      = S_gas
         DX(NLOC+1) = XX(1) - X(NLOC+1)
! 
! ...... Reset 3nd or 4th variable as ice saturation
! 
         IF(NK == 2) THEN
            XX(3)      = S_ice
            DX(NLOC+3) = XX(3) - X(NLOC+3)
         ELSE
            XX(4)      = S_ice
            DX(NLOC+4) = XX(4) - X(NLOC+4)
         END IF
! 
! ...... Call the 4-phase (Aqueous+Hydrate+Gas+Ice) routine 
! 
         CALL PHS4_QuP_Eql(N,NLOC,NMAT)
! 
      ELSE 
! 
         WRITE(6,6102)
         STOP
! 
      END IF IF_PhTran
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'PHS3_AIG_Eql      1.0   20 January   2005',6X,   &
     &         'Routine for assigning the secondary parameters ',&
     &         'for the 3-phase ',  &
     &       /,T48,'(Aqueous + Gas + Ice) system - ',&
     &         'Equilibrium hydrate reaction')
!
 6001 FORMAT(' !!!!!! "PHS3_AIG_Eql": Aqueous phase disappears at ', &
     &       'element "',A8,'" ==> S_aqu = ',1pE15.8,'  TX = ',1pE15.8)
 6002 FORMAT(' !!!!!! "PHS3_AIG_Eql": Ice phase disappears at ',   &
     &       'element "',A8,'" ==> S_ice = ',1pE15.8)
 6003 FORMAT(' !!!!!! "PHS3_AIG_Eql": Hydrate evolves at element "',A8,&   
     &       '" ==> P = ',1pE15.8)
! 
 6101 FORMAT(' !!!!!!!!!!   Routine PHS3_AIG_Eql: ERRONEOUS ',   &
     &       'DATA INITIALIZATION   ==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
 6102 FORMAT(' !!!!!!!!!!   Routine PHS3_AIG_Eql: ERRONEOUS ',   &
     &       'PHASE CHANGE   ==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of PHS3_AIG_Eql
! 
! 
      RETURN
!
      END SUBROUTINE PHS3_AIG_Eql
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE PHS3_IGH_Eql(N,NLOC,NMAT)
! 
! ... Modules to be used 
! 
      USE Universal_Param
      USE Basic_Param
      USE GenControl_Param
      USE Diffusion_Param
!
      USE H2O_Param
      USE Hydrate_Param
!
      USE Variable_Arrays
      USE Element_Arrays
      USE PFMedProp_Arrays
!
      USE H2O_Properties
      USE Hydrate_Properties
      USE RealGas_Properties
	  USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*        ROUTINE FOR COMPUTING AND ASSIGNING ALL THE SECONDARY        *
!*          VARIABLES IN THREE-PHASE (Ice+Hydrate+Gas) SYSTEMS         *
!*                                                                     *
!*                  EQUILIBRIUM HYDRATION REACTION                     *
!*                                                                     *
!*                  Version 1.0 - January 28, 2005                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!                                                                       
      IMPLICIT NONE
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(2) :: Y_inG
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: PX,TX,Psat,X_iA,X_mG,X_wG
      REAL(KIND = 8) :: D_Hydr,H_Hydr,D_ice,H_ice
! 
      REAL(KIND = 8) :: ZLiq,ZGas,D_gas,rho_L,dummy
      REAL(KIND = 8) :: Hi_Gas,Ui_Gas,Hd_Gas,Ud_Gas,Sd_Gas,H_Gas,U_Gas
! 
      REAL(KIND = 8) :: V_gas,T_k,k_rel_gas,k_rel_wx,P_cap
! 
      REAL(KIND = 8) :: S_gas,S_ice,S_Hydr,Peq_Hydr,H_Heat
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN)  :: N,NLOC,NMAT
! 
      INTEGER :: K,ixx
! 
! -------
! ... Character variables     
! -------
! 
      CHARACTER(LEN = 8) :: PhTran 
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of PHS3_IGH_Eql
!
!
      IF(First_call) THEN
         First_call = .FALSE.
         IF(MPI_RANK==0) WRITE(11,6000)
      END IF
! 
! ... Some initializations 
! 
      PhTran = '        '
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>      Main loop determining conditions and assigning properties      >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_NumEqs: DO K=1,NEQ1
!
! ----------
! ...... Initial assignments of primary variable values        
! ...... Primary variables: S_gas, S_ice, Tx        
! ----------
! 
         S_gas = XX(1)                                                      
         S_ice = XX(2)                                     
! 
         IF(NK == 2) THEN
            X_iA = 0.0d0
            TX   = XX(3)
         ELSE IF(NK == 3) THEN
            X_iA = XX(3)
            TX   = XX(4)
         END IF
! 
! ...... Ensure realistic saturation values
! 
         IF((S_gas > 1.0d0 .AND. S_ice > 1.0d0) .OR. &
     &      (S_gas < 0.0d0 .AND. S_ice < 0.0d0)) &
     &   THEN
            IGOOD = 2
            RETURN
         END IF
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the first iteration - Unincremented variables
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         IF_1stEq: IF(K == 1) THEN
! 
! -------------
! ......... Constraints on ice saturation        
! -------------
! 
            IF_SiLLim: IF (S_ice <= 0.0d0) THEN
               S_ice      = 1.0d-4
               XX(2)      = S_ice
               DX(NLOC+2) = XX(2) - X(NLOC+2)
            END IF IF_SiLLim
! 
! -------------
! ......... Constraint on inhibitor mass fraction in the aqueous phase        
! -------------
! 
            IF_SaltMassF: IF (X_iA /= 0.0d0) THEN
               X_iA       =   0.0d0
               XX(3)      =   0.0d0
               DX(NLOC+3) = - X(NLOC+3)
            END IF IF_SaltMassF
!
! -------------
! ......... The hydrate saturation
! -------------
!
            S_Hydr = 1.0d0 - S_gas - S_ice                               
!
!***********************************************************************
!*                                                                     *
!*                 Determine the equilibrium pressure                  *
!*                                                                     *
!***********************************************************************
!
            Peq_Hydr = EqPres_HydrCH4((TX+273.15d0),0.0d0,&
     &                                 F_EqOption,IGOOD)
! 
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
!
!***********************************************************************
!*                                                                     *
!*      Check phase transition from Ice + Hydrate + Gas (3-phase)      *
!*                      to Ice + Hydrate (2-phase)                     *
!*                                                                     *
!***********************************************************************
!
            IF_GasDis: IF(S_gas < 0.0d0) THEN
!
               PhTran = 'IGH->IcH'
               GO TO 1000
!
            END IF IF_GasDis
!
!***********************************************************************
!*                                                                     *
!*      Check phase transition from Ice + Hydrate + Gas (3-phase)      *
!*                        to Ice + Gas (2-phase)                       *
!*                                                                     *
!***********************************************************************
!
            IF_HydDis: IF(S_Hydr < 0.0d0) THEN
! 
               PhTran = 'IGH->IcG'
               GO TO 1000
! 
            END IF IF_HydDis
!
!***********************************************************************
!*                                                                     *
!*    Check phase transition from Aqueous + Hydrate + Gas (3-phase)    *
!*     to Aqueous + Hydrate + Gas + Ice (4-phase: Quadruple point)     *
!*                                                                     *
!***********************************************************************
!
            IF_IceEvol: IF(TX > Tc_quad_hyd) THEN
! 
               PhTran = 'IGH->QuP'
               GO TO 1000
! 
            ELSE
! 
               GO TO 500 
! 
            END IF IF_IceEvol
! 
         END IF IF_1stEq
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the remaining iterations - Incremented variables
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         CASE_Incr: SELECT CASE(K)
! 
! ...... Incrementing gas phase saturation 
! 
         CASE(2)                    
! 
            DELX(NLOC+1) = DFAC
            S_gas        = XX(1) + DELX(NLOC+1)
! 
! ...... Incrementing aqueous phase saturation  
! 
         CASE(3)                     
! 
            DELX(NLOC+2) = DFAC
            S_ice        = XX(2) + DELX(NLOC+2)
! 
! ...... Incrementing temperature (NK = 2) or salt mass fraction (NK = 3) 
! 
         CASE(4) 
! 
            IF(NK == 2) THEN
               X_iA         = 0.0d0
               DELX(NLOC+3) =-DFAC*(273.15d0 + XX(3))
               TX           = XX(3) + DELX(NLOC+3)
            ELSE IF(NK == 3) THEN
               DELX(NLOC+3) = DFAC
               X_iA         = 0.0d0
            END IF
! 
! ...... Incrementing temperature (NK = 3)
! 
         CASE(5) 
! 
            DELX(NLOC+4) =-DFAC*(273.15d0 + XX(4))
            TX           = XX(4) + DELX(NLOC+4)
! 
         END SELECT CASE_Incr
!
!***********************************************************************
!*                                                                     *
!*         For augmented primary variables:                            *
!*             Determine the relevant parameters                       *
!*                                                                     *
!***********************************************************************
! 
         IF_KGT1: IF (K > 1) THEN 
! 
            S_Hydr = 1.0d0 - S_gas - S_ice  ! The augmented hydrate saturation
!
            Peq_Hydr = EqPres_HydrCH4((TX+273.15d0),0.0d0,&
     &                                 F_EqOption,IGOOD)
! 
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
! 
         END IF IF_KGT1                                       
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... Compute thermophysical properties 
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
  500    T_k = TX + 273.15d0  ! The augmented temperature in K 
! 
! ----------
! ...... Determine the pressure in the system       
! ----------
! 
         PX = Peq_Hydr
! 
! ----------
! ...... Determine Psat: the sublimation pressure of H2O at TX       
! ----------
! 
         IGOOD = P_Sublm_H2O_S(TX,Psat)
! 
         IF(IGOOD /= 0) THEN
            CALL WARN_RF(elem(n)%name,XX,NK1,KC)
            RETURN
         END IF                                       
!
!***********************************************************************
!*                                                                     *
!*             P R O P E R T I E S  +  P A R A M E T E R S             *
!*                                                                     *
!***********************************************************************
!
! ...... Internal energy and density the solid hydrate
!
         CALL HRho_Hydrate(TX,PX,D_Hydr,H_Hydr)
!
! ----------
! ...... Determine the ice density and enthalpy
! ----------
!                                                                      
         D_ice = Ice_Density(TX,PX)
         H_ice = Ice_Enthalpy(PX,TX)
! 
! -------------
! ...... Mole fractions in the gas phase      
! -------------
! 
         Y_inG(1) = (Peq_Hydr - Psat)/PX     ! CH4
         Y_inG(2) = 1.0d0-Y_inG(1)           ! H2O
!
! ...... Compute mass fractions in the gas phase
!
         X_mG =   MW_CH4*Y_inG(1)  &
     &         /( MW_CH4*Y_inG(1) + MW_H2O*Y_inG(2) )                                                  
!
         X_wG = 1.0d0-X_mG                                                
!
! ...... Determine the gas density and compressibility
!
         CALL Zcomp(PX,T_k,0.0d0,Y_inG,ZLiq,ZGas,D_gas,rho_L,ixx)
!                                                                      
! ...... Obtain the specific enthalpy of the gas phase
!                                                                      
         CALL HU_IdealGas(0,2.7315d2,T_k,Hi_Gas,Ui_Gas)    ! Ideal enthalpy
         CALL HUS_Departure(T_k,ZGas,Hd_Gas,Ud_Gas,Sd_Gas) ! Departure enthalpy
!                                                                      
         H_gas = ( Hi_Gas - Hd_Gas )  &                 ! Convert to J/Kg/K from J/kgmol/K
     &          /( Y_inG(1)*MW_CH4 + Y_inG(2)*MW_H2O )  
!                                                                      
         U_gas = ( Ui_Gas - Ud_Gas )      &             ! Convert to J/Kg/K from J/kgmol/K
     &          /( Y_inG(1)*MW_CH4 + Y_inG(2)*MW_H2O )  
!                                                                      
! ...... Multicomponent gas viscosity                                                                       
!                                                                      
         V_gas = Gas_Viscosity(PX,T_k,ZGas)                          
!                                                                      
! ----------
! ...... Relative permeability and capillary pressure
! ----------
!                                                                      
         CALL RelPerm_Hyd(1.0d-6,S_gas,k_rel_wx,k_rel_gas,dummy,nmat)                          
         CALL CapPres_Hyd(TX,1.0d-6,S_gas,1.0d3,P_cap,nmat)
! 
! ----------
! ...... Compute the heat of hydrate dissociation/formation 
! ----------
! 
         H_Heat = HeatDiss_HydrCH4(T_k,&
     &                             F_EqOption, &  ! H-equation
     &                             IGOOD)
! 
! ----------
! ...... Assign value to state index  
! ----------
! 
         IF(k == 1) elem(n)%StateIndex = 10_1
! 
! ----------
! ...... Assign values of secondary parameters - Vapor  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(1)   = S_gas
         Cell_V(n,k-1)%p_KRel(1)   = k_rel_gas
         Cell_V(n,k-1)%p_Visc(1)   = V_gas
         Cell_V(n,k-1)%p_Dens(1)   = D_gas
         Cell_V(n,k-1)%p_Enth(1)   = H_Gas      
         Cell_V(n,k-1)%p_PCap(1)   = 0.0d0  
         Cell_V(n,k-1)%p_MasF(1,1) = X_wG
         Cell_V(n,k-1)%p_MasF(2,1) = X_mG
         Cell_V(n,k-1)%p_MasF(3,1) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Aqueous  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(2)   = 0.0d0
         Cell_V(n,k-1)%p_KRel(2)   = 0.0d0
         Cell_V(n,k-1)%p_Visc(2)   = 1.0d0
         Cell_V(n,k-1)%p_Dens(2)   = 0.0d0
         Cell_V(n,k-1)%p_Enth(2)   = 0.0d0
         Cell_V(n,k-1)%p_PCap(2)   = P_cap
         Cell_V(n,k-1)%p_MasF(1,2) = 0.0d0
         Cell_V(n,k-1)%p_MasF(2,2) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,2) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Hydrate phase  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(3)   = S_Hydr
         Cell_V(n,k-1)%p_Dens(3)   = D_Hydr
         Cell_V(n,k-1)%p_Enth(3)   = H_Hydr
         Cell_V(n,k-1)%p_MasF(1,3) = HCom%H2OMF(1)
         Cell_V(n,k-1)%p_MasF(2,3) = HCom%GasMF(1)
         Cell_V(n,k-1)%p_MasF(3,3) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Ice phase 
! ----------
! 
         Cell_V(n,k-1)%p_Satr(4)   = S_ice
         Cell_V(n,k-1)%p_Dens(4)   = D_ice
         Cell_V(n,k-1)%p_Enth(4)   = H_ice
         Cell_V(n,k-1)%p_MasF(1,4) = 1.0d0
         Cell_V(n,k-1)%p_MasF(2,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,4) = 0.0d0
! 
! ----------
! ...... Store the temperature 
! ----------
! 
         Cell_V(n,k-1)%TemC = TX
! 
! ----------
! ...... Store other important variables  
! ----------
! 
         IF(K == 1) THEN
! 
! ......... Store equilibrium hydration pressure at TX 
! 
            Cell_V(n,0)%p_AddD(1) = Peq_Hydr
! 
! ......... Store the hydrate temperature depression  
! 
            Cell_V(n,1)%p_AddD(1) = 0.0d0
! 
         END IF
! 
! ----------
! ...... Store equilibrium hydration pressure 
! ----------
! 
         Cell_V(n,k-1)%p_AddD(2) = Peq_Hydr
! 
! ----------
! ...... Store heat of dissociation/formation 
! ----------
! 
         Cell_V(n,k-1)%p_AddD(3) = H_Heat
! 
! 
! 
      END DO DO_NumEqs
! 
! 
! 
      RETURN     ! Task completed - Exit the routine
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                         PHASE TRANSITIONS                           >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
 1000 CONTINUE
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Ice+Hydrate+Gas (3-phase) 
! .............. to Ice+Hydrate (2-phase)      
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
      IF_PhTran: IF(PhTran == 'IGH->IcH') THEN
!                                                                       
         IF(MOP(5) >= 3) WRITE(6,6001) elem(n)%name,S_gas
         IF(KC == 0) THEN
            WRITE(6,6101) 
            STOP                                             
         END IF
! 
! ...... Reset 1st variable as pressure 
! 
         XX(1)      = Peq_Hydr*(1.0d0 + ZERO)
         DX(NLOC+1) = XX(1) - X(NLOC+1)
! 
! ...... Call the 2-phase (Ice+Hydrate) routine 
! 
         CALL PHS2_IcH_Eql(N,NLOC,NMAT)
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
!  .............. Transition from Ice+Hydrate+Gas (3-phase)       
!  .............. to Ice+Gas (2-phase)      
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! 
      ELSE IF(PhTran == 'IGH->IcG') THEN
!                                                                       
         IF(MOP(5) >= 3) WRITE(6,6002) elem(n)%name,S_hydr
         IF(KC == 0) THEN
            WRITE(6,6101) 
            STOP                                             
         END IF
! 
! ...... Reset 1st variable as pressure 
! 
         XX(1)      = Peq_Hydr
         DX(NLOC+1) = XX(1) - X(NLOC+1)
! 
! ...... Reset 2nd variable as ice phase saturation
! 
         XX(2)      = S_ice + ZERO   ! 1.0d-3
         DX(NLOC+2) = XX(2) - X(NLOC+2)
! 
! ...... Reset 3nd or 4th variable as temperature 
! 
         IF(NK == 2) THEN
            XX(3)      = TX + ZERO   ! 1.0d-3
            DX(NLOC+3) = XX(3) - X(NLOC+3)
         ELSE
            XX(4)      = TX + ZERO   ! 1.0d-3
            DX(NLOC+4) = XX(4) - X(NLOC+4)
         END IF
! 
! ...... Call the 2-phase (H2O+Gas) routine 
! 
         CALL PHS2_IcG_Eql(N,NLOC,NMAT)
! 
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
!  .............. Transition from Ice+Hydrate+Gas (3-phase)       
!  .............. to Quadruple point (4-phases: Aqueous+Hydrate+Gas+Ice)      
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! 
      ELSE IF(PhTran == 'IGH->QuP') THEN
!                                                                       
         IF(MOP(5) >= 3) WRITE(6,6005) elem(n)%name,TX
         IF(KC == 0) THEN
            WRITE(6,6101) 
            STOP                                             
         END IF
! 
! ...... Reset 2nd variable as aqueous phase saturation
! 
         XX(2)      = ZERO   ! 1.0d-4
         DX(NLOC+2) = XX(2) - X(NLOC+2)
! 
! ...... Reset 3nd or 4th variable as ice 
! 
         IF(NK == 2) THEN
            XX(3)      = S_ice
            DX(NLOC+3) = XX(3) - X(NLOC+3)
         ELSE
            XX(4)      = S_ice
            DX(NLOC+4) = XX(4) - X(NLOC+4)
         END IF
! 
! ...... Call the 4-phase (Aqueous+Hydrate+Gas+Ice) routine 
! 
         CALL PHS4_QuP_Eql(N,NLOC,NMAT)
! 
      ELSE 
! 
         WRITE(6,6102)
         STOP
! 
      END IF IF_PhTran
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'PHS3_IGH_Eql      1.0   28 January   2005',6X,  &
     &         'Routine for assigning the secondary parameters of the',                      &
     &       /,T48,'3-phase (Ice+Gas+Hydrate) system - ',&
     &         'Equilibrium hydrate reaction')
!
 6001 FORMAT(' !!!!!! "PHS3_IGH_Eql": Gas phase disappears at ', &
     &       'element "',A8,'" ==> S_gas = ',1pE15.8)
 6002 FORMAT(' !!!!!! "PHS3_IGH_Eql": Hydrate disappears at ',  &
     &       'element "',A8,'" ==> S_hydr = ',1pE15.8)
 6005 FORMAT(' !!!!!! "PHS3_IGH_Eql": Aqueous phase evolves at ',&
     &       'element "',A8,'" ==> TX = ',1pE15.8)
! 
 6101 FORMAT(' !!!!!!!!!!   Routine PHS3_IGH_Eql: ERRONEOUS ',  &
     &       'DATA INITIALIZATION   ==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
 6102 FORMAT(' !!!!!!!!!!   Routine PHS3_IGH_Eql: ERRONEOUS ',  &
     &       'PHASE CHANGE   ==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of PHS3_IGH_Eql
! 
! 
      RETURN
!
      END SUBROUTINE PHS3_IGH_Eql
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE PHS4_QuP_Eql(N,NLOC,NMAT)
! 
! ... Modules to be used 
! 
      USE Universal_Param
      USE Basic_Param
      USE GenControl_Param
      USE Diffusion_Param
!
      USE H2O_Param
      USE Hydrate_Param
!
      USE Variable_Arrays
      USE Element_Arrays
      USE PFMedProp_Arrays
!
      USE H2O_Properties
      USE Hydrate_Properties
      USE RealGas_Properties
	  USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*        ROUTINE FOR COMPUTING AND ASSIGNING ALL THE SECONDARY        *
!*        VARIABLES AT THE QUADRUPLE POINT (H2O+Hydrate+Gas+Ice)       *
!*                                                                     *
!*                  EQUILIBRIUM HYDRATION REACTION                     *
!*                                                                     *
!*                  Version 1.0 - January 20, 2005                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!                                                                       
      IMPLICIT NONE
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(2) :: Y_inG
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: PX,TX,S_gas,S_aqu
      REAL(KIND = 8) :: DW,UW,HW,D_Hydr,H_Hydr
      REAL(KIND = 8) :: S_ice,D_ice,H_ice
! 
      REAL(KIND = 8) :: ZLiq,ZGas,D_gas,rho_L,H_Sol,D_aqu,dummy
      REAL(KIND = 8) :: Hi_Gas,Ui_Gas,Hd_Gas,Ud_Gas,Sd_Gas,H_Gas,U_Gas
! 
      REAL(KIND = 8) :: V_aqu,V_gas,T_k
! 
      REAL(KIND = 8) :: k_rel_aqu,k_rel_gas,P_cap
! 
      REAL(KIND = 8) :: X_mG,X_wG,Xmol_mA,X_mA,X_wA
      REAL(KIND = 8) :: S_Hydr,Peq_Hydr,HK_GinH2O,H_Heat
      REAL(KIND = 8) :: T_dev,X_iA,Xmol_iA,Xmol_mA_old,Xmol_iA_old
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN)  :: N,NLOC,NMAT
! 
      INTEGER :: K,ixx
! 
      INTEGER :: HPeq_Paramtrs  ! Integer function
! 
! -------
! ... Character variables     
! -------
! 
      CHARACTER(LEN = 8) :: PhTran 
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of PHS4_QuP_Eql
!
!
      IF(First_call) THEN
         IF(MPI_RANK==0) WRITE(11,6000)
         First_call = .FALSE.
      END IF
! 
! ... Some initializations 
! 
      PhTran = '        '
! 
! -------
! ... Initialization of T-depression variables        
! -------
! 
      T_dev    = Cell_V(n,1)%p_AddD(1)  
      Xmol_mA  = 0.0d0
      Xmol_iA  = 0.0d0
! 
      Xmol_mA_old  = 0.0d0
      Xmol_iA_old  = 0.0d0
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>      Main loop determining conditions and assigning properties      >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_NumEqs: DO K=1,NEQ1
!
! ----------
! ...... Initial assignments of primary variable values        
! ...... Primary variables: S_gas, S_aqu, S_ice        
! ----------
! 
         S_gas = XX(1)                                                      
         S_aqu = XX(2)                                     
! 
         IF(NK == 2) THEN
            X_iA  = 0.0d0
            S_ice = XX(3)
         ELSE IF(NK == 3) THEN
            X_iA  = XX(3)
            S_ice = XX(4)
         END IF
! 
! ...... Ensure realistic saturation values
! 
         IF((S_ice > 1.0d0 .AND. S_aqu > 1.0d0) .OR. &
     &      (S_ice < 0.0d0 .AND. S_aqu < 0.0d0) .OR.  &
     &      (S_gas > 1.0d0 .AND. S_aqu > 1.0d0) .OR. &
     &      (S_gas < 0.0d0 .AND. S_aqu < 0.0d0) .OR. &
     &      (S_gas > 1.0d0 .AND. S_ice > 1.0d0) .OR. &
     &      (S_gas < 0.0d0 .AND. S_ice < 0.0d0) .OR. &
     &      (S_gas > 1.0d0 .AND. S_aqu > 1.0d0 .AND. S_ice > 1.0d0) .OR.   &                   
     &      (S_gas < 0.0d0 .AND. S_aqu < 0.0d0 .AND. S_ice < 0.0d0)         &                  
     &     ) THEN
            IGOOD = 2
            RETURN
         END IF
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the first iteration - Unincremented variables
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         IF_1stEq: IF(K == 1) THEN
!
! -------------
! ......... Hydrate saturation
! -------------
!
            S_Hydr = 1.0d0 - S_aqu - S_gas - S_ice
! 
! -------------
! ......... Constraint on inhibitor mass fraction in the aqueous phase        
! -------------
! 
            IF_SaltMassF: IF (X_iA < 0.0d0) THEN
               X_iA       =   0.0d0
               XX(3)      =   0.0d0
               DX(NLOC+3) = - X(NLOC+3)
            END IF IF_SaltMassF
!
! -------------
! ......... Pressure
! -------------
!
            PX = P_quad_hyd                             
!
            TX = 1.0d-2   ! Initial temperature estimate  
!
!***********************************************************************
!*                                                                     *
!* Determine iteratively the temperature shift at the quadruple point  *
!*                                                                     *
!***********************************************************************
!
            IGOOD = HPeq_Paramtrs(1,TX,X_iA,.TRUE.,     &                    ! In only
     &                            Xmol_iA,Xmol_mA_old,Xmol_iA_old,T_dev,&    ! In and out
     &                            Peq_Hydr,HK_GinH2O,Xmol_mA,X_mA)          ! Out only
! 
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
!
!***********************************************************************
!*                                                                     *
!*                    Check phase transition from                      *
!*       Aqueous + Hydrate + Ice + Gas (4-phase, Quadruple point)      *
!*                  to Aqueous + Ice + Gas (3-phase)                   *
!*                                                                     *
!***********************************************************************
!
            IF_HydDis: IF(S_Hydr < 0.0d0) THEN
! 
               PhTran = 'QuP->AIG'
               GO TO 1000
!
            END IF IF_HydDis
!
!***********************************************************************
!*                                                                     *
!*                    Check phase transition from                      *
!*       Aqueous + Hydrate + Ice + Gas (4-phase, Quadruple point)      *
!*                to Aqueous + Ice + Hydrate (3-phase)                 *
!*                                                                     *
!***********************************************************************
!
            IF_GasDis: IF(S_gas < 0.0d0) THEN
! 
               PhTran = 'QuP->AIH'
               GO TO 1000
!
            END IF IF_GasDis
!
!***********************************************************************
!*                                                                     *
!*                    Check phase transition from                      *
!*       Aqueous + Hydrate + Ice + Gas (4-phase, Quadruple point)      *
!*                  to Ice + Gas + Hydrate (3-phase)                   *
!*                                                                     *
!***********************************************************************
!
            IF_AquDis: IF(S_aqu < 0.0d0) THEN
! 
               PhTran = 'QuP->IGH'
               GO TO 1000
!
            END IF IF_AquDis
!
!***********************************************************************
!*                                                                     *
!*                    Check phase transition from                      *
!*       Aqueous + Hydrate + Ice + Gas (4-phase, Quadruple point)      *
!*                to Aqueous + Gas + Hydrate (3-phase)                 *
!*                                                                     *
!***********************************************************************
!
            IF_IceDis: IF(S_ice < 0.0d0) THEN
!
               PhTran = 'QuP->AGH'
               GO TO 1000
! 
            ELSE
! 
               GO TO 500 
! 
            END IF IF_IceDis
! 
         END IF IF_1stEq
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the remaining iterations - Incremented variables
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         CASE_Incr: SELECT CASE(k)
! 
! ...... Incrementing gas phase saturation 
! 
         CASE(2)                    
! 
            DELX(NLOC+1) = DFAC
            S_gas        = XX(1) + DELX(NLOC+1)
! 
! ...... Incrementing aqueous phase saturation  
! 
         CASE(3)                     
! 
            DELX(NLOC+2) = DFAC
            S_aqu        = XX(2) + DELX(NLOC+2)
! 
! ...... Incrementing ice saturation (NK = 2) or salt mass fraction (NK = 3) 
! 
         CASE(4) 
! 
            IF(NK == 2) THEN
               X_iA         = 0.0d0
               DELX(NLOC+3) = DFAC
               S_ice        = XX(3) + DELX(NLOC+3)
            ELSE IF(NK == 3) THEN
               DELX(NLOC+3) = DFAC
               X_iA         = XX(3) + DELX(NLOC+3)
            END IF
! 
! ...... Incrementing hydrate saturation (NK = 3)
! 
         CASE(5) 
! 
            DELX(NLOC+4) = DFAC
            S_ice        = XX(4) + DELX(NLOC+4)
! 
         END SELECT CASE_Incr
!
!***********************************************************************
!*                                                                     *
!*         For augmented primary variables:                            *
!*             Determine the relevant parameters                       *
!*                                                                     *
!***********************************************************************
! 
         IF_KGT1: IF(k > 1) THEN
!
! ......... Augmented hydrate saturation
!
            S_Hydr = 1.0d0 - S_aqu - S_gas - S_ice
!
            TX = 1.0d-2   ! Initial temperature estimate  
!
! ......... Determine iteratively the mole fractions at Peq_Hyd         
!
            IGOOD = HPeq_Paramtrs(1,TX,X_iA,.TRUE.,      &                  ! In only
     &                            Xmol_iA,Xmol_mA_old,Xmol_iA_old,T_dev,&   ! In and out
     &                            Peq_Hydr,HK_GinH2O,Xmol_mA,X_mA)         ! Out only
! 
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
!
          END IF IF_KGT1 
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... Compute thermophysical properties 
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
  500    T_k = TX + 273.15d0   ! Initial temperature estimate  
!
!***********************************************************************
!*                                                                     *
!*             P R O P E R T I E S  +  P A R A M E T E R S             *
!*                                                                     *
!***********************************************************************
!
! ...... Internal energy and density of liquid H2O 
!
         CALL UHRho_LiqH2O(TX,PX,DW,UW,HW,IGOOD)                         
!
! ...... Internal energy and density the solid hydrate
!
         CALL HRho_Hydrate(TX,PX,D_Hydr,H_Hydr)
!
! ----------
! ...... Determine the ice density and enthalpy
! ----------
!                                                                      
         D_ice = Ice_Density(TX,PX)
         H_ice = Ice_Enthalpy(PX,TX)
! 
! -------------
! ...... Mole fractions in the gas phase      
! -------------
! 
         Y_inG(1) = (PX - P_H2O_3p)/PX       ! CH4
         Y_inG(2) = 1.0d0 - Y_inG(1)         ! H2O
!
! ...... Compute mass fractions in the gas phase
!
         X_mG =   MW_CH4*Y_inG(1)  & 
     &         /( MW_CH4*Y_inG(1) + MW_H2O*Y_inG(2) )                                                  
!
         X_wG = 1.0d0 - X_mG                                                
!
! ...... Determine the gas density and compressibility
!
         CALL Zcomp(PX,T_k,0.0d0,Y_inG,ZLiq,ZGas,D_gas,rho_L,ixx)
!                                                                      
! ...... Multicomponent gas viscosity                                                                       
!                                                                      
         V_gas = Gas_Viscosity(PX,T_k,ZGas)                          
!                                                                      
! ...... Obtain the specific enthalpy of the gas phase
!                                                                      
         CALL HU_IdealGas(0,2.7315d2,T_k,Hi_Gas,Ui_Gas)    ! Ideal enthalpy
         CALL HUS_Departure(T_k,ZGas,Hd_Gas,Ud_Gas,Sd_Gas) ! Departure enthalpy
!                                                                      
         H_gas = ( Hi_Gas - Hd_Gas )         &          ! Convert to J/Kg/K from J/kgmol/K
     &          /( Y_inG(1)*MW_CH4 + Y_inG(2)*MW_H2O )  
!                                                                      
         U_gas = ( Ui_Gas - Ud_Gas )   &                ! Convert to J/Kg/K from J/kgmol/K
     &          /( Y_inG(1)*MW_CH4 + Y_inG(2)*MW_H2O )  
!
! ...... Mass fraction of H2O in the aqueous phase
!
         X_wA = 1.0d0 - X_mA-X_iA                     
!                                                                      
! ...... Aqueous phase density (assumed equal to water)                                                                      
!                                                                      
         D_aqu = X_wA*DW + X_iA*D_Inhib + X_mA*D_gas
!                                                                      
! ...... Liquid H2O viscosity                                                                      
!                                                                      
         V_aqu = ViscLV_H2Ox(TX,D_aqu)            
!                                                                      
! ...... Obtain the heat of CH4 solution in H2O
!                                                                      
         H_Sol = HEAT_GasSolution(1,TX,Xmol_iA,HK_GinH2O,dummy)                                     
!                                                                      
! -------------
! ..... Relative permeability and capillary pressure
! -------------
!                                                                      
         CALL RelPerm_Hyd(S_aqu,S_gas,k_rel_aqu,k_rel_gas,dummy,NMAT)                          
         CALL CapPres_Hyd(TX,S_aqu,S_gas,D_aqu,P_cap,NMAT)
! 
! ----------
! ...... Compute the heat of hydrate dissociation/formation 
! ----------
! 
         H_Heat = HeatDiss_HydrCH4(T_k,&
     &                             F_EqOption,&   ! H-equation
     &                             IGOOD)
! 
! ----------
! ...... Assign value to state index  
! ----------
! 
         IF(k == 1) elem(n)%StateIndex = 11_1
! 
! ----------
! ...... Assign values of secondary parameters - Vapor  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(1)   = S_gas
         Cell_V(n,k-1)%p_KRel(1)   = k_rel_gas
         Cell_V(n,k-1)%p_Visc(1)   = V_gas 
         Cell_V(n,k-1)%p_Dens(1)   = D_gas
         Cell_V(n,k-1)%p_Enth(1)   = H_Gas      
         Cell_V(n,k-1)%p_PCap(1)   = 0.0d0  
         Cell_V(n,k-1)%p_MasF(1,1) = X_wG
         Cell_V(n,k-1)%p_MasF(2,1) = X_mG
         Cell_V(n,k-1)%p_MasF(3,1) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Aqueous  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(2)   = S_aqu
         Cell_V(n,k-1)%p_KRel(2)   = k_rel_aqu
         Cell_V(n,k-1)%p_Visc(2)   = V_aqu
         Cell_V(n,k-1)%p_Dens(2)   = D_aqu
         Cell_V(n,k-1)%p_Enth(2)   =  X_wA*HW &
     &                              + X_mA*(H_Gas + H_Sol) &
     &                              + X_iA*H_InhSol
         Cell_V(n,k-1)%p_PCap(2)   = P_cap
         Cell_V(n,k-1)%p_MasF(1,2) = X_wA
         Cell_V(n,k-1)%p_MasF(2,2) = X_mA
         Cell_V(n,k-1)%p_MasF(3,2) = X_iA
! 
! ----------
! ...... Assign values of secondary parameters - Hydrate phase  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(3)   = S_Hydr
         Cell_V(n,k-1)%p_Dens(3)   = D_Hydr
         Cell_V(n,k-1)%p_Enth(3)   = H_Hydr
         Cell_V(n,k-1)%p_MasF(1,3) = HCom%H2OMF(1)
         Cell_V(n,k-1)%p_MasF(2,3) = HCom%GasMF(1)
         Cell_V(n,k-1)%p_MasF(3,3) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Ice phase 
! ----------
! 
         Cell_V(n,k-1)%p_Satr(4)   = S_ice
         Cell_V(n,k-1)%p_Dens(4)   = D_ice
         Cell_V(n,k-1)%p_Enth(4)   = H_ice
         Cell_V(n,k-1)%p_MasF(1,4) = 1.0d0
         Cell_V(n,k-1)%p_MasF(2,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,4) = 0.0d0
! 
! ----------
! ...... Store the temperature 
! ----------
! 
         Cell_V(n,k-1)%TemC = TX
! 
! ----------
! ...... Store other important variables  
! ----------
! 
         IF(K == 1) THEN
! 
! ......... Store equilibrium hydration pressure at TX 
! 
            Cell_V(n,0)%p_AddD(1) = P_quad_hyd
! 
! ......... Store the hydrate temperature depression  
! 
            Cell_V(n,1)%p_AddD(1) = T_dev
! 
         END IF
! 
! ----------
! ...... Store equilibrium hydration pressure 
! ----------
! 
         Cell_V(n,k-1)%p_AddD(2) = P_quad_hyd
! 
! ----------
! ...... Store heat of dissociation/formation 
! ----------
! 
         Cell_V(n,k-1)%p_AddD(3) = H_Heat
! 
! 
! 
      END DO DO_NumEqs
! 
! 
! 
      RETURN     ! Task completed - Exit the routine
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                         PHASE TRANSITIONS                           >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
 1000 CONTINUE
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Quadruple point (4-phase) 
! .............. to Aqueous + Hydrate + Gas (3-phase)      
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
      IF_PhTran: IF(PhTran == 'QuP->AGH') THEN
!                                                                       
         IF(MOP(5) >= 3) WRITE(6,6001) elem(n)%name,S_ice
         IF(KC == 0) THEN
            WRITE(6,6101) 
            STOP                                             
         END IF
! 
! ...... Reset 2nd variable as aqueous phase saturation
! 
         XX(2)      = S_aqu + ZERO  
         DX(NLOC+2) = XX(2) - X(NLOC+2)
! 
! ...... Reset 3nd or 4th variable as temperature 
! 
         IF(NK == 2) THEN
            XX(3)      = TX + ZERO  
            DX(NLOC+3) = XX(3) - X(NLOC+3)
         ELSE
            XX(4)      = TX + ZERO  
            DX(NLOC+4) = XX(4) - X(NLOC+4)
         END IF
! 
! ...... Call the 3-phase (L-H2O + Hydrate + Gas) routine 
! 
         CALL PHS3_AGH_Eql(N,NLOC,NMAT)
! 
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Quadruple point (4-phase) 
! .............. to Aqueous + Gas + Ice (3-phase)      
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! 
      ELSE IF(PhTran == 'QuP->AIG') THEN
!                                                                       
         IF(MOP(5) >= 3) WRITE(6,6004) elem(n)%name,S_Hydr
         IF(KC == 0) THEN
            WRITE(6,6101) 
            STOP                                             
         END IF
! 
! ...... Reset 1st variable as pressure
! 
         XX(1)      = PX*(1.0d0 - ZERO)
         DX(NLOC+1) = XX(1) - X(NLOC+1)
! 
! ...... Reset 3nd or 4th variable as gas saturation 
! 
         IF(NK == 2) THEN
            XX(3)      = S_gas
            DX(NLOC+3) = XX(3) - X(NLOC+3)
         ELSE
            XX(4)      = S_gas
            DX(NLOC+4) = XX(4) - X(NLOC+4)
         END IF
! 
! ...... Call the 3-phase (I-H2O+Hydrate+Gas) routine 
! 
         CALL PHS3_AIG_Eql(N,NLOC,NMAT)
! 
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Quadruple point (4-phase) 
! .............. to Aqueous + Hydrate + Ice (3-phase)      
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! 
      ELSE IF(PhTran == 'QuP->AIH') THEN
!                                                                       
         IF(MOP(5) >= 3) WRITE(6,6003) elem(n)%name,S_gas
         IF(KC == 0) THEN
            WRITE(6,6101) 
            STOP                                             
         END IF
! 
! ...... Reset 1st variable as ice saturation
! 
         XX(1)      = S_ice
         DX(NLOC+1) = XX(1) - X(NLOC+1)
! 
! ...... Reset 3nd or 4th variable as temperature 
! 
         IF(NK == 2) THEN
            XX(3)      = TX
            DX(NLOC+3) = XX(3) - X(NLOC+3)
         ELSE
            XX(4)      = TX
            DX(NLOC+4) = XX(4) - X(NLOC+4)
         END IF
! 
! ...... Call the 3-phase (Aqueous+Hydrate+Gas) routine 
! 
         CALL PHS3_AIH_Eql(N,NLOC,NMAT)
! 
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Quadruple point (4-phase) 
! .............. to Aqueous + Hydrate + Gas (3-phase)      
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! 
      ELSE IF(PhTran == 'QuP->IGH') THEN
!                                                                       
         IF(MOP(5) >= 3) WRITE(6,6002) elem(n)%name,S_aqu
         IF(KC == 0) THEN
            WRITE(6,6101) 
            STOP                                             
         END IF
! 
! ...... Reset 2nd variable as ice saturation
! 
         XX(2)      = S_ice
         DX(NLOC+2) = XX(2) - X(NLOC+2)
! 
! ...... Reset 3nd or 4th variable as temperature 
! 
         IF(NK == 2) THEN
            XX(3)      = TX
            DX(NLOC+3) = XX(3) - X(NLOC+3)
         ELSE
            XX(4)      = TX
            DX(NLOC+4) = XX(4) - X(NLOC+4)
         END IF
! 
! ...... Call the 3-phase (Aqueous+Hydrate+Gas) routine 
! 
         CALL PHS3_IGH_Eql(N,NLOC,NMAT)
! 
      ELSE 
! 
         WRITE(6,6102)
         STOP
! 
      END IF IF_PhTran
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'PHS4_QuP_Eql      1.0   20 January   2005',6X,  &
     &         'Routine for assigning the secondary parameters ',&
     &         'at the hydrate quadruple point ',    &
     &       /,T48,'(Aqueous + Gas + Hydrate + Ice) - ',&
     &         'Equilibrium hydrate reaction')
!
 6001 FORMAT(' !!!!!! "PHS4_QuP_Eql": Ice disappears at element "',A8,&
     &       '" ==> S_ice = ',1pE15.8)
 6002 FORMAT(' !!!!!! "PHS4_QuP_Eql": Aqueous phase disappears at ',&
     &       'element "',A8,'" ==> S_aqu = ',1pE15.8)
 6003 FORMAT(' !!!!!! "PHS4_QuP_Eql": Gas phase disappears at ',&
     &       'element "',A8,'" ==> S_gas = ',1pE15.8)
 6004 FORMAT(' !!!!!! "PHS4_QuP_Eql": Hydrate disappears at ',&
     &       'element "',A8,'" ==> S_Hydr = ',1pE15.8)
! 
 6101 FORMAT(' !!!!!!!!!!   Routine PHS4_QuP_Eql: ERRONEOUS ', & 
     &       'DATA INITIALIZATION   ==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
 6102 FORMAT(' !!!!!!!!!!   Routine PHS4_QuP_Eql: ERRONEOUS ',  & 
     &       'PHASE CHANGE   ==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of PHS4_QuP_Eql
! 
! 
      RETURN
!
      END SUBROUTINE PHS4_QuP_Eql
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      INTEGER FUNCTION HPeq_Paramtrs(I_Flag,T_c,X_iA,T_determine,&     ! In only
     &                      Xmol_iA,Xmol_mA_old,Xmol_iA_old,T_dev,&    ! In and out
     &                      Peq_Hydr,HK_GinH2O,Xmol_mA,X_mA)          ! Out only
! 
! ... Modules to be used 
! 
      USE Basic_Param,      ONLY: NK
      USE GenControl_Param, ONLY: MOP
! 
      USE H2O_Param
      USE Hydrate_Param
!
      USE Hydrate_Properties
      USE RealGas_Properties
	  USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*         ROUTINE FOR DETERMINING THE EQUILIBRIUM HYDRATION           *
!*     PRESSURE AND THE CORRESPONDING MOLE FRACTIONS AND T-SHIFT       *
!*                                                                     *
!*                   Version 1.0 - January 18, 2005                    *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!                                                                       
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN)    :: X_iA
      REAL(KIND = 8), INTENT(INOUT) :: T_c,Xmol_iA,Xmol_mA_old,&
     &                                 Xmol_iA_old,T_dev
      REAL(KIND = 8), INTENT(OUT)   :: Peq_Hydr,HK_GinH2O,Xmol_mA,X_mA
! 
      REAL(KIND = 8) :: T_c0,Xm_Dif,Xi_Dif
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: I_Flag
! 
      INTEGER :: IGOOD
! 
      INTEGER :: n_it,NK_index
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL, INTENT(IN) :: T_determine 
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of HPeq_Paramtrs
!
!
      IF(First_call) THEN
         First_call = .FALSE.
         IF(MPI_RANK==0) WRITE(11,6000)
      END IF
! 
! -------
! ... Select options       
! -------
! 
      IF(I_Flag > 0) THEN
         NK_index = 2
      ELSE
         NK_index = 3
      END IF
! 
      T_c0 = T_c
! 
! 
! 
      SELECT CASE(ABS(I_Flag))
!
!
!***********************************************************************
!*                                                                     *
!*       Option #1: Determine the Peq_Hyd and corresponding data       *
!*                                                                     *
!***********************************************************************
!
!
      CASE(1)
!
         DO_MoleF1: DO n_it = 1,10
! 
! ......... Determine Peq_Hydr: the equilibrium hydration pressure at TX       
! 
            Peq_Hydr = EqPres_HydrCH4((T_c+273.15d0),T_dev,&
     &                                 F_EqOption,IGOOD)
!
            IF(IGOOD /= 0) THEN
               HPeq_Paramtrs = 2
               RETURN
            END IF                                       
! 
! ......... Determine mole fraction of dissolved CH4 in H2O       
! 
            HK_GinH2O = HenryS_K(1,T_c,Xmol_iA,.TRUE.)
            Xmol_mA   = Peq_Hydr/HK_GinH2O
! 
! ......... Determine mass fraction of dissolved CH4 in H2O       
! 
            X_mA =  ( Xmol_mA*MW_CH4 )    &                   
     &             /( Xmol_iA*MW_Inhib + Xmol_mA*MW_CH4 &
     &               + ( 1.0d0 - Xmol_iA - Xmol_mA )*MW_H2O )
! 
! ......... Exit the loop if salt is not considered       
! 
            IF(NK == NK_index) THEN
               HPeq_Paramtrs = 0
               RETURN
            END IF
! 
! ......... Determine mole fraction of dissolved inhbitor in H2O       
! 
            Xmol_iA =  ( X_iA/MW_Inhib )  &                      
     &                /( X_iA/MW_Inhib + X_mA/MW_CH4 &
     &                  + ( 1.0d0 - X_iA - X_mA )/MW_H2O )
! 
! ......... Determine the salt-induced suppression of the hydration T   
! 
            T_dev = T_MaxOff*DLOG(1.0d0-Xmol_iA)&
     &                      /DLOG(1.0d0-C_MaxOff+1.0d-10)            
!
            IF(T_determine .EQV. .TRUE.) T_c = T_c0 - T_dev           
!
            Xm_Dif = Xmol_mA - Xmol_mA_old
            Xi_Dif = Xmol_iA - Xmol_iA_old
! 
! ......... Test the convergence   
! 
            IF(MAX(ABS(Xm_Dif/(Xmol_mA+1.0d-20)),& 
     &             ABS(Xi_Dif/(Xmol_iA+1.0d-20))) < 1.0d-9)  THEN
               Xmol_mA_old   = Xmol_mA
               Xmol_iA_old   = Xmol_iA
               HPeq_Paramtrs = 0
               RETURN                       ! Successful convergence; mission accomplished
            ELSE
               Xmol_mA_old  = Xmol_mA
               Xmol_iA_old  = Xmol_iA
            END IF
!
         END DO DO_MoleF1
! 
! -------------
! ...... Upon unsuccessful convergence, set the function value     
! -------------
! 
         HPeq_Paramtrs = 2
! 
         RETURN             ! Exit the routine
!
!
!***********************************************************************
!*                                                                     *
!*       Option #2: Determine the Peq_Hyd and corresponding data       *
!*                  when checking Aqueous + Gas systems                *
!*                                                                     *
!***********************************************************************
!
!
      CASE(2)
!
         DO_MoleF2: DO n_it = 1,10
! 
! ......... Determine Peq_Hydr: the equilibrium hydration pressure at TX       
! 
            Peq_Hydr = EqPres_HydrCH4((T_c+273.15d0),T_dev,&
     &                                 F_EqOption,IGOOD)
            IF(IGOOD /= 0) THEN
               HPeq_Paramtrs = 2
               RETURN
            END IF                                       
! 
! ......... Exit the loop if salt is not considered       
! 
            IF(NK == NK_index) THEN
               HPeq_Paramtrs = 0
               RETURN
            END IF
! 
! ......... Determine mole fraction of dissolved CH4 in H2O       
! 
            HK_GinH2O = HenryS_K(1,T_c,Xmol_iA,.TRUE.)
            Xmol_mA   = Peq_Hydr/HK_GinH2O
! 
! ......... Determine mass fraction of dissolved CH4 in H2O       
! 
            X_mA =  ( Xmol_mA*MW_CH4 )                      &   
     &             /( Xmol_iA*MW_Inhib + Xmol_mA*MW_CH4 &
     &               + ( 1.0d0 - Xmol_iA - Xmol_mA )*MW_H2O )
! 
! ......... Determine mole fraction of dissolved inhbitor in H2O       
! 
            Xmol_iA =  ( X_iA/MW_Inhib ) &                        
     &                /( X_iA/MW_Inhib + X_mA/MW_CH4 &
     &                  + ( 1.0d0 - X_iA - X_mA )/MW_H2O )
! 
! ......... Determine the salt-induced suppression of the hydration T   
! 
            T_dev = T_MaxOff*DLOG(1.0d0-Xmol_iA)&
     &                      /DLOG(1.0d0-C_MaxOff+1.0d-10)            
!
            IF(T_determine .EQV. .TRUE.) T_c = T_c0 - T_dev           
!
            Xm_Dif = Xmol_mA - Xmol_mA_old
            Xi_Dif = Xmol_iA - Xmol_iA_old
! 
! ......... Test the convergence   
! 
            IF(MAX(ABS(Xm_Dif/(Xmol_mA+1.0d-20)),& 
     &             ABS(Xi_Dif/(Xmol_iA+1.0d-20))) < 1.0d-9)  THEN
               Xmol_mA_old   = Xmol_mA
               Xmol_iA_old   = Xmol_iA
               HPeq_Paramtrs = 0
               RETURN                       ! Successful convergence; mission accomplished
            ELSE
               Xmol_mA_old  = Xmol_mA
               Xmol_iA_old  = Xmol_iA
            END IF
!
         END DO DO_MoleF2
! 
! -------------
! ...... Upon unsuccessful convergence, set the function value     
! -------------
! 
         HPeq_Paramtrs = 2
! 
         RETURN             ! Exit the routine
! 
! 
! 
      END SELECT
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'HPeq_Paramtrs     1.0   18 January   2005',6X,   &
     &         'Determine the hydration equilibrium pressure, ',  & 
     &         'dissolved mass/mole fractions and T-shift') 
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of HPeq_Paramtrs
!
!
      RETURN
!
      END FUNCTION HPeq_Paramtrs                                                            
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      INTEGER FUNCTION PCH4_Paramtrs(I_Flag,P_CH4,T_c,X_iA,        &      ! In only
     &                      Xmol_iA,Xmol_mA_old,Xmol_iA_old,T_dev,  &     ! In and out 
     &                      HK_GinH2O,X_mA,Xmol_mA)                      ! Out only
! 
! ... Modules to be used 
! 
      USE Basic_Param,      ONLY: NK
      USE GenControl_Param, ONLY: MOP
! 
      USE H2O_Param
      USE Hydrate_Param
!
      USE Hydrate_Properties
      USE RealGas_Properties
	  USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*       ROUTINE FOR DETERMINING THE MOLE FRACTIONS AND T_SHIFT        *
!*                AT THE METHANE PARTIAL PRESSURE P_CH4                *
!*                                                                     *
!*                   Version 1.0 - January 18, 2005                    *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!                                                                       
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN)    :: P_CH4,T_c,X_iA
      REAL(KIND = 8), INTENT(INOUT) :: Xmol_iA,Xmol_mA_old,&
     &                                 Xmol_iA_old,T_dev
      REAL(KIND = 8), INTENT(OUT)   :: HK_GinH2O,Xmol_mA,X_mA
! 
      REAL(KIND = 8) :: Xm_Dif,Xi_Dif
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: I_Flag
! 
      INTEGER :: n_it,NK_index
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of PCH4_Paramtrs
!
!
      IF(First_call) THEN
         First_call = .FALSE.
         IF(MPI_RANK==0) WRITE(11,6000)
      END IF
! 
! 
! 
      IF(I_Flag > 0) THEN
         NK_index = 2
      ELSE
         NK_index = 3
      END IF
!
!
!***********************************************************************
!*                                                                     *
!*       Option #1: Determine the Peq_Hyd and corresponding data       *
!*                                                                     *
!***********************************************************************
!
!
      DO_MoleF1: DO n_it = 1,10
! 
! ...... Determine mole fraction of dissolved CH4 in H2O       
! 
         HK_GinH2O = HenryS_K(1,T_c,Xmol_iA,.TRUE.)
         Xmol_mA   = P_CH4/HK_GinH2O
! 
! ...... Determine mass fraction of dissolved CH4 in H2O       
! 
         X_mA =  ( Xmol_mA*MW_CH4 )  &                      
     &          /( Xmol_iA*MW_Inhib + Xmol_mA*MW_CH4 &
     &            + ( 1.0d0 - Xmol_iA - Xmol_mA )*MW_H2O )
! 
! ...... Exit the loop if salt is not considered       
! 
         IF(NK == NK_index) THEN
            PCH4_Paramtrs = 0
            RETURN
         END IF
! 
! ...... Determine mole fraction of dissolved inhbitor in H2O       
! 
         Xmol_iA =  ( X_iA/MW_Inhib )                 &     
     &             /( X_iA/MW_Inhib + X_mA/MW_CH4 &
     &               + ( 1.0d0 - X_iA - X_mA )/MW_H2O )
! 
! ...... Determine the salt-induced suppression of the hydration T   
! 
         T_dev = T_MaxOff*DLOG(1.0d0-Xmol_iA)&
     &                   /DLOG(1.0d0-C_MaxOff+1.0d-10)            
!
         Xm_Dif = Xmol_mA - Xmol_mA_old
         Xi_Dif = Xmol_iA - Xmol_iA_old
! 
! ...... Test the convergence   
! 
         IF(MAX(ABS(Xm_Dif/(Xmol_mA+1.0d-16)),& 
     &          ABS(Xi_Dif/(Xmol_iA+1.0d-16))) < 1.0d-12)  THEN
            Xmol_mA_old   = Xmol_mA
            Xmol_iA_old   = Xmol_iA
            PCH4_Paramtrs = 0
            RETURN                       ! Successful convergence; mission accomplished
         ELSE
            Xmol_mA_old  = Xmol_mA
            Xmol_iA_old  = Xmol_iA
         END IF
!
      END DO DO_MoleF1
! 
! ----------
! ... Upon unsuccessful convergence, set the function value     
! ----------
! 
      PCH4_Paramtrs = 2
! 
      RETURN             ! Exit the routine
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'PCH4_Paramtrs     1.0   18 January   2005',6X,  & 
     &         'Determine dissolved mass/mole fractions and T-shift ',  &
     &         'at the partial pressure P_CH4') 
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of PCH4_Paramtrs
!
!
      RETURN
!
      END FUNCTION PCH4_Paramtrs                                                            
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      INTEGER FUNCTION PTXS_Paramtrs(I_Flag,PX,X_iA,   &                    ! In only
     &                      T_c,Xmol_iA,Xmol_mA_old,Xmol_iA_old,T_dev, &    ! In and out 
     &                      P_CH4,Psat,HK_GinH2O,X_mA,Xmol_mA)             ! Out only
! 
! ... Modules to be used 
! 
      USE Basic_Param,      ONLY: NK
      USE GenControl_Param, ONLY: MOP
! 
      USE H2O_Param
      USE Hydrate_Param
!
      USE H2O_Properties
      USE Hydrate_Properties
      USE RealGas_Properties
	  USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*       ROUTINE FOR DETERMINING H2O AND CH4 PARTIAL PRESSURES,        *
!*       TEMPERATURES & SOLUBILITIES IN A GAS+ICE+L-H2O SYSTEM         *
!*                                                                     *
!*                   Version 1.0 - January 21, 2005                    *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!                                                                       
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN)    :: PX,X_iA
      REAL(KIND = 8), INTENT(INOUT) :: T_c,Xmol_iA,Xmol_mA_old,&
     &                                 Xmol_iA_old,T_dev
      REAL(KIND = 8), INTENT(OUT)   :: P_CH4,Psat,HK_GinH2O,Xmol_mA,X_mA
! 
      REAL(KIND = 8) :: T_c0,Xm_Dif,Xi_Dif
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: I_Flag
! 
      INTEGER :: n_it,NK_index
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of PCH4_Paramtrs
!
!
      IF(First_call) THEN
         First_call = .FALSE.
         IF(MPI_RANK==0) WRITE(11,6000)
      END IF
! 
! 
! 
      IF(I_Flag > 0) THEN
         NK_index = 2
      ELSE
         NK_index = 3
      END IF
!
      T_c0 = T_c
!
!
!***********************************************************************
!*                                                                     *
!*       Option #1: Determine the Peq_Hyd and corresponding data       *
!*                                                                     *
!***********************************************************************
!
!
      DO_MoleF1: DO n_it = 1,10
!
! -------------
! ...... Vapor pressure
! -------------
!
         IF(T_c <= 1.0d-2) THEN
            PTXS_Paramtrs = P_Sat_H2O_S(T_c,Psat)
         ELSE
            PTXS_Paramtrs = P_Sublm_H2O_S(T_c,Psat)
         END IF
! 
         IF(PTXS_Paramtrs /= 0) THEN
            RETURN
         END IF                                       
!                                                                       
!....... Determine the CH4 partial pressure
!                                                                       
         P_CH4 = PX - Psat
! 
! ...... Determine mole fraction of dissolved CH4 in H2O       
! 
         HK_GinH2O = HenryS_K(1,T_c,Xmol_iA,.TRUE.)
         Xmol_mA   = P_CH4/HK_GinH2O
! 
! ...... Determine mass fraction of dissolved CH4 in H2O       
! 
         X_mA =  ( Xmol_mA*MW_CH4 )   &                      
     &          /( Xmol_iA*MW_Inhib + Xmol_mA*MW_CH4 &
     &            + ( 1.0d0 - Xmol_iA - Xmol_mA )*MW_H2O )
! 
! ...... Exit the loop if salt is not considered       
! 
         IF(NK == NK_index) THEN
            PTXS_Paramtrs = 0
            RETURN
         END IF
! 
! ...... Determine mole fraction of dissolved inhbitor in H2O       
! 
         Xmol_iA =  ( X_iA/MW_Inhib )                 &        
     &             /( X_iA/MW_Inhib + X_mA/MW_CH4 &
     &               + ( 1.0d0 - X_iA - X_mA )/MW_H2O )
! 
! ...... Determine the salt-induced suppression of the hydration T   
! 
         T_dev = T_MaxOff*DLOG(1.0d0-Xmol_iA)&
     &                   /DLOG(1.0d0-C_MaxOff+1.0d-10)            
!
         T_c = T_c0 - T_dev            
!
         Xm_Dif = Xmol_mA - Xmol_mA_old
         Xi_Dif = Xmol_iA - Xmol_iA_old
! 
! ...... Test the convergence   
! 
         IF(MAX(ABS(Xm_Dif/(Xmol_mA+1.0d-16)),& 
     &      ABS(Xi_Dif/(Xmol_iA+1.0d-16))) < 1.0d-12)  THEN
            Xmol_mA_old   = Xmol_mA
            Xmol_iA_old   = Xmol_iA
            PTXS_Paramtrs = 0
            RETURN                       ! Successful convergence; mission accomplished
         ELSE
            Xmol_mA_old  = Xmol_mA
            Xmol_iA_old  = Xmol_iA
         END IF
!
      END DO DO_MoleF1
! 
! ----------
! ... Upon unsuccessful convergence, set the function value     
! ----------
! 
      PTXS_Paramtrs = 2
! 
      RETURN             ! Exit the routine
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'PTXS_Paramtrs     1.0   21 January   2005',6X,   &
     &         'Determine partial P, T and CH4 ',   &
     &         'solubilities in a Gas + Ice + L-H2O system') 
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of PTXS_Paramtrs
!
!
      RETURN
!
      END FUNCTION PTXS_Paramtrs                                                            
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE EOS_Kinetic
! 
! ... Modules to be used 
! 
      USE Universal_Param
      USE Basic_Param
      USE GenControl_Param
!
      USE Variable_Arrays
      USE Element_Arrays
      USE PFMedProp_Arrays
	  
	  USE MPI_PARAM
	  USE MPI_ARRAYS
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*     DETERMINES PHASE TRANSITIONS AND COMPUTES ALL THERMOPHYSICAL    *     
!*       PROPERTIES OF THREE COMPONENTS (Water, Air, HYDRATE IN 1,     *     
!*         2 OR 3 PHASES.  THE AQUEOUS PHASE DOES NOT DISAPPEAR        *     
!*              Kinetic hydrate dissociation or formation              *     
!*                                                                     *
!*                  Version 1.0 - September 29, 2004                   *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: XINCR
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: N,M,NLOC,N1,NMAT,IERR
! 
      INTEGER(KIND = 1) :: State_X
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of EOS_Kinetic
!
!
      IF(First_call) THEN
         First_call = .FALSE.
         IF(MPI_RANK==0) WRITE(11,6000)
      END IF
!                                                                       
!                                                                       
      IF (MOP(5) >= 7 .and. MPI_RANK==0) WRITE(6,6001) KCYC,ITER
! 
!***********************************************************************
!*                                                                     *
!*                      SET UP INITIAL CONDITIONS                      *
!*                                                                     *
!***********************************************************************
! 
! 
      IF (MOP(5) >= 7 .and. MPI_RANK==0) WRITE(6,6101)                                     
! 
      IF (KC == 0) CALL EOS_InitAssignKin
! 
      IF (MOP(5) >= 7 .and. MPI_RANK==0) WRITE(6,6101)                                     
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>      Main loop determining conditions and assigning properties      >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      IF_N1Val: IF(KC > 0) THEN
                   N1 = NELAL
                ELSE
                   N1 = NELL
                END IF IF_N1Val  
! 
!***********************************************************************
!*                                                                     *
!*        Set updated primary variables after last NR-iteration        *
!*                                                                     *
!***********************************************************************
! 
      DO_NumEle: DO n=1,N1                                                       
!
         NMAT = elem(n)%MatNo 
         NLOC = (N-1)*NK1        
		 
! 
! ----------
! ...... Regular variable update in active gridblocks 
! ----------
! 
         DO_UpdatePV: DO m=1,NK1
! 
            XINCR = 0.0d0                                                      
            IF(ITER /= 0) XINCR = DX(NLOC+m)                               
            XX(m) = X(NLOC+m)+XINCR                                         
! 
         END DO DO_UpdatePV   
! 
! 
! >>>>>>>>>>>>>>>>>>
! .....
! ..... Choice of phase conditions: "Normal" vs. time step reduction 
! .....
! >>>>>>>>>>>>>>>>>>
! 
! 
         IF_Ihaf_NE_0: IF(IHALVE /= 0 .AND. ITER == 0) THEN                                       
            State_X = elem(n)%StatePoint
         ELSE
            State_X = elem(n)%StateIndex
         END IF IF_Ihaf_NE_0
! 
! ...... Ensure realistic pressures
! 
         IF ( ( ABS(XX(1)) > 1.0d8 ) .OR.  &
     &        ( State_X /= 7_1 .AND. State_X /= 10_1 .AND. &
     &          State_X /= 11_1 .AND. XX(1) < 0.0d0 ) .OR.  &
     &        ( XX(1) /= XX(1) ) ) IGOOD = 5                                 
!                                                                       
         IF (MOP(5) >= 7) WRITE(6,6102) elem(n)%name,(XX(M),M=1,NK1)
!                                                                       
         IF_IGood: IF(IGOOD /= 0) THEN
            CALL WARN_RF(elem(n)%name,XX,NK1,KC)
            RETURN
         END IF IF_IGood                                      
! 
! 
! >>>>>>>>>>>>>>>>>>
! .....
! ..... Choice of phase conditions 
! .....
! >>>>>>>>>>>>>>>>>>
! 
! 
         Case_State: SELECT CASE(State_X)
! 
! ---------- 
! ...... Single phase: Aqueous           
! ---------- 
!    
         CASE( 2_1)
!    
            CALL PHS1_Aqu_Kin(N,NLOC,NMAT,.FALSE.)
!    
			
            IF(IGOOD /= 0) THEN
              RETURN
            ELSE                                      
               CYCLE DO_NumEle  
            END IF                                       
! 
! ---------- 
! ...... Two phases: Aqueous+Gas   
! ---------- 
!    
         CASE( 3_1)
!    
            CALL PHS2_AqG_Kin(N,NLOC,NMAT,.FALSE.)
!    
            IF(IGOOD /= 0) THEN
               RETURN
            ELSE                                      
               CYCLE DO_NumEle  
            END IF                                       
! 
! ---------- 
! ...... Two phases: Aqueous+Hydrate    
! ---------- 
!    
         CASE( 5_1)
!    
            CALL PHS2_AqH_Kin(N,NLOC,NMAT,.FALSE.)
!    
            IF(IGOOD /= 0) THEN
               RETURN
            ELSE                                      
               CYCLE DO_NumEle  
            END IF                                       
! 
! ---------- 
! ...... Three phases: Aqueous+Hydrate+Gas    
! ---------- 
!    
         CASE( 7_1)
!    
            CALL PHS3_AGH_Kin(N,NLOC,NMAT,.FALSE.)
!    
            IF(IGOOD /= 0) THEN
               RETURN
            ELSE                                      
               CYCLE DO_NumEle  
            END IF                                       
! 
! ---------- 
! ...... Three phases: Aqueous+Ice+Gas    
! ---------- 
!    
         CASE( 8_1)
!    
            CALL PHS3_AIG_Kin(N,NLOC,NMAT,.FALSE.)
!    
            IF(IGOOD /= 0) THEN
               RETURN
            ELSE                                      
               CYCLE DO_NumEle  
            END IF                                       
! 
! ---------- 
! ...... Quadruple point: Aqueous+Hydrate+Gas+Ice    
! ---------- 
!    
         CASE(11_1)
!    
           CALL PHS4_QuP_Kin(N,NLOC,NMAT,.FALSE.)
!    
            IF(IGOOD /= 0) THEN
               RETURN
            ELSE                                      
               CYCLE DO_NumEle  
            END IF                                       
!    
         END SELECT Case_State
!                                                                    
      END DO DO_NumEle   
! 
! 
!***********************************************************************
!*                                                                     *
!*           PRINT SECONDARY PARAMETERS (When MOP(5) >= 8)             *
!*                                                                     *
!***********************************************************************
! 
! 
      IF(MOP(5) >= 8) CALL PRINT_SecPar                                                       
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'EOS_Kinetic       1.0   29 September 2004',6X,   &
     &         'Thermophysical properties of the Water+CH4+',   &
     &         'Hydrate system - Kinetic reaction') 
!                                                                       
 6001 FORMAT(/,' %%%%%%%%%% E O S %%%%%%%%%% E O S %%%%%%%%%%',  &  
     &          '  [KCYC,ITER] = [',I4,',',I3,']'/) 
!                                                                       
 6101 FORMAT(/,' PRIMARY VARIABLES',/)                                    
 6102 FORMAT(' AT ELEMENT *',A8,'* --- ',(4(2X,1pE20.13)))              
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of EOS_Kinetic
!
!
      RETURN
!
      END SUBROUTINE EOS_Kinetic                                                            
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE EOS_InitAssignKin
! 
! ... Modules to be used 
! 
      USE Universal_Param
      USE Basic_Param
      USE GenControl_Param
!
      USE H2O_Param
      USE Hydrate_Param
!
      USE Variable_Arrays
      USE Element_Arrays
      USE PFMedProp_Arrays
!
      USE H2O_Properties
      USE Hydrate_Properties
      USE RealGas_Properties
	  
	  USE MPI_PARAM
	  USE MPI_ARRAYS
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*     DETERMINES PHASE TRANSITIONS AND COMPUTES ALL THERMOPHYSICAL    *     
!*       PROPERTIES OF THREE COMPONENTS (Water, Air, HYDRATE IN 1,     *     
!*         2 OR 3 PHASES.  THE AQUEOUS PHASE DOES NOT DISAPPEAR        *     
!*              Kinetic hydrate dissociation or formation              *     
!*                                                                     *
!*                  Version 1.0 - September 29, 2004                   *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: TX,Peq_Hydr,Teq_Hydr,P_CH4,Psat
      REAL(KIND = 8) :: X_mA,Xmol_mA,X_act
! 
      REAL(KIND = 8) :: T_dev,X_iA,Xmol_iA,Xmol_mA_old,Xmol_iA_old
! 
      REAL(KIND = 8) :: S_aqu,S_gas,S_hyd,S_ice,HK_GinH2O
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: n,NLOC
! 
      INTEGER :: HPeq_Paramtrs,PTXS_Paramtrs  ! Function name
! 
! -------
! ... Character variables
! -------
! 
      CHARACTER(LEN = 1) :: H1 = ' '
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call, H1
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of EOS_InitAssignKin
!
!
      IF(First_call) THEN
!
         IF(MPI_RANK==0) WRITE(11,6000)
!
         First_call = .FALSE.
         P_quad_hyd = EqPres_HydrCH4(273.16d0,0.0d0,&
     &                               F_EqOption,IGOOD)
!
      END IF
!                                                                       
!                                                                       
!                                                                       
      IF (MOP(5) >= 7 .AND. MPI_RANK==0) WRITE(6,6001) KCYC,ITER
! 
! -------
! ... Initialization of T-depression variables        
! -------
! 
      T_dev    = 0.0d0  
      Xmol_mA  = 0.0d0
      Xmol_iA  = 0.0d0
! 
      Xmol_mA_old  = 0.0d0
      Xmol_iA_old  = 0.0d0
! 
!***********************************************************************
!*                                                                     *
!*                      SET UP INITIAL CONDITIONS                      *
!*                                                                     *
!***********************************************************************
! 
      DO_InConSt: DO n=1,NELL                                                     
!
         NLOC = (n-1)*NK1                                                  
!
         IF(NK == 3) THEN
            X_iA = 0.0d0  
            TX   = X(NLOC+4)  
         ELSE
            X_iA = X(NLOC+4)  
            TX   = X(NLOC+5)  
         END IF
!
!***********************************************************************
!*                                                                     *
!*         Determine iteratively the mole fractions at Peq_Hyd         *
!*                                                                     *
!***********************************************************************
!
      IGOOD = HPeq_Paramtrs(-1,TX,X_iA,.FALSE.,      &                    ! In only
     &                         Xmol_iA,Xmol_mA_old,Xmol_iA_old,T_dev, &   ! In and out
     &                         Peq_Hydr,HK_GinH2O,Xmol_mA,X_mA)          ! Out only
!
      IF(IGOOD /= 0) THEN
         CALL WARN_RF(elem(n)%name,XX,NK1,KC)
         RETURN
      END IF                                       
! 
! 
! >>>>>>>>>>>>>>>>>>
! .....
! ..... Choice of phase conditions 
! .....
! >>>>>>>>>>>>>>>>>>
! 
! 
         Case_State: SELECT CASE(elem(n)%StateIndex)
!
!***********************************************************************
!*                                                                     *
!*   Check if the element is under single-phase conditions: Aqueous    *
!*                                                                     *
!***********************************************************************
! 
         CASE (2_1)
! 
! ......... Erroneous 1-phase conditions are specified           
! 
               X_act = X(NLOC+2)
               IF (X_act > X_mA) THEN            
                  WRITE(6,6054) elem(n)%name,TX,X_act,X_mA                     
                  X(NLOC+2) = X_mA                                                 
               END IF 
! 
! ........ Initializing the pressure, temperature, saturations, indices 
! 
            elem(n)%P = X(NLOC+1)
            elem(n)%T = TX
! 
            Cell_V(n,0)%p_Satr(1) = 0.0d0
            Cell_V(n,0)%p_Satr(2) = 1.0d0
            Cell_V(n,0)%p_Satr(3) = 0.0d0
            Cell_V(n,0)%p_Satr(4) = 0.0d0
!
!***********************************************************************
!*                                                                     *
!*   Check if the element is under 2-phase conditions: Aqueous+Gas     *
!*                                                                     *
!***********************************************************************
! 
         CASE (3_1)
! 
            S_aqu = X(NLOC+2)
! 
! ------------- 
! ......... Check if erroneous 2-phase conditions are specified           
! ------------- 
! 
! ............ Check the pressure          
! 
               IF (X(NLOC+1) > Peq_Hydr) THEN            
                  WRITE(6,6055) elem(n)%name,TX,X(NLOC+1),Peq_Hydr                        
                  X(NLOC+1) = 0.90d0*Peq_Hydr                                                  
               END IF 
! 
! ............ Check the water saturation          
! 
               IF(S_aqu > 1.0d0 .OR. S_aqu < 0.0d0) THEN
                  WRITE(6,6505) elem(n)%name,S_aqu     ! Print a clarifying message about the error
                  STOP                                 ! Stop the simulation                                                
               END IF
! 
! ........ Initializing the pressure, temperature, saturations, indices 
! 
            elem(n)%P = X(NLOC+1)
            elem(n)%T = TX
! 
            Cell_V(n,0)%p_Satr(1) = 1.0d0 - S_aqu
            Cell_V(n,0)%p_Satr(2) = S_aqu
            Cell_V(n,0)%p_Satr(3) = 0.0d0
            Cell_V(n,0)%p_Satr(4) = 0.0d0
!
!***********************************************************************
!*                                                                     *
!*  Check if the element is under 2-phase conditions: Aqueous+Hydrate  *
!*                                                                     *
!***********************************************************************
! 
         CASE (5_1)
! 
! ......... The water saturation          
! 
            S_aqu = X(NLOC+2)
! 
! ------------- 
! ......... Check if erroneous 2-phase conditions are specified           
! ------------- 
! 
! ............ Determine Peq_Hydr: the equilibrium hydration temperature at PX       
! 
               Teq_Hydr = EqTemp_HydrCH4(X(NLOC+1),T_dev,&
     &                                   F_EqOption,0,IGOOD)-273.15d0
!
               IF(IGOOD /= 0) THEN
                  CALL WARN_RF(elem(n)%name,XX,NK1,KC)
                  RETURN
               END IF                                       
! 
! ............ Check the temperature       
! 
               IF (TX > Teq_Hydr) THEN            
                  WRITE(6,6056) elem(n)%name,X(NLOC+1),TX,Teq_Hydr                    
                  X(NLOC+3) = Teq_Hydr                                                 
               END IF 
! 
! ............ Check the water saturation      
! 
! 
               IF(S_aqu > 1.0d0 .OR. S_aqu < 0.0d0) THEN
                  WRITE(6,6505) elem(n)%name,S_aqu     ! Print a clarifying message about the error
                 STOP                                  ! Stop the simulation                                                
               END IF
! 
! ........ Initializing the pressure, temperature, saturations 
! 
            elem(n)%P = X(NLOC+1)
            elem(n)%T = X(NLOC+3)  !Is this right
! 
            Cell_V(n,0)%p_Satr(1) = 0.0d0
            Cell_V(n,0)%p_Satr(2) = S_aqu
            Cell_V(n,0)%p_Satr(3) = 1.0d0 - S_aqu
            Cell_V(n,0)%p_Satr(4) = 0.0d0
!
!***********************************************************************
!*                                                                     *
!*         Check if the element is under 3-phase conditions:           *
!*                      Aqueous + Hydrate + Gas                        *
!*                                                                     *
!***********************************************************************
! 
         CASE (7_1)
! 
! ------------- 
! ......... Phase saturations          
! ------------- 
! 
            S_gas = X(NLOC+3)
            S_aqu = X(NLOC+2)  
            S_hyd = 1.0d0 - S_gas - S_aqu
! 
! ------------- 
! ......... Check if erroneous 3-phase conditions are specified           
! ------------- 
! 
! ............ Check the gas saturation          
! 
               IF(S_gas > 1.0d0 .OR. S_gas < 0.0d0) THEN
                  WRITE(6,6506) elem(n)%name,S_gas     ! Print a clarifying message about the error
                  STOP                                 ! Stop the simulation                                                
               END IF
! 
! ............ Check the water saturation          
! 
               IF(S_aqu > 1.0d0 .OR. S_aqu < 0.0d0) THEN
                  WRITE(6,6505) elem(n)%name,S_aqu     ! Print a clarifying message about the error
                  STOP                                 ! Stop the simulation                                                
               END IF
! 
! ............ Check the hydrate saturation          
! 
               IF (S_hyd < 0.0d0) THEN            
                  WRITE(6,6058) elem(n)%name,(1.0d0 - S_hyd)                        
                  STOP
               END IF 
! 
! ............ Check the pressure         
! 
               IF (X(NLOC+1) /= Peq_Hydr) THEN            
                  !WRITE(6,6059) elem(n)%name,TX,Peq_Hydr,X(NLOC+1)                        
                  X(NLOC+1) = Peq_Hydr                                                  
               END IF 
! 
! ------------- 
! ......... Assigning initial saturations         
! ------------- 
! 
            Cell_V(n,0)%p_Satr(1) = S_gas 
            Cell_V(n,0)%p_Satr(2) = S_aqu
            Cell_V(n,0)%p_Satr(3) = S_hyd
            Cell_V(n,0)%p_Satr(4) = 0.0d0
! 
! ........ Initializing the pressure, temperature 
! 
            elem(n)%P = X(NLOC+1)
            elem(n)%T = TX
!
!***********************************************************************
!*                                                                     *
!*         Check if the element is under 3-phase conditions:           *
!*                        Aqueous + Ice + Gas                          *
!*                                                                     *
!***********************************************************************
! 
         CASE(8_1)
! 
! ------------- 
! ......... Phase saturations          
! ------------- 
! 
            IF(NK == 3) THEN 
               S_gas = X(NLOC+4)
            ELSE
               S_gas = X(NLOC+5)
            END IF
! 
            S_aqu = X(NLOC+2)
            S_ice = 1.0d0 - S_gas - S_aqu
! 
! ------------- 
! ......... Check if erroneous 3-phase conditions are specified           
! ------------- 
! 
! ............ Check the gas saturation          
! 
               IF(S_gas > 1.0d0 .OR. S_gas < 0.0d0) THEN
                  WRITE(6,6506) elem(n)%name,S_gas      ! Print a clarifying message about the error
                  STOP                                  ! Stop the simulation                                                
               END IF
! 
! ............ Check the water saturation          
! 
               IF(S_aqu > 1.0d0 .OR. S_aqu < 0.0d0) THEN
                  WRITE(6,6505) elem(n)%name,S_aqu      ! Print a clarifying message about the error
                  STOP                                  ! Stop the simulation                                                
               END IF
! 
! ............ Check the ice saturation          
! 
               IF (S_ice < 0.0d0) THEN            
                  WRITE(6,6071) elem(n)%name,(S_gas + S_ice)                        
                  STOP
               END IF 
! 
! ............ Check the pressure          
! 
               IF(X(NLOC+1) > P_quad_hyd) THEN
                  !WRITE(6,6057) elem(n)%name,P_quad_hyd,X(NLOC+1)                        
                  X(NLOC+1) = 9.9d-1*P_quad_hyd                                                  
               END IF 
! 
! ------------- 
! ......... Initializing the pressure
! ------------- 
! 
            elem(n)%P = X(NLOC+1)
! 
! ------------- 
! ......... Assigning initial saturations         
! ------------- 
! 
            Cell_V(n,0)%p_Satr(1) = S_gas 
            Cell_V(n,0)%p_Satr(2) = S_aqu
            Cell_V(n,0)%p_Satr(3) = 0.0d0
            Cell_V(n,0)%p_Satr(4) = S_ice
! 
! ......... A starting T estimate
! 
            TX = Tc_quad_hyd   ! Initial temperature estimate  
!
! ------------- 
! ......... Determine iteratively partial P, T, T-dev and CH4 solubility   
! ------------- 
!
            IGOOD = PTXS_Paramtrs(-1,X(NLOC+1),X_iA,  &                           
     &                               TX,Xmol_iA,Xmol_mA_old,&
     &                               Xmol_iA_old,T_dev,      &
     &                               P_CH4,Psat,HK_GinH2O,X_mA,Xmol_mA)          
! 
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
! 
! ------------- 
! ......... Initializing the temperature
! ------------- 
! 
            elem(n)%T = TX
!
!***********************************************************************
!*                                                                     *
!*         Check if the element is under 4-phase conditions:           *
!*          Quadruple Point: Aqueous + Ice + Hydrate + Gas             *
!*                                                                     *
!***********************************************************************
! 
         CASE (11_1)
! 
! ------------- 
! ......... Phase saturations          
! ------------- 
! 
            S_aqu = X(NLOC+2)
            S_gas = X(NLOC+3)
! 
            IF(NK == 3) THEN
               S_ice = X(NLOC+4)
            ELSE
               S_ice = X(NLOC+5)
            END IF
! 
            S_hyd = 1.0d0 - S_gas - S_aqu - S_ice
! 
! ------------- 
! ......... Check if erroneous 4-phase conditions are specified           
! ------------- 
! 
! ............ Check the gas saturation          
! 
               IF(S_gas > 1.0d0 .OR. S_gas < 0.0d0) THEN
                  WRITE(6,6506) elem(n)%name,S_gas     ! Print a clarifying message about the error
                  STOP                                 ! Stop the simulation                                                
               END IF
! 
! ............ Check the water saturation          
! 
               IF(S_aqu > 1.0d0 .OR. S_aqu < 0.0d0) THEN
                  WRITE(6,6505) elem(n)%name,S_aqu     ! Print a clarifying message about the error
                  STOP                                 ! Stop the simulation                                                
               END IF
! 
! ............ Check the ice saturation          
! 
               IF(S_ice > 1.0d0 .OR. S_ice < 0.0d0) THEN
                  WRITE(6,6507) elem(n)%name,S_ice     ! Print a clarifying message about the error
                  STOP                                 ! Stop the simulation                                                
               END IF
! 
! ............ Check the hydrate saturation          
! 
               IF (S_hyd < 0.0d0) THEN            
                  WRITE(6,6070) elem(n)%name,(S_gas + S_aqu + S_ice)                       
                  STOP
               END IF 
! 
! ............ Check the pressure          
! 
               IF (X(NLOC+1) /= Peq_Hydr) THEN            
                  WRITE(6,6060) elem(n)%name,TX,P_quad_hyd,X(NLOC+1)                        
                  X(NLOC+1) = P_quad_hyd                                                  
               END IF 
! 
! ------------- 
! ......... Assigning initial saturations         
! ------------- 
! 
            Cell_V(n,0)%p_Satr(1) = S_gas 
            Cell_V(n,0)%p_Satr(2) = S_aqu
            Cell_V(n,0)%p_Satr(3) = S_hyd
            Cell_V(n,0)%p_Satr(4) = S_ice
! 
! ........ Initializing the pressure, temperature 
! 
            elem(n)%P = X(NLOC+1)
            elem(n)%T = Tc_quad_hyd - T_dev
!
!***********************************************************************
!*                                                                     *
!*              Unrealistic/incorrect initial conditions               *
!*                                                                     *
!***********************************************************************
! 
        CASE DEFAULT
! 
           CALL WARN_RF(elem(n)%name,XX,NK1,KC)
! 
        END SELECT Case_State
!                                                                       
!                                                                       
      END DO DO_InConSt                                                        
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'EOS_InitAssignKin 1.0   29 September 2004',6X, & 
     &         'Initial assignment of thermophysical properties in a ',   &
     &         'hydrate system - Equilibrium reaction') 
!                                                                       
 6001 FORMAT(/,' %%%%%%%%%% E O S %%%%%%%%%% E O S %%%%%%%%%%',    &
     &          '  [KCYC,ITER] = [',I4,',',I3,']'/) 
!                                                                       
 6505 FORMAT(//,20('ERROR-'),//,  &
     &         T20,'R U N   A B O R T E D',/,   &
     &         T20,'The conditions specified in ',   &
     &             'element "',a8,'" are erroneous !!!',/,   &
     &         T20,'The aqueous phase saturation ',   &
     &             'S_aqu =',1pe11.4,' is outside the possible ',&
     &             'range (0 <= S_aqu <= 1)',//,   &
     &         T20,'CORRECT AND TRY AGAIN',//,   &
     &          20('ERROR-'))
 6506 FORMAT(//,20('ERROR-'),//,  &
     &         T20,'R U N   A B O R T E D',/,   &
     &         T20,'The conditions specified in ',  &
     &             'element "',a8,'" are erroneous !!!',/,   &
     &         T20,'The gas phase saturation ',  &
     &             'S_gas =',1pe11.4,' is outside the possible ',&
     &             'range (0 <= S_gas <= 1)',//,   &
     &         T20,'CORRECT AND TRY AGAIN',//,   &
     &          20('ERROR-'))
 6507 FORMAT(//,20('ERROR-'),//,  & 
     &         T20,'R U N   A B O R T E D',/,   &
     &         T20,'The conditions specified in ',   &
     &             'element "',a8,'" are erroneous !!!',/,   &
     &         T20,'The ice phase saturation ',  &
     &             'S_ice =',1pe11.4,' is outside the possible ',&
     &             'range (0 <= S_gas <= 1)',//,  &
     &         T20,'CORRECT AND TRY AGAIN',//,  &
     &          20('ERROR-'))
!                                                                       
 6053 FORMAT(' THE SINGLE-PHASE CONDITIONS SPECIFIED AT ELEMENT "',A8,   &    
     &         '" ARE ERRONEOUS',/,  &
     &         ' At T = ',1pE12.5,' C, the CH4 mole fraction',       &
     &         'in the gas phase "Xmol_mG" =',1pE12.5, &
     &         ' exceeds the maximum = 1',/,  &
     &         ' !!! ACTION TAKEN: SET Xmol_mG = 1   ==>   EXECUTION',   &
     &         ' CONTINUES',/)   
!                                                                       
 6054 FORMAT(' THE SINGLE-PHASE CONDITIONS SPECIFIED AT ELEMENT "',A8,   &      
     &         '" ARE ERRONEOUS',/,  &
     &         ' At T = ',1pE12.5,' C, the CH4 mass fraction',  &
     &         'in H2O "X_mA" =',1pE12.5,&
     &         ' exceeds the maximum = ',1pE12.5,' (T-dependent)',/,      &                                        
     &         ' ACTION TAKEN: SET X_mA = Max  ==>  EXECUTION',&
     &         ' CONTINUES',/)   
!                                                                       
 6055   FORMAT(' THE TWO-PHASE CONDITIONS SPECIFIED AT ELEMENT "',A8,&
     &         '" ARE ERRONEOUS',/,  &
     &         ' At T = ',1pE12.5,' C, the pressure P =',1pE15.8,&
     &         ' Pa exceeds the equilibrium pressure ',&
     &         '"Peq_Hydr" = ',1pE15.8,' Pa',/,  &
     &         ' !!! ACTION TAKEN: SET P = 0.9*Peq_Hydr   ',&
     &         '==>   EXECUTION CONTINUES',/)   
!                                                                       
 6056   FORMAT(' THE TWO-PHASE CONDITIONS SPECIFIED AT ELEMENT "',A8,     &  
     &         '" ARE ERRONEOUS',/,  &
     &         ' At P =',1pE12.5,' Pa, the input temperature T =',  &
     &         1pE12.5,' oC exceeds the hydrate equilibrium ',&
     &         '"Teq_Hydr"  = ',1pE15.8,' oC',/,               &                         
     &         ' ACTION TAKEN: SET T = Teq_Hydr  ==>  EXECUTION',   &
     &         ' CONTINUES',/)   
!                                                                       
 6057   FORMAT(' THE THREE-PHASE CONDITIONS SPECIFIED AT ELEMENT "',A8,    &
     &         '" ARE ERRONEOUS',/, &
     &         ' The quadruple point pressure "P_quad_hyd" = ',1pE15.8,  &
     &         ' Pa < the total pressure P = ',1pE15.8,' Pa',/,   &
     &         ' !!! ACTION TAKEN: SET P = 0.99*P_quad_hyd   ',&
     &         '==>   EXECUTION CONTINUES',/)   
!                                                                       
 6058 FORMAT(//,20('ERROR-'),//,&
     &         T20,'R U N   A B O R T E D',/,  &
     &         T20,'The three-phase conditions specified in ',   &
     &             'element "',a8,'" are erroneous !!!',/,   &
     &         T20,'The sum of the aqueous and gas phase saturations ', &
     &             'S_aqu + S_gas =',1pe11.4,' -> exceeds 1',//,  &
     &         T20,'CORRECT AND TRY AGAIN',//, &
     &          20('ERROR-'))
!                                                                       
 6059   FORMAT(' THE THREE-PHASE CONDITIONS SPECIFIED AT ELEMENT "',A8, &
     &         '" ARE ERRONEOUS',/, &
     &         ' At T = ',1pE12.5,' C, the equilibrium pressure ',&
     &         '"Peq_Hydr" =',1pE15.8, &
     &         ' Pa differs from the total pressure P = ',&
     &           1pE15.8,' Pa',/,                          &               
     &         ' ACTION TAKEN: SET P = Peq_Hydr  ==>  EXECUTION', &
     &         ' CONTINUES',/)   
!                                                                       
 6060   FORMAT(' THE FOUR-PHASE CONDITIONS SPECIFIED AT ELEMENT "',A8,&
     &         '" ARE ERRONEOUS',/,&
     &         ' At T = ',1pE12.5,' C, the quadruple point ',&
     &         'pressure "P_quad_hyd" =',1pE15.8,&
     &         ' Pa differs from the total pressure P = ',&
     &           1pE15.8,' Pa',/,                                      &     
     &         ' ACTION TAKEN: SET P = P_quad_hyd  ==>  EXECUTION',&
     &         ' CONTINUES',/)   
!                                                                       
 6067   FORMAT(' THE THREE-PHASE CONDITIONS SPECIFIED AT ELEMENT "',A8,&
     &         '" ARE ERRONEOUS',/,&
     &         ' The quadruple point pressure "P_quad_hyd" = ',1pE15.8,&
     &         ' Pa is larger than the total pressure P = ',1pE15.8,&
     &         ' !!! ACTION TAKEN: SET P = 1.01*P_quad_hyd   ',&
     &         ' Pa',/,'==>   EXECUTION CONTINUES',/)   
!                                                                       
 6068   FORMAT(' THE THREE-PHASE CONDITIONS SPECIFIED AT ELEMENT "',A8,&     
     &         '" ARE ERRONEOUS',/, &
     &         ' The specified temperature T = ',1pE15.8,' oC is ',&
     &         'different from the temperature ',/,&
     &         ' T_f = ',1pE15.8,' oC corresponding to the ',&
     &         'pressure P = ',1pE15.8,' Pa',/, &
     &         ' !!! ACTION TAKEN: SET T = T_f   ==>   ',&
     &         'EXECUTION CONTINUES',/)   
!                                                                       
 6069 FORMAT(//,20('ERROR-'),//,  &
     &         T20,'R U N   A B O R T E D',/,&
     &         T20,'The three-phase conditions specified in ', &
     &             'element "',a8,'" are erroneous !!!',/,&
     &         T20,'The sum of the aqueous and gas phase saturations ',&
     &             'S_aqu + S_ice =',1pe11.4,' -> exceeds 1',//, &
     &         T20,'CORRECT AND TRY AGAIN',//, &
     &          20('ERROR-'))
!                                                                       
 6070 FORMAT(//,20('ERROR-'),//, &
     &         T20,'R U N   A B O R T E D',/,&
     &         T20,'The quadruple point conditions specified in ',&
     &             'element "',a8,'" are erroneous !!!',/,&
     &         T20,'The sum of the aqueous, gas and ice ',&
     &             'phase saturations ',&
     &             'S_aqu + S_gas + S_ice =',1pe11.4,' -> exceeds 1',//,&
     &         T20,'CORRECT AND TRY AGAIN',//,   &
     &          20('ERROR-'))
!                                                                       
 6071 FORMAT(//,20('ERROR-'),//,   &
     &         T20,'R U N   A B O R T E D',/, &
     &         T20,'The three-phase conditions specified in ', &
     &             'element "',a8,'" are erroneous !!!',/,   &
     &         T20,'The sum of the ice and gas phase saturations ', &
     &             'S_ice + S_gas =',1pe11.4,' -> exceeds 1',//, &
     &         T20,'CORRECT AND TRY AGAIN',//, &
     &          20('ERROR-'))
!                                                                       
 6100 FORMAT(' !!!!! WARNING: Primary variables with "NaN" values are', &
     &       ' encountered in "EOS".',   &
     &       ' Dt is reduced and the simulation continues <===')                                 
!                                                                       
 6101 FORMAT(/,' PRIMARY VARIABLES',/)                                    
 6102 FORMAT(' AT ELEMENT *',A8,'* --- ',(4(2X,1pE20.13)))              
!                                                                       
 6103 FORMAT(' >>>> Excessive CH4 molar fraction increase :  XX(3)=', &
     &       1pE12.5,' ==> Hydrate appears at element "',  &
     &       A8,'"')
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of EOS_InitAssignKin
!
!
      RETURN
!
      END SUBROUTINE EOS_InitAssignKin                                                          
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE PHS1_Aqu_Kin(N,NLOC,NMAT,HDis_Switch)   
! 
! ... Modules to be used 
! 
      USE Universal_Param
      USE Basic_Param
      USE GenControl_Param
      USE Diffusion_Param
!
      USE H2O_Param
      USE Hydrate_Param
!
      USE Variable_Arrays
      USE Element_Arrays
      USE PFMedProp_Arrays
!
      USE H2O_Properties
      USE Hydrate_Properties
      USE RealGas_Properties
	  USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*       ROUTINE FOR COMPUTING AND ASSIGNING ALL THE SECONDARY         *
!*         VARIABLES UNDER SINGLE-PHASE (AQUEOUS) CONDITIONS           *
!*               IN HYDRATE SYSTEMS - Extended T-scale                 *
!*                                                                     *
!*                  Version 1.0 - September 29, 2004                   *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!                                                                       
      IMPLICIT NONE
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(2) :: Y_inG
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: PX,TX,Psat,P_evol,D_gas
      REAL(KIND = 8) :: DW,UW,HW,V_aqu,D_aqu
! 
      REAL(KIND = 8) :: X_mA,Xmol_mA,S_Hydr,Peq_Hydr,D_Hydr,H_Hydr
      REAL(KIND = 8) :: H_Sol,XDEN,dummy
      REAL(KIND = 8) :: P_CH4,HK_GinH2O,H_Rate,H_Heat
! 
      REAL(KIND = 8) :: ZLiq,ZGas,rho_L,T_k
      REAL(KIND = 8) :: Hi_Gas,Ui_Gas,Hd_Gas,Ud_Gas,Sd_Gas,H_Gas,U_Gas
      REAL(KIND = 8) :: T_dev,X_iA,Xmol_iA
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN)  :: N,NLOC,NMAT
! 
      INTEGER :: K,ixx
! 
! -------
! ... Character variables     
! -------
! 
      CHARACTER(LEN = 8) :: PhTran 
! 
! -------
! ... Logical variables     
! -------
! 
      LOGICAL :: HDis_Switch,HApr_Switch 
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of PHS1_Aqu_Kin
!
!
      IF(First_call) THEN
         First_call = .FALSE.
         IF(MPI_RANK==0) WRITE(11,6000)
      END IF
! 
! ... Some initializations 
! 
      PhTran      = '        '
      HApr_Switch = .FALSE.
!
      S_Hydr   = 0.0d0
      Peq_Hydr = 0.0d0
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>      Main loop determining conditions and assigning properties      >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_NumEqs: DO K=1,NEQ1
!
! ----------
! ...... Initial assignments of primary variable values        
! ...... Primary variables: Px, Xaw, S_hydrate, Tx        
! ----------
! 
         PX     = XX(1)
         X_mA   = XX(2)    ! Mass fraction of CH4 in the aqueous phase
         S_Hydr = XX(3)    ! Hydrate saturation
! 
         IF(NK == 3) THEN
            X_iA = 0.0d0
            TX   = XX(4)
         ELSE IF(NK == 4) THEN
            X_iA = XX(4)
            TX   = XX(5)
         END IF
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the first iteration - Unincremented variables
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         IF_1stEq: IF(K == 1) THEN
! 
! -------------
! ......... Constraint on inhibitor mass fraction in the aqueous phase        
! -------------
! 
            IF_SaltMassF: IF (X_iA < 0.0d0) THEN
               X_iA       =   0.0d0
               XX(4)      =   0.0d0
               DX(NLOC+4) = - X(NLOC+4)
            END IF IF_SaltMassF
! 
! -------------
! ......... Determine H2O vapor pressure        
! -------------
! 
            IGOOD = P_Sat_H2O_S(TX,Psat)
!
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
! 
! -------------
! ......... Constraint on CH4 fraction in the aqueous phase        
! -------------
! 
            IF_CH4MassF: IF (X_mA < 0.0d0) THEN
! 
               IF(mop(5) >= 4) THEN
                  WRITE(6,6001) elem(n)%name,px,X_mA,S_Hydr,tx,psat
               END IF
!                                                                       
               X_mA       = 0.0d0
               XX(2)      = 0.0d0
               DX(NLOC+2) = XX(2)-X(NLOC+2)
!                                                                       
            END IF IF_CH4MassF
! 
! -------------
! ......... Constraint on hydrate saturation        
! -------------
! 
            IF_HydSat: IF (S_Hydr < 0.0d0) THEN
!                                                                       
               IF(mop(5) >= 4) THEN
                  WRITE(6,6002) elem(n)%name,PX,X_mA,S_Hydr,tx,psat
               END IF
!                                                                       
               S_Hydr     = 0.0d0
               XX(3)      = 0.0d0
               DX(NLOC+3) = XX(3)-X(NLOC+3)
!                                                                       
            END IF IF_HydSat
! 
! -------------
! ......... Determine mole fractions      
! -------------
! 
            XDEN =(  X_mA/MW_CH4  &                     ! CH4
     &             + X_iA/MW_Inhib &                    ! Inhibitor
     &             + ( 1.0d0 - X_mA - X_iA)/MW_H2O )   ! Water
 
            Xmol_mA = (X_mA/MW_CH4)/XDEN
!
            Xmol_iA = (X_iA/MW_Inhib)/XDEN
! 
! -------------
! ......... Determine the CH4 partial pressure corresponding to the dissolved CH4 fraction  
! -------------
! 
            P_CH4 = Xmol_mA*HenryS_K(1,TX,Xmol_iA,.TRUE.)
! 
! -------------
! ......... Determine the salt-induced suppression of the hydration T   
! -------------
! 
            T_dev = T_MaxOff*DLOG(1.0d0-Xmol_iA)   &
     &                      /DLOG(1.0d0-C_MaxOff+1.0d-10)            
! 
! ......... Determine Peq_Hydr: the equilibrium hydration pressure at TX       
! 
            Peq_Hydr = EqPres_HydrCH4(TX+273.15d0,T_dev,&
     &                                F_EqOption,IGOOD)
!
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
!
!***********************************************************************
!*                                                                     *
!*                    Check phase transition from                      *
!*          1-phase (Aqueous) to 2-phase (Aqueous + Hydrate)           *
!*                                                                     *
!***********************************************************************
!
! ......... Determine whether hydrate evolves   
! 
            IF_HydrEv: IF(P_CH4 > (1.0d0+ZERO)*(Peq_Hydr-Psat)) THEN
! 
               PhTran = 'Aqu->AqH'
               GO TO 1000
! 
            END IF IF_HydrEv
!
!***********************************************************************
!*                                                                     *
!*                    Check phase transition from                      *
!*            1-phase (Aqueous) to 2-phase (Aqueous + Gas)             *
!*                                                                     *
!***********************************************************************
!
! ......... Compute the sum of the H2O and the CH4 pressures: switching criterion 
! 
            P_evol = Psat + P_CH4
! 
! ......... Gas evolution   
! 
            IF_GasEv: IF(PX < P_evol) THEN
! 
               PhTran = 'Aqu->AqG'
               GO TO 1000
! 
            ELSE
               GO TO 500 
            END IF IF_GasEv
! 
         END IF IF_1stEq
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the remaining iterations - Incremented variables
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         CASE_Incr: SELECT CASE(K)
! 
! ...... Incrementing pressure 
! 
         CASE(2)                    
! 
            DELX(NLOC+1) = DFAC*XX(1)
            PX           = XX(1) + DELX(NLOC+1)
! 
! ...... Incrementing mass fraction of dissolved CH4 in H2O 
! 
         CASE(3)                     
! 
            DELX(NLOC+2) = DFAC
            X_mA         = XX(2) + DELX(NLOC+2)
! 
! ...... Incrementing the hydrate saturation
! 
         CASE(4)                                
! 
            DELX(NLOC+3) = DFAC
            S_Hydr       = 0.0d0
! 
! ...... Incrementing temperature (NK = 3) or salt mass fraction (NK = 4) 
! 
         CASE(5) 
! 
            IF(NK == 3) THEN
               X_iA         = 0.0d0
               DELX(NLOC+4) = DFAC*(273.15d0 + XX(4))
               TX           = XX(4) + DELX(NLOC+4)
            ELSE IF(NK == 4) THEN
               DELX(NLOC+4) = DFAC
               X_iA         = XX(4) + DELX(NLOC+4)
            END IF
! 
! ...... Incrementing temperature (NK = 4)
! 
         CASE(6) 
! 
            DELX(NLOC+5) = DFAC*(273.15d0 + XX(5))
            TX           = XX(5) + DELX(NLOC+5)
! 
         END SELECT CASE_Incr
!
!***********************************************************************
!*                                                                     *
!*         For augmented primary variables:                            *
!*             Determine the relevant parameters                       *
!*                                                                     *
!***********************************************************************
! 
         IF_KGT1: IF(k > 1) THEN
!                                                                       
            XDEN =(  X_mA/MW_CH4                         & ! CH4
     &             + X_iA/MW_Inhib                       & ! Inhibitor
     &             + ( 1.0d0 - X_mA - X_iA )/MW_H2O )     ! Water
! 
            Xmol_iA = (X_iA/MW_Inhib)/XDEN                ! Inhibitor
! 
! ......... Determine the salt-induced suppression of the hydration T   
! 
            T_dev = T_MaxOff*DLOG(1.0d0-Xmol_iA)   &
     &                      /DLOG(1.0d0-C_MaxOff-1.0d-10)            
!                                                                       
         END IF IF_KGT1
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... Compute thermophysical properties 
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
  500    T_k = TX + 273.15d0  ! ...... Temperature in K   
!
!***********************************************************************
!*                                                                     *
!*             P R O P E R T I E S  +  P A R A M E T E R S             *
!*                                                                     *
!***********************************************************************
!
! 
! -------------
! ...... Mole fractions in the dissolved gas      
! -------------
! 
         Y_inG(1) = 1.0d0   ! CH4
         Y_inG(2) = 0.0d0   ! H2O
!                                                                      
! -------------
! ...... Determine the real gas density and compressibility
! -------------
!                                                                      
         CALL Zcomp(PX,T_k,0.0d0,Y_inG,ZLiq,ZGas,D_gas,rho_L,ixx)
!                                                                       
! ----------
! ...... Internal energy and density of liquid H2O                                                                       
! ----------
!                                                                       
         CALL UHRho_LiqH2O(TX,PX,DW,UW,HW,IGOOD)
! 
! ----------
! ...... Mixture model for aqueous phase density: Assume volume  
! ...... conservation during CH4 dissolution  
! ----------
! 
         D_aqu = X_iA*D_Inhib+X_mA*D_gas+(1.0d0-X_iA-X_mA)*DW    
!                                                                       
! ----------
! ...... Viscosity of liquid H2O                                                                        
! ----------
!                                                                       
         V_aqu = ViscLV_H2Ox(TX,D_aqu)            ! Aqueous phase
! 
! ----------
! ...... Compute the enthalpy of the dissolved CH4 
! ----------
! 
         HK_GinH2O = HenryS_K(1,TX,Xmol_iA,.TRUE.)
!
! ...... Gas specific enthalpy and internal energy
!
         CALL HU_IdealGas(1,2.7315d2,T_k,Hi_Gas,Ui_Gas)                                  
         CALL HUS_Departure(T_k,ZGas,Hd_Gas,Ud_Gas,Sd_Gas) ! Departure enthalpy
!                                                                      
         H_Gas = (Hi_Gas - Hd_Gas)/MW_CH4     ! Convert to J/Kg/K from J/kgmol/K
         U_Gas = (Ui_Gas - Ud_Gas)/MW_CH4     
!
! ...... Enthalpy of CH4 dissolution
!
         H_Sol = HEAT_GasSolution(1,TX,Xmol_iA,HK_GinH2O,dummy)                                     
! 
! ----------
! ...... Compute the rate of kinetic hydrate dissociation/formation 
! ----------
! 
         IF(HDis_Switch .EQV. .TRUE.) THEN
! 
            CALL HRho_Hydrate(TX,PX,D_Hydr,H_Hydr)
            H_Rate = -Cell_V(n,k-1)%p_Satr(3)*D_Hydr*elem(n)%phi/FORD
!
            H_Heat = HeatDiss_HydrCH4(TX+273.15d0,&
     &                                F_EqOption,  & ! H-equation
     &                                IGOOD)
!
         ELSE
            H_Rate = 0.0d0
            H_Heat = 0.0d0
         END IF
! 
! ----------
! ...... Assign value to state index  
! ----------
! 
         IF(k == 1) elem(n)%StateIndex = 2_1
! 
! ----------
! ...... Assign values of secondary parameters - Aqueous phase
! ----------
! 
         Cell_V(n,k-1)%p_Satr(1)   = 0.0d0
         Cell_V(n,k-1)%p_KRel(1)   = 0.0d0
         Cell_V(n,k-1)%p_Visc(1)   = 1.0d0
         Cell_V(n,k-1)%p_Dens(1)   = 0.0d0
         Cell_V(n,k-1)%p_Enth(1)   = 0.0d0
         Cell_V(n,k-1)%p_PCap(1)   = 0.0d0
         Cell_V(n,k-1)%p_MasF(1,1) = 0.0d0
         Cell_V(n,k-1)%p_MasF(2,1) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,1) = 0.0d0
         Cell_V(n,k-1)%p_MasF(4,1) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Aqueous phase 
! ----------
! 
         Cell_V(n,k-1)%p_Satr(2)   = 1.0d0
         Cell_V(n,k-1)%p_KRel(2)   = 1.0d0
         Cell_V(n,k-1)%p_Visc(2)   = V_aqu
         Cell_V(n,k-1)%p_Dens(2)   = D_aqu
         Cell_V(n,k-1)%p_Enth(2)   =  (1.0d0 - X_mA - X_iA)*HW &
     &                               + X_mA*(H_Gas + H_Sol) &
     &                               + X_iA*H_InhSol
         Cell_V(n,k-1)%p_PCap(2)   = 0.0d0                             
         Cell_V(n,k-1)%p_MasF(1,2) = 1.0d0-X_mA-X_iA
         Cell_V(n,k-1)%p_MasF(2,2) = X_mA
         Cell_V(n,k-1)%p_MasF(3,2) = 0.0d0
         Cell_V(n,k-1)%p_MasF(4,2) = X_iA
! 
! ----------
! ...... Assign values of secondary parameters - Hydrate phase 
! ----------
! 
         Cell_V(n,k-1)%p_Satr(3)   = 0.0d0
         Cell_V(n,k-1)%p_Dens(3)   = 0.0d0
         Cell_V(n,k-1)%p_Enth(3)   = 0.0d0
         Cell_V(n,k-1)%p_MasF(1,3) = 0.0d0
         Cell_V(n,k-1)%p_MasF(2,3) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,3) = 0.0d0
         Cell_V(n,k-1)%p_MasF(4,3) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Ice phase 
! ----------
! 
         Cell_V(n,k-1)%p_Satr(4)   = 0.0d0
         Cell_V(n,k-1)%p_Dens(4)   = 0.0d0
         Cell_V(n,k-1)%p_Enth(4)   = 0.0d0
         Cell_V(n,k-1)%p_MasF(1,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(2,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(4,4) = 0.0d0
! 
! ----------
! ...... Store the temperature 
! ----------
! 
         Cell_V(n,k-1)%TemC = TX
! 
! ----------
! ...... Store other important variables  
! ----------
! 
         IF(K == 1) THEN
! 
! ......... Store equilibrium hydration pressure at TX 
! 
            Cell_V(n,0)%p_AddD(1) = Peq_Hydr
! 
! ......... Store the hydrate temperature depression  
! 
            Cell_V(n,1)%p_AddD(1) = T_dev
! 
         END IF
! 
! ----------
! ...... Store rate of kinetic hydrate dissociation/formation 
! ----------
! 
         Cell_V(n,k-1)%p_AddD(2) = H_Rate
! 
! ----------
! ...... Store heat of dissociation/formation 
! ----------
! 
         Cell_V(n,k-1)%p_AddD(3) = H_Heat
! 
! 
! 
      END DO DO_NumEqs
! 
! 
! 
      RETURN     ! Task completed - Exit the routine
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                         PHASE TRANSITIONS                           >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
 1000 CONTINUE
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Aqueous (1-phase)
! .............. to Aqueous + Hydrate (2-phase)    
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
      IF_PhTran: IF(PhTran == 'Aqu->AqH') THEN
! 
                    IF(MOP(5) >= 3) WRITE(6,6003) elem(n)%name,P_CH4,&
     &                                           (Peq_Hydr-Psat),X_mA
                    IF(KC == 0) THEN
                      WRITE(6,6101) 
                      STOP                                             
                    END IF
! 
! ................. Adjust the 1st variable as pressure 
! 
                    XX(1)      = Peq_Hydr*(1.0d0+ZERO)
                    DX(NLOC+1) = XX(1)-X(NLOC+1)
! 
! ................. Reset the 2nd variable as aqueous phase saturation 
! 
                    XX(2)      = 1.0d0-ZERO
                    DX(NLOC+2) = XX(2)-X(NLOC+2)
! 
! ................. Reset the 3rd variable as CH4 mass fraction in the aqueous phase
! 
                    XX(3)      = XX(2)
                    DX(NLOC+3) = XX(3)-X(NLOC+3)
! 
! ................. Activate the logical switch trigger       
! 
                    HApr_Switch = .TRUE.
! 
! ................. Call the 2-phase (H2O+Hydrate) routine 
! 
                    CALL PHS2_AqH_Kin(N,NLOC,NMAT,HApr_Switch)
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Aqueous (1-phase)
! .............. to Aqueous + Gas (2-phase)    
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
                 ELSE IF(PhTran == 'Aqu->AqG') THEN
! 
                   IF(MOP(5) >= 3) WRITE(6,6004) elem(n)%name,PX,P_evol
                       IF(KC == 0) THEN
                       WRITE(6,6101)                                              
                       STOP                                             
                    END IF
! 
! ................. Reset the 2nd variable as aqueous phase saturation 
! 
                    XX(2)      = 1.0d0-ZERO
                    DX(NLOC+2) = XX(2)-X(NLOC+2)
! 
! ................. Reset the 3rd variable as CH4 mole fraction in the gas phase
! 
                    XX(3)      = P_CH4/PX
                    DX(NLOC+3) = XX(3)-X(NLOC+3)
! 
! ................. Call the 2-phase (H2O+Gas) routine 
! 
                    CALL PHS2_AqG_Kin(N,NLOC,NMAT,.FALSE.)
! 
                 ELSE 
! 
                    WRITE(6,6102)
                    STOP
! 
                 END IF IF_PhTran
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'PHS1_Aqu_Kin      1.0   29 September 2004',6X, &
     &         'Routine for assigning the secondary ', &
     &         'parameters under 1-phase (aqueous) conditions in a', &
     &       /,T48,'hydrate system - Kinetic hydrate reaction')
!
 6001 FORMAT(' X_mA in "PHS1_Aqu_Kin" : Element "',A8,&
     &       '" ==> Px =',1pE13.6,' X_mA =',1pE13.6, &
     &       ' S_Hydr =',1pE13.6,' Tx =',1pE13.6, &
     &       ' PSat_H2O =',1pE13.6)
 6002 FORMAT(' S_Hydr in "PHS1_Aqu_Kin" : Element "',A8,&
     &       '" ==> Px =',1pE13.6,' X_mA =',1pE13.6,&
     &       'S_Hydr =',1pE13.6,' Tx =',1pE13.6,&
     &       ' PSat_H2O =',1pE13.6)
 6003 FORMAT(' !!!!!! "PHS1_Aqu_Eql": Hydrate evolves at element "',A8,&
     &       '" ==> P_CH4 = ',1pE15.8,'  (Peq_Hydr-Psat) = ',1pE15.8, &
     &       '  X_mA = ',1pE15.8)
 6004 FORMAT(' !!!!!! "PHS1_Aqu_Eql": Gas phase evolves at ',&    
     &       'element "',A8,'" ==> P = ',1pE15.8,'  P_evol = ',1pE15.8)
! 
 6101 FORMAT(' !!!!!!!!!!   Routine PHS1_Aqu_Kin: ERRONEOUS DATA ',&
     &       'INITIALIZATION   ==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
 6102 FORMAT(' !!!!!!!!!!   Routine PHS1_Aqu_Kin: ',  &
     &       'ERRONEOUS PHASE CHANGE   ==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of PHS1_Aqu_Kin
! 
! 
      RETURN
!
      END SUBROUTINE PHS1_Aqu_Kin
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE PHS2_AqH_Kin(N,NLOC,NMAT,HApr_Switch)
! 
! ... Modules to be used 
! 
      USE Universal_Param
      USE Basic_Param
      USE GenControl_Param
      USE Diffusion_Param
!
      USE H2O_Param
      USE Hydrate_Param
!
      USE Variable_Arrays
      USE Element_Arrays
      USE PFMedProp_Arrays
!
      USE H2O_Properties
      USE Hydrate_Properties
      USE RealGas_Properties
	  USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*       ROUTINE FOR COMPUTING AND ASSIGNING ALL THE SECONDARY         *
!*            VARIABLES IN TWO-PHASE (H2O+HYDRATE) SYSTEMS             *
!*                                                                     *
!*                  Version 1.0 - September 29, 2004                   *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!                                                                       
      IMPLICIT NONE
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(2) :: Y_inG
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: PX,S_aqu,TX,P_evol,XDEN
      REAL(KIND = 8) :: PSAT,DW,UW,HW,D_Hydr,H_Hydr,V_aqu
      REAL(KIND = 8) :: k_rel_aqu,k_rel_gas,D_aqu,T_k
! 
      REAL(KIND = 8) :: X_mA,S_Hydr,Peq_Hydr,dummy
      REAL(KIND = 8) :: H_Rate,H_Heat,H_Sol,HK_GinH2O
! 
      REAL(KIND = 8) :: ZLiq,ZGas,rho_G,rho_L,P_CH4,V_Hydr,R_Hydr
      REAL(KIND = 8) :: Hi_Gas,Ui_Gas,Hd_Gas,Ud_Gas,Sd_Gas,H_Gas,U_Gas
      REAL(KIND = 8) :: T_dev,X_iA,Xmol_iA,Xmol_mA
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN)  :: N,NLOC,NMAT
! 
      INTEGER :: K,ixx
! 
! -------
! ... Character variables     
! -------
! 
      CHARACTER(LEN = 8) :: PhTran 
! 
! -------
! ... Logical variables     
! -------
! 
      LOGICAL :: HDis_Switch,HApr_Switch 
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of PHS2_AqH_Kin
!
!
      IF(First_call) THEN
         First_call = .FALSE.
         IF(MPI_RANK==0) WRITE(11,6000)
      END IF
! 
! ... Some initializations 
! 
      PhTran      = '        '
      HDis_Switch = .FALSE.
! 
! -------
! ... Initialization of T-depression variables        
! -------
! 
      T_dev    = 0.0d0  
      Xmol_iA  = 0.0d0
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>      Main loop determining conditions and assigning properties      >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_NumEqs: DO K=1,NEQ1
!
! ----------
! ...... Initial assignments of primary variable values        
! ...... Primary variables: Px,S_aqu,X_mA, Tx        
! ----------
! 
         PX    = XX(1)
         S_aqu = XX(2)
         X_mA  = XX(3)
! 
         IF(NK == 3) THEN
            X_iA = 0.0d0
            TX   = XX(4)
         ELSE IF(NK == 4) THEN
            X_iA = XX(4)
            TX   = XX(5)
         END IF
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the first iteration - Unincremented variables 
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         IF_1stEq: IF(K == 1) THEN
! 
! -------------
! ......... Constraint on inhibitor mass fraction in the aqueous phase        
! -------------
! 
            IF_SaltMassF: IF (X_iA < 0.0d0) THEN
               X_iA       =   0.0d0
               XX(4)      =   0.0d0
               DX(NLOC+4) = - X(NLOC+4)
            END IF IF_SaltMassF
! 
! -------------
! ......... Determine PSAT: the H2O saturation pressure at TX                                                                       
! -------------
!                                                                       
            IGOOD = P_Sat_H2O_S(TX,Psat)
!
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
! 
! -------------
! ......... Constraint on CH4 mass fraction in the liquid phase        
! -------------
! 
            IF_CH4MassF: IF (X_mA < 0.0d0) THEN
! 
               IF(mop(5) >= 4) THEN
                  WRITE(6,6001) elem(n)%name,px,S_aqu,X_mA,tx,psat
               END IF
!                                                                       
               X_mA       = 0.0d0
               XX(3)      = 0.0d0
               DX(NLOC+3) = XX(3)-X(NLOC+3)
! 
            END IF IF_CH4MassF
! 
! -------------
! ......... Constraint on water saturation        
! -------------
! 
            IF_SwLim: IF (S_aqu <= 0.0d0) THEN
               S_aqu      = 1.0000d-4
               XX(2)      = S_aqu
               DX(NLOC+2) = XX(2)-X(NLOC+2)
            END IF IF_SwLim
!
!***********************************************************************
!*                                                                     *
!*      Determine the mole fractions and T-depression at Peq_Hyd       *
!*                                                                     *
!***********************************************************************
! 
! ......... Determine mole fraction of dissolved inhbitor in H2O       
! 
            XDEN =   (X_iA/MW_Inhib) + ( X_mA/MW_CH4) &
     &             + ( 1.0d0 - X_iA - X_mA )/MW_H2O
!
            Xmol_iA = (X_iA/MW_Inhib)/XDEN
            Xmol_mA = (X_mA/MW_CH4)/XDEN
						
! 
! ......... Determine the salt-induced suppression of the hydration T   
! 
            T_dev = T_MaxOff*DLOG(1.0d0 - Xmol_iA)  &
     &                      /DLOG(1.0d0 - C_MaxOff-1.0d-10)            
! 
! ......... Determine Peq_Hydr: the equilibrium hydration pressure at TX       
! 
            Peq_Hydr = EqPres_HydrCH4(TX+273.15d0,T_dev,&
     &                                F_EqOption,IGOOD)
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
! 
! ......... Determine the partial pressure of dissolved CH4 in H2O       
! 
            HK_GinH2O = HenryS_K(1,TX,Xmol_iA,.TRUE.)
            P_CH4     = HK_GinH2O*Xmol_mA 
!
!***********************************************************************
!*                                                                     *
!*                    Check phase transition from                      *
!*          2-phase (Aqueous + Hydrate) to 1-phase (Aqueous)           *
!*                                                                     *
!***********************************************************************
! 
! ......... Hydrate disappearance   
! 
            IF_HydDis: IF(S_aqu > 1.0d0) THEN
! 
               PhTran = 'AqH->Aqu'
               GO TO 1000
! 
            END IF IF_HydDis
!
!***********************************************************************
!*                                                                     *
!*                    Check phase transition from                      *
!*  2-phase (Aqueous + Hydrate) to 3-phase (Aqueous + Hydrate + Gas)   *
!*                                                                     *
!***********************************************************************
!
! ......... Compare Peq_Hydr to PX: Switching criteria       
! 
            P_evol = Peq_Hydr
! 
! ......... Gas evolution  
! 
            IF_GasEv: IF(PX*(1.0d0+ZERO) < P_evol) THEN
! 
               PhTran = 'AqH->AGH'
               GO TO 1000
! 
            ELSE
               GO TO 500 
            END IF IF_GasEv
! 
         END IF IF_1stEq
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the remaining iterations - Incremented variables
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         CASE_Incr: SELECT CASE(K)
! 
! ...... Incrementing pressure 
! 
         CASE(2)                    
!                                                                       
            DELX(NLOC+1) = DFAC*XX(1)
            PX           = XX(1)+DELX(NLOC+1)
! 
! ...... Incrementing the aqueous phase saturation 
! 
         CASE(3)                     
!                                                                       
            DELX(NLOC+2) = DFAC
            S_aqu        = XX(2)+DELX(NLOC+2)
! 
! ...... Incrementing the mass fraction of dissolved CH4 in H2O
! 
         CASE(4)                                
!                                                                       
            DELX(NLOC+3) = DFAC
            X_mA         = XX(3)+DELX(NLOC+3)
! 
! ...... Incrementing temperature (NK = 3) or salt mass fraction (NK = 4) 
! 
         CASE(5) 
! 
            IF(NK == 3) THEN
               X_iA         = 0.0d0
               DELX(NLOC+4) = DFAC*(273.15d0 + XX(4))
               TX           = XX(4) + DELX(NLOC+4)
            ELSE IF(NK == 4) THEN
               DELX(NLOC+4) = DFAC
               X_iA         = XX(4)+DELX(NLOC+4)
            END IF
! 
! ...... Incrementing temperature (NK = 4)
! 
         CASE(6) 
! 
            DELX(NLOC+5) = DFAC*(273.15d0 + XX(5))
            TX           = XX(5) + DELX(NLOC+5)
! 
         END SELECT CASE_Incr
!
!***********************************************************************
!*                                                                     *
!*         For augmented primary variables:                            *
!*             Determine the relevant parameters                       *
!*                                                                     *
!***********************************************************************
! 
         IF_KGT1: IF(k > 1) THEN
! 
! ......... Determine mole fraction of dissolved CH4 and inhbitor in H2O       
! 
            XDEN =     X_iA/MW_Inhib + X_mA/MW_CH4 &
     &             + ( 1.0d0 - X_iA - X_mA )/MW_H2O
!   
            Xmol_iA =  (X_iA/MW_Inhib)/XDEN                      
            Xmol_mA =  (X_mA/MW_CH4)/XDEN                      
! 
! ......... Determine the salt-induced suppression of the hydration T   
! 
            T_dev = T_MaxOff*DLOG(1.0d0-Xmol_iA)   &
     &                      /DLOG(1.0d0-C_MaxOff-1.0d-10)            
! 
! ......... Determine Peq_Hydr: the equilibrium hydration pressure at TX       
! 
            Peq_Hydr = EqPres_HydrCH4((TX + 273.15d0),T_dev,&
     &                                 F_EqOption,IGOOD)
! 
! ......... Determine the partial pressure of dissolved CH4 in H2O       
! 
            HK_GinH2O = HenryS_K(1,TX,Xmol_iA,.TRUE.)
            P_CH4     = HK_GinH2O*Xmol_mA 
!                                                                       
! ......... Obtain saturation pressure of H2O                                                                    
!                                                                       
            IGOOD = P_Sat_H2O_S(TX,Psat)
!
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
! 
         END IF IF_KGT1                                     
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... Compute thermophysical properties 
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
  500    S_Hydr = 1.0d0 - S_aqu
!                                                                       
! ...... Temperature in K                                                                       
!                                                                       
         T_k = TX + 273.15d0
!
!***********************************************************************
!*                                                                     *
!*             P R O P E R T I E S  +  P A R A M E T E R S             *
!*                                                                     *
!***********************************************************************
! 
! -------------
! ...... Mole fractions in the dissolved gas      
! -------------
! 
         Y_inG(1) = 1.0     ! CH4
         Y_inG(2) = 0.0d0   ! H2O
!                                                                      
! -------------
! ...... Determine the real gas density and compressibility
! -------------
!                                                                      
         CALL Zcomp(PX,T_k,0.0d0,Y_inG,ZLiq,ZGas,rho_G,rho_L,ixx)
!                                                                       
!                                                                       
! ...... Obtain density and internal energy of liquid H2O                                                                  
!                                                                       
         CALL UHRho_LiqH2O(TX,PX,DW,UW,HW,IGOOD)
! 
! ----------
! ...... Mixture model for aqueous phase density: Assume volume  
! ...... conservation during Hydrate dissolution  
! ----------
! 
         D_aqu = X_iA*D_Inhib+X_mA*rho_G+(1.0d0-X_iA-X_mA)*DW    
!                                                                       
! ...... Obtain H2O viscosity (infinite viscosity for hydrate)                                                              
!                                                                       
         V_aqu = ViscLV_H2Ox(TX,D_aqu) 
!                                                                       
! ...... Obtain density and enthalpy of the solid hydrate                                                                  
!                                                                       
         CALL HRho_Hydrate(TX,PX,D_Hydr,H_Hydr)
!                                                                      
! -------------
! ...... Relative permeability
! -------------
!                                                                      
         CALL RelPerm_Hyd(S_aqu,0.0d0,k_rel_aqu,k_rel_gas,dummy,NMAT)  
					                        
! 
! ----------
! ...... Compute the specific enthalpy and internal energy of the dissolved CH4 
! ----------
! 
         CALL HU_IdealGas(1,2.7315d2,T_k,Hi_Gas,Ui_Gas)                                  
         CALL HUS_Departure(T_k,ZGas,Hd_Gas,Ud_Gas,Sd_Gas) ! Departure enthalpy
!                                                                      
         H_Gas = (Hi_Gas - Hd_Gas)/MW_CH4     ! Convert to J/Kg/K from J/kgmol/K
         U_Gas = (Ui_Gas - Ud_Gas)/MW_CH4     
!
! ...... Dissolution enthalpy of CH4 at TX (J/kkg)
!
         H_Sol = HEAT_GasSolution(1,TX,Xmol_iA,HK_GinH2O,dummy)                                     
! 
! ----------
! ...... Compute the rate of kinetic hydrate dissociation/formation 
! ----------
! 
         IF(HApr_Switch .EQV. .TRUE.) THEN
! 
            CALL HRho_Hydrate(TX,PX,D_Hydr,H_Hydr)
            H_Rate = S_Hydr*D_Hydr*elem(n)%phi/FORD
!
         ELSE
            V_Hydr =  elem(n)%phi*S_Hydr  &        ! Compute the volume of each ...
     &               /PoMed(nmat)%NVoid           ! ... hydrate grain at time t 
! 
            R_Hydr = max(  PoMed(nmat)%RVoid  &    ! Compute the radius of the hydrate ...
     &                    *( V_Hydr          &     ! ... particle at time t 
     &                      /PoMed(nmat)%VVoid&
     &                     )**(1.0d0/3.0d0),&
     &                   1.0d-10)
! 
            SArea_Hyd(n) =    Area_Factor  &       ! Compute the hydrate surface area ... 
     &                       *PoMed(nmat)%SArea  & ! ... per unit volume at time t  
     &                       *R_Hydr*R_Hydr  &       
     &                     /( PoMed(nmat)%RVoid&
     &                       *PoMed(nmat)%RVoid)            
! 
            H_Rate = Rate_KineticDiss(n,P_CH4,&
     &                                Peq_Hydr, &    ! The equilibrium pressure (Pa)
     &                                TX,     &      ! The temperature (C)
     &                                0.0d0,  &      ! The salt molality
     &                                Y_inG,  &      ! The mole fraction vector
     &                                SArea_Hyd(n)) ! Flag for printing options
!
         END IF
! 
! ----------
! ...... Compute the heat of hydrate dissociation/formation 
! ----------
! 
         H_Heat = HeatDiss_HydrCH4(TX+273.15d0,&
     &                             F_EqOption, &  ! H-equation
     &                             IGOOD)
! 
! ----------
! ...... Assign value to state index  
! ----------
! 
         IF(k == 1) elem(n)%StateIndex = 5_1
! 
! ----------
! ...... Assign values of secondary parameters - Gas phase  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(1)   = 0.0d0
         Cell_V(n,k-1)%p_KRel(1)   = 0.0d0
         Cell_V(n,k-1)%p_Visc(1)   = 1.0d0
         Cell_V(n,k-1)%p_Dens(1)   = 0.0d0
         Cell_V(n,k-1)%p_Enth(1)   = 0.0d0
         Cell_V(n,k-1)%p_PCap(1)   = 0.0d0
         Cell_V(n,k-1)%p_MasF(1,1) = 0.0d0
         Cell_V(n,k-1)%p_MasF(2,1) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,1) = 0.0d0
         Cell_V(n,k-1)%p_MasF(4,1) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Aqueous phase
! ----------
! 
         Cell_V(n,k-1)%p_Satr(2)   = S_aqu
         Cell_V(n,k-1)%p_KRel(2)   = k_rel_aqu
         Cell_V(n,k-1)%p_Visc(2)   = V_aqu
         Cell_V(n,k-1)%p_Dens(2)   = D_aqu
         Cell_V(n,k-1)%p_Enth(2)   = (1.0d0 - X_mA - X_iA)*HW &
     &                              + X_mA*(H_Gas + H_Sol) &
     &                              + X_iA*H_InhSol
         Cell_V(n,k-1)%p_PCap(2)   = 0.0d0                                   
         Cell_V(n,k-1)%p_MasF(1,2) = 1.0d0 - X_mA - X_iA
         Cell_V(n,k-1)%p_MasF(2,2) = X_mA
         Cell_V(n,k-1)%p_MasF(3,2) = 0.0d0
         Cell_V(n,k-1)%p_MasF(4,2) = X_iA
! 
! ----------
! ...... Assign values of secondary parameters - Hydrate phase  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(3)   = S_Hydr
         Cell_V(n,k-1)%p_Dens(3)   = D_Hydr
         Cell_V(n,k-1)%p_Enth(3)   = H_Hydr
         Cell_V(n,k-1)%p_MasF(1,3) = 0.0d0
         Cell_V(n,k-1)%p_MasF(2,3) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,3) = 1.0d0
         Cell_V(n,k-1)%p_MasF(4,3) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Ice phase 
! ----------
! 
         Cell_V(n,k-1)%p_Satr(4)   = 0.0d0
         Cell_V(n,k-1)%p_Dens(4)   = 0.0d0
         Cell_V(n,k-1)%p_Enth(4)   = 0.0d0
         Cell_V(n,k-1)%p_MasF(1,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(2,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(4,4) = 0.0d0
! 
! ----------
! ...... Store the temperature 
! ----------
! 
         Cell_V(n,k-1)%TemC = TX
! 
! ----------
! ...... Store other important variables  
! ----------
! 
         IF(K == 1) THEN
! 
! ......... Store equilibrium hydration pressure at TX 
! 
            Cell_V(n,0)%p_AddD(1) = Peq_Hydr
! 
! ......... Store the hydrate temperature depression  
! 
            Cell_V(n,1)%p_AddD(1) = T_dev
! 
         END IF
! 
! ----------
! ...... Store rate of kinetic hydrate dissociation/formation 
! ----------
! 
         Cell_V(n,k-1)%p_AddD(2) = H_Rate
! 
! ----------
! ...... Store heat of dissociation/formation 
! ----------
! 
         Cell_V(n,k-1)%p_AddD(3) = H_Heat
! 
! 
! 
      END DO DO_NumEqs
! 
! 
! 
      RETURN     ! Task completed - Exit the routine
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                         PHASE TRANSITIONS                           >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
 1000 CONTINUE
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Aqueous + Hydrate (2-phase)
! .............. to Aqueous (1-phase)      
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
      IF_PhTran: IF(PhTran == 'AqH->Aqu') THEN
! 
                    IF(MOP(5) >= 3) WRITE(6,6002) elem(n)%name,S_aqu
                    IF(KC == 0) THEN
                       WRITE(6,6101) 
                       STOP                                             
                    END IF
!                                                                      
! ................. Adjust the 1st primary variable as the pressure
!                                                                      
                    XX(1)      = Peq_Hydr*9.999d-1             
                    DX(NLOC+1) = XX(1) - X(NLOC+1)
!                                                                      
! ................. Reset the 2nd primary variable as mass fraction of dissolved CH4
!                                                                      
                    XX(2)      = XX(3)                
                    DX(NLOC+2) = XX(2) - X(NLOC+2)
! 
                    XX(3)      = 0.0d0
                    DX(NLOC+3) = XX(3) - X(NLOC+3)
! 
! ................. Activate the logical switch trigger       
! 
                    HDis_Switch = .TRUE.
! 
! ................. Call the single-phase (H2O) routine 
! 
                    CALL PHS1_Aqu_Kin(N,NLOC,NMAT,HDis_Switch)
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Aqueous + Hydrate (2-phase)
! .............. to Aqueous + Gas + Hydrate (3-phase)      
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
                 ELSE IF(PhTran == 'AqH->AGH') THEN
! 
                    IF(MOP(5) >= 3) WRITE(6,6003) elem(n)%name,PX,P_evol
                    IF(KC == 0) THEN
                       WRITE(6,6101)                                              
                       STOP                                             
                    END IF
! 
! ................. Ensure that the pressure is set to the bubbling P at TX
! 
                    XX(1)      = P_evol*9.999d-1    
                    DX(NLOC+1) = XX(1) - X(NLOC+1)
! 
! ................. Reset the 3nd variable as the gas saturation 
! 
                    XX(3)      = 1.0d-4
                    DX(NLOC+3) = XX(3) - X(NLOC+3)
! 
! ................. Call the 3-phase (H2O+Hydrate+Gas) routine 
! 
                    CALL PHS3_AGH_Kin(N,NLOC,NMAT,.FALSE.)
! 
                 ELSE 
! 
                    WRITE(6,6102)
                    STOP
! 
                 END IF IF_PhTran
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'PHS2_AqH_Kin      1.0   29 September 2004',6X,  &
     &         'Routine for assigning the secondary ',  &
     &         'parameters of the 2-phase (Aqueous+Hydrate) ', &
     &       /,T48,'system - Kinetic hydrate reaction')
!
 6001 FORMAT(' !!!!!! "PHS2_AqH_Kin": Ice phase evolves at ', &
     &       'element "',A8,'" ==> TX = ',1pE15.8,' oC')
 6002 FORMAT(' !!!!!! "PHS2_AqH_Kin": Hydrate disappears at ',&
     &       'element "',A8,'" ==> S_aqu = ',1pE15.8)
 6003 FORMAT(' !!!!!! "PHS2_AqH_Kin": Gas phase evolves at ',&
     &       'element "',A8,'" ==> Px  = ',1pE15.8,'  P_evol = ',&
     &       1pE15.8)
! 
 6101 FORMAT(' !!!!!!!!!!   Routine PHS2_AqH_Kin: ERRONEOUS DATA ',&
     &       'INITIALIZATION   ==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
 6102 FORMAT(' !!!!!!!!!!   Routine PHS2_AqH_Kin: ',&
     &       'ERRONEOUS PHASE CHANGE   ==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of PHS2_AqH_Kin
! 
! 
      RETURN
!
      END SUBROUTINE PHS2_AqH_Kin
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE PHS2_AqG_Kin(N,NLOC,NMAT,HDis_Switch)
! 
! ... Modules to be used 
! 
      USE Universal_Param
      USE Basic_Param
      USE GenControl_Param
      USE Diffusion_Param
!
      USE H2O_Param
      USE Hydrate_Param
!
      USE Variable_Arrays
      USE Element_Arrays
      USE PFMedProp_Arrays
!
      USE H2O_Properties
      USE Hydrate_Properties
      USE RealGas_Properties
	  
	  USE MPI_param
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*       ROUTINE FOR COMPUTING AND ASSIGNING ALL THE SECONDARY         *
!*    VARIABLES IN TWO-PHASE (H2O+Gas) SYSTEMS - Extended T-scale      *
!*                                                                     *
!*                  Version 1.0 - September 29, 2004                   *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!                                                                       
      IMPLICIT NONE
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(2) :: Y_inG
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: PX,S_aqu,S_gas,TX,PS,P_evol,dummy,S_Hydr
      REAL(KIND = 8) :: ZLiq,ZGas,D_gas,rho_L,H_Sol
      REAL(KIND = 8) :: Hi_Gas,Ui_Gas,Hd_Gas,Ud_Gas,Sd_Gas,H_Gas,U_Gas
! 
      REAL(KIND = 8) :: V_aqu,DW,UW,HW,k_rel_aqu,k_rel_gas,P_cap
      REAL(KIND = 8) :: V_gas,D_aqu,T_k,D_Hydr,H_Hydr
! 
      REAL(KIND = 8) :: X_mG,X_wG
      REAL(KIND = 8) :: Xmol_mA,X_mA,X_wA
      REAL(KIND = 8) :: Peq_Hydr,P_CH4,HK_GinH2O,H_Rate,H_Heat
      REAL(KIND = 8) :: T_dev,X_iA,Xmol_iA,Xmol_mA_old,Xmol_iA_old
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN)  :: N,NLOC,NMAT
! 
      INTEGER :: K,ixx
! 
      INTEGER :: HPeq_Paramtrs,PCH4_Paramtrs   ! Function names
! 
! -------
! ... Character variables     
! -------
! 
      CHARACTER(LEN = 8) :: PhTran 
! 
! -------
! ... Logical variables     
! -------
! 
      LOGICAL :: HDis_Switch,HApr_Switch 
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of PHS2_AqG_Kin
!
!
      IF(First_call) THEN
         First_call = .FALSE.
         IF(MPI_RANK==0) WRITE(11,6000)
      END IF
! 
! ... Some initializations 
! 
      PhTran      = '        '
      HApr_Switch = .FALSE.
! 
! -------
! ... Initialization of T-depression variables        
! -------
! 
      T_dev    = 0.0d0  
      Xmol_mA  = 0.0d0
      Xmol_iA  = 0.0d0
! 
      Xmol_mA_old  = 0.0d0
      Xmol_iA_old  = 0.0d0
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>      Main loop determining conditions and assigning properties      >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_NumEqs: DO K=1,NEQ1
!
! ----------
! ...... Initial assignments of primary variable values        
! ...... Primary variables: Px, Sw, S_hydrate, Tx        
! ----------
! 
         PX      = XX(1)                                                      
         S_aqu   = XX(2)
         S_Hydr  = XX(3)
! 
         IF(NK == 3) THEN
            X_iA = 0.0d0
            TX   = XX(4)
         ELSE IF(NK == 4) THEN
            X_iA = XX(4)
            TX   = XX(5)
         END IF
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the first iteration - Unincremented variables
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         IF_1stEq: IF(K == 1) THEN
! 
! ......... Gas saturation       
! 
            S_gas = 1.0d0 - S_aqu
! 
! -------------
! ......... Constraint on inhibitor mass fraction in the aqueous phase        
! -------------
! 
            IF_SaltMassF: IF (X_iA < 0.0d0) THEN
               X_iA       =   0.0d0
               XX(4)      =   0.0d0
               DX(NLOC+4) = - X(NLOC+4)
            END IF IF_SaltMassF
! 
! -------------
! ......... Determine PSAT: the H2O saturation pressure at TX                                                                       
! -------------
!                                                                       
            IGOOD = P_Sat_H2O_S(TX,PS)
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
!
! -------------
! ......... Constraint on hydrate saturation       
! -------------
! 
            IF_CH4Lim: IF (S_Hydr < 0.0d0) THEN
!                                                                       
               S_Hydr     = 0.0d0                                                   
               XX(3)      = 0.0d0                                                    
               DX(NLOC+3) =-X(NLOC+3)                                        
! 
            END IF IF_CH4Lim
! 
! -------------
! ......... Actual CH4 partial pressure       
! -------------
! 
            P_CH4 = PX - PS                                          
!
!***********************************************************************
!*                                                                     *
!*            Determine iteratively Peq_Hyd and related info           *
!*                                                                     *
!***********************************************************************
!
            IGOOD = HPeq_Paramtrs(-2,TX,X_iA,.FALSE.,       &                  ! In only
     &                            Xmol_iA,Xmol_mA_old,Xmol_iA_old,T_dev,  &    ! In and out
     &                            Peq_Hydr,HK_GinH2O,Xmol_mA,X_mA)            ! Out only
!
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
!
!***********************************************************************
!*                                                                     *
!*                    Check phase transition from                      *
!*            2-phase (Aqueous + Gas) to 1-phase (Aqueous)             *
!*                                                                     *
!***********************************************************************
!
            IF_GasDis: IF(S_gas > (1.0d0+ZERO)) THEN
! 
               PhTran = 'AqG->Gas'
               GO TO 1000
! 
            END IF IF_GasDis
!
!***********************************************************************
!*                                                                     *
!*                    Check phase transition from                      *
!*            2-phase (Aqueous + Gas) to 1-phase (Aqueous)             *
!*                                                                     *
!***********************************************************************
!
            IF_AquDis: IF(S_aqu > (1.0d0+ZERO)) THEN
! 
               PhTran = 'AqG->Aqu'
               GO TO 1000
! 
            END IF IF_AquDis
!
!***********************************************************************
!*                                                                     *
!*                    Check phase transition from                      *
!*      2-phase (Aqueous + Gas) to 3-phase (Aqueous + Ice + Gas)       *
!*                                                                     *
!***********************************************************************
! 
! ......... Ice evolution  
! 
            IF_IceEv: IF(TX < 1.0d-2) THEN                                   
! 
               PhTran = 'AqG->AIG'
               GO TO 1000
! 
            END IF IF_IceEv
!
!***********************************************************************
!*                                                                     *
!*                    Check phase transition from                      *
!*    2-phase (Aqueous + Gas) to 3-phase (Aqueous + Hydrate + Gas)     *
!*                                                                     *
!***********************************************************************
!
! ......... Determine the CH4 partial pressure: Switching criterion      
! 
            P_evol = Peq_Hydr - PS                                      
! 
! ......... Hydrate evolution  
! 
            IF_HydrEv: IF(P_CH4 > P_evol) THEN                                   
! 
               PhTran = 'AqG->AGH'
               GO TO 1000
! 
            ELSE
               GO TO 500 
            END IF IF_HydrEv
! 
         END IF IF_1stEq
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the remaining iterations - Incremented variables 
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         CASE_Incr: SELECT CASE(K)
! 
! ...... Incrementing pressure 
! 
         CASE(2)                    
! 
            DELX(NLOC+1) = DFAC*XX(1)
            PX           = XX(1) + DELX(NLOC+1)
! 
! ...... Incrementing the aqueous phase saturation
! 
         CASE(3)                     
! 
            DELX(NLOC+2) = DFAC
            S_aqu        = XX(2) + DELX(NLOC+2)
! 
! ...... Incrementing the hydrate saturation
! 
         CASE(4)                                
! 
            DELX(NLOC+3) = DFAC
            S_Hydr       = 0.0d0
! 
! ...... Incrementing temperature (NK = 3) or salt mass fraction (NK = 4) 
! 
         CASE(5) 
! 
            IF(NK == 3) THEN
               X_iA         = 0.0d0
               DELX(NLOC+4) = DFAC*(273.15d0 + XX(4))
               TX           = XX(4) + DELX(NLOC+4)
            ELSE IF(NK == 4) THEN
               DELX(NLOC+4) = DFAC
               X_iA         = XX(4)+DELX(NLOC+4)
            END IF
! 
! ...... Incrementing temperature (NK = 4)
! 
         CASE(6) 
! 
            DELX(NLOC+5) = DFAC*(273.15d0 + XX(5))
            TX           = XX(5) + DELX(NLOC+5)
! 
         END SELECT CASE_Incr
!
!***********************************************************************
!*                                                                     *
!*         For augmented primary variables:                            *
!*             Determine the relevant parameters                       *
!*                                                                     *
!***********************************************************************
! 
         IF_GT1: IF(K > 1) THEN
! 
! ......... Determine the corresponding saturation pressure of H2O      
! 
            IGOOD = P_Sat_H2O_S(TX,PS)
! 
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
!                                                                       
!.......... Determine the CH4 partial pressure
!                                                                       
            P_CH4 = PX - PS
!                                                                       
!.......... Determine iteratively Peq_Hyd and related info
!
            IGOOD = HPeq_Paramtrs(-2,TX,X_iA,.FALSE.,      &                  ! In only
     &                            Xmol_iA,Xmol_mA_old,Xmol_iA_old,T_dev, &    ! In and out
     &                            Peq_Hydr,HK_GinH2O,Xmol_mA,X_mA)           ! Out only
!
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
! 
         END IF IF_GT1
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... Compute thermophysical properties 
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
  500    S_gas = 1.0d0 - S_aqu                                                     
!                                                                       
! ...... Temperature in K                                                                       
!                                                                       
         T_k = TX + 273.15d0
! 
! -------------
! ...... Mole fractions in the gas phase      
! -------------
! 
         Y_inG(1) = P_CH4/PX         ! CH4
         Y_inG(2) = 1.0d0-Y_inG(1)   ! H2O
!                                                                      
! ...... Compute mass fractions in the gas phase
!                                                                      
         X_mG =   MW_CH4*Y_inG(1)    &
     &         /( MW_CH4*Y_inG(1) + MW_H2O*Y_inG(2) )                                                  
!                                                                      
         X_wG = 1.0d0-X_mG                                                
!
!***********************************************************************
!*                                                                     *
!*    Determine iteratively the mole fractions in the aqueous phase    *
!*                                                                     *
!***********************************************************************
!
         IGOOD = PCH4_Paramtrs(-1,P_CH4,TX,X_iA,    &                         ! In only
     &                            Xmol_iA,Xmol_mA_old,Xmol_iA_old,T_dev, &    ! In and out 
     &                            HK_GinH2O,X_mA,Xmol_mA)                    ! Out only
! 
         IF(IGOOD /= 0) THEN
            CALL WARN_RF(elem(n)%name,XX,NK1,KC)
            RETURN
         END IF                                       
!
!***********************************************************************
!*                                                                     *
!*             P R O P E R T I E S  +  P A R A M E T E R S             *
!*                                                                     *
!***********************************************************************
!                                                                      
! ...... Determine the real gas density and compressibility
!                                                                      
         CALL Zcomp(PX,T_k,0.0d0,Y_inG,ZLiq,ZGas,D_gas,rho_L,ixx)
!                                                                      
! ...... Compute the viscosity of the multicomponent gas mixture
!                                                                      
         V_gas = Gas_Viscosity(PX,T_k,ZGas)                          
!                                                                      
! ...... Obtain the specific internal energy and enthalpy of the gas phase
!                                                                      
         CALL HU_IdealGas(0,2.7315d2,T_k,Hi_Gas,Ui_Gas)    ! Ideal component
         CALL HUS_Departure(T_k,ZGas,Hd_Gas,Ud_Gas,Sd_Gas) ! Departure ennthalpy
!                                                                      
         H_gas = ( Hi_Gas - Hd_Gas )  &                     ! Convert to J/Kg/K from J/kgmol/K
     &          /( Y_inG(1)*MW_CH4 + Y_inG(2)*MW_H2O )  
!                                                                      
         U_gas = ( Ui_Gas - Ud_Gas )   &                    ! Convert to J/Kg/K from J/kgmol/K
     &          /( Y_inG(1)*MW_CH4 + Y_inG(2)*MW_H2O )  
!                                                                      
! ...... Compute the heat of CH4 solution
!                                                                      
         H_Sol = HEAT_GasSolution(1,TX,Xmol_iA,HK_GinH2O,dummy)                                     
!                                                                      
! ...... Compute H2O mass fraction in the aqueous phase
!                                                                      
         X_wA = 1.0d0 - X_mA - X_iA                     
!                                                                      
! ...... Obtain the viscosity, density and internal energy of liquid H2O 
!                                                                      
         CALL UHRho_LiqH2O(TX,PX,DW,UW,HW,IGOOD)                                       
!                                                                      
! ...... Aqueous phase density                                                                       
!                                                                      
         D_aqu = X_wA*DW+X_iA*D_Inhib+X_mA*D_gas
!                                                                      
! ...... Compute the viscosity of the multicomponent gas mixture
!                                                                      
         V_aqu = ViscLV_H2Ox(TX,D_aqu)   
!                                                                      
! ----------
! ...... Obtain capillary pressures and relative permeabilities
! ----------
!                                                                      
         CALL CapPres_Hyd(TX,S_aqu,S_gas,D_aqu,P_cap,NMAT)          
         CALL RelPerm_Hyd(S_aqu,S_gas,k_rel_aqu,k_rel_gas,dummy,NMAT)         
! 
! ----------
! ...... Compute the rate of kinetic hydrate dissociation/formation 
! ----------
! 
         IF(HDis_Switch .EQV. .TRUE.) THEN
! 
            CALL HRho_Hydrate(TX,PX,D_Hydr,H_Hydr)
            H_Rate = -Cell_V(n,k-1)%p_Satr(3)*D_Hydr*elem(n)%phi/FORD
!
            H_Heat = HeatDiss_HydrCH4(TX+273.15d0,&
     &                                F_EqOption, &  ! H-equation
     &                                IGOOD)
!
         ELSE
            H_Rate = 0.0d0
            H_Heat = 0.0d0
         END IF
! 
! ----------
! ...... Assign value to state index  
! ----------
! 
         IF(k == 1) elem(n)%StateIndex = 3_1
! 
! ----------
! ...... Assign values of secondary parameters - Gas phase  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(1)   = S_gas
         Cell_V(n,k-1)%p_KRel(1)   = k_rel_gas 
         Cell_V(n,k-1)%p_Visc(1)   = V_gas
         Cell_V(n,k-1)%p_Dens(1)   = D_gas
         Cell_V(n,k-1)%p_Enth(1)   = H_Gas    
         Cell_V(n,k-1)%p_PCap(1)   = 0.0d0
         Cell_V(n,k-1)%p_MasF(1,1) = X_wG
         Cell_V(n,k-1)%p_MasF(2,1) = X_mG
         Cell_V(n,k-1)%p_MasF(3,1) = 0.0d0
         Cell_V(n,k-1)%p_MasF(4,1) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Aqueous phase 
! ----------
! 
         Cell_V(n,k-1)%p_Satr(2)   = S_aqu
         Cell_V(n,k-1)%p_KRel(2)   = k_rel_aqu
         Cell_V(n,k-1)%p_Visc(2)   = V_aqu
         Cell_V(n,k-1)%p_Dens(2)   = D_aqu
         Cell_V(n,k-1)%p_Enth(2)   =  X_wA*HW &
     &                              + X_mA*(H_Gas + H_Sol) &
     &                              + X_iA*H_InhSol
         Cell_V(n,k-1)%p_PCap(2)   = P_cap
         Cell_V(n,k-1)%p_MasF(1,2) = X_wA
         Cell_V(n,k-1)%p_MasF(2,2) = X_mA
         Cell_V(n,k-1)%p_MasF(3,2) = 0.0d0
         Cell_V(n,k-1)%p_MasF(4,2) = X_iA
! 
! ----------
! ...... Assign values of secondary parameters - Hydrate phase  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(3)   = 0.0d0
         Cell_V(n,k-1)%p_Dens(3)   = 0.0d0
         Cell_V(n,k-1)%p_Enth(3)   = 0.0d0
         Cell_V(n,k-1)%p_MasF(1,3) = 0.0d0
         Cell_V(n,k-1)%p_MasF(2,3) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,3) = 0.0d0
         Cell_V(n,k-1)%p_MasF(4,3) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Ice phase 
! ----------
! 
         Cell_V(n,k-1)%p_Satr(4)   = 0.0d0
         Cell_V(n,k-1)%p_Dens(4)   = 0.0d0
         Cell_V(n,k-1)%p_Enth(4)   = 0.0d0
         Cell_V(n,k-1)%p_MasF(1,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(2,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(4,4) = 0.0d0
! 
! ----------
! ...... Store the temperature 
! ----------
! 
         Cell_V(n,k-1)%TemC = TX
! 
! ----------
! ...... Store other important variables  
! ----------
! 
         IF(K == 1) THEN
! 
! ......... Store equilibrium hydration pressure at TX 
! 
            Cell_V(n,0)%p_AddD(1) = Peq_Hydr
! 
! ......... Store the hydrate temperature depression  
! 
            Cell_V(n,1)%p_AddD(1) = T_dev
! 
         END IF
! 
! ----------
! ...... Store rate of kinetic hydrate dissociation/formation 
! ----------
! 
         Cell_V(n,k-1)%p_AddD(2) = H_Rate
! 
! ----------
! ...... Store heat of dissociation/formation 
! ----------
! 
         Cell_V(n,k-1)%p_AddD(3) = H_Heat
! 
! 
! 
      END DO DO_NumEqs
! 
! 
! 
      RETURN     ! Task completed - Exit the routine
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                         PHASE TRANSITIONS                           >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
 1000 CONTINUE
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Aqueous + Gas (2-phase)
! .............. to Aqueous (1-phase)    
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
      IF_PhTran: IF(PhTran == 'AqG->Aqu') THEN
!                                                                       
                    IF(MOP(5) >= 3) WRITE(6,6002) elem(n)%name,S_aqu
                    IF(KC == 0) THEN
                       WRITE(6,6101) 
                       STOP                                             
                    END IF
! 
! ................. Reset 2nd variable as air mass fraction in water 
! 
                    XX(2)      = X_mA
                    DX(NLOC+2) = XX(2)-X(NLOC+2)
! 
! ................. Reset 3rd variable as the hydrate saturation
! 
                    XX(3)      = 0.0d0
                    DX(NLOC+3) = XX(3)-X(NLOC+3)
! 
! ................. Call the single-phase (Aqueous) subroutine 
! 
                    CALL PHS1_Aqu_Kin(N,NLOC,NMAT,.FALSE.)
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Aqueous + Gas (2-phase)
! .............. to Aqueous + Gas + Ice (3-phase)    
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
                 ELSE IF(PhTran == 'AqG->AIG') THEN
!                                                                       
                    IF(MOP(5) >= 3) WRITE(6,6002) elem(n)%name,TX
                    IF(KC == 0) THEN
                       WRITE(6,6101) 
                       STOP                                             
                    END IF
! 
! ................. Adjust the 2nd variable as the aqueous phase saturation 
! 
                    XX(2)      = S_aqu - 1.0d-4
                    DX(NLOC+2) = XX(2)-X(NLOC+2)
! 
! ................. Reset 4th or 5th variable as the gas saturation
! 
                    IF(NK == 3) THEN
                       XX(4)      = S_gas
                       DX(NLOC+4) = XX(4) - X(NLOC+4) 
                    ELSE IF(NK == 4) THEN
                       XX(5)      = S_gas
                       DX(NLOC+5) = XX(5) - X(NLOC+5) 
                    END IF
! 
! ................. Call the single-phase (Aqueous) subroutine 
! 
                    CALL PHS3_AIG_Kin(N,NLOC,NMAT,.FALSE.)
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Aqueous + Gas (2-phase)
! .............. to Aqueous + Gas + Hydrate (3-phase)    
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
                 ELSE IF(PhTran == 'AqG->AGH') THEN
! 
                    IF(MOP(5) >= 3) WRITE(6,6003) elem(n)%name,&
     &                                            P_CH4,Peq_Hydr
                       IF(KC == 0) THEN
                       WRITE(6,6101)                                              
                       STOP                                             
                    END IF
! 
! ................. Ensure that the pressure is set to slightly above the equilibrium P at TX
! 
                    XX(1)      = Peq_Hydr
                    DX(NLOC+1) = XX(1) - X(NLOC+1)
! 
! ................. Reset 3rd variable as the gas saturation
! 
                    XX(3)      = S_gas*(1.0d0 - ZERO)                                        
                    DX(NLOC+3) = XX(3) - X(NLOC+3)                                    
! 
! ................. Activate the logical switch trigger       
! 
                    HApr_Switch = .TRUE.
! 
! ................. Call the 3-phase (H2O+Hydrate+Gas) routine 
! 
                    CALL PHS3_AGH_Kin(N,NLOC,NMAT,HApr_Switch)
! 
                 ELSE 
! 
                    WRITE(6,6102)
                    STOP
! 
                 END IF IF_PhTran
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'PHS2_AqG_Kin      1.0   29 September 2004',6X,  &
     &         'Routine for assigning the secondary parameters of ',&
     &         'the 2-phase (Aqueous+Gas) ',   &
     &       /,T48,'system - Kinetic hydrate reaction')
!
 6001 FORMAT(' !!!!!! "PHS2_AqG_Kin": Aqueous phase disappears at ', &
     &       'element "',A8,'" ==> S_gas = ',1pE15.8)
 6002 FORMAT(' !!!!!! "PHS2_AqG_Kin": Gas phase disappears at ',  &
     &       'element "',A8,'" ==> S_aqu = ',1pE15.8)
 6003 FORMAT(' !!!!!! "PHS2_AqG_Kin": Hydrate evolves at ',   &      
     &       'element "',A8,'" ==> P_CH4 = ',1pE15.8,  &
     &       '  Peq_Hydr = ',1pE15.8) 
! 
 6101 FORMAT(' !!!!!!!!!!   Routine PHS2_AqG_Kin: ERRONEOUS DATA ',&
     &       'INITIALIZATION   ==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
 6102 FORMAT(' !!!!!!!!!!   Routine PHS2_AqG_Kin: ', &  
     &       'ERRONEOUS PHASE CHANGE   ==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of PHS2_AqG_Kin
! 
! 
      RETURN
!
      END SUBROUTINE PHS2_AqG_Kin
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE PHS3_AGH_Kin(N,NLOC,NMAT,HApr_Switch)
! 
! ... Modules to be used 
! 
      USE Universal_Param
      USE Basic_Param
      USE GenControl_Param
      USE Diffusion_Param
!
      USE H2O_Param
      USE Hydrate_Param
!
      USE Variable_Arrays
      USE Element_Arrays
      USE PFMedProp_Arrays
!
      USE H2O_Properties
      USE Hydrate_Properties
      USE RealGas_Properties
	  
	  USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*        ROUTINE FOR COMPUTING AND ASSIGNING ALL THE SECONDARY        *
!*          VARIABLES IN THREE-PHASE (H2O+Hydrate+Gas) SYSTEMS         *
!*                                                                     *
!*                  Version 1.0 - September 29, 2004                   *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!                                                                       
      IMPLICIT NONE
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(2) :: Y_inG
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: PX,TX,S_gas,S_aqu
      REAL(KIND = 8) :: Psat,DW,UW,HW,D_Hydr,H_Hydr,V_Hydr,R_Hydr
! 
      REAL(KIND = 8) :: ZLiq,ZGas,D_gas,rho_L,H_Sol,dummy
      REAL(KIND = 8) :: Hi_Gas,Ui_Gas,Hd_Gas,Ud_Gas,Sd_Gas,H_Gas,U_Gas
! 
      REAL(KIND = 8) :: V_aqu,V_gas,D_aqu,P_CH4,T_k
! 
      REAL(KIND = 8) :: P_cap,k_rel_gas,k_rel_aqu
! 
      REAL(KIND = 8) :: X_mG,X_wG,Xmol_mA,X_mA,X_wA
      REAL(KIND = 8) :: S_Hydr,Peq_Hydr,HK_GinH2O,H_Rate,H_Heat
      REAL(KIND = 8) :: T_dev,X_iA,Xmol_iA,Xmol_mA_old,Xmol_iA_old
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN)  :: N,NLOC,NMAT
! 
      INTEGER :: K,ixx
! 
      INTEGER :: HPeq_Paramtrs,PCH4_Paramtrs   ! Function names
! 
! -------
! ... Character variables     
! -------
! 
      CHARACTER(LEN = 8) :: PhTran 
! 
! -------
! ... Logical variables     
! -------
! 
      LOGICAL :: HDis_Switch,HApr_Switch 
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of PHS3_AGH_Kin
!
!
      IF(First_call) THEN
         IF(MPI_RANK==0) WRITE(11,6000)
         First_call = .FALSE.
      END IF
! 
! ... Some initializations 
! 
      PhTran   = '        '
      HDis_Switch = .FALSE.
! 
! -------
! ... Initialization of T-depression variables        
! -------
! 
      T_dev    = 0.0d0  
      Xmol_mA  = 0.0d0
      Xmol_iA  = 0.0d0
! 
      Xmol_mA_old  = 0.0d0
      Xmol_iA_old  = 0.0d0
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>      Main loop determining conditions and assigning properties      >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_NumEqs: DO K=1,NEQ1
!
! ----------
! ...... Initial assignments of primary variable values        
! ...... Primary variables: Px, S_aqu, S_gas, Tx        
! ----------
! 
         PX    = XX(1)                                                      
         S_aqu = XX(2)                                     
         S_gas = XX(3)                                
! 
         IF(NK == 3) THEN
            X_iA = 0.0d0
            TX   = XX(4)
         ELSE IF(NK == 4) THEN
            X_iA = XX(4)
            TX   = XX(5)
         END IF
! 
! ...... Ensure realistic saturation values
! 
         IF((S_gas > 1.0d0 .AND. S_aqu > 1.0d0) .OR. &
     &      (S_gas < 0.0d0 .AND. S_aqu < 0.0d0)) &
     &   THEN
            IGOOD = 2
            RETURN
         END IF
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the first iteration - Unincremented variables 
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         IF_1stEq: IF(K == 1) THEN
! 
! -------------
! ......... Constraint on inhibitor mass fraction in the aqueous phase        
! -------------
! 
            IF_SaltMassF: IF (X_iA < 0.0d0) THEN
               X_iA       =   0.0d0
               XX(4)      =   0.0d0
               DX(NLOC+4) = - X(NLOC+4)
            END IF IF_SaltMassF
! 
! -------------
! ......... Constraint on water saturation        
! -------------
! 
            IF_SwLim: IF (S_aqu <= 0.0d0) THEN
               S_aqu      = 1.0000d-4
               XX(2)      = S_aqu
               DX(NLOC+2) = XX(2)-X(NLOC+2)
            END IF IF_SwLim
!
! -------------
! ......... The hydrate saturation
! -------------
!
            S_Hydr = 1.0d0 - S_gas - S_aqu                               
! 
! ......... Ensure that kinetic hydrate disappearance occurs at a small starting saturation
! 
            IF(S_Hydr < 0.0d0 .AND. Cell_V(n,0)%p_Satr(3) > 2.0d-2) THEN
               IGOOD = 2
               RETURN
            END IF
!
!***********************************************************************
!*                                                                     *
!*                  Determine iteratively the T_shift                  *
!*                                                                     *
!***********************************************************************
!
            IGOOD = HPeq_Paramtrs(-1,TX,X_iA,.FALSE.,    &                     ! In only
     &                            Xmol_iA,Xmol_mA_old,Xmol_iA_old,T_dev, &     ! In and out
     &                            Peq_Hydr,HK_GinH2O,Xmol_mA,X_mA)            ! Out only
! 
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
!
!***********************************************************************
!*                                                                     *
!*    Check phase transition from Aqueous + Hydrate + Gas (3-phase)    *
!*                    to Aqueous + Hydrate (2-phase)                   *
!*                                                                     *
!***********************************************************************
!
            IF_GasDis: IF(S_gas < 0.0d0) THEN
!
               PhTran = 'AGH->AqH'
               GO TO 1000
!
            END IF IF_GasDis
!
!***********************************************************************
!*                                                                     *
!*    Check phase transition from Aqueous + Hydrate + Gas (3-phase)    *
!*                      to Aqueous + Gas (2-phase)                     *
!*                                                                     *
!***********************************************************************
!
            IF_HydrDis: IF(S_Hydr < 0.0d0 .AND. &
     &                     ABS(S_Hydr) > 1.0d-9) &
     &      THEN
! 
               PhTran = 'AGH->AqG'
               GO TO 1000
! 
            END IF IF_hydrDis
!
!***********************************************************************
!*                                                                     *
!*    Check phase transition from Aqueous + Hydrate + Gas (3-phase)    *
!*     to Aqueous + Hydrate + Gas + Ice (4-phase: Quadruple point)     *
!*                                                                     *
!***********************************************************************
!
            IF_IceEvol: IF((TX+T_dev) < Tc_quad_hyd) THEN
! 
               PhTran = 'AGH->QuP'
               GO TO 1000
! 
            ELSE
! 
               GO TO 500 
! 
            END IF IF_IceEvol
! 
         END IF IF_1stEq
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the remaining iterations - Incremented variables  
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 

         CASE_Incr: SELECT CASE(K)
! 
! ...... Incrementing pressure 
! 
         CASE(2)                    
! 
            DELX(NLOC+1) = DFAC*XX(1)
            PX           = XX(1) + DELX(NLOC+1)
! 
! ...... Incrementing mass fraction of dissolved CH4 in H2O 
!Why incrementing mass fractin when Primary variable is s_aqu !RPS 
!Is it a typo mistake because actually they are calculating S_aqu
! 
         CASE(3)                     
! 
            DELX(NLOC+2) = DFAC
            S_aqu        = XX(2) + DELX(NLOC+2)
! 
! ...... Incrementing the gas saturation
! 
         CASE(4)                                
! 
            DELX(NLOC+3) = DFAC
            S_gas        = XX(3) + DELX(NLOC+3)
! 
! ...... Incrementing temperature (NK = 3) or salt mass fraction (NK = 4) 
! 
         CASE(5) 
! 
            IF(NK == 3) THEN
               X_iA         = 0.0d0
               DELX(NLOC+4) = DFAC*(273.15d0 + XX(4))
               TX           = XX(4) + DELX(NLOC+4)
            ELSE IF(NK == 4) THEN
               DELX(NLOC+4) = DFAC
               X_iA         = XX(4)+DELX(NLOC+4)
            END IF
! 
! ...... Incrementing temperature (NK = 4)
! 
         CASE(6) 
! 
            DELX(NLOC+5) = DFAC*(273.15d0 + XX(5))
            TX           = XX(5) + DELX(NLOC+5)
! 
         END SELECT CASE_Incr
!
!***********************************************************************
!*                                                                     *
!*         For augmented primary variables:                            *
!*             Determine the relevant parameters                       *
!*                                                                     *
!***********************************************************************
! 
         IF_KGT1: IF (K > 1) THEN 
! 
            S_Hydr = 1.0d0 - S_gas - S_aqu  ! The augmented hydrate saturation
! 
! ......... Determine Peq_Hydr: the equilibrium hydration pressure at TX       
! 
            IGOOD = HPeq_Paramtrs(-2,TX,X_iA,.FALSE.,  &                      
     &                            Xmol_iA,Xmol_mA_old,Xmol_iA_old,T_dev,  &
     &                            Peq_Hydr,HK_GinH2O,Xmol_mA,X_mA)          
! 
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
! 
         END IF IF_KGT1                                       
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... Compute thermophysical properties 
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
  500    T_k = TX + 273.15d0  ! The augmented temperature in K 
! 
! ----------
! ...... Determine Psat: the saturation pressure of H2O at TX       
! ----------
! 
         IGOOD = P_Sat_H2O_S(TX,Psat)
!
         IF(IGOOD /= 0) THEN
            CALL WARN_RF(elem(n)%name,XX,NK1,KC)
            RETURN
         END IF      
!
!***********************************************************************
!*                                                                     *
!*   Determine iteratively the mole fractions corresponding to P_CH4   *
!*                                                                     *
!***********************************************************************
!
         P_CH4 = PX - Psat
!
         IGOOD = PCH4_Paramtrs(-1,P_CH4,TX,X_iA,    &                        ! In only
     &                            Xmol_iA,Xmol_mA_old,Xmol_iA_old,T_dev,   & ! In and out 
     &                            HK_GinH2O,X_mA,Xmol_mA)                   ! Out only
! 
         IF(IGOOD /= 0) THEN
            CALL WARN_RF(elem(n)%name,XX,NK1,KC)
            RETURN
         END IF                                       
!
!***********************************************************************
!*                                                                     *
!*             P R O P E R T I E S  +  P A R A M E T E R S             *
!*                                                                     *
!***********************************************************************
!
! ...... Internal energy and density of liquid H2O and Hydrate
!
         CALL UHRho_LiqH2O(TX,PX,DW,UW,HW,IGOOD)                         
!
! ...... Internal energy and density the solid hydrate
!
         CALL HRho_Hydrate(TX,PX,D_Hydr,H_Hydr)
! 
! -------------
! ...... Mole fractions in the gas phase      
! -------------
! 
         Y_inG(1) = 1.0d0-(Psat/PX)          ! CH4
         Y_inG(2) = 1.0d0-Y_inG(1)           ! H2O
!
! ...... Compute mass fractions in the gas phase
!
         X_mG =   MW_CH4*Y_inG(1)   &
     &         /( MW_CH4*Y_inG(1) + MW_H2O*Y_inG(2) )                                                  
!
         X_wG = 1.0d0 - X_mG                                                
!
! ...... Determine the gas density and compressibility
!
         CALL Zcomp(PX,T_k,0.0d0,Y_inG,ZLiq,ZGas,D_gas,rho_L,ixx)
!                                                                      
! ...... Obtain the specific internal energy and enthalpy of CH4
!                                                                      
         CALL HU_IdealGas(0,2.7315d2,T_k,Hi_Gas,Ui_Gas)    ! Ideal enthalpy
         CALL HUS_Departure(T_k,ZGas,Hd_Gas,Ud_Gas,Sd_Gas) ! Departure enthalpy
!                                                                      
         H_gas = ( Hi_Gas - Hd_Gas )   &                ! Convert to J/Kg/K from J/kgmol/K
     &          /( Y_inG(1)*MW_CH4 + Y_inG(2)*MW_H2O )  
!                                                                      
         U_gas = ( Ui_Gas - Ud_Gas )   &                 ! Convert to J/Kg/K from J/kgmol/K
     &          /( Y_inG(1)*MW_CH4 + Y_inG(2)*MW_H2O )  
!
! ...... Mass fraction of H2O in the aqueous phase
!
         X_wA = 1.0d0 - X_mA - X_iA                     
!                                                                      
! ...... Obtain the heat of CH4 solution in H2O
!                                                                      
         H_Sol = HEAT_GasSolution(1,TX,Xmol_iA,HK_GinH2O,dummy)                                     
!                                                                      
! ...... Multicomponent gas viscosity                                                                       
!                                                                      
         V_gas = Gas_Viscosity(PX,T_k,ZGas)                          
!                                                                      
! ...... Aqueous phase density (assumed equal to water)                                                                      
!                                                                      
         D_aqu = X_wA*DW+X_iA*D_Inhib+X_mA*D_gas
!                                                                      
! ...... Liquid H2O viscosity                                                                      
!                                                                      
         V_aqu = ViscLV_H2Ox(TX,D_aqu)                         
!                                                                      
! -------------
! ..... Relative permeability and capillary pressure 
! -------------
!                                                                      
         CALL RelPerm_Hyd(S_aqu,S_gas,k_rel_aqu,k_rel_gas,dummy,nmat)                          
         CALL CapPres_Hyd(TX,S_aqu,S_gas,D_aqu,P_cap,nmat)
! 
! ----------
! ...... Compute the rate of kinetic hydrate dissociation/formation 
! ----------
! 
         IF(HApr_Switch .EQV. .TRUE.) THEN
! 
            CALL HRho_Hydrate(TX,PX,D_Hydr,H_Hydr)
            H_Rate = S_Hydr*D_Hydr*elem(n)%phi/FORD
!
         ELSE
            V_Hydr =  elem(n)%phi*S_Hydr  &        ! Compute the volume of each ...
     &               /PoMed(nmat)%NVoid           ! ... hydrate grain at time t 
! 
            R_Hydr = max(  PoMed(nmat)%RVoid &     ! Compute the radius of the hydrate ...
     &                    *( V_Hydr         &      ! ... particle at time t 
     &                      /PoMed(nmat)%VVoid&
     &                     )**(1.0d0/3.0d0),&
     &                   1.0d-10)
! 

            SArea_Hyd(n) =    Area_Factor &        ! Compute the hydrate surface area ... 
     &                       *PoMed(nmat)%SArea &  ! ... per unit volume at time t  
     &                       *R_Hydr*R_Hydr  &       
     &                     /( PoMed(nmat)%RVoid&
     &                       *PoMed(nmat)%RVoid)            
! 
            H_Rate = Rate_KineticDiss(n,PX,&
     &                                Peq_Hydr,&     ! The equilibrium pressure (Pa)
     &                                TX,   &        ! The temperature (C)
     &                                0.0d0, &       ! The salt molality
     &                                Y_inG,  &      ! The mole fraction vector
     &                                SArea_Hyd(n))                   !where M_hydrate = (MW_CH4+ (6*MW_H2O)) = 124.13 gm/mol
!*(1/(MW_CH4+ (6*MW_H2O))) ! RPS
!
         END IF
		 
! 
! ----------
! ...... Compute the heat of hydrate dissociation/formation 
! ----------
! 
         H_Heat = HeatDiss_HydrCH4(TX+273.15d0,&
     &                             F_EqOption,   &! H-equation
     &                             IGOOD)
! 
! ----------
! ...... Assign value to state index  
! ----------
! 
         IF(k == 1) elem(n)%StateIndex = 7_1
! 
! ----------
! ...... Assign values of secondary parameters - Vapor  
! ----------
! 
				
         Cell_V(n,k-1)%p_Satr(1)   = S_gas
         Cell_V(n,k-1)%p_KRel(1)   = k_rel_gas
         Cell_V(n,k-1)%p_Visc(1)   = V_gas
         Cell_V(n,k-1)%p_Dens(1)   = D_gas
         Cell_V(n,k-1)%p_Enth(1)   = H_Gas               
         Cell_V(n,k-1)%p_PCap(1)   = 0.0d0  
         Cell_V(n,k-1)%p_MasF(1,1) = X_wG
         Cell_V(n,k-1)%p_MasF(2,1) = X_mG
         Cell_V(n,k-1)%p_MasF(3,1) = 0.0d0
         Cell_V(n,k-1)%p_MasF(4,1) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Aqueous  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(2)   = S_aqu
         Cell_V(n,k-1)%p_KRel(2)   = k_rel_aqu
         Cell_V(n,k-1)%p_Visc(2)   = V_aqu
         Cell_V(n,k-1)%p_Dens(2)   = D_aqu
         Cell_V(n,k-1)%p_Enth(2)   =  X_wA*HW &
     &                              + X_mA*(H_Gas + H_Sol) &
     &                              + X_iA*H_InhSol
         Cell_V(n,k-1)%p_PCap(2)   = P_cap
         Cell_V(n,k-1)%p_MasF(1,2) = X_wA
         Cell_V(n,k-1)%p_MasF(2,2) = X_mA
         Cell_V(n,k-1)%p_MasF(3,2) = 0.0d0
         Cell_V(n,k-1)%p_MasF(4,2) = X_iA
! 
! ----------
! ...... Assign values of secondary parameters - Organic phase  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(3)   = S_Hydr
         Cell_V(n,k-1)%p_Dens(3)   = D_Hydr
         Cell_V(n,k-1)%p_Enth(3)   = H_Hydr
         Cell_V(n,k-1)%p_MasF(1,3) = 0.0d0
         Cell_V(n,k-1)%p_MasF(2,3) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,3) = 1.0d0
         Cell_V(n,k-1)%p_MasF(4,3) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Ice phase 
! ----------
! 
         Cell_V(n,k-1)%p_Satr(4)   = 0.0d0
         Cell_V(n,k-1)%p_Dens(4)   = 0.0d0
         Cell_V(n,k-1)%p_Enth(4)   = 0.0d0
         Cell_V(n,k-1)%p_MasF(1,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(2,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(4,4) = 0.0d0
! 
! ----------
! ...... Store the temperature 
! ----------
! 
         Cell_V(n,k-1)%TemC = TX
! 
! ----------
! ...... Store other important variables  
! ----------
! 
         IF(K == 1) THEN
! 
! ......... Store equilibrium hydration pressure at TX 
! 
            Cell_V(n,0)%p_AddD(1) = Peq_Hydr
! 
! ......... Store the hydrate temperature depression  
! 
            Cell_V(n,1)%p_AddD(1) = T_dev
! 
         END IF
! 
! ----------
! ...... Store rate of kinetic hydrate dissociation/formation 
! ----------
! 
         Cell_V(n,k-1)%p_AddD(2) = H_Rate
! 
! ----------
! ...... Store heat of dissociation/formation 
! ----------
! 
         Cell_V(n,k-1)%p_AddD(3) = H_Heat
! 
! 
! 
      END DO DO_NumEqs
! 
! 
! 
      RETURN     ! Task completed - Exit the routine
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                         PHASE TRANSITIONS                           >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
 1000 CONTINUE
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Aqueous+Hydrate+Gas (3-phase) 
! .............. to Aqueous+Hydrate (2-phase)      
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
      IF_PhTran: IF(PhTran == 'AGH->AqH') THEN
!                                                                       
         IF(MOP(5) >= 3) WRITE(6,6001) elem(n)%name,S_gas
         IF(KC == 0) THEN
            WRITE(6,6101) 
            STOP                                             
         END IF
! 
! ...... Reset 3rd variable as air mass fraction in water 
! 
         XX(3)      = X_mA
         DX(NLOC+3) = XX(3)-X(NLOC+3)
! 
! ...... Water saturation constraint 
! 
         IF_WatSat_A: IF(S_aqu >= 1.0d0) THEN
            S_aqu      = 1.0d0 - ZERO
            XX(2)      = S_aqu
            DX(NLOC+2) = XX(2) - X(NLOC+2)
         END IF IF_WatSat_A
! 
! ...... Call the 2-phase (H2O+Hydrate) routine 
! 
         CALL PHS2_AqH_Kin(N,NLOC,NMAT,.FALSE.)
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
!  .............. Transition from Aqueous+Hydrate+Gas (3-phase)       
!  .............. to Aqueous+Gas (2-phase)      
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! 
      ELSE IF(PhTran == 'AGH->AqG') THEN
!
         IF(MOP(5) >= 3) WRITE(6,6002) elem(n)%name,S_hydr
         IF(KC == 0) THEN
            WRITE(6,6101) 
            STOP                                             
         END IF
! 
! ...... Reset the 3rd variable as the hydrate saturation
! 
         XX(3) = 0.0d0                      
         DX(NLOC+3) = XX(3) - X(NLOC+3)                        
!                                                                       
! ...... Water saturation constraint
!                                                                       
         IF_WatSat_B: IF(S_aqu >= 1.0d0) THEN
            S_aqu      = 1.0d0 - ZERO
            XX(2)      = S_aqu
            DX(NLOC+2) = XX(2) - X(NLOC+2)
         END IF IF_WatSat_B
! 
! ...... Activate the logical switch trigger       
! 
         HDis_Switch = .TRUE.
! 
! ...... Call the 2-phase (H2O+Gas) routine 
! 
         CALL PHS2_AqG_Kin(N,NLOC,NMAT,HDis_Switch)
! 
! 
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
!  .............. Transition from Aqueous+Hydrate+Gas (3-phase)       
!  .............. to Quadruple point (4-phases: Aqueous+Hydrate+Gas+Ice)      
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! 
      ELSE IF(PhTran == 'AGH->QuP') THEN
!                                                                       
         IF(MOP(5) >= 3) WRITE(6,6005) elem(n)%name,TX
         IF(KC == 0) THEN
            WRITE(6,6101) 
            STOP                                             
         END IF
! 
! ...... Reset 1st variable as pressure at the quadruple point
! 
         XX(1)      = P_quad_hyd
         DX(NLOC+1) = XX(1) - X(NLOC+1)
! 
! ...... Reset 4nd or 5th variable as ice 
! 
         IF(NK == 3) THEN
            XX(4)      = ZERO
            DX(NLOC+4) = XX(4) - X(NLOC+4)
         ELSE
            XX(5)      = ZERO
            DX(NLOC+5) = XX(5) - X(NLOC+5)
         END IF
! 
! ...... Call the 4-phase (Aqueous+Hydrate+Gas+Ice) routine 
! 
         CALL PHS4_QuP_Kin(N,NLOC,NMAT,.FALSE.)
! 
      ELSE 
! 
         WRITE(6,6102)
         STOP
! 
      END IF IF_PhTran
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'PHS3_AGH_Kin      1.0   29 September 2004',6X, &
     &         'Routine for assigning the secondary ',  &
     &         'parameters of the 3-phase (Aqueous+Gas+Hydrate)', &
     &       /,T48,'system - Kinetic hydrate reaction')
!
 6001 FORMAT(' !!!!!! "PHS3_AGH_Kin": Gas phase disappears at ', &
     &       'element "',A8,'" ==> S_gas = ',1pE15.8)
 6002 FORMAT(' !!!!!! "PHS3_AGH_Kin": Hydrate disappears at ', &
     &       'element "',A8,'" ==> S_hydr = ',1pE15.8)
 6005 FORMAT(' !!!!!! "PHS3_AGH_Kin": Ice evolves at element "',A8,&
     &       '" ==> TX = ',1pE15.8)
! 
 6101 FORMAT(' !!!!!!!!!!   Routine PHS3_AGH_Kin: ERRONEOUS DATA ',& 
     &       'INITIALIZATION   ==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
 6102 FORMAT(' !!!!!!!!!!   Routine PHS3_AGH_Kin: ',&
     &       'ERRONEOUS PHASE CHANGE   ==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of PHS3_AGH_Kin
! 
! 
      RETURN
!
      END SUBROUTINE PHS3_AGH_Kin
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE PHS3_AIG_Kin(N,NLOC,NMAT,HDis_Switch)
! 
! ... Modules to be used 
! 
      USE Universal_Param
      USE Basic_Param
      USE GenControl_Param
      USE Diffusion_Param
!
      USE H2O_Param
      USE Hydrate_Param
!
      USE Variable_Arrays
      USE Element_Arrays
      USE PFMedProp_Arrays
!
      USE H2O_Properties
      USE Hydrate_Properties
      USE RealGas_Properties
	  
	  USE MPI_Param
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*        ROUTINE FOR COMPUTING AND ASSIGNING ALL THE SECONDARY        *
!*        VARIABLES IN THREE-PHASE (Aqueous+Ice+Hydrate) SYSTEMS       *
!*                                                                     *
!*                  EQUILIBRIUM HYDRATION REACTION                     *
!*                                                                     *
!*                  Version 1.0 - January 20, 2005                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!                                                                       
      IMPLICIT NONE
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(2) :: Y_inG
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: PX,TX,Psat,S_gas,S_aqu
      REAL(KIND = 8) :: DW,UW,HW,P_CH4
      REAL(KIND = 8) :: S_ice,D_ice,H_ice,S_Hydr,D_Hydr,H_Hydr
! 
      REAL(KIND = 8) :: ZLiq,ZGas,D_gas,rho_L,H_Sol,D_aqu,dummy
      REAL(KIND = 8) :: Hi_Gas,Ui_Gas,Hd_Gas,Ud_Gas,Sd_Gas,H_Gas,U_Gas
! 
      REAL(KIND = 8) :: V_aqu,V_gas,T_k
! 
      REAL(KIND = 8) :: k_rel_aqu,k_rel_gas,P_cap
! 
      REAL(KIND = 8) :: X_mG,X_wG,Xmol_mA,X_mA,X_wA
      REAL(KIND = 8) :: HK_GinH2O,H_Heat,H_Rate
      REAL(KIND = 8) :: T_dev,X_iA,Xmol_iA,Xmol_mA_old,Xmol_iA_old
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN)  :: N,NLOC,NMAT
! 
      INTEGER :: K,ixx
! 
      INTEGER :: PTXS_Paramtrs  ! Integer function
! 
! -------
! ... Character variables     
! -------
! 
      CHARACTER(LEN = 8) :: PhTran 
! 
! -------
! ... Logical variables     
! -------
! 
      LOGICAL :: HDis_Switch,HApr_Switch 
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of PHS3_AIG_Kin
!
!
      IF(First_call) THEN
         IF(MPI_RANK==0) WRITE(11,6000)
         First_call = .FALSE.
      END IF
! 
! ... Some initializations 
! 
      PhTran = '        '
      HApr_Switch = .FALSE.
! 
! -------
! ... Initialization of T-depression variables        
! -------
! 
      T_dev    = Cell_V(n,1)%p_AddD(1)
      Xmol_mA  = 0.0d0
      Xmol_iA  = 0.0d0
! 
      Xmol_mA_old  = 0.0d0
      Xmol_iA_old  = 0.0d0
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>      Main loop determining conditions and assigning properties      >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_NumEqs: DO K=1,NEQ1
!
! ----------
! ...... Initial assignments of primary variable values        
! ...... Primary variables: P_gas, S_aqu, S_hydr, S_gas       
! ----------
! 
         PX     = XX(1)                                                      
         S_aqu  = XX(2)                                     
         S_hydr = XX(3)
! 
         IF(NK == 3) THEN
            X_iA  = 0.0d0
            S_gas = XX(4)                                     
         ELSE IF(NK == 4) THEN
            X_iA  = XX(4)
            S_gas = XX(5)                                     
         END IF
! 
! ...... Ensure realistic saturation values
! 
         IF((S_gas > 1.0d0 .AND. S_aqu > 1.0d0) .OR. &
     &      (S_gas < 0.0d0 .AND. S_aqu < 0.0d0)) &
     &   THEN
            IGOOD = 2
            RETURN
         END IF
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the first iteration - Unincremented variables
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         IF_1stEq: IF(K == 1) THEN
!
! -------------
! ......... Hydrate saturation
! -------------
!
            S_ice = 1.0d0 - S_aqu - S_gas
! 
! -------------
! ......... Constraint on inhibitor mass fraction in the aqueous phase        
! -------------
! 
            IF_SaltMassF: IF (X_iA < 0.0d0) THEN
               X_iA       =   0.0d0
               XX(3)      =   0.0d0
               DX(NLOC+3) = - X(NLOC+3)
            END IF IF_SaltMassF
!
! -------------
! ......... Temperature
! -------------
!
            TX = 1.0d-2   ! Initial temperature estimate  
!
!***********************************************************************
!*                                                                     *
!*    Determine iteratively partial P, T, T-dev and CH4 solubility     *
!*                                                                     *
!***********************************************************************
!
            IGOOD = PTXS_Paramtrs(-1,PX,X_iA,         &                   ! In only
     &                   TX,Xmol_iA,Xmol_mA_old,Xmol_iA_old,T_dev,  &     ! In and out 
     &                   P_CH4,Psat,HK_GinH2O,X_mA,Xmol_mA)              ! Out only
! 
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
!
!***********************************************************************
!*                                                                     *
!*      Check phase transition from Aqueous + Ice + Gas (3-phase)      *
!*                     to Aqueous + Gas (2-phase)                      *
!*                                                                     *
!***********************************************************************
!
            IF_IceDis: IF(S_ice < 0.0d0) THEN
! 
               PhTran = 'AIG->AqG'
               GO TO 1000
! 
            END IF IF_IceDis
!
!***********************************************************************
!*                                                                     *
!*      Check phase transition from Aqueous + Ice + Gas (3-phase)      *
!*     to Aqueous + Hydrate + Ice + Gas (4-phase, Quadruple point)     *
!*                                                                     *
!***********************************************************************
!
            IF_HydEv: IF(PX > P_quad_hyd) THEN
!
               PhTran = 'AIG->QuP'
               GO TO 1000
! 
            ELSE
! 
               GO TO 500 
! 
            END IF IF_HydEv
! 
         END IF IF_1stEq
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the remaining iterations - Incremented variables
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         CASE_Incr: SELECT CASE(k)
! 
! ...... Incrementing pressure 
! 
         CASE(2)                    
! 
            DELX(NLOC+1) =-DFAC*XX(1)
            PX           = XX(1) + DELX(NLOC+1)
! 
! ...... Incrementing the aqueous phase saturation  
! 
         CASE(3)                     
! 
            DELX(NLOC+2) = DFAC
            S_aqu        = XX(2) + DELX(NLOC+2)
! 
! ...... Incrementing the gas phase saturation  
! 
         CASE(4)                     
! 
            DELX(NLOC+3) = DFAC
            S_hydr       = 0.0d0
! 
! ...... Incrementing temperature (NK = 3) or salt mass fraction (NK = 4) 
! 
         CASE(5) 
! 
            IF(NK == 3) THEN
               X_iA         = 0.0d0
               DELX(NLOC+4) = DFAC
               S_gas        = XX(4) + DELX(NLOC+4)
            ELSE IF(NK == 4) THEN
               DELX(NLOC+4) = DFAC
               X_iA         = XX(4)+DELX(NLOC+4)
            END IF
! 
! ...... Incrementing temperature (NK = 3)
! 
         CASE(6) 
! 
            DELX(NLOC+5) = DFAC
            S_gas        = XX(5) + DELX(NLOC+5)
! 
         END SELECT CASE_Incr
!
!***********************************************************************
!*                                                                     *
!*         For augmented primary variables:                            *
!*             Determine the relevant parameters                       *
!*                                                                     *
!***********************************************************************
! 
         IF_KGT1: IF(k > 1) THEN
!
! ......... Augmented hydrate saturation
!
            S_ice = 1.0d0 - S_aqu - S_gas
! 
            TX = 1.0d-2   ! Initial temperature estimate  
!
! ......... Determine iteratively partial P, T, T-dev and CH4 solubility   
!
            IGOOD = PTXS_Paramtrs(-1,PX,X_iA,                       &       ! In only
     &                   TX,Xmol_iA,Xmol_mA_old,Xmol_iA_old,T_dev,  &       ! In and out 
     &                   P_CH4,Psat,HK_GinH2O,X_mA,Xmol_mA)                ! Out only
! 
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
!
          END IF IF_KGT1 
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... Compute thermophysical properties 
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
  500    T_k = TX + 273.15d0   ! Initial temperature estimate  
!
!***********************************************************************
!*                                                                     *
!*             P R O P E R T I E S  +  P A R A M E T E R S             *
!*                                                                     *
!***********************************************************************
!
! ...... Internal energy and density of liquid H2O 
!
         CALL UHRho_LiqH2O(TX,PX,DW,UW,HW,IGOOD)                         
!
! ----------
! ...... Determine the ice density and enthalpy
! ----------
!                                                                      
         D_ice = Ice_Density(TX,PX)
         H_ice = Ice_Enthalpy(PX,TX)
! 
! -------------
! ...... Mole fractions in the gas phase      
! -------------
! 
         Y_inG(1) = P_CH4/PX           ! CH4
         Y_inG(2) = 1.0d0 - Y_inG(1)   ! H2O
!
! ...... Compute mass fractions in the gas phase
!
         X_mG =   MW_CH4*Y_inG(1)   &
     &         /( MW_CH4*Y_inG(1) + MW_H2O*Y_inG(2) )                                                  
!
         X_wG = 1.0d0 - X_mG                                                
!
! ...... Determine the gas density and compressibility
!
         CALL Zcomp(PX,T_k,0.0d0,Y_inG,ZLiq,ZGas,D_gas,rho_L,ixx)
!                                                                      
! ...... Multicomponent gas viscosity                                                                       
!                                                                      
         V_gas = Gas_Viscosity(PX,T_k,ZGas)                          
!                                                                      
! ...... Obtain the specific enthalpy of the gas phase
!                                                                      
         CALL HU_IdealGas(0,2.7315d2,T_k,Hi_Gas,Ui_Gas)    ! Ideal enthalpy
         CALL HUS_Departure(T_k,ZGas,Hd_Gas,Ud_Gas,Sd_Gas) ! Departure enthalpy
!                                                                      
         H_gas = ( Hi_Gas - Hd_Gas )        &           ! Convert to J/Kg/K from J/kgmol/K
     &          /( Y_inG(1)*MW_CH4 + Y_inG(2)*MW_H2O )  
!                                                                      
         U_gas = ( Ui_Gas - Ud_Gas )        &           ! Convert to J/Kg/K from J/kgmol/K
     &          /( Y_inG(1)*MW_CH4 + Y_inG(2)*MW_H2O )  
!
! ...... Mass fraction of H2O in the aqueous phase
!
         X_wA = 1.0d0 - X_mA-X_iA                     
!                                                                      
! ...... Aqueous phase density (assumed equal to water)                                                                      
!                                                                      
         D_aqu = X_wA*DW + X_iA*D_Inhib + X_mA*D_gas
!                                                                      
! ...... Liquid H2O viscosity                                                                      
!                                                                      
         V_aqu = ViscLV_H2Ox(TX,D_aqu)            
!                                                                      
! ...... Obtain the heat of CH4 solution in H2O
!                                                                      
         H_Sol = HEAT_GasSolution(1,TX,Xmol_iA,HK_GinH2O,dummy)                                     
!                                                                      
! -------------
! ..... Compute relative permeability and capillary pressure
! -------------
!                                                                      
         CALL RelPerm_Hyd(S_aqu,S_gas,k_rel_aqu,k_rel_gas,dummy,nmat)                          
         CALL CapPres_Hyd(TX,S_aqu,S_gas,D_aqu,P_cap,nmat)
! 
! ----------
! ...... Compute the rate and heat of kinetic hydrate dissociation/formation 
! ----------
! 
         IF(HDis_Switch .EQV. .TRUE.) THEN
! 
            CALL HRho_Hydrate(TX,PX,D_Hydr,H_Hydr)
            H_Rate = -Cell_V(n,k-1)%p_Satr(3)*D_Hydr*elem(n)%phi/FORD
!
            H_Heat = HeatDiss_HydrCH4(273.16d0,&
     &                                F_EqOption, &  ! H-equation
     &                                IGOOD)
!
         ELSE
            H_Rate = 0.0d0
            H_Heat = 0.0d0
         END IF
! 
! ----------
! ...... Assign value to state index  
! ----------
! 
         IF(k == 1) elem(n)%StateIndex = 8_1
! 
! ----------
! ...... Assign values of secondary parameters - Vapor  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(1)   = S_gas
         Cell_V(n,k-1)%p_KRel(1)   = k_rel_gas
         Cell_V(n,k-1)%p_Visc(1)   = V_gas 
         Cell_V(n,k-1)%p_Dens(1)   = D_gas
         Cell_V(n,k-1)%p_Enth(1)   = H_Gas      
         Cell_V(n,k-1)%p_PCap(1)   = 0.0d0  
         Cell_V(n,k-1)%p_MasF(1,1) = X_wG
         Cell_V(n,k-1)%p_MasF(2,1) = X_mG
         Cell_V(n,k-1)%p_MasF(3,1) = 0.0d0
         Cell_V(n,k-1)%p_MasF(4,1) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Aqueous  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(2)   = S_aqu
         Cell_V(n,k-1)%p_KRel(2)   = k_rel_aqu
         Cell_V(n,k-1)%p_Visc(2)   = V_aqu
         Cell_V(n,k-1)%p_Dens(2)   = D_aqu
         Cell_V(n,k-1)%p_Enth(2)   =  X_wA*HW &
     &                              + X_mA*(H_Gas + H_Sol) &
     &                              + X_iA*H_InhSol
         Cell_V(n,k-1)%p_PCap(2)   = P_cap
         Cell_V(n,k-1)%p_MasF(1,2) = X_wA
         Cell_V(n,k-1)%p_MasF(2,2) = X_mA
         Cell_V(n,k-1)%p_MasF(3,2) = X_iA
         Cell_V(n,k-1)%p_MasF(4,2) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Hydrate phase  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(3)   = 0.0d0
         Cell_V(n,k-1)%p_Dens(3)   = 0.0d0
         Cell_V(n,k-1)%p_Enth(3)   = 0.0d0
         Cell_V(n,k-1)%p_MasF(1,3) = 0.0d0
         Cell_V(n,k-1)%p_MasF(2,3) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,3) = 0.0d0
         Cell_V(n,k-1)%p_MasF(4,3) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Ice phase 
! ----------
! 
         Cell_V(n,k-1)%p_Satr(4)   = S_ice
         Cell_V(n,k-1)%p_Dens(4)   = D_ice
         Cell_V(n,k-1)%p_Enth(4)   = H_ice
         Cell_V(n,k-1)%p_MasF(1,4) = 1.0d0
         Cell_V(n,k-1)%p_MasF(2,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(4,4) = 0.0d0
! 
! ----------
! ...... Store the temperature 
! ----------
! 
         Cell_V(n,k-1)%TemC = TX
! 
! ----------
! ...... Store other important variables  
! ----------
! 
         IF(K == 1) THEN
! 
! ......... Store equilibrium hydration pressure at TX 
! 
            Cell_V(n,0)%p_AddD(1) = P_quad_hyd
! 
! ......... Store the hydrate temperature depression  
! 
            Cell_V(n,1)%p_AddD(1) = T_dev
! 
         END IF
! 
! ----------
! ...... Store rate of kinetic hydrate dissociation/formation 
! ----------
! 
         Cell_V(n,k-1)%p_AddD(2) = H_Rate
! 
! ----------
! ...... Store heat of dissociation/formation 
! ----------
! 
         Cell_V(n,k-1)%p_AddD(3) = H_Heat
! 
! 
! 
      END DO DO_NumEqs
! 
! 
! 
      RETURN     ! Task completed - Exit the routine
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                         PHASE TRANSITIONS                           >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
 1000 CONTINUE
! 
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from 3-phase (Aqueous + Gas + Ice)
! .............. to 2-phase (Aqueous + Hydrate)  
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! 
      IF_PhTran: IF(PhTran == 'AIG->AqG') THEN
!                                                                       
         IF(MOP(5) >= 3) WRITE(6,6002) elem(n)%name,S_ice
         IF(KC == 0) THEN
            WRITE(6,6101) 
            STOP                                             
         END IF
! 
! ...... Reset 1st variable as pressure 
! 
         XX(1)      = PX*(1.0d0-ZERO)  
         DX(NLOC+1) = XX(1) - X(NLOC+1)
! 
! ...... Reset 2nd variable as aqueous saturation 
! 
         XX(2)      = S_aqu + ZERO   
         DX(NLOC+2) = XX(2) - X(NLOC+2)
! 
! ...... Reset 4th or 5th variable as temperature 
! 
         IF(NK == 3) THEN
            XX(4)      = TX + ZERO   
            DX(NLOC+4) = XX(4) - X(NLOC+4)
         ELSE
            XX(5)      = TX + ZERO   
            DX(NLOC+5) = XX(5) - X(NLOC+5)
         END IF
! 
! ...... Call the two-phase (Ice+Gas) subroutine 
! 
         CALL PHS2_AqG_Kin(N,NLOC,NMAT,.FALSE.)
! 
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
!  .............. Transition from Aqueous + Hydrate + Ice (3-phase)       
!  .............. to Quadruple point (4-phases: Aqueous+Hydrate+Gas+Ice)      
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! 
      ELSE IF(PhTran == 'AIG->QuP') THEN
!                                                                       
         IF(MOP(5) >= 3) WRITE(6,6003) elem(n)%name,PX
         IF(KC == 0) THEN
            WRITE(6,6101) 
            STOP                                             
         END IF
! 
! ...... Reset 1st variable as pressure 
! 
         XX(1)      = P_quad_hyd*(1.0d0 - ZERO)
         DX(NLOC+1) = XX(1) - X(NLOC+1)
! 
! ...... Reset 4th or 5th variable as ice saturation
! 
         IF(NK == 3) THEN
            XX(4)      = S_ice
            DX(NLOC+4) = XX(4) - X(NLOC+4)
         ELSE
            XX(5)      = S_ice  
            DX(NLOC+5) = XX(5) - X(NLOC+5)
         END IF
! 
! ...... Activate the logical switch trigger       
! 
         HApr_Switch = .TRUE.
! 
! ...... Call the 4-phase (Aqueous+Hydrate+Gas+Ice) routine 
! 
         CALL PHS4_QuP_Kin(N,NLOC,NMAT,HApr_Switch)
! 
      ELSE 
! 
         WRITE(6,6102)
         STOP
! 
      END IF IF_PhTran
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'PHS3_AIG_Kin      1.0   20 January   2005',6X, &
     &         'Routine for assigning the secondary parameters ',&
     &         'for the 3-phase ',&
     &       /,T48,'(Aqueous + Gas + Ice) system - ',&
     &         'Kinetic hydrate reaction')
!
 6001 FORMAT(' !!!!!! "PHS3_AIG_Kin": Aqueous phase disappears ',&
     &       'at element "',A8,&
     &       '" ==> S_aqu = ',1pE15.8,'  TX = ',1pE15.8)
 6002 FORMAT(' !!!!!! "PHS3_AIG_Kin": Ice phase disappears ',&
     &       'at element "',A8, &
     &       '" ==> S_ice = ',1pE15.8)
 6003 FORMAT(' !!!!!! "PHS3_AIG_Kin": Hydrate evolves at element "',A8,&
     &       '" ==> P = ',1pE15.8)
! 
 6101 FORMAT(' !!!!!!!!!!   Routine PHS3_AIG_Kin: ERRONEOUS ',&
     &       'DATA INITIALIZATION   ==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
 6102 FORMAT(' !!!!!!!!!!   Routine PHS3_AIG_Kin: ERRONEOUS ',&
     &       'PHASE CHANGE   ==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of PHS3_AIG_Kin
! 
! 
      RETURN
!
      END SUBROUTINE PHS3_AIG_Kin
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE PHS4_QuP_Kin(N,NLOC,NMAT,HApr_Switch)
! 
! ... Modules to be used 
! 
      USE Universal_Param
      USE Basic_Param
      USE GenControl_Param
      USE Diffusion_Param
!
      USE H2O_Param
      USE Hydrate_Param
!
      USE Variable_Arrays
      USE Element_Arrays
      USE PFMedProp_Arrays
!
      USE H2O_Properties
      USE Hydrate_Properties
      USE RealGas_Properties
	  USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*        ROUTINE FOR COMPUTING AND ASSIGNING ALL THE SECONDARY        *
!*        VARIABLES AT THE QUADRUPLE POINT (H2O+Hydrate+Gas+Ice)       *
!*                   FOR KINETIC HYDRATION REACTION                    *
!*                                                                     *
!*                  Version 1.0 - January 20, 2005                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!                                                                       
      IMPLICIT NONE
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(2) :: Y_inG
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: PX,TX,S_gas,S_aqu
      REAL(KIND = 8) :: Psat,DW,UW,HW,D_Hydr,H_Hydr,V_Hydr,R_Hydr
! 
      REAL(KIND = 8) :: ZLiq,ZGas,D_gas,rho_L,H_Sol,dummy
      REAL(KIND = 8) :: Hi_Gas,Ui_Gas,Hd_Gas,Ud_Gas,Sd_Gas,H_Gas,U_Gas
! 
      REAL(KIND = 8) :: V_aqu,V_gas,D_aqu,P_CH4,T_k
      REAL(KIND = 8) :: S_ice,D_ice,H_ice
! 
      REAL(KIND = 8) :: P_cap,k_rel_gas,k_rel_aqu
! 
      REAL(KIND = 8) :: X_mG,X_wG,Xmol_mA,X_mA,X_wA
      REAL(KIND = 8) :: S_Hydr,Peq_Hydr,HK_GinH2O,H_Rate,H_Heat
      REAL(KIND = 8) :: T_dev,X_iA,Xmol_iA,Xmol_mA_old,Xmol_iA_old
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN)  :: N,NLOC,NMAT
! 
      INTEGER :: K,ixx
! 
      INTEGER :: HPeq_Paramtrs,PCH4_Paramtrs   ! Function names
! 
! -------
! ... Character variables     
! -------
! 
      CHARACTER(LEN = 8) :: PhTran 
! 
! -------
! ... Logical variables     
! -------
! 
      LOGICAL :: HDis_Switch,HApr_Switch 
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of PHS4_QuP_Kin
!
!
      IF(First_call) THEN
         IF(MPI_RANK==0) WRITE(11,6000)
         First_call = .FALSE.
      END IF
! 
! ... Some initializations 
! 
      PhTran   = '        '
      HDis_Switch = .FALSE.
! 
! -------
! ... Initialization of T-depression variables        
! -------
! 
      T_dev    = 0.0d0  
      Xmol_mA  = 0.0d0
      Xmol_iA  = 0.0d0
! 
      Xmol_mA_old  = 0.0d0
      Xmol_iA_old  = 0.0d0
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>      Main loop determining conditions and assigning properties      >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_NumEqs: DO K=1,NEQ1
!
! ----------
! ...... Initial assignments of primary variable values        
! ...... Primary variables: Px, S_aqu, S_gas, S_ice        
! ----------
! 
         PX    = XX(1)                                                      
         S_aqu = XX(2)                                     
         S_gas = XX(3)                                
! 
         IF(NK == 3) THEN
            X_iA  = 0.0d0
            S_ice = XX(4)
         ELSE IF(NK == 4) THEN
            X_iA = XX(4)
            S_ice = XX(5)
         END IF
! 
! ...... Ensure realistic saturation values
! 
         IF((S_ice > 1.0d0 .AND. S_aqu > 1.0d0) .OR. &
     &      (S_ice < 0.0d0 .AND. S_aqu < 0.0d0) .OR.&
     &      (S_gas > 1.0d0 .AND. S_aqu > 1.0d0) .OR. &
     &      (S_gas < 0.0d0 .AND. S_aqu < 0.0d0) .OR. &
     &      (S_gas > 1.0d0 .AND. S_ice > 1.0d0) .OR. &
     &      (S_gas < 0.0d0 .AND. S_ice < 0.0d0) .OR. &
     &      (S_gas > 1.0d0 .AND. S_aqu > 1.0d0 .AND. S_ice > 1.0d0) .OR.   &                   
     &      (S_gas < 0.0d0 .AND. S_aqu < 0.0d0 .AND. S_ice < 0.0d0)         &                  
     &     ) THEN
            IGOOD = 2
            RETURN
         END IF
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the first iteration - Unincremented variables 
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         IF_1stEq: IF(k == 1) THEN
! 
! -------------
! ......... Constraint on inhibitor mass fraction in the aqueous phase        
! -------------
! 
            IF_SaltMassF: IF (X_iA < 0.0d0) THEN
               X_iA       =   0.0d0
               XX(4)      =   0.0d0
               DX(NLOC+4) = - X(NLOC+4)
            END IF IF_SaltMassF
!
! -------------
! ......... The hydrate saturation
! -------------
!
            S_Hydr = 1.0d0 - S_gas - S_aqu - S_ice                               
! 
! ......... Ensure that kinetic hydrate disappearance occurs at a small starting saturation
! 
            IF(S_Hydr < 0.0d0 .AND. Cell_V(n,0)%p_Satr(3) > 2.0d-2) THEN
               IGOOD = 2
               RETURN
            END IF
!
            TX = 1.0d-2   ! Initial temperature estimate  
!
!***********************************************************************
!*                                                                     *
!* Determine iteratively the temperature shift at the quadruple point  *
!*                                                                     *
!***********************************************************************
!
            IGOOD = HPeq_Paramtrs(-1,TX,X_iA,.TRUE.,                       &! In only
     &                            Xmol_iA,Xmol_mA_old,Xmol_iA_old,T_dev,   &! In and out
     &                            Peq_Hydr,HK_GinH2O,Xmol_mA,X_mA)         ! Out only
! 
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
!
!***********************************************************************
!*                                                                     *
!*                    Check phase transition from                      *
!*       Aqueous + Hydrate + Ice + Gas (4-phase, Quadruple point)      *
!*                  to Aqueous + Ice + Gas (3-phase)                   *
!*                                                                     *
!***********************************************************************
!
            IF_HydDis: IF(S_Hydr < 0.0d0) THEN
! 
               PhTran = 'QuP->AIG'
               GO TO 1000
!
            END IF IF_HydDis
!
!***********************************************************************
!*                                                                     *
!*                    Check phase transition from                      *
!*       Aqueous + Hydrate + Ice + Gas (4-phase, Quadruple point)      *
!*                to Aqueous + Gas + Hydrate (3-phase)                 *
!*                                                                     *
!***********************************************************************
!
            IF_IceDis: IF(S_ice < 0.0d0) THEN
!
               PhTran = 'QuP->AGH'
               GO TO 1000
! 
            ELSE
! 
               GO TO 500 
! 
            END IF IF_IceDis
! 
         END IF IF_1stEq
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For the remaining iterations - Incremented variables  
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
         CASE_Incr: SELECT CASE(K)
! 
! ...... Incrementing pressure 
! 
         CASE(2)                    
! 
            DELX(NLOC+1) = DFAC*XX(1)
            PX           = XX(1) + DELX(NLOC+1)
! 
! ...... Incrementing aqueous phase saturation
! 
         CASE(3)                     
! 
            DELX(NLOC+2) = DFAC
            S_aqu        = XX(2) + DELX(NLOC+2)
! 
! ...... Incrementing the gas saturation
! 
         CASE(4)                                
! 
            DELX(NLOC+3) = DFAC
            S_gas        = XX(3) + DELX(NLOC+3)
! 
! ...... Incrementing temperature (NK = 3) or salt mass fraction (NK = 4) 
! 
         CASE(5) 
! 
            IF(NK == 3) THEN
               X_iA         = 0.0d0
               DELX(NLOC+4) = DFAC
               S_ice        = XX(4) + DELX(NLOC+4)
            ELSE IF(NK == 4) THEN
               DELX(NLOC+4) = DFAC
               X_iA         = XX(4)+DELX(NLOC+4)
            END IF
! 
! ...... Incrementing temperature (NK = 4)
! 
         CASE(6) 
! 
            DELX(NLOC+5) = DFAC
            S_ice        = XX(5) + DELX(NLOC+5)
! 
         END SELECT CASE_Incr
!
!***********************************************************************
!*                                                                     *
!*         For augmented primary variables:                            *
!*             Determine the relevant parameters                       *
!*                                                                     *
!***********************************************************************
! 
         IF_KGT1: IF (K > 1) THEN 
! 
            S_Hydr = 1.0d0 - S_gas - S_aqu - S_ice  ! The augmented hydrate saturation
!
            TX = 1.0d-2   ! Initial temperature estimate  
!
! ......... Determine iteratively the mole fractions at Peq_Hyd         
!
            IGOOD = HPeq_Paramtrs(-1,TX,X_iA,.TRUE.,   &                    ! In only
     &                            Xmol_iA,Xmol_mA_old,Xmol_iA_old,T_dev, &  ! In and out
     &                            Peq_Hydr,HK_GinH2O,Xmol_mA,X_mA)         ! Out only
! 
            IF(IGOOD /= 0) THEN
               CALL WARN_RF(elem(n)%name,XX,NK1,KC)
               RETURN
            END IF                                       
!
          END IF IF_KGT1 
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... Compute thermophysical properties 
! ......
! >>>>>>>>>>>>>>>>>>
! 
! 
  500    T_k = TX + 273.15d0  ! The augmented temperature in K 
! 
! ----------
! ...... Determine Psat: the saturation pressure of H2O at TX       
! ----------
! 
         IGOOD = P_Sat_H2O_S(TX,Psat)
!
         IF(IGOOD /= 0) THEN
            CALL WARN_RF(elem(n)%name,XX,NK1,KC)
            RETURN
         END IF      
!
!***********************************************************************
!*                                                                     *
!*   Determine iteratively the mole fractions corresponding to P_CH4   *
!*                                                                     *
!***********************************************************************
!
         P_CH4 = PX - Psat
!
         IGOOD = PCH4_Paramtrs(-1,P_CH4,TX,X_iA,         &                   ! In only
     &                            Xmol_iA,Xmol_mA_old,Xmol_iA_old,T_dev,  &  ! In and out 
     &                            HK_GinH2O,X_mA,Xmol_mA)                   ! Out only
! 
         IF(IGOOD /= 0) THEN
            CALL WARN_RF(elem(n)%name,XX,NK1,KC)
            RETURN
         END IF                                       
!
!***********************************************************************
!*                                                                     *
!*             P R O P E R T I E S  +  P A R A M E T E R S             *
!*                                                                     *
!***********************************************************************
!
! ...... Internal energy and density of liquid H2O and Hydrate
!
         CALL UHRho_LiqH2O(TX,PX,DW,UW,HW,IGOOD)                         
!
! ...... Internal energy and density the solid hydrate
!
         CALL HRho_Hydrate(TX,PX,D_Hydr,H_Hydr)
!
! ----------
! ...... Determine the ice density and enthalpy
! ----------
!                                                                      
         D_ice = Ice_Density(TX,PX)
         H_ice = Ice_Enthalpy(PX,TX)
! 
! -------------
! ...... Mole fractions in the gas phase      
! -------------
! 
         Y_inG(1) = P_CH4/PX                 ! CH4
         Y_inG(2) = 1.0d0-Y_inG(1)           ! H2O
!
! ...... Compute mass fractions in the gas phase
!
         X_mG =   MW_CH4*Y_inG(1)  &
     &         /( MW_CH4*Y_inG(1) + MW_H2O*Y_inG(2) )                                                  
!
         X_wG = 1.0d0 - X_mG                                                
!
! ...... Determine the gas density and compressibility
!
         CALL Zcomp(PX,T_k,0.0d0,Y_inG,ZLiq,ZGas,D_gas,rho_L,ixx)
!                                                                      
! ...... Obtain the specific internal energy and enthalpy of CH4
!                                                                      
         CALL HU_IdealGas(0,2.7315d2,T_k,Hi_Gas,Ui_Gas)    ! Ideal enthalpy
         CALL HUS_Departure(T_k,ZGas,Hd_Gas,Ud_Gas,Sd_Gas) ! Departure enthalpy
!                                                                      
         H_gas = ( Hi_Gas - Hd_Gas )   &                 ! Convert to J/Kg/K from J/kgmol/K
     &          /( Y_inG(1)*MW_CH4 + Y_inG(2)*MW_H2O )  
!                                                                      
         U_gas = ( Ui_Gas - Ud_Gas )   &                 ! Convert to J/Kg/K from J/kgmol/K
     &          /( Y_inG(1)*MW_CH4 + Y_inG(2)*MW_H2O )  
!
! ...... Mass fraction of H2O in the aqueous phase
!
         X_wA = 1.0d0 - X_mA - X_iA                     
!                                                                      
! ...... Obtain the heat of CH4 solution in H2O
!                                                                      
         H_Sol = HEAT_GasSolution(1,TX,Xmol_iA,HK_GinH2O,dummy)                                     
!                                                                      
! ...... Multicomponent gas viscosity                                                                       
!                                                                      
         V_gas = Gas_Viscosity(PX,T_k,ZGas)                          
!                                                                      
! ...... Aqueous phase density (assumed equal to water)                                                                      
!                                                                      
         D_aqu = X_wA*DW+X_iA*D_Inhib+X_mA*D_gas
!                                                                      
! ...... Liquid H2O viscosity                                                                      
!                                                                      
         V_aqu = ViscLV_H2Ox(TX,D_aqu)                         
!                                                                      
! -------------
! ..... Relative permeability and capillary pressure
! -------------
!                                                                      
         CALL RelPerm_Hyd(S_aqu,S_gas,k_rel_aqu,k_rel_gas,dummy,NMAT)                          
         CALL CapPres_Hyd(TX,S_aqu,S_gas,D_aqu,P_cap,NMAT)
! 
! ----------
! ...... Compute the rate of kinetic hydrate dissociation/formation 
! ----------
! 
         IF(HApr_Switch .EQV. .TRUE.) THEN
! 
            CALL HRho_Hydrate(TX,PX,D_Hydr,H_Hydr)
            H_Rate = S_Hydr*D_Hydr*elem(n)%phi/FORD
!
         ELSE
            V_Hydr =  elem(n)%phi*S_Hydr  &        ! Compute the volume of each ...
     &               /PoMed(nmat)%NVoid           ! ... hydrate grain at time t 
! 
            R_Hydr = max(  PoMed(nmat)%RVoid &     ! Compute the radius of the hydrate ...
     &                    *( V_Hydr    &           ! ... particle at time t 
     &                      /PoMed(nmat)%VVoid&
     &                     )**(1.0d0/3.0d0),&
     &                   1.0d-10)
! 
            SArea_Hyd(n) =    Area_Factor &        ! Compute the hydrate surface area ... 
     &                       *PoMed(nmat)%SArea&   ! ... per unit volume at time t  
     &                       *R_Hydr*R_Hydr  &       
     &                     /( PoMed(nmat)%RVoid&
     &                       *PoMed(nmat)%RVoid)            
! 
            H_Rate = Rate_KineticDiss(n,PX,&
     &                                Peq_Hydr, &    ! The equilibrium pressure (Pa)
     &                                TX,   &        ! The temperature (C)
     &                                0.0d0, &       ! The salt molality
     &                                Y_inG, &       ! The mole fraction vector
     &                                SArea_Hyd(n)) ! The mole fraction vector
!
         END IF
! 
! ----------
! ...... Compute the heat of hydrate dissociation/formation 
! ----------
! 
         H_Heat = HeatDiss_HydrCH4(TX+273.15d0,&
     &                             F_EqOption, &  ! H-equation
     &                             IGOOD)
! 
! ----------
! ...... Assign value to state index  
! ----------
! 
         IF(k == 1) elem(n)%StateIndex = 11_1
! 
! ----------
! ...... Assign values of secondary parameters - Vapor  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(1)   = S_gas
         Cell_V(n,k-1)%p_KRel(1)   = k_rel_gas
         Cell_V(n,k-1)%p_Visc(1)   = V_gas 
         Cell_V(n,k-1)%p_Dens(1)   = D_gas
         Cell_V(n,k-1)%p_Enth(1)   = H_Gas      
         Cell_V(n,k-1)%p_PCap(1)   = 0.0d0  
         Cell_V(n,k-1)%p_MasF(1,1) = X_wG
         Cell_V(n,k-1)%p_MasF(2,1) = X_mG
         Cell_V(n,k-1)%p_MasF(3,1) = 0.0d0
         Cell_V(n,k-1)%p_MasF(4,1) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Aqueous  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(2)   = S_aqu
         Cell_V(n,k-1)%p_KRel(2)   = k_rel_aqu
         Cell_V(n,k-1)%p_Visc(2)   = V_aqu
         Cell_V(n,k-1)%p_Dens(2)   = D_aqu
         Cell_V(n,k-1)%p_Enth(2)   =  X_wA*HW &
     &                              + X_mA*(H_Gas + H_Sol) &
     &                              + X_iA*H_InhSol
         Cell_V(n,k-1)%p_PCap(2)   = P_cap
         Cell_V(n,k-1)%p_MasF(1,2) = X_wA
         Cell_V(n,k-1)%p_MasF(2,2) = X_mA
         Cell_V(n,k-1)%p_MasF(3,2) = 0.0d0
         Cell_V(n,k-1)%p_MasF(4,2) = X_iA
! 
! ----------
! ...... Assign values of secondary parameters - Hydrate phase  
! ----------
! 
         Cell_V(n,k-1)%p_Satr(3)   = S_Hydr
         Cell_V(n,k-1)%p_Dens(3)   = D_Hydr
         Cell_V(n,k-1)%p_Enth(3)   = H_Hydr
         Cell_V(n,k-1)%p_MasF(1,3) = 0.0d0
         Cell_V(n,k-1)%p_MasF(2,3) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,3) = 1.0d0
         Cell_V(n,k-1)%p_MasF(4,3) = 0.0d0
! 
! ----------
! ...... Assign values of secondary parameters - Ice phase 
! ----------
! 
         Cell_V(n,k-1)%p_Satr(4)   = S_ice
         Cell_V(n,k-1)%p_Dens(4)   = D_ice
         Cell_V(n,k-1)%p_Enth(4)   = H_ice
         Cell_V(n,k-1)%p_MasF(1,4) = 1.0d0
         Cell_V(n,k-1)%p_MasF(2,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(3,4) = 0.0d0
         Cell_V(n,k-1)%p_MasF(4,4) = 0.0d0
! 
! ----------
! ...... Store the temperature 
! ----------
! 
         Cell_V(n,k-1)%TemC = TX
! 
! ----------
! ...... Store other important variables  
! ----------
! 
         IF(K == 1) THEN
! 
! ......... Store equilibrium hydration pressure at TX 
! 
            Cell_V(n,0)%p_AddD(1) = Peq_Hydr
! 
! ......... Store the hydrate temperature depression  
! 
            Cell_V(n,1)%p_AddD(1) = T_dev
! 
         END IF
! 
! ----------
! ...... Store rate of kinetic hydrate dissociation/formation 
! ----------
! 
         Cell_V(n,k-1)%p_AddD(2) = H_Rate
! 
! ----------
! ...... Store heat of dissociation/formation 
! ----------
! 
         Cell_V(n,k-1)%p_AddD(3) = H_Heat
! 
! 
! 
      END DO DO_NumEqs
! 
! 
! 
      RETURN     ! Task completed - Exit the routine
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                         PHASE TRANSITIONS                           >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
 1000 CONTINUE
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Quadruple point (4-phase) 
! .............. to Aqueous + Hydrate + Gas (3-phase)      
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
      IF_PhTran: IF(PhTran == 'QuP->AGH') THEN
!                                                                       
         IF(MOP(5) >= 3) WRITE(6,6001) elem(n)%name,S_ice
         IF(KC == 0) THEN
            WRITE(6,6101) 
            STOP                                             
         END IF
! 
! ...... Adjust 1st variable as pressure
! 
         XX(1)      = P_quad_hyd*(1.0d0 + ZERO)
         DX(NLOC+1) = XX(1) - X(NLOC+1)
! 
! ...... Adjust 2nd variable as aqueous phase saturation
! 
         XX(2)      = S_aqu + ZERO  
         DX(NLOC+2) = XX(2) - X(NLOC+2)
! 
! ...... Reset 4th or 5th variable as temperature 
! 
         IF(NK == 3) THEN
            XX(4)      = TX 
            DX(NLOC+4) = XX(4) - X(NLOC+4)
         ELSE
            XX(5)      = TX 
            DX(NLOC+5) = XX(5) - X(NLOC+5)
         END IF
! 
! ...... Call the 3-phase (L-H2O + Hydrate + Gas) routine 
! 
         CALL PHS3_AGH_Kin(N,NLOC,NMAT,.FALSE.)
! 
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! .............. Transition from Quadruple point (4-phase) 
! .............. to Aqueous + Gas + Ice (3-phase)      
! 
! >>>>>>>>>>
! >>>>>>>>>>
! 
! 
      ELSE IF(PhTran == 'QuP->AIG') THEN
!                                                                       
         IF(MOP(5) >= 3) WRITE(6,6004) elem(n)%name,S_Hydr
         IF(KC == 0) THEN
            WRITE(6,6101) 
            STOP                                             
         END IF
! 
! ...... Adjust 1st variable as pressure
! 
         XX(1)      = P_quad_hyd*(1.0d0 - ZERO)
         DX(NLOC+1) = XX(1) - X(NLOC+1)
! 
! ...... Adjust 3rd variable as the "bogus" hydrate saturation
! 
         XX(3)      = 0.0d0 
         DX(NLOC+3) = XX(3) - X(NLOC+3)
! 
! ...... Reset 4th or 5th variable as the gas saturation 
! 
         IF(NK == 3) THEN
            XX(4)      = S_gas 
            DX(NLOC+4) = XX(4) - X(NLOC+4)
         ELSE
            XX(5)      = S_gas 
            DX(NLOC+5) = XX(5) - X(NLOC+5)
         END IF
! 
! ...... Activate the logical switch trigger       
! 
         HDis_Switch = .TRUE.
! 
! ...... Call the 3-phase (I-H2O+Hydrate+Gas) routine 
! 
         CALL PHS3_AIG_Kin(N,NLOC,NMAT,HDis_Switch)
! 
      ELSE 
! 
         WRITE(6,6102)
         STOP
! 
      END IF IF_PhTran
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'PHS4_QuP_Kin      1.0   20 January   2005',6X, & 
     &         'Routine for assigning the secondary parameters ',     &
     &         'at the hydrate quadruple point ', &
     &       /,T48,'(Aqueous + Gas + Hydrate + Ice) - ',     &
     &         'Kinetic hydrate reaction')
!
 6001 FORMAT(' !!!!!! "PHS4_QuP_Eql": Ice disappears at element "',A8,&
     &       '" ==> S_ice = ',1pE15.8)
 6002 FORMAT(' !!!!!! "PHS4_QuP_Eql": Aqueous phase disappears ',     &
     &       'at element "',A8,'" ==> S_aqu = ',1pE15.8)
 6003 FORMAT(' !!!!!! "PHS4_QuP_Eql": Gas phase disappears at ',     &
     &       'element "',A8,'" ==> S_gas = ',1pE15.8)
 6004 FORMAT(' !!!!!! "PHS4_QuP_Eql": Hydrate disappears at ',     &
     &       'element "',A8,'" ==> S_Hydr = ',1pE15.8)
! 
 6101 FORMAT(' !!!!!!!!!!   Routine PHS4_QuP_Kin: ERRONEOUS DATA ',     &
     &       'INITIALIZATION   ==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
 6102 FORMAT(' !!!!!!!!!!   Routine PHS4_QuP_Kin: ',&
     &       'ERRONEOUS PHASE CHANGE   ==>  STOP EXECUTION <<<<<<<<<<')                                 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of PHS4_QuP_Kin
! 
! 
      RETURN
!
      END SUBROUTINE PHS4_QuP_Kin
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************!
!
!
      SUBROUTINE PRINT_SecPar
! 
! ... Modules to be used 
! 
      USE Basic_Param
      USE GenControl_Param, ONLY: MOP
!
      USE Variable_Arrays
      USE Element_Arrays,  ONLY: ELEM
	  
	  USE MPI_PARAM
	  USE MPI_ARRAYS
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*          ROUTINE FOR PRINTING ALL THE SECONDARY PARAMETERS          *
!C*                                                                     *
!*                  Version 1.00, September 23, 2003                   *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision local arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(Num_SecPar) :: DP
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: NLOC,n,k,m,i,kk,ierr
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of PRINT_SecPar
!
!
      ICALL = ICALL+1
      IF(ICALL == 1 .and. MPI_Rank==0) WRITE(11,6000)
!
      WRITE(6,6001)     ! Write a heading                                             
! 
! 
!***********************************************************************
!*                                                                     *
!*                          MAIN ELEMENT LOOP                          *
!*                                                                     *
!***********************************************************************
! 
! 
	  DO I=0,MPI_RANK-1 
		CALL MPI_BARRIER(MPI_COMM_WORLD,IERR)
	  END DO

      DO_NoElem: DO n = 1,NELL                                                     
! 
! -------------
! ......... Print parameters at state point        
! -------------
! 
         WRITE(6,6002) elem(n)%name,&
     &                 (Cell_V(n,0)%p_Satr(i),&
     &                  Cell_V(n,0)%p_KRel(i),&
     &                  Cell_V(n,0)%p_Visc(i),&
     &                  Cell_V(n,0)%p_Dens(i),&
     &                  Cell_V(n,0)%p_Enth(i),&
     &                  Cell_V(n,0)%p_PCap(i),&
     &                  Cell_V(n,0)%p_MasF(1,i),&
     &                  Cell_V(n,0)%p_MasF(2,i),&
     &                  Cell_V(n,0)%p_MasF(3,i),i=1,3),             &
     &                  Cell_V(n,0)%TemC,           &
     &                 (Cell_V(n,0)%p_AddD(i),i=1,M_add)           
!c 
!c -------------
!c! ........ Print incremented parameters        
!c -------------
!c 
         IF_MOPx: IF(MOP(5) == 8) THEN                                       
!                                                                       
            DO_NEq_1: DO k=2,NEQ1                                                  
               WRITE(6,6003)  (Cell_V(n,k-1)%p_Satr(i),&
     &                         Cell_V(n,k-1)%p_KRel(i),&
     &                         Cell_V(n,k-1)%p_Visc(i),&
     &                         Cell_V(n,k-1)%p_Dens(i),&
     &                         Cell_V(n,k-1)%p_Enth(i),&
     &                         Cell_V(n,k-1)%p_PCap(i),&
     &                         Cell_V(n,k-1)%p_MasF(1,i),&
     &                         Cell_V(n,k-1)%p_MasF(2,i),&
     &                         Cell_V(n,k-1)%p_MasF(3,i),i=1,3),             &
     &                         Cell_V(n,k-1)%TemC,           &
     &                        (Cell_V(n,k-1)%p_AddD(i),i=1,M_add)            
            END DO DO_NEq_1                                                      
!c 
!c -------------
!c! ........ Print derivatives        
!c -------------
!c 
         ELSE                                                         
!                                                                       
            NLOC = (n-1)*NK1                                                  
!                                                                       
            DO_1: DO k=2,NEQ1                                                  
!                                                                     
               kk = 0
!                                                                     
! ............ Basic parameters                                                                     
!                                                                     
               DO_2: DO i=1,NPH                                               
!                                                                     
                  kk = kk+1
                  DP(kk) = ( Cell_V(n,k-1)%p_Satr(i)&
     &                      -Cell_V(n,0)%p_Satr(i))/DELX(NLOC+K-1)
                  kk = kk+1
                  DP(kk) = ( Cell_V(n,k-1)%p_KRel(i)&
     &                      -Cell_V(n,0)%p_KRel(i))/DELX(NLOC+K-1)
                  kk = kk+1
                  DP(kk) = ( Cell_V(n,k-1)%p_Visc(i)&
     &                      -Cell_V(n,0)%p_Visc(i))/DELX(NLOC+K-1)
                  kk = kk+1
                  DP(kk) = ( Cell_V(n,k-1)%p_Dens(i)&
     &                      -Cell_V(n,0)%p_Dens(i))/DELX(NLOC+K-1)
                  kk = kk+1
                  DP(kk) = ( Cell_V(n,k-1)%p_PCap(i)&
     &                      -Cell_V(n,0)%p_PCap(i))/DELX(NLOC+K-1)
!
                  kk = kk+1
                  DP(kk) = ( Cell_V(n,k-1)%p_MasF(1,i)&
     &                      -Cell_V(n,0)%p_MasF(1,i))/DELX(NLOC+K-1)
                  kk = kk+1
                  DP(kk) = ( Cell_V(n,k-1)%p_MasF(2,i)&
     &                      -Cell_V(n,0)%p_MasF(2,i))/DELX(NLOC+K-1)
                  kk = kk+1
                  DP(kk) = ( Cell_V(n,k-1)%p_MasF(3,i)&
     &                      -Cell_V(n,0)%p_MasF(3,i))/DELX(NLOC+K-1)
!                                                                     
! ............... Diffusion-related parameters                                                                    
!                                                                     
                  IF_BinDif: IF(M_BinDif /= 0 .AND. NK >= 2) THEN
!                                                                     
                     kk = kk+1
                     DP(kk) = ( Cell_V(n,k-1)%p_DifA(i)&
     &                         -Cell_V(n,0)%p_DifA(i))/DELX(NLOC+K-1)
!                                                                     
                     kk = kk+1
                     DP(kk) = ( Cell_V(n,k-1)%p_DifB(i)&
     &                         -Cell_V(n,0)%p_DifB(i))/DELX(NLOC+K-1)
!                                                                     
                  END IF IF_BinDif
!
               END DO DO_2                                                      
!                                                                     
! ............ Temperature                                                                    
!                                                                     
               kk = kk+1
               DP(kk) = ( Cell_V(n,k-1)%TemC&
     &                   -Cell_V(n,0)%TemC)/DELX(NLOC+K-1)
!                                                                     
! ............ Additional parameters                                                                      
!                                                                     
               DO_3: DO i=1,M_add                                               
                  kk = kk+1
                  DP(kk) = ( Cell_V(n,k-1)%p_AddD(i)&
     &                      -Cell_V(n,0)%p_AddD(i))/DELX(NLOC+K-1)
               END DO DO_3                                                      
!                                                                       
               WRITE(6,6003) (DP(m),m=1,Num_SecPar)                          
!                                                                       
            END DO DO_1                                                      
!                                                                       
         END IF IF_MOPx                                                         
!                                                                       
      END DO DO_NoElem     

	  DO I=MPI_RANK,MPI_NP-1
		CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)
	  END DO
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
 6000 FORMAT(/,'PRINT_SecPar      1.0   23 August    2003',6X,&
     &         'Routine for printing all secondary parameters ',&
     &         '(stored in the derived-type array Cell_V)')
!                                                                       
 6001 FORMAT(/,' SECONDARY PARAMETERS')                                  
 6002 FORMAT(/,' ELEMENT ',A8/(10(1X,1pE12.5)))                          
 6003 FORMAT(/,(10(1X,1pE12.5)))                                         
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of PRINT_SecPar
!
!
      RETURN
!
      END SUBROUTINE PRINT_SecPar
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE PRINT_Output                                                    
! 
! ... Modules to be used 
! 
	  USE MPI_PARAM
	  USE MPI_ARRAYS
	  USE MPI_SUBROUTINES
      USE Universal_Param
      USE Basic_Param
      USE GenControl_Param
!
      USE H2O_Param
      USE Hydrate_Param
!
      USE Element_Arrays
      USE Connection_Arrays
      USE Q_Arrays
!
      USE PFMedProp_Arrays
      USE Variable_Arrays
!
      USE H2O_Properties
      USE Hydrate_Properties
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*                 ROUTINE FOR GENERATING PRINT-OUT                    *
!*                                                                     *
!*                 Version 1.00, September 22, 2003                    *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision local arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(4) :: DXM, DXMP
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: DAY,TX,SW,SH,C_CH4inG,C_CH4inA  
      REAL(KIND = 8) :: PSAT,PSATO,PRES,P_CH4,X_mG,Xmol_mG  
      REAL(KIND = 8) :: GC,HGC,H_Rate,C_Inhib,S_InhinAqu
      REAL(KIND = 8) :: S_CH4inGas,S_CH4inAqu,S_CH4inTot,Delv_CH4 
! 
      REAL(KIND = 8) :: Old_HMass,New_HMass 
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0,IERR
! 
      INTEGER :: n,NLOC,NP,i,j,request(MPI_STATUS_SIZE)                                            
! 
! -------
! ... Character variables
! -------
! 
      CHARACTER(LEN = 1) :: HB = ' ', H0 = ' '
! 
      CHARACTER(LEN = 1) :: ConvInd
! 

! -------
! ... Counter for file writing offset
! -------
	  INTEGER(KIND=MPI_OFFSET_KIND) :: off = 0
	  
	  CHARACTER(LEN=145) :: Buff

      SAVE ICALL,HB,H0,off
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of PRINT_Output
!
!
      ICALL=ICALL+1                                                     
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)
!
      DAY = SUMTIM/8.64d4 
! 
! -------
! ... Option for printing short TOUGH-Fx outputs
! -------
! 
      IF(MOP(19) == 8 .OR. MOP(19)==9) THEN
		 !Resetting the view to be MPI_CHARACTER VIEW
		 call MPI_FILE_SET_VIEW(PD1_file,off,MPI_CHARACTER,MPI_CHARACTER,'internal',MPI_INFO_NULL,ierr)
         IF(MPI_RANK==0) THEN
			 WRITE(Buff,6105) SUMTIM,NEW_LINE(NLINE)
			 call MPI_FILE_WRITE(PD1_file,buff,23,MPI_CHARACTER,MPI_STATUS_IGNORE,ierr)
		 END IF
		 off = off+23
		 call MPI_FILE_SET_VIEW(PD1_file,off,MPI_CHARACTER,PD1_filetype,'internal',MPI_INFO_NULL,ierr)
		 off = off+145*NEL
         if(MOP(19)==9) GO TO 101
      END IF
! 
!***********************************************************************
!*                                                                     *
!*                       COMPUTE MAXIMUM CHANGES                       *
!*                                                                     *
!***********************************************************************
! 
      DXMP(1) = MAXVAL(ABS(DX(1:(NELL-1)*NK1+1:NK1)))                                                     
      DXMP(2) = MAXVAL(ABS(DX(2:(NELL-1)*NK1+2:NK1)))                                                     
      DXMP(3) = MAXVAL(ABS(DX(3:(NELL-1)*NK1+3:NK1)))     

	  !Find maxinum change between the mpi processes
	  call MPI_REDUCE(DXMP,DXM,3,MPI_REAL8,MPI_MAX,0,MPI_COMM_WORLD,ierr)
! 
!***********************************************************************
!*                                                                     *
!*                    PRINT HEADER INFORMATION - 1                     *
!*                                                                     *
!***********************************************************************
! 
!

      IF(Converge_Flag .EQV. .TRUE.) THEN
         ConvInd = 'Y'
      ELSE
         ConvInd = 'N'
      END IF
!
     if(MPI_RANK==0) then
		WRITE(6, 6002) H0,TITLE,KCYC,ITER,DAY        
		WRITE(6, 6004) HB                                
	 
		 
		WRITE(6, 6006) SUMTIM,KCYC,ITER,ITERC,ConvInd,       &
		 &               (DXM(I),I=1,3),RERM,NER,KER,DELTEX                                             
		WRITE(6, 6004) HB                                
		WRITE(6, 6008) H0                                
		WRITE(6, 6010)      
	  end if
	                              
!
!
!


!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                         ELEMENT LOOP - 1                            >
!>             Print primary variables & related information           >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!------------------------Printing one by one in each processor---------
	
! 
	101 CONTINUE
        CALL MPI_BARRIER(MPI_COMM_WORLD,IERR) !WAITING FOR MASTER
   DO_NumEle1: DO n=1,NEL                                                  
!    	
		 !Printing inactive elements only in 0th process
		 
		If_thisproc: if((procnum(N)==MPI_RANK .and. N<=NELA)&
					.OR. (N>NELA .and. MPI_RANK==0)) THEN		!Printing inactive elements only in master proc
			J = locnum(N)				
	! ...... Locations in the X array
	!    
			 NLOC = (J-1)*NK1                                                  
	!    
	! ...... Primary variables 
	!    
			 TX = Cell_V(J,0)%TemC                                            
			 Sw = Cell_V(J,0)%p_Satr(2)                                            
	!
			 SH = Cell_V(J,0)%p_Satr(3)                                            
	!    
	! ----------    
	! ...... Determine the saturation pressure of H2O
	! ----------    
	!    
			 IGOOD = P_Sat_H2O_S(TX,PSAT)                             
	!    
	! ----------    
	! ...... Determine pressure (depends on the 1st primary variable) 
	! ----------    
	!    
			 IF( (F_Kinetic .EQV. .FALSE.) .AND.  &
		 &       (elem(J)%StateIndex == 7_1  .OR.   &
		 &        elem(J)%StateIndex == 10_1 .OR. &
		 &        elem(J)%StateIndex == 11_1) )    &
		 &   THEN
	!    
				IF(Converge_Flag .EQV. .TRUE.) THEN    ! IF P is not the 1st primary variable, ...
				   PRES = Cell_V(J,0)%p_AddD(2)        ! ... use the stored derived pressure
				ELSE                                                 
				   PRES = EqPres_HydrCH4(TX+273.15d0,&
		 &                               Cell_V(J,1)%p_AddD(1),0,IGOOD) 
				END IF                         
	!    
			 ELSE                                      ! IF P is the 1st primary variable
	!    
				IF(Converge_Flag .EQV. .TRUE.) THEN
				   PRES = X(NLOC+1) 
				ELSE                                                 
				   PRES = X(NLOC+1)+DX(NLOC+1) 
				END IF                         
	!    
			 END IF
	!    
	! ----------    
	! ...... Determine the equilibrium hydration pressure
	! ----------    
	!    
			 PSATO = Cell_V(J,0)%p_AddD(1)
	!    
	! ----------    
	! ...... Determine the partial pressure of CH4 
	! ----------    
	!    
			 X_mG    = Cell_V(J,0)%p_MasF(2,1)            ! Mass fraction in the gas phase
			 Xmol_mG = (X_mG/MW_CH4)   &                   ! Mole fraction in the gas phase
		 &            /(X_mG/MW_CH4+(1.0d0-X_mG)/MW_H2O)
	!    
			 P_CH4   = PRES*Xmol_mG                       ! CH4 partial pressure
	!    
	! ----------    
	! ...... Retrieve the salt mass fraction 
	! ----------    
	!    
			 IF(NK == 3 .AND. (F_Kinetic .EQV. .FALSE.)) THEN
				C_Inhib = Cell_V(J,0)%p_MasF(3,2)
			 ELSE IF(NK == 4 .AND. (F_Kinetic .EQV. .TRUE.)) THEN
				C_Inhib = Cell_V(J,0)%p_MasF(4,2)
			 ELSE
				C_Inhib = 0.0d0
			 END IF
	!    
	! ----------    
	! ...... Print out the data 
	! ----------    
	!    
			 IF(MOP(19) < 8) THEN                         ! Standard output
				WRITE(6, 6012) ADJUSTR(elem(J)%name),J,PRES,TX,SH,SW,&
		 &                     Cell_V(J,0)%p_Satr(1),Cell_V(J,0)%p_Satr(4),&
		 &                     P_CH4,PSATO,PSAT,C_Inhib
			 ELSE                
				IF(MOP(19) == 8) THEN    ! Standard output
					WRITE(6, 6012) ADJUSTR(elem(J)%name),J,PRES,TX,SH,SW,&
			 &                     Cell_V(J,0)%p_Satr(1),Cell_V(J,0)%p_Satr(4),&
			 &                     P_CH4,PSATO,PSAT,C_Inhib
				END IF
				! Plotting files 
				
				WRITE(buff,6014) PRES,TX,SH,SW,& 
		 &                     Cell_V(J,0)%p_Satr(1),Cell_V(J,0)%p_Satr(4),&
		 &                     C_Inhib,&
		 &                     Cell_V(J,0)%p_KRel(1),&
		 &                     Cell_V(J,0)%p_KRel(2),NEW_LINE(NLINE)
			
				call MPI_FILE_WRITE(PD1_file,buff,1,PD1_linetype,MPI_STATUS_IGNORE,ierr)
			 END IF
			 call MPI_BARRIER(MPI_COMM_WORLD,IERR)
		ELSE
			 call MPI_BARRIER(MPI_COMM_WORLD,IERR)
		END IF If_thisproc

! <<<                      
! <<< End of the ELEMENT LOOP         
! <<<
!
      END DO DO_NumEle1
! 
! -------
! ... Option for printing short TOUGH-Fx outputs
! -------
! 
!---------We have to write a blank record at the tail for a gap between two time steps
     
	  IF(MOP(19) == 8 .OR. MOP(19)==9) THEN
		 !Resetting the view to be MPI_CHARACTER VIEW
		 call MPI_FILE_SET_VIEW(PD1_file,off,MPI_CHARACTER,MPI_CHARACTER,'internal',MPI_INFO_NULL,ierr)
         IF(MPI_RANK==0) THEN
			 WRITE(Buff,'(2A1)') NEW_LINE(NLINE),NEW_LINE(NLINE)
			 call MPI_FILE_WRITE(PD1_file,buff,1,MPI_CHARACTER,MPI_STATUS_IGNORE,ierr)
		 END IF
		 off = off+1
         if(MOP(19)==9) GO TO 1001
      END IF
! 
!***********************************************************************
!*                                                                     *
!*                    PRINT HEADER INFORMATION - 2                     *
!*                                                                     *
!***********************************************************************
! 
	if(MPI_RANK==0) then
      WRITE(6, 6015)                                   
!                                                                       
      WRITE(6, 6016) ' ',TITLE,KCYC,ITER,SUMTIM         
      WRITE(6, 6018) H0                                
      WRITE(6, 6020)      
	end if
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                         ELEMENT LOOP - 2                            >
!>                 Print other important parameters                    >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!

                                CALL MPI_BARRIER(MPI_COMM_WORLD,IERR)
      DO_NumEle2: DO n=1,NEL                                                   
!    
		
		If_thisproc2: if((procnum(N)==MPI_RANK .and. N<=NELA)&
					.OR. (N>NELA .and. MPI_RANK==0)) THEN		!Printing inactive elements only in master proc
						
	! ...... Locations in the X array
	!    
			 J = locnum(N)
			 NLOC = (J-1)*NK1                                                    
	!    
	! ...... Primary variables 
	!    
			 TX = Cell_V(J,0)%TemC                                            
			 PRES = X(NLOC+1)                                                    
	!    
	! ...... CH4 concentrations in the gas and the aqueous phases
	!    
			 C_CH4inG = Cell_V(J,0)%p_MasF(2,1)*Cell_V(J,0)%p_Dens(1)                                           
			 C_CH4inA = Cell_V(J,0)%p_MasF(2,2)*Cell_V(J,0)%p_Dens(2)                                           
	!    
	! ...... Print-out 
	!    
	 6003    WRITE(6,6022) ADJUSTR(elem(J)%name),J,C_CH4inG,C_CH4inA,&
		 &                 Cell_V(J,0)%p_Dens(1),&
		 &                 Cell_V(J,0)%p_Dens(2),&
		 &                 Cell_V(J,0)%p_Dens(3),&
		 &                 Cell_V(J,0)%p_Visc(1),&
		 &                 Cell_V(J,0)%p_Visc(2),&
		 &                 elem(J)%phi,&
		 &                 Cell_V(J,0)%p_PCap(2),&
		 &                 Cell_V(J,0)%p_KRel(1),&
		 &                 Cell_V(J,0)%p_KRel(2)
	!
		 CALL MPI_BARRIER(MPI_COMM_WORLD,IERR)
	ELSE
		 CALL MPI_BARRIER(MPI_COMM_WORLD,IERR)
	END IF  If_thisproc2
! <<<                      
! <<< End of the ELEMENT LOOP         
! <<<
!
      END DO DO_NumEle2
! 
!***********************************************************************
!*                                                                     *
!*                    PRINT HEADER INFORMATION - 3                     *
!*                                                                     *
!***********************************************************************
! 
	 if(MPI_RANK==0) THEN
      WRITE(6, 6015)                            
      IF (MOD(KDATA,10) < 2 .OR. NCON == 0) GO TO 1001
!
      WRITE(6, 6016) ' ',TITLE,KCYC,ITER,SUMTIM        
      WRITE(6, 6024) H0                               
      WRITE(6, 6026) 
	end if
      IF (MOD(KDATA,10) < 2 .OR. NCON == 0) GO TO 1001
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                      MAIN CONNECTION LOOP                           >
!>                        Print flow terms                             >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
	DO I=0,MPI_RANK-1
		CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)
	END DO

      DO_NumCon1: DO n=1,NCONT
!
         IF(conx(n)%n1 == 0 .OR. conx(n)%n2 == 0) CYCLE                    
!
		!Since the connections between to neighbors are shared by multiple process
		!Print the connection information in the process whose rank is least
		
		if(n>NCONI) then
			if(conx(n)%n1<=NELL) then	!n1 belongs to this process
				if(update_p(conx(n)%n2-NELL)<MPI_RANK) CYCLE
			else						!n2 belongs to this process
				if(update_p(conx(n)%n1-NELL)<MPI_RANK) CYCLE
			end if
		end if
		
         WRITE(6,6028)  ADJUSTR(conx(n)%nam1),ADJUSTR(conx(n)%nam2),&
     &                  N,conx(n)%FluxH,&
     &                  conx(n)%FluxF(1),conx(n)%FluxF(2),&
     &                  conV(n)%FluxVF(1),conV(n)%FluxVF(2),&
     &                  conx(n)%VelPh(1),conx(n)%VelPh(2)
!
! <<<                          
! <<< End of the CONNECTIONS LOOP         
! <<<    
!
      END DO DO_NumCon1
! 
	DO I=MPI_RANK,MPI_NP-2
		CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)
	END DO
	
!***********************************************************************
!*                                                                     *
!*                    More print-outs for KDATA > 2                    *
!*                                                                     *
!***********************************************************************
! 
	if(MPI_RANK==0) THEN
      WRITE(6,6015)                                  
!
      IF(MOD(KDATA,10) < 3) GO TO 1001                                
!
      WRITE(6, 6016) ' ',TITLE,KCYC,ITER,SUMTIM        
!
      IF(NK == 4) THEN
         WRITE(6,6032) H0                               
      ELSE IF(NK == 3) THEN
         WRITE(6,6030) H0                               
      ELSE 
         WRITE(6, 6034) H0                               
      END IF
	 END IF
    
	IF(MOD(KDATA,10) < 3) GO TO 1001  
!
!
	 
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                         ELEMENT LOOP - 3                            >
!>                Primary variables and their changes                  >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!

                                CALL MPI_BARRIER(MPI_COMM_WORLD,IERR)
      DO_NumEle3: DO N=1,NEL                                                   
!    
		If_thisproc3: if((procnum(N)==MPI_RANK .and. N<=NELA)&
					.OR. (N>NELA .and. MPI_RANK==0)) THEN		!Printing inactive elements only in master proc
			
	! ...... Locations in the X array
	!    
			 J = locnum(N)
			 NLOC = (J-1)*NK1                                                  
	! 
			 IF(NK == 4) THEN
			!	WRITE(6,6041) ADJUSTR(elem(J)%name),J,&
		 !&                   ( X(NLOC+I),I=1,NK1),&
		 !&                   (DX(NLOC+I),I=1,NK1)  
			 ELSE
			!	WRITE(6,6040) ADJUSTR(elem(J)%name),J,&
		 !&                   ( X(NLOC+I),I=1,NK1),&
		 !&                   (DX(NLOC+I),I=1,NK1)  
			 END IF
	!
			CALL MPI_BARRIER(MPI_COMM_WORLD,IERR)
		ELSE
			call MPI_BARRIER(MPI_COMM_WORLD,IERR)
		END IF If_thisproc3
!

	! <<<                      
	! <<< End of the ELEMENT LOOP         
	! <<<
      END DO DO_NumEle3
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                         ELEMENT LOOP - 4                            >
!>             Enthalpies, reaction rates, T-depression                >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
	
      if(MPI_RANK==0) WRITE(6, 6031) H0                               
! 
! ----------
! ... More headings  
! ----------
! 

                                CALL MPI_BARRIER(MPI_COMM_WORLD,IERR)
      DO_NumEle4: DO N=1,NEL                                                 
!    
		If_thisproc4: if((procnum(N)==MPI_RANK .and. N<=NELA)&
					.OR. (N>NELA .and. MPI_RANK==0)) THEN		!Printing inactive elements only in master proc
					
			 J = locnum(N)
			 if(J>NELAL .and. MPI_RANK/=0) 	CYCLE
			 
	! ...... Locations in the X array
	!    
			 NLOC = (J-1)*NK1                                                  
	! 
	! -------------
	! ...... Compute hydration reaction rates  
	! -------------
	! 
			 IF(F_Kinetic .EQV. .TRUE.) THEN                ! Kinetic conditions
				H_Rate = Cell_V(J,0)%p_AddD(2)*elem(J)%vol
			 ELSE
	! 
				Old_HMass =  Cell_V(J,3)%p_AddD(1)          ! From "NI_Converged"
				New_HMass =  Cell_V(J,0)%p_Satr(3)&
		 &                  *Cell_V(J,0)%p_Dens(3)
	! 
				H_Rate = elem(J)%vol&
		 &              *elem(J)%phi&
		 &              *(New_HMass-Old_HMass)/DELTEX  
	! 
			 END IF
	! 
	! -------------
	! ...... Print more element data  
	! -------------
	! 
			 WRITE(6,6040) ADJUSTR(elem(J)%name),J,&
		 &                 Cell_V(J,0)%p_Enth(1),&
		 &                 Cell_V(J,0)%p_Enth(2),&
		 &                 Cell_V(J,0)%p_Enth(3),&
		 &                 0.0d0,&
		 &                 H_Rate,&
		 &                 Cell_V(J,0)%p_AddD(3)
	!
	! <<<                      
	! <<< End of the ELEMENT LOOP         
	! <<<
!		
			CALL MPI_BARRIER(MPI_COMM_WORLD,IERR)
		ELSE
			CALL MPI_BARRIER(MPI_COMM_WORLD,IERR)
		END IF If_thisproc4
      END DO DO_NumEle4
!    
     if(MPI_RANK==0) WRITE(6, 6015)                              
!    
!    
!    
	
 1001 IF(NOGN == 0) RETURN  
!                          
! ... Initializations - only if active wells exist        
!    
      GC       = 0.0d0                                                             
      HGC      = 0.0d0                                                            
      Delv_CH4 = 0.0d0
! 
!***********************************************************************
!*                                                                     *
!*                    PRINT HEADER INFORMATION - 4                     *
!*                                                                     *
!***********************************************************************
! 
	if(MPI_RANK==0) THEN
      WRITE(6, 6016) ' ',TITLE,KCYC,ITER,SUMTIM    
      WRITE(6, 6042) H0                           
      WRITE(6, 6044)   
	end if
!C                                                                       
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                         SINKS & SOURCES                             >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
                                CALL MPI_BARRIER(MPI_COMM_WORLD,IERR)
      DO_NumSoSi: DO n=1,NOGN                                                 
!    
! ...... Locations of the S/S element
!    
		 If_thisproc5: if(procnum(SS(n)%el_num) == MPI_RANK) THEN
		 
			 j = locnum(SS(n)%el_num)                                                       
	!
			 IF (j == 0) CYCLE DO_NumSoSi                                        
	! 
	! ----------
	! ...... Compute flowing CH4 mass fraction in gas and aqueous phases          
	! ----------
	! 
			 IF(SS(n)%rate_m < 0.0d0) THEN
				S_CH4inGas = SS(n)%frac_flo(1)*Cell_V(j,0)%p_MasF(2,1)
				S_CH4inAqu = SS(n)%frac_flo(2)*Cell_V(j,0)%p_MasF(2,2)
				S_CH4inTot = S_CH4inGas+S_CH4inAqu
	!
	! ......... Determining the inhibitor mass fraction in the production stream
	!
				IF(NK == 3 .AND. (F_Kinetic .EQV. .FALSE.)) THEN
				   S_InhinAqu = SS(n)%frac_flo(2)*Cell_V(j,0)%p_MasF(3,2)
				ELSE IF(NK == 4 .AND. (F_Kinetic .EQV. .TRUE.)) THEN
				   S_InhinAqu = SS(n)%frac_flo(2)*Cell_V(j,0)%p_MasF(4,2)
				ELSE
				   S_InhinAqu = 0.0d0
				END IF
	!
			 ELSE
				S_CH4inGas = 0.0d0
				S_CH4inAqu = 0.0d0
				S_CH4inTot = 0.0d0
				S_InhinAqu = 0.0d0
			 END IF
	! 
	! ----------
	! ...... Print-out options          
	! ----------
	! 
	!    
	! ...... For injection
	!    
			 IF_GPO: IF(SS(n)%rate_m > 0.0d0) THEN
				WRITE(6,6046) ADJUSTR(SS(n)%name_el),SS(n)%name,N,&
		 &                    SS(n)%rate_m,SS(n)%enth,Inh_WMasF(n) 
	!    
	! ...... For production
	!    
			 ELSE
	!
				WRITE(6,6046)  ADJUSTR(SS(n)%name_el),SS(n)%name,n,&
		 &                     SS(n)%rate_m,SS(n)%enth,              &                 
		 &                    (SS(n)%frac_flo(np),np=1,2),&
		 &                     S_CH4inTot*SS(n)%rate_m,&
		 &                     S_CH4inTot*SS(n)%rate_m*1.47724d0,&
		 &                     S_CH4inGas*SS(n)%rate_m*1.47724d0,&
		 &                     S_CH4inAqu*SS(n)%rate_m*1.47724d0,&
		 &                     S_InhinAqu*SS(n)%rate_m      
	!
			 END IF IF_GPO 
			 CALL MPI_BARRIER(MPI_COMM_WORLD,IERR)
		ELSE
			CALL MPI_BARRIER(MPI_COMM_WORLD,IERR)
		END IF If_thisproc5
! 
! ----------
! ...... Compute total flowrate  and flowing enthalpy for wells on          
! ...... deliverablity with completions in different layers or elements          
! ......          
! ...... All open intervals of a well MUST be known by the same source           
! ...... name, and MUST be given in uninterrupted sequence           
! ----------
! 
         IF((NOGN == 1) .OR. &
     &   (n == 1 .AND. SS(n+1)%name /= SS(n)%name)) CYCLE
!
         IF_Deliv: IF((NOGN == 1) .OR.&
     &                (SS(n-1)%name == SS(n)%name .OR. &
     &                 SS(n)%name   == SS(n+1)%name)) &
     &   THEN
!
			
            GC       = GC+SS(n)%rate_m                                                   
            HGC      = HGC+SS(n)%rate_m*SS(n)%enth                                            
            Delv_CH4 = Delv_CH4+SS(n)%rate_m*S_CH4inTot
			
			!Communicate GC,HGC,Delv_CH4 from the process to which SS belongs
			!To all the processes.
			TEMP_REAL(1:3) = (/ GC, HGC, Delv_CH4 /)
			
			CALL MPI_BROADCAST(REAL_ARRAY = TEMP_REAL, &
					REAL_SIZE = 3, root=procnum(SS(n)%el_num))
			GC = TEMP_REAL(1)
			HGC = TEMP_REAL(2)
			Delv_CH4 = TEMP_REAL(3)
! 
! -------------
! ......... Determine if chain terminates          
! -------------
! 
            IF(n == NOGN) THEN
!    
! ............ For the chain end
!    
               IF(GC /= 0.0d0) THEN                                           
                  HGC      = HGC/GC
                  Delv_CH4 = Delv_CH4/GC
               END IF
!    
               if(MPI_RANK==0) WRITE(6, 6050) SS(n)%name,GC,HGC,Delv_CH4                       
!    
               GC       = 0.0d0                                                          
               HGC      = 0.0d0                                                         
               Delv_CH4 = 0.0d0
!    
            END IF                                        
!    
            IF(SS(n+1)%name == SS(n)%name) CYCLE                         
!    
         ELSE                                        
!    
            GC       = 0.0d0                                                          
            HGC      = 0.0d0                                                         
            Delv_CH4 = 0.0d0
!    
         END IF IF_Deliv                                      
!
! <<<                      
! <<< End of the SINKS & SOURCES LOOP         
! <<<
!
      END DO DO_NumSoSi
!
!
      if(MPI_RANK==0) WRITE(6, 6015)                                             
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'PRINT_Output      1.0   22 September 2004',6X,&
     &         'Print results for elements, connections, ',&
     &         'and sinks & sources')     
!                                       
 6002 FORMAT(A1,/,' ',A80,//,&
     &       10X,'OUTPUT DATA AFTER (',I4,',',I3,') - ',  &
     &           '- TIME STEPS',51(' '),'THE TIME IS ',1pE12.5,' DAYS')             
 6004 FORMAT(/151('@'),/,A1)                                          
 6006 FORMAT(T3,  'TOTAL TIME (s)',T21,'KCYC',T30,'ITER',T38,'ITERC',&
     &       T46, 'Convergence',T63,'DX1M',T77,'DX2M',T91,'DX3M',&
     &       T103,'Max Residual',T121,'NER',T130,'KER',&
     &       T141,'DELTEX',/, &
     &       1X,1pE12.5,T18,i7,T27,i7,T36,i7,T51,a1,T58,1pE13.6,&
     &       T72,1pE13.6,T86,1pE13.6,T102,1pE13.6,T120,i4,T127,i6,&
     &       T137,1pE13.6)           
 6008 FORMAT(A1,/,T4,'ELEM',T11,'INDEX',T20,'Pressure',T33,&
     &       'Temperature',T46,'S_Hydrate',T58,'S_aqueous',T72,           &
     &       'S_gas',T84,'S_Ice',T98,'P_CH4',T111,'P_EqHydr',T126,          &
     &       'P_SatWat',T139,'C_Inhibitor')                                                   
 6010 FORMAT(T20,'  (Pa)  ',T34,'(Deg. C)',T98,2('(Pa)',11x),&
     &           '(Pa)',5x,'(Mass Fraction)',/,151('_'),/)           
!
 6012 FORMAT(A8,1X,I6,1X,1pe14.7,1X,1pE12.5,4(1X,1pE11.4),&
     &       3(1X,1pE14.7),1X,1pE12.5)       
 6014 FORMAT(9(1pE15.8,1x),A1)       
 6015 FORMAT(/,151('@'))                                             
 6016 FORMAT(A1,/,10X,A80,//,&
     &       80X,'KCYC =',I5,'  -  ITER =',I5,'  -  TIME =',1pE12.5)                                            
 6018 FORMAT(A1,/,145('_'),//,&
     &       T4,'ELEM',T11,'INDEX',T18,'C-CH4inGas',1X,&
     &       'C-CH4inAqu',2X,             &
     &       'Dens_Gas   Dens_Aqu   Dens_Hydr  Visc_Gas   Visc_Aqu   ',&
     &       '    Phi          PC_w      Krel_Gas    Krel_Aqu')     
 6020 FORMAT(18X,'(Kg/m^3) ',(4(2X,'(Kg/m^3) ')),2X,'(Kg/m*s)',3X,  & 
     &       '(Kg/m*s)',7X,'    ',9X,'(Pa)',/,&
     &       145('_'),/)                                                       
!
 6022 FORMAT(A8,1x,I6,1x,7(1pE11.4),1x,2(1pe12.5,1x),2(1pe11.4,1x))                                          
 6024 FORMAT(A1,/,112('_'),//,&
     &       T4,'ELEM1    ELEM2   INDEX  Heat Flux    ',&
     &          'Gas Flux   Aqu. Flux  CH4inGs-Flx ',&
     &          'CH4inAq-Flx   Gas Veloc   Aqu Veloc')                              
 6026 FORMAT(T31,'(W)        (Kg/s)      (Kg/s)      ',&
     &           '(Kg/s)      (Kg/s)      (m/s)       ',&
     &           '(m/s)',/,112('_'),/)             
 6028 FORMAT(A8,1x,A8,1x,I7,(7(1X,1pE11.4)))                                   
 6030 FORMAT(A1,/,151('_'),//,&
     &       T4,'ELEM   INDEX       X1            X2          ',&
     &          '  X3            X4           DX1           ',&
     &          'DX2           DX3           DX4',/,151('_'),/)                                     
!
 6031 FORMAT(A1,/,151('_'),//,&
     &       T4,'ELEM   INDEX    Enthl-Gas    Enthl-Aqu     ',&
     &          'Enthl-Hydr',4x,'Enthl-Ice',4x,'Reactn_Rate',3x,&
     &          'Reactn_Heat',/                                    &
     &       T13,(4(8X,'(J/Kg)')),7x,'(Kg/s)',8x,'(J/kg)',/,151('_'),/)                                      
!
 6032 FORMAT(A1,/,151('_'),//,&
     &       T4,'ELEM   INDEX       X1           X2         ',&
     &          '  X3           X4           X5          DX1          ',&
     &          'DX2          DX3          DX4          DX5',&
     &       /,151('_'),/)                                      
!
 6034 FORMAT(A1,/,151('_'),//,&
     &       T4,'ELEM   INDEX       X1            X2         ',&
     &          '   X3           DX1           DX2           DX3',&
     &       /,151('_'),/)                                      
!
!
 6040 FORMAT(A8,1x,I6,8(1x,1pE13.6))                                          
 6041 FORMAT(A8,1x,I6,10(1x,1pE12.5))                                          
!
 6042 FORMAT(A1,/,151('_'),//,&
     &       ' ELEMENT SOURCE INDEX   Generation Rate    ',&
     &       'Enthalpy     GasFraction   AquFraction   ',&
     &       'CH4-MasRate  CH4_VRateTot  CH4_VRateGas  ',&
     &       'CH4_VRateAqu   Inhib-MRate')               
 6044 FORMAT(T26, '(Kg/s) or (W)      (J/Kg)',T87,'(kg/s)',&
     &       T100, 'St m^3/s',T114,'St m^3/s',T128,'St m^3/s',&
     &       T143,'(kg/s)',/,151('_'),/)        
 6046 FORMAT(A8,2X,A5,2X,I4,4X,1pE12.5,4X,1pE12.5,7(2X,1pE12.5))          
 6050 FORMAT(' >>>>> SOURCE  "',A5,'":     Rate =',1pE12.5,  &            
     &       ' kg/s     Flowing Enthalpy =',1pE12.5,' J/kg    ',  &   
     &       ' CH4 Mass Fraction =',1pE12.5/)
!
 6105 FORMAT('TIME = ',1pe15.8,A1)
 6106 FORMAT(//)
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of PRINT_Output
!
!
      RETURN
! 
      END SUBROUTINE PRINT_Output
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE VME_Balance                                                  
! 
! ... Modules to be used 
! 
      USE Universal_Param
      USE Basic_Param
      USE GenControl_Param
!
      USE Hydrate_Param
!
      USE Element_Arrays
      USE PFMedProp_Arrays
!
      USE Variable_Arrays
      USE SolMatrix_Arrays, ONLY: CO
	  USE MPI_PARAM
	  USE MPI_SUBROUTINES
	  USE MPI_ARRAYS
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*          ROUTINE FOR COMPUTING VOLUME AND MASS BALANCES             *
!*                                                                     *
!*                 Version 1.00, September 16, 2004                    *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision local arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(4)   :: vol_phase,mass_phase,mass_comp
      REAL(KIND = 8), DIMENSION(4,4) :: mass_CinPh
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: DAY,Sumx
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: n,i,k                                              
! 
      INTEGER :: ICALL = 0,IERR                                            
! 
! -------
! ... Saving variables
! -------
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of VME_Balance
!
!
      ICALL = ICALL + 1
      IF(ICALL == 1 .and. MPI_RANK==0) WRITE(11,6000)
! 
!***********************************************************************
!*                                                                     *
!*                        BASIC INITIALIZATIONS                        *
!*                                                                     *
!***********************************************************************
! 
      DAY = SUMTIM/8.64E4                                                 
! 
! ----------
! ... Initialize arrays - CAREFUL: Whole array operation 
! ----------
! 
      vol_phase  = 0.0d0
      mass_phase = 0.0d0
      mass_comp  = 0.0d0
      mass_CinPh = 0.0d0
	  TEMP_REAL = 0.0d0
!                          
! ... Printing out headings           
!    
      IF(MPI_RANK==0) WRITE(6,6001)                                            
!                                                                       
      IF(MPI_RANK==0) WRITE(6,6002) KCYC,ITER,SUMTIM,DAY                         
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                PHASE LOOP - ! Whole Array Operations                >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_NumPhase: DO i=1,NPH
!
! ...... Determine the product (Vol*Phi*Saturation)
!
         FORALL (n=1:NELAL) CO(n) = Cell_V(n,0)%p_Satr(i)
!
         CO(NELAL+1:2*NELAL) = elem(1:NELAL)%phi*elem(1:NELAL)%vol&
     &                                       *CO(1:NELAL)
!
! ...... Determine the product (Vol*Phi*Saturation*Dens)
!
         FORALL (n=1:NELAL) CO(n) = Cell_V(n,0)%p_Dens(i)
!
         CO(2*NELAL+1:3*NELAL) = CO(NELAL+1:2*NELAL)*CO(1:NELAL)
!
! ...... Compute phase volumes and masses
!
         vol_phase(i)  = SUM(CO(NELAL+1:2*NELAL))
         mass_phase(i) = SUM(CO(2*NELAL+1:3*NELAL))
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... COMPONENT LOOP - Summation of component masses in each phase  
! ...... 
! >>>>>>>>>>>>>>>>>>
! 
! 
         DO_NumComp: DO k=1,NK
!
! ......... Determine the product (Vol*Phi*Saturation*Dens*Mass fraction)
!
            FORALL (n=1:NELAL) CO(n) = Cell_V(n,0)%p_MasF(k,i)
            Sumx = SUM(CO(2*NELAL+1:3*NELAL)*CO(1:NELAL))
!
! ......... Compute mass of component k
!
            mass_comp(k)    = mass_comp(k)    + Sumx    ! Total mass in all phases
            mass_CinPh(k,i) = mass_CinPh(k,i) + Sumx    ! Mass in phase i
!    
! <<<<<<                          
! <<<<<< End of the COMPONENT LOOP         
! <<<<<<    
!    
         END DO DO_NumComp
!    
! <<<                          
! <<< End of the PHASE LOOP         
! <<<   
!    
      END DO DO_NumPhase
! 
!-------------
			!Sum among all the processes the masses and volumes
!-------------
	  call MPI_ALLREDUCE( (/ vol_phase(1:4),mass_phase(1:4),mass_comp(1:4),(mass_CinPh(1:4,i), i=1,4)/), &
						TEMP_REAL, 12+16,&
							MPI_REAL8, MPI_SUM, MPI_COMM_WORLD, ierr )
			
	  
	   vol_phase(1:4) = TEMP_REAL(1:4)
	   mass_phase(1:4) = TEMP_REAL(5:8)
	   mass_comp(1:4) = TEMP_REAL(9:12)
	   forall(i=1:4) mass_CinPh(1:4,i) = TEMP_REAL(13+(i-1)*4:12+i*4)
! -------
! ... Initial hydrate mass        
! -------
! 
      IF(ICALL == 1) Initial_HydMass = mass_phase(3)
! 
! 
!***********************************************************************
!*                                                                     *
!*                            PRINT-OUTS                               *
!*                                                                     *
!***********************************************************************
! 
! 
      IF(MPI_Rank==0) WRITE(6,6003) (vol_phase(i), i=1,NPH),(mass_phase(i),i=1,NPH)
! 
      mass_CinPh(:,4) = 0.0d0
! 
! -------
! ... Equilibrium + Salt
! -------
! 
      IF(NK == 3 .AND. (F_Kinetic .EQV. .FALSE.) .and. mpi_rank==0) THEN
         WRITE(6,6004) (mass_CinPh(i,1),i=1,2),0.0d0,mass_CinPh(3,1),&
     &                 (mass_CinPh(i,2),i=1,2),0.0d0,mass_CinPh(3,2), & 
     &                 (mass_CinPh(i,3),i=1,2),0.0d0,mass_CinPh(3,3), &  
     &                 (mass_CinPh(i,4),i=1,2),0.0d0,mass_CinPh(3,4), & 
     &                 (mass_comp(k),k=1,2),0.0d0,mass_comp(3)
! 
! -------
! ... Equilibrium Without Salt
! -------
! 
      ELSE IF(NK == 2 .AND. (F_Kinetic .EQV. .FALSE.) .and. mpi_rank==0) THEN
         WRITE(6,6004) (mass_CinPh(i,1),i=1,3),0.0d0,   &
     &                 (mass_CinPh(i,2),i=1,3),0.0d0,  & 
     &                 (mass_CinPh(i,3),i=1,3),0.0d0, &  
     &                 (mass_CinPh(i,4),i=1,3),0.0d0,  & 
     &                 (mass_comp(k),k=1,3),0.0d0
! 
! -------
! ... Kinetic Without Salt
! -------
! 
      ELSE IF(NK == 3 .AND. (F_Kinetic .EQV. .TRUE.) .and. mpi_rank==0) THEN
         WRITE(6,6004) (mass_CinPh(i,1),i=1,3),0.0d0,  &
     &                 (mass_CinPh(i,2),i=1,3),0.0d0, &  
     &                 (mass_CinPh(i,3),i=1,3),0.0d0,   &
     &                 (mass_CinPh(i,4),i=1,3),0.0d0, &
     &                 (mass_comp(k),k=1,3),0.0d0
! 
! -------
! ... Kinetic + Salt
! -------
! 
      ELSE IF(NK == 4 .AND. (F_Kinetic .EQV. .TRUE.) .and. mpi_rank==0) THEN
         WRITE(6,6004) (mass_CinPh(i,1),i=1,4), &  
     &                 (mass_CinPh(i,2),i=1,4),   &
     &                 (mass_CinPh(i,3),i=1,4),   &
     &                 (mass_CinPh(i,4),i=1,4),   &
     &                 (mass_comp(k),k=1,4)
      END IF
! 
      IF(mpi_rank==0) WRITE(6,6005)

!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'VME_Balance       1.0   16 September 2004',6X, &   
     &         'Perform summary balances for volume, mass, and energy')          
!
 6001 FORMAT(//,130('*'),/,'*',T130,'*',/,'*',   &
     &       T43,'V O L U M E   A N D   M A S S   B A L A N C E S',   &
     &       T130,'*',/,'*',T130,'*',/,130('*'),/)
 6002 FORMAT(' ==========  [KCYC,ITER] = [',I4,',',I3,&
     &       ']  ==========',27X,  &
     &       'The time is ',1pE12.5,' seconds, or ',1pe12.5,' days'/)            
!
 6003 FORMAT(/,36X,  &
     &       'PHASES PRESENT',/,   &
     &       ' ',84('='),/,   &
     &       '  PHASES       |         Gas            Aqueous    ',  &
     &       '     Hydrate            Ice',/, &
     &       ' ',84('='),/,15x,'|',/,  &
     &       '  VOLUME (m^3) |',4(2X,1pE15.8),/,   &
     &       '  MASS   (Kg)  |',4(2X,1pE15.8),/,   &
     &       15x,'|',/,' ',84('='))
 6004 FORMAT(//,73X,   &
     &       'COMPONENT MASS IN PLACE (Kg)',/,   &
     &       43x,' ',86('='),/,   &
     &       43x,'   COMPONENTS    |       Water             ',&
     &       'CH4            Hydrate',11x,'Salt'/,  &
     &       43x,' ',86('='),/, &
     &       46x,'PHASES',8X,'|',/,44x,16('-'),'|',/,  &
     &       46X,'Gas phase     |',4(2X,1pE15.8),/,   &
     &       46X,'Aqueous       |',4(2X,1pE15.8),/,&
     &       46X,'Hydrate       |',4(2X,1pE15.8),/,&
     &       46X,'Ice           |',4(2X,1pE15.8),/,&
     &       43x,' ',86('-'),/,   &
     &       46X,'TOTAL         |',4(2X,1pE15.8),/,&
     &       60x,'|',/,43x,' ',86('='))
!
 6005 FORMAT(/,1X,129('*'),/,1X,129('*'),//)                                            
!
 6010 FORMAT(1pe12.5,1x,11(1pe13.6,1x))                                            
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of VME_Balance
!
!
      RETURN                                                            
! 
      END SUBROUTINE VME_Balance
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE WARN_RF(ELEM,XX,NK1,KC)
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*       ROUTINE FOR PRINTING WARNINGS AND RELATED INFORMATION         *
!*   WHEN THE RANGE IS EXCEEDED AND/OR INITILIALIZATION IS INCORRECT   *
!*                                                                     *
!*                   Version 1.00, September 4, 2003                   *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! ----------
! ... Integer variables
! ----------
! 
      INTEGER, INTENT(IN) :: NK1,KC
! 
      INTEGER :: m
! 
! ----------
! ... Double precision arrays
! ----------
! 
      REAL(KIND=8), DIMENSION(NK1), INTENT(IN) :: XX
! 
! ----------
! ... Character variables
! ----------
! 
      CHARACTER(LEN=5), INTENT(IN) :: ELEM
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  BEGIN WARN_RF
!
!
      WRITE(6,6100) ELEM,(XX(m), m=1,NK1)                      
!
      IF(KC == 0) THEN
         WRITE(6,6101) 
         STOP                                             
      END IF
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6100 FORMAT(' +++++++++ CANNOT FIND PARAMETERS AT ELEMENT *',A8,     &
     &       '*          XX(M) =',10(1X,1pE12.5))                               
 6101 FORMAT(' !!!!!!!!!!!  ERRONEOUS DATA INITIALIZATION  !!!!!!!!!!',   &
     &       11X,'STOP EXECUTION  <=========')                                 
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  END WARN_RF
!
!
      RETURN
!
      END SUBROUTINE WARN_RF
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
! 
!
      SUBROUTINE JACOBIAN_SetUp
! 
! ...... Modules to be used 
! 
         USE Universal_Param
         USE Basic_Param
         USE GenControl_Param
         USE Diffusion_Param
!
         USE H2O_Param
         USE Hydrate_Param
!
         USE Element_Arrays
         USE Connection_Arrays
!
         USE PFMedProp_Arrays
         USE Variable_Arrays
!
         USE SolMatrix_Arrays
         USE TempStoFlo_Arrays
!
         USE H2O_Properties
		 
		 USE MPI_PARAM
		 USE MPI_SUBROUTINES
		 USE MPI_ARRAYS
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*  ROUTINE FOR SETTING UP THE JACOBIAN MATRIX FOR A HYDRATE SYSTEM    *
!*                                                                     *
!*                  Version 1.0 - September 24, 2003                   *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: Rock_HC,Phi_NN,S_phase,Phi_S_Rho,PRES,RERMP,TEMP
! 
      REAL(KIND = 8) :: D1,D2,WT1,WT2,PO1,PO2,GX,AX,FAC1,FAC2,DPX0
      REAL(KIND = 8) :: PRES1,PRES2,DPX,T1,T2,CONI,PERI,DPHI,Phi_N
! 
      REAL(KIND = 8) :: PER1,REL1,S1,VIS1,RHO1,RHO10,H1,PCAP1,PCAP10,W1
      REAL(KIND = 8) :: PER2,REL2,S2,VIS2,RHO2,RHO20,H2,PCAP2,PCAP20,W2
! 
      REAL(KIND = 8) :: RHOX,RHOX0,DR,DR0,Wup_1,Wup_2,XM1,XM2,DEN,DMOBI
      REAL(KIND = 8) :: Phi_S,F_NP_M,XNPMK,HNPM,T1k,T2k
! 
      REAL(KIND = 8) :: Hyd_Heat,Hyd_Mass,DEG_m,DEG_h
      REAL(KIND = 8) :: PS1,PS2,S1H,S2H,S1i,S2i
! 
      REAL(KIND = 8) :: Thrm_ConductGHydr     ! Function name
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: M11,N,NLOC,NLOCP,NMAT,M,NP,K
      INTEGER :: N1,N2,N1LOC,N2LOC,N1LOCP,N2LOCP,L
! 
      INTEGER :: NMAT1,NMAT2,ISO
      INTEGER :: LOC_j,NN1,KK1,N1KL,N2KL,m_1,m_2,mx,nx,Num_E,NEQ_2
! 
      INTEGER :: ICALL = 0, IERR
			integer :: i,j  !by RPS
			REAL    :: aa,bb,cc,dd, ee,ff,gg,hh !By RPS
	
! 
! -------
! ... Saving variables
! -------
! 
      SAVE ICALL,M11,Num_E,NEQ_2
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of JACOBIAN_SetUp
!
!
!
      ICALL = ICALL + 1
      IF(ICALL == 1) THEN
         IF(MPI_RANK==0) WRITE(11,6000)
!
         M11   = MOD(MOP(11),2)
         Num_E = NELL
         NEQ_2 = NEQ*NEQ
!
      ELSE 
         Num_E = NELAL
      END IF
!                          
! ... Some print-out for large MOP(3) values           
!   
      IF_PrOut: IF(MOP(3) >= 1) THEN
         IF(MPI_RANK==0) WRITE(6,6001) KCYC,ITER
         IF(MPI_RANK==0) WRITE(6,6002)       
      END IF IF_PrOut
! 
!***********************************************************************
!*                                                                     *
!*                        BASIC INITIALIZATIONS                        *
!*                                                                     *
!***********************************************************************
! 
      NZ = 0
! 
! ----------
! ... Initialize residuals - CAREFUL: Whole array operation 
! ----------
! 
      R(1:(NELAL+NUL)*NEQ) = 0.0d0
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                        MAIN ELEMENT LOOP                            >
!>  Compute accumulation-related properties, parameters and variables  >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!

      DO_NumEle: DO n=1,NELAL
!                          
! ...... Identify matrix block           
!    
         NLOC  = (n-1)*NEQ
         NLOCP = (n-1)*NK1
!                          
! ...... Assign quantities which depend only on element index N          
!    
         Phi_N = elem(n)%phi
         nmat  = elem(n)%MatNo
!
         Rock_HC = media(nmat)%SpcHt*media(nmat)%DensG&
     &                              *(1.0d0-media(nmat)%Poros)  

					 aa = media(nmat)%SpcHt
					bb = media(nmat)%DensG
					cc = (1.0d0-media(nmat)%Poros)
!
! ----------
! ...... Initialization  
! ----------
! 
         D = 0.0d0   ! CAREFUL: Whole array operation (array D) 
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... EQUATIONS LOOP - Derivative computation 
! ...... 
! >>>>>>>>>>>>>>>>>>
! 
! 
         DO_NumEq: DO m=1,NEQ1
!    
! ......... Initializations
!    
            DEG_m = 0.0d0
            DEG_h = 0.0d0
!    
! -------------    
! ......... Use the current pressure; Check is pressure is the 1st primary variable 
! -------------    
!    
            IF( (F_Kinetic .EQV. .FALSE.) .AND.  &
     &          (elem(n)%StateIndex == 7_1  .OR.  &  
     &           elem(n)%StateIndex == 10_1 .OR. &
     &           elem(n)%StateIndex == 11_1) )     &
     &      THEN
               PRES = Cell_V(n,m-1)%p_AddD(2)   ! Use the stored derived pressure
            ELSE
!    
               IF(M == 2) THEN
                  PRES = X(NLOCP+1)+DX(NLOCP+1)+DELX(NLOCP+1)
               ELSE
                  PRES = X(NLOCP+1)+DX(NLOCP+1)
               END IF
!    
            END IF
!    
! ......... Compute porosity chages 
!    
            DPHI   = Phi_N*( media(nmat)%Compr*(PRES-elem(n)%P)&
     &                      +media(nmat)%Expan*( Cell_V(n,m-1)%TemC&
     &                                          -elem(n)%T&
     &                                         )&
     &                     )
!    
            Phi_NN = Phi_N+DPHI
!write(*,*) ' i am in jacobian_setup'
!write(*,*) 'phi_NN=',Phi_NN 
		
!write(*,*) 'PRES=',PRES
!write(*,*) 'stateIndex=',elem(n)%StateIndex
! 
! 
! >>>>>>>>>>>>>>>>>>>>>
! .........
! ......... PHASE LOOP  
! ......... 
! >>>>>>>>>>>>>>>>>>>>>
! 
! 
            DO_NumPhase: DO np=1,NPH
!    
! ............ Retrieval of saturation and density 
!    
               S_phase = Cell_V(n,m-1)%p_Satr(np)
! 
! ----------------
! ............ Cycle if the phase saturation is zero       
! ----------------
! 
               IF(S_phase == 0.0d0) THEN
!
                  IF(F_Kinetic .EQV. .FALSE.) THEN
                     CYCLE DO_NumPhase
                  ELSE
                     IF(np /= 3) CYCLE DO_NumPhase
                  END IF
!
               END IF
! 
! ----------------
! ............ Compute phase masses       
! ----------------
! 
               Phi_S_Rho = Phi_NN*S_phase*Cell_V(n,m-1)%p_Dens(np)
!write(*,*) 'Cell_V(n,m-1)%p_Dens(np)=',Cell_V(n,m-1)%p_Dens(np)
!write(*,*) 'Phi_S_Rho=',Phi_S_Rho
! 
!***********************************************************************
!*                                                                     *
!*                  For the hydrate equation ONLY !!!                  *
!*                                                                     *
!***********************************************************************
! 
               IF_HydrPhase: IF( np == 3 ) THEN
! 
! -------------------
! ............... Compute hydrate dissociation rate for kinetic reaction       
! -------------------
! 
                  IF_KineticR: IF((F_Kinetic .EQV. .TRUE.) .AND. &
     &                            (elem(n)%StateIndex /= 9_1) ) &
     &            THEN
!                          
! .................. The dissociation contribution to mass          
!    
                     DEG_m = FORD*Cell_V(n,m-1)%p_AddD(2)  
!                          
! .................. The dissociation contribution to heat         
!    
                     DEG_h = DEG_m*Cell_V(n,m-1)%p_AddD(3)
! 
! -------------------
! ............... Compute hydrate mass for equilibrium hydrate reaction        
! -------------------
! 
                  ELSE
! 
                     DEG_m = Phi_S_Rho
! 
                  END IF IF_KineticR
! 
               END IF IF_HydrPhase
! 
! 
! >>>>>>>>>>>>>>>>>>>>>
! ............
! ............ COMPONENT LOOP - Summation of components in each phase  
! ............
! >>>>>>>>>>>>>>>>>>>>>
! 
! 
               DO_NumComp: DO K=1,NK
!                          
! ............... Mass fraction of component K in phase NP ( = phi*S*rho*F_mass(k in np))         
!    
                  D(K,m) = D(K,m)+Phi_S_Rho*Cell_V(n,m-1)%p_MasF(k,np)
!write(*,*) 'D(K,m)=',D(K,m)
!write(*,*) 'Cell_V(n,m-1)%p_MasF(k,np)=',Cell_V(n,m-1)%p_MasF(k,np)
!                          
! <<<<<<<<<<<<                          
! <<<<<<<<<<<< End of the COMPONENT LOOP         
! <<<<<<<<<<<<    
!                          
               END DO DO_NumComp
! 
! ----------------
! ............ Compute internal energy in phase NP         
! ----------------
! 
               D(NK1,m) = D(NK1,m) + Phi_S_Rho*Cell_V(n,m-1)%p_Enth(np)
!     &                   -Phi_NN*S_phase*PRES
!                          
! <<<<<<<<<                          
! <<<<<<<<< End of the PHASE LOOP         
! <<<<<<<<<   
!                          
            END DO DO_NumPhase
! 
		
! ----------------
! ......... Account for rock energy ( = HC_rock * T)           
! ----------------
! 
            D(NK1,M) = D(NK1,M) + Rock_HC*Cell_V(n,m-1)%TemC
! 
!***********************************************************************
!*                                                                     *
!*    Assign accumulation terms at the beginning of each time step     *
!*                                                                     *
!***********************************************************************
! 
            IF_DTBgn: IF(ITER == 1 .AND. m == 1) THEN
!                          
! ............ DOLD(NLOC+K) holds the quantity of component K in element N          
! ............ at the end of the last completed time step         
!    
               DOLD(NLOC+1:NLOC+NEQ) = D(1:NEQ,1)  ! CAREFUL - Whole array operations
!write(*,*) 'dold=',dold
!                          
! ............ For EQUILIBRIUM reaction ONLY !!!!        
! ............ Store the hydrate mass at the beginning of the time step        
!    
               IF( (F_Kinetic .EQV. .FALSE.) .OR.  &
     &            ((F_Kinetic .EQV. .TRUE.) .AND. &
     &             (elem(n)%StateIndex == 9_1) )&
     &           ) Cell_V(n,3)%p_AddD(1) =  DEG_m
!    
            END IF IF_DTBgn
!
            IF(M == 1 .AND. MOP(3) >= 3) THEN
               WRITE(6,6005) elem(n)%name,(D(K,1),K=1,NEQ),DEG_m,DEG_h
            END IF 
! 	
!***********************************************************************
!*                                                                     *
!* Kinetic reaction: Add the hydrate terms to the mass/heat equations  *
!*                                                                     *
!***********************************************************************
! 
            IF_Kinetic: IF(F_Kinetic .EQV. .TRUE.) THEN
! 
! ............ Consider the hydrate contribution
! 
               IF(Cell_V(n,m-1)%p_Satr(3) /= 0.0d0 .OR. &
     &            DEG_m /= 0.0d0) &
     &         THEN
!
                  IF(elem(n)%StateIndex /= 9_1) THEN
                     Hyd_Mass = DEG_m
                     Hyd_Heat = DEG_h
                  ELSE
                     Hyd_Mass = (DEG_m - Cell_V(n,3)%p_AddD(1))
                     Hyd_Heat = Hyd_Mass*Cell_V(n,m-1)%p_AddD(3)
                  END IF
! 
! ............... The CH4, H2O and hydrate equations 
! 
                  D(1,m) = D(1,m) + Hyd_Mass*HCom%H2OMF(1)
                  D(2,m) = D(2,m) + Hyd_Mass*HCom%GasMF(1)  
                  D(3,m) = D(3,m) - Hyd_Mass
! 
! ............... The heat equation 
! 
                  IF(NK == 3) THEN
                     D(4,m) = D(4,m) - Hyd_Heat  ! Inhibitor is NOT considered
                  ELSE
                     D(5,m) = D(5,m) - Hyd_Heat  ! Inhibitor is considered
                  END IF
! 
               ELSE
! 
                  D(3,M)       = 0.0d0
                  DOLD(NLOC+3) = 0.0d0
! 
               END IF
! 
!***********************************************************************
!*                                                                     *
!*      Equilibrium reaction: Add the dissociation heat component      *
!*                                                                     *
!***********************************************************************
! 
            ELSE
! 
! ............ Compute the heat of dissociation 
! 
               Hyd_Heat = (DEG_m - Cell_V(n,3)%p_AddD(1))&
     &                    *Cell_V(n,m-1)%p_AddD(3)
! 
! ............ Account for the heat of dissociation in the heat equation 
! 
               IF(NK == 2) THEN
                  D(3,m) = D(3,m) - Hyd_Heat  ! Inhibitor is NOT considered
               ELSE
                  D(4,m) = D(4,m) - Hyd_Heat  ! Inhibitor is considered
               END IF
! 
            END IF IF_Kinetic
!
! <<<<<<                          
! <<<<<< End of the EQUATIONS LOOP         
! <<<<<<    
!
         END DO DO_NumEq

! 
! 
!***********************************************************************
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*             ASSIGNING ALL SINGLE-ELEMENT-RELATED TERMS              *
!*                                                                     *
!***********************************************************************
!***********************************************************************
!***********************************************************************
! 
! 
!                          
! ...... K is the row index in matrix block (N,N)          
!    
         DO_NumEq1: DO K=1,NEQ
!                          
! ......... Initialize residuals         
!    
            R(NLOC+K) = D(K,1) - DOLD(NLOC+K)

! 
! -------------
! ......... L is the column index in matrix block (N,N)           
! ......... Compute matrix element arising from dependence of 
! ......... component K upon primary variable L          
! -------------
! 
            DO_NumEq2: DO L=1,NEQ
!
               IRN(NZ+1) = NLOC+K
               ICN(NZ+1) = NLOC+L
			   
!
               IF(Phi_NN == 0.0d0 .AND. K == L .AND. K /= NK1) THEN
                  CO(NZ+1) = 1.0d0
               ELSE
                  CO(NZ+1) = -(D(K,L+1) - D(K,1))/DELX(NLOCP+L)
               END IF
!
               NZ = NZ+1

!
!
            END DO DO_NumEq2
!do i=1,neq
!write(*,*) d(i,1)
!write(*,*) (d(i,j), j=1,neq)
!end do
!
! <<<<<<                   
! <<<<<< End of the single-element assignments         
! <<<<<<    
!
         END DO DO_NumEq1
!
! <<<                      
! <<< End of the MAIN ELEMENT LOOP         
! <<<
!
      END DO DO_NumEle
!
					
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                         Sinks & Sources                             >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      IF(NOGN /= 0) CALL Q_SourceSink
      IF(IGOOD == 3) RETURN
!                          
      IF(MOP(15) > 1) CALL HeatExch_An
! 
! 
!***********************************************************************
!*                                                                     *
!*        FOR MODIFIED PERMEABILITY, SCALE CAPILLARY PRESSURE          *
!*                                                                     *
!***********************************************************************
! 
! 

      IF_PermMod: IF(PermModif /= 'NONE') THEN
!                          
         IF(iter == 1) CALL EOS      ! For the first iteration only
!                          
         IF(icall == 1) THEN 
            Num_E = NELL
         ELSE
            Num_E = NELAL
         END IF
!                          
         DO_NEQ: DO mx = 0,NEQ
            DO_NumEl: DO nx = 1,Num_E
               Cell_V(nx,mx)%p_PCap(1:NPH) = Cell_V(nx,mx)%p_PCap(1:NPH)&
     &                                      /pm(nx)
            END DO DO_NumEl
         END DO DO_NEQ
!                          
      END IF IF_PermMod
!
!
!
!	------Communicate update set (boundary element of neighboring subdomains and external set) before moving on to connection loop---


	  call MPI_COMM_ELEM
	  
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                      MAIN CONNECTION LOOP                           >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!                          
! ... Initialization - CAREFUL: Whole array operation         
!    
      FORALL (n=1:Num_MobPhs) conV%FluxVF(n) = 0.0d0
! 
! 
! >>>>>>>>>>>>>>>>>>
! ...
! ... At each interface, we have one flux for each component, which is    
! ... a sum of contributions from all phases. For computing            
! ... derivatives, we need also the fluxes obtained by incrementing   
! ... any of the NEQ variables for the "1st" element, and any of the  
! ... NEQ variables for the "2nd" element   
! ... 
! >>>>>>>>>>>>>>>>>>
! 
! 

      DO_NumConX: DO n=1,NCONT
!
!
!
         n1 = conx(n)%n1
         n2 = conx(n)%n2
         IF(n1 == 0 .OR. n2 == 0)      CYCLE DO_NumConX  ! At least one non-existent element in the connection
         IF(  (n1 > NELAL .and. n1<=NELL) .AND.&
				(n2 > NELAL .and. n2<=NELL) ) CYCLE DO_NumConX  ! Two inactive elements in the connection
!           
! ...... Identify location of matrix blocks in element arrays          
!    
         N1LOC  = (n1-1)*NEQ
         N2LOC  = (n2-1)*NEQ
         N1LOCP = (n1-1)*NK1
         N2LOCP = (n2-1)*NK1
		 
		 !N1LOC and N2LOC will be different for update set
		         
		 if(n1>NELL) 	N1LOC = (n1 - 1 - (NELL-NELAL))*NEQ
		 if(n2>NELL) 	N2LOC = (n2 - 1 - (NELL-NELAL))*NEQ
			
!    
! ----------
! ...... Basic connection parameters & quantities        
! ----------
! 
         D1 = conx(n)%d1
         D2 = conx(n)%d2
!                      
! ...... Spatial interpolation factors       
!    
         WT1 = D2/(D1+D2)
         WT2 = 1.0d0-WT1
!                        
! ...... Rock type/material and associated properties       
!    
         nmat1 = elem(n1)%MatNo
         nmat2 = elem(n2)%MatNo
!                                  
         PO1 = media(nmat1)%Poros
         PO2 = media(nmat2)%Poros
!                   
         ISO = conx(n)%ki
         GX  = conx(n)%beta*GF
         AX  = conx(n)%area 
!          
! ...... Assign factors for flux terms      
!    
		
         IF(N1 <= NELAL .OR. N1>NELL) THEN
            FAC1 = FORD/elem(n1)%vol
         ELSE
            FAC1 = 0.0d0
         END IF
!
         IF(N2 <= NELAL .or. N2>NELL) THEN
            FAC2 = FORD/elem(n2)%vol
         ELSE
            FAC2 = 0.0d0
         END IF
!               
! ----------    
! ...... Check is pressure is the 1st primary variable  
! ...... in connection element #1 -  Set current pressure 
! ----------    
!    
         IF( (F_Kinetic .EQV. .FALSE.) .AND.  &
     &       (elem(n1)%StateIndex == 7_1  .OR.    &
     &        elem(n1)%StateIndex == 10_1 .OR. &
     &        elem(n1)%StateIndex == 11_1) )    &
     &   THEN
            PRES1 = Cell_V(n1,0)%p_AddD(2)   ! Use the stored derived pressure
         ELSE
            PRES1 = X(N1LOCP+1)+DX(N1LOCP+1)
         END IF
!    
! ----------    
! ...... Check is pressure is the 1st primary variable  
! ...... in connection element #1 -  Compute current pressure 
! ----------    
!    
         IF( (F_Kinetic .EQV. .FALSE.) .AND.  &
     &       (elem(n2)%StateIndex == 7_1  .OR.   &
     &        elem(n2)%StateIndex == 10_1 .OR. &
     &        elem(n2)%StateIndex == 11_1) )     &
     &   THEN
            PRES2 = Cell_V(n2,0)%p_AddD(2)   ! Use the stored derived pressure
         ELSE
            PRES2 = X(N2LOCP+1)+DX(N2LOCP+1)
         END IF
!                          
! ...... Pressure gradient betwen the two connection elements      
!    
         DPX0 = (PRES2-PRES1)/(D1+D2)  !is this the pressure gradient of gas or water? !RPS ?
!
         IF(MOP(3) >= 4) Write(6,6008) n,elem(n1)%name,elem(n2)%name,&
     &                                   FAC1,FAC2
!
! ----------
! ...... Flux initialization - CAREFUL: Whole array operation 
! ----------
! 
         F = 0.0d0
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... PERTURBATION LOOP
! ......
! ...... M=1 corresponds to the parameter values at the state point = latest updated variables 
! ...... M=2, ... ,NEQ+1 are the NEQ perturbations corresponding to incrementing 
! ......                 the variables for element N1 
! ...... M=NEQ+2, ... ,2*NEQ+1 are the NEQ perturbations corresponding to 
! ......                       incrementing the variables for element N2
! ...... 
! >>>>>>>>>>>>>>>>>>
! 
! 
         DO_NumFlx: DO m=1,NFLUX
! 
! -------------
! ......... Determine indices of secondary variables           
! ......... m_1 denotes fluxes incremented at N1, m_2 denotes fluxes incremented at N2           
! -------------
! 
            IF_MValB: IF(m >= 2 .AND. m <= NEQ1) THEN
                         m_1 = m
                         m_2 = 1
                      ELSE IF(m == 1) THEN
                         m_1 = 1
                         m_2 = 1
                      ELSE
                         m_1 = 1
                         m_2 = m - NEQ
                      END IF IF_MValB
! 
! >>>>>>>>>>>>>
! .........         
! ......... Compute pressure gradients          
! .........         
! >>>>>>>>>>>>>
! 
! ......... Check if P is the 1st primary variable in connection element #1           
! -------------
! 
            IF( (F_Kinetic .EQV. .FALSE.) .AND. & 
     &          (elem(n1)%StateIndex == 7_1  .OR.    &
     &           elem(n1)%StateIndex == 10_1 .OR. &
     &           elem(n1)%StateIndex == 11_1) )     &
     &      THEN
! 
               PRES1 = Cell_V(n1,m_1-1)%p_AddD(2)                 ! Use the stored derived pressure
! 
            ELSE
! 
               IF(M == 2) THEN                                    ! Compute the pressure
                  PRES1 = X(N1LOCP+1)+DX(N1LOCP+1)+DELX(N1LOCP+1)
               ELSE 
                  PRES1 = X(N1LOCP+1)+DX(N1LOCP+1)
               END IF 
! 
            END IF
! 
! -------------
! ......... Check if P is the 1st primary variable in connection element #2           
! -------------
! 
            IF( (F_Kinetic .EQV. .FALSE.) .AND.  &
     &          (elem(n2)%StateIndex == 7_1  .OR.   &
     &           elem(n2)%StateIndex == 10_1 .OR. &
     &           elem(n2)%StateIndex == 11_1) )     &
     &      THEN
! 
               PRES2 = Cell_V(n2,m_2-1)%p_AddD(2)                 ! Use the stored derived pressure
! 
            ELSE
! 
               IF(M == NEQ+2) THEN                                ! Compute the pressure
                  PRES2 = X(N2LOCP+1)+DX(N2LOCP+1)+DELX(N2LOCP+1)
               ELSE 
                  PRES2 = X(N2LOCP+1)+DX(N2LOCP+1)
               END IF 
! 
            END IF
!
! ......... Pressures and pressure gradients
!
            DPX = (PRES2-PRES1)/(D1+D2)
! 
! >>>>>>>>>>>>>
! .........         
! ......... Compute thermal conductivity (contributions from all phases + rock)          
! .........         
! >>>>>>>>>>>>>
! 
            T1 = Cell_V(n1,m_1-1)%TemC
            T2 = Cell_V(n2,m_2-1)%TemC
! 
! -------------
! ......... Computation of conductive heat fluxes           
! ......... ONLY when the radiative heat transfer coefficient is non-negative         
! -------------
! 
            IF_SigPos: IF(CondHeat_Flag .EQV. .TRUE.) THEN
!
! ............ Retrieve parameters
!
               S1  = MAX(Cell_V(n1,m_1-1)%p_Satr(2),0.0d0)
               S2  = MAX(Cell_V(n2,m_2-1)%p_Satr(2),0.0d0)
!
               S1H = Cell_V(n1,m_1-1)%p_Satr(3)
               S2H = Cell_V(n2,m_2-1)%p_Satr(3)
!
               S1i = Cell_V(n1,m_1-1)%p_Satr(4)
               S2i = Cell_V(n2,m_2-1)%p_Satr(4)
!
! ............ If the thermal conductivity of the gases is accounted for
!
               IF(MOP(10) >= 2) THEN
                  IGOOD = P_Sat_H2O_S(T1,PS1) 
                  IGOOD = P_Sat_H2O_S(T2,PS2) 
               END IF
!
! ............ Compute thermal conductivity (contributions from all phases + rock)
!
               CONI = Thrm_ConductGHydr(n1,n2,PRES1,PRES2,T1,T2,D1,D2,&
     &                                  S1,S2,S1H,S2H,S1i,S2i,&
     &                                  Cell_V(n1,m_1-1)%p_Dens(2),&
     &                                  Cell_V(n2,m_2-1)%p_Dens(2),&
     &                                  PS1,PS2,WT1,WT2,&
     &                                  NMAT1,NMAT2,0.0d0,&
     &                                  Cell_V(n1,m_1-1)%p_MasF(2,1),&
     &                                  Cell_V(n2,m_2-1)%p_MasF(2,1),&
     &                                  MOP(10))
!
! ............ Conductive heat flow = K_th*Area*DT/Dx

	
!this is done by RPS for validation of LIANG_et_al.
	
						if (nmat1 == 3 .or. nmat2 ==3)then
              !coni = 0.0
              F(NK1,m) = 0.0!CONI*AX*(T2-T1)/(D1+D2)
							else if (nmat1 ==2 .or. nmat2==2)then
              F(NK1,m) = 6.0*AX*(T2-T1)
              else
              F(NK1,m) = CONI*AX*(T2-T1)/(D1+D2)
              end if
!
               !F(NK1,m) = CONI*AX*(T2-T1)/(D1+D2)
!           
            END IF IF_SigPos
! 
! -------------
! ......... Computation of radiative heat fluxes           
! -------------
! 
            IF_SigNoZ: IF(RadiHeat_Flag .EQV. .TRUE.) THEN
!
               T1k = T1 + 273.15d0
               T2k = T2 + 273.15d0
!
               F(nk1,m) = F(nk1,m)&
     &                   +ax*ABS(SIG(n))*Stef_Boltz*( T2k*T2k*T2k*T2k&
     &                                               -T1k*T1k*T1k*T1k&
     &                                              )
!           
            END IF IF_SigNoZ
! 
! -------------
! ......... Compute the interface intrinsic permeability (harmonic average)        
! -------------
! 
            CALL PERM_Interface(ISO,NMAT1,NMAT2,PRES1,PRES2,WT1,WT2,&
     &                          PermModif,pm(n1),pm(n2),PER1,PER2,PERI)
! 
! -------------
! ......... Print-outs for large MOP(3) values        
! -------------
! 
            IF((MOP(3) >= 6 .AND. m == 1) .OR. MOP(3) >= 8) THEN
               WRITE(6,6010) m,PRES1,PRES2,DPX
               WRITE(6,6011) CONI,T1,T2,(T2-T1)/(D1+D2),F(NK1,m)
            END IF
! 
! 
! >>>>>>>>>>>>>>>>>>
! .........
! ......... PHASE LOOP (Obtain Flux for Each Phase)
! .........
! >>>>>>>>>>>>>>>>>>
! 
! 
            DO_NumPhFlx: DO np=1,Num_MobPhs
!
! ............ Initializations
!
               IF(M == 1) conx(n)%VelPh(np) = 0.0d0
               F_NP_M = 0.0d0
!
! ............ Relative Permeabilities
!
               REL1 = Cell_V(n1,m_1-1)%p_KRel(np)
               REL2 = Cell_V(n2,m_2-1)%p_KRel(np)
!
! ............ Check phase mobility & realistic perm. regime
!
               IF(REL1 == 0.0d0 .AND. REL2 == 0.0d0) GO TO 500
               IF(PERI == 0.0d0) GO TO 500
! 
! ----------------
! ............ Retrieve properties and parameters of the two connection elements  
! ----------------
! 
               S1     = Cell_V(n1,m_1-1)%p_Satr(np)
               VIS1   = Cell_V(n1,m_1-1)%p_Visc(np)
               RHO1   = Cell_V(n1,m_1-1)%p_Dens(np)
               RHO10  = Cell_V(n1,0)%p_Dens(np)
               H1     = Cell_V(n1,m_1-1)%p_Enth(np)
               PCAP1  = Cell_V(n1,m_1-1)%p_PCap(np)
               PCAP10 = Cell_V(n1,0)%p_PCap(np)
!
               S2     = Cell_V(n2,m_2-1)%p_Satr(np)
               VIS2   = Cell_V(n2,m_2-1)%p_Visc(np)
               RHO2   = Cell_V(n2,m_2-1)%p_Dens(np)
               RHO20  = Cell_V(n2,0)%p_Dens(np)
               H2     = Cell_V(n2,m_2-1)%p_Enth(np)
               PCAP2  = Cell_V(n2,m_2-1)%p_PCap(np)
               PCAP20 = Cell_V(n2,0)%p_PCap(np)
!
! ............ Compute weighted interface density 
!
               IF_W1Val: IF(RHO1 == 0.0d0) THEN
                            W1 = 0.0d0
                         ELSE IF(RHO2 == 0.0d0) THEN
                            W1 = 1.0d0
                         ELSE
                            W1 = 5.0d-1
                         END IF IF_W1Val
!
               W2 = 1.0d0-W1
!
               RHOX  = W1*RHO1  + W2*RHO2
               RHOX0 = W1*RHO10 + W2*RHO20
!
! ............ Effective pressure gradient 
!
               DR  = DPX  + (PCAP2-PCAP1)/(D1+D2)   - RHOX*GX
               DR0 = DPX0 + (PCAP20-PCAP10)/(D1+D2) - RHOX0*GX
! 
! ----------------
! ............ Determine upstream weighting factors 
! ----------------
! 
               IF(DR0 > 0.0d0 .AND. S2 == 0.0d0) GO TO 500
               IF(DR0 < 0.0d0 .AND. S1 == 0.0d0) GO TO 500
!
               IF_UpstrW: IF(M11 >= 1) THEN
                             Wup_1 = W1
                          ELSE 
                             IF_PreCrit: IF(DR0 > 0.0d0) THEN
                                            Wup_1 = 1.0d0 - WUP
                                         ELSE 
                                            Wup_1 = WUP
                                         END IF IF_PreCrit 
                          END IF IF_UpstrW
!
               IF(RHO1 == 0.0d0) Wup_1 = 0.0d0
               IF(RHO2 == 0.0d0) Wup_1 = 1.0d0
!
               Wup_2 = 1.0d0-Wup_1
!
! ............ Relative permeability when the nodal point is at the interface 
! ............ (i.e., nodal distance = 0) -> Use relative permeability of other cell 
!
               IF(D1 == 0.0d0 .AND. REL1 == 0.0d0) REL1=REL2
               IF(D2 == 0.0d0 .AND. REL2 == 0.0d0) REL2=REL1
! 
! ----------------
! ............ Adjust intrinsic permeability for upstream weighting  
! ............ Upstream for MOP(11) <= 1; Harmonic for MOP(11) >1 (computed earlier)
! ----------------
! 
               IF_MOP11: IF(MOP(11) <= 1) THEN
                            IF(DR0 > 0.0d0) THEN
                               PERI = PER2
                            ELSE 
                               PERI = PER1
                            END IF
                         END IF IF_MOP11
!
! ............ Intrinsic permeability when the nodal point is at the interface 
! ............ (i.e., nodal distance = 0) -> Use intrinsic permeability of other cell 
!
               IF(D1 == 0.0d0) PERI=PER2
               IF(D2 == 0.0d0) PERI=PER1
! 
! ----------------
! ............ Compute interface mobilities 
! ----------------
! 
               IF_IntMob: IF(MOP(11) == 4) THEN
!
                  XM1 = PER1*REL1/(VIS1+1.0d-20)
                  XM2 = PER2*REL2/(VIS2+1.0d-20)
                  DEN = WT1*XM1 + WT2*XM2
!
                  IF(DEN /= 0.0d0) THEN
                     DMOBI = XM1*XM2/DEN
                  ELSE
                     DMOBI = 0.0d0
                  END IF
!
               ELSE
!
                  DMOBI = (  Wup_1*REL1/(VIS1+1.0d-20) &
     &                     + Wup_2*REL2/(VIS2+1.0d-20) )*PERI
!
               END IF IF_IntMob
! 
! ----------------
! ............ Compute pore velocities for gaseous and liquid phases
! ----------------
! 
               IF_PoreVel: IF(m == 1) THEN
! 
                  Phi_S = Wup_1*elem(n1)%phi*S1 + Wup_2*elem(n2)%phi*S2
                  IF(Phi_S /= 0.0d0) conx(n)%VelPh(np) = DMOBI*DR/Phi_S
! 
               END IF IF_PoreVel
!
! ............ Redefine interface density with upstream weighting
!
               IF(MOP(18) == 0 .OR. S1 == 0.0d0 .OR. S2 == 0.0d0) THEN
                  RHOX = Wup_1*RHO1+Wup_2*RHO2
               END IF  
! 
! ----------------
! ............ Compute flux M in phase NP
! ----------------
! 
               F_NP_M = DMOBI*RHOX*DR*AX
! 
! 
! >>>>>>>>>>>>>>>>>>>>>
! ............
! ............ COMPONENT LOOP (Obtain Flux for Each Component in Each Phase)
! ............
! >>>>>>>>>>>>>>>>>>>>>
! 
! 
               DO_NumCompn: DO k=1,NK
!
                  XNPMK = Wup_1*Cell_V(n1,m_1-1)%p_MasF(k,np)    &
     &                   +Wup_2*Cell_V(n2,m_2-1)%p_MasF(k,np)
!
                  IF(m == 1 .AND. k == 2) THEN
                     conV(n)%FluxVF(np) = XNPMK*F_NP_M
                  END IF
!
! ............... Compute fluid component fluxes
!
                  F(k,m) = F(k,m)+XNPMK*F_NP_M
!
!
! <<<<<<<<<<<<                          
! <<<<<<<<<<<< End of the COMPONENT LOOP         
! <<<<<<<<<<<<    
               END DO DO_NumCompn
! 
! ----------------
! ............ Compute heat flux in phase NP
! ----------------
! 
               HNPM     = Wup_1*H1 + Wup_2*H2
!
               F(NK1,m) = F(NK1,m) + HNPM*F_NP_M
!
!
!
  500          CONTINUE
! 
! ----------------
! ............ Print-outs for MOP(3) >= 7
! ----------------
! 
               IF_PrintOut: IF((MOP(3) == 7 .AND. M == 1) .OR.   & 
     &                         (MOP(3) == 8 .AND. M == 1) .OR.   & 
     &                          MOP(3) == 9       &               
     &                        ) THEN
!
                  WRITE(6,6012) NP,PER1,PER2,PERI
                  WRITE(6,6014) S1,REL1,VIS1,RHO1,H1,PCAP1
                  WRITE(6,6016) S2,REL2,VIS2,RHO2,H2,PCAP2
                  WRITE(6,6018) DMOBI,RHOX,DR,F_NP_M,HNPM
!
                  WRITE(6,6020) (F(K,M),K=1,NK1)
!
               END IF IF_PrintOut
! 
! ----------------
! ............ Store the fluxes in each phase (at the state point)
! ----------------
! 
               IF(m == 1) conx(n)%FluxF(np) = F_NP_M   
!
! <<<<<<<<<                          
! <<<<<<<<< End of the PHASE LOOP         
! <<<<<<<<<    
!
            END DO DO_NumPhFlx
!
! <<<<<<                          
! <<<<<< End of the FLUX LOOP         
! <<<<<<    
!
         END DO DO_NumFlx
! 
! 
!***********************************************************************
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*                     ASSIGN ALL INTERFACE TERMS                      *
!*                                                                     *
!***********************************************************************
!***********************************************************************
!***********************************************************************
! 
! 
         conx(n)%FluxH = F(NK1,1)
! 
! 
! >>>>>>>>>>>>>>>>>>>>>
! ......
! ...... EQUATION-1 LOOP (Filling-up the Jacobian matrix)
! ...... K is the row index within a block pertaining to element N1 or N2      
! ......
! >>>>>>>>>>>>>>>>>>>>>
! 
! 
         DO_NumEqs1: DO K=1,NEQ
! 
! -------------
! ......... Compute flux contributions to residuals      
! -------------
! 
            IF(N1 <= NELAL ) R(N1LOC+K) = R(N1LOC+K) - FAC1*F(K,1)
! 
            IF(N2 <= NELAL ) R(N2LOC+K) = R(N2LOC+K) + FAC2*F(K,1)
			
! 
! 
! >>>>>>>>>>>>>>>>>>>>>
! .........
! ......... EQUATION-2 LOOP (Filling-up the Jacobian matrix)
! ......... L is the column index within a block (element N1 or N2)      
! .........
! >>>>>>>>>>>>>>>>>>>>>
! 
! 
            DO_NumEqs2: DO L=1,NEQ
! 
               IF_N1N2Act: IF((N1 <= NELAL .OR. N1>NELL) .AND. (N2 <= NELAL .OR. N2>NELL)) THEN
! 
! ............... Off-diagonal term in equation for element N1,    
! ............... arising from dependence of component K-flux     
! ............... upon variable L in element N2    
! ............... Do only if n1 is local element
				  IF(n1<=NELAL) THEN
					  IRN(NZ+1) = N1LOC+K
					  ICN(NZ+1) = N2LOC+L
	!    			 
					  CO(NZ+1) = FAC1*(F(K,L+1+NEQ)-F(K,1))/DELX(N2LOCP+L)
					  NZ       = NZ+1
				  END IF
! 
! ............... Off-diagonal term in equation for element N2,    
! ............... arising from dependence of component K-flux     
! ............... upon variable L in element N1    
! ............... Do only if n2 is local element
				  
				  IF(n2<=NELAL) THEN
					  IRN(NZ+1) = N2LOC+K
					  ICN(NZ+1) = N1LOC+L
	!    
					  CO(NZ+1) = -FAC2*(F(K,L+1)-F(K,1))/DELX(N1LOCP+L)
					  NZ       = NZ+1
				  END IF
! 
               END IF IF_N1N2Act
! 
! 
! ............ Diagonal term in equation for element N1,    
! ............ arising from dependence of component K-flux     
! ............ upon variable L in element N1    
! ............ Do only if N1 is local element
! 
               IF_N1Act: IF(N1 <= NELAL) THEN
!    
                  N1KL     = (N1-1)*NEQ*NEQ+(K-1)*NEQ+L
				  
                  CO(N1KL) =   CO(N1KL) &
     &                       + FAC1*(F(K,L+1)-F(K,1))/DELX(N1LOCP+L)
! 
               END IF IF_N1Act
! 
! 
! ............ Diagonal term in equation for element N2,    
! ............ arising from dependence of component K-flux     
! ............ upon variable L in element N2    
! ............ Do only if N2 is local element
! 
               IF_N2Act: IF(N2 <= NELAL) THEN
! 
                  N2KL     = (N2-1)*NEQ*NEQ+(K-1)*NEQ+L
!    				
                  CO(N2KL) =   CO(N2KL) &
     &                       - FAC2*(F(K,L+1+NEQ)-F(K,1))/DELX(N2LOCP+L)
!    
               END IF IF_N2Act
!
! <<<<<<<<<                          
! <<<<<<<<< End of the EQUATION-2 LOOP         
! <<<<<<<<<    
!
            END DO DO_NumEqs2
!
! <<<<<<                          
! <<<<<< End of the EQUATION-1 LOOP         
! <<<<<<    
!
            END DO DO_NumEqs1
!
! <<<                          
! <<< End of the CONNECTIONS LOOP         
! <<<    
!
      END DO DO_NumConX
!

!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                        CONVERGENCE TEST                             >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      IF(MOP(3) >= 2) THEN
         if(MPI_RANK==0) WRITE(6,6022)
         WRITE(6,6024) (elem(n)%name,(R((N-1)*NEQ+K),K=1,NEQ), N=1,NELAL)
      END IF
! 
! -------
! ... Compute residuals - CAREFUL: Whole array/subarray operations
! -------
! 
      WHERE(ABS(DOLD(1:(NELAL+NUL)*NEQ)) > RE2) 
         WKAREA(1:(NELAL+NUL)*NEQ) = ABS(R(1:(NELAL+NUL)*NEQ)/DOLD(1:(NELAL+NUL)*NEQ))
      ELSEWHERE
         WKAREA(1:(NELAL+NUL)*NEQ) = ABS(R(1:(NELAL+NUL)*NEQ)/RE2)
      END WHERE
! 
! -------
! ... Determine maximum residual and its location in the 1-D array
! ... CAREFUL: Whole array/subarray operations
! -------
! 
      RERMP = MAXVAL(WKAREA(1:(NELAL+NUL)*NEQ))
!
      JVECT(1:1) = MAXLOC(WKAREA(1:(NELAL+NUL)*NEQ))
	  
!.... Determining max residual among all the processes
	  
	  call MPI_ALLREDUCE(RERMP,RERM,1,MPI_REAL8,MPI_MAX,MPI_COMM_WORLD,IERR)
	  
	  if(RERM /= RERMP) JVECT(1) = -1
	  
! 
! -------
! ... Determine the location of maximum residual (Cell # and Equation #)
! -------
! 
      LOC_j = JVECT(1)
!
	  if(LOC_j/=-1) THEN
		  NN1 = LOC_j/NEQ
		  KK1 = MOD(LOC_j,NEQ)
	  
	!
		  IF(KK1 == 0) THEN
			 NER = NN1
			 KER = NEQ
		  ELSE 
			 NER = NN1+1
			 KER = KK1
		  END IF 
	  else
		  NER = -1
		  KER = -1
	  end if
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,  'JACOBIAN_SetUp    1.0   24 September 2004',6X,   & 
     &           'Assemble all accumulation and flow terms - ',    &
     &           'Hydrates in porous media')
!
 6001 FORMAT(/,' !!!!!!!!!!   ==> SUBROUTINE "JACOBIAN_SetUp":   ', &   
     &         '[KCYC,ITER] = [',I4,',',I3,']',/)
 6002 FORMAT(' <=><=><=><=><=>  ACCUMULATION TERMS:  Mass balances ',    &
     &       'first, Energy balances last',/)
!
 6005 FORMAT('    At element "',A8,'":  ',5(1X,1pe17.10))
 6008 FORMAT(/,' ::::: Connection #',I5,'   Elements: (',A8,',',A8,')',    &
     &         '  =>  (DELT/V1) = ',1pE12.5,'   (DELT/V2) = ',1pE12.5)
!
 6010 FORMAT(/,'   *** FLUX NO.',I3,15X,   &
     &         '      P1 = ',1pE12.5,'  P2 = ',1pE12.5,   &
     &         '    DPX = ',1pE12.5)
 6011 FORMAT(21X,'       CONI = ',1pE12.5,'    T1 = ',1pE12.5,    &
     &           '    T2 = ',1pE12.5,' DTX = ',1pE12.5,&
     &           '   FNK1 = ',1pE12.5)
!
 6012 FORMAT(9X,'NP =',I2,'    PER1 = ',1pE12.5,' PER2= ',1pE12.5,&
     &          '   PERI = ',1pE12.5)
 6014 FORMAT('       S1   = ',1pE12.5,'  REL1 = ',1pE12.5,&
     &       '  VIS1 = ',1pE12.5,   &
     &       '  RHO1 = ',1pE12.5,'  H1 = ',1pE12.5,'  PCAP1 = ',1pE12.5)
 6016 FORMAT('       S2   = ',1pE12.5,'  REL2 = ',1pE12.5,&
     &       '  VIS2 = ',1pE12.5,   &
     &       '  RHO2 = ',1pE12.5,'  H2 = ',1pE12.5,'  PCAP2 = ',1pE12.5)
 6018 FORMAT('       DMOBI= ',1pE12.5,'  RHOX = ',1pE12.5,  &
     &       '    DR = ',1pE12.5,'  F_NP_M = ',1pE12.5,&
     &       ' HNPM= ',1pE12.5)
 6020 FORMAT('     * FLOW TERMS',8(1X,1pE12.5))
!
 6022 FORMAT(/,' => => => => =>  RESIDUALS:  Mass balances first, ',&
     &         'Energy balance last',/)
 6024 FORMAT('    AT ELEMENT "',A8,'":   ',4(1X,1pe17.10))
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of JACOBIAN_SetUp
!
!
      RETURN
! 
      END SUBROUTINE JACOBIAN_SetUp
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
!
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<






