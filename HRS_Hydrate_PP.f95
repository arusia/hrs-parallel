!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                                                                     >
!>             HRS_Hydrate_PP.f95: Module including all                >
!>             hydrate-related properties and processes                >
!>                                                                     >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      MODULE EOS_DefaultParam
! 
! ----------
! ...... Character parameter arrays     
! ----------
! 
         CHARACTER(LEN = 3), PARAMETER, &
     &     DIMENSION(11) :: EOS_state = (/ "Gas", "Aqu", "AqG",  &
     &                                     "IcG", "AqH", "IcH",  &
     &                                     "AGH", "AIG", "AIH",  &
     &                                     "IGH", "QuP"         /) 
!
!
! 
         CONTAINS
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
         SUBROUTINE EOS_DefaultNum(EOS_name,MaxNum_MComp,MaxNum_Equat,&
     &                             MaxNum_Phase,MaxNum_MobPh,M_Add)
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*        Routine for default (maximum) numbers of components,         *
!*          equations and phases of the EOS used by TOUGH-Fx           *
!*                                                                     *
!*                  Version 1.0 - September 29, 2004                   *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
		 USE MPI_PARAM
		 
         IMPLICIT NONE
! 
! -------------
! ...... Integer variables
! -------------
! 
         INTEGER :: MaxNum_MComp,MaxNum_Equat,&
     &              MaxNum_Phase,MaxNum_MobPh,M_Add
! 
! -------------
! ...... Character variables
! -------------
! 
         CHARACTER(LEN = 15) :: EOS_name
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of EqTemp_HydrCH4 
!
!
         IF(MPI_RANK==0) WRITE(11,6000)
! 
! 
!***********************************************************************
!*                                                                     *
!*         DEFAULT NUMBERS FOR THE HYDRATE EQUATION OF STATE           *
!*                                                                     *
!***********************************************************************
! 
! 

         IF_Defaults: IF(EOS_Name(1:11) == 'HYDRATE-EQU') THEN
! 
                         MaxNum_MComp = 3   ! Maximum number of mass components
                         MaxNum_Equat = 4   ! Maximum number of equations
                         MaxNum_Phase = 4   ! Maximum number of phases
                         MaxNum_MobPh = 2   ! Maximum number of mobile phases
! 
                      ELSE IF(EOS_Name(1:11) == 'HYDRATE-KIN') THEN
! 
                         MaxNum_MComp = 4   ! Maximum number of mass components
                         MaxNum_Equat = 5   ! Maximum number of equations
                         MaxNum_Phase = 4   ! Maximum number of phases
                         MaxNum_MobPh = 2   ! Maximum number of mobile phases
! 
                      ELSE 
! 
                         WRITE(6, 6001) EOS_Name
                         WRITE(50,6001) EOS_Name
                         STOP
! 
                      END IF IF_Defaults
!
! ----------
! ...... Adjust the parameter controlling the size of the auxilliary parameter array
! ----------
!
         IF((EOS_Name(1:7) == 'HYDRATE') .AND. (M_Add < 3)) M_Add = 3
!     
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'EOS_DefaultNum    1.0   29 September 2004',6X,&
     &         'Default parameters of the hydrate equation of state ')
!
 6001 FORMAT(//,20('ERROR-'),//,T33,&
     &             '       S I M U L A T I O N   A B O R T E D',/,&
     &         T20,'The equation of state specified by EOS_Name = ',&
     &         A15,' is unavailable'&
     &       /,T32,'               CORRECT AND TRY AGAIN',     &
     &       //,20('ERROR-'))
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of EqTemp_HydrCH4
!
!
            RETURN
!
         END SUBROUTINE EOS_DefaultNum
!
!
!
      END MODULE EOS_DefaultParam
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      MODULE Hydrate_Param
! 
         SAVE
! 
! ----------
! ...... Double precision parameters
! ----------
! 
         REAL(KIND = 8), PARAMETER :: MW_CH4 = 1.604300d1
! 
         REAL(KIND = 8), PARAMETER :: Tc_quad_hyd = 1.0d-2  ! Quadruple point temperature
! 
! ----------
! ...... Double precision variables
! ----------
! 
         REAL(KIND = 8) :: P_quad_hyd  ! Quadruple point pressure
! 
         REAL(KIND = 8) :: MW_Hydrate  ! Molecular weight of the composite hydrate
! 
         REAL(KIND = 8) :: Initial_HydMass  ! Initial hydrate mass
! 
         REAL(KIND = 8) :: Area_Factor      ! Initial hydrate mass
! 
         REAL(KIND = 8) :: T_MaxOff,&  ! Maximum T-shift in the equilibrium curve due to inhibitor (C)
     &                     C_MaxOff, & ! Salt mole fraction in H2O at T_MaxOff (kg/m^3)
     &                     MW_Inhib,  &! Molecular weight of the inhibitor (salt or alcohol) (g/mole)
     &                     D_Inhib,   &! Density of the inhibitor (salt or alcohol) (kg/m^3)
     &                     H_InhSol,  &! Enthalpy of inhibitor dissolution (J/kg)
     &                     DifCo_Inh  ! Diffusion coefficient of inhibitor (m^2/s)
! 
         REAL(KIND = 8) :: Activ_En, &  ! Activation energy for GH dissociation [J/mol]
     &                     InRate_k0   ! At input: intrinsic dissoc. rate constant [mol/(m^2.Pa.s)]
!                                      !    Later converted to [Kg/(m^2.Pa.s)] in "READ_HydrateData"
! 
! ----------
! ...... Derived-type variables     
! ----------
! 
         TYPE HydrProperties
! 
            INTEGER :: N_ThC           ! # of coefficients in the k_theta polynomial
            INTEGER :: N_SpH           ! # of coefficients in the sp. heat polynomial
            INTEGER :: N_Rho           ! # of coefficients in the density polynomial
! 
            REAL(KIND = 8), &
     &      DIMENSION(:),   &
     &      POINTER :: p_ThC           ! Coefficients of the hydrate thermal conductivity 
!                                      ! k_theta polynomial function [W/m/C^n, n=1,...,N_ThC]
            REAL(KIND = 8), &
     &      DIMENSION(:),   &
     &      POINTER :: p_SpH           ! Coefficients of the hydrate specific 
!                                      ! k_theta polynomial function [W/m/C^n, n=1,...,N_SpH]
            REAL(KIND = 8), &
     &      DIMENSION(:),   &
     &      POINTER :: p_Rho           ! Coefficients of the hydrate density 
!                                      ! polynomial function [kg/m^3/C^(n-1), n=1,...,N_Rho]
!                                      ! Later converted to Kg/... in "READ_MainInputF"
! 
         END TYPE HydrProperties
! 
! 
         TYPE HydrComposition
! 
            INTEGER                          :: Ncom   ! The number of components of the ... 
!                                                      ! ... composite hydrate
            CHARACTER(LEN = 6), DIMENSION(2) :: nameG  ! The name of the hydrate-forming gases ...
!   
                                                  ! ... in the composite hydrate
            REAL(KIND = 8),     DIMENSION(2) :: hydrN,& ! The hydration number of the hydrate component
     &                                          moleF, &! Component mole fraction in the hydrate
     &                                          MolWt, &! Molecular weight of component hydrate
     &                                          GasMF, &! Mass fraction of the gas in the hydrate
     &                                          H2OMF  ! Mass fraction of H2O in the hydrate
! 
         END TYPE HydrComposition
! 
! 
         TYPE(HydrProperties)  :: GH
         TYPE(HydrComposition) :: HCom
! 
! ----------
! ...... Integer parameters
! ----------
! 
         INTEGER :: F_EqOption    ! Flag for selection of hydrate Peq - Teq equilibrium function 
                                  ! = 0: Moridis [2003]; = 1: Kamath (1984)
! 
! ----------
! ...... Logical parameters
! ----------
! 
         LOGICAL :: F_Kinetic    ! Flag indicating kinetic dissociation/formation
! 
! ----------
! ...... Double precision allocatable arrays     
! ----------
! 
         REAL(KIND = 8), ALLOCATABLE, DIMENSION(:) :: SArea_Hyd  ! Area of hydration reaction
         REAL(KIND = 8), ALLOCATABLE, DIMENSION(:) :: Inh_WMasF  ! Mass fraction of inhibitor in injected stream
! 
      END MODULE Hydrate_Param
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      MODULE Hydrate_Properties
!
		 USE MPI_PARAM
		 
         IMPLICIT NONE
! 
         PRIVATE
! 
         PUBLIC :: EqPres_HydrCH4,dPdT_HydrCH4, &
     &             EqTemp_HydrCH4,HeatDiss_HydrCH4,&
     &             HRho_Hydrate,Rate_KineticDiss
!
!
! 
         CONTAINS
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
         REAL(KIND = 8) FUNCTION EqTemp_HydrCH4(P_in,T_dev,F_EqOption,&
     &                                          F_RefOption,IGOOD)
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*         Routine for calculating the equilibrium hydration           *
!*         temperature (K) of CH4 for a given pressure T (Pa)          *
!*                                                                     *
!*                     Version 1.0 - May 9, 2002                       *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
         IMPLICIT NONE
! 
! ----------
! ...... Double precision parameters
! ----------
! 
         REAL(KIND=8), PARAMETER :: &
     &      A0 = +2.71879403137724d+02,  A1 = -8.53544773062347d+00,&
     &      A2 = +1.53755648386698d+01,  A3 = -5.83149716185870d+00,&
     &      A4 = +9.74098947798766d-01,  A5 = -5.87598544451665d-02
! 
         REAL(KIND=8), PARAMETER :: &
     &      B0 = +2.44985665071437d+02,  B1 = +2.84733044556904d+01,&
     &      B2 = +1.95698759053591d+00,  B3 = -7.88664152911403d-01,&
     &      B4 = -2.82320929096826d-01,  B5 = -2.49796978504878d-02
! 
! ...... NOTE: The C coefficients return the temperature in degrees !
! 
         REAL(KIND=8), PARAMETER :: &
     &      C0 = -2.53827246064861d+02,  C1 = +6.79365137110571d+02,&
     &      C2 = -6.10380802145495d+02,  C3 = +1.85231612182883d+02
! 
! ----------
! ...... Double precision variables
! ----------
! 
!         REAL(KIND=8), INTENT(IN) :: P_in,   ! in Pa
!     &                               T_dev   ! in C or K
         REAL(KIND=8) :: P_in, &  ! in Pa
     &                   T_dev   ! in C or K
! 
         REAL(KIND=8) :: P_x,Final_V,Dp,X_0
! 
! -------------
! ......... Integer variables
! -------------
! 
         INTEGER :: F_EqOption,F_RefOption,IGOOD
! 
         INTEGER :: ICALL = 0, Cond,k
! 
         SAVE ICALL
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of EqTemp_HydrCH4 
!
!
         ICALL = ICALL+1
         IF(ICALL == 1 .and. MPI_rank==0) WRITE(11,6000)
!
!***********************************************************************
!*                                                                     *
!*                  Using the Kamath (1984) equation                   *
!*                                                                     *
!***********************************************************************
!
         IF_Kamath: IF(F_EqOption == 1) THEN
! 
            IGOOD = 0
! 
! ......... Initialization
!
            P_x = DLOG(P_in*1.0d-3)
! 
! ......... Above 0 oC (For T > 25 oC, this is an extrapolated estimate) 
!
            IF(P_in >= 2.30663d6) THEN 
!
               EqTemp_HydrCH4 = 8.5338d3/(3.898d1-P_x) - T_dev    ! in K
! 
! ......... Below 0 oC (For T <-25 oC, this is an extrapolated estimate) 
!
            ELSE 
!
               EqTemp_HydrCH4 = 1.88679d3/(1.4717d1-P_x) - T_dev  ! in K
! 
            END IF
!
            RETURN       ! Done! Exit the routine
!
         END IF IF_Kamath
!
!***********************************************************************
!*                                                                     *
!*                 Using the Moridis [2003] equation                   *
!*                                                                     *
!***********************************************************************
! 
! ...... Initialization
!
         P_x = DLOG(P_in*1.0d-6)
! 
         IGOOD = 0
!
         IF_Moridis: IF( P_in > 4.23497d+08) THEN
!
            PRINT 6002, P_in     ! Print a warning message
            IGOOD = 2
            RETURN
!
         ELSE IF( P_in >= 2.87605d+06 .AND. &
     &            P_in <  4.23497d+08) &
     &   THEN
!
            EqTemp_HydrCH4 =  A0+P_x*&
     &                       (A1+P_x*&
     &                       (A2+P_x*&
     &                       (A3+P_x*&
     &                       (A4+P_x*&
     &                        A5))))-T_dev       ! in K
!
         ELSE IF( P_in >= 2.57384d+06 .AND. &
     &            P_in <  2.87605d+06) &
     &   THEN
!
            EqTemp_HydrCH4 =  C0+P_x* &
     &                       (C1+P_x*&
     &                       (C2+P_x*&
     &                        C3))&
     &                      -T_dev+273.15d0      ! in K
!
         ELSE IF(P_in <  2.57384d+06 .AND. &
     &           P_in >= 6.33037d+05) &
     &   THEN
!
            EqTemp_HydrCH4 =  B0+P_x*&
     &                       (B1+P_x*&
     &                       (B2+P_x*&
     &                       (B3+P_x*&
     &                       (B4+P_x*&
     &                        B5))))-T_dev       ! in K
!
         ELSE 
!
            PRINT 6002, P_in     ! Print a warning message
            IGOOD = 2
            RETURN
!
         END IF IF_Moridis
!
!***********************************************************************
!*                                                                     *
!*         For F_RefOption = 1, use the Moridis [2003] estimate        *
!*         a solution conforming to the "EqPres_HydrCH4" values        *
!*                                                                     *
!***********************************************************************
! 
         IF_ConformP: IF(F_RefOption /= 0) THEN
!
            X_0 = EqTemp_HydrCH4
!
            CALL Find_FRoots( X_0, &             ! Initial T estimate
     &                        P_in, &            ! P(T estimate)
     &                        1.0d-12,   &       ! Tolerance for T solution
     &                        1.0d-12,    &      ! Tolerance for P = P(T)
     &                        15,          &     ! Maximum number of iterations
     &                        F_EqOption,   &    ! Equation option
     &                        IGOOD,         &   ! Flag for warning print-out
     &                        Final_V,        &  ! Final T estimate
     &                        Dp,            &   ! Deviation estimate
     &                        Cond,           &  ! Flag for result interepretation
     &                        k              &   ! Iterations to convergence
     &                      )
!
            IF(IGOOD /= 0) THEN
!
               PRINT 6004, Final_V     ! Print a warning message
               IGOOD = 2
               RETURN
!
            END IF
!
            EqTemp_HydrCH4 = Final_V
!
         END IF IF_ConformP
!     
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'EqTemp_HydrCH4    1.0    9 May       2002',6X, &
     &         'Equilibrium CH4 hydration temperature for a given ',&
     &         'pressure')
! 
 6002 FORMAT(/,' ===> WARNING :',&
     &      ' Pressure = ',1pE12.6,' is out of range',&
     &      ' (-124.4 C to 45.2 C) in the Moridis [2003] equation',&
     &      ' in "EqTemp_HydrCH4" - Teq is extrapolated')
!
 6004 FORMAT(' : : : : : Function "EqTemp_HydrCH4": The temperature ',&
     &       1pe15.8,' oC is out of range')
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of EqTemp_HydrCH4
!
!
            RETURN
!
         END FUNCTION EqTemp_HydrCH4
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
         REAL(KIND = 8) FUNCTION EqPres_HydrCH4(T_inK,T_dev,&
     &                                          F_EqOption,IGOOD)
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*   Routine for calculating the equilibrium hydration pressure (Pa)   *
!*               of CH4 for a given temperature T (in K)               *
!*                                                                     *
!*                     Version 1.0 - March 2, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
         IMPLICIT NONE
! 
! ----------
! ...... Double precision parameters
! ----------
! 
         REAL(KIND=8), PARAMETER :: &
     &      A0 = -1.94138504464560d05,  A1 = +3.31018213397926d03,&
     &      A2 = -2.25540264493806d01,  A3 = +7.67559117787059d-2,&
     &      A4 = -1.30465829788791d-4,  A5 = +8.86065316687571d-8
! 
         REAL(KIND=8), PARAMETER :: &
     &      B0 = -4.38921173434628d01,  B1 = +7.76302133739303d-1,&
     &      B2 = -7.27291427030502d-3,  B3 = +3.85413985900724d-5,&
     &      B4 = -1.03669656828834d-7,  B5 = +1.09882180475307d-10
! 
         REAL(KIND=8), PARAMETER :: &
     &      C0 = +9.652117566301735d-1, C1 = +5.563942679876470d-2,&
     &      C2 = +2.934835672207024d-2, C3 = +7.696735279663661d-3,&
     &      C4 = -6.147609081030884d-3, C5 = -1.931115655395969d-3,&
     &      C6 = +6.350841470341581d-4, C7 = +1.682282887657391d-4
! 
! ----------
! ...... Double precision variables
! ----------
! 
         REAL(KIND=8) :: T_inK, &  ! in K
     &                   T_dev    ! in K or !
! 
         REAL(KIND=8) :: T_x,T_c
! 
! -------------
! ......... Integer variables
! -------------
! 
         INTEGER :: F_EqOption,IGOOD
! 
         INTEGER :: ICALL = 0
! 
         SAVE ICALL
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of EqPres_HydrCH4 
!
!
         ICALL = ICALL+1
         IF(ICALL == 1 .and. MPI_rank==0) WRITE(11,6000)
! 
! ...... Initialization
!
         T_x = T_inK+T_dev
         IGOOD = 0
!
!***********************************************************************
!*                                                                     *
!*                  Using the Kamath (1984) equation                   *
!*                                                                     *
!***********************************************************************
!
         IF_Kamath: IF(F_EqOption == 1) THEN
! 
! ......... Above 0 oC (For T > 25 oC, this is an extrapolated estimate) 
!!
            IF(T_x >= 273.1d0) THEN 
!
               EqPres_HydrCH4 = 1.15*DEXP(4.93185d1-9.4590d3/T_x) !By RPS
               !EqPres_HydrCH4 = 1.0d3*DEXP(3.8980d1-8.53380d3/T_x)		 !original
! 
! ......... Below 0 oC (For T <-25 oC, this is an extrapolated estimate) 
!
            ELSE 
!
               EqPres_HydrCH4 = 1.0d3*DEXP(1.4717d1-1.88679d3/T_x)
!
            END IF
!
            RETURN       ! Done! Exit the routine
!
         END IF IF_Kamath
!
!***********************************************************************
!*                                                                     *
!*                 Using the Moridis [2003] equation                   *
!*                                                                     *
!***********************************************************************
!
         IF_Moridis: IF(T_x > 3.204d2) THEN
!
            EqPres_HydrCH4 = 1.0d10
!
         ELSE IF(T_x > 2.737d2 .AND. T_x <= 3.204d2) THEN
!
            EqPres_HydrCH4 = 1.0d6*DEXP( A0+T_x*&  !originally multiplied by 1.0d6 RPS
     &                                  (A1+T_x*&
     &                                  (A2+T_x*&
     &                                  (A3+T_x*&
     &                                  (A4+T_x*&
     &                                   A5))))&
     &                                 )
!
         ELSE IF(T_x > 2.722d2 .AND. T_x <= 2.737d2) THEN
!
            T_c = T_x-273.15d0
!
            EqPres_HydrCH4 = 1.0d6*DEXP( C0+T_c*&
     &                                  (C1+T_c*&
     &                                  (C2+T_c*&
     &                                  (C3+T_c*&
     &                                  (C4+T_c*&
     &                                  (C5+T_c*&
     &                                  (C6+T_c*&
     &                                   C7))))))&
     &                                 )
!
         ELSE IF(T_x <= 2.722d2 .AND. T_x >= 1.488d2) THEN
!
            EqPres_HydrCH4 = 1.0d6*DEXP( B0+T_x*&
     &                                  (B1+T_x*&
     &                                  (B2+T_x*&
     &                                  (B3+T_x*&
     &                                  (B4+T_x*&
     &                                   B5))))&
     &                                 )
!
         ELSE 
!
            EqPres_HydrCH4 = 1.0d6*DEXP( B0+T_x*&
     &                                  (B1+T_x*&
     &                                  (B2+T_x*&
     &                                  (B3+T_x*&
     &                                  (B4+T_x*&
     &                                   B5))))&
     &                                 )
!
            PRINT 6002, T_x     ! Print a warning message
            IGOOD = 2
            RETURN
!
         END IF IF_Moridis
!     
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'EqPres_HydrCH4    1.0    2 March     2004',6X,&
     &         'Equilibrium CH4 hydration pressure for a given ',&
     &         'temperature')
! 
 6002 FORMAT(/,' ===> WARNING :',&
     &      ' Temperature = ',1pE12.6,' is out of range',&
     &      ' (-124.4 C to 45.2 C) in the Moridis [2003] equation',&
     &      ' in "EqPres_HydrCH4" - Peq is extrapolated')
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of EqPres_HydrCH4
!
!
            RETURN
!
         END FUNCTION EqPres_HydrCH4
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
         REAL(KIND = 8) FUNCTION dPdT_HydrCH4(T_inK,T_dev,&
     &                                        F_EqOption,IGOOD)
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*   Routine for calculating the equilibrium hydration pressure (Pa)   *
!*               of CH4 for a given temperature T (in K)               *
!*                                                                     *
!*                     Version 1.0 - May 9, 2002                       *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
         IMPLICIT NONE
! 
! ----------
! ...... Double precision parameters
! ----------
! 
         REAL(KIND=8), PARAMETER :: &
     &      A0 = -1.94138504464560d05,  A1 = +3.31018213397926d03,&
     &      A2 = -2.25540264493806d01,  A3 = +7.67559117787059d-2,&
     &      A4 = -1.30465829788791d-4,  A5 = +8.86065316687571d-8
! 
         REAL(KIND=8), PARAMETER :: &
     &      B0 = -4.38921173434628d01,  B1 = +7.76302133739303d-1,&
     &      B2 = -7.27291427030502d-3,  B3 = +3.85413985900724d-5,&
     &      B4 = -1.03669656828834d-7,  B5 = +1.09882180475307d-10
! 
         REAL(KIND=8), PARAMETER :: &
     &      C0 = +0.96521175663017d0,   C1 = +0.05563942679876d0,&
     &      C2 = +0.02934835672207d0,   C3 = +0.00769673527966d0,&
     &      C4 = -0.00614760908103d0,   C5 = -0.00193111565540d0,&
     &      C6 = +0.00063508414703d0,   C7 = +0.00016822828877d0
! 
! ----------
! ...... Double precision variables
! ----------
! 
         REAL(KIND=8) :: T_inK, &  ! in K
     &                   T_dev    ! in K or !
! 
         REAL(KIND=8) :: T_x,T_c
! 
! -------------
! ......... Integer variables
! -------------
! 
         INTEGER :: F_EqOption,IGOOD
! 
         INTEGER :: ICALL = 0
! 
         SAVE ICALL
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of dPdT_HydrCH4 
!
!
         ICALL = ICALL+1
         IF(ICALL == 1 .and. MPI_rank==0) WRITE(11,6000)
! 
! ...... Initialization
!
         T_x = T_inK+T_dev
!
!***********************************************************************
!*                                                                     *
!*                  Using the Kamath (1984) equation                   *
!*                                                                     *
!***********************************************************************
!
         IF_Kamath: IF(F_EqOption == 1) THEN
! 
            IGOOD = 0
! 
! ......... Above 0 !
!
            IF(T_x > 273.1d0) THEN 
!
               dPdT_HydrCH4 = 8.5338d3/(T_x*T_x)
! 
! ......... Below 0 !
!
            ELSE 
!
               dPdT_HydrCH4 = 1.88679d3/(T_x*T_x)
!
            END IF
!
            RETURN       ! Done! Exit the routine
!
         END IF IF_Kamath
!
!***********************************************************************
!*                                                                     *
!*                 Using the Moridis [2003] equation                   *
!*                                                                     *
!***********************************************************************
!
         IGOOD = 0
! 
         IF_Moridis: IF(T_x > 320.4d0) THEN
!
            dPdT_HydrCH4 = 0.0d0
!
         ELSE IF(T_x > 2.742d2 .AND. T_x <= 3.204d2) THEN
!
            dPdT_HydrCH4 =        A1+T_x*&
     &                     (2.0d0*A2+T_x*&
     &                     (3.0d0*A3+T_x*&
     &                     (4.0d0*A4+T_x*&
     &                      5.0d0*A5)))
!
         ELSE IF(T_x > 2.722d2 .AND. T_x <= 2.742d2) THEN
!
            T_c = T_x-273.15d0
!
            dPdT_HydrCH4 =        C1+T_c*&
     &                     (2.0d0*C2+T_c*&
     &                     (3.0d0*C3+T_c*&
     &                     (4.0d0*C4+T_c*&
     &                     (5.0d0*C5+T_c*&
     &                     (6.0d0*C6+T_c*&
     &                      7.0d0*C7)))))
!
         ELSE IF(T_x <= 2.722d2 .AND. T_x >= 1.488d2) THEN
!
            dPdT_HydrCH4 =        B1+T_x*&
     &                     (2.0d0*B2+T_x*&
     &                     (3.0d0*B3+T_x*&
     &                     (4.0d0*B4+T_x*&
     &                      5.0d0*B5)))
!
         ELSE 
!
            WRITE(6,6002) T_x     ! Print a warning message
            IGOOD = 2
            RETURN
!
         END IF IF_Moridis
!     
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'dPdT_HydrCH4      1.0    9 May       2002',6X,&
     &         'Derivative of the CH4 hydration pressure vs T for a ',&
     &         'given temperature')
! 
 6002 FORMAT(/,' ===> WARNING :',&
     &      ' Temperature = ',1pE12.6,' is out of range',&
     &      ' (-124.4 C to 45.2 C) in the Moridis',&
     &      ' [2003] equation in "dPdT_HydrCH4" - Peq is extrapolated')
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of dPdT_HydrCH4
!
!
            RETURN
!
         END FUNCTION dPdT_HydrCH4
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
         REAL(KIND = 8) FUNCTION HeatDiss_HydrCH4(T_inK,iFlag,IGOOD)
!
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*          Routine for calculating the heat of dissociation           *
!*            of the CH4 hydrate at a given pressure T (Pa)            *
!*                                                                     *
!*                     Version 1.0 - May 3, 2004                       *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
         IMPLICIT NONE
! 
! ----------
! ...... Double precision parameters
! ----------
! 
         REAL(KIND=8), PARAMETER ::     &
     &      A0 = +6.80813945837983d+04,  A1 = -6.58311333693302d+03,    &
     &      A2 = +1.68245357892968d+03,  A3 = -2.09255366641564d+02,    &
     &      A4 = +1.52539098964571d+01,  A5 = -6.68126421586659d-01,    &
     &      A6 = +1.78939172983573d-02,  A7 = -2.71557051904435d-04,    &
     &      A8 = +1.78024519771415d-06
! 
         REAL(KIND=8), PARAMETER ::     &
     &      B0 = -2.27898617777320d+05,  B1 = +7.22257188708381d+03,   &
     &      B2 = -8.84642299066819d+01,  B3 = +5.41148599042605d-01,    &
     &      B4 = -1.62173900583980d-03,  B5 = +1.89174899797338d-06
!
          REAL(KIND=8), PARAMETER :: &    
     &      Trans1 = 2.03975D2        ! Transition T from Moridis eq. TO constant value 
!                                     ! (at local max between 190 and 230 on Moridis eq.)
          REAL(KIND=8), PARAMETER ::  &   
     &      Trans2 = 273.2D0          ! Transition temp(K) at 273.2 between constant values 
!                                     ! (CH4-hydrate quadruple point)
          REAL(KIND=8), PARAMETER :: &    
     &      Trans3 = 2.77175D2        ! Transition temp(K) to Moridis eqn FROM constant value   
!                                     ! (at local min between 275 and 280 on Moridis eqn.)
          REAL(KIND=8), PARAMETER :: &    
     &      a_1 = 40.0D0,             &    ! Steepness parameters
     &      a_2 = 5.0D0,               &   ! Steepness parameter near the hydrate quadruple point   
     &      a_3 = 40.0D0,               &  ! Steepness parameters
     &      F2 = 1.783850902387808D4,   &  ! F2: Value of Moridis eqn. at Trans1  
     &      F3 = 5.856543530641794D4      ! F3: Value of Moridis eqn. at Trans3
!     
! ----------
! ...... Double precision variables
! ----------
! 
         REAL(KIND=8), INTENT(IN) :: T_inK
! 
         REAL(KIND=8) :: T_x,T_c
         REAL(KIND=8) :: F1,F4
         REAL(KIND=8) :: Psi_1, &       ! Factor to suppress values for T<Trans1
     &                   Psi_2,  &      ! Factor to suppress values for T<Trans2
     &                   Psi_3         ! Factor to suppress values for T<Trans3
! 
! -------------
! ......... Integer variables
! -------------
! 
         INTEGER, INTENT(INOUT) :: iFlag,IGOOD
! 
! ----------
! ...... Logical variables
! ----------
! 
         LOGICAL :: First_call = .TRUE.
! 
! ----------
! ...... Saving variables
! ----------
! 
         SAVE First_call
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of HeatDiss_HydrCH4 
!
!
         IF(First_call) THEN
            First_call = .FALSE.
            WRITE(11,6000)
         END IF
! 
! ...... Initialization
!
         T_x = T_inK
         T_c = T_x-273.15d0
!
         IGOOD = 0
!
!***********************************************************************
!*                                                                     *
!*                  Using the Kamath (1984) equation                   *
!*                                                                     *
!***********************************************************************
!
         IF_Kamath: IF(iFlag /= 0) THEN
! 
! ......... Above 0 oC : Note that the Kamath equation applies up to 25 oC !
!
            IF(T_x >= 273.1d0) THEN 
!
               HeatDiss_HydrCH4  &             ! Heat of CH4 hydrate dissociation DH in J/kg
     &         =  3.372995d1      &            ! Conversion factor (J g /cal/kg of CH4 hydrate)
     &          *(1.3521d+04-4.02d+00*T_x)    ! Before, DH in cal/gmol
! 
! ......... Below 0 oC : Note that the Kamath equation applies down to -25 oC !
!
            ELSE 
!
               HeatDiss_HydrCH4    &           ! Heat of CH4 hydrate dissociation DH in J/kg
     &         =  3.372995d1        &          ! Conversion factor (J g /cal/kg of CH4 hydrate)
     &          *(6.534d+03-11.97d+00*T_x)    ! Before, DH in cal/gmol
!
            END IF
!
            RETURN       ! Done! Exit the routine
!
         END IF IF_Kamath
!
!***********************************************************************
!*                                                                     *
!*                 Using the Moridis [2003] equation                   *
!*                                                                     *
!*     (5/27/04: New function implemented for HeatDiss_HydrCH4)        *
!*                                                                     *
!***********************************************************************
!
!
         IF_Moridis: IF(T_x >= 148.8d0) THEN
!
            Psi_1 = 5.0d-1*( 1.0d0 &
     &      + Error_Functions(a_1*( T_x - Trans1 + 2.0d0/a_1),0) )
            Psi_2 = 5.0d-1*( 1.0d0 &
     &      + Error_Functions(a_2*( T_x - Trans2 + 2.0d0/a_2),0) )
            Psi_3 = 5.0d-1*( 1.0d0 &
     &      + Error_Functions(a_3*( T_x - Trans3 + 2.0d0/a_3),0) )
!
            F1 = ( B0+T_x*    &         ! F1: Moridis eqn for T<TRANS1
     &            (B1+T_x*   &
     &            (B2+T_x*   &
     &            (B3+T_x*   &
     &            (B4+T_x*   &
     &             B5)))))
!
            F4 = ( A0+T_c*  &           ! F4: Moridis eqn for T>TRANS3
     &            (A1+T_c*   &
     &            (A2+T_c*   &
     &            (A3+T_c*   &
     &            (A4+T_c*   &
     &            (A5+T_c*   &
     &            (A6+T_c*   &
     &            (A7+T_c*   &
     &             A8))))))))
!
            HeatDiss_HydrCH4  &       ! Heat of CH4 hydrate dissociation DH in J/kg
     &      = 8.0558755d0      &      ! Conversion factor (gmol/kg of CH4 hydrate)
     &                   *( F1*(1.0d0-Psi_1)                    &
     &                     +F2*(Psi_1)*(1.0d0-Psi_2)   &
     &                     +F3*(Psi_2)*(1.0d0-Psi_3)    &
     &                     +F4*(Psi_3)                   &               
     &                    )
!         
         ELSE 
!
            PRINT 6002, T_x     ! Print a warning message
            IGOOD = 2
            RETURN
!
         END IF IF_Moridis
!     
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'HeatDiss_HydrCH4  1.0    3 May       2004',6X,  & 
     &         'Heat of dissociation of the CH4 hydrate pressure ',   &
     &         'for a given temperature')
! 
 6001 FORMAT(' ===> WARNING :',   &
     &       ' Temperature = ',1pE12.6,' is out of range',   &
     &       ' (-25 C to 25 C) in the Kamath [1984] equation',&  
     &       ' in "HeatDiss_HydrCH4" - DH is extrapolated')
 6002 FORMAT(' ===> WARNING :',   &
     &       ' Temperature = ',1pE12.6,' is out of range',   &
     &       ' (-124.4 C to 45.2 C) in the Moridis [2003] equation',  &
     &       ' in "HeatDiss_HydrCH4" - DH is extrapolated')
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of HeatDiss_HydrCH4
!
!
         RETURN
!
         END FUNCTION HeatDiss_HydrCH4
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE HRho_Hydrate(T_c,P_Pa,D_Hydr,H_Hydr)
! 
! ...... Modules to be used 
! 
         USE Hydrate_Param
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*      Routine for calculating the density and specific enthalpy      *
!*                            of hydrates                              *
!*                                                                     *
!*                      Version 1.0 - May 6, 2004                      *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision parameters for ice compressibility 
! ... From Marion and Jakubowski: Cold Regions Science and Technology, 38, 211-218, (2004) 
! -------
! 
      REAL(KIND=8), PARAMETER :: B0 = +1.398427d-08,&
     &                           B1 = -1.121305d-10,&
     &                           B2 =  2.258122d-13   
! 
! -------
! ... Double precision parameters for thermal expansivity of ice   
! ... From data in Tanaka: Journal of Molecular Structure, V461-462, 561-567, (1999) 
! -------
! 
      REAL(KIND=8), PARAMETER :: C0 = -1.72176258645362d-05,   & 
     &                           C1 =  3.56011844951369d-07,    &
     &                           C2 = -4.65623949357508d-10    
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND=8), INTENT(IN)  :: T_c,P_Pa
      REAL(KIND=8), INTENT(OUT) :: D_Hydr,H_Hydr
!
      REAL(KIND=8) :: SUM,T_k,T_k2,T_k3,F_dP,F_dT
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: i, ICALL = 0
! 
      SAVE ICALL
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of HRho_Hydrate
!
!
      ICALL = ICALL+1
      IF(ICALL == 1 .and. MPI_RANk==0) WRITE(11,6000)
! 
!***********************************************************************
!*                                                                     *
!*                      Computation of density                         *
!*                                                                     *
!***********************************************************************
! 
      IF(GH%N_Rho == 1) THEN
! 
! ----------
! ...... Temperature-invariant  
! ----------
! 
         D_Hydr = GH%p_Rho(1)
      ELSE 
! 
! ----------
! ...... Temperature-dependent   
! ----------
! 
         SUM = GH%p_Rho(GH%N_Rho)
! 
         DO i=GH%N_Rho-1,1,-1
            SUM = SUM*T_c+GH%p_Rho(i)
         END DO
! 
         D_Hydr = SUM
! 
      END IF
!
! ------
! ... Computing intermediate variables ...
! ------
! 
      T_k  = T_c+273.15d0
      T_k2 = T_k*T_k
      T_k3 = T_k*T_k2
!
! ------
! ... The compressibility contribution to the volume change is ...
! ------
! 
      F_dP =  ( B0 + T_k*( B1 + T_k*B2) )*(P_Pa - 2.56d6)  
!
! ------
! ... The expansivity contribution to the volume change is ...
! ------
! 
      F_dT =   C0*(T_k  - 273.16d0)  &             
     &       + C1*(T_k2 - 7.4616385d+04)/2.0d0        &
     &       + C2*(T_k3 - 2.0382211d+07)/3.0d0       
!
! ------
! ... The hydrate density is ...
! ------
! 
      D_Hydr = D_Hydr/(1.0d0 - F_dP + F_dT)
! 
!***********************************************************************
!*                                                                     *
!*                      Computation of enthalpy                        *
!*                                                                     *
!***********************************************************************
! 
      IF(GH%N_SpH == 1) THEN
! 
! ----------
! ...... Temperature-invariant  
! ----------
! 
         H_Hydr = GH%p_SpH(1)*T_c
      ELSE 
! 
! ----------
! ...... Temperature-dependent   
! ----------
! 
         SUM = T_c*GH%p_SpH(GH%N_SpH)/DBLE(GH%N_SpH)
! 
         DO i=GH%N_SpH-1,1,-1
            SUM = (SUM+GH%p_SpH(i)/DBLE(i))*T_c
         END DO
! 
         H_Hydr = SUM
! 
      END IF
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'HRho_Hydrate      1.0    6 May       2004',6X,&
     &         'Density and internal energy of solid hydrate')
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of HRho_Hydrate
!
!
      RETURN
!
      END SUBROUTINE HRho_Hydrate
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      REAL(KIND = 8) FUNCTION Rate_KineticDiss(n,PP,P_ac,T_c,&
     &                                         S_Molal,G_MoleF,&
     &                                         Area_Hyd)
! 
! ... Modules to be used 
! 
         USE Hydrate_Param
! 
         USE RefRGas_Param, ONLY: NumCom
! 
         USE RealGas_Properties
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*     Computation of the rate of kinetic dissociation of hydrate      *     
!*                                                                     *
!*                    Version 1.0 - April 18, 2003                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8), INTENT(IN), DIMENSION(NumCom) :: G_MoleF
! 
      REAL(KIND = 8), DIMENSION(2) :: FugC
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN) :: PP,T_c,S_Molal,Area_Hyd,P_ac
! 
      REAL(KIND = 8) :: T_k,ZLiq,ZGas,rho_G,rho_L
      REAL(KIND = 8) :: FugC_t,FugC_eq
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: n
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: ixx
! 
      SAVE ICALL
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of Rate_KineticDiss
! 
! 
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .and. MPI_rank==0) WRITE(11,6000)                                      
! 
!***********************************************************************
!*                                                                     *
!*      Computation of the kinetic rate of hydrate dissociation        *
!*                                                                     *
!***********************************************************************
! 
      T_k = T_c+273.15d0  ! Temperature in degrees Kelvin
! 
! -------
! ... For high-pressure systems
! -------
! 
      IF(PP >= 1.01d5) THEN
! 
! ...... Compute compressibilities and basic parameters at PP,T_k
! 
         CALL Zcomp(PP,T_k,S_Molal,G_MoleF,ZLiq,ZGas,rho_G,rho_L,ixx)
! 
! ...... Compute fugacity coefficient at P,T (Gas phase)
! 
         CALL Gas_Fugacity(PP,T_k,ZGas,2,FugC) 
         FugC_t = FugC(1)
! 
! -------
! ... For low-pressure systems
! -------
! 
      ELSE
         FugC_t = 1.0d0
      END IF
! 
! -------
! ... Compute compressibilities and basic parameters at P_eq,T_k
! -------
! 
      CALL Zcomp(P_ac,T_k,S_Molal,G_MoleF,ZLiq,ZGas,rho_G,rho_L,ixx)
! 
! -------
! ... Compute equilibrium fugacity coefficient at P_eq,T_c/T_k 
! -------
! 
      CALL Gas_Fugacity(P_ac,T_k,ZGas,2,FugC) 
      FugC_eq = FugC(1)
! 

! Hydrate reaction area is calculated based 
! 
! -------
! ... Compute the kinetic rate of dissociation (kg/s/m^3)
! -------
! 
! RPS NOte- M_ch4 = 16.04 gm/mol and M_h2o = 18.015 gm/mol
      Rate_KineticDiss =&
     &  - InRate_k0*124.13e-3     &              ! Intrinsic diss. rate const. [now in kg/(m^2.Pa.s)] !RPS M_hydrate = 127.13e-3 Kg/mol or 127.13 gm/mol
     &   *EXP(-Activ_En/T_k/8.314d0)   &! Effect of activation energy
     &   *Area_Hyd                     &! The hydrate surface area per unit volume (m^2/s)
     &   *(P_ac-PP)
!     &   *(P_ac*FugC_eq-PP*FugC_t)     &! Fugacity coefficient differential
!     &   *G_MoleF(1)                   ! Mole fraction of CH4 in the gas phase (Pa)     
    
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'Rate_KineticDiss  1.0   19 March     2003',6X,&
     &         'Computing the rate of kinetic dissociation ',&
     &         'of hydrates') 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of Rate_KineticDiss
! 
! 
      RETURN
!
      END FUNCTION Rate_KineticDiss
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE Find_FRoots(X_0,F_Value,Delta,Epsilon,MaxIte,&
     &                       F_EqOption,IGOOD,X_3,Dp,Cond,n)
! 
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*          Computation of the roots of a non-linear function          *     
!*         using Newton-Raphson with Steffensen's acceleration         *     
!*                                                                     *
!*                    Version 1.0 - April 22, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision parameterss
! -------
! 
      REAL(KIND = 8), PARAMETER :: Small=1.0d-20
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN)    :: F_Value,Delta,Epsilon
      REAL(KIND = 8), INTENT(OUT)   :: X_3,Dp
      REAL(KIND = 8), INTENT(INOUT) :: X_0
! 
      REAL(KIND = 8) :: D1,D2,X_1,X_2,RelError,Y3
      REAL(KIND = 8) :: FP0,DF0,FP1,DF1
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN)    :: MaxIte
      INTEGER, INTENT(OUT)   :: Cond,n
! 
      INTEGER :: ICALL = 0, F_EqOption,IGOOD
! 
      SAVE ICALL
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of Find_FRoots
! 
! 
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .and. MPI_rank==0) WRITE(11,6000)                                      
! 
! -------
! ... Initializations
! -------
! 
      Cond = 0
! 
      IGOOD = 0
! 
      X_3 = X_0
      X_2 = X_0+1.0d0
      X_1 = X_0+2.0d0
! 
! **********************************************************************
! *                                                                    *
! *                         MAIN ITERATION LOOP                        *
! *                                                                    *
! **********************************************************************
!
      DO_NRSLoop: DO n=1,MaxIte
!
         X_0 = X_3
!
! ----------
! ...... Compute F(X_0), dF/dx(X_0), and the equation to solve F(X_0)-V = 0.0d0
! ----------
!
         FP0 = EqPres_HydrCH4(X_0,0.0d0,F_EqOption,IGOOD)
         IF(IGOOD /= 0) RETURN
!
         DF0 = FP0*dPdT_HydrCH4(X_0,0.0d0,F_EqOption,IGOOD) ! Exponential function
         IF(IGOOD /= 0) RETURN
!
         FP0 = FP0-F_Value
!
! ----------
! ...... Obtain a first approximation of the solution X_1
! ----------
!
         IF_1stPass: IF (DF0 /= 0.0d0) THEN
            X_1 = X_0 - FP0/DF0
         ELSE
            Cond = 1
            Dp   = X_3-X_2
            X_3  = X_0
         END IF IF_1stPass
!
! ----------
! ...... Compute F(X_1), dF/dx(X_1), and the equation to solve F(X_1)-V = 0.0d0
! ----------
!
         FP1 = EqPres_HydrCH4(X_1,0.0d0,F_EqOption,IGOOD)
         IF(IGOOD /= 0) RETURN
!
         DF1 = FP1*dPdT_HydrCH4(X_1,0.0d0,F_EqOption,IGOOD)
         IF(IGOOD /= 0) RETURN
!
         FP1 = FP1-F_Value
!
! ----------
! ...... Improve the approximation of the solution X_2
! ----------
!
         IF_2ndPass: IF (DF1 == 0.0d0) THEN
            Cond = 1
            Dp   = X_1-X_0
            X_3  = X_1
         ELSE
!
            X_2 =  X_1 - FP1/DF1
            D1  = (X_1-X_0)*(X_1-X_0)
            D2  =  X_2-2.0d0*X_1+X_0
!
            IF (D2 == 0.0d0) THEN
               Cond = 1
               Dp   = X_2-X_1
               X_3  = X_2
            ELSE
               X_3 = X_0-D1/D2
               Dp  = X_3-X_2
            ENDIF
!
!           Y3       = EqPres_HydrCH4(P3,0.0d0,0,IGOOD)-F_Value
            RelError = ABS(Dp)/(ABS(X_3)+Small)
!
! -------------
! ......... Ensure at least one iteration
! -------------
!
            IF_AtLeast1: IF(n > 1) THEN
!
               IF (RelError  < Delta)   Cond = 2
!              IF (ABS(Y3) < Epsilon) Cond = 3 
!              IF ((RelError < Delta) .AND. (ABS(Y3) < Epsilon)) Cond = 4
!
               SELECT CASE(Cond)
!
! ............ Division by zero - the starting value is used as the solution
!
               CASE(1)
                  PRINT 6001                                  ! Print warning  
                  X_3 = EqPres_HydrCH4(F_Value,0.0d0,F_EqOption,IGOOD)  
                  EXIT DO_NRSLoop
!
! ............ Convergence within the desired tolerance
!
               CASE(2)
                  EXIT DO_NRSLoop
!
! ............... Continue the iteration
!
               CASE DEFAULT
                  CYCLE DO_NRSLoop
               END SELECT
!
            END IF IF_AtLeast1
!
!
!
         END IF IF_2ndPass
! <<<
! <<< End of the iteration loop
! <<<
      END DO DO_NRSLoop
!
! -------
! ... Check if convergence has been achieved within MaxIte
! -------
!
      IF(Cond == 0) PRINT 6002
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'Find_FRoots       1.0   22 April     2004',6X,&
     &         'Determining the real roots of a function: Newton-',&
     &         'Raphson iteration ',&
     &       /,T48,'with Steffensen acceleration') 
!
 6001 FORMAT(/,' ===> WARNING :',&
     &      ' Unable to obtain roots in "Find_FRoots"; the initial',&
     &      ' estimate X_0 is used')
 6002 FORMAT(/,' ===> WARNING :',&
     &      ' No convergence within "MaxIte" in "Find_FRoots"; the',&
     &      ' initial estimate X_0 is used')
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of Find_FRoots
! 
! 
      RETURN
!
      END SUBROUTINE Find_FRoots
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      REAL(KIND = 8) FUNCTION Error_Functions(ARG,JINT)
!     
!
!***********************************************************************
!***********************************************************************
!
! This packet evaluates  erf(x),  erfc(x),  and  exp(x*x)*erfc(x)
!   for a real argument  x.  It contains the FUNCTION type
!   subprogram ERFUNC.  The calling
!   statements for the primary entries are:
!
!                   Y=ERFUNC(X,0),
!
!                   Y=ERFUNC(X,1),
!   and
!                   Y=ERFUNC(X,2).
!
!   The routine  ERFUNC  is intended for internal packet use only,
!   all computations within the packet being concentrated in this
!   routine.  The parameter usage is as follows
!
!      Function                     Parameters for ERFUNC
!       call              ARG                  ERFUNC          JINT
!
!     ERFUNC(X,0)   ANY REAL ARGUMENT         ERF(ARG)          0
!     ERFUNC(X,1)   ABS(ARG) .LT. XBIG        ERFC(ARG)         1
!     ERFUNC(X,2)   XNEG .LT. ARG .LT. XMAX   ERFCX(ARG)        2
!
!   The main computation evaluates near-minimax approximations
!   from "Rational Chebyshev approximations for the error function"
!   by W. J. Cody, Math. Comp., 1969, PP. 631-638.  This
!   transportable program uses rational functions that theoretically
!   approximate  erf(x)  and  erfc(x)  to at least 18 significant
!   decimal digits.  The accuracy achieved depends on the arithmeti!
!   system, the compiler, the intrinsic functions, and proper
!   selection of the machine-dependent constants.
!
!*******************************************************************
!*******************************************************************
!
! Explanation of machine-dependent constants
!
!   XMIN   = the smallest positive floating-point number.
!   XINF   = the largest positive finite floating-point number.
!   XNEG   = the largest negative argument acceptable to ERFCX;
!            the negative of the solution to the equation
!            2*exp(x*x) = XINF.
!   XSMALL = argument below which erf(x) may be represented by
!            2*x/sqrt(pi)  and above which  x*x  will not underflow.
!            A conservative value is the largest machine number X
!            such that   1.0 + X = 1.0   to machine precision.
!   XBIG   = largest argument acceptable to ERFC;  solution to
!            the equation:  W(x) * (1-0.5/x**2) = XMIN,  where
!            W(x) = exp(-x*x)/[x*sqrt(pi)].
!   XHUGE  = argument above which  1.0 - 1/(2*x*x) = 1.0  to
!            machine precision.  A conservative value is
!            1/[2*sqrt(XSMALL)]
!   XMAX   = largest acceptable argument to ERFCX; the minimum
!            of XINF and 1/[sqrt(pi)*XMIN].
!
!   Approximate values for some important machines are:
!
!                          XMIN       XINF        XNEG     XSMALL
!
!  CDC 7600      (S.P.)  3.13E-294   1.26E+322   -27.220  7.11E-15
!  CRAY-1        (S.P.)  4.58E-2467  5.45E+2465  -75.345  7.11E-15
!  IEEE (IBM/XT,
!    SUN, etc.)  (S.P.)  1.18E-38    3.40E+38     -9.382  5.96E-8
!  IEEE (IBM/XT,
!    SUN, etc.)  (D.P.)  2.23D-308   1.79D+308   -26.628  1.11D-16
!  IBM 195       (D.P.)  5.40D-79    7.23E+75    -13.190  1.39D-17
!  UNIVAC 1108   (D.P.)  2.78D-309   8.98D+307   -26.615  1.73D-18
!  VAX D-Format  (D.P.)  2.94D-39    1.70D+38     -9.345  1.39D-17
!  VAX G-Format  (D.P.)  5.56D-309   8.98D+307   -26.615  1.11D-16
!
!
!                          XBIG       XHUGE       XMAX
!
!  CDC 7600      (S.P.)  25.922      8.39E+6     1.80X+293
!  CRAY-1        (S.P.)  75.326      8.39E+6     5.45E+2465
!  IEEE (IBM/XT,
!    SUN, etc.)  (S.P.)   9.194      2.90E+3     4.79E+37
!  IEEE (IBM/XT,
!    SUN, etc.)  (D.P.)  26.543      6.71D+7     2.53D+307
!  IBM 195       (D.P.)  13.306      1.90D+8     7.23E+75
!  UNIVAC 1108   (D.P.)  26.582      5.37D+8     8.98D+307
!  VAX D-Format  (D.P.)   9.269      1.90D+8     1.70D+38
!  VAX G-Format  (D.P.)  26.569      6.71D+7     8.98D+307
!
!*******************************************************************
!*******************************************************************
!
! Error returns
!
!  The program returns  ERFC = 0      for  ARG .GE. XBIG;
!
!                       ERFCX = XINF  for  ARG .LT. XNEG;
!      and
!                       ERFCX = 0     for  ARG .GE. XMAX.
!
!
! Intrinsic functions required are:
!
!     DABS, AINT, DEXP
!
!
!  Author: W. J. Cody
!          Mathematics and Computer Science Division
!          Argonne National Laboratory
!          Argonne, IL 60439
!
!  Latest modification: May 16, 2004 (by G. J. Moridis, LBNL)
!
!
!***********************************************************************
!***********************************************************************
! 
! 
! -------
! ... Double precision parameter arrays
! -------
! 
! ... Coefficients for approximation to ERF in first interval
! 
      REAL(KIND = 8), PARAMETER, DIMENSION(5) :: &
     &      A = (/ 3.16112374387056560D00, 1.13864154151050156D02,  &
     &             3.77485237685302021D02, 3.20937758913846947D03,&
     &             1.85777706184603153D-1 /)
! 
      REAL(KIND = 8), PARAMETER, DIMENSION(4) :: &
     &      B = (/ 2.36012909523441209D01, 2.44024637934444173D02,&
     &             1.28261652607737228D03, 2.84423683343917062D03 /)
! 
! ... Coefficients for approximation to ERFC in second interval
! 
      REAL(KIND = 8), PARAMETER, DIMENSION(9) :: &
     &      C = (/ 5.64188496988670089D-1, 8.88314979438837594D0,&
     &             6.61191906371416295D01, 2.98635138197400131D02,&
     &             8.81952221241769090D02, 1.71204761263407058D03,&
     &             2.05107837782607147D03, 1.23033935479799725D03,&
     &             2.15311535474403846D-8 /)
! 
      REAL(KIND = 8), PARAMETER, DIMENSION(8) :: &
     &      D = (/ 1.57449261107098347D01, 1.17693950891312499D02,&
     &             5.37181101862009858D02, 1.62138957456669019D03,&
     &             3.29079923573345963D03, 4.36261909014324716D03,&
     &             3.43936767414372164D03, 1.23033935480374942D03 /)
! 
! ... Coefficients for approximation to ERFC in third interval
! 
      REAL(KIND = 8), PARAMETER, DIMENSION(6) :: &
     &      P = (/ 3.05326634961232344D-1, 3.60344899949804439D-1,&
     &             1.25781726111229246D-1, 1.60837851487422766D-2,&
     &             6.58749161529837803D-4, 1.63153871373020978D-2 /)
! 
      REAL(KIND = 8), PARAMETER, DIMENSION(5) :: &
     &      Q = (/ 2.56852019228982242D00, 1.87295284992346047D00,&
     &             5.27905102951428412D-1, 6.05183413124413191D-2,&
     &             2.33520497626869185D-3 /)
! 
! -------
! ... Double precision parameters
! -------
! 
      REAL(KIND = 8), PARAMETER :: ZERO = 0.0d0, HALF   = 5.0d-1,&
     &                             ONE  = 1.0d0, TWO    = 2.0d0, &
     &                             FOUR = 4.0d0, SIXTEN = 1.6d1
! 
      REAL(KIND = 8), PARAMETER :: THRESH = 4.6875D-1,&
     &                             SQRPI  = 5.6418958354775628695D-1
! 
      REAL(KIND = 8), PARAMETER :: XINF   =  1.79D308,&
     &                             XNEG   = -2.6628D1,&
     &                             XSMALL =  1.11D-16,&
     &                             XBIG   =  2.6543D1,&
     &                             XHUGE  =  6.71D7,&
     &                             XMAX   =  2.53D307
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN) :: ARG
! 
      REAL(KIND = 8) :: DEL,X,Y,XNUM,XDEN,YSQ,ERFUNC
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: JINT
! 
      INTEGER :: i
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of Error_Functions
! 
! 
      X = ARG
      Y = DABS(X)
! 
! **********************************************************************
! *                                                                    *
! *                Evaluate  ERF  for  |X| <= 0.46875                  *
! *                                                                    *
! **********************************************************************
!
      IF_ArgValue: IF (Y <= THRESH) THEN
!
            YSQ = ZERO
            IF (Y > XSMALL) YSQ = Y * Y
!
            XNUM = A(5)*YSQ
            XDEN = YSQ
!
            DO_Int1: DO i = 1, 3
               XNUM = (XNUM + A(i)) * YSQ
               XDEN = (XDEN + B(i)) * YSQ
            END DO DO_Int1
!
            ERFUNC = X * (XNUM + A(4)) / (XDEN + B(4))
!
            IF (JINT == 1) THEN
               ERFUNC = ONE - ERFUNC
            ELSE IF (JINT == 2) THEN
               ERFUNC = DEXP(YSQ) * ERFUNC
            END IF
! 
            Error_Functions = ERFUNC   ! Assign value to the function
!
            RETURN                     ! Done - Exit the function            
! 
! **********************************************************************
! *                                                                    *
! *             Evaluate  ERFC  for 0.46875 <= |X| <= 4.0              *
! *                                                                    *
! **********************************************************************
!
         ELSE IF (Y <= FOUR .AND. Y > THRESH) THEN
!
            XNUM = C(9)*Y
            XDEN = Y
!
            DO_Int2: DO i = 1, 7
               XNUM = (XNUM + C(i)) * Y
               XDEN = (XDEN + D(i)) * Y
            END DO DO_Int2
!
            ERFUNC = (XNUM + C(8)) / (XDEN + D(8))
!
            IF (JINT /= 2) THEN
               YSQ    = AINT(Y*SIXTEN)/SIXTEN
               DEL    = (Y-YSQ)*(Y+YSQ)
               ERFUNC = DEXP(-YSQ*YSQ) * DEXP(-DEL) * ERFUNC
            END IF
! 
! **********************************************************************
! *                                                                    *
! *                   Evaluate  ERFC  for |X| > 4.0                    *
! *                                                                    *
! **********************************************************************
!
         ELSE
! 
            ERFUNC = ZERO
! 
            IF (Y >= XBIG) THEN
! 
               IF ((JINT /= 2) .OR. (Y >= XMAX)) GO TO 1000
! 
               IF (Y >= XHUGE) THEN
                  ERFUNC = SQRPI / Y
                  GO TO 1000
               END IF
! 
            END IF
! 
            YSQ = ONE / (Y * Y)
            XNUM = P(6)*YSQ
            XDEN = YSQ
! 
            DO_Int3: DO i = 1, 4
               XNUM = (XNUM + P(I)) * YSQ
               XDEN = (XDEN + Q(I)) * YSQ
            END DO DO_Int3
! 
            ERFUNC = YSQ *(XNUM + P(5)) / (XDEN + Q(5))
            ERFUNC = (SQRPI -  ERFUNC) / Y
! 
            IF (JINT /= 2) THEN
               YSQ    = AINT(Y*SIXTEN)/SIXTEN
               DEL    = (Y-YSQ)*(Y+YSQ)
               ERFUNC = DEXP(-YSQ*YSQ) * DEXP(-DEL) * ERFUNC
            END IF
! 
      END IF IF_ArgValue
! 
! **********************************************************************
! *                                                                    *
! *                      Finalize adjustments                          *
! *                                                                    *
! **********************************************************************
!
 1000 IF_JintValue: IF (JINT == 0) THEN         ! Compute erf(x) 
! 
            ERFUNC = (HALF - ERFUNC) + HALF
            IF (X < ZERO) ERFUNC = -ERFUNC
! 
         ELSE IF (JINT == 1) THEN               ! Compute erfc(x)
! 
            IF (X < ZERO) ERFUNC = TWO - ERFUNC
! 
         ELSE                                   ! Compute exp(x*x)*erfc(x)
! 
            IF (X < ZERO) THEN
! 
               IF (X < XNEG) THEN
                     ERFUNC = XINF
                  ELSE
                     YSQ    = AINT(X*SIXTEN)/SIXTEN
                     DEL    = (X-YSQ)*(X+YSQ)
                     Y      = DEXP(YSQ*YSQ) * DEXP(DEL)
                     ERFUNC = (Y+Y) - ERFUNC
               END IF
! 
            END IF
! 
      END IF IF_JintValue
! 
      Error_Functions = ERFUNC
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of Error_Functions
! 
! 
      RETURN
!
      END FUNCTION Error_Functions
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************

!
!
!
      END MODULE Hydrate_Properties
!
!
!
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

