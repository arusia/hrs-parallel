!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                                                                     >
!>       TFx_Inputs.f95: Code unit including the routines that         >
!>            read the inputs from the various input files             >
!>                                                                     >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
! 
! 
      SUBROUTINE READ_MainInputF(MC)
! 
! ...... Modules to be used 
! 
         USE EOS_Parameters 
! 
         USE Input_Comments
!
         USE Basic_Param
         USE GenControl_Param
         USE TimeSeriesP_Param
         USE SELEC_Param
         USE Diffusion_Param
         USE Solver_Param
!
         USE Connection_Arrays
         USE Variable_Arrays
         USE PFMedProp_Arrays
		 USE MPI_PARAM
		 USE MPI_SUBROUTINES
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*         ROUTINE FOR READING THE DATA IN THE MAIN INPUT FILE         *
!*          DIRECTLY FROM THE "HydrateResSim" INPUT DATA BLOCK         *
!*                                                                     *
!*                  Version 1.0 - September 29, 2004                   *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(OUT) :: MC
! 
      INTEGER :: ICALL = 0, icom = 0
! 
      INTEGER :: N_DomInit ,IERR
! 
      INTEGER :: IE1,IE18,ier1,I_match,ico,i,j,k,n
! 
! -------
! ... Character variables     
! -------
! 
      CHARACTER(LEN = 5)  :: KeyWord
      CHARACTER(LEN = 75) :: w75

	
! 
      SAVE ICALL,icom
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of READ_MainInputF
!
!
      ICALL = ICALL+1
      IF(ICALL == 1 .and. MPI_RANK==0) WRITE(11,6000)
! 
! 
!***********************************************************************
!*                                                                     *
!*                           INITIALIZATIONS                           *
!*                                                                     *
!***********************************************************************
! 
! 
      DELTMX = 0.0d0  !
      ITI    = 0      ! ITI = # of print-out time data specified
!
      N_DomInit = 0   ! Number of domains for domain-wise initialization
!
      MC = 0          ! Flag for default mesh file (MC /= 0 when the MINC concept is involved)
!
      IRPD = 5        ! Default relative permeability (all phases fully mobile)
      ICPD = 1        ! Default capillary pressure (PC = 0)
!
      CPD(1) = 0.0d0  ! Default capillary pressure parameters (1...3)
      CPD(2) = 0.0d0
      CPD(3) = 1.0d0
!
      iddiag = 0      ! Flag for consideration of diffusion (�DIFFU�-data) 
! 
! ----------
! ... Initialize logical variables        
! ----------
! 
      NoFloSimul    = .FALSE.   ! NoFloSimul = .FALSE. : Flow simulation will be ... 
                                ! ... conducted - Reset to .TRUE. if flow simulation is bypassed
! 
      NoVersion     = .FALSE.   ! NoVersion = .FALSE. : Version info is to be ...
                                ! ... printed - Reset to .TRUE. if reset by the NOVER keyword
!
      SolverBlok    = .FALSE.   ! NoSolvrBlk = .FALSE. : Solver data from MOP(21); 
                                !    Reset to .TRUE. if the SOLVR block is present
!
      CoordNeed     = .FALSE.   ! CoordNeed = .FALSE. : Coordinate arrays are ...  
                                ! ... needed - Will be internally reset to .TRUE. if otherwise
!
      WellTabData   = .FALSE.   ! WellTabdata = .FALSE. : There are no tabular well data  
                                !    Will be internally reset to .TRUE. if otherwise 
!
      RadiHeat_Flag = .FALSE.   ! Radiative heat transport is not accounted for 
                                !    Will be internally reset to .TRUE. if otherwise 
!
      CondHeat_Flag = .TRUE.    ! Conductive heat transport is accounted for 
                                !    Will be internally reset to .FALSE. if otherwise 
!
      RandmOrdrIC   = .FALSE.    ! RandmOrdrIC = .FALSE. : Initial conditions are entered ...  
                                 !    in the element order - denoted by the presence of the START keyword 
                                 !    Will be internally reset to .TRUE. (random order) if otherwise 
! 
! ----------
! ... Initialize character variables        
! ----------
! 
      PermModif = 'NONE'      ! PermModif = 'NONE' : No cell-by-cell permeability modification 
! 
! ----------
! ... Initialize primary variable values - CAREFUL! Array operations        
! ----------
! 
      DEP(1:NK1) = 0.0d0     
      xx         = 0.0d0
!
! ... Initialize fluxes: CAREFUL! Whole array operation
!
		IF(MPI_RANK==0) THEN
		  IF(NPH > 1) THEN
			 conV%FluxVD      = 0.0d0
			 FORALL (n=1:MaxNum_Conx) conV(n)%FluxVF(1:Num_MobPhs) = 0.0d0
		  END IF
	!
		   conx%FluxH  = 0.0d0
	!
		  FORALL (n=1:MaxNum_Conx) 
			 conx(n)%FluxF(1:Num_MobPhs) = 0.0d0
			 conx(n)%VelPh(1:Num_MobPhs) = 0.0d0
		  END FORALL 
		END IF
! 
! ----------
! ... Initialize component and equation parameters       
! ----------
! 
      NEQ1  = NEQ+1     
      NK1   = NK+1
      NFLUX = 2*NEQ+1
! 
! 
!***********************************************************************
!*                                                                     *
!*                 READING AND SEARCHING FOR KEYWORDS                  *
!*                                                                     *
!***********************************************************************
! 
! 
 DO WHILE (1==1)
	if(MPI_rank==0) READ (100,5002),TEMP_CHAR(1:5),TEMP_CHAR(6:80) 
	call MPI_BROADCAST(CHAR_ARRAY=TEMP_CHAR(1:80),char_size=80,root=0)
	write(KeyWord,'(5A1)'),TEMP_CHAR(1:5)
	write(w75,'(75A1)'),TEMP_CHAR(6:80)
!
!
!
!write(*,*) x
      CASE_KeyWord: SELECT CASE(KeyWord)
! 
! 
!***********************************************************************
!*                                                                     *
!*               KeyWord = 'ROCKS': READ ROCK PROPERTIES               *
!*                                                                     *
!***********************************************************************
! 
! 
      CASE ('ROCKS','MEDIA')
!
         CALL READ_RockData
!
         CYCLE
! 


! 
!***********************************************************************
!*                                                                     *
!*     KeyWord = 'DOMDE': PARAM FOR DOMAIN DECOMPOSITION ALGORITHMS    *
!*                                                                     *
!***********************************************************************
	  CASE  ('DOMDE')
		 if(MPI_RANK==0) READ (100,5003), overlap,reorder
		 overlap_loc = 1 !Not in use currently
		 N_SUB = 1 !Not in use currently
		 TEMP_INT(1:4)=(/N_SUB,overlap_loc,overlap,reorder/)
		 call MPI_BROADCAST(INT_ARRAY = TEMP_INT, INT_SIZE = 4, root =0)
		 N_SUB = TEMP_INT(1)
		 overlap_loc = TEMP_INT(2)
		 overlap = TEMP_INT(3)
		 reorder = TEMP_INT(4)
		 
		 
		 CYCLE
! 
!***********************************************************************
!*                                                                     *
!*    KeyWord = 'RPCAP': READ WETTABILITY PROPERTIES & PARAMETERS      *
!*                                                                     *
!***********************************************************************
! 
! 
      CASE ('RPCAP')
!
		if(MPI_RANK==0) THEN
        READ (100,5004), TEMP_INT(1),TEMP_REAL(1:7)
		READ (100,5004), TEMP_INT(2),TEMP_REAL(8:14)
		END IF
!
		CALL MPI_BROADCAST (INT_ARRAY = TEMP_INT(1:2), REAL_ARRAY = TEMP_REAL(1:14),&
					   int_size = 2, real_size=14, root=0)
					   
		IRPD = TEMP_INT(1)           ! Relative permeability function number
		RPD(1:7) = TEMP_REAL(1:7)    ! Relative permeability function parameters
		ICPD = TEMP_INT(2)           ! Capillary pressure function number
		CPD(1:7) = TEMP_REAL(8:14)   ! Capillary pressure function parameters
         
		CYCLE
! 
! 
!***********************************************************************
!*                                                                     *
!*      KeyWord = 'MESHM': CALL INTERNAL MESH GENERATION MODULE        *
!*                                                                     *
!***********************************************************************
! 
! 
      CASE ('MESHM')
!
         IF(MPI_RANK==0) CALL MESHM(MC)
!
         CYCLE
! 
! 
!***********************************************************************
!*                                                                     *
!*     KeyWord = 'START': SET FLAG TO ALLOW RANDOMN INITIALIZATION     *
!*                                                                     *
!***********************************************************************
! 
! 
      CASE ('START')
!
         RandmOrdrIC = .TRUE.   ! Initial conditions to be read in random order
!
         CYCLE
! 
! 
!***********************************************************************
!*                                                                     *
!*          KeyWord = 'PARAM': READ COMPUTATIONAL PARAMETERS           *
!*                                                                     *
!***********************************************************************
! 
! 
      CASE ('PARAM')
!
         CALL READ_ParamData
!
         CYCLE
! 
! 
!***********************************************************************
!*                                                                     *
!*            KeyWord = 'ELEME': READ ELEMENT-RELATED DATA             *
!*                                                                     *
!***********************************************************************
! 
! 
      CASE ('ELEME')
!
         IF(MPI_RANK==0) CALL READ_ElemData
!
         CYCLE
! 
! 
!***********************************************************************
!*                                                                     *
!*          KeyWord = 'CONNE': READ CONNECTION-RELATED DATA            *
!*                                                                     *
!***********************************************************************
! 
! 
      CASE ('CONNE')
!
         IF(MPI_RANK==0) CALL READ_ConxData
!
         CYCLE
! 
! 
!***********************************************************************
!*                                                                     *
!*         KeyWord = 'GENER': READ SINK/SOURCE-RELATED DATA            *
!*                                                                     *
!***********************************************************************
! 
! 
      CASE ('GENER')
!
         IF(MPI_RANK==0) CALL READ_GnerData
!
         CYCLE
! 
! 
!***********************************************************************
!*                                                                     *
!*     KeyWord = 'INCON': READ MULTI-COMPONENT INITIAL CONDITIONS      *
!*                                                                     *
!***********************************************************************
! 
! 
      CASE ('INCON')
!
         IF(MPI_RANK==0) CALL READ_InConData
!

	
         CYCLE
! 
! 
!***********************************************************************
!*                                                                     *
!*     KeyWord = 'INDOM': READ DOMAIN-SPECIFIC INITIAL CONDITIONS      *
!*                                                                     *
!***********************************************************************
! 
! 
      CASE ('INDOM')
!
         CALL READ_InDomData(N_DomInit)
!
         CYCLE
! 
! 
!***********************************************************************
!*                                                                     *
!*    KeyWord = 'NOVER': SET FLAG FOR SUPPRESSING VERSION-PRINTOUT     *
!*                                                                     *
!***********************************************************************
! 
! 
      CASE ('NOVER')
!
         NoVersion = .TRUE.
!
         CYCLE
! 
! 
!***********************************************************************
!*                                                                     *
!*  KeyWord = 'SOLVR': READ SOLVER TYPE AND CORRESPONDING PARAMETERS   *
!*                                                                     *
!***********************************************************************
! 
! 
      CASE ('SOLVR')
!
         SolverBlok = .TRUE.
         IF(MPI_RANK==0) READ (100,5008),TEMP_INT(1),TEMP_CHAR(1:4),TEMP_REAL(1:2)
		 
		 CALL MPI_BROADCAST(INT_ARRAY=TEMP_INT(1),CHAR_ARRAY=TEMP_CHAR(1:4)&
					&,REAL_ARRAY=TEMP_REAL(1:2),INT_SIZE=1,CHAR_SIZE=4,&
					&REAL_SIZE=2,ROOT=0)
  		 matslv = TEMP_INT(1)
		 write(zprocs,'(2A1)'), TEMP_CHAR(1:2)
		 write(oprocs,'(2A1)'), TEMP_CHAR(3:4)
		 ritmax = TEMP_REAL(1)
		 closur = TEMP_REAL(2)
!
         CYCLE
! 
! 
!***********************************************************************
!*                                                                     *
!*            KeyWord = 'HYDRA': READ HYDRATE PARAMETERS               *
!*                                                                     *
!***********************************************************************
! 
! 
      CASE ('HYDRA')
!
         CALL READ_HydrateData
!
         CYCLE
! 
! 
!***********************************************************************
!*                                                                     *
!*            KeyWord = 'TIMES': READ TIMES FOR PRINT-OUTS             *
!*                                                                     *
!***********************************************************************
! 
! 
      CASE ('TIMES')
!
         CALL READ_TimeData
!
         CYCLE
! 
! 
!***********************************************************************
!*                                                                     *
!*     KeyWord = 'SELEC': READ DATA BLOCK WITH SELECTION PARAMETERS    *
!*                                                                     *
!***********************************************************************
! 
! 
      CASE ('SELEC')
!
         IF(MPI_RANK==0) READ (100,5006), (IE(i),i=1,16)
		 CALL MPI_BROADCAST(INT_ARRAY=IE,INT_SIZE=16,root=0)
		 
         IE1 = 1
         IF(IE(1) > 1) IE1=IE(1)
         IF(IE1 > 64 .and. MPI_RANK==0) PRINT 6004,IE1
! 
! ----------
! ...... Allocate memory to the storage array FE   
! ----------
! 
         IE18 = MIN(IE1*8,512)             ! Determine size
!
         ALLOCATE(FE(IE18), STAT=ier1)     ! Allocate memory
!
         IF(MPI_RANK==0) READ (100,5010), (FE(i),i=1,IE18)       ! Read the input data
!
		 CALL MPI_BROADCAST(REAL_ARRAY=FE,REAL_SIZE=IE18,root=0)
         CYCLE
! 
! 
!***********************************************************************
!*                                                                     *
!*                   KeyWord = 'Elem_Time_Series'                      *
!*            READ ELEMENT NAMES FOR TIME SERIES OUTPUT                *
!*                                                                     *
!***********************************************************************
! 
! 
      CASE ('Elem_','FOFT')
!
! ...... Initialization - CAREFUL! Whole array operation
!
         N_obs_elem    = 0
         obs_elem_Flag = .FALSE.
         obs_elem%name = '        '
         obs_elem%num  = 0
!
! ...... Read in the observation elements
!
		IF(MPI_RANK==0) THEN
			 DO i=1,100
				READ (100,5012), TEMP_CHAR(5*(i)-4:5*i)
				write(obs_elem%name(i),'(5A1)'), TEMP_CHAR(5*(i)-4:5*i)
				IF(obs_elem%name(i) == '     ') EXIT
			 END DO
		END IF
		TEMP_INT(1) = i
		CALL MPI_BROADCAST(int_array=temp_int,int_size=1,root=0)
		i = temp_int(1)
		CALL MPI_BROADCAST(CHAR_ARRAY=TEMP_CHAR,char_size=i*5,&
							root=0)
	!
		 N_obs_elem = i-1
	!
		DO i=1,N_obs_elem+1
			write(obs_elem%name(i),'(5A1)'), TEMP_CHAR(5*(i)-4:5*i)
		END DO
		
		 IF(N_obs_elem > 0) THEN
			obs_elem_Flag = .TRUE.
			OPEN(12,file='Elem_Time_Series',status='unknown')
			REWIND 12
		 END IF
!
         CYCLE
! 
! 
!***********************************************************************
!*                                                                     *
!*                     KeyWord = 'SS_Time_Series'                      *
!*           READ SOURCE/SINK NAMES FOR TIME SERIES OUTPUT             *
!*                                                                     *
!***********************************************************************
! 
! 
      CASE ('SS_Ti','GOFT')
!
         OPEN(13,file='SS_Time_Series',status='unknown')
         REWIND 13
!
! ...... Initialization - CAREFUL! Whole array operation
!
!
         N_obs_SS    = 0
         obs_SS_Flag = .FALSE.
         obs_SS%name = '     '
         obs_SS%num  = 0
!
! ...... Read in the observation sources/sinks
!
		IF(MPI_RANK==0) THEN
			 DO i=1,100
				READ (100,'(5A1)'), TEMP_CHAR(5*(i)-4:5*i)
				WRITE(obs_SS%name(i),'(5A1)'), TEMP_CHAR(5*(i)-4:5*i)
				IF(obs_SS%name(i) == '     ') EXIT
			 END DO
		END IF
		TEMP_INT(1) = i
		CALL MPI_BROADCAST(int_array=TEMP_INT,int_size=1,root=0)
		i = TEMP_INT(1)
		CALL MPI_BROADCAST(CHAR_ARRAY=TEMP_CHAR,char_size=i*5,&
							root=0)
        N_obs_SS = i-1
        
		DO i=1,N_obs_SS+1
            WRITE(obs_SS%name(i),'(5A1)'), TEMP_CHAR(5*(i)-4:5*i)
         END DO
!
!
! .....  When no element data are given in block "SS_Time_Series", set "N_obs_SS" to a 
! .....  flag value that will cause all elements with generation data to be tabulated
!
         IF(N_obs_elem == 0) N_obs_SS = -1
!
         obs_SS_Flag = .TRUE.
!
         CYCLE
! 
! 
!***********************************************************************
!*                                                                     *
!*                   KeyWord = 'Conx_Time_Series'                      *
!*           READ CONNECTION NAMES FOR TIME SERIES OUTPUT              *
!*                                                                     *
!***********************************************************************
! 
! 
      CASE ('Conx_','COFT')
!
! ...... Initialization - CAREFUL! Whole array operation
!
         N_obs_conx    = 0
         obs_conx_Flag = .FALSE.
         obs_conx%name = '                '
         obs_conx%num  = 0
!
! ...... Read in the observation connections
!
		IF(MPI_RANK==0) THEN
			 DO i=1,100
				READ (100,5014), TEMP_CHAR(10*(i)-9:10*i)
				WRITE(obs_conx%name(i),'(5A1)'), TEMP_CHAR(5*(i)-4:5*i)
				IF( obs_conx%name(i) == '          ') EXIT
			 END DO
		END IF
		TEMP_INT(1) = i
		CALL MPI_BROADCAST(int_array=TEMP_INT,int_size=1,root=0)
		i = TEMP_INT(1)
		CALL MPI_BROADCAST(CHAR_ARRAY=TEMP_CHAR,char_size=i*5,&
							root=0)
        N_obs_conx = i-1
        
		DO i=1,N_obs_conx+1
            WRITE(obs_conx%name(i),'(5A1)'), TEMP_CHAR(5*(i)-4:5*i)
         END DO
!
!
         IF(N_obs_conx > 0) THEN
            obs_conx_Flag = .TRUE.
            OPEN(14,file='Conx_Time_Series',status='unknown')
            REWIND 14
         END IF
!
         CYCLE
! 
! 
!***********************************************************************
!*                                                                     *
!*    KeyWord = 'DIFFU': READ MULTICOMPONENT DIFFUSION COEFFICIENTS    *
!*                                                                     *
!***********************************************************************
! 
! 
      CASE ('DIFFU ')
!
         iddiag = iddiag+1
!
         DO k=1,nk
            IF(MPI_RANK==0) &
				&READ (100,5010), TEMP_REAL((k-1)*nph+1:k*nph)
         END DO
		 CALL MPI_BROADCAST(REAL_ARRAY=TEMP_REAL,real_size=nph*k,root=0)
		 
		 DO k=1,nk
			diffusivity(1:nph,k)=TEMP_REAL((k-1)*nph+1:k*nph)
		 END DO
!
         CYCLE
! 
! 
!***********************************************************************
!*                                                                     *
!*              KeyWord = 'ENDFI': BYPASS FLOW SIMULATION              *
!*                                                                     *
!***********************************************************************
! 
! 
      CASE ('ENDFI')
!
         IF(MPI_RANK==0) PRINT 6006
!
         NoFloSimul = .TRUE.
!
         RETURN
! 
! 
!***********************************************************************
!*                                                                     *
!*        KeyWord = 'ENDCY': READING OF INPUT FILE IS COMPLETE         *
!*                                                                     *
!***********************************************************************
! 
! 
      CASE ('ENDCY')
!
! ...... Initialization - CAREFUL! Array operations 
!
         StateIndex = 0
         YIN        = 0.0d0
! 
! ----------
! ...... Assign domain-specific initial conditions
! ----------
! 
         IF_DomIn: IF(N_DomInit > 0) THEN
!
! ......... Process domain-specific initial conditions
!
            DO_NoIDom: DO j=1,N_DomInit
!
               I_match = 0
               DO_NoRok: DO i=1,N_PFmedia
!
                  IF(MAT(i) == Rk_name(j)) THEN
                     I_match = 1
                     EXIT
                  END IF
!
               END DO DO_NoRok
! 
! ............ Setting initial conditions - CAREFUL! Array operations 
! 
               IF(I_match == 1) THEN
                  StateIndex(i)         = StateI(j)
                  YIN(1:MaxNum_Equat,i) = XIN(1:MaxNum_Equat,j)
               ELSE
                  EXIT
               END IF
! 
            END DO DO_NoIDom
!
         END IF IF_DomIn
! 
! ----------
! ...... Assign initial relative permeability parameters for undeclared PF media
! ...... CAREFUL! Array operations
! ----------
! 
         FORALL (n=1:N_PFmedia, media(n)%NumF_RP == 0)
            media(n)%ParRP(1:7) = RPD(1:7)  
            media(n)%NumF_RP    = IRPD                                                     
         END FORALL 
!
! ----------
! ...... Assign initial capillary pressure parameters for undeclared PF media
! ...... CAREFUL! Array operations
! ----------
!
         FORALL (n=1:N_PFmedia, media(n)%NumF_CP == 0)
            media(n)%ParCP(1:7) = CPD(1:7)  
            media(n)%NumF_CP    = ICPD                                                     
         END FORALL
! 
! ----------
! ...... Print accumulated comments
! ----------
! 
         IF_ComPrint: IF(icom /= 0) THEN
!
            ico = MIN(50,icom)
            if(MPI_RANK==0) PRINT 6010, ' ',icom
!
            DO i=1,ico
               if(MPI_RANK==0) PRINT 6011, comm(i)
            END DO
!
            if(MPI_RANK==0) PRINT 6012
!
         END IF IF_ComPrint
! 
! 
!***********************************************************************
!*                                                                     *
!*      Default case: The lines that start with unknown keywords       *
!*                    are stored as comments (<= 50 lines)             *
!*                                                                     *
!***********************************************************************
! 
! 
      CASE DEFAULT
! 
! ...... Storing lines with unknown leading keywords 
! 
         icom = icom+1
         IF(icom <= 50) comm(icom) = KeyWord//w75
!
         CYCLE
!
!
! 
      END SELECT CASE_KeyWord
	 
	 EXIT
END DO
!write(*,*) x
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 5002 FORMAT(5A1,75A1)
 5003 FORMAT(2I10) 
 5004 FORMAT(I5,5X,7E10.4)
!
 5006 FORMAT(16I5)
 5008 FORMAT(i1,2x,2a,3x,2a,2(e10.4))
!
 5010 FORMAT(8E10.4)
 5012 FORMAT(A5,A75)
 5014 format(10A1)
! 
! 
! 
 6000 FORMAT(/,'READ_MainInputF   1.0   29 September 2004',6X,&
     &         'Read all data provided through the main input file')
! 
 6004 FORMAT(' !!!!! WARNING !!!!! ==> ',&
     &       ' IE(1) =',I5,' in block "SELEC" is too large;',&
     &       ' Can read at most 64 records',/)
!
 6006 FORMAT(/,' HAVE READ KEYWORD "ENDFI" IN FILE INPUT ==>',&
     &         ' BYPASS FLOW SIMULATION   ',62('*'),/,&
     &         ' OPTIONAL  PRINTOUT OF INPUT DATA (FOR MOP(7) /= 0)',&
     &         ' IS AVAILABLE')
!
 6010 format(A1,/,23X,86('*'),/,23X,'*',84X,'*',/,&
     &       23X,'*',5X,'Comments in Input Data (have ',&
     &       'encountered',I4,' records with unknown keywords)',&
     &       4X,'*',/,&
     &       23X,'*',30X,'(print up to 50 of them)',30X,'*',/,&
     &       23X,'*',84X,'*'/23X,86('*'),/,23X,'*',84X,'*')
 6011 format(23X,'*  ',A80,'  *')
 6012 format(23X,'*',84X,'*'/23X,86('*'))
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of READ_MainInputF
!
!
      RETURN
!
      END SUBROUTINE READ_MainInputF
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE READ_ElemData
! 
! ...... Modules to be used 
! 
         USE Basic_Param
! 
         USE PFMedProp_Arrays, ONLY: MAT
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*     ROUTINE FOR READING THE DATA DESCRIBING THE DOMAIN ELEMENTS     *
!*         DIRECTLY FROM THE "HydrateResSim" INPUT DATA BLOCK          *
!*                                                                     *
!*                   Version 1.0 - January 6, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: VOLX,AHTX,zref,X,Y,Z
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: NE,NSEQ,NSEQ1,NADD,MA2
      INTEGER :: n,i,m,i_ASCII
! 
! -------
! ... Character variables
! -------
! 
      CHARACTER(LEN = 3) :: EL,MA1
      CHARACTER(LEN = 5) :: MA12
      CHARACTER(LEN = 6) :: EL6_0
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: Medium_ByName
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of READ_ElemData
!
!
      ICALL = ICALL+1                                                     
      IF(ICALL == 1) WRITE(11,6000)                                      
! 
! --------
! ... Printing headings        
! --------
! 
      PRINT 5002
      REWIND 4
! 
! --------
! ... Printing according to the number of characters in the element names         
! --------
! 
      IF_ElCh8: IF(No_CEN /= 8) THEN
!
! ................ 5-Character elements
!
                   PRINT 6004
                   WRITE(4,6002)
                ELSE
!
! ................ 8-Character elements
!
                   PRINT 6008, 'ext8'
                   WRITE(4,6006) 'ext8'
                END IF IF_ElCh8
! 
!***********************************************************************
!*                                                                     *
!*                       READING THE ELEMENT DATA                      *
!*                                                                     *
!***********************************************************************
! 
	DO WHILE(1==1)
	  IF_ElCh: IF(No_CEN /= 8) THEN
	!
	! ............... Read data for 5-Character elements
	!
					  READ (100,5010), EL, &   ! 3-Character component of the element name
		 &                       NE,  &  ! 2-Digit number component of the element name
		 &                       NSEQ, & ! Number of additional elements with the same attributes
		 &                       NADD, & ! Increment of the numbers of similar elements
		 &                       MA12, & ! Name of the corresponding rock type
		 &                       VOLX, & ! Element volume (m^3)
		 &                       AHTX, & ! Element area for estimation of boundary heat exchange (m^2)
		 &                       zref, & ! A reference elevation
		 &                       X,  &   ! X-coordinate of element center (m)
		 &                       Y, &    ! Y-coordinate of element center (m)
		 &                       Z      ! Z-coordinate of element center (m)
				   ELSE
	!
	! ............... Read data for 8-Character elements
	!
					  READ (100,5012), EL6_0, &      ! 6-Character component of the element name
		 &                       NE,MA12,&      ! All other parameters as above
		 &                       VOLX,AHTX,&
		 &                       zref,X,Y,Z
	!
					  EL   = EL6_0(1:3)
					  NSEQ = 0
					  NADD = 0
				   END IF IF_ElCh
	! 
	! --------
	! ... End of the element records         
	! --------
	! 
		  IF_ElEnd: IF(EL == '   ' .AND. NE == 0) THEN
					   WRITE(4,5002)
					   RETURN
					END IF IF_ElEnd
	!
		  NSEQ1 = NSEQ+1
	! 
	!***********************************************************************
	!*                                                                     *
	!*                  ASSIGNING THE MATERIAL/ROCK TYPE                   *
	!*           WRITING THE ELEMENT DATA INTO THE �MESH� FILE             *
	!*                                                                     *
	!***********************************************************************
	! 
		  Medium_ByName = .FALSE.                     ! Default: Medium by number
	! 
		  DO_MedName: DO i=1,5
	! 
			 i_ASCII = ICHAR(MA12(i:1))               ! Determine the ASCII value of ...
	!                                                 ! ... each character of the medium name
			 IF(i_ASCII == 32) CYCLE DO_MedName       ! Continue iteration for blanks
	!                                                 
			 IF(i_ASCII > 57 .OR. i_ASCII <48) THEN   ! If any character is a letter, ...
				Medium_ByName = .TRUE.                ! ... the flag is reset
				EXIT DO_MedName
			 END IF
	! 
		  END DO DO_MedName
	! 
	! -----------
	! ...... Elements specified by material number         
	! -----------
	! 
		  IF_MatNum: IF(Medium_ByName .EQV. .FALSE.) THEN
	! 
			 READ(MA12,'(I5)') MA2        ! Read MA2(target) from MA12(string) 
	!
	! ...... 5-Character elements
	!
			 IF_El8: IF(No_CEN /= 8) THEN
	!
				DO I=1,NSEQ1
				   N = NE+(I-1)*NADD
				   WRITE(4,5114) EL,N,MA2,VOLX,AHTX,zref,X,Y,Z
				END DO
	!
	! ...... 8-Character elements
	!
			 ELSE
	!
				DO I=1,NSEQ1
				   N = NE+(I-1)*NADD
				   WRITE(4,5116) EL6_0,N,MA2,VOLX,AHTX,zref,X,Y,Z
				END DO
	!
			 END IF IF_El8
	!
	! ...... Continue reading data
	!
			 CYCLE
	! 
	! -----------
	! ...... Elements specified by material name         
	! -----------
	! 
		  ELSE
	!
	! ...... Search the rock types for match
	!
			 DO_NumRoks: DO M=1,N_PFmedia
	! 
	! --------------
	! ......... Matching rock type        
	! ......... Printing warning and ignoring the element      
	! --------------
	! 
				IF_MatchN: IF(MA12 == MAT(M)) THEN
	!
	! ............ 5-Character elements
	!
				   IF(No_CEN /= 8) THEN
					  DO_E1: DO I=1,NSEQ1
						 N = NE+(I-1)*NADD
						 WRITE(4,5014) EL,N,M,VOLX,AHTX,zref,X,Y,Z
					  END DO DO_E1
	! 
	! ............ 8-Character elements
	!
				   ELSE
					  DO_E2: DO I=1,NSEQ1
						 N = NE+(I-1)*NADD
						 WRITE(4,5016) EL6_0,N,M,VOLX,AHTX,zref,X,Y,Z
					  END DO DO_E2
				   END IF
	!
	! ............ Continue reading data
	!
				   CYCLE
	!
				END IF IF_MatchN
	!
			 END DO DO_NumRoks
	! 
	! -----------
	! ...... Unknown rock type        
	! ...... Printing warning and ignoring the element      
	! -----------
	! 
			 IF(No_CEN /= 8) THEN
				PRINT 6401, MA1,MA2,EL,NE      ! 5-Character elements
			 ELSE 
				PRINT 6402, MA1,MA2,EL6_0,NE   ! 8-Character elements
			 END IF
	!
	! ...... Continue reading element data
	!
			 CYCLE
	!
	! ...... Continue reading element data
	!
		  END IF IF_MatNum
		  EXIT
	END DO
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 5002 FORMAT('     ')
!
 5010 FORMAT(A3,I2,2I5,A5,6E10.4)
 5012 FORMAT(A6,I2, 7X,A5,6E10.4)
! 
 5014 FORMAT(A3,I2,10X,I5,6E10.4)
 5016 FORMAT(A6,I2, 7X,I5,6E10.4)
! 
 5114 FORMAT(A3,I2,10X,I5,6E10.4)
 5116 FORMAT(A6,I2, 7X,I5,6E10.4)
!
!
!
 6000 FORMAT(/,'READ_ElemData     1.0    6 January   2004',6X,&
     &         'Read the element data from the ELEME block of ',&
     &         'the input data file') 
!
 6002 FORMAT('ELEME')
 6004 FORMAT(' WRITE FILE *MESH* FROM INPUT DATA')
 6006 FORMAT('ELEME',a4)
 6008 FORMAT(' WRITE FILE *MESH* FROM INPUT DATA -', &
     &       ' Mesh data follow the ',a4,' option')
!
 6401 FORMAT(' REFERENCE TO UNKNOWN MATERIAL ',A3,A2,' AT ELEMENT ',&
     &        A3,I2,' --- WILL IGNORE ELEMENT')
 6402 FORMAT(' REFERENCE TO UNKNOWN MATERIAL ',A3,A2,' AT ELEMENT ',&
     &        A6,I2,' --- WILL IGNORE ELEMENT')
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of READ_ElemData
!
!
      RETURN
! 
      END SUBROUTINE READ_ElemData
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE READ_ConxData
! 
! ...... Modules to be used 
! 
         USE Basic_Param
!   
         USE Connection_Arrays, ONLY: conx
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*   ROUTINE FOR READING THE DATA DESCRIBING THE DOMAIN CONNECTIONS    *
!*         DIRECTLY FROM THE "HydrateResSim" INPUT DATA BLOCK          *
!*                                                                     *
!*                   Version 1.0 - January 6, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: D1,D2,AREAX,BETAX,sigx
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: NE1,NE2,NSEQ,NSEQ1,NAD1,NAD2,ISOT
      INTEGER :: N,N1,N2,I
! 
! -------
! ... Character variables
! -------
! 
      CHARACTER(LEN = 3) :: EL1,EL2
      CHARACTER(LEN = 6) :: EL6_1,EL6_2
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of READ_ConxData
!
!
      ICALL = ICALL+1                                                     
      IF(ICALL == 1) WRITE(11,6000)                                      
! 
! --------
! ... Printing headings and initializing        
! --------
! 
      WRITE(4,6002)
!
! ... Initializing the connection counter to zero
!
      NCON  = 0
! 
!***********************************************************************
!*                                                                     *
!*                     READING THE CONNECTION DATA                     *
!*                                                                     *
!***********************************************************************
! 
 1000 IF_ElCh: IF(No_CEN /= 8) THEN
!
! ............... Read connection data for 5-Character elements
!
                  READ (100,5004), EL1, &   ! 3-Character component of the name of the 1st element in the connection 
     &                       NE1,  &  ! 2-Digit number component of the name of the 1st element in the connection
     &                       EL2,  &  ! 3-Character component of the name of the 2nd element in the connection
     &                       NE2,  &  ! 2-Digit number component of the name of the 2nd element in the connection
     &                       NSEQ,&   ! Number of additional connections with the same attributes
     &                       NAD1, &  ! Increment of the 1st element # in the connections
     &                       NAD2,&  ! Increment of the 2nd element # in the connections
     &                       ISOT,&   ! =1,2,3: Permeability between the elements given by the ISOT perm. component in ROCKS
     &                       D1,&    ! Distance of interface from the center of 1st element (m)
     &                       D2,&     ! Distance of interface from the center of 2nd element (m)
     &                       AREAX,&  ! Interface area (m^2)
     &                       BETAX,& ! = Cos(b), b: angle between vertical and line connecting the element centers
     &                       sigx    ! Radiant emittance factor for radiative heat transfer
               ELSE
!
! ............... Read connection data for 8-Character elements
!
                  READ (100,5006), EL6_1, &     ! 6-Character component of the name of the 1st element in the connection
     &                       NE1,   &     ! As previously
     &                       EL6_2, &     ! 6-Character component of the name of the 2nd element in the connection
     &                       NE2,   &     ! All other parameters as previously defined
     &                       ISOT,D1,D2,&
     &                       AREAX,BETAX,sigx
!
                  EL1  = EL6_1(1:3)
                  NSEQ = 0
                  NAD1 = 0
                  NAD2 = 0
               END IF IF_ElCh
! 
! --------
! ... End of the connection records - No info on connection elements        
! --------
! 
      IF_El1End: IF(EL1 == '   ' .AND. NE1 == 0) THEN
! 
                    WRITE(4,5001)
! 
! ................. The connection data read-in is completed 
! 
                    RETURN
! 
                 END IF IF_El1End
! 
! --------
! ... End of the connection records - Info on connection elements        
! --------
! 
      IF_El2End: IF(EL1 == '+++') THEN
! 
                    WRITE(4,5002)
! 
! ................. Read the connection-associated element numbers 
! 
                    IF(No_CEN /= 8) THEN
                       READ (100,5010),    (conx(N)%n1,conx(N)%n2,N=1,NCON)
                       WRITE(4,5010) (conx(N)%n1,conx(N)%n2,N=1,NCON)
                    ELSE
                       READ (100,5015),    (conx(N)%n1,conx(N)%n2,N=1,NCON)
                       WRITE(4,5015) (conx(N)%n1,conx(N)%n2,N=1,NCON)
                    END IF
! 
! ................. The connection data read-in is completed 
! 
                    WRITE(4,5001)
                    RETURN
! 
                 END IF IF_El2End
!
      NSEQ1 = NSEQ+1
! 
!***********************************************************************
!*                                                                     *
!*          WRITING THE CONNECTION DATA INTO THE �MESH� FILE           *
!*                                                                     *
!***********************************************************************
! 
      DO_Seq: DO I=1,NSEQ1
!
! ...... Count connections
!
         NCON = NCON+1
!
! ...... Determine element numbers in connection NCON
!
         N1 = NE1+(I-1)*NAD1
         N2 = NE2+(I-1)*NAD2
!
! ...... Writing the element info into MESH
!
         IF(No_CEN /= 8) THEN
            WRITE(4,6004) EL1,N1,EL2,N2,ISOT,D1,D2,&
     &                    AREAX,BETAX,sigx
         ELSE
            WRITE(4,6006) EL6_1,NE1,EL6_2,NE2,ISOT,D1,D2,&
     &                    AREAX,BETAX,sigx
         END IF
! 
      END DO DO_Seq
! 
! --------
! ... Continue reading connection data
! --------
! 
      GO TO 1000
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 5001 FORMAT('     ')
 5002 FORMAT('+++  ')
!
 5004 FORMAT(A3,I2,A3,I2,4I5,5E10.4)
 5006 FORMAT(A6,I2,A6,I2,9X,I5,5E10.4)
! 
 5010 FORMAT(16I5)
 5015 FORMAT(10I8)
! 
 5114 FORMAT(A3,I2,13X,A2,6E10.4)
 5116 FORMAT(A6,I2,10X,A2,6E10.4)
!
 6000 FORMAT(/,'READ_ConxData     1.0    6 January   2004',6X,&
     &         'Read the connection data from the CONNE block of ',&
     &         'the input data file') 
!
 6002 FORMAT('CONNE')
!
 6004 FORMAT(A3,I2,A3,I2,15X,I5,5E10.4)
 6006 FORMAT(A6,I2,A6,I2, 9X,I5,5E10.4)
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of READ_ConxData
!
!
      RETURN
! 
      END SUBROUTINE READ_ConxData
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE READ_RockData
! 
! ...... Modules to be used 
! 
         USE Universal_Param
         USE Basic_Param
         USE Diffusion_Param
! 
         USE PFMedProp_Arrays
		 USE MPI_PARAM
		 USE MPI_SUBROUTINES
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*    ROUTINE FOR READING THE DATA DESCRIBING THE MEDIA PROPERTIES     *
!*         DIRECTLY FROM THE "HydrateResSim" INPUT DATA BLOCK          *
!*                                                                     *
!*                   Version 1.0 - January 6, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: OrgCF,VolGrain
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0
      INTEGER :: NAD,n,n_i,i
! 
! -------
! ... Character variables
! -------
! 
      CHARACTER(LEN = 5) :: MAT_name
! 		
	  
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of READ_RockData
!
!
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .and. MPI_RANK==0) WRITE(11,6000)                                      
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                         ROCK TYPE LOOP                              >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_NumRock: DO n_i = 1,MaxNum_Media+1
! 
!***********************************************************************
!*                                                                     *
!*             Checking the adequacy of the dimensioning               *
!*                                                                     *
!***********************************************************************
! 
         IF_Limit: IF(n_i == MaxNum_Media+1) THEN   
! 
             IF(MPI_RANK==0) THEN
				READ (100,5002), MAT_name      ! Reading the MaxNum_Media+1 record
! 
! .......... If 'MAT_name' is not blank, print an error message & stop 
	! 
				 IF_Last: IF(MAT_name /= '     ' .AND. MPI_RANK==0) THEN   
					PRINT 6001, MaxNum_Media   
					STOP
				 END IF IF_Last      
			 END IF
	! 
	! ......... Otherwise, the rock data read-in is completed 
	! 
			 N_PFmedia = MaxNum_Media  ! The number of media is set
			 EXIT DO_NumRock
             
!
         END IF IF_Limit
! 
!***********************************************************************
!*                                                                     *
!*              READING THE ROCK TYPE DATA: 1st Record                 *
!*                                                                     *
!***********************************************************************
! 
         PoMed(n_i)%RGrain = 0.0d0
! 
	IF(MPI_RANK==0) THEN
         READ (100,5002), TEMP_CHAR(1:5),&
     &              TEMP_INT(1),   &             
     &              TEMP_real(1:8)           
	 END IF
	 
	 CALL MPI_BROADCAST(INT_ARRAY=TEMP_INT(1),CHAR_ARRAY=TEMP_CHAR(1:5),real_ARRAY=TEMP_real(1:8),&
					INT_SIZE=1,CHAR_SIZE=5,real_SIZE=8,root=0)
	 
	 WRITE(MAT(n_i),'(5A1)'), TEMP_CHAR(1:5)  ! Rock type name
     NAD = TEMP_INT(1)					! Number of additional lines with media properties
     media(n_i)%DensG = TEMP_real(1)		! Rock bulk density (kg/m^3)
     media(n_i)%Poros = TEMP_real(2)		! Porosity
     media(n_i)%Perm(1:3)= TEMP_real(3:5) ! Intrinsic permeability along principal axes (m^2)
     media(n_i)%KThrW= TEMP_real(6)		! Thermal conductivity of water-saturated rock (W/m^2)
     media(n_i)%SpcHt= TEMP_real(7)		! Rock specific heat (J/kg/C)
     PoMed(n_i)%RGrain = TEMP_real(8)  	! Radius of the media grains (J/kg/C)		
! 
! --------
! ... End of the rock records        
! --------
! 
         IF_RokEnd: IF(MAT(n_i) == '     ') THEN
! 
! ......... Determine the number of media in the system 
! 
            N_PFmedia = n_i-1
! 
! ......... Otherwise, the rock data read-in is completed 
! 
            EXIT DO_NumRock
! 
         END IF IF_RokEnd
! 
!***********************************************************************
!*                                                                     *
!*             INITIALIZATION OF DATA IN SECOND RECORD                 *
!*                                                                     *
!***********************************************************************
! 
         media(n_i)%Compr = 0.0D0     
         media(n_i)%Expan = 0.0D0     
         media(n_i)%KThrD = 0.0D0     
         media(n_i)%Tortu = 0.0D0     
         media(n_i)%Klink = 0.0D0     
! 
         media(n_i)%NumF_RP = 0           
         media(n_i)%NumF_CP = 0           
! 
! -----------
! ...... Account for the number of additional records for the cases of  
! ...... (a) reference conditions or (b) seeds for permeability multipliers        
! -----------
!        
         IF(mat(n_i) == 'REFCO' .OR. mat(n_i) == 'SEED') THEN
            IF(NAD /= 0) NAD = 0
         END IF
! 
!***********************************************************************
!*                                                                     *
!*              READING THE ROCK TYPE DATA: 2nd Record                 *
!*                                                                     *
!***********************************************************************
! 
         IF_RokInLine2: IF(NAD > 0 ) THEN
! 
			if(MPI_RANK==0) READ (100,5004), TEMP_real(1:9)
			
			call MPI_BROADCAST(real_ARRAY = TEMP_real(1:9),REAL_SIZE=9,root=0)
			
			media(n_i)%Compr = TEMP_real(1)  
			media(n_i)%Expan = TEMP_real(2)  
			media(n_i)%KThrD = TEMP_real(3) 
			media(n_i)%Tortu = TEMP_real(4) 
			media(n_i)%Klink = TEMP_real(5) 
			OrgCF = TEMP_real(6)
			! 
         END IF IF_RokInLine2
		 
		 
! 
! -----------
! ...... Assign default value to dry thermal conductivity if needed  
! -----------
!        
         IF(ABS(media(n_i)%KThrD) < 1.0D-6) THEN
            media(n_i)%KThrD = media(n_i)%KThrW
         END IF
! 
!***********************************************************************
!*                                                                     *
!*           READING THE ROCK TYPE DATA: 3rd & 4th Records             *
!*                                                                     *
!***********************************************************************
! 
         IF_RokInLine3: IF(NAD > 1) THEN
! 
			if(MPI_RANK==0) THEN
				READ (100,5008), TEMP_INT(1),TEMP_REAL(1:7)
				READ (100,5008), TEMP_INT(2),TEMP_REAL(8:14)
			end if
! 		
				
			 call MPI_BROADCAST(INT_ARRAY=TEMP_INT(1:2),REAL_ARRAY=TEMP_REAL(1:14),&
								INT_SIZE=2, REAL_SIZE=14, root=0)
								
			 media(n_i)%NumF_RP = TEMP_INT(1)       ! Relative permeability function # 
			 media(n_i)%ParRP(1:7) = TEMP_REAL(1:7)  ! Rel-perm. function parameters
			 media(n_i)%NumF_CP = TEMP_INT(2)       ! Capillary pressure function #
			 media(n_i)%ParCP(1:7) = TEMP_REAL(8:14)! Cap-press. function parameters
	! 
         END IF IF_RokInLine3
		 
		 
! ...... Printing the rock type number and name 
! 
         if(MPI_RANK==0) PRINT 6002, n_i,MAT(n_i)
!
! <<<                      
! <<< End of the ROCK TYPE LOOP         
! <<<
!
      END DO DO_NumRock
! 
! 
!***********************************************************************
!*                                                                     *
!*      Initializations and baseline computations to determine         *
!*     the reaction surface area of kinetic hydrate dissociation       *
!*                                                                     *
!***********************************************************************
! 
! 
      IF(EOS_name(1:7) /= 'HYDRATE') RETURN  ! If not a hydrate simulation, ...
!                                            ! ... exit the routine 
! 
      DO_NumMedia: DO n=1,N_PFmedia 
! 
! ----------
! ...... If the porosity is zero, then set all the parameters to zero
! ----------
! 
         IF(media(n)%Poros < 1.0d-6) THEN
! 
            PoMed(n)%RGrain = 0.0d0           
            PoMed(n)%SArea  = 0.0d0
            PoMed(n)%NVoid  = 0.0d0 
            PoMed(n)%VVoid  = 0.0d0
            PoMed(n)%RVoid  = 0.0d0
! 
            CYCLE DO_NumMedia       ! Continue computatioins for remaining media
! 
         END IF
! 
! ----------
! ...... If the input grain radius = 0 => estimated from the Kozeny-Carman equation
! ----------
! 
         IF(PoMed(n)%RGrain < 1.0d-6) THEN
            PoMed(n)%RGrain =  SQRT( 4.5d1*media(n)%Perm(1)&
     &                                    *(1.0d0-media(n)%Poros)**2&
     &                                    /(media(n)%Poros**3)    &         
     &                             )             
         END IF
! 
! ----------
! ...... Compute the surface area of each grain 
! ...... Temporary use of "PoMed(nmat)%SArea"
! ----------
! 
         PoMed(n)%SArea = 4.0d0*GRPI*PoMed(n)%RGrain&
     &                              *PoMed(n)%RGrain              
! 
! ----------
! ...... Compute the volume of each grain 
! ----------
! 
         VolGrain = PoMed(n)%SArea*PoMed(n)%RGrain/3.0d0             
! 
! ----------
! ...... Compute the number of voids per unit volume 
! ...... For a tetrahedral structure: 1-phi = 0.74 
! ----------
! 
         PoMed(n)%NVoid = (1.0d0-media(n)%Poros)/VolGrain             
!        PoMed(n)%NVoid = 7.4d-1/VolGrain             
! 
! ----------
! ...... Compute the volume of each void 
! ...... For a tetrahedral structure: phi = 0.26 
! ----------
! 
         PoMed(n)%VVoid = media(n)%Poros/PoMed(n)%NVoid             
!        PoMed(n)%VVoid = 2.6d-1/PoMed(n)%NVoid             
! 
! ----------
! ...... Compute the surface area of grains/voids per unit volume 
! ...... Final use of "PoMed(nmat)%SArea"
! ----------
! 
         PoMed(n)%SArea = PoMed(n)%NVoid*PoMed(n)%SArea            
! 
! ----------
! ...... Compute the radius of the interstitial space 
! ----------
! 
         PoMed(n)%RVoid =  1.547d-1*PoMed(n)%RGrain            
! 
! 
      END DO DO_NumMedia
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 5002 FORMAT(5A,I5,8E10.4)
 5004 FORMAT(9E10.4)
 5008 FORMAT(I5,5X,7E10.4)
!
 6000 FORMAT(/,'READ_RockData     1.0    6 January   2004',6X,&
     &         'Read the rock type data from the ROCK block of ',&
     &         'the input data file') 
!
 6001 FORMAT(//,20('ERROR-'),//,T43,&
     &             'S I M U L A T I O N   A B O R T E D',/,&
     &         T12,'The number of input rock types n_i = ',i4,&
     &             ' exceeds the maximum declared number ',&
     &             '�MaxNum_Media� = ',i4,&
     &       /,T50,'CORRECT AND TRY AGAIN',     &
     &       //,20('ERROR-'))
!
 6002 FORMAT(/,' DOMAIN NO.',I3,'     MATERIAL NAME -- ',A5)
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of READ_RockData
!
!
      RETURN
! 
      END SUBROUTINE READ_RockData
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE READ_ParamData
! 
! ...... Modules to be used 
! 
         USE Basic_Param
         USE GenControl_Param
         USE Diffusion_Param
! 
         USE Variable_Arrays
		 USE MPI_PARAM
		 USE MPI_SUBROUTINES
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*       ROUTINE FOR READING THE MAIN COMPUTATIONAL PARAMETERS         *
!*                                                                     *
!*                 Version 1.0 - September 21, 2004                    *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0
      INTEGER :: I,J,N
! 
! -------
! ... Character variables
! -------
! 
      CHARACTER(LEN = 3) :: State_id
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of READ_ParamData
!
!
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .AND. MPI_RANK==0) WRITE(11,6000)                                      
! 
!***********************************************************************
!*                                                                     *
!*       READING THE COMPUTATIONAL PARAMETER DATA: 1st Record          *
!*                                                                     *
!***********************************************************************
! 
      IF(MPI_RANK == 0) READ (100,5002), TEMP_INT(1:29),TEMP_REAL(1:4)

		!MCYC = 10 RPS

!***********************************************************************
!*                                                                     *
!*       READING THE COMPUTATIONAL PARAMETER DATA: 2nd Record          *
!*                                                                     *
!***********************************************************************
! 
    if(MPI_RANK==0)  READ (100,5004), TEMP_REAL(5:8),TEMP_CHAR(1:5),TEMP_REAL(9:11)

!***********************************************************************
!*                                                                     *
!*       READING THE COMPUTATIONAL PARAMETER DATA: 3rd Record          *
!*                                                                     *
!***********************************************************************
! 
     if(MPI_RANK==0) READ (100,5010), TEMP_REAL(12:18),TEMP_CHAR(6:8)
	  
!---***COMMUNICATING READ DATA TO OTHER PROCESSES***
	CALL MPI_BROADCAST(INT_ARRAY=TEMP_INT(1:29),CHAR_ARRAY=TEMP_CHAR(1:8),REAL_ARRAY=TEMP_REAL(1:18),&
						INT_SIZE=29,CHAR_SIZE=8,REAL_SIZE=18,root=0)
!---Assigning the variables
	NOITE = TEMP_INT(1)         ! Maximum number of Newtonian iterations
    KDATA = TEMP_INT(2)         ! A flag determining the amount of output  
    MCYC  = TEMP_INT(3)         ! Maximum number of time steps
    MSEC  = TEMP_INT(4)         ! Maximum execution time (in CPU seconds)
    MCYPR = TEMP_INT(5)			! Print-outs every MCYPR time steps
    MOP(1:24) = TEMP_INT(6:29)  ! List of the computational choice parameters
    DIFF0 = TEMP_REAL(1)        ! Base diffudion coefficient
    TEXP  = TEMP_REAL(2)        ! Temperature-related exponent of DIFF0
    BE    = TEMP_REAL(3)        ! Additional diffusion parameter
	
	
	TSTART= TEMP_REAL(5)  		! Time at the beginning of the simulation (sec)
    TIMAX = TEMP_REAL(6)   		! Time at the end of the simulation (sec)  
    DELTEN= TEMP_REAL(7)  		! Initial time step size (sec)
    DELTMX= TEMP_REAL(8) 		! Maximum time step size (sec)
    WRITE(ELST,*),TEMP_CHAR(1:5)! Name of tracked element 
    GF	  = TEMP_REAL(9) 		! Acceleration of gravity
    REDLT = TEMP_REAL(10)      ! Maximum number of Newtonian iterations for doubling the time step size
    SCALE = TEMP_REAL(11)       ! Scale of mesh (geometrical system only)
	
	RE1	  = TEMP_REAL(12)   		  ! Convergence criterion #1 for Newtonian iterations
    RE2   = TEMP_REAL(13)   		  ! Convergence criterion #2 for Newtonian iterations
    U_p   = TEMP_REAL(14)    		  ! Additional info
    WUP   = TEMP_REAL(15) 			  ! Upstream weighing factor
    WNR	  = TEMP_REAL(16)			  ! Weighing factor
    DFAC  = TEMP_REAL(17)    		  ! Increment for estimation of numerical derivative
    FOR	  = TEMP_REAL(18)			  ! Additional info
    write(State_id,'(3A1)'), TEMP_CHAR(6:8) ! State identifier: initial conditions applied uniformly over the domain
!-----------------------------------------
	
!---
! ... Reset MOP(14) to 1 (T-dependent function) for hydrate simulation  
! 
      IF(EOS_name(1:7) == 'HYDRATE' .AND. MOP(14) /= 1) MOP(14) = 1   
! 
! ... Allow more than 10000 time steps 
! 
      IF(MCYC < 0) MCYC = 10000*ABS(MCYC)   
! 
! 
! --------
! ... For DELTEN < 0, read list of specific DELTs to be used         
! --------
! 
      NDLT = 0
! 
      IF_SpcDELT: IF(DELTEN < 0.0d0) THEN
! 
                     NDLT = -INT(DELTEN)
                     DO N=1,NDLT
                        IF(MPI_RANK==0) READ (100,5006),TEMP_REAL(1:8)
                     END DO
					 CALL MPI_BROADCAST(REAL_ARRAY=TEMP_REAL(1:8),REAL_SIZE=8,root=0)
					 DO J=1,8
						DLT(J+8*(N-1))=TEMP_REAL(J)
					 END DO
! 
                     DELTEN = DLT(1)
! 
                  END IF IF_SpcDELT 
! 

! ----------
! ... Determine the state index
! ----------
! 
      Case_State: SELECT CASE(State_id)
! 
! ... Single phase: Gas           
!    
      CASE('Gas')
!    
         GenStIndx = 1                     
! 
! ... Single phase: Aqueous           
!    
      CASE('Aqu')
!    
         GenStIndx = 2                     
! 
! ... Two phases: Aqueous+Gas   
!    
      CASE('AqG')
!    
         GenStIndx = 3                     
! 
! ... Two phases: Ice+Gas   
!    
      CASE('IcG')
!    
         GenStIndx = 4                     
! 
! ... Two phases: Aqueous+Hydrate    
!    
      CASE('AqH')
!    
         GenStIndx = 5                     
! 
! ... Two phases: Ice+Hydrate    
!    
      CASE('IcH')
!    
         GenStIndx = 6                     
! 
! ... Three phases: Aqueous+Hydrate+Gas    
!    
      CASE('AGH')
! 
         GenStIndx = 7                     
! 
! ... Three phases: Aqueous+Ice+Gas    
!    
      CASE('AIG')
! 
         GenStIndx = 8                     
! 
! ... Three phases: Aqueous+Ice+Hydrate    
!    
      CASE('AIH')
! 
         GenStIndx = 9                     
! 
! ... Three phases: Gas+Ice+Hydrate    
!    
      CASE('IGH')
! 
         GenStIndx = 10                     
! 
! ... Quadruple point: Gas+Ice+Hydrate+Aqueous    
!    
      CASE('QuP')
! 
         GenStIndx = 11                     
!    
! 
! ... Otherwise: Error ...    
!    
      CASE DEFAULT 
!    
         WRITE(6,6100) State_id
         STOP
!    
      END SELECT Case_State
! 
! --------
! ... Assign default values        
! --------
! 
      IF(NOITE == 0) NOITE = 8
      IF(KDATA == 0) KDATA = 1
      IF(MCYPR == 0) MCYPR = 1
! 
!      IF(TEXP  == 0.0d0) TEXP  = 1.8d0
      IF(REDLT == 0.0d0) REDLT = 4.0d0
      IF(SCALE == 0.0d0) SCALE = 1.0d0
! 
      IF(RE1 == 0.0d0) RE1 = 1.0d-5
      IF(RE2 == 0.0d0) RE2 = 1.0d0
      IF(FOR == 0.0d0) FOR = 1.0d0
! 
      IF(WUP <= 0.0d0) WUP = 1.0d0
      IF(WNR == 0.0d0) WNR = 1.0d0
! 
      IF(U_p  == 0.0d0) U_p  = 1.0d-1
! 
! --------
! ... Initialize some parameters        
! --------
! 
      KCYC  = 0        ! Number of elapsed time steps
      ITER  = 0        ! Number of Newtonian iterations in current Dt
      ITERC = 0        ! Cumulative number of Newtonian iterations
      TIMIN = TSTART   
! 
!***********************************************************************
!*                                                                     *
!*        READING THE INITIAL VALUES OF THE PRIMARY VARIABLES          *
!*                                                                     *
!***********************************************************************
! 
      IF(MPI_RANK==0) READ (100,5020), (DEP(i),i=1,NK1)
	  CALL MPI_BROADCAST(REAL_ARRAY=DEP(1:NK1),REAL_SIZE=NK1,root=0)
	  
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 5002 FORMAT(2I2,3I4,24I1,4E10.4)
 5004 FORMAT(4E10.4,5A,5X,3E10.4)
!
 5006 FORMAT(8E10.4)
 5010 FORMAT(7E10.4,2x,3A)
 5020 FORMAT(4E20.13)
!
 6000 FORMAT(/,'READ_ParamData    1.0   21 September 2004',6X,&
     &         'Read the computational parameter data from the ',&
     &         'PARAM block of the input data file') 
!
 6100 FORMAT(//,20('ERROR-'),//,T43,&
     &             'S I M U L A T I O N   A B O R T E D',/,&
     &         T25,'The state identifier of uniform initial ',&
     &             'conditions "State_id_U" = "',a3,'"',/,&
     &         T31,'This is not an available option for this ',&
     &             'Equation of State',&
     &       /,T50,'CORRECT AND TRY AGAIN',     &
     &       //,20('ERROR-'))
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of READ_ParamData
!
!
      RETURN
! 
      END SUBROUTINE READ_ParamData
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE READ_GnerData
! 
! ...... Modules to be used 
! 
         USE Basic_Param
! 
         USE Q_Arrays
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*     ROUTINE FOR READING THE GENERATION (SOURCE AND SINK) DATA       *
!*         DIRECTLY FROM THE "HydrateResSim" INPUT DATA BLOCK          *
!*                                                                     *
!*                  Version 1.0 - September 11, 2004                   *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: GX,EX,HX,Inh_WInjMF
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: NE,NS,N1,N2,NSEQ,NSEQ1,NADD,NADS,LTAB
      INTEGER :: n,i,k,LTABA,ier
! 
! -------
! ... Character variables
! -------
! 
      CHARACTER(LEN = 1) :: ITAB
      CHARACTER(LEN = 3) :: EL,SL
      CHARACTER(LEN = 4) :: SS_TypX
      CHARACTER(LEN = 6) :: EL6_0
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of READ_GnerData
!
!
      ICALL = ICALL+1                                                     
      IF(ICALL == 1) WRITE(11,6000)                                      
! 
! --------
! ... Printing headings and attaching GENER file        
! --------
! 
      PRINT 5001
      REWIND 3
!
      PRINT 6002
      WRITE(3,6004)
!
! ... Seeting the source/sink counter to zero
!
      NOGN  = 0
! 
!***********************************************************************
!*                                                                     *
!*                    READING THE SOURCE/SINK DATA                     *
!*                                                                     *
!***********************************************************************
!
 
 1000 IF_ElCh: IF(No_CEN /= 8) THEN
!
! ............... Read data for 5-Character elements
!
                  READ (100,5004), EL,    &     ! 3-Character component of the name of the source/sink element  
     &                       NE,   &      ! 2-Digit number component of the name of the source/sink element
     &                       SL,   &     ! 3-Character component of the source/sink name 
     &                       NS,    &     ! 2-Digit number component of the source/sink name
     &                       NSEQ,  &     ! Number of additional source/sink elements with the same attributes
     &                       NADD,  &     ! Increment of the source/sink element # 
     &                       NADS,  &     ! Increment of the source/sink # 
     &                       LTAB,  &     ! Additional info
     &                       SS_TypX, &   ! Type of source/sink
     &                       ITAB,&      ! Additional info
     &                       GX,   &      ! Generation rate (kg/s)
     &                       EX,  &       ! Corresponding specific enthalpy (J/kg, Injection ONLY !!!)
     &                       HX, &       ! Layer thickness (for deliverability only)
     &                       Inh_WInjMF  ! Mass fraction of inhibitor in injected H2O
               ELSE
!
! ............... Read data for 8-Character elements
!
                  READ (100,5006), EL6_0,   &   ! 6-Character component of the name of the source/sink element
     &                       NE,    &     ! All other parameters as previously defined
     &                       SL,NS,&
     &                       LTAB,SS_TypX,ITAB,GX,EX,HX,Inh_WInjMF
!
                  EL   = EL6_0(1:3)
                  NSEQ = 0
                  NADD = 0
                  NADS = 0
               END IF IF_ElCh
! 
! --------
! ... End of the source/sink records - No info on correlation to elements        
! --------
! 
      IF_El1End: IF(EL == '   ' .AND. NE == 0) THEN
! 
                    WRITE(3,5001)
! 
! ................. The source/sink data read-in is completed 
! 
                    RETURN
! 
                 END IF IF_El1End
! 
! --------
! ... End of the source/sink records - Info on correlation to elements        
! --------
! 
      IF_El2End: IF(EL == '+++') THEN
! 
                    WRITE(3,5002)
! 
! ................. Read the source/sink-associated element numbers 
! 
                    IF(No_CEN /= 8) THEN
                       READ (100,5010),    (SS(n)%el_num,n=1,NOGN)
                       WRITE(3,5010) (SS(n)%el_num,n=1,NOGN)
                    ELSE
                       READ (100,5015),    (SS(n)%el_num,n=1,NOGN)
                       WRITE(3,5015) (SS(n)%el_num,n=1,NOGN)
                    END IF
! 
! ................. The source/sink data read-in is completed 
! 
                    WRITE(3,5001)
                    RETURN
! 
                 END IF IF_El2End
!
      NSEQ1 = NSEQ+1
      IF(LTAB == 0) LTAB=1
! 
!***********************************************************************
!*                                                                     *
!*         WRITING THE CONNECTION DATA INTO THE 'GENER' FILE           *
!*                                                                     *
!***********************************************************************
! 
      DO_Seq: DO i=1,NSEQ1
!
! ...... Count sources/sinks
!
         NOGN = NOGN+1
!
! ...... Determine number component of element names 
!
         N1 = NE+(i-1)*NADD
         N2 = NS+(i-1)*NADS
!
! ...... Writing the element info into file GENER
!
         IF(No_CEN /= 8) THEN
            WRITE(3,5008) EL,N1,SL,N2,LTAB,SS_TypX,ITAB,&
     &                    GX,EX,HX,Inh_WInjMF
         ELSE
            WRITE(3,5006) EL6_0,N1,SL,N2,&
     &                    GX,EX,HX,Inh_WInjMF
         END IF
! 
         LTABA = ABS(LTAB) 
! 
!***********************************************************************
!*                                                                     *
!* READING TABULAR DATA FROM THE 'GENER' DATA BLOCK IN THE INPUT FILE  *
!*                                                                     *
!***********************************************************************
! 
         IF_Table: IF(LTABA > 1 .AND. SS_TypX /= 'DELV') THEN
! 
! -------------
! ......... Allocate memory to the temporary arrays F1, F2, F3    
! -------------
! 
            ALLOCATE(F1(LTABA),F2(LTABA),F3(LTABA), STAT = ier)
! 
! ......... Print explanatory comments  
! 
            IF(ier == 0) THEN
               WRITE(50,6101) 
            ELSE
               PRINT 6102 
               WRITE(50,6102) 
               STOP
            END IF
! 
! -------------
! ......... Reading the data    
! -------------
! 
            IF_I2: IF(I < 2) THEN
! 
               READ (100,5020), (F1(k),k=1,LTABA)   ! Tabular data of times
               READ (100,5020), (F2(k),k=1,LTABA)   ! Tabular data of injection/production rates
               IF(ITAB /= ' ') THEN
                  READ (100,5020),(F3(k),k=1,LTABA) ! Tabular data of corresponding specific enthalpy
               END IF
! 
            END IF IF_I2
! 
!***********************************************************************
!*                                                                     *
!*             WRITING TABULAR DATA INTO THE 'GENER' FILE              *
!*                                                                     *
!***********************************************************************
! 
            WRITE(3,5020) (F1(k),k=1,LTABA)
            WRITE(3,5020) (F2(k),k=1,LTABA)
            IF(ITAB /= ' ') WRITE(3,5020) (F3(k),k=1,LTABA)
! 
! -------------
! ......... Deallocate memory from the temporary arrays F1, F2, F3    
! -------------
! 
            DEALLOCATE(F1,F2,F3, STAT = ier)
! 
! ......... Print explanatory comments  
! 
            IF(ier == 0) THEN
               WRITE(50,6103) 
            ELSE
               PRINT 6104 
               WRITE(50,6104) 
               STOP
            END IF
! 
         END IF IF_Table
!
! <<<                      
! <<<           
! <<<
!
      END DO DO_Seq
! 
! -----------
! ...... Continue reading source/sink data
! -----------
! 
      GO TO 1000
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 5001 FORMAT('     ')
 5002 FORMAT('+++  ')
!
 5004 FORMAT(A3,I2,A3,I2,4I5,5X,A4,A1,4E10.4)
 5006 FORMAT(A6,I2.2,A3,I2,12X,I5,5X,A4,A1,4E10.4)
! 
 5008 FORMAT(A3,I2.2,A3,I2,15X,I5,5X,A4,A1,4E10.4)
! 
 5010 FORMAT(16I5)
 5015 FORMAT(10I8)
! 
 5020 FORMAT(4E14.7)
!
 6000 FORMAT(/,'READ_GnerData     1.0   11 September 2004',6X,&
     &         'Read the source/sink data from the GENER block of ',&
     &         'the input data file') 
!
 6002 FORMAT(' WRITE FILE *GENER* FROM INPUT DATA')
 6004 FORMAT('GENER')
!
 6101 FORMAT(T2,'Memory allocation to arrays F1,F2,F3 in subroutine ',&
     &          '�READ_GnerData� was successful')
 6102 FORMAT(//,20('ERROR-'),//,&
     &       T2,'Memory allocation to arrays F1,F2,F3 in subroutine ',&
     &          '�READ_GnerData� was unsuccessful',//,&
     &       T2,'!!!!!!!    THE EXECUTION WAS STOPPED    !!!!!!!',&
     &       //,20('ERROR-'))
!
 6103 FORMAT(T2,'Memory deallocation to arrays F1,F2,F3 in subroutine ',&
     &          '�READ_GnerData� was successful')
 6104 FORMAT(//,20('ERROR-'),//,&
     &       T2,'Memory deallocation to arrays F1,F2,F3 in subroutine ',&
     &          '�READ_GnerData� was unsuccessful',//,&
     &       T2,'!!!!!!!    THE EXECUTION WAS STOPPED    !!!!!!!',&
     &       //,20('ERROR-'))
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of READ_GnerData
!
!
      RETURN
! 
      END SUBROUTINE READ_GnerData
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE READ_InConData
! 
! ...... Modules to be used 
! 
         USE Basic_Param
		 USE MPI_PARAM
! 
         USE Variable_Arrays,   ONLY: XX
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*          ROUTINE FOR READING THE INITIAL CONDITION DATA             *
!*        DIRECTLY FROM THE "HydrateResSim" INPUT DATA BLOCK           *
!*                                                                     *
!*                 Version 1.0 - September 26, 2004                    *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: PORX,TSTX,TIMINX
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0,IERR
! 
      INTEGER :: NE,N1,NSEQ,NSEQ1,NADD,KCYCX,ITERCX,NMX
      INTEGER :: I,J
! 
! -------
! ... Character variables
! -------
! 
      CHARACTER(LEN = 3) :: EL,State_id
      CHARACTER(LEN = 6) :: EL6_0
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of READ_InConData
!
!
      ICALL = ICALL+1                                                     
      IF(ICALL == 1) WRITE(11,6000)                                      
! 
! --------
! ... Printing headings and attaching the INCON file        
! --------
! 
      PRINT 5001
      PRINT 6002
!
      CALL MPI_FILE_SEEK(IM_1,ZeroOffset,MPI_SEEK_SET,IERR)

	  WRITE(FBUFF,6004)
	  CALL MPI_FILE_WRITE(IM_1,FBUFF(1:5)//NLINE,6,MPI_CHARACTER,MPI_STATUS_IGNORE,IERR)
      
! 
!***********************************************************************
!*                                                                     *
!*            READING THE INITIAL CONDITION DATA: 1st record           *
!*                                                                     *
!***********************************************************************
! 
 1000 IF_ElCh: IF(No_CEN /= 8) THEN
!
! ............... Read initial condition data for 5-Character elements
!
                  READ (100,5004), EL,   &    ! 3-Character component of the element name   
     &                       NE,   &    ! 2-Digit number component of the element name
     &                       NSEQ,  &   ! Number of additional elements with the same attributes
     &                       NADD,  &   ! Increment of the element # 
     &                       PORX,  &   ! Element porosity
     &                       State_Id  ! State index
               ELSE
!
! ............... Read initial condition data for 8-Character elements
!
                  READ (100,5006), EL6_0,   &          ! 6-Character component of the element name
     &                       NE,PORX,State_Id
!
                  EL   = EL6_0(1:3)
                  NSEQ = 0
                  NADD = 0
               END IF IF_ElCh
! 
! --------
! ... End of the initial condition records - No continuation info         
! --------
! 
      IF_El1End: IF(EL == '   ' .AND. NE == 0) THEN
! 
					
					  WRITE(FBUFF,5001)
					  CALL MPI_FILE_WRITE(IM_1,FBUFF(1:5)//NLINE,6,MPI_CHARACTER,MPI_STATUS_IGNORE,IERR)
! 
! ................. The initial condition data read-in is completed 
! 
                    RETURN
! 
                 END IF IF_El1End
! 
! --------
! ... End of the initial condition records - Info on continuation        
! --------
! 
      IF_El2End: IF(EL == '+++') THEN
! 
			
				  WRITE(FBUFF,5002)
				  CALL MPI_FILE_WRITE(IM_1,FBUFF(1:5)//NLINE,6,MPI_CHARACTER,MPI_STATUS_IGNORE,IERR)
! 
					
! 
! ................. Read the continuation data 
! 
                    READ (100,5009), KCYCX,  &  ! Number of completed timesteps
     &                         ITERCX, &  ! Number of completed Newtonian iterations
     &                         NMX,   &   ! Number of ...
     &                         TSTX,  &   ! Time at ...
     &                         TIMINX    ! Time at beginning of continuation simulation
! 
! ................. Write the continuation data into file INCON
! 
				   WRITE(FBUFF,5009),KCYCX,ITERCX,NMX,TSTX,TIMINX
				   CALL MPI_FILE_WRITE(IM_1,FBUFF(1:45)//NLINE,46,MPI_CHARACTER,MPI_STATUS_IGNORE,IERR)
! 
! 
! ................. The initial condition data read-in is completed 
! 
                    
				   WRITE(FBUFF,5001)
				   CALL MPI_FILE_WRITE(IM_1,FBUFF(1:5)//NLINE,6,MPI_CHARACTER,MPI_STATUS_IGNORE,IERR)
! 
                    RETURN
! 
                 END IF IF_El2End
! 
!***********************************************************************
!*                                                                     *
!* READING THE INITIAL CONDITION DATA: 2nd record (Primary Variables)  *
!*                                                                     *
!***********************************************************************
! 
      READ (100,5010), (xx(i),i=1,NK1)
! 
      NSEQ1 = NSEQ+1
! 
!***********************************************************************
!*                                                                     *
!*      WRITING THE INITIAL CONDITION DATA INTO THE �INCON� FILE       *
!*                                                                     *
!***********************************************************************
! 
      DO_Seq: DO I=1,NSEQ1
!
! ...... Determine number component of element name 
!
         N1 = NE+(I-1)*NADD
!
! ...... Writing the element initial condition info into file INCON
!
         IF(No_CEN /= 8) THEN
		   WRITE(FBUFF,6006) EL,N1,PORX,State_id
		   CALL MPI_FILE_WRITE(IM_1,FBUFF(1:35)//NLINE,36,MPI_CHARACTER,MPI_STATUS_IGNORE,IERR)
! 
         ELSE
		   WRITE(FBUFF,6008) EL6_0,N1,PORX,State_id
		   CALL MPI_FILE_WRITE(IM_1,FBUFF(1:35)//NLINE,36,MPI_CHARACTER,MPI_STATUS_IGNORE,IERR)
         END IF
! 
		   WRITE(FBUFF,5010) (xx(j),j=1,NK1)
		   CALL MPI_FILE_WRITE(IM_1,FBUFF(1:80)//NLINE,81,MPI_CHARACTER,MPI_STATUS_IGNORE,IERR)
! 
      END DO DO_Seq
! 
!
! -----------
! ...... Continue reading initial condition data
! -----------
! 
      GO TO 1000
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 5001 FORMAT('     ')
 5002 FORMAT('+++  ')
!
 5004 FORMAT(A3,I2,2I5,E15.8,2x,a3)
 5006 FORMAT(A6,I2, 7X,E15.8,2x,a3)
! 
 5009 FORMAT(3I5,2E15.8)
 5010 FORMAT(4E20.13)
!
 6000 FORMAT(/,'READ_InConData    1.0   26 September 2004',6X,&
     &         'Read the initial condition data from the INCON ',&
     &         'block of the input data file') 
!
 6002 FORMAT(' WRITE FILE *INCON* FROM INPUT DATA')
 6004 FORMAT('INCON')
!
 6006 FORMAT(A3,I2,10X,E15.8,2x,a3)
 6008 FORMAT(A6,I2.2, 7X,E15.8,2x,a3)
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of READ_InConData
!
!
      RETURN
! 
      END SUBROUTINE READ_InConData
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE READ_InDomData(N_DomInit)
! 
! ...... Modules to be used 
! 
         USE Basic_Param
! 
         USE PFMedProp_Arrays, ONLY: XIN,Rk_name,StateI
		 USE MPI_PARAM
		 USE MPI_SUBROUTINES
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*     ROUTINE FOR READING DOMAIN-SPECIFIC INITIAL CONDITION DATA      *
!*         DIRECTLY FROM THE "HydrateResSim" INPUT DATA BLOCK          *
!*                                                                     *
!*                 Version 1.0 - September 26, 2004                    *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(OUT) :: N_DomInit
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: I
! 
! -------
! ... Character variables
! -------
! 
      CHARACTER(LEN = 3) :: State_id
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of READ_InDomData
!
!
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .AND. MPI_RANK==0) WRITE(11,6000)                                      
! 
! --------
! ... Initializations  
! --------
! 
      N_DomInit = 0
! 
      StateI    = 0 ! CAREFUL! Whole array operation
! 
!***********************************************************************
!*                                                                     *
!*            READING THE INITIAL CONDITION DATA: 1st record           *
!*                                                                     *
!***********************************************************************
! 
	DO WHILE(.TRUE.)
	  IF(MPI_RANK==0) READ (100,5001), TEMP_CHAR(1:8)
	  CALL MPI_BROADCAST(CHAR_ARRAY=TEMP_CHAR(1:2),CHAR_SIZE=2,root=0)
      write(Rk_name(N_DomInit+1),*),TEMP_CHAR(1:5)
	  write(State_id,*),TEMP_CHAR(6:8)
	  
! 
! --------
! ... End of the domain-specific initial condition records         
! --------
! 
      IF_DomEnd: IF(Rk_name(N_DomInit+1) == '   ' ) THEN
! 
! .... The initial condition data read-in is completed 
! 
         RETURN
! 
      END IF IF_DomEnd
! 
!***********************************************************************
!*                                                                     *
!*            DETERMINING THE DOMAIN-SPECIFIC INITIAL STATE            *
!*                                                                     *
!***********************************************************************
! 
      Case_State: SELECT CASE(State_id)
! 
! ... Single phase: Gas           
!    
      CASE('Gas')
!    
         StateI(N_DomInit+1) = 1                     
! 
! ... Single phase: Aqueous           
!    
      CASE('Aqu')
!    
         StateI(N_DomInit+1) = 2                     
! 
! ... Two phases: Aqueous+Gas   
!    
      CASE('AqG')
!    
         StateI(N_DomInit+1) = 3                     
! 
! ... Two phases: Ice+Gas   
!    
      CASE('IcG')
!    
         StateI(N_DomInit+1) = 4                     
! 
! ... Two phases: Aqueous+Hydrate    
!    
      CASE('AqH')
!    
         StateI(N_DomInit+1) = 5                     
! 
! ... Two phases: Ice+Hydrate    
!    
      CASE('IcH')
!    
         StateI(N_DomInit+1) = 6                     
! 
! ... Three phases: Aqueous+Hydrate+Gas    
!    
      CASE('AGH')
! 
         StateI(N_DomInit+1) = 7                     
! 
! ... Three phases: Aqueous+Ice+Gas    
!    
      CASE('AIG')
! 
         StateI(N_DomInit+1) = 8                     
! 
! ... Three phases: Aqueous+Ice+Hydrate    
!    
      CASE('AIH')
! 
         StateI(N_DomInit+1) = 9                     
! 
! ... Three phases: Gas+Ice+Hydrate    
!    
      CASE('IGH')
! 
         StateI(N_DomInit+1) = 10                     
! 
! ... Quadruple point: Gas+Ice+Hydrate+Aqueous    
!    
      CASE('QuP')
! 
         StateI(N_DomInit+1) = 11                     
!    
! 
! ... Otherwise: Error ...    
!    
      CASE DEFAULT 
!    
         IF(MPI_RANK==0) THEN
			 WRITE(6,6100) Rk_name(N_DomInit+1),State_id
			 STOP
		 END IF
!    
      END SELECT Case_State
! 
! ... Counting the number of domains for which initial conditions were provided 
! 
      N_DomInit = N_DomInit+1
! 
!***********************************************************************
!*                                                                     *
!*         READING THE DOMAIN-SPECIFIC INITIAL CONDITION DATA          *
!*                   2nd record (Primary Variables)                    *
!*                                                                     *
!***********************************************************************
! 
      IF(MPI_RANK==0)  READ (100,5010), (XIN(i,N_DomInit), i=1,NK1)
	  CALL MPI_BROADCAST(REAL_ARRAY=XIN(1:NK1,N_DomInit),real_size=NK1,root=0)
! 
! -----------
! ...... Continue reading initial condition data
! -----------
! 
    END DO
	  
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 5001 FORMAT(5A,2x,3a)
 5010 FORMAT(4E20.13)
!
 6000 FORMAT(/,'READ_InDomData    1.0   26 September 2004',6X,&
     &         'Read the domain-specific initial conditions from ',&
     &         'the INDOM block of the input data file') 
!
 6100 FORMAT(//,20('ERROR-'),//,T43,&
     &             'S I M U L A T I O N   A B O R T E D',/,&
     &         T18,'In domain "',a5,'", the state identifier of ',&
     &             'initial conditions "State_id_D" = "',a3,'"',/,&
     &         T31,'This is not an available option for this ',&
     &             'Equation of State',&
     &       /,T50,'CORRECT AND TRY AGAIN',    & 
     &       //,20('ERROR-'))
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of READ_InDomData
!
!
      RETURN
! 
      END SUBROUTINE READ_InDomData
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE READ_TimeData
! 
! ...... Modules to be used 
! 
         USE GenControl_Param, ONLY: ITI,DELAF,TIS
		 USE MPI_PARAM
		 USE MPI_SUBROUTINES
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*             ROUTINE FOR READING THE PRINT-OUT TIME DATA             *
!*         DIRECTLY FROM THE "HydrateResSim" INPUT DATA BLOCK          *
!*                                                                     *
!*                   Version 1.0 - January 6, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: TINTER
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0
      INTEGER :: ITE,ITI1,I
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of READ_TimeData
!
!
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .AND. MPI_RANK==0) WRITE(11,6000)                                      
! 
!***********************************************************************
!*                                                                     *
!*      READING THE TIME DATA: 1st record - Basic time parameters      *
!*                                                                     *
!***********************************************************************
! 
      IF(MPI_RANK==0) READ (100,5001),TEMP_INT(1:2),TEMP_REAL(1:2)
	  CALL MPI_BROADCAST(REAL_ARRAY=TEMP_REAL,INT_ARRAY=TEMP_INT,&
						real_size=2,int_size=2,root=0)
	  ITI = TEMP_INT(1)     ! Number of provided print-out times (ITE<=100)
      ITE = TEMP_INT(2)     ! Number of desired print-out times  (ITI<=ITE<=100) 
      DELAF = TEMP_REAL(1)  ! Maximum Dt size after any of the proscribed times have been reached (sec) 
      TINTER = TEMP_REAL(2) ! Time increment for times ITI,ITI+1,ITI+2,... 
! 
!***********************************************************************
!*                                                                     *
!*         READING THE TIME DATA: 2nd record - Print-out times         *
!*                                                                     *
!***********************************************************************
! 
      IF(MPI_RANK==0) READ (100,5002), (TIS(I),I=1,ITI) 
	  CALL MPI_BROADCAST(REAL_ARRAY=TIS,REAL_SIZE=ITI,root=0)
! 
! ... Reading of print-out times is completed 
! 
      IF(ITE <= ITI .OR. TINTER <= 0.0d0) RETURN
! 
!***********************************************************************
!*                                                                     *
!*        ASSIGNING INCREMENTED PRINT-OUT TIMES FOR ITE > ITI          *
!*                                                                     *
!***********************************************************************
! 
      ITI1 = ITI+1
! 
      DO I=ITI1,ITE
         TIS(I) = TIS(I-1)+TINTER
      END DO
!      
! ... Reset ITI      
!      
      ITI = ITE
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 5001 FORMAT(2I5,3E10.4)
 5002 FORMAT(8E10.4)
!
 6000 FORMAT(/,'READ_TimeData     1.0    7 January   2004',6X,&
     &         'Read the print out times from the TIMES ',&
     &         'block of the input data file') 
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of READ_TimeData
!
!
      RETURN
! 
      END SUBROUTINE READ_TimeData
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE PermMod_Init
! 
! ...... Modules to be used 
! 
         USE Basic_Param
		 USE MPI_Param
         USE GenControl_Param
! 
         USE Element_Arrays
         USE PFMedProp_Arrays
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*     ROUTINE INITIALIZING BLOCK-BY-BLOCK PERMEABILITY MODIFIERS      *
!*                                                                     *
!*                   Version 1.00 - April 2, 2004                      *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: s,sran,pim
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: n,i,im
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: DomSEED_Exists
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of PermMod_Init
!
!
      ICALL=ICALL+1
	  if(MPI_RANK==0) THEN
		  IF(ICALL == 1) WRITE(11,6000)
	! 
	! -------
	! ... Print supporting information 
	! -------
	! 
		  print 6002, ' '
		  print 6004
		  print 6006
		  print 6008
	  END IF
! 
! -------
! ... Initialize the flag denoting block-by-block permeability modification
! -------
! 
      PermModif = 'NONE'
! 
! -------
! ... Determine if a rock/domain SEED is available in the input file
! -------
! 
      DomSEED_Exists = .FALSE.
! 
      DO n=1,N_PFmedia
! 
         IF(mat(n) == 'SEED ') THEN
            DomSEED_Exists = .TRUE.
            EXIT
         END IF
! 
      END DO
! 
!***********************************************************************
!*                                                                     *
!*            When a rock/domain SEED is not present ...               *
!*                                                                     *
!***********************************************************************
! 
      IF_NoSEED: IF(DomSEED_Exists .EQV. .FALSE. ) THEN
! 
! ...... Print clarifying information into output file
! 
	    if(MPI_RANK==0) THEN
         print 6010
         print 6012
         print 6014
         print 6016
         print 6020
		end if
! 
         RETURN          ! Done!  Exit the routine
! 
      END IF IF_NoSEED
! 
!***********************************************************************
!*                                                                     *
!*              When a rock/domain SEED is present ...                 *
!*                                                                     *
!***********************************************************************
! 
     
	    if(MPI_RANK==0) THEN
			print 6022
! 
! -------
! ... Print the permeability modifier information from SEED
! -------
! 
		  print 6024, media(n)%DensG
		  print 6026, media(n)%Poros
		  print 6028, media(n)%Perm(1)
		  print 6030, media(n)%Perm(2)
	    end if
! 
!***********************************************************************
!*                                                                     *
!*            Determine the type of permeability modifier              *
!*                                                                     *
!***********************************************************************
! 
! 
! -------
! ... Use "linear" permeability modifiers
! -------
! 
      IF_PerModType: IF(media(n)%DensG /= 0.0d0) THEN         
! 
         PermModif = 'LINR'           ! Reset the "PermModif" indicator
         s = media(n)%DensG
! 
! -------
! ... Use "logarithmic" permeability modifiers
! -------
! 
      ELSE IF(media(n)%Poros /= 0.0d0) THEN   
! 
         PermModif = 'LOGR'           ! Reset the "PermModif" indicator
         s = media(n)%Poros
! 
! -------
! ... Use externally supplied PM-data as permeability modifiers
! -------
! 
      ELSE                            
!                                     
         PermModif = 'EXTR'           ! Reset the "PermModif" indicator
         if(MPI_RANK==0) print 6032
         GO TO 1000
!                                     
      END IF IF_PerModType
! 
!***********************************************************************
!*                                                                     *
!*           Using optional scale factors provided in SEED             *
!*                                                                     *
!***********************************************************************
! 
      sran = 1.0d0
      IF(media(n)%Perm(1) /= 0.0d0) sran = media(n)%Perm(1)
      IF(MPI_RANK==0) print 6034, nel,s,sran

      IF(PermModif == 'LINR' .AND. MPI_RANK==0) print 6036  ! Print info for linear multipliers
      IF(PermModif == 'LOGR' .AND. MPI_RANK==0) print 6038  ! Print info for logarithmic multipliers
! 
!***********************************************************************
!*                                                                     *
!*   Generating random numbers for use in permeability multipliers     *
!*                                                                     *
!***********************************************************************
! 
      DO i=1,nel
!
         CALL RANDOM_NUMBER(s)
!
         IF(PermModif == 'LINR') pm(i) = sran*s
         IF(PermModif == 'LOGR') pm(i) = exp(-sran*s)
! 
      END DO
! 
!***********************************************************************
!*                                                                     *
!*          When a shift in the multiplier value is applied            *
!*                                                                     *
!***********************************************************************
! 
 1000 IF(media(n)%Perm(2) /= 0.0d0) THEN
! 
! ----------
! ...... CAREFUL - Whole array operations 
! ----------
! 
         pm(1:NEL) = pm(1:NEL)-media(1:NEL)%Perm(2) ! Apply shift
!
         im = COUNT(pm(1:NEL) < 0)                  ! Counting negative elements in array 'pm'
!
         WHERE(pm < 0.0d0) pm = 0.0d0               ! For pm < 0, reset pm to 0 (impermeable)
!
         pim = 1.0d2*float(im)/float(nel)           ! Compute % impermeable cells
         if(MPI_RANK==0) print 6040, nel,im,pim                     ! Print info
!c
      END IF
! 
! -------
! ... Option for printing additional information (for MOP(3) > 8)
! -------
! 
      IF(mop(3) >= 8 .AND. MPI_RANK==0) THEN
         print 6042, (pm(i),i=1,nel)
         print 6044
      END IF
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'PermMod_Init      1.0    2 April     2004',6X,&
     &         'Initialize block-by-block permeability modifiers')
!
 6002 format(A1,/,' ',131('&')/' ',33('&'),&
     x'   Summary of capabilities for',&
     x' random permeability modification   ',32('&')/' ',&
     x131('&')//' Modification of absolute permeability on a grid',&
     x' block-by-grid block basis will be made when a domain',&
     x' "SEED " is present in data'/' block "ROCKS", as follows.'/&
     x30X,'      k    --->    k = k*m'//&
     x' Here, k is the absolute permeability specified for the',&
     x' reservoir domain to which the grid block belongs.',&
     x' Parameter m is a'/' "permeability modifier" which can be',&
     x' internally generated or externally prescribed by the',&
     x' user on a block-by-block basis.'/)
!
 6004 format(' When permeability modification',&
     x' is in effect, the strength of capillary pressure',&
     x' will, following Leverett (1941), automatically be'/&
     x' scaled as   Pcap ---> Pcap = Pcap/SQRT(m).'//&
     x' User-supplied permeability modifiers have to be',&
     x' entered as parameter "PMX" in columns 41-50 of an',&
     x' ELEMEnt record.'/&
     x' Permeability modification options are selected through',&
     x' parameters in data block "ROCKS".'/)
!
 6006 format(' ',131('&')/&
     x' Summary of available permeability modification options'/&
     x'          (with s - random number between 0 and 1;',&
     x' PMX - user-supplied modifiers in data block "ELEME"):'/&
     x'      (1) externally supplied:        m = PMX - PER(2)'/&
     x'      (2) "linear"      (DROK.ne.0):  m = PER(1) * s',&
     x' - PER(2)'/&
     x'      (3) "logarithmic" (DROK.eq.0):  m = exp(',&
     x'- PER(1) * s) - PER(2)'/' ',131('&')/)
!
 6008 format(12X,'&&&& if a domain "SEED " is present,',&
     x' permeability modification will be made'/&
     x12X,'&&&& if no domain "SEED " is present,',&
     x' no permeability modification will be made')
!
 6010 format(' ',66('>'),65('<')/' ',27('>'),'  domain =',&
     x' "SEED " is not present, no permeability modification will',&
     x' be made  ',26('<')/' ',66('>'),65('<'))
!
 6012 format(/' Data provided in domain "SEED " are used to',&
     x' select the following options.'//&
     x' DROK   = *** random number seed for internal',&
     x' generation of "linear" permeability modifiers.'/&
     x'        = 0: (default) no internal generation of',&
     x' "linear" permeability modifiers.'/&
     x'        > 0: perform "linear" permeability modification;',&
     x' random modifiers are generated internally with DROK',&
     x' as seed.') 
!
 6014 format(/' POR    = *** random number seed for',&
     x' internal generation of "logarithmic" permeability',&
     x' modifiers,'/'        = 0: (default) no internal',&
     x' generation of "logarithmic" permeability modifiers.'/&
     x'        > 0: perform "logarithmic" permeability',&
     x' modification; random modifiers are generated internally',&
     x' with POR as seed.')
!
 6016 format(/12X,120('&')/12X,'&&&&& note: if both DROK and POR',&
     x' are specified as non-zero, DROK takes precedence',34X,&
     x5('&')/12X,5('&'),7X,'if both DROK and POR are zero,',&
     x' permeability modifiers as supplied through "ELEME" data',&
     x' will be used    &&&&&'/12X,120('&')//' PER(1) = *** scale',&
     x' factor (optional) for internally generated',&
     x'  permeability modifiers.'/'        = 0: (defaults to PER(1)',&
     x' = 1): permeability modifiers are generated as random',&
     x' numbers in the interval (0, 1).'/&
     x'        > 0: permeability modifiers are generated as random',&
     x' numbers in the interval (0, PER(1)).'/)
!
 6020 format(' PER(2) = *** shift (optional) for internal',&
     x' or external permeability modifiers.'/'        = 0: (default)',&
     x' no shift is applied to permeability modifiers.'/&
     x'        > 0: permeability modifiers are shifted according to ',&
     x'm = m - PER(2). All m < 0 are set equal to zero.'/)
!
 6022 format(' ',65('>'),66('<')/' ',30('>'),'  domain =',&
     x' "SEED " is present, permeability modification will be made  ',&
     x30('<')/' ',65('>'),66('<'))
!
 6024 format(/' Data provided in domain "SEED " are used to',&
     x' select the following options.'//&
     x' DROK   = ',1pE12.5,' *** random number seed for internal',&
     x' generation of "linear" permeability modifiers.'/&
     x'        = 0: (default) no internal generation of',&
     x' "linear" permeability modifiers.'/&
     x'        > 0: perform "linear" permeability modification;',&
     x' random modifiers are generated internally with DROK',&
     x' as seed.') 
!
 6026 format(/' POR    = ',1pE12.5,' *** random number seed for',&
     x' internal generation of "logarithmic" permeability',&
     x' modifiers,'/'        = 0: (default) no internal',&
     x' generation of "logarithmic" permeability modifiers.'/&
     x'        > 0: perform "logarithmic" permeability',&
     x' modification; random modifiers are generated internally',&
     x' with POR as seed.')
!
 6028 format(/12X,120('&')/12X,'&&&&& note: if both DROK and POR',&
     x' are specified as non-zero, DROK takes precedence',34X,&
     x5('&')/12X,5('&'),7X,'if both DROK and POR are zero,',&
     x' permeability modifiers as supplied through "ELEME" data',&
     x' will be used    &&&&&'/12X,120('&')//' PER(1) = ',1pE12.5,&
     x ' *** scale factor (optional) for internally generated',&
     x'  permeability modifiers.'/'        = 0: (defaults to PER(1)',&
     x' = 1): permeability modifiers are generated as random',&
     x' numbers in the interval (0, 1).'/&
     x'        > 0: permeability modifiers are generated as random',&
     x' numbers in the interval (0, PER(1)).'/)
!
 6030 format(' PER(2) = ',1pE12.5,' *** shift (optional) for internal',&
     x' or external permeability modifiers.'/'        = 0: (default)',&
     x' no shift is applied to permeability modifiers.'/&
     x'        > 0: permeability modifiers are shifted according to ',&
     x' m = m - PER(2). All m < 0 are set equal to zero.'/)
!
 6032 format(' ',131('&')/' ',3('&'),25X,'   Option 1:',&
     x' Externally supplied permeability modification is in',&
     x' effect   ',26X,3('&')/' ',131('&')/)
!
 6034 format(' ',131('&')/' ',3('&'),&
     x'   Generate random permeability modifiers for',I5,&
     x' grid blocks with seed S = ',1pE12.5,', scale factor',&
     x' SRAN = ',1pE12.5,'  ',3('&'))
!
 6036 format(' ',3('&'),31X,'   Option 2: "Linear" permeability',&
     x' modification is in effect   ',31X,3('&')/&
     x' ',131('&')/)
!
 6038 format(' ',3('&'),29X,'   Option 3: "Logarithmic"',&
     x' permeability modification is in effect   ',28X,3('&')/&
     x' ',131('&')/)
!
 6040 FORMAT(' of NEL = ',I5,' grid blocks, a total',&
     x   ' of IM = ',I5,' or ',F5.2,' % is impermeable'/)
!
 6042 format(' Permeability Modifiers'/(10(1X,1pE12.5)))
 6044 format(/' ',131('&')/)
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of PermMod_Init
!
!
      RETURN
! 
!
      END SUBROUTINE PermMod_Init
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE READ_Files(MC)
! 
! ...... Modules to be used 
! 
         USE GenControl_Param, ONLY: NoFloSimul
		 USE MPI_PARAM
		 USE MPI_SUBROUTINES
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*  ROUTINE FOR READING INPUT DATA FROM DISK FILES, WHICH ARE EITHER   *
!*        PROVIDED BY THE USER, OR ARE INTERNALLY GENERATED IN         *
!*        SUBROUTINE INPUT FROM DATA GIVEN IN THE JOB DECK             *
!*                                                                     *
!*                  Version 1.00 - January 12, 2004                    *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: MC
! 
      INTEGER :: ICALL = 0,IERR
! 
      INTEGER :: fileno,n2x,IM
! 
! -------
! ... Character variables
! -------
! 
      CHARACTER(LEN = 5)  :: suffix
      CHARACTER(LEN = 10) :: filenm
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: EX

! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of READ_Files
!
!
      ICALL=ICALL+1
      IF(ICALL.EQ.1 .AND. MPI_RANK==0) WRITE(11,6000)
! 
! ----------
! ... Opening files for writing outputs for plotting
! ----------
! 
      DO_PlotF: DO n2x=1,2
! 
         IF(n2x == 1) THEN
            suffix = 'coord'       ! Suffix for the coordinate file
         ELSE 
            suffix = 'Data1'       ! Suffix for the output data file
         END IF            
! 
         filenm = 'Plot_'//suffix  ! The new file name
         fileno = 39+n2x           ! The new file number
! 
! -------------
! ...... Determine if the files exist
! -------------
! 
		if(n2x==2) then
			cycle !We are gonna open the file using MPI I/O in another subroutine
		end if
        OPEN(fileno,FILE=filenm)
! 
      END DO DO_PlotF
! 
! ... Printing for outout file formatting purposes (cosmetic) 
! 
      IF(MPI_RANK==0) PRINT 6005
! 
! 


! 
!***********************************************************************
!*                                                                     *
!*    Read data from MESH file and invoke partitioning algorithm       *
!*                                                                     *
!***********************************************************************
  IF_fileNo: IF(MC == 0) THEN
                    IM = 4         ! Regular MESH file
                 ELSE
                    IM = 10        ! MINC-type mesh file
                 END IF IF_fileNo
!
	  CALL Read_partitionMESH(IM)
	  
	  IF(NoFloSimul .EQV. .TRUE.) RETURN
! 
 
! 
!***********************************************************************
!*                                                                     *
!*    Read  and process data from GENER file in master process first   *
!*                                                                     *
!***********************************************************************
	  if(MPI_RANK==0) CALL READ_FileGENER_Master
	  
	

	  IF(NoFloSimul .EQV. .TRUE.) RETURN
	 
! 
!***********************************************************************
!*                                                                     *
!*   Deallocate global arrays from master and allocate 				   *
!*					local arrays in all proc       					   *
!*                                                                     *
!*********************************************************************** 

	  CALL ALLOCATELOC
!***********************************************************************
!*                                                                     *
!*  Read data from the ELEME block of the file MESH in each local proc *
!*                                                                     *
!***********************************************************************
! 
! 
    
!
      CALL READ_ElemMESH(IM)
	  
! 
! -------
! ... Determine the numbers of elements in the "Elem_Time_Series" list for time-series printouts
! -------
! 
      CALL NUM_TimSrsPrnt('Elem_')
! 
! 
!***********************************************************************
!*                                                                     *
!*          Read data from the CONNE block of the file MESH            *
!*                                                                     *
!***********************************************************************
! 
! 
      CALL READ_ConxMESH(IM)
! 
! -------
! ... Determine the numbers of elements in the connections of the "Conx_Time_Series" list for time-series printouts
! -------
! 
      CALL NUM_TimSrsPrnt('Conx_')
! 
! -------
! ... Optional block-by-block permeability modification
! -------
! 
      CALL PermMod_Init
! 
! 
!***********************************************************************
!*                                                                     *
!*               Read sink/source data from file GENER                 *
!*                                                                     *
!***********************************************************************
! 
! 
      CALL READ_FileGENER_loc
! 
! -------
! ... Determine the numbers of elements in the sources/sinks of the "Conx_Time_Series" list for time-series printouts
! -------
! 
      CALL NUM_TimSrsPrnt('SS_Ti')
! 
! 
!***********************************************************************
!*                                                                     *
!*            Read initial condition data from file INCON              *
!*                                                                     *
!***********************************************************************
! 
! 
      CALL READ_FileINCON
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'READ_Files        1.0   12 January   2004',6X,&
     &         'Initialize data from files "MESH" ',&
     &         'or "MINC", "GENER", AND "INCON"',/,&
     &        47X,'Also initializes permeability modifiers and ',&
     &            'coordinate arrays',/,&
     &        47X,'and optionally reads tables with flowing ',&
     &            'wellbore pressures')
!
 6001 FORMAT(' FILE "',a10,'" DOES NOT EXIST ==> OPEN AS A NEW FILE')
 6002 FORMAT(' FILE "',a10,'" EXISTS ==> OPEN AS AN OLD FILE')
 6005 FORMAT(' ')
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of READ_Files
!
!
      RETURN
! 
      END SUBROUTINE READ_Files
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE READ_ElemMESH(IM)

   ! ...... Modules to be used 
! 
         USE Basic_Param
         USE GenControl_Param
         USE TimeSeriesP_Param
! 
         USE Element_Arrays
         USE PFMedProp_Arrays
		 USE MPI_PARAM
		 USE MPI_ARRAYS
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*     ROUTINE FOR READING THE DATA DESCRIBING THE DOMAIN ELEMENTS     *
!*                   DIRECTLY FROM THE FILE 'MESH'                     *
!*                                                                     *
!*                  Version 1.00 - August 19, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: SCALEA,SCALEV
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: IM
! 
      INTEGER :: ICALL = 0,ECOUNT
! 
      INTEGER :: N,M,matmat,i,i_ASCII,ierr
! 
! -------
! ... Character variables
! -------
! 
      CHARACTER(LEN = 5) :: MA12,DENT
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: Medium_ByName
      LOGICAL :: Active_Element
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of READ_ElemMESH
!
!
      ICALL = ICALL+1                                                     
! 
! ----------
! ... Reading the headings of the file �MESH�
! ----------
! 
      REWIND IM
      READ(IM,5001) DENT
! 
! ----------
! ... Initialization
! ----------
! 
	Active_Element = .FALSE.
! 
! 
! ----------
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                       MAIN ELEMENT LOOPS                            >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
ECOUNT=0
      DO_NumEle1: DO n=1,NEL
! 
		 i=NUL+1  !Assigning i to be out of bounds to update set, will change if current element is in update set
		 
		 if(procnum(n)/=MPI_rank ) THEN		!Not an internal element, check if update element
			do i=1,NUL						!Checking if an update element	
				if(update_g(i)==n) exit
			end do
			if(i>NUL) then					!not an update element		
				READ (IM,*)					!Read record and continue	
				CYCLE
			end if
		 end if
		 
		 if(i>NUL) then
			ECOUNT = locnum(n)
		 else
			ECOUNT = i+NELL
		 end if
		
		 if(ECOUNT>NTL) THEN
			!There must be error
			print *, "ERROR AT 22ZZ"
			stop
		 END IF
! ...... Initialize the element name
! 
         elem(ECOUNT)%name = '        '
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For 5-character elements 
! ...... 
! >>>>>>>>>>>>>>>>>>
! 
! 
         IF_NumCh: IF(No_CEN == 5) THEN   ! For 5-character elements
! 
! ----------------
! ......... Read the element data from the ELEME block of MESH
! ----------------
! 	
            READ(IM,5001) elem(ECOUNT)%name(1:5), &! 5-Character element name
     &                    MA12,          &    ! 5-Character name of corresponding rock type
     &                    elem(ECOUNT)%vol,    &   ! Element volume (m^3)
     &                    AHT(ECOUNT),        &    ! Interface area of heat exchange with semi-infinite confining bed(m^2)
     &                    pm(ECOUNT),         &    ! Permeability multiplier
     &                    X_coord(ECOUNT),    &    ! X-coordinate element center (m)
     &                    Y_coord(ECOUNT),    &    ! Y-coordinate of element center (m)
     &                    Z_coord(ECOUNT)         ! Z-coordinate of element center (m)
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For 8-character elements 
! ...... 
! >>>>>>>>>>>>>>>>>>
! 
! 
         ELSE IF(No_CEN == 8) THEN   
! 
! ----------------
! ......... Read the element data from the ELEME block of MESH
! ----------------
! 
            READ(IM,5002) elem(ECOUNT)%name,& ! 5-Character element name
     &                    MA12,      &   ! 5-Character name of corresponding rock type
     &                    elem(ECOUNT)%vol,&  ! Element volume (m^3)
     &                    AHT(ECOUNT),  &     ! Interface area of heat exchange with semi-infinite confining bed(m^2)
     &                    pm(ECOUNT),   &     ! Permeability multiplier
     &                    X_coord(ECOUNT),&   ! X-coordinate element center (m)
     &                    Y_coord(ECOUNT), &  ! Y-coordinate of element center (m)
     &                    Z_coord(ECOUNT)    ! Z-coordinate of element center (m)
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... Otherwise 
! ...... 
! >>>>>>>>>>>>>>>>>>
! 
! 
         ELSE                        ! If we get here, there must have been a mistake
! 
            WRITE(6,6801) No_CEN     ! Print error message
			WRITE(*,6801) No_CEN
            STOP                     ! Simulation is aborted
! 
! <<<<<<<
! <<<<<<<
! <<<<<<<
! 
		
         END IF IF_NumCh
! 
! 
!***********************************************************************
!*                                                                     *
!*                Processing the element-specific data                 *
!*                                                                     *
!***********************************************************************
! 
!    
! 
! -------------
! ...... Determine if the medium type is by name or by number
! -------------
! 
         Medium_ByName = .FALSE.                     ! Default: Medium by number
! 
         DO_MedName: DO i=1,5
! 
            i_ASCII = ICHAR(MA12(i:1))               ! Determine the ASCII value of ...
!                                                       ! ... each character of the medium name
			IF(i_ASCII == 32) CYCLE DO_MedName       ! Continue iteration for blanks
!                                                 
            IF(i_ASCII > 57 .OR. i_ASCII <48) THEN   ! If any character is a letter, ...
               Medium_ByName = .TRUE.                ! ... the flag is reset
               EXIT DO_MedName
            END IF
! 
         END DO DO_MedName
! 
! -------------
! ...... Assign rock type to elements
! -------------
! 
         IF_Mat1: IF(Medium_ByName .EQV. .FALSE.) THEN
!
            READ(MA12,'(I5)') elem(ECOUNT)%MatNo  ! Elements with a domain number
                                             ! Read MATX(ECOUNT)(target) from MA12(string) 
! 
         ELSE
! 
! ......... Elements with a domain name 
! 
            matmat = 0
            DO_NumRok1: DO m=1,N_PFmedia
! 
               IF(MA12 == MAT(m)) THEN
                  elem(ECOUNT)%MatNo = m          ! Find material index
                  matmat        = 1
                  EXIT DO_NumRok1
               END IF
! 
            END DO DO_NumRok1
! 
! ......... On failure to match, print warning 
! 
            IF_NoMatch1: IF(matmat == 0) THEN
               WRITE(6,6004) MA12,elem(ECOUNT)%name
               elem(ECOUNT)%MatNo = 0
            END IF IF_NoMatch1
! 
         END IF IF_Mat1
! 
! -------------
! ...... Elements without domain assignment are assigned to Domain #1 by default
! -------------
! 
         IF_DomDef1: IF(elem(ECOUNT)%MatNo == 0) THEN
            elem(ECOUNT)%MatNo = 1
            WRITE(6,6005) elem(ECOUNT)%name,MAT(1)
         END IF IF_DomDef1
! 
! -------------
! ...... Account for scaling    
! -------------
! 
         IF_Scale1: IF(SCALE /= 1.0D0) THEN
            elem(ECOUNT)%vol = elem(ECOUNT)%vol*SCALEV
            aht(ECOUNT)      = aht(ECOUNT)*scalea
         END IF IF_Scale1
!
! <<<                      
! <<< End of the ELEMENT LOOP         
! <<<
!
! 
!
      END DO DO_NumEle1
!
!----------READING THE BLANK RECORD-------
	  READ (IM,*)	
!-------------------------------------------

!***********************************************************************
!*                                                                     *
!*           DETERMINE NUMBER OF FLAG ELEMENT (FOR PRINT-OUT)          *
!*                                                                     *
!***********************************************************************
! 
   NST = 0
      IF_ActEl2: IF(ELST(1:5) /= '     ') THEN
! 
         DO_NumEleB: DO n = 1,NELL
! 
            IF(elem(n)%name == ELST) THEN
               NST = n
               EXIT
            END IF
! 
         END DO DO_NumEleB
! 		
		NST = -1 				!NST is present but not on this proc
      END IF IF_ActEl2
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 5001 FORMAT(A5,10X,A5,6(E10.4))
 5002 FORMAT(A8, 7X,A5,6(E10.4))
!
!
 6000 FORMAT(/,'READ_ElemMESH     1.0   19 August    2004',6X,&
     &         'Read the element data from the ELEME block of ',&
     &         'the file MESH') 
!
 6001 FORMAT(' NUMBER OF ELEMENTS SPECIFIED IN DATA BLOCK "ELEME"',&
     &       ' EXCEEDS ALLOWABLE MAXIMUM OF ',I7/&
     &       ' INCREASE PARAMETER "MaxNum_Elem" IN MAIN PROGRAM,',&
     &       ' AND RECOMPILE',//,&
     &       ' >>>>>>>>>>   F L O W   S I M U L A T I O N  ',&
     &       ' A B O R T E D   <<<<<<<<<<')
 6002 FORMAT(3(1pE15.8,1x))
!
 6004 FORMAT(' REFERENCE TO UNKNOWN MATERIAL ',A5,' AT ELEMENT "',A8,&
     &       '" ==> WILL IGNORE " ')
 6005 FORMAT(' !!!!! ==> WARNING <== !!!!! :',&
     &       ' ELEMENT "',A8,'" HAS NO DOMAIN ASSIGNMENT;',&
     &       ' ASSIGN TO DOMAIN # 1, "',A5,'"')
!
 6801 FORMAT(//,20('ERROR-'),//,T33,&
     &             '       S I M U L A T I O N   A B O R T E D',/,&
     &         T5,'The number of charcters of the element names ',&
     &            'No_CEN = ',i3,' is not an acceptable number ',&
     &            '(must be either 5 or 8)',&
     &       /,T33,'               CORRECT AND TRY AGAIN',     &
     &       //,20('ERROR-'))
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of READ_ElemMESH
!
!
      RETURN
! 
      END SUBROUTINE READ_ElemMESH
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE READ_ConxMESH(IM)
! 
! ...... Modules to be used 
! 
         USE Basic_Param
         USE GenControl_Param
         USE TimeSeriesP_Param
! 
         USE Element_Arrays
         USE Connection_Arrays
		 USE MPI_ARRAYS
		 USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*   ROUTINE FOR READING THE DATA DESCRIBING THE DOMAIN CONNECTIONS    *
!*                   DIRECTLY FROM THE FILE �MESH�                     *
!*                                                                     *
!*                   Version 1.00, August 20, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: SCALEA
! 
! -------
! ... Integer arrays
! -------
! 
      INTEGER, DIMENSION(3) :: isumd = 0
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: IM
! 
      INTEGER :: ICALL  = 0,ierr
! 
      INTEGER :: i,j,N,ipluss,k,ECOUNT,ICOUNT
      INTEGER :: n_RadHt,n_ConHt,N_invalid_elem
! 
! -------
! ... Character variables
! -------
! 
      CHARACTER(LEN = 5) :: EL5_CR,DENT
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of READ_ConxMESH
!
!
      ICALL = ICALL+1                              
! 
      isumd  = 0  ! CAREFUL! Whole array operation
! 
! ----------
! ... Compute scaling parameters for area-based parameters
! ----------
! 
      SCALEA = SCALE*SCALE
! 
! ----------
! ... Reading the headings of the block �CONNE�
! ----------
! 
      READ(IM,5001) DENT
! 
! ... Initialization - CAREFUL! Whole array operations 
! 
         conx%nam1 = '        '
         conx%nam2 = '        '
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                       MAIN CONNECTION LOOP                          >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
	  Icount = 0
	  Ecount = NCONI
      DO_NumCon: DO n=1,NCON+1
! 
! 
	  if(ICOUNT+1>NCONI .and. ECOUNT+1>NCONT) THEN   !No more connections to read
		exit
	  end if
	  
	  !Determining if this connection belongs to current processor. If yes, finding its location at k
	  if(ICOUNT+1>NCONI) THEN			!Internal connections exhausted				
		if(globalconx(Ecount+1)/=n) then !No, connection is not local
			READ(IM,*)
			CYCLE
		end if
		ECOUNT = ECOUNT+1
		k=ECOUNT
	  elseif(ECOUNT+1>NCONT) then		!External connections exhausted
		if(globalconx(ICOUNT+1)/=n) then!No, connection is not local
			READ(IM,*)
			CYCLE
		end if
		ICOUNT = ICOUNT+1
		k=ICOUNT
	  else								!Both internal and external connections left
		if(globalconx(ICOUNT+1)/=n .and. globalconx(Ecount+1)/=n)  then !No, connection is not local
			READ(IM,*)
			CYCLE
		end if
		if(globalconx(ICOUNT+1)==n) then	!Internal connection
			ICOUNT = ICOUNT+1
			k=ICOUNT
		else								!External connection
			ECOUNT = ECOUNT+1
			k=ECOUNT
		end if
		
	  end if
! 
! 
! 
         IF_NumCh: IF(No_CEN == 8) THEN   
! 
! ----------------
! ......... Read the element data from the CONNE block of MESH - 8-character elements
! ----------------
! 
            READ(IM,5002) conx(k)%nam1, & ! 8-Character name of connection element #1  
     &                    conx(k)%nam2, & ! 8-Character name of connection element #2
     &                    conx(k)%ki,   & ! Directional indicator of the permeability tensor
     &                    conx(k)%d1,  &  ! Distance of the center of element #1 from interface (m)
     &                    conx(k)%d2,  &  ! Distance of the center of element #2 from interface (m)
     &                    conx(k)%area,&  ! Interface area (m^2)
     &                    conx(k)%beta, & ! = COS(b), b = angle between the vertical and the connection line 
     &                    sig(k)         ! Radiant emittance factor for radiative heat transfer
! 
            EL5_CR = conx(k)%nam1(1:5)
! 
         ELSE
! 
! ----------------
! ......... Read the element data from the CONNE block of MESH - 5-character elements
! ----------------
! 
            READ(IM,5001) conx(k)%nam1(1:5), & ! 8-Character name of connection element #1  
     &                    conx(k)%nam2(1:5), & ! 8-Character name of connection element #2
     &                    conx(k)%ki,        & ! Directional indicator of the permeability tensor
     &                    conx(k)%d1,        & ! Distance of the center of element #1 from interface (m)
     &                    conx(k)%d2,        & ! Distance of the center of element #2 from interface (m)
     &                    conx(k)%area,      & ! Interface area (m^2)
     &                    conx(k)%beta,      & ! = COS(b), b = angle between the vertical and the connection line 
     &                    sig(k)              ! Radiant emittance factor for radiative heat transfer
! 
            EL5_CR = conx(k)%nam1
! 
         END IF IF_NumCh
! 
! -------------
! ...... If EL5_CR = �+++  �, or blanck, exit
! -------------
! 
         IF_ConEnd1: IF(EL5_CR == '+++  ' .OR. EL5_CR == '     ') THEN  
! 
            EXIT                                                ! Done !
! 
         END IF IF_ConEnd1        
! -------------
! 
         IF_Scale: IF(SCALE /= 1.0D0) THEN
                      conx(k)%d1   = conx(k)%d1*SCALE
                      conx(k)%d1   = conx(k)%d1*SCALE
                      conx(k)%area = conx(k)%area*SCALEA
                   END IF IF_Scale
!
! <<<                      
! <<< End of the CONNECTION LOOP         
! <<<
!
      END DO DO_NumCon
	  
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 5001 FORMAT(2A5,15X,I5,5E10.4)
 5002 FORMAT(2A8, 9X,I5,5E10.4)
!
 5003 FORMAT(16I5)
 5004 FORMAT(10I8)
!
 6000 FORMAT(/,'READ_ConxMESH     1.0   20 August    2004',6X,&
     &         'Read the connection data from the CONNE block of ',&
     &         'the file MESH') 
!
 6001 FORMAT('NUMBER OF CONNECTIONS SPECIFIED IN DATA BLOCK "CONNE"',&
     &       ' EXCEEDS ALLOWABLE MAXIMUM OF ',I8,/&
     &       ' INCREASE PARAMETER "MaxNum_Conx" IN MAIN PROGRAM',&
     &       ' AND RECOMPILE',//,&
     &       ' !!!!! ==> ==> ==> FLOW SIMULATION IS ABORTED')
 6002 FORMAT('+++  ')
!
 6004 FORMAT(' REFERENCE TO UNKNOWN MATERIAL ',A5,' AT ELEMENT "',A8,&
     &       '" ==> WILL IGNORE ELEMENT')
 6005 FORMAT(' !!!!! ==> WARNING <== !!!!! :',&
     &       ' ELEMENT "',A8,'" HAS NO DOMAIN ASSIGNMENT;',&
     &       ' ASSIGN TO DOMAIN # 1, "',A5,'"')
 6008 FORMAT(' REFERENCE TO UNKNOWN ELEMENT ',A8,' AT CONNECTION ',&
     &       I6,' ==> WILL IGNORE ')

 6078 FORMAT(//,20('ERROR-'),//,T33,&
     &             '       S I M U L A T I O N   A B O R T E D',/,&
     &         T9,'The determined number of active dimensions',&
     &             ' NACTDI = ',i1,&
     &             ' is not an acceptable number (must be 1, 2 or 3)',&
     &       /,T33,'               CORRECT AND TRY AGAIN',     &
     &       //,20('ERROR-'))
 6079 FORMAT(//,20('ERROR-'),//,T33,&
     &             '       S I M U L A T I O N   A B O R T E D',/,&
     &         T10,'The index of the second active dimension',&
     &             ' NACTD2 = ',i2,&
     &             ' is not an acceptable number (must be 2 or 3)',&
     &       /,T33,'               CORRECT AND TRY AGAIN',     &
     &       //,20('ERROR-'))
 6080 FORMAT(//,130('*'),/,'*',T130,'*',/,'*',&
     &       T10,'The number of active dimensions NACTDI =',i2,&
     &       T130,'*',/,'*',T130,'*')
 6081 FORMAT('*',T10,'The numbers of connections in the X-, Y- ',&
     &               'and Z-directions are ',i6,', ',i6,' and ',i6,&
     &               ' respectively',T130,'*',&
     &             /,'*',T130,'*',/,130('*'),//)
!
 6801 FORMAT(//,20('ERROR-'),//,T33,&
     &             '       S I M U L A T I O N   A B O R T E D',/,&
     &         T5,'The number of charcters of the element names ',&
     &            'No_CEN = ',i3,' is not an acceptable number ',&
     &            '(must be either 5 or 8)',&
     &       /,T33,'               CORRECT AND TRY AGAIN',     &
     &       //,20('ERROR-'))
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of READ_ConxMESH
!
!
      RETURN
! 
      END SUBROUTINE READ_ConxMESH
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE NUM_TimSrsPrnt(aa)
! 
! ...... Modules to be used 
! 
         USE Basic_Param
         USE TimeSeriesP_Param
! 
         USE Element_Arrays
         USE Connection_Arrays
         USE Q_Arrays
		 USE MPI_PARAM
		 USE MPI_ARRAYS
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*        ROUTINE FOR DETERMINING THE ELEMENT, CONNECTION AND          *
!*           SOURCE/SINK NUMBERS FOR TIME-SERIES PRINT-OUTS            *
!*                                                                     *
!*                 Version 1.0 - September 29, 2004                    *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
	  
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL  = 0
      INTEGER :: non_common_elem,non_common_conx,non_common_SS
! 
      INTEGER :: i,n,ierr
	  INTEGER, allocatable,dimension(:) :: temp
! 
! -------
! ... Character variables
! -------
! 
      CHARACTER(LEN = 5), INTENT(IN) :: aa
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of NUM_TimSrsPrnt
!
!
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 ) WRITE(11,6000)       

!
!***********************************************************************
!*                                                                     *
!*         DETERMINE INDEX NUMBERS FOR TIME-SERIES PRINT-OUT           *
!*                                                                     *
!***********************************************************************
!
      SELECT_TimeSeries: SELECT CASE(aa)
!
!***********************************************************************
!*                                                                     *
!*            FOR TIME-SERIES PRINT-OUT OF AN ELEMENT LIST             *
!*                                                                     *
!***********************************************************************
!
      CASE('Elem_','FOFT')     
!
         IF_IOFT: IF(N_obs_elem > 0) THEN
! 
! ----------------
! ......... Determining the element numbers of the observation elements 
! ----------------
! 
			
			allocate (temp(1:N_obs_elem))
			
            DO i=1,N_obs_elem 
			DO n=1,NELL
               IF(    elem(n)%name &
     &                          == obs_elem%name(i)) obs_elem%num(i) = n
            END DO
            END DO
			
! 
! ----------------
! ......... Determining the observation elements not corresponding to the actual elements
! ----------------
!           
			call MPI_REDUCE( obs_elem%num(1:N_obs_elem), temp, N_obs_elem,&
							MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD, ierr )
			
			non_common_elem = COUNT(temp(1:N_obs_elem) == 0)   ! The # of non-common elements
!			
            IF(non_common_elem >= N_obs_elem .And. MPI_RANK==0) &
				& obs_elem_Flag = .FALSE.  ! Reset the flag if no common elements
				
! 
! ----------------
! ......... Printing a warning, and continuing
! ----------------
! 
            IF( non_common_elem > 0 .AND. &
     &         (obs_elem_Flag .EQV. .TRUE.) .And. MPI_RANK==0) &
     &      THEN
               DO_ObsElem: DO i=1,N_obs_elem
                  IF_NonComE: IF(temp(i) == 0) THEN
                     PRINT 6100, ADJUSTL(obs_elem%name(i))
                  END IF IF_NonComE
               END DO DO_ObsElem
            END IF
!
			non_common_elem = COUNT(obs_elem%num(1:N_obs_elem) == 0)
			if(non_common_elem>=N_obs_elem) obs_elem_Flag = .FALSE.
!
			
			deallocate(temp)
			
         END IF IF_IOFT
!
!***********************************************************************
!*                                                                     *
!*           FOR TIME-SERIES PRINT-OUT OF A CONNECTION LIST            *
!*                                                                     *
!***********************************************************************
!
      CASE('Conx_','COFT')     
!
         IF_ICOFT: IF(N_obs_conx > 0) THEN
! 
! ----------------
! ......... Determining the connection numbers of the observation connections 
! ----------------
! 
            DO_ConObsLoop: DO i=1,N_obs_conx 
               DO_ConxLoop: DO n=1,NCONI 
                  IF(   TRIM(ADJUSTL(conx(n)%nam1))&
     &                //TRIM(ADJUSTL(conx(n)%nam2)) &
     &               == TRIM(ADJUSTL(obs_conx%name(i)))) &
     &            THEN
                     obs_conx%num(i) = n
                     EXIT DO_ConxLoop
                  END IF
               END DO DO_ConxLoop
            END DO DO_ConObsLoop
			
			allocate(temp(N_obs_conx))
! 
! ----------------
! ......... Determining the observation connections not corresponding to the actual connections
! ----------------
! 
			
			call MPI_REDUCE( obs_conx%num(1:N_obs_conx), temp, N_obs_conx,&
							MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD, ierr )
							
							
            non_common_conx = COUNT(temp(1:N_obs_conx) == 0)   ! The # of non-common connections
!
            IF(non_common_conx >= N_obs_conx) obs_conx_Flag = .FALSE.  ! Reset the flag if no common connections
! 
! ----------------
! ......... Printing a warning, and continuing
! ----------------
! 
            IF( non_common_conx > 0 .AND. &
     &         (obs_conx_Flag .EQV. .TRUE.) .AND. MPI_RANK == 0) &
     &      THEN
               DO_ObsConx: DO i=1,N_obs_conx
                  IF_NonComC: IF(temp(i) == 0) THEN
                     PRINT 6102, obs_conx%name(i)(1:5),&
     &                           obs_conx%name(i)(6:10)
                  END IF IF_NonComC
               END DO DO_ObsConx
            END IF
!
!			

            non_common_conx = COUNT(obs_conx%num(1:N_obs_conx) == 0)   ! The # of non-common connections
!
            IF(non_common_conx >= N_obs_conx) obs_conx_Flag = .FALSE.  ! Reset the flag if no common connections
! 

			deallocate(temp)
         END IF IF_ICOFT
!
!***********************************************************************
!*                                                                     *
!*          FOR TIME-SERIES PRINT-OUT OF A SOURCE/SINK LIST            *
!*                                                                     *
!***********************************************************************
!
      CASE('SS_Ti','GOFT')     
!
         IF_IGOFT: IF(N_obs_SS > 0) THEN
! 
! ----------------
! ......... Determining the source/sink numbers of the observation sources/sinks 
! ----------------
! 
			
            DO i=1,N_obs_SS 
			DO n=1,NOGN   
               if (SS(n)%el_num /= 0 .AND. & 
                                  SS(n)%name &
                               == obs_SS%name(i)) obs_SS%num(i) = n
            END DO
			END DO
			non_common_SS = COUNT(obs_SS%num(1:N_obs_SS) == 0)   ! The # of non-common elements
!			
            IF(non_common_SS >= N_obs_SS) obs_SS_Flag = .FALSE.  ! Reset the flag if no common sources/sinks
! 
! ----------------
! ......... Determining the observation source/sink not corresponding to the actual sources/sinks
! ----------------
! 
			
! ----------------
! ......... Printing a warning, and continuing
! ----------------
! 
            IF( non_common_SS > 0 .AND. &
     &         (obs_SS_Flag .EQV. .TRUE.) .and. MPI_RANK==0) &
     &      THEN
               DO_ObsSS: DO i=1,N_obs_SS
                  IF_NonComSS: IF(obs_SS%num(i)== 0) THEN
                     PRINT 6104, ADJUSTL(obs_SS%name(i))
                  END IF IF_NonComSS
               END DO DO_ObsSS
            END IF
!
         END IF IF_IGOFT
!
! <<<                      
! <<< End of the SELECT CASE construct         
! <<<
!
      END SELECT SELECT_TimeSeries
	  
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'NUM_TimSrsPrnt    1.0   29 September 2004',6X,&
     &         'Determine element, connection or source/sink numbers ',&
     &         'for time-series printouts') 
!
 6100 FORMAT(' Have encountered unknown element "',A8,'" in data block',&
     &       ' "Elem_Time_Series" - Will ignore unknown element')
 6102 FORMAT(' Have encountered unknown connection (',A8,')-(',A8,')',&
     &       ' in data block "Conx_Time_Series" -',&
     &       ' Will ignore unknown connection')
 6104 FORMAT(' The source/sink "',A8,'" in data block "SS_Time_Series"',&
     &       ' is not included in the valid source/sink list in',&
     &       ' "GENER" - Ignore and proceed')
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of NUM_TimSrsPrnt
!
!
      RETURN
! 
      END SUBROUTINE NUM_TimSrsPrnt
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE ACTIV_Conexns
! 
! ...... Modules to be used 
! 
         USE Basic_Param
!  
         USE Element_Arrays
         USE Connection_Arrays
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*     ROUTINE FOR IDENTIFYING ACTIVE CONNECTIONS  FROM THE INITIAL    *
!*                 �CONNE� BLOCK OF THE FILE �MESH�                    *
!*                                                                     *
!*                   Version 1.0 - January 9, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL  = 0, ncon_a = 0 
      INTEGER :: ier158 = 0
! 
      INTEGER :: N
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: EX
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of ACTIV_Conexns
!
!
      ICALL = ICALL+1                                                     
      IF(ICALL == 1) WRITE(11,6000)                                      
!
!***********************************************************************
!*                                                                     *
!*    ALLOCATE MEMORY FOR CONNECTION WORK ARRAYS IF THE RUN IS MADE    *
!*     TO OBTAIN A REDUCED CONNE BLOCK WITH ACTIVE CONNECTIONS ONLY    *
!*                                                                     *
!***********************************************************************
!
      ALLOCATE(NEX1_a(NCON),NEX2_a(NCON),IFL_con(NCON),STAT=ier158)
!
      IF(ier158 == 0) THEN     ! If all�s well, ...
         WRITE(50,6001) 158    ! write the error code and continue 
      ELSE                     ! Otherwise, ...
         PRINT 6002, 158
         WRITE(50,6002) 158    ! write the error code and ...
         STOP                  ! stop !
      END IF
! 
! ----------
! ... Determine the status of the file �CONN_act�
! ----------
! 
      OPEN(51,FILE='CONN_act')
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                       MAIN CONNECTION LOOP                          >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_NumCon: DO n = 1,NCON
!
         IF(    (conx(n)%n1 >  NELA .AND. conx(n)%n2 > NELA)&
     &      .OR.(conx(n)%n1 <= 0    .AND. conx(n)%n2 > NELA)&
     &      .OR.(conx(n)%n1 >  NELA .AND. conx(n)%n2 <= 0)&
     &      .OR.(conx(n)%n1 <= 0    .AND. conx(n)%n2 <= 0))&
     &   THEN                                         ! Active connections: ...
            IFL_con(n) = 0                            !    Set flag  
         ELSE                                         ! Inactive connections: ...
            ncon_a          = ncon_a+1                !    Count them
            IFL_con(n)      = 1                       !    Set flag
            NEX1_a(ncon_a)  = conx(n)%n1              !    Renumber them
            NEX2_a(ncon_a)  = conx(n)%n2
         END IF
!
! <<<                      
! <<< End of the CONNECTION LOOP         
! <<<
!
      END DO DO_NumCon
!
!***********************************************************************
!*                                                                     *
!*         WRITE THE CONNECTION DATA IN THE NEW FILE �CONN_act�        *
!*                                                                     *
!***********************************************************************
!
      WRITE(51,6003)                  ! Write the 'CONNE' heading
! 
! ----------
! ... Write the active connection data into the new file �CONN_act�
! ----------
! 
      IF_CharElN: IF(No_CEN == 8) THEN
! 
         DO n=1,NCON                  ! 8-character elements
            IF(IFL_con(n) == 1) THEN
               WRITE(51,5002) conx(n)%nam1,conx(n)%nam2,&
     &                        conx(n)%ki,conx(n)%d1,conx(n)%d2,&
     &                        conx(n)%area,conx(n)%beta,sig(n)
            END IF
         END DO
! 
      ELSE
! 
         DO n=1,NCON                  ! 5-character elements
            IF(IFL_con(n) == 1) THEN
               WRITE(51,5001) conx(n)%nam1(1:5),conx(n)%nam2(1:5),&
     &                        conx(n)%ki,conx(n)%d1,conx(n)%d2,&
     &                        conx(n)%area,conx(n)%beta,sig(n)
            END IF
         END DO
! 
      END IF IF_CharElN
!
!***********************************************************************
!*                                                                     *
!*           WRITE THE ELEMENT NUMBERS OF THE CONNECTIONS              *
!*                 AT THE END OF THE �CONN_act� FILE                   *
!*                                                                     *
!***********************************************************************
!
         WRITE(51,6004)                  ! Write the '+++  ' heading
!
         IF(No_CEN == 8) THEN
            WRITE(51,5004) (NEX1_a(n),NEX2_a(n), n=1,ncon_a)  ! 8-character elements
         ELSE
            WRITE(51,5003) (NEX1_a(n),NEX2_a(n), n=1,ncon_a)  ! 5-character elements
         END IF
!
         ENDFILE 51
!
         PRINT 6998, ncon_a     ! Print info on the number of active connections
         PRINT 6999             ! Print explanatory message
!
         STOP                   ! Simulation completed. Stop!
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 5001 FORMAT(2A5,15X,I5,5E10.4)
 5002 FORMAT(2A8, 9X,I5,5E10.4)
!
 5003 FORMAT(16I5)
 5004 FORMAT(10I8)
!
 6000 FORMAT(/,'ACTIV_Conexns     1.0    9 January   2004',6X,&
     &         'Determine active connections from a pre-existing ',&
     &         'file MESH and write reduced CONNE block') 
!
 6001 FORMAT(T2,'MEMORY ALLOCATION AT POINT ',i3,' WAS SUCCESSFUL')
 6002 FORMAT(//,20('ERROR-'),//,&
     &       T2,'MEMORY ALLOCATION AT POINT ',i3,' WAS UNSUCCESSFUL',//,&
     &       T2,'!!!!!!!    THE EXECUTION WAS STOPPED    !!!!!!!',&
     &       //,20('ERROR-'))
!
 6003 FORMAT('CONNE')
 6004 FORMAT('+++  ')
!
 6998 FORMAT(//,T2,'The number of active connections (involving ',&
     &             'at least an active element) NCON_a = ',i8,//)
 6999 FORMAT(//,12('ATTENTION-'),//,T41,&
     &             'S I M U L A T I O N   C O M P L E T E D',//,&
     &         T15,'The reduced CONNE block (incuding only active ',&
     &             'connections) is stored in the file "CONN_act"',&
     &       //,12('ATTENTION-'))
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of ACTIV_Conexns
!
!
      RETURN
! 
      END SUBROUTINE ACTIV_Conexns
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
!
      SUBROUTINE READ_FileGENER_Master
! 
! ...... Modules to be used 
! 
         USE Basic_Param
         USE GenControl_Param
         USE Hydrate_Param, ONLY: Inh_WMasF
! 
         USE Element_Arrays
         USE Q_Arrays
		 USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*        ROUTINE FOR READING THE SOURCE/SINK GENERATION DATA          *
!*                   DIRECTLY FROM THE FILE �GENER�                    *
!*                                                                     *
!*                  Version 1.0 - September 18, 2004                   *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL  = 0, NGL  = 0
! 
      INTEGER :: i,j,k,n,LTABA,ier,kf,non_common_SS
! 
! -------
! ... Character variables
! -------
! 
      CHARACTER(LEN = 4) :: SS_Typ
      CHARACTER(LEN = 5) :: EL5_CR,DENT
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of READ_FileGENER
!
!
      ICALL = ICALL+1                                                     
      IF(ICALL == 1) WRITE(11,6000)                                      
! 
! ----------
! ... Initializations
! ----------
! 
      NOGN = 0
      kf   = 0
! 
! ... CAREFUL! Whole array operations
! 
      SS%name_el = '        '
      SS%el_num  = 0
! 
! ----------
! ... Reading the headings of the file �GENER�
! ----------
! 
      REWIND 3
      READ(3,5001) DENT
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                         SINK/SOURCE LOOP                            >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_NumSS: DO n=1,MaxNum_SS+1
! 
! -------------
! ...... Check if MaxNum_SS is exceeded
! -------------
! 
         IF_Limit: IF(n == MaxNum_SS+1) THEN
! 
            READ(3,5001) EL5_CR     
!
            IF(EL5_CR /= '+++  ' .AND. EL5_CR /= '     ') THEN
               PRINT 6001, MaxNum_SS
               NoFloSimul = .TRUE.
               RETURN
            ELSE IF (EL5_CR == '+++  ') THEN
               GO TO 1000
            ELSE IF (EL5_CR == '     ') THEN
               GO TO 2000
            END IF
! 
         END IF IF_Limit
! 
! -------------
! ...... Reading Source/Sink data
! -------------
! 
         IF_NumCh: IF(No_CEN == 8) THEN   
! 
! ----------------
! ......... Read the source/sink data from the GENER file - 8-character elements
! ----------------
! 
            READ(3,5002) SS(n)%name_el, &! 8-Character name of cell containing
     &                   SS(n)%name,    &! 5-Character name of source/sink (well name)
     &                   SS(n)%n_TableP,&! Number of points in the generation table
     &                   SS(n)%type,    &! Type of source/sink
     &                   SS(n)%rate_m,  &! Injection/production rate (kg/s) or PI for SS(n)%type = 'DELV'
     &                   SS(n)%enth,    &! Specific enthalpy of injected fluid (J/kg) or bottomhole pressure for SS(n)%type = �DELV� 
     &                   SS(n)%z_layer, &! Layer thickness (m) for SS_Type = �DELV�
     &                   Inh_WMasF(n)   ! Mass fraction of inhibitor in injected H2O
! 
            EL5_CR = SS(n)%name_el
! 
         ELSE
! 
! ----------------
! ......... Read the source/sink data from the GENER file - 5-character elements
! ----------------
! 
            READ(3,5001) SS(n)%name_el(1:5), &! 5-Character name of cell containing
     &                   SS(n)%name,         &! 5-Character name of source/sink (well name)
     &                   SS(n)%n_TableP,     &! Number of points in the generation table
     &                   SS(n)%type,         &! Type of source/sink
	 &                   SS(n)%rate_m,       &! Injection/production rate (kg/s) or PI for SS(n)%type = 'DELV'
     &                   SS(n)%enth,         &! Specific enthalpy of injected fluid (J/kg) or bottomhole pressure for SS(n)%type = �DELV� 
     &                   SS(n)%z_layer,      &! Layer thickness (m) for SS_Type = �DELV�
     &                   Inh_WMasF(n)        ! Mass fraction of inhibitor in injected H2O
! 
            EL5_CR = SS(n)%name_el(1:5)
! 
         END IF IF_NumCh
		 
         SS_Typ = SS(n)%type(1:4)
! 
!***********************************************************************
!*                                                                     *
!*  If EL5_CR = �+++  �, read element numbers in the source/sink list  *
!*                                                                     *
!***********************************************************************
!
 1000    IF_SSEnd1: IF(EL5_CR == '+++  ') THEN  
! 
            NOGN = n-1                           ! Define actual NOGN
! 
			IF(NOGN<=0) return
			
            IF(No_CEN == 8) THEN
               READ(3,5004) (SS(i)%el_num, i=1,NOGN)  ! Read connection-element # data, 8-ch. names  
            ELSE
               READ(3,5003) (SS(i)%el_num, i=1,NOGN)  ! Read connection-element # data, 5-ch. names  
            END IF
! 
            RETURN                               ! End of the GENER file reached. Done !
! 
         END IF IF_SSEnd1        
!
!***********************************************************************
!*                                                                     *
!*         If EL5_CR is blank, the element list is exhausted           *
!*                                                                     *
!***********************************************************************
!
 2000    IF_SSEnd2: IF(EL5_CR == '     ') THEN  
! 
            NOGN = n-1                            ! Define actual NOGN
! 
            BACKSPACE 3                           ! Move back one line in GENER
            WRITE(3 ,6002)                        ! Write �+++  � 
! 
            IF(NOGN <= 0) RETURN
! 
! ----------------
! ......... Determine the element number in the cell that contains the source/sink
! ----------------
! 
            DO i=1,NOGN 
			DO j=1,NEL
               IF(&
     &                 elem(j)%name == SS(i)%name_el) SS(i)%el_num = j
            END DO
			END DO
! 
! ----------------
! ......... Determining the sources not corresponding to actual elements
! ----------------
! 
            non_common_SS = COUNT(SS(1:NOGN)%el_num == 0)   ! CAREFUL! Whole array operations              
! 
! ----------------
! ......... Printing a warning, and continuing
! ----------------
! 
            IF(non_common_SS > 0) THEN
               DO_ObsSS: DO i=1,NOGN
                  IF(SS(i)%el_num == 0) THEN
                     PRINT 6004, ADJUSTL(SS(i)%name_el),i
                  END IF
               END DO DO_ObsSS
            END IF
! 
! ----------------
! ......... Print the source/sink element numbers at the end of the GENER file
! ----------------
! 
            IF(No_CEN /= 8) THEN
               WRITE(3,5003) (SS(i)%el_num, i=1,NOGN)
            ELSE
               WRITE(3,5004) (SS(i)%el_num, i=1,NOGN)
            END IF            
! 
            ENDFILE 3                              ! Close the GENER file
! 
            RETURN                                 ! The processing of data in GENER is complete 
! 
         END IF IF_SSEnd2        
!

! 
!***********************************************************************
!*                                                                     *
!*  SKIPPING RECORDS FOR TABULAR SINK/SOURCE DATA (will read locally)  *
!*                                                                     *
!***********************************************************************
!
		 LTABA = ABS(SS(n)%n_TableP)
         IF(LTABA <= 1 .OR. SS_Typ == 'DELV') CYCLE DO_NumSS
		 
         READ(3,5006)
!
         READ(3,5006) 
         IF(SS(n)%type(5:5) /= ' ') THEN
            READ(3,5006) 
         END IF
!
! <<<                      
! <<< End of the SOURCE/SINK LOOP         
! <<<
!
      END DO DO_NumSS
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 5001 FORMAT(2A5,15X,I5,5X,A5,4G10.4)
 5002 FORMAT(A8,A5,12X,I5,5X,A5,4G10.4)
!
 5003 FORMAT(16I5)
 5004 FORMAT(10I8)
!
 5006 FORMAT(4E14.7)
!
 6000 FORMAT(/,'READ_FileGENER    1.0   18 September 2004',6X,&
     &         'Read the sink/source data from file GENER') 
!
 6001 FORMAT(' NUMBER OF SINKS/SOURCES SPECIFIED IN DATA BLOCK "GENER"',&
     &       ' EXCEEDS ALLOWABLE MAXIMUM OF ',I4/&
     &       ' INCREASE PARAMETER "MaxNum_SS" IN MAIN',&
     &       ' PROGRAM, AND RECOMPILE',//,&
     &       ' >>>>>>>>>>  S K I P   F L O W   S I M U L A T I O N  ',&
     &       ' <<<<<<<<<<')
 6002 FORMAT('+++  ')
!
 6004 FORMAT(' REFERENCE TO UNKNOWN ELEMENT ',A8,&
     &       ' AT SOURCE ',I2,' --- WILL IGNORE SOURCE')
 6005 FORMAT(' IGNORE UNKNOWN GENERATION OPTION "',A4,&
     &       '" AT ELEMENT "',A8,'" SOURCE "',A5,'"')
 6006 FORMAT(' NUMBER OF TABULAR GENERATION DATA SPECIFIED IN DATA',&
     &       ' BLOCK "GENER" EXCEEDS ALLOWABLE MAXIMUM OF ',I5/&
     &       ' INCREASE PARAMETER "MGTAB" IN MAIN PROGRAM,',&
     &       ' AND RECOMPILE',//,&
     &       ' >>>>>>>>>>  S K I P   F L O W   S I M U L A T I O N  ',&
     &       ' <<<<<<<<<<')
!
 6103 FORMAT(T2,'Memory allocation of "Well_TData(n)%p_TList, ',&
     &          'Well_TData(n)%p_QList" in subroutine ',&
     &          '�READ_FileGENER� was successful')
 6104 FORMAT(//,20('ERROR-'),//,&
     &       T2,'Memory allocation of "Well_TData(n)%p_TList, ',&
     &          'Well_TData(n)%p_QList" in subroutine ',&
     &          '"READ_FileGENER" was unsuccessful',//,&
     &       T2,'!!!!!!!    THE EXECUTION WAS STOPPED    !!!!!!!',&
     &       //,20('ERROR-'))
!
 6105 FORMAT(T2,'Memory allocation of �p_HList� in subroutine ',&
     &          '�READ_FileGENER� was successful')
 6106 FORMAT(//,20('ERROR-'),//,&
     &       T2,'Memory allocation of �p_HList� in subroutine ',&
     &          '"READ_FileGENER" was unsuccessful',//,&
     &       T2,'!!!!!!!    THE EXECUTION WAS STOPPED    !!!!!!!',&
     &       //,20('ERROR-'))
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of READ_FileGENER
!
!
      RETURN
! 
  END SUBROUTINE READ_FileGENER_Master
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE READ_FileINCON
! 
! ...... Modules to be used 
! 
         USE Basic_Param
         USE GenControl_Param
! 
		 USE MPI_Param
		 USE MPI_ARRAYS
		 USE MPI_SUBROUTINES
!
         USE Element_Arrays
         USE Variable_Arrays
         USE PFMedProp_Arrays
         USE TempStoFlo_Arrays, ONLY: DEPU
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*     ROUTINE FOR READING THE DATA DESCRIBING THE DOMAIN ELEMENTS     *
!*                   DIRECTLY FROM THE FILE �MESH�                     *
!*                                                                     *
!*                   Version 1.0 - October 1, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: PORX
			REAL(KIND  = 8) :: z_dis
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0,IERR
! 
      INTEGER :: i,j,N,NMX,NLOC,JLOC,n_up,No_UnkN,StIndx,n1

! 
! -------
! ... Character variables
! -------
! 
      CHARACTER(LEN = 3) :: State_id
      CHARACTER(LEN = 5) :: DENT,EL
      CHARACTER(LEN = 8) :: EL8_0
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of READ_FileINCON
!
!
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .And. MPI_RANK==0) WRITE(11,6000)                                      
! 
      No_UnkN = 0
! 
! ----------
! ... Reading the headings of the file �INCON�
! ----------
! 
      CALL MPI_FILE_SEEK(IM_1,ZeroOffset,MPI_SEEK_SET,IERR)
	  CALL MPI_READ_FROM_FILE(IM_1,FBUFF,6)
      READ(FBUFF,5001) DENT
!
!***********************************************************************
!*                                                                     *
!*       Assign generic initial conditions and modify them using       *
!*              the START and INDOM keywords/data blocks               *
!*                                                                     *
!***********************************************************************
!
      IF_RandmOrdrIC: IF(RandmOrdrIC .EQV. .TRUE.) THEN     ! START keyword is present 
! 
         elem(1:NELL)%phi = media(elem(1:NELL)%MatNo)%Poros   ! Assign porosities - CAREFUL! Whole array operation
! 
! 
! 
         DO_NumEle: DO n=1,NELL
!
            NLOC   = (n-1)*NK1                         ! Determine the location in the arrays
					  
            elem(n)%phi = media(elem(n)%MatNo)%Poros   ! Assign porosities
! 
! -------------
! ......... Assigning uniform initial conditions 
! -------------
! 
            IF_InUn: IF(YIN(1,elem(n)%MatNo) == 0.0D0) THEN
! 
               elem(n)%StateIndex = GenStIndx  ! Assign initial state index
							 
								
               X(NLOC+1:NLOC+NK1) = DEP(1:NK1) ! Assign initial conditions
                                               ! CAREFUL: Whole array operation !!!

					   
! 
! -------------
! ......... Assign domain-specific initial conditions (INDOM block present)
! -------------
! 
            ELSE
!
               elem(n)%StateIndex = StateIndex(elem(n)%MatNo) ! Assign initial state index
               X(NLOC+1:NLOC+NK1) = YIN(1:NK1,elem(n)%MatNo)  ! Assign initial conditions
!                                                             ! CAREFUL: Whole array operation !!!
            END IF IF_InUn
!
         END DO DO_NumEle
!
      END IF IF_RandmOrdrIC
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>           ELEMENT LOOP FOR ASSIGNING INITIAL CONDITIONS             >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      IF(RandmOrdrIC .EQV. .FALSE.) THEN
         n_up = NEL+1                  ! When restart info is read from the INCON file
      ELSE
         n_up = 10000000               ! When initial conditions are read 
      END IF                           !    with the START parameter
! 
! 
! 
      DO_NumEle1: DO n=1,n_up
! 
! -------------
! ...... Read the element data from the INCON block 
! -------------
! 
         IF_NumCh: IF(No_CEN == 8) THEN   
! 
		
		  CALL MPI_READ_FROM_FILE(IM_1,FBUFF,36)
		  READ(FBUFF,5003) EL8_0,  & ! 8-character element name
     &                   PORX, &   ! Porosity
     &                   State_id ! State identifier
            EL = EL8_0(1:5)
! 
         ELSE
! 
		  
			CALL MPI_READ_FROM_FILE(IM_1,FBUFF,36)
            READ(FBUFF,5002) EL,  &    ! 5-character element name
     &                   PORX, &   ! Porosity
     &                   State_id ! State identifier
            EL8_0 = EL//'   '
! 
         END IF IF_NumCh
! 
! -------------
! ...... If EL is blank, the element list is exhausted
! -------------
! 
         IF(EL == '     ') THEN
            GO TO 2000                                ! Done; exit the routine
         END IF
! 
! -------------
! ...... If EL = �+++  �, read restart data
! -------------
! 
         IF(EL == '+++  ') THEN
! 
			
			CALL MPI_READ_FROM_FILE(IM_1,FBUFF,46)
            READ(FBUFF,5008) KCYC,ITERC,NMX,TSTART,TIMIN
! 
            IF(nmx /= N_PFmedia .AND. MPI_RANK==0) THEN                ! Interrogate number of domains provided by INCON
               PRINT 6005, nmx,N_PFmedia,N_PFmedia   ! Print warning message  
            END IF
! 
            IF(TIMAX /= 0.0D0 .AND. TIMIN >= TIMAX) TIMIN = 0.0D0
! 
            GO TO 2000                               ! Done; exit the routine
! 
         END IF
! 
! -------------
! ...... Determine the state index
! -------------
! 
      Case_State: SELECT CASE(State_id)
! 
! ... Single phase: Gas           
!    
      CASE('Gas')
!    
         StIndx = 1                     
! 
! ... Single phase: Aqueous           
!    
      CASE('Aqu')
!    
         StIndx = 2                     
! 
! ... Two phases: Aqueous+Gas   
!    
      CASE('AqG')
!    
         StIndx = 3                     
! 
! ... Two phases: Ice+Gas   
!    
      CASE('IcG')
!    
         StIndx = 4                     
! 
! ... Two phases: Aqueous+Hydrate    
!    
      CASE('AqH')
!    
         StIndx = 5                     
! 
! ... Two phases: Ice+Hydrate    
!    
      CASE('IcH')
!    
         StIndx = 6                     
! 
! ... Three phases: Aqueous+Hydrate+Gas    
!    
      CASE('AGH')
! 
         StIndx = 7                     
! 
! ... Three phases: Aqueous+Ice+Gas    
!    
      CASE('AIG')
! 
         StIndx = 8                     
! 
! ... Three phases: Aqueous+Ice+Hydrate    
!    
      CASE('AIH')
! 
         StIndx = 9                     
! 
! ... Three phases: Gas+Ice+Hydrate    
!    
      CASE('IGH')
! 
         StIndx = 10                     
! 
! ... Quadruple point: Gas+Ice+Hydrate+Aqueous    
!    
      CASE('QuP')
! 
         StIndx = 11                     
!    
! 
! ... Otherwise: Error ...    
!    
      CASE DEFAULT 
!    
            WRITE(6,6100) n,State_id
            STOP
!    
      END SELECT Case_State
! 
! -------------
! ...... Read the initial conditions - primary variables
! -------------
! 
		 
		CALL MPI_READ_FROM_FILE(IM_1,FBUFF,81)
         READ(FBUFF,5005) (DEPU(i), i=1,NK1)
! 
! -------------
! ...... Assign initial conditions for restart
! -------------
! 
         IF_OrdrIC: IF(RandmOrdrIC .EQV. .FALSE.) THEN
! 
			if(procnum(n)/=MPI_RANK) CYCLE
			n1 = locnum(n)
            NLOC = (n1-1)*NK1                       ! Determine pointer
            IF(PORX /= 0.0D0) elem(n1)%phi = PORX   ! Override porosity 
            elem(n1)%StateIndex            = StIndx ! Assign state index
! 
            X(NLOC+1:NLOC+NK1) = DEPU(1:NK1)       ! Assign/override initial conditions
!                                                  ! CAREFUL! Whole array operation
! 
! -------------
! ...... Determine the element number and assign initial conditions (START option)
! -------------
! 
         ELSE
! 
            DO_NumEle2: DO j=1,NELL
! 
               IF(No_CEN == 8) THEN
                  IF(elem(j)%name /= EL8_0) CYCLE DO_NumEle2
               ELSE
                  IF(elem(j)%name(1:5) /= EL) CYCLE DO_NumEle2
               END IF
! 
               JLOC = (j-1)*NK1                       ! Determine pointer
               IF(PORX /= 0.0D0) elem(j)%phi = PORX   ! Override porosity 
               elem(j)%StateIndex            = StIndx ! Assign state index
! 
               X(JLOC+1:JLOC+NK1) = DEPU(1:NK1)       ! Assign/override initial conditions
!                                                     ! CAREFUL! Whole array operation
               CYCLE DO_NumEle1
!                                            
            END DO DO_NumEle2
! 
          END IF IF_OrdrIC
!
! <<<                      
! <<< End of the ELEMENT LOOP         
! <<<
!
  END DO DO_NumEle1
!
!
!***********************************************************************
!*                                                                     *
!*             Store the state index in case of Dt cutback             *
!*                                                                     *
!***********************************************************************
! 
! 
 2000 elem%StatePoint = elem%StateIndex   ! CAREFUL! Whole array operation
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 5001 FORMAT(A5)
!
 5002 FORMAT(A5,10X,E15.8,2x,a3)
 5003 FORMAT(A8, 7X,E15.8,2x,a3)
 5005 FORMAT(4E20.13)
 5008 FORMAT(3I5,2E15.8)
!
 6000 FORMAT(/,'READ_FileINCON    1.0    1 October   2004',6X,&
     &         'Read the initial condition data directly from ',&
     &         'the file GENER') 
 6001 FORMAT(/,' !!!!! WARNING !!!!!! ==>  INCON data at ',I5,&
     &          ' unknown elements have been ignored')
!
 6005 FORMAT(' !!!!! WARNING !!!!!! ==>',&
     &       ' The number of domains read from file INCON is',I4,/,&
     &       21x,' This is different from "N_PFmedia" =',I4,&
     &           ' provided through input data',/,&
     &       ' The latter value "N_PFmedia" =',I3,' will be used !',/)
 6008 FORMAT(' WILL IGNORE INITIAL CONDITION AT UNKNOWN ELEMENT ',A8)
!
 6100 FORMAT(//,20('ERROR-'),//,T43,&
     &             'S I M U L A T I O N   A B O R T E D',/,&
     &         T19,'In element #',i6,', the state identifier of ',&
     &             'initial conditions "State_id" = "',a3,'"',/,&
     &         T31,'This is not an available option for this ',&
     &             'Equation of State',&
     &       /,T50,'CORRECT AND TRY AGAIN',     &
     &       //,20('ERROR-'))
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of READ_FileINCON
!
!
      RETURN
! 
      END SUBROUTINE READ_FileINCON
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


 SUBROUTINE Read_partitionMESH(IM)
! 
! ...... Modules to be used 
! 
         USE GenControl_Param, ONLY: NoFloSimul,NST
		 USE MPI_PARAM
		 USE MPI_SUBROUTINES
		 USE Connection_Arrays
		 USE BASIC_PARAM
		 USE MPI_ARRAYS
		 IMPLICIT NONE
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: IM
! 
      INTEGER :: ICALL = 0, Edges, ierr,i,j,k,l
! 
	  LOGICAL :: EX
! -------
! ... Logical variables
! -------
! 
		CHARACTER(LEN=14) :: filenm,Num_proc
! 
      SAVE ICALL
		 
		 
		 
!---------First read ELEM from MESH --------

      IF(MPI_RANK==0) CALL READ_ElemMESH_MASTER(IM)
	  
	  TEMP_INT(1) = NEL
	  TEMP_INT(2) = NELA
	  TEMP_INT(3) = NST
	  TEMP_LOGICAL(1) = NoFloSimul
	  call MPI_BROADCAST(INT_ARRAY = TEMP_INT(1:3), INT_SIZE = 3 ,&
						LOGICAL_ARRAY = TEMP_LOGICAL, logical_size=1, ROOT=0)
	  NEL = TEMP_INT(1)
	  NELA = TEMP_INT(2)
	  NST =  TEMP_INT(3)
	  NoFloSimul = TEMP_LOGICAL(1)
	  
	  IF(NoFloSimul .EQV. .TRUE.) RETURN
	  
! 
	  
!---------Next read CONNE from MESH---------

      CALL READ_ConxMESH_MASTER(IM)
	  
	  IF(NoFloSimul .EQV. .TRUE.) RETURN
	  

!---------Check if partition file exists. If not, write in METIS_ARRAYS.dat required arrays for partition and stop--------
	  
	  write (filenm,"(I10)" ),MPI_np
	  filenm =trim("graph.part."//adjustl(filenm))
	  
      INQUIRE(FILE=filenm,EXIST = EX)
	  
	  EX1: IF(.NOT. EX) THEN
	  
		  IF(MPI_RANK==0) THEN	
				IF(MPI_NP==1) THEN
					OPEN(802,FILE=filenm)
					WRITE(802,*) , (/ (0, i=1,NELA)/)
					CLOSE(802)
				ELSE
					!***************************************************************
					!*****Writing graph data in METIS compatible input file*********
					!***************************************************************
						
					INQUIRE(FILE="graph",EXIST = EX)
					
					EX2: IF(.NOT. EX) THEN
						OPEN(801,FILE="graph")  
						REWIND 801
						!Determining the number of edges in the graph excluding the inactive elements
						Edges = 0
						DO j=1,NCON
							if(conx(j)%n1<=NELA .and. conx(j)%n2<=NELA  .and. &
								conx(j)%n1>0 .and. conx(j)%n2>0) Edges=Edges+1
						END DO
						
						!Writing the header
						write(801,*), NELA,Edges
						!Writing the graph lines
						!For 'i'th line in the file, write all the elements connected to 'i'th element (except all the elements <i)
						call write_graph
						
						close(801)
					END IF EX2
					
					!Converting number of processes to string
					write(Num_proc,'(1I14)'), MPI_NP
					Num_proc = trim(adjustl(Num_proc))
					!CALLING METIS PARTITION PROGRAM USING CALL TO SYSTEM
					ierr = system('gpmetis graph '//Num_proc)
					if(ierr/=0) then
						print *, "ERROR in METIS partitioning algorithm at 77PX"
						stop
					end if 
				END IF
		  END IF
	  END IF EX1
	  
	 CALL MPI_BARRIER(MPI_COMM_WORLD,IERR)		!Wait while master completes partitioning
	  
	 !READ THE PARTITION DATA
	 OPEN(802,FILE = filenm)
	 
	 call PART_COMM !Partition and communicate the arrays required for communication

	  
END SUBROUTINE Read_partitionMESH

SUBROUTINE WRITE_GRAPH

USE MPI_PARAM
USE BASIC_PARAM
USE Connection_Arrays

IMPLICIT NONE

INTEGER :: I,J
INTEGER :: Num_CONX(1:NELA)
INTEGER , ALLOCATABLE :: GRAPH(:,:)
INTEGER :: MAX_CONX,IERROR


Max_CONX=0
Num_CONX = 0 !WARNING: WHOLE ARRAY OPERATION

DO i=1,NCON
	if(conx(i)%n1>NELA .OR. conx(i)%n2>NELA .or. conx(i)%n1<1 .or. conx(i)%n2<1) CYCLE
	Num_CONX(conx(i)%n1)=Num_CONX(conx(i)%n1)+1
	Num_CONX(conx(i)%n2)=Num_CONX(conx(i)%n2)+1	
END DO
Max_CONX = maxval(Num_CONX)

allocate(GRAPH(NELA,Max_CONX),STAT = IERROR)
if(IERROR/=0) print *, "ERROR IN ALLOCATION AT 2288XX"

Num_CONX = 0
DO i=1,NCON
	if(conx(i)%n1>NELA .OR. conx(i)%n2>NELA .or. conx(i)%n1<1 .or. conx(i)%n2<1) CYCLE
	Num_CONX(conx(i)%n1)=Num_CONX(conx(i)%n1)+1
	GRAPH(conx(i)%n1,Num_CONX(conx(i)%n1)) = conx(i)%n2
	
	Num_CONX(conx(i)%n2)=Num_CONX(conx(i)%n2)+1
	GRAPH(conx(i)%n2,Num_CONX(conx(i)%n2)) = conx(i)%n1
END DO

DO i=1,NELA
	DO J=1,Num_CONX(i)
		write(801,'(I10)',advance="no"), GRAPH(i,j)
	END DO
	write(801,"(X)")
END DO

deallocate(GRAPH)
END SUBROUTINE WRITE_GRAPH

      SUBROUTINE READ_ElemMESH_MASTER(IM)
! 
! ...... Modules to be used 
! 
         USE Basic_Param
         USE GenControl_Param
         USE TimeSeriesP_Param
! 
         USE Element_Arrays
         USE PFMedProp_Arrays
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*     ROUTINE FOR READING THE DATA DESCRIBING THE DOMAIN ELEMENTS     *
!*                   DIRECTLY FROM THE FILE 'MESH'                     *
!*                                                                     *
!*                  Version 1.00 - August 19, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: SCALEA,SCALEV
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: IM
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: N,M,matmat,i,i_ASCII
! 
! -------
! ... Character variables
! -------
! 
      CHARACTER(LEN = 5) :: MA12,DENT
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: Medium_ByName
      LOGICAL :: Active_Element
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of READ_ElemMESH
!
!
      ICALL = ICALL+1                                                     
      IF(ICALL == 1) WRITE(11,6000)                                      
! 
! ----------
! ... Reading the headings of the file �MESH�
! ----------
! 
      REWIND IM
      READ(IM,5001) DENT
! 
! ----------
! ... Initialization
! ----------
! 
      NELA = 0                  ! The number of active gridblocks
! 
      Active_Element = .FALSE.  ! The flag indicating that the # of active  
                                ! elements has been determined
! 
! ----------
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                       MAIN ELEMENT LOOPS                            >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_NumEle1: DO n=1,10000000
! 
! -------------
! ...... Check if "MaxNum_Elem" is exceeded
! -------------
! 
         IF(n > MaxNum_Elem) THEN
            WRITE(6,6001) MaxNum_Elem
            NoFloSimul = .TRUE.              ! Bypass simulation
            RETURN
         END IF
! 
! ...... Initialize the element name
! 
         elem(n)%name = '        '
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For 5-character elements 
! ...... 
! >>>>>>>>>>>>>>>>>>
! 
! 
         IF_NumCh: IF(No_CEN == 5) THEN   ! For 5-character elements
! 
! ----------------
! ......... Read the element data from the ELEME block of MESH
! ----------------
! 
            READ(IM,5001) elem(n)%name(1:5), &! 5-Character element name
     &                    MA12,          &    ! 5-Character name of corresponding rock type
     &                    elem(n)%vol,    &   ! Element volume (m^3)
     &                    AHT(n),        &    ! Interface area of heat exchange with semi-infinite confining bed(m^2)
     &                    pm(n),         &    ! Permeability multiplier
     &                    X_coord(n),    &    ! X-coordinate element center (m)
     &                    Y_coord(n),    &    ! Y-coordinate of element center (m)
     &                    Z_coord(n)         ! Z-coordinate of element center (m)
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... For 8-character elements 
! ...... 
! >>>>>>>>>>>>>>>>>>
! 
! 
         ELSE IF(No_CEN == 8) THEN   
! 
! ----------------
! ......... Read the element data from the ELEME block of MESH
! ----------------
! 
            READ(IM,5002) elem(n)%name,& ! 5-Character element name
     &                    MA12,      &   ! 5-Character name of corresponding rock type
     &                    elem(n)%vol,&  ! Element volume (m^3)
     &                    AHT(n),  &     ! Interface area of heat exchange with semi-infinite confining bed(m^2)
     &                    pm(n),   &     ! Permeability multiplier
     &                    X_coord(n),&   ! X-coordinate element center (m)
     &                    Y_coord(n), &  ! Y-coordinate of element center (m)
     &                    Z_coord(n)    ! Z-coordinate of element center (m)
! 
! 
! >>>>>>>>>>>>>>>>>>
! ......
! ...... Otherwise 
! ...... 
! >>>>>>>>>>>>>>>>>>
! 
! 
         ELSE                        ! If we get here, there must have been a mistake
! 
            WRITE(6,6801) No_CEN     ! Print error message
            STOP                     ! Simulation is aborted
! 
! <<<<<<<
! <<<<<<<
! <<<<<<<
! 
         END IF IF_NumCh
! 
! 
!***********************************************************************
!*                                                                     *
!*                Processing the element-specific data                 *
!*                                                                     *
!***********************************************************************
! 
! -------------
! ...... If the element name is blank, the element list is exhausted
! -------------
! 
         IF_ElEnd: IF(elem(n)%name(1:5) == '     ') THEN  
! 
            NEL = n-1
            IF(NELA == 0) NELA = NEL
            EXIT DO_NumEle1
! 
         END IF IF_ElEnd        
! 
! -------------
! ...... Determine the number of active elements
! -------------
! 
         IF_ElAct0: IF((n > 1) .AND. &
     &                 (Active_Element .EQV. .FALSE.))&
     &   THEN 
! 
            IF_ElAct1: IF((elem(n)%vol  <= 0.0d0) .AND. &
     &                    (elem(n-1)%vol > 0.0d0)) &
     &      THEN  
! 
               NELA           =  n-1
               Active_Element = .TRUE.  ! Reset the active element flag 
! 
            END IF IF_ElAct1        
! 
         END IF IF_ElAct0         
! 
! -------------
! ...... Print the coordinates of active elements in file �Plot_Coord�
! -------------
! 
         WRITE(40,6002) X_coord(n),Y_coord(n),Z_coord(n)
! 
!
      END DO DO_NumEle1
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 5001 FORMAT(A5,10X,A5,6(E10.4))
 5002 FORMAT(A8, 7X,A5,6(E10.4))
!
!
 6000 FORMAT(/,'READ_ElemMESH_MASTER     1.0   22 MARCH 2017',6X,&
     &         'Read the element data from the ELEME block of ',&
     &         'the file MESH in MASTER') 
!
 6001 FORMAT(' NUMBER OF ELEMENTS SPECIFIED IN DATA BLOCK "ELEME"',&
     &       ' EXCEEDS ALLOWABLE MAXIMUM OF ',I7/&
     &       ' INCREASE PARAMETER "MaxNum_Elem" IN MAIN PROGRAM,',&
     &       ' AND RECOMPILE',//,&
     &       ' >>>>>>>>>>   F L O W   S I M U L A T I O N  ',&
     &       ' A B O R T E D   <<<<<<<<<<')
 6002 FORMAT(3(1pE15.8,1x))
!
 6004 FORMAT(' REFERENCE TO UNKNOWN MATERIAL ',A5,' AT ELEMENT "',A8,&
     &       '" ==> WILL IGNORE " ')
 6005 FORMAT(' !!!!! ==> WARNING <== !!!!! :',&
     &       ' ELEMENT "',A8,'" HAS NO DOMAIN ASSIGNMENT;',&
     &       ' ASSIGN TO DOMAIN # 1, "',A5,'"')
!
 6801 FORMAT(//,20('ERROR-'),//,T33,&
     &             '       S I M U L A T I O N   A B O R T E D',/,&
     &         T5,'The number of charcters of the element names ',&
     &            'No_CEN = ',i3,' is not an acceptable number ',&
     &            '(must be either 5 or 8)',&
     &       /,T33,'               CORRECT AND TRY AGAIN',     &
     &       //,20('ERROR-'))
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of READ_ElemMESH
!
!
      RETURN
! 
      END SUBROUTINE READ_ElemMESH_MASTER
!

 SUBROUTINE READ_ConxMESH_MASTER(IM)
! 
! ...... Modules to be used 
! 
		 USE MPI_PARAM
		 USE MPI_SUBROUTINES
         USE Basic_Param
         USE GenControl_Param
         USE TimeSeriesP_Param
! 
         USE Element_Arrays
         USE Connection_Arrays
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*   ROUTINE FOR READING THE DATA DESCRIBING THE DOMAIN CONNECTIONS    *
!*                   DIRECTLY FROM THE FILE �MESH�                     *
!*                                                                     *
!*                   Version 1.00, August 20, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: SCALEA
! 
! -------
! ... Integer arrays
! -------
! 
      INTEGER, DIMENSION(3) :: isumd = 0
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: IM
! 
      INTEGER :: ICALL  = 0
! 
      INTEGER :: i,j,N,ipluss
      INTEGER :: n_RadHt,n_ConHt,N_invalid_elem
! 
! -------
! ... Character variables
! -------
! 

      CHARACTER(LEN = 5) :: EL5_CR,DENT
! 

	  
	  
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of READ_ConxMESH
!
!
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .AND. MPI_RANK==0) WRITE(11,6000)                                      
! 
! ----------
! ... Initializations
! ----------
! 
      NCON   = 0
      ipluss = 0
! 
      isumd  = 0  ! CAREFUL! Whole array operation
! 
! ... Reading the headings of the block �CONNE�
! ----------
! 
      
	 if(MPI_RANK==0) READ(IM,5001) DENT
! 
! ... Initialization - CAREFUL! Whole array operations 
! 
	 IF(MPI_RANK==0) THEN
         conx%n1   = 0
         conx%n2   = 0
         conx%nam1 = '        '
         conx%nam2 = '        '
	 END IF
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                       MAIN CONNECTION LOOP                          >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
if(MPI_RANK==0) then
		DO_NumCon: DO n=1,10000000
	! 
	! -------------
	! ...... Check if "MaxNum_Conx" is exceeded
	! -------------
	! 
			 IF(n > MaxNum_Conx) THEN
				PRINT 6001, MaxNum_Conx
				NoFloSimul = .TRUE.        ! Bypass simulation
				EXIT
			 END IF
	! 
	! 
	! 
			 IF_NumCh: IF(No_CEN == 8) THEN   
	! 
	! ----------------
	! ......... Read the element data from the CONNE block of MESH - 8-character elements
	! ----------------
	! 
				READ(IM,5002) conx(n)%nam1, & ! 8-Character name of connection element #1  
		 &                    conx(n)%nam2, & ! 8-Character name of connection element #2
		 &                    conx(n)%ki,   & ! Directional indicator of the permeability tensor
		 &                    conx(n)%d1,  &  ! Distance of the center of element #1 from interface (m)
		 &                    conx(n)%d2,  &  ! Distance of the center of element #2 from interface (m)
		 &                    conx(n)%area,&  ! Interface area (m^2)
		 &                    conx(n)%beta, & ! = COS(b), b = angle between the vertical and the connection line 
		 &                    sig(n)         ! Radiant emittance factor for radiative heat transfer
	! 
				EL5_CR = conx(n)%nam1(1:5)
	! 
			 ELSE
	! 
	! ----------------
	! ......... Read the element data from the CONNE block of MESH - 5-character elements
	! ----------------
	! 
				READ(IM,5001) conx(n)%nam1(1:5), & ! 8-Character name of connection element #1  
		 &                    conx(n)%nam2(1:5), & ! 8-Character name of connection element #2
		 &                    conx(n)%ki,        & ! Directional indicator of the permeability tensor
		 &                    conx(n)%d1,        & ! Distance of the center of element #1 from interface (m)
		 &                    conx(n)%d2,        & ! Distance of the center of element #2 from interface (m)
		 &                    conx(n)%area,      & ! Interface area (m^2)
		 &                    conx(n)%beta,      & ! = COS(b), b = angle between the vertical and the connection line 
		 &                    sig(n)              ! Radiant emittance factor for radiative heat transfer
	! 
				EL5_CR = conx(n)%nam1
	! 
			 END IF IF_NumCh
	! 
	! -------------
	! ...... If EL5_CR = �+++  �, read element numbers in the connection list
	! -------------
	! 
			 IF_ConEnd1: IF(EL5_CR == '+++  ') THEN  
	! 
				NCON = n-1                                          ! Define actual NCON
	! 
				IF(No_CEN == 8) THEN
				   READ(IM,5004) (conx(i)%n1,conx(i)%n2, i=1,NCON)  ! Read connection-element # data, 8-ch. names  
				ELSE
				   READ(IM,5003) (conx(i)%n1,conx(i)%n2, i=1,NCON)  ! Read connection-element # data, 5-ch. names  
				END IF
	! 
				ipluss = 1                                          ! Set flag for determination of active connections
	! 
				EXIT                                                ! Done !
	! 
			 END IF IF_ConEnd1        
	! 
	! -------------
	! ...... If EL5_CR is blank, the element list is exhausted
	! -------------
	! 
			 IF_ConEnd2: IF(EL5_CR == '     ') THEN  
	! 
				NCON = n-1                                ! Define actual NCON
				BACKSPACE IM                              ! Move back one line in MESH
				EXIT                                      ! Done !
	! 
			 END IF IF_ConEnd2        
	! 
	! -------------
	! ...... Determine the number of connections along the principal directions
	! -------------
	! 
			 IF(conx(n)%ki == 1) isumd(1) = isumd(1)+1
			 IF(conx(n)%ki == 2) isumd(2) = isumd(2)+1
			 IF(conx(n)%ki == 3) isumd(3) = isumd(3)+1
	! 
			 IF_3to2D: IF(conx(n)%ki < 0) THEN
						  IF(conx(n)%ki == -2) THEN
							 isumd(3) = isumd(3)+1
						  ELSE
							 isumd(1) = isumd(1)+1
						  END IF
					   END IF IF_3to2D
	! 
	! -------------
	! ...... Account for scaling    
	! -------------
	! 
			 IF_Scale: IF(SCALE /= 1.0D0) THEN
						  conx(n)%d1   = conx(n)%d1*SCALE
						  conx(n)%d1   = conx(n)%d1*SCALE
						  conx(n)%area = conx(n)%area*SCALEA
					   END IF IF_Scale
	!
	! <<<                      
	! <<< End of the CONNECTION LOOP         
	! <<<
	!
		  END DO DO_NumCon
	!
END IF

TEMP_LOGICAL(1) = NoFloSimul
CALL MPI_BROADCAST(LOGICAL_ARRAY= TEMP_LOGICAL, LOGICAL_SIZE=1, root=0)
NoFloSimul = TEMP_LOGICAL(1)

if(NoFloSimul .eqv. .TRUE.) RETURN

!***********************************************************************
!*                                                                     *
!*                     DETERMINE ACTIVE DIMENSIONS                     *
!*                                                                     *
!***********************************************************************
!
	 if(MPI_RANK==0) then
		  nactdi = COUNT(isumd > 0)    ! Number of active dimensions, array operation
	! 
		  IF(nactdi /= 1 .AND. nactdi /= 2 .AND. nactdi /= 3 .AND. MPI_RANK==0) THEN
			 PRINT 6078, nactdi
			 STOP
		  END IF
	! 
	! -------------
	! ...... Determine the 2nd active direction in a 2D system (X,R is always the 1st)
	! -------------
	! 
		  IF_2D: IF(nactdi == 2) THEN
	! 
			 IF(isumd(2) == 0) THEN         
				nactd2 = 3                   ! The 3rd direction is active
			 ELSE IF(isumd(3) == 0) THEN
				nactd2 = 2                   ! The 2nd direction is active
			 ELSE
				IF(MPI_RANK==0) PRINT 6079, nactd2
			 END IF
	! 
		  END IF IF_2D
	! 
		  PRINT 6080, nactdi
		  PRINT 6081, isumd(1),isumd(2),isumd(3)
	 end if
	  temp_int(1)=nactdi
	  temp_int(2)=nactd2
	  temp_int(3)=NCON
	  call MPI_BROADCAST(INT_ARRAY = temp_int(1:3),INT_SIZE=3,root=0)
	  nactdi = temp_int(1)
	  nactd2 = temp_int(2)
	  NCON = temp_int(3)
	  

!
!***********************************************************************
!*                                                                     *
!*  DETERMINE IF RADIATIVE & CONDUCTIVE HEAT TRANSPORT IS CONSIDERED   *
!*                                                                     *
!***********************************************************************
!
	  if(MPI_RANK==0) THEN
      n_RadHt = COUNT(ABS(sig(1:NCON)) > 1.0d-10)      ! Count occurrences of SIGMA /= 0  
      n_ConHt = COUNT(sig(1:NCON) < 0.0d0)             ! Count occurrences of SIGMA < 0  
! 
      IF(n_RadHt > 0) RadiHeat_Flag = .TRUE.   ! Reset the flag for radiative heat transport
      IF(n_ConHt > 0) CondHeat_Flag = .FALSE.  ! Reset the flag for conductive heat transport
!
	  END IF
	  
	  temp_logical(1)=RadiHeat_Flag
	  temp_logical(2)=CondHeat_Flag
	  call MPI_BROADCAST(LOGICAL_ARRAY = temp_logical(1:2),logical_SIZE=2,root=0)
	  RadiHeat_Flag = temp_logical(1)
	  CondHeat_Flag = temp_logical(2)

!***********************************************************************
!*                                                                     *
!*    DETERMINE IF THE ELEMENT NUMBERS OF THE CONNECTIONS ARE KNOWN    *
!*                                                                     *
!***********************************************************************
!
if(MPI_RANK == 0) THEN
		  IF(ipluss == 1) THEN              ! When the connection element numbers are known  
	! 
			 IF(FLG_con == 'ACTIVE') THEN   ! When the run aims to determine active connections only
				CALL ACTIV_Conexns          !    Determine active connections  
				RETURN                      !    Done !
			 ELSE
				RETURN                      ! Done!
			 END IF
	! 
		  END IF
	!
	!***********************************************************************
	!*                                                                     *
	!*          DETERMINE THE ELEMENT NUMBERS OF THE CONNECTIONS           *
	!*        CAREFUL! W h o l e   A r r a y   O p e r a t i o n s         *
	!*                                                                     *
	!***********************************************************************
	!
		  DO n = 1,NEL
			 WHERE(conx%nam1 == elem(n)%name) conx%n1 = n  ! Identify the 1st elements in the connections    
			 WHERE(conx%nam2 == elem(n)%name) conx%n2 = n  ! Identify the 2nd elements in the connections    
		  END DO
	!
		  N_invalid_elem = COUNT(conx%n1 == 0)+COUNT(conx%n2 == 0)  ! Total # of invalid elements in connections
	!
		  IF(N_invalid_elem /= 0) THEN                              ! Print warnings and continue
			 DO j = 1,NCON
				IF(conx(j)%n1 == 0) WRITE(6,6008) conx(j)%nam1,j
				IF(conx(j)%n2 == 0) WRITE(6,6008) conx(j)%nam2,j
			 END DO
		  END IF
	!
	!***********************************************************************
	!*                                                                     *
	!*           WRITE THE ELEMENT NUMBERS OF THE CONNECTIONS              *
	!*                  AT THE END OF THE �MESH� FILE                      *
	!*                                                                     *
	!***********************************************************************
	!
		  WRITE(IM,6002)      ! Write �+++  � at the end of MESH
	! 
		  IF_ChEl: IF(No_CEN == 8) THEN
					  WRITE(IM,5004) (conx(i)%n1,conx(i)%n2, i=1,NCON)
				   ELSE
					  WRITE(IM,5003) (conx(i)%n1,conx(i)%n2, i=1,NCON)
				   END IF IF_ChEl
	!
		  ENDFILE IM
	!
	!
END IF
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 5001 FORMAT(2A5,15X,I5,5E10.4)
 5002 FORMAT(2A8, 9X,I5,5E10.4)
!
 5003 FORMAT(16I5)
 5004 FORMAT(10I8)
!
 6000 FORMAT(/,'READ_ConxMESH     1.0   20 August    2004',6X,&
     &         'Read the connection data from the CONNE block of ',&
     &         'the file MESH') 
!
 6001 FORMAT('NUMBER OF CONNECTIONS SPECIFIED IN DATA BLOCK "CONNE"',&
     &       ' EXCEEDS ALLOWABLE MAXIMUM OF ',I8,/&
     &       ' INCREASE PARAMETER "MaxNum_Conx" IN MAIN PROGRAM',&
     &       ' AND RECOMPILE',//,&
     &       ' !!!!! ==> ==> ==> FLOW SIMULATION IS ABORTED')
 6002 FORMAT('+++  ')
!
 6004 FORMAT(' REFERENCE TO UNKNOWN MATERIAL ',A5,' AT ELEMENT "',A8,&
     &       '" ==> WILL IGNORE ELEMENT')
 6005 FORMAT(' !!!!! ==> WARNING <== !!!!! :',&
     &       ' ELEMENT "',A8,'" HAS NO DOMAIN ASSIGNMENT;',&
     &       ' ASSIGN TO DOMAIN # 1, "',A5,'"')
 6008 FORMAT(' REFERENCE TO UNKNOWN ELEMENT ',A8,' AT CONNECTION ',&
     &       I6,' ==> WILL IGNORE ')

 6078 FORMAT(//,20('ERROR-'),//,T33,&
     &             '       S I M U L A T I O N   A B O R T E D',/,&
     &         T9,'The determined number of active dimensions',&
     &             ' NACTDI = ',i1,&
     &             ' is not an acceptable number (must be 1, 2 or 3)',&
     &       /,T33,'               CORRECT AND TRY AGAIN',     &
     &       //,20('ERROR-'))
 6079 FORMAT(//,20('ERROR-'),//,T33,&
     &             '       S I M U L A T I O N   A B O R T E D',/,&
     &         T10,'The index of the second active dimension',&
     &             ' NACTD2 = ',i2,&
     &             ' is not an acceptable number (must be 2 or 3)',&
     &       /,T33,'               CORRECT AND TRY AGAIN',     &
     &       //,20('ERROR-'))
 6080 FORMAT(//,130('*'),/,'*',T130,'*',/,'*',&
     &       T10,'The number of active dimensions NACTDI =',i2,&
     &       T130,'*',/,'*',T130,'*')
 6081 FORMAT('*',T10,'The numbers of connections in the X-, Y- ',&
     &               'and Z-directions are ',i6,', ',i6,' and ',i6,&
     &               ' respectively',T130,'*',&
     &             /,'*',T130,'*',/,130('*'),//)
!
 6801 FORMAT(//,20('ERROR-'),//,T33,&
     &             '       S I M U L A T I O N   A B O R T E D',/,&
     &         T5,'The number of charcters of the element names ',&
     &            'No_CEN = ',i3,' is not an acceptable number ',&
     &            '(must be either 5 or 8)',&
     &       /,T33,'               CORRECT AND TRY AGAIN',     &
     &       //,20('ERROR-'))
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of READ_ConxMESH
!
!
      RETURN
! 
      END SUBROUTINE READ_ConxMESH_MASTER
!
!
subroutine PART_COMM
	
	USE BASIC_PARAM
	USE MPI_PARAM
	USE MPI_SUBROUTINES
	USE MPI_ARRAYS
	USE Connection_Arrays
	
	IMPLICIT NONE
	
	INTERFACE 
	 function insert_update(update_g0,update_p0,update_l0,NUL,K, K_P,K_L)

		integer :: insert_update,NUL,K,K_P,K_L
		integer :: update_g0(*),update_p0(*),update_l0(*)
		integer :: i
	 end function
	END INTERFACE
	
	INTEGER :: I,J,K, NL,II,JJ,KK,ierror(10)=0
	INTEGER, ALLOCATABLE, dimension(:) :: TNUL,TNCONI,TNCONE,INTARRAY
	INTEGER :: P1,P2,L(11)
	INTEGER :: IERR,STAS(MPI_STATUS_SIZE)
	
	
	allocate(procnum(NEL))
	allocate(locnum(NEL))
	allocate(COUNTI(0:MPI_NP-1))
	
	procnum = 0
	READ(802,*), procnum(1:NELA)
	!----Find array locnum-----
	COUNTI(0:MPI_NP-1)=0
	DO I=1,NELA
		COUNTI(procnum(I)) = COUNTI(procnum(I)) + 1
		locnum(I) = COUNTI(procnum(I))
	END DO
	
	IF(MPI_RANK==0) THEN
		allocate(TNUL(0:MPI_NP-1))
		allocate(TNCONI(0:MPI_NP-1))
		allocate(TNCONE(0:MPI_NP-1))
		
		
		TNUL=0
		TNCONI=0
		TNCONE=0
		DO I=1,NCON
			J = conx(i)%n1
			K = conx(i)%n2
			!Check if any of J or K is inactive
			if(J>NELA .OR. K>NELA) THEN	
				if(J>NELA .and. K<=NELA) THEN
					TNCONI(procnum(K))=TNCONI(procnum(K))+1
					cycle
				elseif (K>NELA .and. J<=NELA) THEN
					TNCONI(procnum(J))=TNCONI(procnum(J))+1
					cycle
				elseif (K>NELA .and. J>NELA) THEN
					TNCONI = TNCONI + 1
					CYCLE
				end if
			end if
			
			if(procnum(J)==procnum(K)) THEN
				!INTERNAL CONNECTION
				TNCONI(procnum(J))=TNCONI(procnum(J))+1
			else
				TNCONE(procnum(J))=TNCONE(procnum(J))+1
				TNCONE(procnum(K))=TNCONE(procnum(K))+1
			end if
		END DO
		
		L(1) = 5						!For update_p0
		L(3) = L(1) + maxval(TNCONE)		!For update_g0
		L(4) = L(3) + maxval(TNCONE)	!For update_l0
		L(5) = L(4) +  maxval(TNCONE)	!For CONX_E1
		L(6) = L(5) +  maxval(TNCONE)	!For CONX_E2
		L(7) = L(6)+  maxval(TNCONE)	!For CONX_I1
		L(8) = L(7)+  maxval(TNCONI)	!For CONX_I2
		L(9) = L(8) + maxval(TNCONI)    !For global conx num of internal conx
		L(10) = L(9) + maxval(TNCONI)	!For global conx num of external conx
		L(11) = L(10) + maxval(TNCONE)-1 !For total size
		allocate (INTARRAY(L(11)*MPI_NP))
		
		TNUL=0
		TNCONI=0
		TNCONE=0
		DO I=1,NCON
			J = conx(i)%n1
			K = conx(i)%n2						!External connection
			P1 = procnum(J)
			P2 = procnum(K)
			!Check if any of J or K is inactive
			if(J>NELA .OR. K>NELA)	THEN
				if(J>NELA .and. K<=NELA) THEN
					TNCONI(P2)=TNCONI(P2)+1
					KK = locnum(K)
					JJ = J-NELA+COUNTI(P2)
					INTARRAY(L(11)*(P2)+L(7)-1+TNCONI(P2)) = JJ
					INTARRAY(L(11)*(P2)+L(8)-1+TNCONI(P2)) = KK
					INTARRAY(L(11)*(P2)+L(9)-1+TNCONI(P2)) = I	
					cycle
				elseif (K>NELA .and. J<=NELA) THEN
					TNCONI(P1)=TNCONI(P1)+1
					JJ = locnum(J)
					KK = K-NELA+COUNTI(P1)
					INTARRAY(L(11)*(P1)+L(7)-1+TNCONI(P1)) = JJ
					INTARRAY(L(11)*(P1)+L(8)-1+TNCONI(P1)) = KK
					INTARRAY(L(11)*(P1)+L(9)-1+TNCONI(P1)) = I	
					cycle
				elseif (K>NELA .and. J>NELA) THEN
					do II=0,MPI_NP-1
						TNCONI(II)=TNCONI(II)+1
						JJ = J-NELA+COUNTI(II)
						KK = K-NELA+COUNTI(II)
						INTARRAY(L(11)*(II)+L(7)-1+TNCONI(II)) = JJ
						INTARRAY(L(11)*(II)+L(8)-1+TNCONI(II)) = KK
						INTARRAY(L(11)*(II)+L(9)-1+TNCONI(II)) = I	
					END DO
					CYCLE
				end if
			end if
			
			if(P1==P2) THEN  !Internal connection
				TNCONI(P1)=TNCONI(P1)+1
				!Changing global number to local number
				J = locnum(J)
				K = locnum(K)
				INTARRAY(L(11)*(P1)+L(7)-1+TNCONI(P1)) = J
				INTARRAY(L(11)*(P1)+L(8)-1+TNCONI(P1)) = K
				INTARRAY(L(11)*(P1)+L(9)-1+TNCONI(P1)) = I
			else	
				TNCONE(P1)=TNCONE(P1)+1
				TNCONE(P2)=TNCONE(P2)+1
			!For first element
				!Insert update element and/or return its local position in update array
				KK= insert_update( INTARRAY(L(11)*P1 +L(3)),INTARRAY(L(11)*P1 +L(1))&
				,INTARRAY(L(11)*P1 +L(4)),TNUL(P1),K,P2,locnum(K))
				JJ = locnum(J)
				INTARRAY(L(11)*(P1)+L(5)-1+TNCONE(P1)) = JJ
				INTARRAY(L(11)*(P1)+L(6)-1+TNCONE(P1)) = KK+COUNTI(P1)+NEL-NELA
				INTARRAY(L(11)*(P1)+L(10)-1+TNCONE(P1)) = I
			!For second element
				!Insert update element and/or return its local position in update array
				JJ= insert_update( INTARRAY(L(11)*P2 +L(3)),INTARRAY(L(11)*P2 +L(1))&
				,INTARRAY(L(11)*P2 +L(4)),TNUL(P2),J,P1,locnum(J))
				KK = locnum(K)
				
				INTARRAY(L(11)*(P2)+L(5)-1+TNCONE(P2)) = JJ+COUNTI(P2)+NEL-NELA	
				INTARRAY(L(11)*(P2)+L(6)-1+TNCONE(P2)) = KK
				INTARRAY(L(11)*(P2)+L(10)-1+TNCONE(P2)) = I
			end if
		END DO
		
		CALL MPI_BROADCAST(INT_ARRAY = L(1:11),INT_SIZE=11,ROOT=0)
		DO I=0,MPI_NP-1
			INTARRAY (L(11)*I + 1) = NOGN
			INTARRAY(L(11)*I + 2) = TNUL(I)
			INTARRAY(L(11)*I + 3) = TNCONI(I)
			INTARRAY(L(11)*I + 4) = TNCONE(I)
			if(I==0) CYCLE
		!Communicate the INTARRAY TO SLAVES
			CALL MPI_SEND(INTARRAY(L(11)*I+1:L(11)*(I+1)),L(11),MPI_INT,I,0,MPI_COMM_WORLD,IERR)
		END DO
		
		!Deallocate temporary arrays
		deallocate(TNUL)
		deallocate(TNCONI)
		deallocate(TNCONE)
		
		!deallocate connection array used before
		deallocate(conx)
	ELSE
		CALL MPI_BROADCAST(INT_ARRAY = L(1:11),INT_SIZE=11,ROOT=0)
		ALLOCATE (INTARRAY(L(11)))
		CALL MPI_RECV(INTARRAY,L(11),MPI_INT,0,0,MPI_COMM_WORLD,STAS,ierr)
		if(IERR/=0) THEN
			PRINT *, "ERROR IN MPI_IRECV at 88OO"
			stop
		end if
	END IF

!Find local parameters
	NELAL = COUNTI(MPI_RANK)
	NELL = NELAL+NEL-NELA			!Adding inactive elements to form NELL--total num elements
	NOGN = INTARRAY(1)
	NUL = INTARRAY(2)
	NCONI = INTARRAY(3)
	NCONE = INTARRAY(4)
	NCONT = NCONI+NCONE
	NTL = NELL+NUL
	
!Assign inactive elements in procnum and locnum in all the local procs
	procnum(NELA+1:NEL) = MPI_RANK
	FORALL(i=NELA+1:NEL) locnum(i) = i-NELA+NELAL
!Allocate global0
	allocate(global0(NEQ*NELAL))
!Allocate update arrays
	allocate(update_p(NUL))
	allocate(update_l(NUL))
	allocate(update_g(NUL))
	if(count(ierror/=0)>0) then
		print *, "ALLOCATION ERROR 99Z2"
	end if
!Find update arrays
	
	update_g(1:NUL) = INTARRAY(L(3):L(3)+NUL-1)
	update_p(1:NUL) = INTARRAY(L(1):L(1)+NUL-1)
	update_l(1:NUL) = INTARRAY(L(4):L(4)+NUL-1)
!Allocate and find conx arrays
	allocate(conx(NCONT))
	allocate(globalconx(NCONT))
	conx(1:NCONI)%n1 = INTARRAY(L(7):L(7)+NCONI-1)
	conx(1:NCONI)%n2 = INTARRAY(L(8):L(8)+NCONI-1)
	conx(NCONI+1:NCONT)%n1 = INTARRAY(L(5):L(5)+NCONE-1)
	conx(NCONI+1:NCONT)%n2 = INTARRAY(L(6):L(6)+NCONE-1)
	
	globalconx(1:NCONI)=INTARRAY(L(9):L(9)+NCONI-1)
	globalconx(NCONI+1:NCONT)=INTARRAY(L(10):L(10)+NCONE-1)
		
!Deallocate temporary array
	DEALLOCATE(INTARRAY)
	DEALLOCATE(COUNTI)
end subroutine PART_COMM


function insert_update(update_g0,update_p0,update_l0,NUL,K, K_P,K_L)

	implicit none

	integer :: insert_update,NUL,K,K_P,K_L
	integer :: update_g0(*),update_p0(*),update_l0(*)
	integer :: i
	
	DO I=1,NUL
		IF(update_g0(I) == K) EXIT
	END DO

	IF(I<NUL+1) THEN
		INSERT_UPDATE = I
		return
	ELSE
		NUL = NUL+1
		INSERT_UPDATE = NUL
		update_g0(NUL) = K
		update_p0(NUL) = K_P
		update_l0(NUL) = K_L
		return
	END IF

end function insert_update


!
!
      SUBROUTINE READ_FileGENER_loc
! 
! ...... Modules to be used 
! 
         USE Basic_Param
         USE GenControl_Param
         USE Hydrate_Param, ONLY: Inh_WMasF
! 
         USE Element_Arrays
         USE Q_Arrays
		 USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*        ROUTINE FOR READING THE SOURCE/SINK GENERATION DATA          *
!*                   DIRECTLY FROM THE FILE �GENER�                    *
!*                                                                     *
!*                  Version 1.0 - September 18, 2004                   *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL  = 0, NGL  = 0
! 
      INTEGER :: i,j,k,n,LTABA,ier,kf,non_common_SS
! 
! -------
! ... Character variables
! -------
! 
      CHARACTER(LEN = 4) :: SS_Typ
      CHARACTER(LEN = 5) :: EL5_CR,DENT
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of READ_FileGENER
!
!
      ICALL = ICALL+1                                                     
! 
! ----------
! ... Initializations
! ----------
! 
      kf   = 0
! 
! ... CAREFUL! Whole array operations
! 
      SS%name_el = '        '
      SS%el_num  = 0
! 
! ----------
! ... Reading the headings of the file �GENER�
! ----------
! 
      REWIND 3
      READ(3,5001) DENT
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                         SINK/SOURCE LOOP                            >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
	  IF(NOGN==0) RETURN 
	  
      DO_NumSS: DO n=1,NOGN+1
! 
		 
! -------------
! ...... Reading Source/Sink data
! -------------
! 
         IF_NumCh: IF(No_CEN == 8) THEN   
! 
! ----------------
! ......... Read the source/sink data from the GENER file - 8-character elements
! ----------------
! 
            READ(3,5002) SS(n)%name_el, &! 8-Character name of cell containing
     &                   SS(n)%name,    &! 5-Character name of source/sink (well name)
     &                   SS(n)%n_TableP,&! Number of points in the generation table
     &                   SS(n)%type,    &! Type of source/sink
     &                   SS(n)%rate_m,  &! Injection/production rate (kg/s) or PI for SS(n)%type = 'DELV'
     &                   SS(n)%enth,    &! Specific enthalpy of injected fluid (J/kg) or bottomhole pressure for SS(n)%type = �DELV� 
     &                   SS(n)%z_layer, &! Layer thickness (m) for SS_Type = �DELV�
     &                   Inh_WMasF(n)   ! Mass fraction of inhibitor in injected H2O
! 
            EL5_CR = SS(n)%name_el
! 
         ELSE
! 
! ----------------
! ......... Read the source/sink data from the GENER file - 5-character elements
! ----------------
! 
            READ(3,5001) SS(n)%name_el(1:5), &! 5-Character name of cell containing
     &                   SS(n)%name,         &! 5-Character name of source/sink (well name)
     &                   SS(n)%n_TableP,     &! Number of points in the generation table
     &                   SS(n)%type,         &! Type of source/sink
	 &                   SS(n)%rate_m,       &! Injection/production rate (kg/s) or PI for SS(n)%type = 'DELV'
     &                   SS(n)%enth,         &! Specific enthalpy of injected fluid (J/kg) or bottomhole pressure for SS(n)%type = �DELV� 
     &                   SS(n)%z_layer,      &! Layer thickness (m) for SS_Type = �DELV�
     &                   Inh_WMasF(n)        ! Mass fraction of inhibitor in injected H2O
! 
            EL5_CR = SS(n)%name_el(1:5)
! 
         END IF IF_NumCh
		 
		 
!----------------------------------------------------------------------------------		 
! 
         SS_Typ = SS(n)%type(1:4)
! 

	!***********************************************************************
	!*                                                                     *
	!*  If EL5_CR = �+++  �, read element numbers in the source/sink list  *
	!*                                                                     *
	!***********************************************************************
	!
		IF_SSEnd1: IF(EL5_CR == '+++  ') THEN  
			
	! 		
				IF(No_CEN == 8) THEN
				   READ(3,5004), (SS(i)%el_num, i=1,NOGN)  ! Read connection-element # data, 8-ch. names  
				ELSE
				   READ(3,5003), (SS(i)%el_num, i=1,NOGN)  ! Read connection-element # data, 5-ch. names  
				END IF
	! 
				RETURN                               ! End of the GENER file reached. Done !
	! 
			 END IF IF_SSEnd1        
	!
! ----------
! ...... Mass fraction for injected inhibitor - For HYDRATE simulation only 
! -----------
! 
         IF(EOS_name(1:7) == 'HYDRATE') THEN
! 
            IF( SS(n)%enth > 0.0d0 .AND. &
     &         (SS_Typ == 'WATE' .OR. SS_Typ == 'COM1'))&
     &      THEN
               IF( Inh_WMasF(n) <= 0.0d0 .OR. &
     &             Inh_WMasF(n) >= 1.0d0 .OR.&
     &            (NK == 2 .AND. EOS_name(1:11) == 'HYDRATE_EQU') .OR.&
     &            (NK == 3 .AND. EOS_name(1:11) == 'HYDRATE_KIN')) &
     &         THEN
                  Inh_WMasF(n) = 0.0d0
               END IF
            END IF
! 
         END IF
!
!***********************************************************************
!*                                                                     *
!*      Deliverability with flowing wellbore pressure correction       *
!*                                                                     *
!***********************************************************************
!
         SS(n)%typ_indx = 0                      ! Initialization of well type index
!
         IF(SS_Typ(1:1) == char(70) .OR. &        ! Denoted by an upper or ...
     &      SS_Typ(1:1) == char(102))  &          ! ... lower-case �F� 
     &   THEN
!
            CALL WELL_Delivrblty(n,kf)
!   
            IF(SS(n)%typ_indx == nk1+2) CYCLE DO_NumSS  ! Read a new record
!
         END IF
!
!***********************************************************************
!*                                                                     *
!*       READING AND PROCESSING SINK/SOURCE DATA IS IN PROGRESS        *
!*                                                                     *
!***********************************************************************
!

! 
! -------------
! ...... Specifying source/sink option
! -------------
! 
         SELECT_SS: SELECT CASE(SS_Typ)
! 
! -------------------------
! ................. Source/sink fluid component is H2O
! -------------------------
! 
                    CASE('WATE') 
                       SS(n)%typ_indx = 1
! 
! -------------------------
! ................. Source/sink fluid component is Air
! -------------------------
! 
                    CASE('AIR ') 
                       SS(n)%typ_indx = 2
! 
! -------------------------
! ................. Source/sink component is heat
! -------------------------
! 
                    CASE('HEAT') 
                       SS(n)%typ_indx = NK1
! 
! -------------------------
! ................. Salt (NaCl) option
! -------------------------
! 
                    CASE('NACL') 
                       SS(n)%typ_indx = 1
! 
! -------------------------
! ................. Second water (WATR) option (for EOS1)
! -------------------------
! 
                    CASE('WATR')         
                       SS(n)%typ_indx = 2
! 
! -------------------------
! ................. Source/sink fluid component is COM1 = H2O
! -------------------------
! 
                    CASE('COM1') 
                       SS(n)%typ_indx = 1
! 
! -------------------------
! ................. Source/sink fluid component is COM2 = Air
! -------------------------
! 
                    CASE('COM2') 
                       SS(n)%typ_indx = 2
! 
! -------------------------
! ................. Source/sink fluid component is COM3 = ...
! -------------------------
! 
                    CASE('COM3') 
                       SS(n)%typ_indx = 3
! 
! -------------------------
! ................. Source/sink fluid component is COM4 = ...
! -------------------------
! 
                    CASE('COM4') 
                       SS(n)%typ_indx = 4
! 
! -------------------------
! ................. Source/sink fluid component is COM5 = ...
! -------------------------
! 
                    CASE('COM5') 
                       SS(n)%typ_indx = 5
! 
! -------------------------
! ................. Source/sink fluid component is COM6 = ...
! -------------------------
! 
                    CASE('COM6') 
                       SS(n)%typ_indx = 6
! 
! -------------------------
! ................. Deliverability option
! -------------------------
! 
                    CASE('DELV')         
                       SS(n)%typ_indx = NK1+1
                       SS(n)%pi       = SS(n)%rate_m    ! Productivity index
                       SS(n)%bhp      = SS(n)%enth      ! Bottomhole pressure
! 
! -------------------------
! ................. Volumetric rate option
! -------------------------
! 
                    CASE('VOL.')          
                       SS(n)%typ_indx = NK1+3
                       SS(n)%rate_v   = SS(n)%rate_m    ! Indicates the volumetric rate   
! 
! -------------------------
! ................. Default case: No available option
! -------------------------
! 
                    CASE DEFAULT 
! 
                       SS(n)%typ_indx = 0
! 
                       IF(No_CEN == 8) THEN                           ! ==> For 8-character element names
                          PRINT 6005, SS_Typ,SS(n)%name_el,SS(n)%name !       Print a warning
                          SS(n)%typ_indx = 1                          !       Assign the COM1 option
                          SS(n)%rate_m   = 0.0d0                      !       Set the rate at zero
                          CYCLE                                       !       Continue the source/sink loop
                       ELSE                                           ! ==> For 5-character element names
                          PRINT 6005, SS_Typ,SS(n)%name_el(1:5), &     !       Print a warning
     &                                SS(n)%name                      !
                          SS(n)%typ_indx = 1                          !       Assign the COM1 option
                          SS(n)%rate_m   = 0.0d0                      !       Set the rate at zero
                          CYCLE                                       !       Continue the source/sink loop
                       END IF                          
! 
                    END SELECT SELECT_SS        
!
!***********************************************************************
!*                                                                     *
!*                 READING TABULAR SINK/SOURCE DATA                    *
!*                                                                     *
!***********************************************************************
!
         LTABA = ABS(SS(n)%n_TableP)
         IF(LTABA <= 1 .OR. SS_Typ == 'DELV') CYCLE DO_NumSS
!
         WellTabData = .TRUE.
!
         Well_TData(n)%N_points = LTABA
!
         ALLOCATE(Well_TData(n)%p_TList(LTABA), &
     &            Well_TData(n)%p_QList(LTABA), STAT = ier)
! 
! -------
! ... Print explanatory comments  
! -------
! 
         IF(ier == 0) THEN
            WRITE(50,6103) 
         ELSE
            PRINT 6104
            WRITE(50,6104) 
            STOP
         END IF
! 
         IF(SS(n)%type(5:5) /= ' ') THEN
            ALLOCATE(Well_TData(n)%p_HList(LTABA), STAT = ier)
         END IF
! 
! -------
! ... Print explanatory comments  
! -------
! 
         IF(ier == 0) THEN
            WRITE(50,6105) 
         ELSE
            PRINT 6106
            WRITE(50,6106) 
            STOP
         END IF
! 
! ----------
! ...... Read the tabular data
! ----------
! 
         READ(3,5006) (Well_TData(n)%p_TList(k),k=1,LTABA)    ! Time series
!
         READ(3,5006) (Well_TData(n)%p_QList(k),k=1,LTABA)    ! Corresponding rates
! 
         IF(SS(n)%type(5:5) /= ' ') THEN
            READ(3,5006) (Well_TData(n)%p_QList(k),k=1,LTABA) ! Corresponding specific enthalpies 
         END IF
!
! <<<                      
! <<< End of the SOURCE/SINK LOOP         
! <<<
!
      END DO DO_NumSS
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 5001 FORMAT(2A5,15X,I5,5X,A5,4G10.4)
 5002 FORMAT(A8,A5,12X,I5,5X,A5,4G10.4)
!
 5003 FORMAT(16I5)
 5004 FORMAT(10I8)
!
 5006 FORMAT(4E14.7)
!
 6000 FORMAT(/,'READ_FileGENER    1.0   18 September 2004',6X,&
     &         'Read the sink/source data from file GENER') 
!
 6001 FORMAT(' NUMBER OF SINKS/SOURCES SPECIFIED IN DATA BLOCK "GENER"',&
     &       ' EXCEEDS ALLOWABLE MAXIMUM OF ',I4/&
     &       ' INCREASE PARAMETER "MaxNum_SS" IN MAIN',&
     &       ' PROGRAM, AND RECOMPILE',//,&
     &       ' >>>>>>>>>>  S K I P   F L O W   S I M U L A T I O N  ',&
     &       ' <<<<<<<<<<')
 6002 FORMAT('+++  ')
!
 6004 FORMAT(' REFERENCE TO UNKNOWN ELEMENT ',A8,&
     &       ' AT SOURCE ',I2,' --- WILL IGNORE SOURCE')
 6005 FORMAT(' IGNORE UNKNOWN GENERATION OPTION "',A4,&
     &       '" AT ELEMENT "',A8,'" SOURCE "',A5,'"')
 6006 FORMAT(' NUMBER OF TABULAR GENERATION DATA SPECIFIED IN DATA',&
     &       ' BLOCK "GENER" EXCEEDS ALLOWABLE MAXIMUM OF ',I5/&
     &       ' INCREASE PARAMETER "MGTAB" IN MAIN PROGRAM,',&
     &       ' AND RECOMPILE',//,&
     &       ' >>>>>>>>>>  S K I P   F L O W   S I M U L A T I O N  ',&
     &       ' <<<<<<<<<<')
!
 6103 FORMAT(T2,'Memory allocation of "Well_TData(n)%p_TList, ',&
     &          'Well_TData(n)%p_QList" in subroutine ',&
     &          '�READ_FileGENER� was successful')
 6104 FORMAT(//,20('ERROR-'),//,&
     &       T2,'Memory allocation of "Well_TData(n)%p_TList, ',&
     &          'Well_TData(n)%p_QList" in subroutine ',&
     &          '"READ_FileGENER" was unsuccessful',//,&
     &       T2,'!!!!!!!    THE EXECUTION WAS STOPPED    !!!!!!!',&
     &       //,20('ERROR-'))
!
 6105 FORMAT(T2,'Memory allocation of �p_HList� in subroutine ',&
     &          '�READ_FileGENER� was successful')
 6106 FORMAT(//,20('ERROR-'),//,&
     &       T2,'Memory allocation of �p_HList� in subroutine ',&
     &          '"READ_FileGENER" was unsuccessful',//,&
     &       T2,'!!!!!!!    THE EXECUTION WAS STOPPED    !!!!!!!',&
     &       //,20('ERROR-'))
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of READ_FileGENER
!
!
      RETURN
! 
  END SUBROUTINE READ_FileGENER_loc
!

!
!