!C>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!C>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!C>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!C>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!C>                                                                     >
!C>                                                                     >
!C>!               ITERATIVE AND DIRECT MATRIX SOLVERS                  >
!C>                                                                     >
!C>                                                                     >
!C>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!C>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!C>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!C>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!C
!C
!C
      MODULE Matrix_Solvers
! 
         PRIVATE
! 
! ----------
! ...... Iterative Solvers (Preconditioned Conjugate Gradient)
! ----------
! 
         PUBLIC :: DSLUBC,DSLUCS,DSLUGM,DLUSTB
! 
! ----------
! ...... Direct Solvers (LU Decomposition)
! ----------
! 
         PUBLIC :: DGBTRF,DGBTRS
!
!
! 
         CONTAINS
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!      I T E R A T I V E    S O L V E R S
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
      SUBROUTINE DBCG(N, B, X, NELT, IA, JA, A, ISYM, MATVEC, MTTVEC,&
     &     MSOLVE, MTSOLV, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, &
     &     R, Z, P, RR, ZZ, PP, DZ, RWORK, IWORK)
!
!
!
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
!
      INTEGER N, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITMAX
      INTEGER ITER, IERR, IUNIT, IWORK(*)
!
      REAL(KIND = 8) B(N), X(N), A(NELT), TOL, ERR, R(N), Z(N), P(N)
      REAL(KIND = 8) RR(N), ZZ(N), PP(N), DZ(N), RWORK(*)
!
      REAL(KIND = 8) DMACH(5)
      DATA DMACH(3) / 1.1101827117665D-16 /

!      EXTERNAL MATVEC, MTTVEC, MSOLVE, MTSOLV
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DBCG
!
!
      ITER = 0
      IERR = 0
      IF( N.LT.1 ) THEN
         IERR = 3
         RETURN
      ENDIF
      FUZZ   = DMACH(3)
      TOLMIN = 5.0d2*FUZZ
      FUZZ   = FUZZ*FUZZ
      IF( TOL.LT.TOLMIN ) THEN
         TOL  = TOLMIN
         IERR = 4
      ENDIF
!         
      CALL MATVEC(N, X, R, NELT, IA, JA, A, ISYM)
      DO 10 I = 1, N
         R(I)  = B(I) - R(I)
         RR(I) = R(I)
 10   CONTINUE
 
      CALL MSOLVE(N, R, Z, NELT, IA, JA, A, ISYM, RWORK, IWORK)
      CALL MTSOLV(N, RR, ZZ, NELT, IA, JA, A, ISYM, RWORK, IWORK)
!         
!         
      ISDBCG = 0
      ITOL   = 1
      IF(ITER .EQ. 0) THEN
         BNRM = 0.0d0
         DO 11 I5=1,N
            BNRM = BNRM+B(I5)*B(I5)
   11    CONTINUE
         BNRM = SQRT(BNRM)
      ENDIF
       ERR = 0.0D0
       DO 12 I5=1,N
         ERR = ERR+R(I5)*R(I5)
   12 CONTINUE
      ERR = SQRT(ERR)/BNRM
!         
      IF(IUNIT .NE. 0) THEN
         IF( ITER.EQ.0 ) THEN
            WRITE(IUNIT,1000) N, ITOL
         ENDIF
         WRITE(IUNIT,1010) ITER, ERR, AK, BK
      ENDIF
      IF(ERR .LE. TOL) ISDBCG = 1
!         
      IF(ISDBCG.NE.0) GO TO 200
!         
!         
      DO 100 K=1,ITMAX
         ITER = K
!         
         DDOT = 0.0d0
         DO 15 I = 1,N
           DDOT = DDOT + Z(I)*RR(I)
   15    CONTINUE
         BKNUM = DDOT

         IF( ABS(BKNUM).LE.FUZZ ) THEN
            IERR = 6
            RETURN
         ENDIF
         IF(ITER .EQ. 1) THEN
            DO 18 I = 1,N
              P(I) = Z(I)
              PP(I) = ZZ(I)
   18       CONTINUE         
         ELSE
            BK = BKNUM/BKDEN
            DO 20 I = 1, N
               P(I)  = Z(I) + BK*P(I)
               PP(I) = ZZ(I) + BK*PP(I)
 20         CONTINUE
         ENDIF
         BKDEN = BKNUM
!         
         CALL MATVEC(N, P, Z, NELT, IA, JA, A, ISYM)

         DDOT = 0.0d0
         DO 25 I = 1,N
           DDOT = DDOT + PP(I)*Z(I)
   25    CONTINUE
         AKDEN = DDOT

         AK = BKNUM/AKDEN
         IF( ABS(AKDEN).LE.FUZZ ) THEN
            IERR = 6
            RETURN
         ENDIF
         
          DO 26 I = 1,N
             X(I) = X(I) + AK*P(I)
   26     CONTINUE

          DO 27 I = 1,N
             R(I) = R(I) - AK*Z(I)
   27     CONTINUE
         
         CALL MTTVEC(N, PP, ZZ, NELT, IA, JA, A, ISYM)
         
          DO 28 I = 1,N
             RR(I) = RR(I) - AK*ZZ(I)
   28     CONTINUE
         
         CALL MSOLVE(N, R, Z, NELT, IA, JA, A, ISYM, RWORK, IWORK)
         CALL MTSOLV(N, RR, ZZ, NELT, IA, JA, A, ISYM, RWORK, IWORK)
!
         ISDBCG = 0
         ITOL   = 1
         ERR    = 0.0D0
         DO 33 I5=1,N
            ERR = ERR+R(I5)*R(I5)
   33    CONTINUE
         ERR = SQRT(ERR)/BNRM
!         
         IF(IUNIT .NE. 0) THEN
            WRITE(IUNIT,1010) ITER, ERR, AK, BK
         ENDIF
         IF(ERR .LE. TOL) ISDBCG = 1
!         
         IF(ISDBCG.NE.0) GO TO 200
!         
 100  CONTINUE
 1000 FORMAT(' Preconditioned BiConjugate Gradient for N, ITOL = ',&
      &    I5,I5,/' ITER','   Error Estimate','            Alpha',&
     &     '             Beta')
 1010 FORMAT(1X,I4,1X,E16.7,1X,E16.7,1X,E16.7)
!         
!         
      ITER = ITMAX + 1
      IERR = 2
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DBCG
!
!
 200  RETURN
      END SUBROUTINE DBCG
!
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
      SUBROUTINE DSLUBC(N,B,X,NELT,IA,JA,A,TOL,ITMAX,ITER,ERR,&
     &                  IERR,IUNIT,RWORK,LENW,IWORK,LENIW)
!
!
!
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
!
      INTEGER N, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITMAX, ITER
      INTEGER IERR, IUNIT, LENW, IWORK(LENIW), LENIW
      REAL(KIND = 8) B(N), X(N), A(NELT), TOL, ERR, RWORK(LENW)
      EXTERNAL DSMV, DSMTV, DSLUI, DSLUTI
!
      INTEGER, PARAMETER  :: LOCRB=1, LOCIB=11
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSLUB!
!
!
      ISYM = 0
      ITOL = 1
!
      IERR = 0
      IF( N.LT.1 .OR. NELT.LT.1 ) THEN
         IERR = 3
         RETURN
      ENDIF
      CALL DS2Y( N, NELT, IA, JA, A, ISYM, IUNIT )
!
      NL = 0
      NU = 0
      DO 20 ICOL = 1, N
!         Don't count diagonal.
         JBGN = JA(ICOL)+1
         JEND = JA(ICOL+1)-1
         IF( JBGN.LE.JEND ) THEN
            DO 10 J = JBGN, JEND
               IF( IA(J).GT.ICOL ) THEN
                  NL = NL + 1
               ELSE
                  NU = NU + 1
               ENDIF
 10         CONTINUE
         ENDIF
 20   CONTINUE
!         
      LOCIL = LOCIB
      LOCJL = LOCIL + N+1
      LOCIU = LOCJL + NL
      LOCJU = LOCIU + NU
      LOCNR = LOCJU + N+1
      LOCNC = LOCNR + N
      LOCIW = LOCNC + N
!
      LOCL = LOCRB
      LOCDIN = LOCL + NL
      LOCU = LOCDIN + N
      LOCR = LOCU + NU
      LOCZ = LOCR + N
      LOCP = LOCZ + N
      LOCRR = LOCP + N
      LOCZZ = LOCRR + N
      LOCPP = LOCZZ + N
      LOCDZ = LOCPP + N
      LOCW = LOCDZ + N
!
      CALL DCHKW( 'DSLUBC',LOCIW,LENIW,LOCW,LENW,IERR,ITER,ERR)
      IF( IERR.NE.0 ) RETURN
!
      IWORK(1) = LOCIL
      IWORK(2) = LOCJL
      IWORK(3) = LOCIU
      IWORK(4) = LOCJU
      IWORK(5) = LOCL
      IWORK(6) = LOCDIN
      IWORK(7) = LOCU
      IWORK(9) = LOCIW
      IWORK(10) = LOCW
!
      CALL DSILUS( N, NELT, IA, JA, A, ISYM, NL, IWORK(LOCIL),&
          & IWORK(LOCJL), RWORK(LOCL), RWORK(LOCDIN), NU, IWORK(LOCIU),&
         & IWORK(LOCJU), RWORK(LOCU), IWORK(LOCNR), IWORK(LOCNC) )
!         
      CALL DBCG(N, B, X, NELT, IA, JA, A, ISYM, DSMV, DSMTV,&
        &  DSLUI, DSLUTI, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT,&
       &   RWORK(LOCR), RWORK(LOCZ), RWORK(LOCP),&
      &    RWORK(LOCRR), RWORK(LOCZZ), RWORK(LOCPP),&
     &     RWORK(LOCDZ), RWORK, IWORK )
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DSLUB!
!
!
      RETURN
      END SUBROUTINE DSLUBC
!
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
      SUBROUTINE DCGS(N, B, X, NELT, IA, JA, A, ISYM, MATVEC,&
     &     MSOLVE, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, &
     &     R, R0, P, Q, U, V1, V2, RWORK, IWORK)
!
!!N, B, X, NELT, IA, JA, A, ISYM, DSMV,&           !!!ITERATION BASICALLY STARTS AFTER THIS. Algorithm after here.
 !         DSLUI, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT,&
 !         RWORK(LOCR), RWORK(LOCR0), RWORK(LOCP),&
 !         RWORK(LOCQ), RWORK(LOCU), RWORK(LOCV1),&
  !        RWORK(LOCV2), RWORK, IWORK
!
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
!
      INTEGER N, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITMAX
      INTEGER ITER, IERR, IUNIT, IWORK(*)
      REAL(KIND = 8) B(N), X(N), A(NELT), TOL, ERR, R(N), R0(N), P(N)
      REAL(KIND = 8) Q(N), U(N), V1(N), V2(N), RWORK(*)
!
      REAL(KIND = 8) DMACH(5)
      DATA DMACH(3) / 1.1101827117665D-16 /

!      EXTERNAL MATVEC, MSOLVE
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DCGS
!
!
      ITER = 0
      IERR = 0
!
      IF( N < 1 ) THEN
         IERR = 3
         RETURN
      ENDIF
!
      TOLMIN = 5.0d2*DMACH(3)
      IF( TOL < TOLMIN ) THEN
         TOL  = TOLMIN
         IERR = 4
      ENDIF
!         
      CALL MATVEC(N, X, R, NELT, IA, JA, A, ISYM) !DSMV Matvec computes AX = R. MSOLVE solves the system MR = V1, where V1 is residual AX - R from previous step, M = LDU - the ldu decomposition from preconditioning.
!         
      V1(1:N)  = R(1:N) - B(1:N)
!         
      CALL MSOLVE(N, V1, R, NELT, IA, JA, A, ISYM, RWORK, IWORK)!DSLUI - Solves for R in LDU.R = B1 Directly parallelizable
!         
      ISDCGS = 0
      ITOL   = 2
      IF(ITER == 0) THEN
         CALL MSOLVE(N, B, V2, NELT, IA, JA, A, ISYM, RWORK, IWORK)
         BNRM = SQRT(DOT_PRODUCT(V2(1:N),V2(1:N)))
      ENDIF
!         
      ERR = SQRT(DOT_PRODUCT(R(1:N),R(1:N)))/BNRM
!         
      IF(IUNIT /= 0) THEN
         IF( ITER == 0 ) THEN
            WRITE(IUNIT,1000) N, ITOL
         ENDIF
         WRITE(IUNIT,1010) ITER, ERR, AK, BK
      ENDIF
      IF(ERR <= TOL) ISDCGS = 1
!         
      IF(ISDCGS /= 0) GO TO 200
!
!
      FUZZ = DMACH(3)**2
!
      R0(1:N) = R(1:N)
!
      RHONM1 = 1.0D0
!         
!         
      DO 100 K=1,ITMAX
         ITER = K
!
         RHON = DOT_PRODUCT(R0(1:N),R(1:N))
!
         IF( ABS(RHONM1) < FUZZ ) GO TO 998
!
         BK = RHON/RHONM1
!
         IF( ITER.EQ.1 ) THEN
            U(1:N) = R(1:N)
            P(1:N) = R(1:N)
         ELSE
            U(1:N)  = R(1:N) + BK*Q(1:N)
            V1(1:N) = Q(1:N) + BK*P(1:N)
            P(1:N)  = U(1:N) + BK*V1(1:N)
         ENDIF
!         
         CALL MATVEC(N, P, V2, NELT, IA, JA, A, ISYM)
         CALL MSOLVE(N, V2, V1, NELT, IA, JA, A, ISYM, RWORK, IWORK)
!         
         SIGMA = DOT_PRODUCT(R0(1:N),V1(1:N))

         IF( ABS(SIGMA) < FUZZ ) GO TO 999
!         
         AK = RHON/SIGMA
         AKM = -AK
!         
         Q(1:N)  = U(1:N) + AKM*V1(1:N)
!         
         V1(1:N) = U(1:N) + Q(1:N)
!         
         X(1:N)  = X(1:N) + AKM*V1(1:N)
!                      
         CALL MATVEC(N, V1, V2, NELT, IA, JA, A, ISYM)
         CALL MSOLVE(N, V2, V1, NELT, IA, JA, A, ISYM, RWORK, IWORK)

         R(1:N) = R(1:N) + AKM*V1(1:N)
!         
         ISDCGS = 0
         ITOL   = 2
!         
         ERR = SQRT(DOT_PRODUCT(R(1:N),R(1:N)))/BNRM
!         
         IF(IUNIT /= 0) THEN
            WRITE(IUNIT,1010) ITER, ERR, AK, BK
         ENDIF
         IF(ERR <= TOL) ISDCGS = 1
!         
         IF(ISDCGS /= 0) GO TO 200
!
         RHONM1 = RHON
 100  CONTINUE
 1000 FORMAT(' Preconditioned BiConjugate Gradient Squared for ',&
          'N, ITOL = ',I5, I5,&
          /' ITER','   Error Estimate','            Alpha',&
          '             Beta')
 1010 FORMAT(1X,I4,1X,E16.7,1X,E16.7,1X,E16.7)
!         
      ITER = ITMAX + 1
      IERR = 2
 200  RETURN
!
 998  IERR = 5
      RETURN
!
 999  IERR = 6
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DCGS
!
!
      RETURN
      END SUBROUTINE DCGS
!
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
      SUBROUTINE DSLUCS(N,B,X,NELT,IA,JA,A,TOL,ITMAX,ITER,ERR,&
     &                  IERR,IUNIT,RWORK,LENW,IWORK,LENIW)
!---------------------------------------------------------
!This the parallel version of DSLUCS. Changed on 160616. Author - Aman Rusia
!--------------------------------------------------------
!N - Originally, the size of the system. New???
!B - Right hand side of the system - processor dependent size
!X - wkarea. It is the final solution - processor dependent size
!NELT - NZ - size of CO in original. NZ = mnz+1. In parallel version - size = (NZ - 1)/world_size + 1. This is because last element has some use.
!IA,JA,A - irn ,icn,co distributed
!    &                  CLOSUR,NMAXIT,ITERU,ERR,IERR,&
!   &                  IUNIT
!RWORK - size LENW.  Work array. Originally LENW = max(1000+mnz+38*MAXNUM_ELEM * NEQ, 9*MaxNum_Conx+1000) where mnz = max size of CO = NEQ*NEQ*(MaxNum_elements+2*MaxNum_conx). New LENW = 1000+mnz+38*MAXNUM_ELEM * NEQ, 9*MaxNum_Conx+1000
!IWORK - size LENIW. jvect. Work array. Originally LENIW = 100+mnz+5*MAXNUM_ELEM*NEQ. IWORK(1:10) stores Upper,Lower triangular matrices and Diagonal inverse of preconditioner, and their column and row information
!new the above sizes are changed according to new MAXNUM_ELEM = MAXNUM_ELEM/size. Note that MaxNum_Conx should remain intact since it could be fully inside a processor.
!RWORK also stores values of elements in L, U and Dinv after preconditioning. IWORK stores their respective row and column indices
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
!
      INTEGER N, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITMAX, ITER
      INTEGER IERR, IUNIT, LENW, IWORK(LENIW), LENIW
      REAL(KIND = 8) B(N), X(N), A(NELT), TOL, ERR, RWORK(LENW)
      EXTERNAL DSMV, DSLUI
!
      INTEGER, PARAMETER :: LOCRB=1, LOCIB=11
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSLUCS
!
!
      ISYM = 0
      ITOL = 2
!
      IERR = 0
      IF( N.LT.1 .OR. NELT.LT.1 ) THEN
         IERR = 3
         RETURN
      ENDIF
      CALL DS2Y( N, NELT, IA, JA, A, ISYM, IUNIT ) !No changes required in parallel version.
![p] No changes done in the following block ------212
      NL = 0
      NU = 0
      DO 20 ICOL = 1, N
         JBGN = JA(ICOL)+1
         JEND = JA(ICOL+1)-1
         IF( JBGN.LE.JEND ) THEN
            DO 10 J = JBGN, JEND
               IF( IA(J).GT.ICOL ) THEN
                  NL = NL + 1
               ELSE
                  NU = NU + 1
               ENDIF
 10         CONTINUE
         ENDIF
 20   CONTINUE
!--------212         
      LOCIL = LOCIB !Starting location in IWORK
      LOCJL = LOCIL + N+1
      LOCIU = LOCJL + NL
      LOCJU = LOCIU + NU
      LOCNR = LOCJU + N+1	!LOCNR = NROW in DSILUS
      LOCNC = LOCNR + N		!LOCNC = NCOL in DSILUS
      LOCIW = LOCNC + N
!
      LOCL   = LOCRB      !Starting location in RWORK
      LOCDIN = LOCL + NL
      LOCUU  = LOCDIN + N
      LOCR   = LOCUU + NU
      LOCR0  = LOCR + N
      LOCP   = LOCR0 + N
      LOCQ   = LOCP + N
      LOCU   = LOCQ + N
      LOCV1  = LOCU + N
      LOCV2  = LOCV1 + N
      LOCW   = LOCV2 + N
!
      CALL DCHKW( 'DSLUCS', LOCIW, LENIW, LOCW, LENW, IERR, ITER, ERR ) !Check if LOCW and LOCIW exceed the limit or not
      IF( IERR.NE.0 ) RETURN
!
      IWORK(1) = LOCIL
      IWORK(2) = LOCJL
      IWORK(3) = LOCIU
      IWORK(4) = LOCJU
      IWORK(5) = LOCL
      IWORK(6) = LOCDIN
      IWORK(7) = LOCUU
      IWORK(9) = LOCIW
      IWORK(10) = LOCW
!
      CALL DSILUS( N, NELT, IA, JA, A, ISYM, NL, IWORK(LOCIL),&
          IWORK(LOCJL), RWORK(LOCL), RWORK(LOCDIN), NU, IWORK(LOCIU),&
          IWORK(LOCJU), RWORK(LOCUU), IWORK(LOCNR), IWORK(LOCNC) )    !Preconditioner. 
!         
      CALL DCGS(N, B, X, NELT, IA, JA, A, ISYM, DSMV,&
          DSLUI, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT,&
          RWORK(LOCR), RWORK(LOCR0), RWORK(LOCP),&
          RWORK(LOCQ), RWORK(LOCU), RWORK(LOCV1),&
          RWORK(LOCV2), RWORK, IWORK )
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DSLUCS
!
!
      RETURN
      END SUBROUTINE DSLUCS
!
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
      SUBROUTINE DCGSTB(N, B, X, NELT, IA, JA, A, ISYM, MATVEC,&
     &     MSOLVE, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, &
     &     R, R0, P, Q, U, V1, V2, RWORK, IWORK)
!
!
!
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
!
      INTEGER N, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITMAX
      INTEGER ITER, IERR, IUNIT, IWORK(*)
      REAL(KIND = 8) B(N), X(N), A(NELT), TOL, ERR, R(N), R0(N), P(N)
      REAL(KIND = 8) Q(N), U(N), V1(N), V2(N), RWORK(*)

      REAL(KIND = 8) DMACH(5)
      DATA DMACH(3) / 1.1101827117665D-16 /

!      EXTERNAL MATVEC, MSOLVE
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DCGSTB
!
!
      ITER = 0
      IERR = 0
!
      IF( N.LT.1 ) THEN
         IERR = 3
         RETURN
      ENDIF
      TOLMIN = 5.0d2*DMACH(3)
      IF(TOL.LT.TOLMIN) THEN
         TOL = TOLMIN
         IERR = 4
      ENDIF
!         
      DO 2 I = 1, N
         R(I) = B(I)
    2 CONTINUE
!
      ERR1 = 0.0d0
      DO 4 i=1,n
         err1 = err1+x(i)*x(i)
    4 CONTINUE
!
!
!
      IF(err1.NE.0.0d0) THEN
         CALL MATVEC(N, X, R, NELT, IA, JA, A, ISYM)
!
         ERR2 = 0.0D0
         DO 5 i=1,n
            R(I)  = B(I)-R(I)
            ERR2  = ERR2+R(I)*R(I)
    5    CONTINUE
         ERR2 = SQRT(ERR2)
         IF(ERR2.LE.TOL) IBCGST = 1
         IF(IBCGST.NE.0) GO TO 200
      END IF
!
!
!
      RHONM1 = 1.0D0
      ALPHA  = 1.0D0
      OMEGA  = 1.0D0
      BETA   = 0.0d0
!
!
!         
      IBCGST = 0
      ITOL   = 2
      BNRM   = 0.0d0
      DO 11 I5=1,N
         BNRM = BNRM+B(I5)*B(I5)
   11 CONTINUE
      BNRM = SQRT(BNRM)
!
      ERR = 0.0D0
      DO 12 I5=1,N
         ERR = ERR+R(I5)*R(I5)
   12 CONTINUE
      ERR = SQRT(ERR)/BNRM
!         
      IF(IUNIT.NE.0) THEN
         IF(ITER.EQ.0) THEN
            WRITE(IUNIT,1000) N, ITOL
         ENDIF
         WRITE(IUNIT,1010) ITER, ERR, Alpha, BETA, OMEGA
      ENDIF
      IF(ERR.LE.TOL) IBCGST = 1
!         
      IF(IBCGST.NE.0) GO TO 200
!
!
      FUZZ = DMACH(3)**2
      DO 14 I = 1, N
         R0(I) = R(I)
   14 CONTINUE
!         
!
!         
      DO 100 K=1,ITMAX
         ITER = K
!
         DDOT = 0.0d0
         DO 15 I = 1,N
           DDOT = DDOT + R0(I)*R(I)
   15    CONTINUE
         RHON = DDOT

         IF(ABS(RHONM1).LT.FUZZ) GO TO 998
         BETA = (RHON/RHONM1)*(ALPHA/OMEGA)
!
         IF( ITER.EQ.1 ) THEN
            DO 20 I = 1, N
               P(I)  = R(I)
   20       CONTINUE
         ELSE
            DO 25 I = 1, N
               U(I) = P(I)-OMEGA*V1(I)
   25       CONTINUE
            DO 26 I = 1, N
               P(I) = R(I)+BETA*U(I)
   26       CONTINUE
         END IF
!         
         CALL MSOLVE(N, P, V2, NELT, IA, JA, A, ISYM, RWORK, IWORK)
         CALL MATVEC(N, V2, V1, NELT, IA, JA, A, ISYM)

         DDOT = 0.0D0
         DO 30 I = 1,N
           DDOT = DDOT + R0(I)*V1(I)
   30    CONTINUE
         SIGMA = DDOT

         IF( ABS(SIGMA).LT.FUZZ ) GOTO 999
         ALPHA = RHON/SIGMA
!
         DO 35 I=1,N
            Q(I) = R(I)-ALPHA*V1(I)
   35    CONTINUE         
!
         ERR = 0.0d0
         DO 40 I5=1,N
            ERR = ERR+Q(I5)*Q(I5)
   40    CONTINUE
         ERR = SQRT(ERR)/BNRM
!         
         IF(ERR.LE.TOL) IBCGST = 1
!
         DO 42 I5=1,N
            X(I5) = X(I5)+ALPHA*V2(I5)
   42    CONTINUE
!         
         IF(IBCGST.NE.0) THEN 
            IF(IUNIT.NE.0) THEN
               WRITE(IUNIT,1010) ITER, ERR, Alpha, BETA, OMEGA
            ENDIF
            GO TO 200
         END IF       
!                      
         CALL MSOLVE(N, Q, V2, NELT, IA, JA, A, ISYM, RWORK, IWORK)
         CALL MATVEC(N, V2, U, NELT, IA, JA, A, ISYM)
!
         SUM1=0.0D0 
         SUM2=0.0D0 
         DO 50 I = 1, N
            SUM1 = SUM1+U(I)*Q(I)
            SUM2 = SUM2+U(I)*U(I)
   50    CONTINUE
         OMEGA = SUM1/SUM2

          DO 60 I = 1,N
             X(I) = X(I)+OMEGA*V2(I)
   60     CONTINUE
!
         DO 65 I = 1,N
            R(I) = Q(I) - OMEGA*U(I)
   65    CONTINUE
!         
         IBCGST = 0
         ITOL   = 2
         ERR    = 0.0D0
         DO 78 I5=1,N
            ERR = ERR+R(I5)*R(I5)
   78    CONTINUE
         ERR = SQRT(ERR)/BNRM
!         
         IF(IUNIT .NE. 0) THEN
            WRITE(IUNIT,1010) ITER, ERR, Alpha, BETA, OMEGA
         ENDIF
         IF(ERR .LE. TOL) IBCGST = 1
!         
         IF(IBCGST.NE.0) GO TO 200
!
         RHONM1 = RHON
 100  CONTINUE
 1000 FORMAT(' Preconditioned BiConjugate Gradient Stabilized for ',&
          'N, ITOL = ',I5, I5,&
          /' ITER','   Error Estimate','            Alpha',&
          '             Beta','             Omega')
 1010 FORMAT(1X,I4,1X,1pE16.7,1X,1pE16.7,1X,1pE16.7,1X,1pE16.7)
!         
      ITER = ITMAX + 1
      IERR = 2
 200  RETURN
!
 998  IERR = 5
      RETURN
!
 999  IERR = 6
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DCGSTB
!
!
      RETURN
      END SUBROUTINE DCGSTB
!
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
      SUBROUTINE DLUSTB(N,B,X,NELT,IA,JA,A,TOL,ITMAX,ITER,ERR,&
     &                  IERR,IUNIT,RWORK,LENW,IWORK,LENIW)
!
!
!
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
!
      INTEGER N, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITMAX, ITER
      INTEGER IERR, IUNIT, LENW, IWORK(LENIW), LENIW
      REAL(KIND = 8) B(N), X(N), A(NELT), TOL, ERR, RWORK(LENW)
!
      EXTERNAL DSMV, DSLUI
!
      INTEGER, PARAMETER :: LOCRB=1, LOCIB=11
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DLUSTB
!
!
      ISYM = 0
      ITER = 2
!
      IERR = 0
      IF( N.LT.1 .OR. NELT.LT.1 ) THEN
         IERR = 3
         RETURN
      ENDIF
      CALL DS2Y( N, NELT, IA, JA, A, ISYM, IUNIT )
!
      NL = 0
      NU = 0
      DO 20 ICOL = 1, N
         JBGN = JA(ICOL)+1
         JEND = JA(ICOL+1)-1
         IF( JBGN.LE.JEND ) THEN
            DO 10 J = JBGN, JEND
               IF( IA(J).GT.ICOL ) THEN
                  NL = NL + 1
               ELSE
                  NU = NU + 1
               ENDIF
 10         CONTINUE
         ENDIF
 20   CONTINUE
!         
      LOCIL = LOCIB
      LOCJL = LOCIL + N+1
      LOCIU = LOCJL + NL
      LOCJU = LOCIU + NU
      LOCNR = LOCJU + N+1
      LOCNC = LOCNR + N
      LOCIW = LOCNC + N
!
      LOCL   = LOCRB
      LOCDIN = LOCL + NL
      LOCUU  = LOCDIN + N
      LOCR   = LOCUU + NU
      LOCR0  = LOCR + N
      LOCP   = LOCR0 + N
      LOCQ   = LOCP + N
      LOCU   = LOCQ + N
      LOCV1  = LOCU + N
      LOCV2  = LOCV1 + N
      LOCW   = LOCV2 + N
!
      CALL DCHKW('DLUSTB',LOCIW,LENIW,LOCW,LENW,IERR,ITER,ERR)
      IF( IERR.NE.0 ) RETURN
!
      IWORK(1) = LOCIL
      IWORK(2) = LOCJL
      IWORK(3) = LOCIU
      IWORK(4) = LOCJU
      IWORK(5) = LOCL
      IWORK(6) = LOCDIN
      IWORK(7) = LOCUU
      IWORK(9) = LOCIW
      IWORK(10) = LOCW
!
      CALL DSILUS( N, NELT, IA, JA, A, ISYM, NL, IWORK(LOCIL),&
          IWORK(LOCJL), RWORK(LOCL), RWORK(LOCDIN), NU, IWORK(LOCIU),&
         IWORK(LOCJU), RWORK(LOCUU), IWORK(LOCNR), IWORK(LOCNC) )
!         
      CALL DCGSTB(N, B, X, NELT, IA, JA, A, ISYM, DSMV,&
         DSLUI, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT,&
          RWORK(LOCR), RWORK(LOCR0), RWORK(LOCP),&
          RWORK(LOCQ), RWORK(LOCU), RWORK(LOCV1),&
          RWORK(LOCV2), RWORK, IWORK )
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DLUSTB
!
!
      RETURN
      END SUBROUTINE DLUSTB
!
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
      SUBROUTINE DGMRES(N, B, X, NELT, IA, JA, A, ISYM, MATVEC, MSOLVE,&
     &     ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, SB, SX, &
     &     RGWK, LRGW, IGWK, LIGW, RWORK, IWORK )
!
!
!
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
!
      INTEGER  N, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITMAX, ITER
      INTEGER  IERR, IUNIT, LRGW, LIGW, IGWK(LIGW)
      INTEGER  IWORK(*)
      REAL(KIND = 8) B(N), X(N), A(NELT), TOL, ERR, SB(N), SX(N)
      REAL(KIND = 8) RGWK(LRGW), RWORK(*)
      EXTERNAL MATVEC, MSOLVE
      INTEGER JPRE, KMP, MAXL, NMS, MAXLP1, NMSL, NRSTS, NRMAX
      INTEGER I, IFLAG, LR, LDL, LHES, LGMR, LQ, LV, LW
      REAL(KIND = 8) BNRM, RHOL

      REAL(KIND = 8) DMACH(5)
      DATA DMACH(3) / 1.1101827117665D-16 /
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DGMRES
!
!
      IERR = 0
      MAXL = IGWK(1)
      IF (MAXL .EQ. 0) MAXL = 10
      IF (MAXL .GT. N) MAXL = N
      KMP = IGWK(2)
      IF (KMP .EQ. 0) KMP = MAXL
      IF (KMP .GT. MAXL) KMP = MAXL
      JSCAL = IGWK(3)
      JPRE = IGWK(4)

      IF( ITOL.EQ.1 .AND. JPRE.LT.0 ) GOTO 650
      IF( ITOL.EQ.2 .AND. JPRE.GE.0 ) GOTO 650
      NRMAX = IGWK(5)
      
      IF( NRMAX.EQ.0 ) NRMAX = 10
      IF( NRMAX.EQ.-1 ) NRMAX = 0
      IF( TOL.EQ.0.0D0 ) TOL = 5.0d2*DMACH(3)
!
      ITER  = 0
      NMS   = 0
      NRSTS = 0
!
!
!
      MAXLP1 = MAXL + 1
      LV = 1
      LR = LV + N*MAXLP1
      LHES = LR + N + 1
      LQ = LHES + MAXL*MAXLP1
      LDL = LQ + 2*MAXL
      LW = LDL + N
      LXL = LW + N
      LZ = LXL + N
!
      IGWK(6) = LZ + N - 1
      IF( LZ+N-1.GT.LRGW ) GOTO 640
      
      IF (JPRE .LT. 0) THEN
         CALL MSOLVE(N,B,RGWK(LR),NELT,IA,JA,A,ISYM,RWORK,IWORK)
         NMS = NMS + 1
      ELSE
         DO  7 I = 1,N
           RGWK(LR+I-1) = B(I)
    7    CONTINUE               

      ENDIF
      IF( JSCAL.EQ.2 .OR. JSCAL.EQ.3 ) THEN
         BNRM = 0.0d0
         DO 10 I = 1,N
            BNRM = BNRM +(RGWK(LR-1+I)*SB(I))&
     &                  *(RGWK(LR-1+I)*SB(I))
 10      CONTINUE
         BNRM = SQRT(BNRM)
      ELSE
         BNRM = 0.0d0
         DO 12 I = 1,N
            BNRM = BNRM + RGWK(LR-1+I)*RGWK(LR-1+I)
 12      CONTINUE
         BNRM = SQRT(BNRM)
      ENDIF
      
      CALL MATVEC(N, X, RGWK(LR), NELT, IA, JA, A, ISYM)
      
      DO 50 I = 1,N
         RGWK(LR-1+I) = B(I) - RGWK(LR-1+I)
 50   CONTINUE

 100  CONTINUE
      IF( NRSTS.GT.NRMAX ) GOTO 610
      
      IF( NRSTS.GT.0 ) THEN
         DO 17 I = 1,N
           RGWK(LR+I-1) = RGWK(LDL+I-1)
   17    CONTINUE               
      ENDIF

      CALL DPIGMR(N, RGWK(LR), SB, SX, JSCAL, MAXL, MAXLP1, KMP,&
            NRSTS, JPRE, MATVEC, MSOLVE, NMSL, RGWK(LZ), RGWK(LV),&
           RGWK(LHES), RGWK(LQ), LGMR, RWORK, IWORK, RGWK(LW),&
            RGWK(LDL), RHOL, NRMAX, B, BNRM, X, RGWK(LXL), ITOL,&
            TOL, NELT, IA, JA, A, ISYM, IUNIT, IFLAG, ERR)
      ITER = ITER + LGMR
      NMS = NMS + NMSL
!
!
      LZM1 = LZ - 1
      DO 110 I = 1,N
         X(I) = X(I) + RGWK(LZM1+I)
 110  CONTINUE
      IF( IFLAG.EQ.0 ) GOTO 600
      IF( IFLAG.EQ.1 ) THEN
         NRSTS = NRSTS + 1
         GOTO 100
      ENDIF
      IF( IFLAG.EQ.2 ) GOTO 620
!
 600  CONTINUE
      IGWK(7) = NMS
      RGWK(1) = RHOL
      IERR = 0
      RETURN
!
 610  CONTINUE
      IGWK(7) = NMS
      RGWK(1) = RHOL
      IERR = 1
      RETURN
!
 620  CONTINUE
      IGWK(7) = NMS
      RGWK(1) = RHOL
      IERR = 2
      RETURN
      
 640  CONTINUE
      ERR = TOL
      IERR = -1
      RETURN
      
 650  CONTINUE
      ERR = TOL
      IERR = -2
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DGMRES
!
!
      RETURN
      END SUBROUTINE DGMRES
!
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
      SUBROUTINE DSLUGM(N,B,X,NELT,IA,JA,A,NSAVE,TOL,ITMAX,ITER,&
     &                  ERR,IERR,IUNIT,RWORK,LENW,IWORK,LENIW)
!
!
!
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
!
      INTEGER  N, NELT, IA(NELT), JA(NELT), ISYM, NSAVE, ITOL
      INTEGER  ITMAX, ITER, IERR, IUNIT, LENW, LENIW, IWORK(LENIW)
      REAL(KIND = 8) B(N), X(N), A(NELT), TOL, ERR, RWORK(LENW)
      EXTERNAL DSMV, DSLUI
!
      INTEGER, PARAMETER :: LOCRB=1, LOCIB=11
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSLUGM
!
!
      ISYM = 0
      ITOL = 0
!
      IERR = 0
      ERR  = 0.0d0
      IF( NSAVE.LE.1 ) THEN
         IERR = 3
         RETURN
      ENDIF
      CALL DS2Y( N, NELT, IA, JA, A, ISYM, IUNIT )
!
      NL = 0
      NU = 0
      DO 20 ICOL = 1, N
!         Don't count diagonal.
         JBGN = JA(ICOL)+1
         JEND = JA(ICOL+1)-1
         IF( JBGN.LE.JEND ) THEN
            DO 10 J = JBGN, JEND
               IF( IA(J).GT.ICOL ) THEN
                  NL = NL + 1
               ELSE
                  NU = NU + 1
               ENDIF
 10         CONTINUE
         ENDIF
 20   CONTINUE
!         
      LOCIGW = LOCIB
      LOCIL = LOCIGW + 20
      LOCJL = LOCIL + N+1
      LOCIU = LOCJL + NL
      LOCJU = LOCIU + NU
      LOCNR = LOCJU + N+1
      LOCNC = LOCNR + N
      LOCIW = LOCNC + N
!
      LOCL = LOCRB
      LOCDIN = LOCL + NL
      LOCU = LOCDIN + N
      LOCRGW = LOCU + NU
      LOCW = LOCRGW + 1+N*(NSAVE+6)+NSAVE*(NSAVE+3)
!
      CALL DCHKW( 'DSLUGM', LOCIW, LENIW, LOCW, LENW, IERR, ITER, ERR )
      IF( IERR.NE.0 ) RETURN
!
      IWORK(1) = LOCIL
      IWORK(2) = LOCJL
      IWORK(3) = LOCIU
      IWORK(4) = LOCJU
      IWORK(5) = LOCL
      IWORK(6) = LOCDIN
      IWORK(7) = LOCU
      IWORK(9) = LOCIW
      IWORK(10) = LOCW
!
      CALL DSILUS( N, NELT, IA, JA, A, ISYM, NL, IWORK(LOCIL),&
          IWORK(LOCJL), RWORK(LOCL), RWORK(LOCDIN), NU, IWORK(LOCIU),&
          IWORK(LOCJU), RWORK(LOCU), IWORK(LOCNR), IWORK(LOCNC) )
!         
      IWORK(LOCIGW  ) = NSAVE
      IWORK(LOCIGW+1) = NSAVE
      IWORK(LOCIGW+2) = 0
      IWORK(LOCIGW+3) = -1
      IWORK(LOCIGW+4) = ITMAX/NSAVE
      MYITOL = 0
!      
      CALL DGMRES( N, B, X, NELT, IA, JA, A, ISYM, DSMV, DSLUI,&
          MYITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, RWORK, RWORK,&
          RWORK(LOCRGW), LENW-LOCRGW, IWORK(LOCIGW), 20,&
          RWORK, IWORK )
!
      IF( ITER.GT.ITMAX ) IERR = 2
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DSLUGM
!
!
      RETURN
      END SUBROUTINE DSLUGM
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
      SUBROUTINE DHELS(A, LDA, N, Q, B)
!
!
!
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
!
      INTEGER LDA, N
!
      REAL(KIND = 8) A(LDA,*), B(*), Q(*)
!
      INTEGER IQ, K, KB, KP1
      REAL(KIND = 8) C, S, T, T1, T2
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DHELS
!
!
      DO 20 K = 1, N
         KP1 = K + 1
         IQ     = 2*(K-1) + 1
         C      = Q(IQ)
         S      = Q(IQ+1)
         T1     = B(K)
         T2     = B(KP1)
         B(K)   = C*T1 - S*T2
         B(KP1) = S*T1 + C*T2
 20   CONTINUE
!
!
      DO 40 KB = 1, N
         K    = N + 1 - KB
         B(K) = B(K)/A(K,K)
         T    =-B(K)
         DO 38 I = 1,K-1
            B(I) = B(I) + T*A(I,K)
   38    CONTINUE
 40   CONTINUE
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DHELS
!
!
      RETURN
      END SUBROUTINE DHELS
!
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
      SUBROUTINE DHEQR(A, LDA, N, Q, INFO, IJOB)
!
!
!
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      INTEGER LDA, N, INFO, IJOB
      REAL(KIND = 8) A(LDA,*), Q(*)
!
!
      INTEGER I, IQ, J, K, KM1, KP1, NM1
      REAL(KIND = 8) C, S, T, T1, T2
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DHEQR
!
!
      IF (IJOB .GT. 1) GO TO 70
!
      INFO = 0
      DO 60 K = 1, N
         KM1 = K - 1
         KP1 = K + 1
!
         IF (KM1 .LT. 1) GO TO 20
         DO 10 J = 1, KM1
            I  = 2*(J-1) + 1
            T1 = A(J,K)
            T2 = A(J+1,K)
            C  = Q(I)
            S  = Q(I+1)
            A(J,K)   = C*T1 - S*T2
            A(J+1,K) = S*T1 + C*T2
 10      CONTINUE
!
!
 20      CONTINUE
         IQ = 2*KM1 + 1
         T1 = A(K,K)
         T2 = A(KP1,K)
         IF( T2.EQ.0.0D0 ) THEN
            C = 1.0D0
            S = 0.0D0
         ELSEIF( ABS(T2).GE.ABS(T1) ) THEN
            T = T1/T2
            S = -1.0D0/SQRT(1.0D0+T*T)
            C = -S*T
         ELSE
            T = T2/T1
            C = 1.0D0/SQRT(1.0D0+T*T)
            S = -C*T
         ENDIF
         Q(IQ)   = C
         Q(IQ+1) = S
         A(K,K)  = C*T1 - S*T2
         IF( A(K,K).EQ.0.0D0 ) INFO = K
 60   CONTINUE
      RETURN
      
 70   CONTINUE
      NM1 = N - 1
      
      DO 100 K = 1,NM1
         I  = 2*(K-1) + 1
         T1 = A(K,N)
         T2 = A(K+1,N)
         C  = Q(I)
         S  = Q(I+1)
         A(K,N)   = C*T1 - S*T2
         A(K+1,N) = S*T1 + C*T2
 100  CONTINUE

      INFO = 0
      T1 = A(N,N)
      T2 = A(N+1,N)
      IF ( T2.EQ.0.0D0 ) THEN
         C = 1.0D0
         S = 0.0D0
      ELSEIF( ABS(T2).GE.ABS(T1) ) THEN
         T = T1/T2
         S = -1.0D0/SQRT(1.0D0+T*T)
         C = -S*T
      ELSE
         T = T2/T1
         C = 1.0D0/SQRT(1.0D0+T*T)
         S = -C*T
      ENDIF
      
      IQ      = 2*N - 1
      Q(IQ)   = C
      Q(IQ+1) = S
      A(N,N)  = C*T1 - S*T2
      IF (A(N,N) .EQ. 0.0D0) INFO = N
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DHEQR
!
!
      RETURN
      END SUBROUTINE DHEQR
!
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
      SUBROUTINE DORTH(VNEW, V, HES, N, LL, LDHES, KMP, SNORMW)
!
!
!
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      INTEGER N, LL, LDHES, KMP
      REAL(KIND = 8) VNEW, V, HES, SNORMW
      DIMENSION VNEW(*), V(N,*), HES(LDHES,*)
!
      INTEGER I, I0
      REAL(KIND = 8) ARG, SUMDSQ, TEM, VNRM
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DORTH
!
!
       VNRM = 0.0d0
       DO 2 I = 1,N
          VNRM = VNRM + VNEW(I)*VNEW(I)
  2    CONTINUE
       VNRM = SQRT(VNRM)
      
      I0 = MAX0(1,LL-KMP+1)
      DO 10 I = I0,LL
         DDOT = 0.0D0
         DO 5 J = 1,N
            DDOT = DDOT + V(J,I)*VNEW(J)
    5    CONTINUE
         HES(I,LL) = DDOT

         TEM = -HES(I,LL)
         DO 8 J = 1,N
            VNEW(J) = VNEW(J) + TEM*V(J,I)
    8    CONTINUE
 10   CONTINUE
 
      SNORMW = 0.0D0
      DO 9 I = 1,N
         SNORMW = SNORMW + VNEW(I)*VNEW(I)
  9   CONTINUE
      SNORMW = SQRT(SNORMW)
         
      IF (VNRM + 0.001D0*SNORMW .NE. VNRM) RETURN
      SUMDSQ = 0.0D0
      DO 30 I = I0,LL
         DDOT = 0.0D0
         DO 25 J = 1,N
            DDOT = DDOT + V(J,I)*VNEW(J)
   25    CONTINUE
         TEM = -DDOT

         IF (HES(I,LL) + 0.001D0*TEM .EQ. HES(I,LL)) GO TO 30
         HES(I,LL) = HES(I,LL) - TEM
         
         DO 28 J = 1,N
            VNEW(J) = VNEW(J) + TEM*V(J,I)
   28    CONTINUE
         SUMDSQ = SUMDSQ + TEM**2
 30   CONTINUE
 
      IF (SUMDSQ .EQ. 0.0D0) RETURN
      ARG = MAX(0.0D0,SNORMW**2 - SUMDSQ)
      SNORMW = SQRT(ARG)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DORTH
!
!
      RETURN
      END SUBROUTINE DORTH
!
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
      SUBROUTINE DPIGMR(N, R0, SR, SZ, JSCAL, MAXL, MAXLP1, KMP, &
     &     NRSTS, JPRE, MATVEC, MSOLVE, NMSL, Z, V, HES, Q, LGMR,&
     &     RPAR, IPAR, WK, DL, RHOL, NRMAX, B, BNRM, X, XL,&
     &     ITOL, TOL, NELT, IA, JA, A, ISYM, IUNIT, IFLAG, ERR)
!
!
!
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      EXTERNAL MATVEC, MSOLVE
      INTEGER N,MAXL,MAXLP1,KMP,JPRE,NMSL,LGMR,IFLAG,JSCAL,NRSTS
      INTEGER NRMAX,ITOL,NELT,ISYM
      REAL(KIND = 8) RHOL, BNRM, TOL
      REAL(KIND = 8) R0(*), SR(*), SZ(*), Z(*), V(N,*)
      REAL(KIND = 8) HES(MAXLP1,*), Q(*), RPAR(*), WK(*), DL(*)
      REAL(KIND = 8) A(NELT), B(*), X(*), XL(*)
      INTEGER IPAR(*), IA(NELT), JA(NELT)
!
      INTEGER I, INFO, IP1, I2, J, K, LL, LLP1
      REAL(KIND = 8) R0NRM,C,DLNRM,PROD,RHO,S,SNORMW,TEM
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DPIGMR
!
!
      DO 5 I = 1,N
         Z(I) = 0.0D0
 5    CONTINUE
!
      IFLAG = 0
      LGMR  = 0
      NMSL  = 0
      ITMAX =(NRMAX+1)*MAXL
      
      IF ((JPRE .LT. 0) .AND.(NRSTS .EQ. 0)) THEN
      
         DO 8 I = 1,N
            WK(I) = R0(I)
    8    CONTINUE         
         
         CALL MSOLVE(N, WK, R0, NELT, IA, JA, A, ISYM, RPAR, IPAR)
         NMSL = NMSL + 1
      ENDIF
      
      IF (((JSCAL.EQ.2) .OR.(JSCAL.EQ.3)) .AND.(NRSTS.EQ.0)) THEN
         DO 10 I = 1,N
            V(I,1) = R0(I)*SR(I)
 10      CONTINUE
      ELSE
         DO 20 I = 1,N
            V(I,1) = R0(I)
 20      CONTINUE
      ENDIF
      
      R0NRM = 0.0d0
      DO 22 I = 1,N
         R0NRM = R0NRM + V(I,1)*V(I,1)
 22   CONTINUE
      R0NRM = SQRT(R0NRM)
      ITER = NRSTS*MAXL
!
!         
      ISDGMR = 0
      ITOL   = 0
      ERR    = R0NRM/BNRM
!         
      IF( IUNIT.NE.0 ) THEN
         IF( ITER.EQ.0 ) THEN
            WRITE(IUNIT,1000) N, ITOL, MAXL, KMP
         ENDIF
         WRITE(IUNIT,1010) ITER, ERR, ERR
      ENDIF
      IF ( ERR.LE.TOL ) ISDGMR = 1
!         
      IF(ISDGMR.NE.0) RETURN
!
      TEM = 1.0D0/R0NRM
      
      DO 33 I = 1,N
        V(I,1) = TEM*V(I,1)
   33 CONTINUE
!
!
      DO 50 J = 1,MAXL
         DO 40 I = 1,MAXLP1
            HES(I,J) = 0.0D0
 40      CONTINUE
 50   CONTINUE

      PROD = 1.0D0
      DO 90 LL = 1,MAXL
         LGMR = LL         
         IF ((JSCAL .EQ. 1) .OR.(JSCAL .EQ. 3)) THEN
            DO 60 I = 1,N
               WK(I) = V(I,LL)/SZ(I)
 60         CONTINUE
         ELSE
            DO 63 I = 1,N
               WK(I) = V(I,LL)
   63       CONTINUE         
         ENDIF
        
        IF (JPRE .GT. 0) THEN
           CALL MSOLVE(N,WK,Z,NELT,IA,JA,A,ISYM,RPAR,IPAR)
           NMSL = NMSL + 1
           CALL MATVEC(N,Z,V(1,LL+1),NELT,IA,JA,A,ISYM)
        ELSE
           CALL MATVEC(N,WK,V(1,LL+1),NELT,IA,JA,A,ISYM)
        ENDIF
        
        IF (JPRE .LT. 0) THEN
        
           DO 64 I = 1,N
              WK(I) = V(I,LL+1)
   64      CONTINUE         
           
           CALL MSOLVE(N,WK,V(1,LL+1),NELT,IA,JA,A,ISYM,RPAR,IPAR)
           NMSL = NMSL + 1
        ENDIF
        
        IF ((JSCAL .EQ. 2) .OR.(JSCAL .EQ. 3)) THEN
           DO 65 I = 1,N
              V(I,LL+1) = V(I,LL+1)*SR(I)
 65        CONTINUE
        ENDIF

        CALL DORTH(V(1,LL+1), V, HES, N, LL, MAXLP1, KMP, SNORMW)

        HES(LL+1,LL) = SNORMW
        CALL DHEQR(HES, MAXLP1, LL, Q, INFO, LL)
        IF (INFO .EQ. LL) GO TO 120
        
        PROD = PROD*Q(2*LL)
        RHO  = ABS(PROD*R0NRM)
        
        IF ((LL.GT.KMP) .AND.(KMP.LT.MAXL)) THEN
           IF (LL .EQ. KMP+1) THEN
              DO 68 I = 1,N
                 DL(I) = V(I,1)
   68         CONTINUE         

              DO 75 I = 1,KMP
                 IP1 = I + 1
                 I2  = I*2
                 S   = Q(I2)
                 C   = Q(I2-1)
                 DO 70 K = 1,N
                    DL(K) = S*DL(K) + C*V(K,IP1)
 70              CONTINUE
 75           CONTINUE
           ENDIF
           S = Q(2*LL)
           C = Q(2*LL-1)/SNORMW
           LLP1 = LL + 1
           DO 80 K = 1,N
              DL(K) = S*DL(K) + C*V(K,LLP1)
 80        CONTINUE

           DLNRM = 0.0d0
           DO 82 I = 1,N
              DLNRM = DLNRM + DL(I)*DL(I)
 82        CONTINUE
           DLNRM = SQRT(DLNRM)
           RHO   = RHO*DLNRM
        ENDIF
        RHOL = RHO
!        
        ITER = NRSTS*MAXL + LGMR
!
!         
        ISDGMR = 0
        ITOL   = 0
        ERR    = RHOL/BNRM      
!         
        IF( IUNIT.NE.0 ) THEN
           IF( ITER.EQ.0 ) THEN
              WRITE(IUNIT,1000) N, ITOL, MAXL, KMP
           ENDIF
           WRITE(IUNIT,1010) ITER, ERR, ERR
        ENDIF
        IF ( ERR.LE.TOL ) ISDGMR = 1
!         
        IF(ISDGMR.NE.0) GO TO 200
!
        IF (LL .EQ. MAXL) GO TO 100
        TEM = 1.0D0/SNORMW
        
      DO 88 I = 1,N
        V(I,LL+1) = TEM*V(I,LL+1)
   88 CONTINUE
        
 90   CONTINUE
 100  CONTINUE

      IF (RHO .LT. R0NRM) GO TO 150
 120  CONTINUE
      IFLAG = 2
!
!
      DO 130 I = 1,N
         Z(I) = 0.0d0
 130  CONTINUE
      RETURN
 150  IFLAG = 1
!
!
      IF (NRMAX .GT. 0) THEN
!
         IF (KMP .EQ. MAXL) THEN
            DO 158 I = 1,N
               DL(I) = V(I,1)
  158       CONTINUE         

            LLM1 = MAXL - 1
            DO 165 I = 1,LLM1
               IP1 = I + 1
               I2  = I*2
               S   = Q(I2)
               C   = Q(I2-1)
               DO 160 K = 1,N
                  DL(K) = S*DL(K) + C*V(K,IP1)
160            CONTINUE
165         CONTINUE
            S    = Q(2*MAXL)
            C    = Q(2*MAXL-1)/SNORMW
            LLP1 = MAXL + 1
            DO 170 K = 1,N
               DL(K) = S*DL(K) + C*V(K,LLP1)
170         CONTINUE
         ENDIF
!
!
         TEM = R0NRM*PROD
         DO 175 I = 1,N
           DL(I) = TEM*DL(I)
  175    CONTINUE
!
      ENDIF
!
!
!
 200  CONTINUE

      LL   = LGMR
      LLP1 = LL + 1
      DO 210 K = 1,LLP1
         R0(K) = 0.0D0
 210  CONTINUE

      R0(1) = R0NRM
      CALL DHELS(HES, MAXLP1, LL, Q, R0)
      DO 220 K = 1,N
         Z(K) = 0.0D0
 220  CONTINUE

      DO 230 I = 1,LL
          DO 228 J = 1,N
             Z(J) = Z(J) + R0(I)*V(J,I)
  228     CONTINUE
 230  CONTINUE

      IF ((JSCAL .EQ. 1) .OR.(JSCAL .EQ. 3)) THEN
         DO 240 I = 1,N
            Z(I) = Z(I)/SZ(I)
 240     CONTINUE
      ENDIF

      IF (JPRE .GT. 0) THEN
         DO 250 I = 1,N
            WK(I) = Z(I)
  250    CONTINUE         
         CALL MSOLVE(N, WK, Z, NELT, IA, JA, A, ISYM, RPAR, IPAR)
         NMSL = NMSL + 1
      ENDIF

 1000 FORMAT(' Generalized Minimum Residual(',I3,I3,') for ',&
          'N, ITOL = ',I5, I5,&
          /' ITER','   Natral Err Est','   Error Estimate')
 1010 FORMAT(1X,I4,1X,E16.7,1X,E16.7)
 1020 FORMAT(1X,' ITER = ',I5, ' IELMAX = ',I5,&
          ' |R(IELMAX)/X(IELMAX)| = ',E12.5)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DPIGMR
!
!
      RETURN
      END SUBROUTINE DPIGMR
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
      SUBROUTINE DSILUS(N,NELT,IA,JA,A,ISYM,NL,IL,JL,&
     &                  L,DINV,NU,IU,JU,U,NROW,NCOL)
!!Parallel version of incomplete LDU preconditioner changed by Aman Rusia on 160616
!!N - size of system (originally), NELT, IA, JA, A, ISYM, NL, IWORK(LOCIL),&
  !        & IWORK(LOCJL), RWORK(LOCL), RWORK(LOCDIN), NU, IWORK(LOCIU),&
  !       & IWORK(LOCJU), RWORK(LOCU), IWORK(LOCNR), IWORK(LOCNC)
!NROW - size 1:N. Since U is stored in SLAP row format. Nrow(i) is number of upper elements in ith column
!NCOL - size 1:N. Since L is stored in SLAP column format. Ncol(i) is number of lower elements in ith column
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      INTEGER N, NELT,IA(NELT),JA(NELT),ISYM,NL,IL(NL),JL(NL)
      INTEGER NU, IU(NU), JU(NU), NROW(N), NCOL(N)			!Although the size of IL and IU are set as NL and NU, there is enough space as provided in DSLUCS. (check!)
      REAL(KIND = 8) A(NELT), L(NL), DINV(N), U(NU)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSILUS
!
!
      NROW(1:N) = 0
      NCOL(1:N) = 0
!
      DO 30 ICOL = 1, N
         JBGN = JA(ICOL)+1
         JEND = JA(ICOL+1)-1
         IF( JBGN.LE.JEND ) THEN
            DO 20 J = JBGN, JEND
               IF( IA(J).LT.ICOL ) THEN
                  NCOL(ICOL) = NCOL(ICOL) + 1
               ELSE
                  NROW(IA(J)) = NROW(IA(J)) + 1
               ENDIF
 20         CONTINUE
         ENDIF
 30   CONTINUE
!Forming JU - column of SLAP column format in U. and IL - row of SLAP row format in L. 1st element of U and L are always 0.
      JU(1) = 1
      IL(1) = 1
      DO 40 ICOL = 1, N
         IL(ICOL+1) = IL(ICOL) + NROW(ICOL)
         JU(ICOL+1) = JU(ICOL) + NCOL(ICOL)
         NROW(ICOL) = IL(ICOL)
         NCOL(ICOL) = JU(ICOL)
 40   CONTINUE
!Copying values into IU, U, JL and L. Note no diagonal elements are stored in either L or U        
      DO 60 ICOL = 1, N
         DINV(ICOL) = A(JA(ICOL))
         JBGN = JA(ICOL)+1
         JEND = JA(ICOL+1)-1
         IF( JBGN.LE.JEND ) THEN
            DO 50 J = JBGN, JEND
               IROW = IA(J)
               IF( IROW.LT.ICOL ) THEN
                  IU(NCOL(ICOL)) = IROW
                  U(NCOL(ICOL)) = A(J)
                  NCOL(ICOL) = NCOL(ICOL) + 1
               ELSE
                  JL(NROW(IROW)) = ICOL
                  L(NROW(IROW)) = A(J)
                  NROW(IROW) = NROW(IROW) + 1
               ENDIF
 50         CONTINUE
         ENDIF
 60   CONTINUE
!Sorting of row and column number. So the appear incremently in each format.
      DO 110 K = 2, N
         JBGN = JU(K)
         JEND = JU(K+1)-1
         IF( JBGN.LT.JEND ) THEN !Do the sorting of row numbers in i increment order for each column.
            DO 80 J = JBGN, JEND-1
               DO 70 I = J+1, JEND
                  IF( IU(J).GT.IU(I) ) THEN
                     ITEMP = IU(J)
                     IU(J) = IU(I)
                     IU(I) = ITEMP
                     TEMP = U(J)
                     U(J) = U(I)
                     U(I) = TEMP
                  ENDIF
 70            CONTINUE
 80         CONTINUE
         ENDIF
         IBGN = IL(K)
         IEND = IL(K+1)-1
         IF( IBGN.LT.IEND ) THEN !Do the sorting of column numbers in each row.
            DO 100 I = IBGN, IEND-1
               DO 90 J = I+1, IEND
                  IF( JL(I).GT.JL(J) ) THEN
                     JTEMP = JU(I)
                     JU(I) = JU(J)
                     JU(J) = JTEMP
                     TEMP = L(I)
                     L(I) = L(J)
                     L(J) = TEMP
                  ENDIF
 90            CONTINUE
 100        CONTINUE
         ENDIF
 110  CONTINUE
!Carrying out forward Sweep
      DO 300 I=2,N  
!Changing lower elements         
         INDX1 = IL(I)
         INDX2 = IL(I+1) - 1
         IF(INDX1 .GT. INDX2) GO TO 200 !No lower elements in the row
         DO 190 INDX=INDX1,INDX2		!Column loop for a row in L
            IF(INDX .EQ. INDX1) GO TO 180 !only one lower element
            INDXR1 = INDX1				
            INDXR2 = INDX - 1
            INDXC1 = JU(JL(INDX))    !JU(JL(INDX)) is just the index of the first element in the same column as INDX present in U
            INDXC2 = JU(JL(INDX)+1) - 1 !Lower most element in a column of U where INDXC1 is the top most.
            IF(INDXC1 .GT. INDXC2) GO TO 180 !No elements in U in the given column 
 160        KR = JL(INDXR1) !Column offset of first element in the current row.
 170        KC = IU(INDXC1) !Row offset of first element in the current column @U
            IF(KR .GT. KC) THEN
               INDXC1 = INDXC1 + 1	
               IF(INDXC1 .LE. INDXC2) GO TO 170
            ELSE IF(KR .LT. KC) THEN
               INDXR1 = INDXR1 + 1
               IF(INDXR1 .LE. INDXR2) GO TO 160
            ELSE IF(KR .EQ. KC) THEN
               L(INDX) = L(INDX) - L(INDXR1)*DINV(KC)*U(INDXC1) !go through transformations. NOTE - EXTRA DINV(KC) because U is also divided by DINV - since this is LDU factorization and not LU.
               INDXR1 = INDXR1 + 1
               INDXC1 = INDXC1 + 1
               IF(INDXR1 .LE. INDXR2 .AND. INDXC1 .LE. INDXC2) GO TO 160
            END IF
 180        L(INDX) = L(INDX)/DINV(JL(INDX)) !Final value stored.
 190     CONTINUE
!Changing upper elements         
 200     INDX1 = JU(I)
         INDX2 = JU(I+1) - 1
         IF(INDX1 .GT. INDX2) GO TO 260
         DO 250 INDX=INDX1,INDX2
            IF(INDX .EQ. INDX1) GO TO 240
            INDXC1 = INDX1
            INDXC2 = INDX - 1
            INDXR1 = IL(IU(INDX))
            INDXR2 = IL(IU(INDX)+1) - 1
            IF(INDXR1 .GT. INDXR2) GO TO 240
 210        KR = JL(INDXR1)
 220        KC = IU(INDXC1)
            IF(KR .GT. KC) THEN
               INDXC1 = INDXC1 + 1
               IF(INDXC1 .LE. INDXC2) GO TO 220
            ELSE IF(KR .LT. KC) THEN
               INDXR1 = INDXR1 + 1
               IF(INDXR1 .LE. INDXR2) GO TO 210
            ELSE IF(KR .EQ. KC) THEN
               U(INDX) = U(INDX) - L(INDXR1)*DINV(KC)*U(INDXC1)
               INDXR1 = INDXR1 + 1
               INDXC1 = INDXC1 + 1
               IF(INDXR1 .LE. INDXR2 .AND. INDXC1 .LE. INDXC2) GO TO 210
            END IF
 240        U(INDX) = U(INDX)/DINV(IU(INDX))
 250     CONTINUE
!Changing diagonal elements  
 260     INDXR1 = IL(I)
         INDXR2 = IL(I+1) - 1
         IF(INDXR1 .GT. INDXR2) GO TO 300
         INDXC1 = JU(I)
         INDXC2 = JU(I+1) - 1
         IF(INDXC1 .GT. INDXC2) GO TO 300
 270     KR = JL(INDXR1)
 280     KC = IU(INDXC1)
         IF(KR .GT. KC) THEN
            INDXC1 = INDXC1 + 1
            IF(INDXC1 .LE. INDXC2) GO TO 280
         ELSE IF(KR .LT. KC) THEN
            INDXR1 = INDXR1 + 1
            IF(INDXR1 .LE. INDXR2) GO TO 270
         ELSE IF(KR .EQ. KC) THEN
            DINV(I) = DINV(I) - L(INDXR1)*DINV(KC)*U(INDXC1)
            INDXR1 = INDXR1 + 1
            INDXC1 = INDXC1 + 1
            IF(INDXR1 .LE. INDXR2 .AND. INDXC1 .LE. INDXC2) GO TO 270
         END IF
!         
 300  CONTINUE
!         
      DINV(1:N) = 1.0d0/DINV(1:N)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DSILUS
!
!
      RETURN
      END SUBROUTINE DSILUS
!
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
      SUBROUTINE DCHKW( NAME, LOCIW, LENIW, LOCW, LENW,&
     &     IERR, ITER, ERR )
!
!
!
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      CHARACTER, dimension(*) ::NAME
      CHARACTER, dimension(72) ::MESG
      INTEGER LOCIW, LENIW, LOCW, LENW, IERR, ITER
      REAL(KIND = 8) ERR
!
      REAL(KIND = 8) DMACH(5)
      DATA DMACH(2) / 1.79769313486231D+308 /
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DCHKW
!
!
      IERR = 0
      IF(LOCIW.GT.LENIW) THEN
         IERR = 1
         ITER = 0
         ERR = DMACH(2)
         MESG = NAME // ': INTEGER work array too short. '//&
             ' IWORK needs i1: have allocated i2.'
      ENDIF
!
      IF(LOCW.GT.LENW) THEN
         IERR = 1
         ITER = 0
         ERR = DMACH(2)
         MESG = NAME // ': REAL work array too short. '//&
             ' RWORK needs i1: have allocated i2.'
      ENDIF
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DCHKW
!
!
      RETURN
      END SUBROUTINE DCHKW
!
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
      SUBROUTINE QS2I1D( IA, JA, A, N, KFLAG, IUNIT )
!
!
!
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      DIMENSION IL(21),IU(21)
      INTEGER   IA(N),JA(N),IT,IIT,JT,JJT
      REAL(KIND = 8) A(N), TA, TTA
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of QS2I1D
!
!
      NN = N
      IF (NN.LT.1) THEN
         WRITE(IUNIT,6100) 
 6100 FORMAT(/,'QS2I1D- the number of values to', &
     &         ' be sorted was NOT POSITIVE.')
         RETURN
      ENDIF
      IF( N.EQ.1 ) RETURN
      KK = IABS(KFLAG)
      IF ( KK.NE.1 ) THEN
         WRITE(IUNIT,6101) 
 6101 FORMAT(/,'QS2I1D- the sort control parameter, k, ',&
     &         'was not 1 OR -1.')
         RETURN
      ENDIF
!
      IF( KFLAG.LT.1 ) THEN
         DO 20 I=1,NN
            IA(I) = -IA(I)
 20      CONTINUE
      ENDIF
!
      M = 1
      I = 1
      J = NN
      R = 3.75D-1
 210  IF( R.LE.5.898437D-1 ) THEN
         R = R + 3.90625D-2
      ELSE
         R = R-2.1875D-1
      ENDIF
 225  K = I
!
!
      IJ = I + IDINT( DBLE(J-I)*R )
      IT = IA(IJ)
      JT = JA(IJ)
      TA = A(IJ)
!
!
      IF( IA(I).GT.IT ) THEN
         IA(IJ) = IA(I)
         IA(I)  = IT
         IT     = IA(IJ)
         JA(IJ) = JA(I)
         JA(I)  = JT
         JT     = JA(IJ)
         A(IJ)  = A(I)
         A(I)   = TA
         TA     = A(IJ)
      ENDIF
      L=J
!                           
!
      IF( IA(J).LT.IT ) THEN
         IA(IJ) = IA(J)
         IA(J)  = IT
         IT     = IA(IJ)
         JA(IJ) = JA(J)
         JA(J)  = JT
         JT     = JA(IJ)
         A(IJ)  = A(J)
         A(J)   = TA
         TA     = A(IJ)
!
!
         IF ( IA(I).GT.IT ) THEN
            IA(IJ) = IA(I)
            IA(I)  = IT
            IT     = IA(IJ)
            JA(IJ) = JA(I)
            JA(I)  = JT
            JT     = JA(IJ)
            A(IJ)  = A(I)
            A(I)   = TA
            TA     = A(IJ)
         ENDIF
      ENDIF
!
!
  240 L=L-1
      IF( IA(L).GT.IT ) GO TO 240
!
!
  245 K=K+1
      IF( IA(K).LT.IT ) GO TO 245
!
!
      IF( K.LE.L ) THEN
         IIT   = IA(L)
         IA(L) = IA(K)
         IA(K) = IIT
         JJT   = JA(L)
         JA(L) = JA(K)
         JA(K) = JJT
         TTA   = A(L)
         A(L)  = A(K)
         A(K)  = TTA
         GOTO 240
      ENDIF
!
!
      IF( L-I.GT.J-K ) THEN
         IL(M) = I
         IU(M) = L
         I = K
         M = M+1
      ELSE
         IL(M) = K
         IU(M) = J
         J = L
         M = M+1
      ENDIF
      GO TO 260
!
!                                  
  255 M = M-1
      IF( M.EQ.0 ) GO TO 300
      I = IL(M)
      J = IU(M)
  260 IF( J-I.GE.1 ) GO TO 225
      IF( I.EQ.J ) GO TO 255
      IF( I.EQ.1 ) GO TO 210
      I = I-1
  265 I = I+1
      IF( I.EQ.J ) GO TO 255
      IT = IA(I+1)
      JT = JA(I+1)
      TA =  A(I+1)
      IF( IA(I).LE.IT ) GO TO 265
      K=I
  270 IA(K+1) = IA(K)
      JA(K+1) = JA(K)
      A(K+1)  =  A(K)
      K = K-1
      IF( IT.LT.IA(K) ) GO TO 270
      IA(K+1) = IT
      JA(K+1) = JT
      A(K+1)  = TA
      GO TO 265
!
!
  300 IF( KFLAG.LT.1 ) THEN
         DO 310 I=1,NN
            IA(I) = -IA(I)
 310     CONTINUE
      ENDIF
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of QS2I1D
!
!
      RETURN
      END SUBROUTINE QS2I1D
!
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
      SUBROUTINE DS2Y(N, NELT, IA, JA, A, ISYM,IUNIT )
!Parallel Version written by Aman Rusia Date 160616
!
!
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      INTEGER N, NELT, IA(NELT), JA(NELT), ISYM
      REAL(KIND = 8) A(NELT)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DS2Y
!
!
      IF( JA(N+1).EQ.NELT+1 ) RETURN
      CALL QS2I1D( JA, IA, A, NELT, 1,IUNIT )
      JA(1) = 1
      DO 20 ICOL = 1, N-1
         DO 10 J = JA(ICOL)+1, NELT
            IF( JA(J).NE.ICOL ) THEN
               JA(ICOL+1) = J
               GOTO 20
            ENDIF
 10      CONTINUE
 20   CONTINUE
      JA(N+1) = NELT+1
!         
      JA(N+2) = 0
!
      DO 70 ICOL = 1, N
         IBGN = JA(ICOL)
         IEND = JA(ICOL+1)-1
         DO 30 I = IBGN, IEND
            IF( IA(I).EQ.ICOL ) THEN
               ITEMP = IA(I)
               IA(I) = IA(IBGN)
               IA(IBGN) = ITEMP
               TEMP = A(I)
               A(I) = A(IBGN)
               A(IBGN) = TEMP
               GOTO 40
            ENDIF
 30      CONTINUE
 40      IBGN = IBGN + 1
         IF( IBGN.LT.IEND ) THEN
            DO 60 I = IBGN, IEND
               DO 50 J = I+1, IEND
                  IF( IA(I).GT.IA(J) ) THEN
                     ITEMP = IA(I)
                     IA(I) = IA(J)
                     IA(J) = ITEMP
                     TEMP = A(I)
                     A(I) = A(J)
                     A(J) = TEMP
                  ENDIF
 50            CONTINUE
 60         CONTINUE
         ENDIF
 70   CONTINUE
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DS2Y
!
!
      RETURN
      END SUBROUTINE DS2Y
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!      D I R E C T    S O L V E R S
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE DGBTRF(M,N,KL,KU,AB,LDAB,IPIV,INFO)
!
!
!
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
!
!      INTEGER IPIV(*)
      INTEGER, DIMENSION(n) :: IPIV
!
!      REAL(KIND = 8) AB(LDAB,*)
      REAL(KIND = 8), DIMENSION(LDAB,n) :: AB
!
      PARAMETER (ONE = 1.0D0, ZERO = 0.0D0)
      PARAMETER (NBMAX = 32,LDWORK = NBMAX+1 )
!
      REAL(KIND = 8) WORK13(LDWORK,NBMAX),WORK31(LDWORK,NBMAX)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DGBTRF
!
!
      KV = KU + KL
      IF(KU.LE.64) THEN
         NB = 1
      ELSE
         NB = 32
      END IF
!
      NB = MIN(NB,NBMAX)
      IF(NB.LE.1.OR.NB.GT.KL) THEN
         CALL DGBTF2(M,N,KL,KU,AB,LDAB,IPIV,INFO)
      ELSE
         DO 20 J = 1,NB
            DO 10 I = 1,J-1
               WORK13(I,J) = ZERO
   10       CONTINUE
   20    CONTINUE
!
         DO 40 J = 1,NB
            DO 30 I = J+1,NB
               WORK31(I,J) = ZERO
   30       CONTINUE
   40    CONTINUE
!
!
!
         JU = 1
         DO 180 J = 1, MIN( M, N ), NB
            JB = MIN( NB, MIN( M, N )-J+1 )
            I2 = MIN( KL-JB, M-J-JB+1 )
            I3 = MIN( JB, M-J-KL+1 )
            DO 80 JJ = J, J + JB - 1
               KM = MIN( KL, M-JJ )
!               JP = IDAMAX( KM+1, AB( KV+1, JJ ), 1 )
               dmax  = abs(AB(KV+1,JJ))
               idamx = 1
               do 31 i8 = 2,KM+2
                  if(i8.gt.km+1) go to 32
                  if(abs(AB(KV+i8,JJ)).le.dmax) go to 31
                  idamx = i8
                  dmax  = abs(AB(KV+i8,JJ))
   31          continue
   32          jp = idamx
               IPIV( JJ ) = JP + JJ - J
               IF( AB(KV+JP,JJ).NE.ZERO ) THEN
                  JU = MAX(JU,MIN(JJ+KU+JP-1,N))
                  IF(JP.NE.1) THEN
                     IF( JP+JJ-1.LT.J+KL ) THEN
                        CALL DSWAP(JB,AB(KV+1+JJ-J,J),LDAB-1,&
                                  AB(KV+JP+JJ-J,J),LDAB-1)
                     ELSE
                        CALL DSWAP(JJ-J,AB(KV+1+JJ-J,J),LDAB-1,&
                                  WORK31(JP+JJ-J-KL,1),LDWORK)
                        CALL DSWAP(J+JB-JJ,AB(KV+1,JJ),LDAB-1,&
                                  AB(KV+JP,JJ),LDAB-1)
                     END IF
                  END IF
!
                  u4 = ONE/AB(KV+1,JJ)
                  do 78 i4=1,km
                     ab(KV+1+i4,JJ)=u4*ab(KV+1+i4,JJ)
   78             continue
!
!                  CALL DSCAL(KM,ONE/AB(KV+1,JJ),AB(KV+2,JJ),1)
                  JM = MIN( JU, J+JB-1 )
                  IF( JM.GT.JJ )&
                    CALL DGER(KM,JM-JJ,-ONE,AB(KV+2,JJ),1,&
                              AB(KV,JJ+1),LDAB-1,&
                              AB(KV+1,JJ+1),LDAB-1)
               ELSE
                  IF(INFO.EQ.0) INFO = JJ
               END IF
!
               NW = MIN( JJ-J+1, I3 )
               IF(NW.GT.0) then
                  do 79 i6=1,nw
                     work31(i6,jj-j+1) = ab(kv+kl-jj+j+i6,jj)
   79             continue
               end if
   80       CONTINUE
!
!
!
            IF( J+JB.LE.N ) THEN
               J2 = MIN( JU-J+1, KV ) - JB
               J3 = MAX( 0, JU-J-KV+1 )
               CALL DLASWP(J2,AB(KV+1-JB,J+JB),LDAB-1,1,JB,IPIV(J),1)
               DO 90 I = J, J + JB - 1
                  IPIV( I ) = IPIV( I ) + J - 1
   90          CONTINUE
               K2 = J - 1 + JB + J2
               DO 110 I = 1, J3
                  JJ = K2 + I
                  DO 100 II = J + I - 1, J + JB - 1
                     IP = IPIV( II )
                     IF( IP.NE.II ) THEN
                        TEMP = AB( KV+1+II-JJ, JJ )
                        AB( KV+1+II-JJ, JJ ) = AB( KV+1+IP-JJ, JJ )
                        AB( KV+1+IP-JJ, JJ ) = TEMP
                     END IF
  100             CONTINUE
  110          CONTINUE
!
!
!
               IF( J2.GT.0 ) THEN
                  CALL DTRSM( JB, J2, AB( KV+1, J ), LDAB-1,&
                             AB( KV+1-JB, J+JB ), LDAB-1 )
!*
                  IF( I2.GT.0 ) THEN
                     CALL DGEMM( I2, J2,&
                                JB, -ONE, AB( KV+1+JB, J ), LDAB-1,&
                                AB( KV+1-JB, J+JB ), LDAB-1, &
                                AB( KV+1, J+JB ), LDAB-1 )
                  END IF
!*
                  IF( I3.GT.0 ) THEN
                     CALL DGEMM( I3, J2,&
                                JB, -ONE, WORK31, LDWORK,&
                                AB( KV+1-JB, J+JB ), LDAB-1, &
                                AB( KV+KL+1-JB, J+JB ), LDAB-1 )
                  END IF
               END IF
!
!
!
               IF( J3.GT.0 ) THEN
                  DO 130 JJ = 1, J3
                     DO 120 II = JJ, JB
                        WORK13( II, JJ ) = AB( II-JJ+1, JJ+J+KV-1 )
  120                CONTINUE
  130             CONTINUE
                  CALL DTRSM( JB, J3, AB( KV+1, J ), LDAB-1,&
                             WORK13, LDWORK )
!*
                  IF( I2.GT.0 ) THEN
                     CALL DGEMM( I2, J3,&
                                JB, -ONE, AB( KV+1+JB, J ), LDAB-1,&
                                WORK13, LDWORK, AB( 1+JB, J+KV ),&
                                LDAB-1 )
                  END IF
!*
                  IF( I3.GT.0 ) THEN
                     CALL DGEMM( I3, J3,&
                                JB, -ONE, WORK31, LDWORK, WORK13,&
                                LDWORK, AB( 1+KL, J+KV ), LDAB-1 )
                  END IF
!
                  DO 150 JJ = 1, J3
                     DO 140 II = JJ, JB
                        AB( II-JJ+1, JJ+J+KV-1 ) = WORK13( II, JJ )
  140                CONTINUE
  150             CONTINUE
               END IF
            ELSE
               DO 160 I = J, J + JB - 1
                  IPIV( I ) = IPIV( I ) + J - 1
  160          CONTINUE
            END IF
!
!
!
            DO 170 JJ = J + JB - 1, J, -1
               JP = IPIV( JJ ) - JJ + 1
               IF( JP.NE.1 ) THEN
                  IF( JP+JJ-1.LT.J+KL ) THEN
                     CALL DSWAP(JJ-J,AB(KV+1+JJ-J,J),LDAB-1,&
                               AB(KV+JP+JJ-J,J),LDAB-1)
                  ELSE
                     CALL DSWAP(JJ-J,AB(KV+1+JJ-J,J),LDAB-1,&
                               WORK31(JP+JJ-J-KL,1),LDWORK)
                  END IF
               END IF
               NW = MIN( I3, JJ-J+1 )
               IF(NW.GT.0) then
                  do 169 i6=1,nw
                     ab(kv+kl-jj+j+i6,jj) = work31(i6,jj-j+1)
  169             continue
               end if
  170       CONTINUE
  180    CONTINUE
      END IF
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DGBTRF
!
!
      RETURN
      END SUBROUTINE DGBTRF
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE DGBTF2(M,N,KL,KU,AB,LDAB,IPIV,INFO)
!
!
!
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
!
      INTEGER, DIMENSION(n) :: IPIV
!
      REAL(KIND = 8), DIMENSION(LDAB,n) :: AB
!
      PARAMETER (ONE = 1.0D0, ZERO = 0.0D0)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DGBTF2
!
!
      KV = KU + KL
!
      JU = 1
      DO 100 J = 1, MIN( M, N )
         KM = MIN(KL,M-J)
         dmax  = abs(AB(KV+1,J))
         idamx = 1
         do 10 i8 = 2,KM+2
            if(i8.gt.km+1) go to 11
            if(abs(AB(KV+i8,J)).le.dmax) go to 10
            idamx = i8
            dmax  = abs(AB(KV+i8,J))
   10    continue
   11    jp = idamx
!
         IPIV( J ) = JP+J-1
         IF(AB(KV+JP,J).NE.ZERO) THEN
!
            JU = MAX(JU,MIN(J+KU+JP-1,N))
            IF(JP.NE.1) then
               do 20 i6=1,(JU-J+1)*(LDAB-1),LDAB-1
                  ddttmm           = ab(KV+JP-1+i6,J)
                  ab(KV+JP-1+i6,J) = ab(KV+i6,J)
                  ab(KV+i6,J)      = ddttmm
   20          continue
            end if

            IF( KM.GT.0 ) THEN
               u4 = ONE/AB(KV+1,J)
               do 40 i4=1,km
                  ab(KV+1+i4,J)=u4*ab(KV+1+i4,J)
   40          continue
               IF( JU.GT.J )&
                 CALL DGER(KM,JU-J,-ONE,AB(KV+2,J),1,AB(KV,J+1),&
                           LDAB-1,AB(KV+1,J+1),LDAB-1)
            END IF
!
         ELSE
            IF( INFO.EQ.0 ) INFO = J
         END IF
  100 CONTINUE
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DGBTF2
!
!
      RETURN
      END SUBROUTINE DGBTF2
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE DGBTRS(N,KL,KU,AB,LDAB,IPIV,B,LDB)
!
!
!
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
!
      INTEGER, DIMENSION(n) :: IPIV
!
      REAL(KIND = 8), DIMENSION(LDAB,n) :: AB
      REAL(KIND = 8), DIMENSION(LDB,1)  :: B
!
      PARAMETER (ONE = 1.0D0)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DGBTRS
!
!
      KD = KU+KL+1
      DO 10 J = 1,N-1
         LM = MIN(KL,N-J)
         L = IPIV(J)
         IF(L.NE.J) CALL DSWAP(1,B(L,1),LDB,B(J,1),LDB)
         CALL DGER(LM,1,-ONE,AB(KD+1,J),1,B(J,1),LDB,B(J+1,1),LDB)
   10 CONTINUE
!
      CALL DTBSV(N,KL+KU,AB,LDAB,B(1,1))
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DGBTRS
!
!
      RETURN
      END SUBROUTINE DGBTRS
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE DLASWP(N,A,LDA,K1,K2,IPIV,INCX)
!
!
!
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      INTEGER IPIV(*)
      REAL(KIND = 8)   A(LDA,*)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DLASWP
!
!
      DO 10 I = K1, K2
         IP = IPIV(I)
         IF(IP.NE.I) CALL DSWAP(N,A(I,1),LDA,A(IP,1),LDA)
   10 CONTINUE
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DLASWP
!
!
      RETURN
      END SUBROUTINE DLASWP
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
      SUBROUTINE DGER(M,N,ALPHA,X,INCX,Y,INCY,A,LDA)
!
!
!
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      REAL(KIND = 8) A(LDA,*),X(*),Y(*)
      PARAMETER ( ZERO = 0.0D0 )
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DGER
!
!
      INFO = 0
!
      IF( INCY.GT.0 )THEN
         JY = 1
      ELSE
         JY = 1 - ( N - 1 )*INCY
      END IF
      IF( INCX.EQ.1 )THEN
         DO 20, J = 1, N
            IF( Y( JY ).NE.ZERO )THEN
               TEMP = ALPHA*Y( JY )
               DO 10, I = 1, M
                  A( I, J ) = A( I, J ) + X( I )*TEMP
   10          CONTINUE
            END IF
            JY = JY + INCY
   20    CONTINUE
      ELSE
         IF( INCX.GT.0 )THEN
            KX = 1
         ELSE
            KX = 1 - ( M - 1 )*INCX
         END IF
         DO 40, J = 1, N
            IF( Y( JY ).NE.ZERO )THEN
               TEMP = ALPHA*Y( JY )
               IX   = KX
               DO 30, I = 1, M
                  A( I, J ) = A( I, J ) + X( IX )*TEMP
                  IX        = IX        + INCX
   30          CONTINUE
            END IF
            JY = JY + INCY
   40    CONTINUE
      END IF
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DGER
!
!
      RETURN
      END SUBROUTINE DGER
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
      SUBROUTINE DTBSV(N,K,A,LDA,X)
!
!
!
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      REAL(KIND = 8) A(LDA,*),X(*)
      PARAMETER (ZERO = 0.0D0)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DTBSV
!
!
      INFO = 0
      KPLUS1 = K + 1
      DO 20, J = N, 1, -1
         IF(X(J).NE.ZERO)THEN
            L = KPLUS1 - J
            X(J) = X(J)/A(KPLUS1,J)
            TEMP = X(J)
            DO 10, I = J-1,MAX(1,J-K),-1
               X(I) = X(I) - TEMP*A(L+I,J)
   10       CONTINUE
         END IF
   20 CONTINUE
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DTBSV
!
!
      RETURN
      END SUBROUTINE DTBSV
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
      SUBROUTINE DGEMM(M,N,K,ALPHA,A,LDA,B,LDB,C,LDC)
!
!
!
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      REAL(KIND = 8) A(LDA,*), B(LDB,*),C(LDC,*)
      PARAMETER (ONE = 1.0D0, ZERO = 0.0D0)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DGEMM
!
!
 !     FORALL(j=1:N, l=1:K, i=1:M, B(l,j) /= ZERO)&
!     &       C(i,j) = C(i,j)-ALPHA*B(l,j)*A(i,l)
!
     DO 100 J=1,N
         DO 80 L=1,K
            IF(B(L,J).NE.ZERO)THEN
               TEMP = ALPHA*B(L,J)
               DO 60 I=1,M
                  C(I,J) = C(I,J)+TEMP*A(I,L)
   60          CONTINUE
            END IF
   80    CONTINUE
  100 CONTINUE
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DGEMM
!
!
      RETURN
      END SUBROUTINE DGEMM
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
      SUBROUTINE DTRSM(M,N,A,LDA,B,LDB)
!
!
!
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      REAL(KIND = 8) A(LDA,*),B(LDB,*)
      REAL(KIND = 8), PARAMETER :: ONE = 1.0D0, ZERO = 0.0D0
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DTRSM
!
!
 !     FORALL(j=1:N, k=1:M, i=1:M, i > k .AND. B(k,j) /= ZERO)&
!     &       B(i,j) = B(i,j)-B(k,j)*A(i,k)

      DO 100 J=1,N
         DO 90 K=1,M
            IF(B(K,J).NE.ZERO) THEN
               DO 80 I=K+1,M
                  B(I,J) = B(I,J)-B(K,J)*A(I,K)
   80          CONTINUE
            END IF
   90    CONTINUE
  100 CONTINUE
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DTRSM
!
!
      RETURN
      END SUBROUTINE DTRSM
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
      SUBROUTINE DSWAP(n,dx,incx,dy,incy)
!
      REAL(KIND = 8) dx(1),dy(1),dtemp
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSWAP
!
!
      if(n.le.0)return
      if(incx.eq.1.and.incy.eq.1) go to 20
!
!       code for unequal increments or equal increments not equal
!         to 1
      ix = 1
      iy = 1
      do 10 i = 1,n
        dtemp = dx(ix)
        dx(ix) = dy(iy)
        dy(iy) = dtemp
        ix = ix + incx
        iy = iy + incy
   10 continue
      return
!
   20 do 50 i = 1,n
        dtemp = dx(i)
        dx(i) = dy(i)
        dy(i) = dtemp
   50 continue
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DSWAP
!
!
      return
      end SUBROUTINE DSWAP
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
!
      END MODULE Matrix_Solvers
!
!
!
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!
!
!
      SUBROUTINE DSMV( N, X, Y, NELT, IA, JA, A, ISYM )
!
!
!
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      INTEGER N, NELT, IA(NELT), JA(NELT), ISYM
      REAL(KIND = 8) A(NELT), X(N), Y(N)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSMV
!
!
      Y(1:N) = 0.0d0     ! CAREFUL! Whole array operations
!
      DO icol = 1, N
         FORALL (j = JA(icol):JA(icol+1)-1) Y(IA(j)) = Y(IA(j))&
     &                                                +A(j)*X(icol)
      END DO
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DSMV
!
!
      RETURN
      END SUBROUTINE DSMV
!
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
      SUBROUTINE DSMTV( N, X, Y, NELT, IA, JA, A, ISYM )
!
!
!
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      INTEGER N, NELT, IA(NELT), JA(NELT), ISYM
      REAL(KIND = 8) X(N), Y(N), A(NELT)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSMTV
!
!
      Y(1:N) = 0.0d0     ! CAREFUL! Whole array operations
!
      DO irow = 1, N
!
         ibgn = JA(irow)
         iend = JA(irow+1)-1
!
         Y(irow) = Y(irow)+SUM(A(ibgn:iend)*X(IA(ibgn:iend)))
!
      END DO
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DSMTV
!
!
      RETURN
      END SUBROUTINE DSMTV
!
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
      SUBROUTINE DSLUI(N, B, X, NELT, IA, JA, A, ISYM, RWORK, IWORK)
!
! !DSLUI returns in R the solution of system LDU.R = V1 where LDU = M
!N, V1, R, NELT, IA, JA, A, ISYM, RWORK, IWORK
!Parallel DSLUI is compatible with SLAP column format of L in LDU
!This subroutine is directly parallelizable

      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      INTEGER N, NELT, IA(NELT), JA(NELT), ISYM, IWORK(*)
      REAL(KIND = 8) B(N), X(N), A(NELT), RWORK(*)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSLUI
!
!
      LOCIL  = IWORK(1)
      LOCJL  = IWORK(2)
      LOCIU  = IWORK(3)
      LOCJU  = IWORK(4)
      LOCL   = IWORK(5)
      LOCDIN = IWORK(6)
      LOCU   = IWORK(7)
!         
      X(1:N) = B(1:N)  ! CAREFUL! Whole array operations
!         
      DO 30 IROW = 2, N
         JBGN = IWORK(LOCIL+IROW-1)
         JEND = IWORK(LOCIL+IROW)-1
         IF( JBGN.LE.JEND ) THEN
            DO 20 J = JBGN, JEND
               X(IROW) = X(IROW)-RWORK(LOCL+J-1)*X(IWORK(LOCJL+J-1))
 20         CONTINUE
         ENDIF
 30   CONTINUE
         
      X(1:N) = X(1:N)*RWORK(LOCDIN:LOCDIN+N-1)  ! CAREFUL! Whole array operations
!         
      DO 60 ICOL = N, 2, -1
         JBGN = IWORK(LOCJU+ICOL-1)
         JEND = IWORK(LOCJU+ICOL)-1
         IF( JBGN.LE.JEND ) THEN
!          X(IWORK(LOCIU-1+JBGN:LOCIU-1+JEND)) 
!     &  = X(IWORK(LOCIU-1+JBGN:LOCIU-1+JEND))
!     &   -RWORK(LOCU-1+JBGN:LOCU-1+JEND)*X(ICOL)
            DO 50 J = JBGN, JEND
               JJUU    = IWORK(LOCIU+J-1)
               X(JJUU) = X(JJUU)-RWORK(LOCU+J-1)*X(ICOL) !Note: U is already divided by the diagonal of the same row.
 50         CONTINUE
         ENDIF
 60   CONTINUE
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DSLUI
!
!
      RETURN
      END SUBROUTINE DSLUI
!
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
      SUBROUTINE DSLUTI(N, B, X, NELT, IA, JA, A, ISYM, RWORK, IWORK)
!
!
!
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      INTEGER N, NELT, IA(NELT), JA(NELT), ISYM, IWORK(*)
      REAL(KIND = 8) B(N), X(N), A(N), RWORK(*)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSLUTI
!
!
      LOCIL  = IWORK(1)
      LOCJL  = IWORK(2)
      LOCIU  = IWORK(3)
      LOCJU  = IWORK(4)
      LOCL   = IWORK(5)
      LOCDIN = IWORK(6)
      LOCU   = IWORK(7)
!
!         
      X(1:N) = B(1:N)  ! CAREFUL! Whole array operations
!         
      DO 80 IROW = 2, N
         JBGN = IWORK(LOCJU+IROW-1)
         JEND = IWORK(LOCJU+IROW)-1
         IF( JBGN.LE.JEND ) THEN
            DO 70 J = JBGN, JEND
               X(IROW) = X(IROW)-RWORK(LOCU+J-1)*X(IWORK(LOCIU+J-1))
 70         CONTINUE
         ENDIF
 80   CONTINUE
!         
      X(1:N) = X(1:N)*RWORK(LOCDIN:LOCDIN+N-1)  ! CAREFUL! Whole array operations
!         
      DO 110 ICOL = N, 2, -1
         JBGN = IWORK(LOCIL+ICOL-1)
         JEND = IWORK(LOCIL+ICOL)-1
         IF( JBGN.LE.JEND ) THEN
            DO 100 J = JBGN, JEND
               JJUU    = IWORK(LOCJL+J-1)
               X(JJUU) = X(JJUU)-RWORK(LOCL+J-1)*X(ICOL)
 100        CONTINUE
         ENDIF
 110  CONTINUE
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DSLUTI
!
!
      RETURN
      END SUBROUTINE DSLUTI
!
!
!
!C***************************************************************************
!C@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!C***************************************************************************
