!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                                                                     >
!>        HRS_MPI.f95: Modules for handlining MPI Communictions        >
!>          Written by Aman Rusia on 3rd June 2016.                    >
!>                                                                     >
!>        Some subroutines are shifted to HRS_AllocateMem.f95          >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
MODULE MPI_param
		
		USE MPI
		
		
		INTEGER :: MPI_rank, MPI_np
		
		 !Parameters needed locally
		 INTEGER :: NELL, NELAL, NCONI,NCONE,NCONT,NUL,NTL    !Local subdomain sizes for NEL, NELA, and NCON
		 INTEGER :: NOGNL
		 
		!File handle numbers
		INTEGER :: IM_1    !IM_1 for INCON
		CHARACTER(LEN=500) :: FBUFF
		INTEGER(KIND=MPI_OFFSET_KIND),parameter :: ZeroOffset=0
		CHARACTER :: NLINE
		SAVE MPI_rank,MPI_np
		
		!save file
		INTEGER :: SAVE_linetype,SAVE_file,SAVE_filetype
		!Plot_Data1 file
		INTEGER :: PD1_linetype,PD1_file,PD1_filetype
		
END MODULE MPI_param
 
	 
MODULE MPI_SUBROUTINES


! ... Temporary arrays for reading and MPI communication 

INTEGER, dimension (100) :: TEMP_INT
CHARACTER, dimension (1000) :: TEMP_CHAR
REAL (kind=8), dimension (100) ::TEMP_REAL
LOGICAL, dimension (100) :: TEMP_LOGICAL

INTEGER :: CHARSIZE

contains
! ... Subroutines for MPI Communictions
SUBROUTINE MPI_INITIALIZE
	
	USE MPI_param
	IMPLICIT NONE
	INTEGER :: ierr
	
	
	CALL MPI_INIT(ierr)
	CALL MPI_COMM_RANK(MPI_COMM_WORLD,MPI_rank,ierr)
	CALL MPI_COMM_SIZE(MPI_COMM_WORLD,MPI_np,ierr)
	
	CHARSIZE = SIZEOF('A') 			!Character size in bytes needed for file writing
	NLINE = NEW_LINE('A')
	open(100,FILE='HRS_Input')
	
END SUBROUTINE MPI_INITIALIZE


SUBROUTINE MPI_BROADCAST(INT_ARRAY,CHAR_ARRAY,REAL_ARRAY,&
					int_size,char_size,real_size,root,&
					logical_array,logical_size)
!Subroutine for broadcasting INT_ARRAY,CHAR_ARRAY, REAL_ARRAY and LOGICAL_ARRAY to
!all the processes using MPI communication

	USE MPI_PARAM
	IMPLICIT NONE

	integer, optional, dimension(*) :: INT_ARRAY
	integer, optional :: int_size,char_size,root,real_size,logical_size
	integer :: ierr
	CHARACTER, optional, dimension(*) :: CHAR_ARRAY
	REAL (kind=8),optional, dimension(*) :: REAL_ARRAY
	LOGICAL,optional,dimension(*) :: LOGICAL_ARRAY
	INTEGER :: ICALL=0
	SAVE ICALL
	ICALL = ICALL+1
		
		if(present(INT_ARRAY)) &
			call MPI_BCAST(INT_ARRAY(1:int_size),int_size,MPI_INT,root,MPI_COMM_WORLD,ierr)
		if(present(CHAR_ARRAY)) &
			call MPI_BCAST(CHAR_ARRAY(1:char_size),char_size,MPI_CHARACTER,root,MPI_COMM_WORLD,ierr)
		if(present(REAL_ARRAY)) &
			call MPI_BCAST(REAL_ARRAY(1:real_size),real_size,MPI_REAL8,root,MPI_COMM_WORLD,ierr)
		if(present(logical_array)) &
			call MPI_BCAST(LOGICAL_ARRAY,logical_size,MPI_LOGICAL,root,MPI_COMM_WORLD,ierr)

END SUBROUTINE MPI_BROADCAST



	subroutine insert_ext(EXT,N,J)
	!Subroutine to add J to set of external elements to be sent to neighbors
	implicit none 

	integer :: Ext(:),N,J,I

	INTEGER :: ierr
	
	DO I=1,N
		if(EXT(I)==J) return
	END DO

	N=N+1
	Ext(N)=J
	end subroutine insert_ext

	
subroutine MPI_CREATE_FILE(filenm,IM)

	USE MPI_PARAM
	IMPLICIT NONE
	
	integer :: charsize,IM,ierr
	CHARACTER(LEN=*) :: filenm
	integer (kind=MPI_OFFSET_KIND) :: disp=0

	
	call MPI_FILE_OPEN(MPI_COMM_WORLD,filenm,MPI_MODE_RDWR+MPI_MODE_CREATE&
					,MPI_INFO_NULL,IM,IERR)
	if(IERR/=0 .and. MPI_RANK==0) THEN
		print *, "ERROR OPENING FILE"//filenm//"AT 22jS"
		STOP
	end if
	
	call MPI_FILE_SET_VIEW(IM,disp,MPI_CHARACTER,MPI_CHARACTER,'internal',MPI_INFO_NULL,ierr)
	if(IERR/=0 .and. MPI_RANK==0) THEN
		print *, "ERROR SETTING VIEW FOR FILE"//filenm//"AT 23jS"
		STOP
	end if
	
END SUBROUTINE MPI_CREATE_FILE

SUBROUTINE MPI_READ_FROM_FILE(IM,FILEBUFF,WCOUNT)
USE MPI_PARAM

IMPLICIT NONE
INTEGER :: IM,WCOUNT,IERR,ICOUNT,i
CHARACTER(LEN=*) :: FILEBUFF
INTEGER :: STATS(MPI_STATUS_SIZE)


CALL MPI_FILE_READ(IM_1,FILEBUFF,WCOUNT,MPI_CHARACTER,STATS,IERR)
CALL MPI_GET_COUNT(STATS,MPI_CHARACTER,ICOUNT,IERR)
FORALL(i=ICOUNT+1:WCOUNT) FILEBUFF(i:i) = ' '

END SUBROUTINE 

SUBROUTINE MPI_CREATE_FILE_LINEWISE(filenm,IM,procnum,linetype,NW,N,sizeperline,inidisp,filetype,ierr)
	USE MPI
	USE MPI_PARAM
	IMPLICIT NONE
	
	integer :: IM,ierr,sizeperline,NW,procnum(*),inidisp
	CHARACTER(LEN=*) :: filenm
	integer (kind=MPI_OFFSET_KIND) :: disp=0
	integer  :: etype,filetype,linetype,linesize,N,blocklen(N),linearray(N),charsize
	integer(kind=MPI_ADDRESS_KIND) :: dis(0:N)
	integer::i,j
	
	call MPI_FILE_OPEN(MPI_COMM_WORLD,filenm,MPI_MODE_WRONLY+MPI_MODE_CREATE&
					,MPI_INFO_NULL,IM,IERR)
	if(IERR/=0 .and. MPI_RANK==0) THEN
		print *, "ERROR OPENING FILE"//filenm//"AT 22jS"
		STOP
	end if
	!Creating structure type
	!First creating basic structure for the line
	call MPI_TYPE_CONTIGUOUS(sizeperline,MPI_CHARACTER,linetype,ierr)
	call MPI_TYPE_COMMIT(linetype,ierr)
	call MPI_TYPE_SIZE(MPI_CHARACTER,charsize,ierr)
	linesize=charsize*sizeperline
	!Looping over procnum to get the displacements array
	dis=0
	dis(0)=inidisp*charsize
	j=0
	do i=1,NW
		if(j>N) exit
		if(procnum(i)==MPI_RANK) then
			dis(j+1)=dis(j)+dis(j+1)
			j=j+1
			if(j==N) exit
		end if
		dis(j+1)=dis(j+1)+linesize
	end do
	blocklen=1
	linearray=linetype
	call MPI_TYPE_CREATE_STRUCT(N,blocklen,dis(1:),linearray,filetype,ierr)
	call MPI_TYPE_COMMIT(filetype,ierr)
	etype=MPI_CHARACTER
	call MPI_FILE_SET_VIEW(IM,disp,etype,filetype,'internal',MPI_INFO_NULL,ierr)
	if(IERR/=0 .and. MPI_RANK==0) THEN
		print *, "ERROR SETTING VIEW FOR FILE"//filenm//"AT 23jS"
		STOP
	end if
	
	
END SUBROUTINE MPI_CREATE_FILE_LINEWISE


END MODULE MPI_SUBROUTINES



	 