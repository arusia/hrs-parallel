!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                                                                     >
!>        TFx_Main.f95: Code unit including the main program           >
!>                      and all related routines                       >
!>                                                                     >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      PROGRAM HydrateResSim
! 
! ...... Modules to be used 
! 
         USE Basic_Param
         USE GenControl_Param
		 USE Variable_Arrays
		 USE Element_Arrays
		 USE MPI_Param
		 USE MPI_SUBROUTINES
!
!***********************************************************************
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*   HydrateResSim Main Program: Calls several lower-level routines    *     
!*                    which execute computations                       *     
!*                                                                     *
!*                   Version 1.0 - April 18, 2004                      *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Character allocatable arrays     
! -------
! 
      CHARACTER(LEN = 5), ALLOCATABLE, DIMENSION(:) :: VV
! 
! -------
! ... Double Precision Variables    
! -------
! 
      REAL(KIND = 8) :: ELT,ELT1,ELTC
! 
! -------
! ... Integer Variables    
! -------
! 
      INTEGER :: MC,i,ierG
			INTEGER :: ii,ierr !By RPS
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of HydrateResSim
!
!--------Initialize MPI Environment--------
	CALL MPI_INITIALIZE 
!---------
! 
! -------
! ... Open file VERS to record subroutine use
! -------
! 

	  OPEN(11,FILE='VERS')
            
! 
	  if(MPI_rank==0) then
      WRITE(11,6000)
	  end if

	
! 
! -------
! ... Reading t	he heading/title of the input data file  
! -------
! 
      READ (100,6002),  TITLE
	  if(MPI_rank==0) then
		PRINT 6003, TITLE
	  end if

! 
! 
!***********************************************************************
!*                                                                     *
!*                Allocate memory for most arrays                      *
!*                                                                     *
!***********************************************************************
! 
! 
	
!write(*,*)'i am here lin 97'
      CALL Allocate_MemGen

!write(*,*)'i am here lin 97'
!write(*,*) x
! 
! 
!***********************************************************************
!*                                                                     *
!*          Print headings and important general information           *
!*                                                                     *
!***********************************************************************
! 
! 
     ! CALL PRINT_Headin
! 
! 
!***********************************************************************
!*                                                                     *
!*               Check the directory for various files                 *
!*                                                                     *
!***********************************************************************
! 
! 
      CALL InOutFiles
!write(*,*)'i am here lin 118'
!write(*,*) x
! 
! 
!***********************************************************************
!*                                                                     *
!*                       Initialize the clock                          *
!*                                                                     *
!***********************************************************************
! 
! 
      CALL CPU_TimeF(TZERO)
! 
! 
!***********************************************************************
!*                                                                     *
!*             Allocate memory to temporary input arrays               *
!*                                                                     *
!***********************************************************************
! 
! 
      CALL Allocate_Input  
!write(*,*) x
   
! 
! 
!***********************************************************************
!*                                                                     *
!*             Read data from the main input data file                 *
!*                                                                     *
!***********************************************************************
! 
! 
!write(*,*) x(4),x(5),x(6),x(7),x(8)
      CALL READ_MainInputF(MC)
!write(*,*) x(4),x(5),x(6),x(7),x(8)
!write(*,*) elem(2)%P,elem(2)%T
!write(*,*) cell_v(2,0)%p_addD(1),cell_v(2,0)%p_addD(2)
	  
! 
! -------
! ... Determine the floating point accuracy of the processor 
! -------
! 
      CALL NumFloPo_Digits
!C                                                       
!C 
!C***********************************************************************
!C*                                                                     *
!C*      Read input data from the various files, both pre-existing      *
!C*                  and created by the INPUT routine                   *
!*                                                                     *
!***********************************************************************
! 
! 
!write(*,*) x
!write(*,*) x(4),x(5),x(6),x(7),x(8)
      CALL READ_Files(MC)                                               !Primary variables are initialized from this routine    

!***********************************************************************************************
!*                                                                                             *
!*             Opening files which are needed to be  written using MPI Input Output            *
!*                                                                                             *
!***********************************************************************************************
! 
! 		
	  CALL CREATE_WRITE_FILES_MPI
		
	!write(*,*) x(4),x(5),x(6),x(7),x(8)
!write(*,*) elem(2)%P,elem(2)%T
!write(*,*) cell_v(2,0)%p_addD(1),cell_v(2,0)%p_addD(2)
!write(*,*) x
!write(*,*) delx

!============================================== !By RPS for testing should be removed later
! FOR temp equation testing
		!do ii = 1,nel
			!nloc = (ii-1)*nk1
			!x(ii*nk1) =  sin(3.14159*x_coord(ii)/(1.12)) !(((x_coord(ii)*(1.0+2.e-3-x_coord(ii)))/((1.+2.e-3)**2))*0.6)+0.0
			!print*, x_coord(ii)
		!end do
!==============================================

!C 
!C 
!C>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!C>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!C>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!C*                                                                     *
!C*                      CONDUCT THE SIMULATION                         *
!C*                                                                     *
!C>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!C>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!C>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!C 
!C 
      IF_NoFloSimul:  IF(NoFloSimul .EQV. .FALSE.) THEN   ! Check if the simulation ...
!                                                         ! ... is to be bypassed
! ----------
! ... Print additional information
! ----------
!
      IF(MPI_RANK == 0) PRINT 6004, NEL,NELA,NCON,NOGN
      IF(PermModif /= 'NONE' .and. MPI_RANK == 0) PRINT 6005
!
! ----------
! ...... Select solver options 
! ----------
! 
         CALL SOLVR_Set
!write(*,*) delx
! 
! 
!***********************************************************************
!*                                                                     *
!*             Allocate memory to matrix solver arrays                 *
!*                                                                     *
!***********************************************************************
! 
! 

         CALL Allocate_MemMatrx 
!write(*,*) delx   

! 
! ----------
! ...... Print input data (when MOP(7) /= 0) 
! ----------
! 
         IF(MOP(7) /= 0) CALL PRINT_InputD

! 
! ----------
! ...... Deterine elapsed time for data input
! ----------
! 
         CALL CPU_TimeF(ELT1)
         ELT1=ELT1-TZERO
!
         IF(MPI_RANK==0) PRINT 6006, ELT1
! 
! ----------
! ...... Deallocate unnecessary memory (Temporary input arrays)  
! ----------
! 
         CALL DeAllocate_Input  
! 
! ----------
! ...... Print additional information 
! ----------
! 
         IF(MPI_RANK==0) CALL PRINT_MorInfo
! 
!   

!***********************************************************************
!*                                                                     *
!* Create MPI templates necessary for communication of boundary data   *
!*                                                                     *
!***********************************************************************
! 
! 

		call MPI_Template
		
!***********************************************************************
!*                                                                     *
!*     Perform simulation by solving the mass and heat equations       *
!*                                                                     *
!***********************************************************************
! 
! 
!write(*,*) delx
         CALL SIMULATION_Cycle
!write(*,*) delx
!
! <<<                      
! <<< End of the "IF_NoFloSimul" construct         
! <<<
!
      END IF IF_NoFloSimul
! 
! 
!***********************************************************************
!*                                                                     *
!*                   Print-out of version information                  *
!*                                                                     *
!***********************************************************************
! 
	! 
	if(MPI_RANK==0) THEN
		  IF_NoVersion: IF(NoVersion .EQV. .FALSE.) THEN  ! Check whether to bypass
	! 
	! ----------
	! ...... Print information 
	! ----------
	! 
			 PRINT 6008  
	!
	! ...... Close the VERS file
	!
			 ENDFILE 11
	!
	! ...... Go to the top of the VERS file
	!
			 REWIND 11
	! 
	! ----------
	! ...... Allocate memory to the VV local array 
	! ----------
	! 
			 ALLOCATE(VV(26), STAT=ierG)
	! 
	! ...... Check if properly allocated 
	! 
			 IF(ierG /= 0) THEN   ! Unsuccesful allocation
				PRINT 6101         
				STOP
			 END IF
	! 
	! ----------
	! ...... Initialization of the VV local array 
	! ----------
	! 
			 DO_Version: DO 
	! 
	! ......... Initialization of the VV local array 
	! 
				VV = '     '     !  CAREFUL - Whole array operation
	!
	! ......... Read version information
	!
				READ(UNIT = 11,FMT = 6010, IOSTAT = ierG) (VV(i),i=1,26)
	!
	! ......... For EOF/EOR, exit the loop
	!
				IF(ierG /= 0) EXIT DO_Version            
	!
	! ......... Write version information
	!
				PRINT 6010,(VV(i),I=1,26)
	!
			 END DO DO_Version 
	! 
	! ----------
	! ...... Allocate memory from the VV local array 
	! ----------
	! 
			 DEALLOCATE (VV, STAT=ierG)
	! 
	! ...... Check if properly deallocated 
	! 
			 IF(ierG /= 0) THEN   ! Unsuccesful deallocation
				PRINT 6102         
				STOP
			 END IF
	!
	!
		  END IF IF_NoVersion
	! 
	! 
	!***********************************************************************
	!*                                                                     *
	!*               Compute execution timing information                  *
	!*                                                                     *
	!***********************************************************************
	! 
	! 
		  PRINT 6015
	!
		  CALL CPU_TimeF(ELT)
	!
		  ELT  = ELT-TZERO
		  ELTC = ELT-ELT1
	!
		  PRINT 6020, ELT,ELT1,ELTC,T_mat_solv,T_jac_setup   !  Print execution time data
		  OPEN(9900, FILE = "sol_time")
		  write(9900,6020), ELT,ELT1,ELTC,T_mat_solv,T_jac_setup
		  CLOSE(9900)
	!
	END IF
!


!-------------------FINALIZE MPI--------------------
	CALL MPI_FINALIZE(IERR)
!C***********************************************************************
!C*                                                                     *
!C*!                       F  O  R  M  A  T  S                          *
!C*                                                                     *
!C***********************************************************************
!
!
 6000 FORMAT('HydrateResSim     1.0   29 September 2004',6X, &
     &       'MAIN PROGRAM')
!
 6002 FORMAT(A80)
 6003 FORMAT(/1X,131('=')//5X,'PROBLEM TITLE:  ',A80//)


 6004 FORMAT(/' MESH HAS',I7,' ELEMENTS (',I7,' ACTIVE) AND',I7, &
     &        ' CONNECTIONS (INTERFACES)', &
     &        ' BETWEEN THEM'/' GENER HAS',I6,' SINKS/SOURCES')
 6005 format(' BLOCK-BY-BLOCK PERMEABILITY MODIFICATION IS IN EFFECT')
!
 6006 FORMAT(//,' END OF "HydrateResSim" INPUT JOB: Elapsed Time = ', &
     &          1pe15.8,' seconds'/)
!
 6008 FORMAT(/, &
     &       132('*'),/,'*',130X,'*',/, &
     &       '*',50X,'SUMMARY OF PROGRAM UNITS', &
     &       ' USED',51X,'*',/,'*',130X,'*',/,132('*'),//, &
     &       '  UNIT          VERSION     DATE               COMMENTS', &
     &       /,132('_'),/)
!
 6010 FORMAT(26A5)
 6015 FORMAT(//,132('*'))
 6020 FORMAT(//, ' END OF "HydrateResSim" SIMULATION RUN:', &
     &       T35,' Elapsed Time             = ',1pe15.8,' sec',/, &
     &       T35,' Data Input Time          = ',1pe15.8,' sec',/, &
     &       T35,' Calculation Time         = ',1pe15.8,' sec',/, &
     &       T40,' Matrix Solving Time = ',1pe15.8,' sec',/,&
	 &       T40,' Jacobian Setup Net Time = ',1pe15.8,' sec')
!
 6101 FORMAT(//,20('ERROR-'),//, &
     &       T2,'Memory allocation of array VV in the main program ', &
     &          '"HydrateResSim" was unsuccessful',//, &
     &       T2,'!!!!!!!    THE EXECUTION WAS STOPPED    !!!!!!!', &
     &       //,20('ERROR-'))
 6102 FORMAT(//,20('ERROR-'),//, &
     &       T2,'Memory deallocation of array VV in "HydrateResSim" ', &
     &          'was unsuccessful',//, &
     &       T2,'!!!!!!!    THE EXECUTION WAS STOPPED    !!!!!!!', &
     &       //,20('ERROR-'))
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of HydrateResSim
!
!
      STOP
      END PROGRAM HydrateResSim
!
!
!C
!C***********************************************************************
!C@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!C***********************************************************************
!
!
!C
      SUBROUTINE PRINT_Headin
! 
! ...... Modules to be used 
! 
         USE EOS_Parameters
! 
         USE Basic_Param
         USE GenControl_Param
		 USE MPI_PARAM
!C
!C***********************************************************************
!C***********************************************************************
!C*                                                                     *
!C*!         ROUTINE FOR PRINTING HEADINGS IN THE OUTPUT FILE           *
!C*!                   Version 1.0 - April 23, 2003                     *     
!C*                                                                     *
!C***********************************************************************
!C***********************************************************************
!C
      IMPLICIT NONE
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of PRINT_Headin
!
!
      if(MPI_RANK==0) WRITE(11,6000)
!
! ... Format
!
 6000 FORMAT(/,'PRINT_Headin      1.0   23 April     2004',6X, &
     &         'Routine for printing the headings in the ', &
     &         'output file')
! 
! -------
! ... Print headers         
! -------
! 
      if(MPI_RANK==0) WRITE(6, 6001) ' '
!
! ... Format
!
 6001 FORMAT(A1,/, &
     &       T14,'@   @  @   @  @@@   @@@    @@   @@@@@  @@@@     ', &
     &'@@@   @@@@   @@@      @@@  @  @   @     @@@   @  @  @   @',/, &
     &       T14,'@   @  @   @  @  @  @  @  @  @    @    @        ', &
     &'@  @  @     @        @     @  @@ @@     @  @  @  @  @@  @',/, &
     &       T14,'@@@@@   @ @   @  @  @@@   @@@@    @    @@@@     ', &
     &'@@@   @@@@   @@       @@   @  @ @ @     @@@   @  @  @ @ @',/, &
     &       T14,'@   @    @    @  @  @ @   @  @    @    @        ', &
     &'@ @   @        @        @  @  @   @     @ @   @  @  @  @@',/, &
     &       T14,'@   @    @    @@@   @  @  @  @    @    @@@@.....', &
     &'@  @  @@@@  @@@ .....@@@   @  @   @     @  @   @@   @   @',//)
!
!
!
      if(MPI_RANK==0) WRITE(6, 6002)
!
! ... Format
!
 6002 FORMAT(19X,'"HydrateResSim" IS A PROGRAM FOR ', &
     &           ' MULTIPHASE MULTICOMPONENT',  &
     &           ' FLOW IN PERMEABLE MEDIA, INCLUDING HEAT FLOW.',/, &
     &       23X,'IT IS BASED ON AN EARLY VERSION OF THE TOUGH-Fx ', &
     &           '[Moridis et al., 2005] FAMILY OF CODES DEVELOPED',/,  &
     &       49X,'AT THE LAWRENCE BERKELEY NATIONAL ', &
     &           'LABORATORY ')
! 
      if(MPI_RANK==0) WRITE(6, 6003)
 6003 FORMAT(/,132('*'),/)
!
!
!
      if(MPI_RANK==0) WRITE(6, 6004)
!
! ... Format
!
 6004 FORMAT(17X,'"HydrateResSim" v1.0 IS WRITTEN IN FORTRAN 95/2003 ', &
     &           'USING AN OBJECT_ORIENTED PROGRAMMING STRUCTURE,',/, &
     &       24X,'AND WAS DEVELOPED BY GEORGE J. MORIDIS ', &
     &           'FOR THE NATIONAL ENERGY TECHNOLOGY LABORATORY',/) 
!
!
!
      if(MPI_RANK==0) WRITE(6, 6005)
!
! ... Format
!
 6005 FORMAT(/24X,85('*'),/,24X,20('*'),45X,20('*'),/, &
     &        24X,20('*'), &
     &        5X,'HydrateResSim V1.0 (September 2004)',5X,20('*')/, &
     &        24X,20('*'),45X,20('*'),/,24X,85('*'),/)
!
      if(MPI_RANK==0) WRITE(6, 6006)
!
! ... Format
!
 6006 format(/,' "HydrateResSim" Copyright (c) 2004, The Regents', &
     &  ' of the University of California (through Lawrence', &
     &  ' Berkeley National Laboratory).',/, &
     &  ' All rights reserved.')
!
!
!
      if(MPI_RANK==0) WRITE(6, 6007)
!
! ... Format
!
 6007 format(//, &
     &' NOTICE: Redistribution and use in source and binary forms,', &
     &' with or without modification, are permitted provided that ',/, &
     &' the following conditions are met:',//, &
     &' (1) Redistributions of source code must retain the above',  &
     &' copyright notice, this list of conditions and the ',/, &
     &'     following disclaimer.',/, &
     &' (2) Redistributions in binary form must reproduce the above', &
     &' copyright notice, this list of conditions and the following',/, &
     &'     disclaimer in the documentation and/or other materials', &
     &' provided with the distribution.',/, &
     &' (3) Neither the name of HydrateResSim, the National Energy', &
     &' Technology Laboratory, the U.S. Dept. of Energy nor the', &
     &' names',/, &
     &'     of its contributors may be used to endorse or promote', &
     &' products derived from this software without specific',/, &
     &'     prior written permission.',//, &
     &' THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND', &
     &' CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED', &
     &' WARRANTIES, INCLUDING',/,  &
     &' BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY', &
     &' AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.', &
     &' IN NO EVENT',/, &
     &' SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR', &
     &' ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,', &
     &' OR CONSEQUENTIAL',/, &
     &' DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF', &
     &' SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;', &
     &' OR BUSINESS',/, &
     &' INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,', &
     &' WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING',/, &
     &' NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE', &
     &' OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY', &
     &' OF SUCH DAMAGE.',/)
! 
      if(MPI_RANK==0) WRITE(6, 6503)
 6503 FORMAT(/,132('*'),/,132('*'),/,132('*'),/)
!
!
!
      if(MPI_RANK==0) WRITE(6, 6008) MaxNum_Elem,MaxNum_Conx,MaxNum_Equat,MaxNum_MComp, &
     &               MaxNum_Phase,MaxNum_SS,M_BinDif,M_Add
!
! ... Format
!
 6008 FORMAT(//,' PARAMETERS FOR DYNAMIC DIMENSIONING OF MAJOR', &
     &          ' ARRAYS (MAIN PROGRAM) ARE AS FOLLOWS',//, &
     &          T10,'MaxNum_Elem  =',I8,/,T10,'MaxNum_Conx  =',I8,/, &
     &          T10,'MaxNum_Equat =',I8,/,T10,'MaxNum_MComP =',I8,/, &
     &          T10,'MaxNum_Phase =',I8,/,T10,'MaxNum_SS    =',I8,/, &
     &          T10,'M_BinDif     =',I8,/,T10,'M_Add        =',I8,//, &
     &      ' ',131('='))
!
!
!
      if(MPI_RANK==0) WRITE(6, 6009) MaxNum_Elem,MaxNum_Conx,MaxNum_PVar, &
     &               MaxNum_SS,MaxNum_Media
!
! ... Format
!
 6009 FORMAT(/,' MAXIMUM NUMBER OF VOLUME ELEMENTS (GRID BLOCKS):',12X, &
     &         'MaxNum_Elem  =',I9,/, &
     &         ' MAXIMUM NUMBER OF CONNECTIONS (INTERFACES):',17X, &
     &         'MaxNum_Conx  =',I9,/, &
     &         ' MAXIMUM LENGTH OF PRIMARY VARIABLE ARRAYS:',18X, &
     &         'MaxNum_PVar  =',I9,/, &
     &         ' MAXIMUM NUMBER OF GENERATION ITEMS (SINKS/SOURCES):', &
     &      9X,'MaxNum_SS    =',I9,/, &
     &         ' MAXIMUM NUMBER OF POROUS/FRACTURED MEDIA:          ', &
     &      9X,'MaxNum_Media =',I9,//,' ',131('='))
!
!
!
      if(MPI_RANK==0) WRITE(6, 6012)
!
! ... Format
!
 6012 format(/,' Array dimensioning is dynamic. ', &
     &         ' The use of the direct solver ', &
     &         ' will reduce the size of the maximum solvable problem')
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of PRINT_Headin
!
!
      RETURN
!
      END SUBROUTINE PRINT_Headin
!
!
!C
!C***********************************************************************
!C@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!C***********************************************************************
!
!
!C
      SUBROUTINE PRINT_MorInfo
! 
! ...... Modules to be used 
! 
         USE Basic_Param
         USE GenControl_Param
!C
!C***********************************************************************
!C***********************************************************************
!C*                                                                     *
!C*!      ROUTINE FOR PRINTING ADDITIONAL INFO IN THE OUTPUT FILE       *
!C*!                   Version 1.0 - April 23, 2003                     *     
!C*                                                                     *
!C***********************************************************************
!C***********************************************************************
!!C
      IMPLICIT NONE
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of PRINT_MorInfo
!
!
      WRITE(11,6000)
!
! ... Format
!
 6000 FORMAT(/,'PRINT_MorInfo     1.0   23 April     2004',6X, &
     &         'Routine for printing additional information in the ', &
     &         'output file')
! 
! -------
! ... Print headers         
! -------
! 
      PRINT 6002 
!
! ... Format
!
 6002 FORMAT(' ',/' ',131('*'))
!
!
!
      PRINT 6003
!
! ... Format
!
 6003 FORMAT(' *          ARRAY *MOP*', &
     &       ' ALLOWS TO GENERATE MORE PRINTOUT IN VARIOUS', &
     &       ' SUBROUTINES, AND TO MAKE SOME CALCULATIONAL CHOICES', &
     &       '            *')
!
!
!
      PRINT 6004
!
! ... Format
!
 6004 FORMAT(' ',131('*')/)
!
!
!
      PRINT 6005, MOP(1)
!
! ... Format
!
 6005 FORMAT(' ','  MOP(1)  =',I2, &
     &       ' *** ALLOWS TO GENERATE A SHORT PRINTOUT FOR', &
     &       ' EACH NEWTON-RAPHSON ITERATION',/, &
     &       '           = 0, 1, OR 2: GENERATE 0, 1, OR 2 LINES', &
     &       ' OF PRINTOUT',//,12X, &
     &       'MORE PRINTOUT IS GENERATED FOR MOP(I) > 0 IN THE ', &
     &       'FOLLOWING SUBROUTINES (THE LARGER MOP IS, THE MORE', &
     &       ' WILL BE PRINTED).'/)
!
!
!
      PRINT 6006, MOP(2),MOP(3),MOP(4),MOP(5),MOP(6),mop(8)
!
! ... Format
!
 6006 FORMAT('   MOP(2)  =',I2,' *** CYCIT   ', &
     &       'MOP(3) =',I2,' *** MULTI   ', &
     &       'MOP(4) =',I2,' *** QU   ', &
     &       'MOP(5) =',I2,' *** EOS   ', &
     &       'MOP(6) =',I2,' *** LINEQ   ', &
     &       'MOP(8)  =',I2,' *** DISF (T2DM ONLY)',/)
!
!
!
      PRINT 6008, MOP(7)
!
! ... Format
!
 6008 FORMAT('   MOP(7)  =',I2,' *** IF UNEQUAL ZERO,',  &
     &       ' WILL GENERATE A PRINTOUT OF INPUT DATA',/)
!
!
!
      PRINT 6009, MOP(9),MOP(10)
!
! ... Format
!
 6009 FORMAT(' ',8X,'   CALCULATIONAL CHOICES OFFERED BY MOP', &
     &              ' ARE AS FOLLOWS:',//, &
     &              '   MOP(9)  =',I2,' *** CHOOSES FLUID COMPOSITION', &
     &              ' ON WITHDRAWAL (PRODUCTION).',/, &
     &              '           = 0: ACCORDING TO RELATIVE ', &
     &              'MOBILITIES',/, &
     &              '           = 1: ACCORDING TO COMPOSITION IN ', &
     &              'PRODUCING ELEMENT',//, &
     &              '   MOP(10) =',I2,' *** CHOOSES INTERPOLATION ', &
     &              'FORMULA FOR DEPENDENCE OF THERMAL CONDUCTIVITY ', &
     &              'ON LIQUID SATURATION (SL).',/, &
     &             '           = 0: K = KDRY + SQRT(SL)*(KWET-KDRY)',/, &
     &             '           = 1: K = KDRY + SL*(KWET-KDRY)',/, &
     &             '           = 2: K COMPUTED FROM COMPONENT THERMAL ', &
     &             'CONDUCTIVITIES (GAS CONTRIBUTION IS NEGLECTED)',/, &
     &             '           = 3: K COMPUTED FROM ALL COMPONENT ', &
     &             'THERMAL CONDUCTIVITIES (GAS CONTRIBUTION ', &
     &             'INCLUDED)',/)
!
!
!
      PRINT 6010, MOP(11)
!
! ... Format
!
 6010 FORMAT('   MOP(11) =',I2,' *** CHOOSES EVALUATION OF MOBILITY', &
     &       ' AND ABSOLUTE PERMEABILITY AT INTERFACES ' ,/ &
     &       '           = 0: MOBILITIES ARE UPSTREAM WEIGHTED', &
     &       ' WITH WUP (DEFAULT IS WUP = 1.0). PERMEABILITY IS ', &
     &       'UPSTREAM WEIGHTED'/ &
     &       '           = 1: MOBILITIES ARE AVERAGED BETWEEN', &
     &       ' ADJACENT ELEMENTS. PERMEABILITY IS UPSTREAM WEIGHTED',/, &
     &       '           = 2: MOBILITIES ARE UPSTREAM ', &
     &       'WEIGHTED WITH WUP', &
     &       ' (DEFAULT IS WUP = 1.0). PERMEABILITY IS ', &
     &       'HARMONIC WEIGHTED,',/, &
     &       '           = 3: MOBILITIES ARE AVERAGED BETWEEN', &
     &       ' ADJACENT ELEMENTS. PERMEABILITY IS HARMONIC WEIGHTED',/, &
     &       '           = 4: MOBILITY * PERMEABILITY PRODUCT IS', &
     &       ' HARMONIC WEIGHTED',/) 
! 
!
!
      PRINT 6011, MOP(12),mop(13)
!
! ... Format
!
 6011 FORMAT('   MOP(12) =',I2,' *** CHOOSES PROCEDURE FOR ',&
     &       'INTERPOLATING GENERATION RATES FROM A TIME ',/&	
     &'           = 0: TRIPLE LINEAR INTERPOLATION.'/&
     &'           = 1: "STEP FUNCTION" OPTION.'//&
     &'   MOP(13) =',I2,' *** T2DM ONLY.  SPECIFIES ASSIGNMENT OF',&
     &' COMPONENTS OF BOUNDARY VELOCITY AND',&
     &' CONCENTRATION GRADIENT VECTORS.'/&
     &'                   AFFECTS ONLY THE INTERPOLATED ',&
     &' COMPONENTS.  DIRECT COMPONENTS ARE USED WHERE AVAILABLE.'/&
     &'           = 0: VELOCITY AND GRADIENT AT BOUNDARY ARE ZERO.'/&
     &'           = 1: VELOCITY IS ZERO; GRADIENT AT BOUNDARY IS',&
     &' NEAREST-NEIGHBOR.',/,&
     &'           = 2: VELOCITY IS NEAREST NEIGHBOR; GRADIENT AT',&
     &' BOUNDARY IS ZERO.',/,&
     &'           = 3: VELOCITY AND GRADIENT AT BOUNDARY',&
     &' ARE NEAREST-NEIGHBOR.',//)
!
!
!
      PRINT 6012, MOP(14),MOP(15)
!
! ... Format
!
 6012 FORMAT("   MOP(14) =",I2," *** SPECIFIES THE HANDLING OF GAS",&
     &       " SOLUBILITY",/,&
     &       "           = 0: USE HENRY'S CONSTANTS (T-indepenent)",/,&
     &       "           = 1: USE APPROPRIATE EQUATIONS OF HENRY'S ",&
     &       "CONSTANTS (T-depenent)",/,&
     &       "           > 1: USE FUGACITIES (T- and P-depenent)",//,&
     &       "   MOP(15) =",I2," *** ALLOWS TO SELECT A",&
     &       " SEMI-ANALYTICAL HEAT EXCHANGE CALCULATION",&
     &       " WITH CONFINING BEDS",/,&
     &       "           = 0: NO SEMI-ANALYTICAL HEAT EXCHANGE",/,&
     &       "           > 0: SEMI-ANALYTICAL HEAT EXCHANGE ENGAGED ",&
     &       "(WHEN A SPECIAL SUBROUTINE MODULE 'QLOSS' IS PRESENT)",/)
!
!
!
      PRINT 6014, MOP(16),MOP(17),MOP(18)
!
! ... Format
!
 6014 FORMAT(' ',&
     &       '  MOP(16) =',I2,' *** PERMITS TO CHOOSE TIME ',&
     &       'STEP SELECTION OPTION',/,&
     &       '           = 0: USE TIME STEPS EXPLICITLY ',&
     &       'PROVIDED AS INPUT',/,&
     &       '           > 0: INCREASE TIME STEP BY AT LEAST ',&
     &       'A FACTOR 2, IF CONVERGENCE OCCURS IN < ',&
     &       'MOP(16) ITERATIONS',//,&
     &       '   MOP(17) =',I2,' *** SPECIFIES THE HANDLING ',&
     &       'OF BINARY GAS DIFFUSIVITIES',/,&
     &       '           = 0: COMPUTE USING THE METHOD OF ',&
     &       'Fuller et al. [1969]',/,&
     &       '           > 7: ADJUST FOR HIGH PRESSURES USING ',&
     &       'THE METHOD OF Riazi and Whitson [1993]',//,&
     &       '   MOP(18) =',I2,' *** ALLOWS TO SELECT HANDLING ',&
     &       'OF INTERFACE DENSITY',/,&
     &       '           = 0: PERFORM UPSTREAM WEIGHTING FOR ',&
     &       'INTERFACE DENSITY',/,&
     &       '           > 0: COMPUTE INTERFACE DENSITY AS ',&
     &       'AVERAGE OF THE TWO GRID BLOCK DENSITIES',/,&
     &       '                HOWEVER, WHEN ONE OF THE TWO PHASE ',&
     &       'SATURATIONS IS ZERO, DO UPSTREAM WEIGHTING',/)
!
!
!
      print 6016, mop(19),mop(20),mop(21)
!
! ... Format
!
 6016 format('   MOP(19) =',I2,' *** is used in some EOS-modules for',&
     &' selecting different options'//&
     &'   MOP(20) =',I2,' *** is used in some EOS-modules for',&
     &' selecting different options',//,&
     &'   MOP(21) =',I2,' *** PERMITS TO SELECT LINEAR',&
     &' EQUATION SOLVER'/&
     &'           = 0: DEFAULTS TO MOP(21) = 3'/&
     &'           = 1: SUBROUTINE LUBAND: Direct solver using',                     &
     &' LU decomposition',/,&
     &'           = 2: SUBROUTINE DSLUBC: BI-CONJUGATE GRADIENT',&
     &' SOLVER; PRECONDITIONER: INCOMPLETE LU FACTORIZATION'/&
     &'           = 3: SUBROUTINE DSLUCS: BI-CONJUGATE GRADIENT',&
     &' SOLVER - LANCZOS TYPE; PRECONDITIONER: INCOMPLETE LU',&
     &' FACTORIZATION',/,&
     &'           = 4: SUBROUTINE DSLUGM: GENERALIZED MINIMUM',&
     &' RESIDUAL CONJUGATE GRADIENTS; PRECONDITIONER: INCOMPLETE LU',&
     &' FACTORIZATION',/,&
     &'           = 5: SUBROUTINE DLUSTB: Stabilized bi-conjugate',&
     &' gradient solver; PRECONDITIONER: INCOMPLETE LU',&
     &' FACTORIZATION',//)
!
!
!
      PRINT 6018, MOP(22),MOP(23)
!
! ... Format
!
 6018 FORMAT('   MOP(22) =',I2,' *** T2DM ONLY.  SPECIFIES METHOD OF',&
     &' SUMMATION OF DUPLICATE ELEMENTS IN THE JACOBIAN. ***',/,&
     &'           = 0: USE SUMDUP2.',/,&
     &'           = 1: USE SUMDUP.',/,&
     &'           = 2: IF MATSLV = 1, USE MA28 INTERNAL SUMMATION.'//,&
     &'   MOP(23) =',I2,' *** T2DM ONLY.  HANDLES EFFECTS OF',&
     &' NON-CONNECTED NEIGHBOR GRID BLOCKS ON DISPERSIVE FLUX IN ',&
     &' DISF. ***',/,&
     &'           = 0: INCLUDE INFLUENCE OF NEIGHBOR GRID BLOCKS.',&
     &'  (MORE ACCURATE JACOBIAN, SLOWER LINEAR EQUATION SOLUTION).'/,&
     &'           = 1: NEGLECT INFLUENCE OF NEIGHBOR GRID BLOCKS.',&
     &'  (LESS ACCURATE JACOBIAN, FASTER LINEAR EQUATION SOLUTION).'//)
!
!
!
      print 6020, mop(24)
!
! ... Format
!
 6020 format('   MOP(24) =',I2,' *** PERMITS TO SELECT HANDLING OF',&
     &' MULTIPHASE DIFFUSIVE FLUXES AT INTERFACES'/&
     &'           = 0: HARMONIC WEIGHTING OF FULLY-COUPLED EFFECTIVE ',&
     &'MULTIPHASE DIFFUSIVITY'/&
     &'           = 1: SEPARATE HARMONIC WEIGHTING OF GAS AND LIQUID ',&
     &'PHASE DIFFUSIVITIES.'//&
     &' ',131('*'))
! 
!*********************************************************************
!*                                                                   *
!*  GENERATE SOME PRINTOUT CONCERNING THE EQUATION-OF-STATE PACKAGE  *
!*                                                                   *
!*********************************************************************
! 
      WRITE(6,6022) 
 6022 FORMAT(' ',/' ',131('*')/' * "HydrateResSim": EQUATION',    &
     &            ' OF STATE FOR WATER, CH4 AND SALT MIXTURES',   &
     &            ' DISTRIBUTED IN FOUR PHASES (AQUEOUS, GAS, ',&
     &            'HYDRATE AND ICE',2X,'*'/                                &
     &            ' ',131('*'))
!
!
!
      WRITE(6,6024)
 6024 FORMAT(' *',129X,'*'/' *',11X,                                    &
     &       ' "EOS_Equilibrium":',99X,'*'/,' *',18X,&
     &       ' Equation of state for CH4 + H2O + Water distributed',&
     &       ' among 4 phases: Gas, Aqueous, Hydrate,',20X,'*'/' *',18X,&
     &       ' Ice (Equilibrium hydrate reaction).',75X,'*'/' *',11X,&
     &       ' "EOS_Kinetic":',103X,'*'/,' *',18X,&
     &       ' Equation of state for CH4 + H2O + Water distributed',&
     &       ' among 4 phases: Gas, Aqueous, Hydrate,',20X,'*'/' *',18X,&
     &       ' Ice (Kinetic hydrate reaction).',79X,'*'/&
     &       ' *',129X,'*'/' ',131('*')/,&
     &       ' *',129X,'*'/,' *',11X,&
     &       ' "EOS_Equilibrium" + "EOS_Kinetic" were developed',&
     &       ' by G. Moridis at Lawrence Berkeley National',&
     &       ' Laboratory.',13X,'*'/&
     &       ' *',129X,'*'/,' *',11X,&
     &       ' References: (1) G. Moridis, M. Kowalsky and K. Pruess, ',&
     &       '"HydrateResSim" v1.0: Users Manual',28X,'*',/,' *',27X,&
     &       ' Lawrence Berkeley National Laboratory, Report',&
     &       ' LBNL-XXXXX, Berkeley, CA 2005',26X,'*'/ &
     &       ' *',129X,'*'/' ',131('*'))
!
!
!
      WRITE(6,6026) NK,NEQ,NPH,M_BinDif
 6026 FORMAT(/,' OPTIONS SELECTED ARE: (NK,NEQ,NPH,M_BinDif) = (',  &        
     &       3(I1,','),I1,')'/)                                                
!
!
!
      WRITE(6,6028) NK,NEQ,NPH,M_BinDif
 6028 FORMAT(25X,'NK        = ',I2,'   - NUMBER OF FLUID COMPONENTS',/,         &
     &       25X,'NEQ       = ',I2,&
     &           '   - NUMBER OF EQUATIONS PER GRID BLOCK'/        &
     &       25X,'NPH       = ',I2,'   - NUMBER OF PHASES THAT ',      &
     &           'CAN BE PRESENT',/,      &
     &       25X,'M_BinDif  = ',I2,&
     &           '   - FLAG INDICATING (when /= 0) CONSIDERATION OF ',&
     &           'MULTICOMPONENT BINARY DIFFUSION')                                    
!
!
!
      WRITE(6,6030)
 6030 FORMAT(/,' AVAILABLE OPTIONS ARE: (NK,NEQ,NPH,M_BinDif) = ',&
     &         '(2,3,4,M_BinDif)- H2O, CH4, HYDRATE, NON_ISOTHERMAL,', &
     &         ' EQUILIBRIUM REACTION',/,&
     &     48X,'(3,4,4,M_BinDif)- H2O, CH4, HYDRATE, INHIBITOR,',&
     &         ' NON-ISOTHERMAL, EQUILIBRIUM REACTION',/,&
     &     48X,'(3,4,4,M_BinDif)- H2O, CH4, HYDRATE,',&
     &         ' NON-ISOTHERMAL, KINETIC REACTION',/,&
     &     48X,'(4,5,4,M_BinDif)- H2O, CH4, HYDRATE, INHIBITOR,',&
     &         ' NON-ISOTHERMAL, KINETIC REACTION')
!
!
!
      WRITE(6,6032)
 6032 FORMAT(/,' THE POSSIBLE PRIMARY VARIABLES ARE:',/,/,&
     &         23X,'     P:   PRESSURE (Pa)',/&
     &         23X,'     T:   TEMPERATURE (C)',/&
     &         23X,'  X_mA:   MASS FRACTION OF CH4 DISSOLVED IN THE',&
     &         ' AQUEOUS PHASE',/,&
     &         23X,'    Sw:   WATER SATURATION',/,&
     &         23X,'    Sg:   GAS SATURATION',/,&
     &         23X,'    Sh:   HYDRATE SATURATION',/,&
     &         23X,'    Si:   ICE SATURATION',/,&
     &         23X,'  X_iA:   MASS FRACTION OF INHIBITOR DISSOLVED',&
     &         ' IN THE AQUEOUS PHASE',/)
!
!
!
      WRITE(6,6033)
 6033 FORMAT(/,132('='))                                             
!
!
!
      WRITE(6,6034)
 6034 FORMAT(/,' FOR (NK,NEQ,NPH) = (2,3,4) - EQUILIBRIUM REACTION',//,&
     &' ******************************  ******************************',&
     &'   *********************************************************',/,&
     &' *         COMPONENTS         *  *           PHASES           *',&
     &'   *   FLUID PHASE CONDITIONS       PRIMARY VARIABLES      *',/,&
     &' ******************************  ******************************',&
     &'   *********************************************************',/,&
     &' *                            *  *                            *',&
     &'   *                              X1    X2   X3   X4  KEY  *',/,&
     &' *       # 1  -  H2O          *  *       # 1  -  GAS          *',&
     &'   *                             ------------------------- *',/,&
     &' *                            *  *                            *',&
     &'   *                                                       *',/,&
     &' *       # 2  -  CH4          *  *       # 2  -  AQUEOUS      *',&
     &'   *   SINGLE-PHASE AQUEOUS       P,  X_mA,  T,   -,  Aqu  *',/,&
     &' *                            *  *                            *', &
     &'   *                                                       *',/,&
     &' *       # 3  -  HEAT         *  *       # 3  -  HYDRATE      *',&
     &'   *   SINGLE-PHASE GAS           Pg, X_mA,  T,   -,  Gas  *',/,&
     &' *                            *  *                            *',&
     &'   *                                                       *',/,&
     &' *                            *  *       # 4  -  ICE          *',&
     &'   *   2-PHASE (AQ.+GAS)          Pg,   Sw,  T,   -,  AqG  *',/,&
     &' *                            *  *                            *',&
     &'   *                                                       *',/,&
     &' ******************************  ******************************',&
     &'   *   2-PHASE (AQ.+HYDRATE)      P,    Sw,  T,   -,  AqH  *',/,&
     &'                                                               ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *   2-PHASE (ICE+GAS)          Pg,   Si,  T,   -,  IcG  *',/,&
     &'                                                               ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *   2-PHASE (ICE+HYDRATE)      P,    Si,  T,   -,  IcH  *',/,&
     &'                                                               ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *   3-PHASE (AQ.+GAS+HYDRATE)  Sg,   Sw,  T,   -,  AGH  *',/,&
     &'                                                               ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *   3-PHASE (ICE+GAS+HYDRATE)  Sg,   Si,  T,   -,  IGH  *',/,&
     &'                                                               ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *   3-PHASE (GAS+AQ.+ICE)      Pg,   Sw,  Sg,  -,  GAI  *',/,&
     &'                                                               ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *   3-PHASE (HYDRATE+Aq.+ICE)  Sh,   Sw,  T,   -,  HAI  *',/,&
     &'                                                               ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *   QUADRUPLE POINT            Pg,   Sw,  T,   -,  QuP  *',/,&
     &'                                                               ',&
     &'   *   (AQ.+GAS+HYDRATE+ICE)                               *',/,&
     &'                                                               ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *********************************************************')

!
!
!
      WRITE(6,6040)
 6040 FORMAT(123('*'))
!
!
!
      WRITE(6,6121)
 6121 FORMAT(/,' FOR (NK,NEQ,NPH) = (3,4,4) - EQUILIBRIUM REACTION',//,&
     &' ******************************  ******************************',&
     &'   *********************************************************',/,&
     &' *         COMPONENTS         *  *           PHASES           *',&
     &'   *   FLUID PHASE CONDITIONS       PRIMARY VARIABLES      *',/,&
     &' ******************************  ******************************',&
     &'   *********************************************************',/,&
     &' *                            *  *                            *',&
     &'   *                              X1    X2   X3   X4  KEY  *',/,&
     &' *       # 1  -  H2O          *  *       # 1  -  GAS          *',&
     &'   *                             ------------------------- *',/,&
     &' *                            *  *                            *',&
     &'   *                                                       *',/,&
     &' *       # 2  -  CH4          *  *       # 2  -  AQUEOUS      *',&
     &'   *   SINGLE-PHASE AQUEOUS       P,  X_mA, x_iA,  T, Aqu  *',/,&
     &' *                            *  *                            *', &
     &'   *                                                       *',/,&
     &' *       # 3  -  INHIBITOR    *  *       # 3  -  HYDRATE      *',&
     &'   *   SINGLE-PHASE GAS           Pg, X_mA, x_iA,  T, Gas  *',/,&
     &' *                            *  *                            *',&
     &'   *                                                       *',/,&
     &' *       # 4  -  HEAT         *  *       # 4  -  ICE          *',&
     &'   *   2-PHASE (AQ.+GAS)          Pg,   Sw, x_iA,  T, AqG  *',/,&
     &' *                            *  *                            *',&
     &'   *                                                       *',/,&
     &' ******************************  ******************************',&
     &'   *   2-PHASE (AQ.+HYDRATE)      Pg,   Sw, x_iA,  T, AqH  *',/,&
     &'                                                               ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *   2-PHASE (ICE+GAS)          Pg,   Si, x_iA,  T, IcG  *',/,&
     &'                                                               ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *   2-PHASE (ICE+HYDRATE)      P,    Si, x_iA,  T, IcH  *',/,&
     &'                                                               ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *   3-PHASE (AQ.+GAS+HYDRATE)  Sg,   Sw, x_iA,  T, AGH  *',/,&
     &'                                                               ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *   3-PHASE (ICE+GAS+HYDRATE)  Sg,   Si, x_iA,  T, IGH  *',/,&
     &'                                                               ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *   3-PHASE (GAS+AQ.+ICE)      Pg,   Sw, x_iA,  T, GAI  *',/,&
     &'                                                               ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *   3-PHASE (HYDRATE+Aq.+ICE)  Sh,   Sw, x_iA,  T, HAI  *',/,&
     &'                                                               ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *   QUADRUPLE POINT            Pg,   Sw, x_iA,  T, QuP  *',/,&
     &'                                                               ',&
     &'   *   (AQ.+GAS+HYDRATE+ICE)                               *',/,&
     &'                                                               ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *********************************************************')
!
!
!
      WRITE(6,6040)
!
!
!
      WRITE(6,6123)
 6123 FORMAT(/,' FOR (NK,NEQ,NPH) = (3,4,4) - KINETIC REACTION',//,&
     &' ******************************  ******************************',&
     &'   *********************************************************',/,&
     &' *         COMPONENTS         *  *           PHASES           *',&
     &'   *   FLUID PHASE CONDITIONS       PRIMARY VARIABLES      *',/,&
     &' ******************************  ******************************',&
     &'   *********************************************************',/,&
     &' *                            *  *                            *',&
     &'   *                              X1    X2   X3   X4  KEY  *',/,&
     &' *       # 1  -  H2O          *  *       # 1  -  GAS          *',&
     &'   *                             ------------------------- *',/,&
     &' *                            *  *                            *',&
     &'   *                                                       *',/,&
     &' *       # 2  -  CH4          *  *       # 2  -  AQUEOUS      *',&
     &'   *   SINGLE-PHASE AQUEOUS       P,  X_mA,  Sh,   T, Aqu  *',/,&
     &' *                            *  *                            *', &
     &'   *                                                       *',/,&
     &' *       # 3  -  HYDRATE      *  *       # 3  -  HYDRATE      *',&
     &'   *   SINGLE-PHASE GAS           Pg, X_mA,  Sh,   T, Gas  *',/,&
     &' *                            *  *                            *',&
     &'   *                                                       *',/,&
     &' *       # 4  -  HEAT         *  *       # 4  -  ICE          *',&
     &'   *   2-PHASE (AQ.+GAS)          Pg,   Sw,  Sh,   T, AqG  *',/,&
     &' *                            *  *                            *',&
     &'   *                                                       *',/,&
     &' ******************************  ******************************',&
     &'   *   2-PHASE (AQ.+HYDRATE)      Pg,   Sw, x_mA,  T, AqH  *',/,&
     &'                                                               ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *   2-PHASE (ICE+GAS)          Pg,   Si,  Sh,   T, IcG  *',/,&
     &'                                                               ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *   2-PHASE (ICE+HYDRATE)      P,    Si,  Sh,   T, IcH  *',/,&
     &'                                                               ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *   3-PHASE (AQ.+GAS+HYDRATE)  Pg,   Sw,  Sg,   T, AGH  *',/,&
     &'                                                               ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *   3-PHASE (ICE+GAS+HYDRATE)  Pg,   Si,  Sh,   T, IGH  *',/,&
     &'                                                               ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *   3-PHASE (GAS+AQ.+ICE)      Pg,   Sw,  Sg,   T, GAI  *',/,&
     &'                                                               ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *   3-PHASE (HYDRATE+Aq.+ICE)  P,    Sw,  Sh,   T, HAI  *',/,&
     &'                                                               ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *   QUADRUPLE POINT            Pg,   Sw,  Sg,  Sh, QuP  *',/,&
     &'                                                               ',&
     &'   *   (AQ.+GAS+HYDRATE+ICE)                               *',/,&
     &'                                                               ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *********************************************************')
!
!
!
      WRITE(6,6040)
!
!
!
      WRITE(6,6125)
 6125 FORMAT(/,' FOR (NK,NEQ,NPH) = (4,5,4) - KINETIC REACTION',//,&
     &' ******************************  ******************************',&
     &'   *********************************************************',/,&
     &' *         COMPONENTS         *  *           PHASES           *',&
     &'   *   FLUID PHASE CONDITIONS       PRIMARY VARIABLES      *',/,&
     &' ******************************  ******************************',&
     &'   *********************************************************',/,&
     &' *                            *  *                            *',&
     &'   *                         X1   X2   X3    X4   X5   KEY *',/,&
     &' *       # 1  -  H2O          *  *       # 1  -  GAS          *',&
     &'   *                         ----------------------------- *',/,&
     &' *                            *  *                            *',&
     &'   *                                                       *',/,&
     &' *       # 2  -  CH4          *  *       # 2  -  AQUEOUS      *',&
     &'   *   SINGLE-PHASE AQUEOUS  P,  X_mA, Sh,  X_iA,  T,  Aqu *',/,&
     &' *                            *  *                            *', &
     &'   *                                                       *',/,&
     &' *       # 3  -  HYDRATE      *  *       # 3  -  HYDRATE      *',&
     &'   *   SINGLE-PHASE GAS      Pg,  Sw,  Sh,  X_iA,  T,  Gas *',/,&
     &' *                            *  *                            *',&
     &'   *                                                       *',/,&
     &' *       # 4  -  INHIBITOR    *  *       # 4  -  ICE          *',&
     &'   *   2-PHASE (AQ.+GAS)     Pg,  Sw,  Sh,  X_iA,  T,  AqG *',/,&
     &' *                            *  *                            *',&
     &'   *                                                       *',/,&
     &' *       # 5  -  HEAT         *  ******************************',&
     &'   *   2-PHASE               Pg,  Sw, X_mA, X_iA,  T,  AqH *',/,&
     &' *                            *                                ',&
     &'   *   (AQ.+HYDRATE)                                       *',/,&
     &' ******************************                                ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *   2-PHASE (ICE+GAS)     Pg,  Si,  Sh,  X_iA,  T,  IcG *',/,&
     &'                                                               ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *   2-PHASE (ICE+HYDRATE) P,   Si,  Sh,  X_iA,  T,  IcH *',/,&
     &'                                                               ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *   3-PHASE               Pg,  Sw,  Sg,  X_iA,  T,  AGH *',/,&
     &'                                                               ',&
     &'   *   (AQ.+GAS+HYDRATE)                                   *',/,&
     &'                                                               ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *   3-PHASE               Pg,  Si,  Sh,  X_iA,  T,  IGH *',/,&
     &'                                                               ',&
     &'   *   (ICE+GAS+HYDRATE)                                   *',/,&
     &'                                                               ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *   3-PHASE (GAS+AQ.+ICE) Pg,  Sw,  Sg,  X_iA,  T,  GAI *',/,&
     &'                                                               ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *   3-PHASE               P,   Sw,  Sh,  X_iA,  T,  HAI *',/,&
     &'                                                               ',&
     &'   *   (HYDRATE+Aq.+ICE)                                   *',/,&
     &'                                                               ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *   QUADRUPLE POINT       Pg,  Sw,  Sg,  X_iA,  Sh, QuP *',/,&
     &'                                                               ',&
     &'   *   (AQ.+GAS+HYDRATE+ICE)                               *',/,&
     &'                                                               ',&
     &'   *                                                       *',/,&
     &'                                                               ',&
     &'   *********************************************************')
!
!
!
      WRITE(6,6040)
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=> End of PRINT_MorInfo
!
!
      RETURN
!
      END SUBROUTINE PRINT_MorInfo
!
!
!C
!C***********************************************************************
!C@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!C***********************************************************************
!
!
!C
      SUBROUTINE InOutFiles
! 
! ...... Modules to be used 
! 
         USE Basic_Param, ONLY: EX_FileVERS
		 USE MPI_PARAM
		 USE Solver_Param, ONLY: IUNIT
		 USE MPI_SUBROUTINES
!C
!!C***********************************************************************
!C***********************************************************************
!C*                                                                     *
!C*!  ROUTINE FOR CHECKING/OPENING THE FILES NEEDED BY "HydrateResSim"  *
!C*                                                                     *
!C*!                  Version 1.0 - January 14, 2003                    *     
!C*                                                                     *
!C***********************************************************************
!C***********************************************************************
!C
      IMPLICIT NONE
! 
! ----------
! ... Logical variables
! ----------
! 
      LOGICAL :: EX
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of InOutFiles
!
!
      if(MPI_RANK==0) PRINT 6001        ! Write comment in the output file     
! 
      if(MPI_RANK==0)  WRITE(11,6000)    ! Write comment in file VERS
! 
! 
! -------
! ... File MESH
! -------
! INT 6004
           OPEN(4,FILE='MESH')
              
! -------
! ... File INCON
! -------
! 	
			CALL MPI_CREATE_FILE('INCON',IM_1)
            
! 
! -------
! ... File GENER
! -------
! 
      OPEN(3,FILE='GENER')
! 
! -------
! ... File LINEQ
! -------
! 
      OPEN(15,FILE='LINEQ')
! 
! -------
! ... File TABLE
! -------
! 
       OPEN(8,FILE='TABLE')
!! -------
! ... File Solver
! -------
! 
       OPEN(IUNIT,FILE='SOLVER')
!
!
!C
!
!C
!C***********************************************************************
!C*                                                                     *
!C*!                       F  O  R  M  A  T  S                          *
!C*                                                                     *
!C***********************************************************************
!
!

 6000 FORMAT(/,'InOutFiles        1.0   14 January   2003',6X,'Open ',&
     &         'files "VERS", "MESH", "INCON", "GENER", ',&
     &         '"LINEQ", and "TABLE"')
!
 6001 FORMAT(//' SUMMARY OF DISK FILES'/)
!
 6002 FORMAT(' FILE "VERS"  EXISTS --- OPEN AS AN OLD FILE')
 6003 FORMAT(' FILE "VERS" DOES NOT EXIST --- OPEN AS A NEW FILE')
!
 6004 FORMAT(' FILE "MESH"  EXISTS --- OPEN AS AN OLD FILE')
 6005 FORMAT(' FILE "MESH" DOES NOT EXIST --- OPEN AS A NEW FILE')
!
 6006 FORMAT(' FILE "INCON" EXISTS --- OPEN AS AN OLD FILE')
 6007 FORMAT(' FILE "INCON" DOES NOT EXIST --- OPEN AS A NEW FILE')
!
 6008 FORMAT(' FILE "GENER" EXISTS --- OPEN AS AN OLD FILE')
 6009 FORMAT(' FILE "GENER" DOES NOT EXIST --- OPEN AS A NEW FILE')
!
 6010 FORMAT(' FILE "SAVE"  EXISTS --- OPEN AS AN OLD FILE')
 6011 FORMAT(' FILE "SAVE" DOES NOT EXIST --- OPEN AS A NEW FILE')
!
 6012 FORMAT(' FILE "LINEQ" EXISTS --- OPEN AS AN OLD FILE')
 6013 FORMAT(' FILE "LINEQ" DOES NOT EXIST --- OPEN AS A NEW FILE')
!
 6014 FORMAT(' FILE "TABLE" EXISTS --- OPEN AS AN OLD FILE')
 6015 FORMAT(' FILE "TABLE" DOES NOT EXIST --- OPEN AS A NEW FILE')
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of InOutFiles
!
!
      RETURN
!
      END SUBROUTINE InOutFiles
!
!
!C
!C***********************************************************************
!C@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!C***********************************************************************
!
!
!C
      SUBROUTINE NumFloPo_Digits
! 
! ...... Modules to be used 
! 
         USE GenControl_Param
		 USE MPI_PARAM
!C
!C***********************************************************************
!C***********************************************************************
!C*                                                                     *
!C*    ROUTINE FOR CALCULATING THE NUMBER OF SIGNIFICANT DIGITS FOR     *
!C*    FLOATING POINT PROCESSING; ASSIGN DEFAULT FOR DFAC, AND PRINT    *
!C*      APPROPRIATE WARNING WHEN MACHINE ACCURACY IS INSUFFICIENT      *
!C*                                                                     *
!C*                   Version 1.0 - March 11, 2003                      *     
!C*                                                                     *
!C***********************************************************************
!C***********************************************************************
!C
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: DF
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: N10
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=> Main body of NumFloPo_Digits
!
!
      IF(MPI_RANK==0) WRITE(11,6000)
! 
! -------
! ... Machine accuracy from FORTRAN95 intrinsic functions          
! -------
! 
      N10 = PRECISION(1.0d-12)
      DF  = SQRT(EPSILON(1.0d-12))
!
      IF(MPI_RANK==0) PRINT 6003, N10,DF
! 
! -------
! ... Determine the derivative increment         
! -------
! 
      IF_DFAC: IF(DFAC == 0.0d0) THEN
                  DFAC = DF
                  IF(MPI_RANK==0) PRINT 6010
               ELSE
                  IF(MPI_RANK==0) PRINT 6011, DFAC
               END IF IF_DFAC
!
      IF(MPI_RANK==0) PRINT 6016
! 
! -------
! ... Additional print-outs         
! -------
! 
      IF(N10 <= 12 .AND. N10 > 8 .AND. MPI_RANK==0) PRINT 6024
      IF(N10 <= 8 .AND. MPI_RANK==0) PRINT 6025
!
!
!C***********************************************************************
!C*                                                                     *
!C*!                       F  O  R  M  A  T  S                          *
!C*                                                                     *
!C***********************************************************************
!
!
 6000 FORMAT(/,'NumFloPo_Digits   1.0   11 March     2003',6X,&
     &         'Calculate number of significant digits for ',&
     &         'floating point arithmetic')
!
 6003 FORMAT(/,24X,84('*'),/,24X,'*',23X,'EVALUATE FLOATING POINT',&
     &       ' ARITHMETIC',25X,'*',/,24X,84('*'),/,24X,'*',82X,'*',/,&
     &       24X,'* FLOATING POINT PROCESSOR HAS APPROXIMATELY',I3,&
     &       ' SIGNIFICANT DIGITS',17X,'*',/,24X,'*',82X,'*',/,24X,'*',&
     &       ' DEFAULT VALUE OF INCREMENT FACTOR FOR NUMERICAL',&
     &       ' DERIVATIVES IS DFAC =',1pE11.4,' *')
 6010 FORMAT(24X,'* DEFAULT VALUE FOR DFAC WILL BE USED',46X,'*')
!
 6011 FORMAT(24X,'* USER-SPECIFIED VALUE DFAC = ',1pE11.4,&
     &           ' WILL BE USED',29X,'*')
 6016 FORMAT(24X,'*',82X,'*'/24X,84('*')/)
!
 6024 FORMAT(' WWWWWWWWWW WARNING WWWWWWWWWW: NUMBER OF SIGNIFICANT',&
     &       ' DIGITS IS MARGINAL;',&
     &       ' EXPECT DETERIORATED CONVERGENCE BEHAVIOR',/)
 6025 FORMAT(' WWWWWWWWWW WARNING WWWWWWWWWW: NUMBER OF SIGNIFICANT',&
     &       ' DIGITS IS INSUFFICIENT;',&
     &       ' CONVERGENCE WILL BE POOR OR FAIL',/,&
     &       ' WWWWWWWWWWWWWWWWWWWWWWWWWWWWW: CODE SHOULD BE RUN',&
     &       ' IN DOUBLE PRECISION!',/)
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of NumFloPo_Digits
!
!
      RETURN
!
      END SUBROUTINE NumFloPo_Digits
!
!
!C
!C***********************************************************************
!C@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!C***********************************************************************
!
!
!C
      SUBROUTINE CPU_TimeF(time)
!
	  USE MPI_PARAM
	  USE MPI_SUBROUTINES
      IMPLICIT NONE
!
      REAL(KIND = 8), INTENT(OUT) :: time
!
      INTEGER :: itime,irate
      INTEGER :: icall = 0
!
      SAVE icall
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Begin CPU_TimeF
!
!
      icall = icall+1
      IF(icall == 1 .and. MPI_RANK==0) WRITE(11,6000)
!
 6000 FORMAT(/,'CPU_TimeF         1.0    9 April     2004',6X,&
     &         'CPU time -- Uses ',&
     &         'FORTRAN95 intrinsic timing functions')
!
!
      call SYSTEM_CLOCK(COUNT=itime, COUNT_RATE=irate)
      TEMP_REAL(1) = dble(itime)/dble(irate)
	  CALL MPI_BROADCAST(REAL_ARRAY=TEMP_REAL,REAL_SIZE=1,ROOT=0)
	  Time=Temp_Real(1)
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End CPU_TimeF
!
!
!c
      RETURN
!
      END SUBROUTINE CPU_TimeF
!
!
!C
!C***********************************************************************
!C@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!C***********************************************************************
!
!
!C
      SUBROUTINE SOLVR_Set
! 
! ...... Modules to be used 
! 
         USE Basic_Param
         USE GenControl_Param
         USE Solver_Param
		 USE MPI_Param
!C
!C***********************************************************************
!C***********************************************************************
!C*                                                                     *
!C*     ROUTINE FOR INITIALIZING PARAMETERS FOR THE SOLVER OPTIONS      *
!C*                   Version 1.0 - July 3, 2003                        *     
!C*                                                                     *
!C***********************************************************************
!C***********************************************************************
!C
      IMPLICIT NONE
!C 
!C 
!C! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of SOLVR_Set
!C 
!C 
      if(MPI_RANK==0 ) THEN 
		WRITE(11,6000)
!
		print 6001, ' '
	  end if
!
!
!C***********************************************************************
!C*                                                                     *
!C*           SETTING SOLVER TYPE AND CORRESPONDING PARAMETERS          *
!C*                                                                     *
!C***********************************************************************
!
!
! 
! -------
! ... Solver selection from MOP(21): Basic parameters    
! -------
! 
      IF_BasicSolv: IF(SolverBlok .EQV. .FALSE.) THEN
!
! ...... Matrix solver
!
         matslv = mop(21)
!
! ...... Z-preconditioning
!
         IF(matslv == 1) THEN
            zprocs = 'Z0'
         ELSE
            zprocs = 'Z1'
         END IF
!
! ...... O-preconditioning
!
         oprocs = 'O0'
!
! ...... Iteration and convergence parameters
!
         ritmax = 1.0d-1
         closur = 1.0d-6
!
      END IF IF_BasicSolv
! 
! -------
! ... Some printout   
! -------
! 
	  if(MPI_RANK==0) THEN
		  PRINT 6015
		  IF(SolverBlok .EQV. .FALSE.) THEN
			 PRINT 6020
		  ELSE
			 PRINT 6021
		  END IF
	   end if
! 
! -------
! ... Default solver (DSLUCS)  
! -------
! 
      If_SolvrDeflt: IF(matslv == 0 .OR. matslv > 7) THEN 
         if(MPI_RANK==0) PRINT 6025, matslv
         matslv = 3
      END IF If_SolvrDeflt
      if(MPI_RANK==0) PRINT 6026, matslv
! 
! -------
! ... Iterative solver selection (preconditioned conjugate gradients) 
! -------
! 
      IF_IterSolv: IF(matslv > 1 .AND. matslv <= 7) THEN
! 
! ...... Limits on convergence criterion
! 
         IF((ABS(closur)) > 1.0d-06) closur = 1.0d-06
         IF((ABS(closur)) < 1.0d-12) closur = 1.0d-12
! 
! ...... Limits on maximum number of CG iterations
! 
         IF(ritmax <= 0.0d0 .OR. ritmax >= 1.0d0) ritmax = 1.0d-1
         nmaxit = MAX(20,INT(NELA*neq*ritmax))
! 
! ...... Some print-out
! 
         if(MPI_RANK==0) PRINT 6030, ritmax,closur,nmaxit
! 
      END IF IF_IterSolv
! 
! -------
! ... Resetting Z-preprocessing for direct solvers 
! -------
! 
      IF_ZDir: IF(zprocs /= 'Z0') THEN
         IF(matslv == 1) THEN
            if(MPI_RANK==0) print 6101, matslv,zprocs
            zprocs='Z0'
         END IF
      END IF IF_ZDir
! 
! -------
! ... Resetting O-preprocessing for direct solvers 
! -------
! 
      IF_ODir: IF(oprocs /= 'O0') THEN
         IF(matslv == 1) THEN
            if(MPI_RANK==0) PRINT 6036, matslv,oprocs
            oprocs='O0'
         END IF
      END IF IF_ODir
! 
! -------
! ... Default Z-preprocessing 
! -------
! 
      IF(zprocs /= 'Z0' .AND. zprocs /= 'Z1' .AND.&
     &   zprocs /= 'Z2' .AND. zprocs /= 'Z3' .AND.&
     &   zprocs /= 'Z4') THEN
            if(MPI_RANK==0) PRINT 6102, zprocs
            zprocs = 'Z1'
      END IF
! 
! -------
! ... Default O-preprocessing 
! -------
! 
      IF(oprocs /= 'O0' .AND. oprocs /= 'O1' .AND.&
     &   oprocs /= 'O2' .AND. oprocs /= 'O3' .AND.&
     &   oprocs /= 'O4') THEN
            if(MPI_RANK==0) PRINT 6037, oprocs
            oprocs = 'O0'
      END IF
! 
! -------
! ... No Z- or O-preprocessing for NEQ = 1
! -------
! 
      IF_OneEq: IF(neq == 1) then
                   if(MPI_RANK==0) PRINT 6200
                   zprocs = 'Z0'
                   oprocs = 'O0'
                END IF IF_OneEq
! 
! -------
! ... Printing the final Z- and O-preprocessing options
! -------
! 
		if(MPI_RANK==0) then
		  PRINT 6103, zprocs
		  PRINT 6038, oprocs
		end if
!
!
!C***********************************************************************
!C*                                                                     *
!C*!                       F  O  R  M  A  T  S                          *
!C*                                                                     *
!C***********************************************************************
!
!
 6000 FORMAT(/,'SOLVR_Set         1.0    3 July      2003',6X,&
     &         'Initialize parameters for the solver package, ',&
     &         'and generate informative printout')
 6001 format(A1)
!
 6015 FORMAT(130('*'),/,'*',T130,'*',/,'*',&
     &       T29,'M A T R I X   S O L V E R   A N D   ',&
     &           'R E L E V A N T   I N F O R M A T I O N',T130,'*',&
     &       /,'*',T130,'*',/,130('*'),/)
 6020 FORMAT(T5,'The solver is determined from MOP(21)')
!
 6021 FORMAT(T5,'The solver is determined from the SOLVR ',&
     &          'data block - MOP(21) IS OVERRIDEN !!!',/)
 6025 FORMAT(T5,'The solution method indicator MATSLV = ',i2,&
     &       5x,'- reset internally to the default, MATSLV = 3')
 6026 FORMAT(/T5,'The solution method indicator MATSLV = ',i2,/, &
     &       T10,'MATSLV = 1: SUBROUTINE LUBAND - ',&
     &           'Direct solver using LU decomposition ',/,&
     &       T10,'MATSLV = 2: SUBROUTINE DSLUBC - ',&
     &           'BI-CONJUGATE GRADIENT SOLVER',/,&
     &       T42,'Incomplete LU factorization preconditioning',/,&
     &       T10,'MATSLV = 3: SUBROUTINE DSLUCS (DEFAULT) - ',&
     &           'Lanczos-type Conjugate Gradient Squared solver ',/,&
     &       T52,'Incomplete LU factorization preconditioning',/,&
     &       T10,'MATSLV = 4: SUBROUTINE DSLUGM - ',&
     &           'Generalized Minimum Residual Conjugate Gradient ',&
     &           'solver',/,&
     &       T42,'Incomplete LU factorization preconditioning',/,&
     &       T10,'MATSLV = 5: SUBROUTINE DLUSTB - ',&
     &           'STABILIZED BI-CONJUGATE GRADIENT SOLVER ',/,&
     &       T42,'Incomplete LU factorization preconditioning',/)
!
 6030 FORMAT(T5,'RITMAX: Maximum # of CG iterations as fraction of ',&
     &          'the total number of equations ',T95,'= ',1PE12.5,/,&
     &       T10,'(0.0 < RITMAX <= 1.0,   Default = 0.1)',/,&
     &       T5,'CLOSUR: Convergence criterion for the CG ',&
     &          'iterations  ',T95,'= ',1PE12.5,/,&
     &       T10,'(1.0e-12 <= CLOSUR <= 1.0e-6,  Default = 1.0e-6)',/,&
     &       T5,'NMAXIT: Maximum # of CG iterations - not to exceed ',&
     &          'the total number of equations NELA*NEQ',&
     &       T95,'= ',I5,/T10,'(20 < NMAXIT <= NREDM)',/)
 6036 FORMAT(/, ' ',15('WARNING-'),//T14,'For MATSLV = ',I1,', no O-',&
     &          'preprocessing can be used',/,&
     &       T14,'Action taken: reset OPROCS = ',A2,&
     &           ' to *O0*; continue execution')
 6037 FORMAT(/, ' ',15('WARNING-'),//T14,'Unknown matrix preprocessing',&
     &          ' option OPROCS = ',a2,/,&
     &       T14,'Action taken: reset OPROCS to',&
     &           ' *O0*; continue execution')
 6038 FORMAT(/T5,'The matrix O-preprocessing system is OPROCS = ',a2,//&
     &       T10,'OPROCS = O0: No O-preprocessing; ',&
     &           'default for NEQ = 1 and for MATSLV = 1'/&
     &       T10,'OPROCS = O1: Elimination of lower half of ',&
     &           'the main-diagonal submatrix with center ',&
     &           'pivoting',/,&
     &       T10,'OPROCS = O2: O1+Elimination of upper half of ',&
     &           'the main-diagonal submatrix with center ',&
     &           'pivoting',/,&
     &       T10,'OPROCS = O3: O2+Normalization - Results in ',&
     &           'unit main-diagonal submatrices ',/,&
     &       T10,'OPROCS = O4: pre-processing which results ',&
     &           'in unit main-diagonal submatrices without center ',&
     &           'pivoting',/)
!
 6101 FORMAT(/, ' ',15('WARNING-'),//T14,'For MATSLV = ',I1,', no Z-',&
     &          'preprocessing can be used',/,&
     &       T14,'Action taken: reset ZPROCS = ',A2,&
     &          ' to *Z0*; continue execution')
 6102 FORMAT(/, ' ',15('WARNING-'),//T14,'Unknown matrix preprocessing',&
     &          ' option ZPROCS = ',a2,/,&
     &       T14,'Action taken: reset ZPROCS to',&
     &           ' *Z1*; continue execution')
 6103 FORMAT(/T5,'The matrix Z-preprocessing system is ZPROCS = ',a2,//&
     &       T10,'ZPROCS = Z0: No Z-preprocessing; ',&
     &           'default for NEQ = 1 and for MATSLV = 1'/&
     &       T10,'ZPROCS = Z1: Replacement of zeros on ',&
     &           'the main-diagonal by a small number; '/&
     &       T24,'default for NEQ > 1 and for 1 < MATSLV < 5'/&
     &       T10,'ZPROCS = Z2: Linear combination of equations ',&
     &           'in each element to produce non-zero ',&
     &           'main diagonal entries',/,&
     &       T10,'ZPROCS = Z3: Normalization of equations, ',&
     &           'followed by Z2',/,&
     &       T10,'ZPROCS = Z4: Same as in OPROCS = O4',/)
!
 6200 format(' !!!!! NEQ=1; do not perform any matrix preprocessing')
!C 
!C 
!C! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of SOLVR_Set
!C 
!C 
      RETURN
! 
      END SUBROUTINE SOLVR_Set
!
!
!C
!C***********************************************************************
!C@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!C***********************************************************************
!
!
!C
      SUBROUTINE PRINT_InputD
! 
! ...... Modules to be used 
! 
         USE Basic_Param
         USE GenControl_Param
         USE Diffusion_Param
! 
         USE Element_Arrays
         USE Connection_Arrays
         USE Variable_Arrays
         USE PFMedProp_Arrays
         USE Q_Arrays
		 USE MPI_PARAM
		 USE MPI_SUBROUTINES
		 USE MPI_ARRAYS
!C
!C***********************************************************************
!C***********************************************************************
!C*                                                                     *
!C*!                ROUTINE COMPUTING ELAPSED CPU TIME                  *
!C*!                                                                    *
!C*!                 Version 1.00 - February 15, 2004                   *     
!C*                                                                     *
!C***********************************************************************
!C***********************************************************************
!C
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
!      REAL(KIND = 8), INTENT(OUT) :: DT,TOTAL
! 
!      REAL(KIND = 8) :: T1,T2
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: i,j,n,idelv,lay,ierr
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of PRINT_InputD
!
!
	if(MPI_RANK == 0 ) THEN
			  WRITE(11,6000)
		! 
		! -------
		! ... Print headings
		! -------
		! 
			  PRINT 100, ' '
			  PRINT 150,TITLE
			  PRINT 1000
			  PRINT 1000
			  PRINT 200
			  PRINT 250
		! 
		! -------
		! ... Print basic computational parameters
		! -------
		! 
			  PRINT 300, NOITE,KDATA,MCYC,MSEC,MCYPR,(MOP(I),I=1,24),&
			 &           DIFF0,TEXP,BE
			  PRINT 350
		! 
		! -------
		! ... Print time and time-step information
		! -------
		! 
			  PRINT 401, TSTART,TIMAX,DELTEN,DELTMX,ELST,GF,REDLT,SCALE
		! 
		! -------
		! ... Print user-specified Dt info 
		! -------
		! 
			  IF(NDLT /= 0) THEN
				 PRINT 450
				 PRINT 451, ((DLT(J+8*(N-1)),J=1,8),N=1,NDLT)
				 PRINT 550
			  ELSE
				 PRINT 500
			  END IF
		! 
		! -------
		! ... Print additional computational parameters
		! -------
		! 
			  PRINT 400, RE1,RE2,U_p,WUP,WNR,DFAC,FOR
			  PRINT 600
		! 
		! -------
		! ... Print the initial values of the primary variables (to be applied uniformly)
		! -------
		! 
			  PRINT 650, DEP(1),DEP(2),DEP(3),DEP(4)
			  PRINT 1000
			  PRINT 700
		! 
		! -------
		! ... Print the properties of the porous/fractured medium
		! -------
		! 
			  DO_NumRok: DO i=1,N_PFmedia
				 PRINT 750
				 PRINT 800, i,MAT(i),&
			 &                media(i)%DensG,&
			 &                media(i)%Poros,&
			 &                media(i)%KThrW,&
			 &                media(i)%SpcHt,&
			 &                media(i)%Compr,&
			 &                media(i)%Expan
				 PRINT 810
				 PRINT 820, media(i)%Perm(1),media(i)%Perm(2),media(i)%Perm(3)
			  END DO DO_NumRok
		! 
			  PRINT 1000
			  PRINT 830
			  PRINT 840
		! 
	end if
! -------
! ... Print the element-specific data
! -------
! 
	 DO i=0,MPI_RANK-1 
		call MPI_BARRIER(MPI_COMM_WORLD,ierr)
	 END DO
	 
      DO_NumEle: DO i=1,NELL
         PRINT 850, elem(i)%name,elem(i)%MatNo,elem(i)%vol
      END DO DO_NumEle
! 
	  DO i=MPI_RANK,MPI_NP-2
		call MPI_BARRIER(MPI_COMM_WORLD,ierr)
	  END DO
	  
	if(MPI_RANK==0) THEN
      PRINT 1000
      PRINT 860
      PRINT 870
	end if
! 
! -------
! ... Print the connection-specific data
! -------
! 
	  
	 DO i=0,MPI_RANK-1 
		call MPI_BARRIER(MPI_COMM_WORLD,ierr)
	 END DO
	 
      DO_NumConx: DO i=1,NCONI
         PRINT 880, conx(i)%nam1,conx(i)%nam2,conx(i)%ki,&
     &              conx(i)%d1,conx(i)%d2,conx(i)%area,conx(i)%beta
      END DO DO_NumConx
	  
	  
	 DO i=MPI_RANK,MPI_NP-2 
		call MPI_BARRIER(MPI_COMM_WORLD,ierr)
	 END DO
! 
! -------
! ... Print the source/sink data
! -------
! 
      IF(NOGN <= 0 ) GO TO 9000
! 
	  IF(MPI_RANK==0) THEN
		  PRINT 1000
		  PRINT 890
	  END IF 
	  
      IDELV = 0
! 
! 
! 
	 DO i=0,MPI_RANK-1 
		call MPI_BARRIER(MPI_COMM_WORLD,ierr)
	 END DO
	 
      DO_NumSS: DO i=1,NOGN
! 
! ...... Deliverability option 
! 
		 if(procnum(SS(i)%el_num)/=MPI_RANK) CYCLE
		 
         IF_DelivR: IF(SS(i)%typ_indx == NK1+1) THEN
! 
            IDELV = IDELV+1
            IF(IDELV == 1) THEN
               LAY = SS(i)%n_TableP
               PRINT 885, LAY
            END IF 
! 
            PRINT 900, SS(i)%name_el,SS(i)%name,SS(i)%pi,&
     &                 SS(i)%bhp,SS(i)%name_el
! 
         END IF IF_DelivR
! 
! ...... Print flow rates of source/sink  
! 
         PRINT 895
         PRINT 900, SS(i)%name_el,SS(i)%name,SS(i)%rate_m,SS(i)%enth
! 
! ...... Print comment for fixed rates or tabular data  
! 
         IF(SS(i)%n_TableP > 1) THEN
            PRINT 897
         ELSE
            PRINT 896
         END IF 
! 
      END DO DO_NumSS
	  
	  
	 DO i=MPI_RANK,MPI_NP-2
		call MPI_BARRIER(MPI_COMM_WORLD,ierr)
	 END DO
! 
! -------
! ... Print initial conditions by element
! -------
! 
 9000 CONTINUE
 if(MPI_RANK==0) THEN
	  PRINT 1000
      PRINT 910
      PRINT 920
 end if

! 
	 DO i=0,MPI_RANK-1
		call MPI_BARRIER(MPI_COMM_WORLD,ierr)
	 END DO
	 
      DO_NumElem: DO i=1,NELL
         PRINT 930, elem(i)%name,elem(i)%phi,(X((i-1)*NK1+j),j=1,NK1)
      END DO DO_NumElem
	  
	 DO i=MPI_RANK,MPI_NP-2
		call MPI_BARRIER(MPI_COMM_WORLD,ierr)
	 END DO
! 
! -------
! ... Print initial closing lines
! -------
! 
	 IF(MPI_RANK==0) THEN
      PRINT 1000
      PRINT 940
      PRINT 1000
      PRINT 1000
	 END IF
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'PRINT_InputD      1.0   22 November  2003',6X,&
     &         'Provide printout of most data provided through ',&
     &         'the main input file')
!
  100 FORMAT(A1,/,'     "HydrateResSim" INPUT DATA'/)
  150 FORMAT(5X,'PROBLEM TITLE: ',A80,/)
!
  200 FORMAT(/,5X,'PROBLEM SPECIFICATIONS',/)
  250 FORMAT('     NOITE     KDATA      MCYC      MSEC     MCYPR',&
     &       '      MOP                            DIFF0          ',&
     &       'TEXP            BE')
!
  300 FORMAT(5(4X,I5,1X),6X,24I1,3(5X,1pE10.3),/)
  350 FORMAT('       TSTART         TIMAX          DELTEN         ',&
     &       'DELTMX            ELST        GF             ',&
     &       'REDLT         SCALE')
!
  400 FORMAT(5X,8(5X,1pE10.3))
  401 FORMAT(4(5X,E10.4),10X,A5,3(5X,E10.4))
  450 FORMAT(/,5X,' VARIABLE TIME STEPS ARE PRESCRIBED')
  451 FORMAT(5X,8(5X,E10.4))
!
  500 FORMAT(/,5X,' A CONSTANT TIME STEP OF DELTEN IS PRESCRIBED',/)
  550 FORMAT('            RE1            RE2            U',&
     &       '              WUP            WNR            ',&
     &       'DFAC           FOR')
!
  600 FORMAT(/,'             DEP(1)              DEP(2)              ',&
     &         'DEP(3)              DEP(4)')
  650 FORMAT(5X,4(5X,1pE15.8))
!
  700 FORMAT(/,5X,'HROCK PROPERTIES',/)
  750 FORMAT(5X,'DOMAIN     MAT        DENSITY        POROSITY     ',&
     &           'CONDUCTIVITY     HEAT CAP       COMPR          EXPAN')
!
  800 FORMAT(5X,I4,6X,A5,6(5X,1pE10.3),/)
  810 FORMAT(2X,'          PERM1          PERM2          PERM3')
  820 FORMAT(5X,3(5X,1pE10.3),/)
  830 FORMAT(/,5X,'ELEMENTS',/)
  840 FORMAT(5X,'        ELEMENT       MATERIAL         VOLUME')
  850 FORMAT(14X,A5,8X,I5,10X,1pE10.3)
!
  885 FORMAT(5X,'WELL ON DELIVERABILITY   $$$$$$$$   OPEN IN',I3,&
     &          ' LAYERS   $$$$$$$$',///,&
     &       5X,'   ELEMENT         SOURCE           PI',&
     &          '             PWB            DEL(Z)',/)
  860 FORMAT(/,5X,'CONNECTIONS',/)
  870 FORMAT(5X,'          ELEM1          ',&
     &          'ELEM2           ISOT           ',&
     &          'DEL1           DEL2           AREA           BETA',/)
  880 FORMAT(5X,2(10X,A5),9X,I5,6X,4(5X,1pE10.3))
  890 FORMAT(/,5X,'GENERATION DATA')
  895 FORMAT('        ELEMENT         SOURCE           RATE',&
     &       '           ENTHALPY'/)
  896 FORMAT(5X,'CONSTANT RATE IS SPECIFIED',/)
  897 FORMAT(5X,'VARIABLE RATE IS SPECIFIED',/)
!
  900 FORMAT(5X,5X,A5,10X,A5,4X,4(5X,1pE10.3))
  910 FORMAT(/,5X,'INITIAL CONDITIONS',/)
  920 FORMAT(5X,'        ELEMENT       POROSITY',&
     &          '         X1             ',&
     &          'X2             X3             X4             X5',/)
  930 FORMAT(5X,10X,A5,6(5X,1pE10.3))
  940 FORMAT(/,5X,'END OF INPUT DATA',/)
!
 1000 FORMAT(5X,117('*'))
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of PRINT_InputD
!
!
      RETURN
! 
!
      END SUBROUTINE PRINT_InputD
!
!
!

	SUBROUTINE CREATE_WRITE_FILES_MPI
		USE MPI_ARRAYS
		USE MPI_PARAM
		USE BASIC_PARAM
		USE MPI_SUBROUTINES
		IMPLICIT NONE
		
		!--CREATING FILE: SAVE. Call only after procnum is assigned values
		INTEGER :: Save_sizeperline=117,save_inidisp=68
		CHARACTER(LEN=4) :: SAVE_name='SAVE'
		
		!--CREATING FILE: Plot_Data1. Call only after procnum is assigned values
		INTEGER :: PD1_sizeperline=145,PD1_inidisp=0
		CHARACTER(LEN=10) :: PD1_name='Plot_Data1'
		
		INTEGER :: ierr
		
		!SAVE FILE
		!To erase previous file, the trick is to open it once using UNIX I/O 
		open(98900,file=SAVE_name,access='sequential')
		write(98900,'(/)')
		close(98900)
		ierr=0
		if(MPI_RANK/=0) THEN
			call MPI_CREATE_FILE_LINEWISE(SAVE_name,SAVE_file,procnum,SAVE_linetype,NEL,NELAL,&
			&Save_sizeperline,save_inidisp,save_filetype,ierr)
		else
			call MPI_CREATE_FILE_LINEWISE(SAVE_name,SAVE_file,procnum,SAVE_linetype,NEL,NELL,&
			&Save_sizeperline,save_inidisp,save_filetype,ierr)
		end if
		if(ierr/=0) THEN
			write(*,*), "Error, can't open the file SAVE"
			call MPI_FINALIZE(ierr)
			stop
		end if
		
		!Plot_Data1 FILE
		open(98901,file=PD1_name,access='sequential')
		write(98901,'(/)')
		close(98901)
		ierr=0
		if(MPI_RANK/=0) THEN
			call MPI_CREATE_FILE_LINEWISE(PD1_name,PD1_file,procnum,PD1_linetype,NEL,NELAL,&
			&PD1_sizeperline,PD1_inidisp,PD1_filetype,ierr)
		else
			call MPI_CREATE_FILE_LINEWISE(PD1_name,PD1_file,procnum,PD1_linetype,NEL,NELL,&
			&PD1_sizeperline,PD1_inidisp,PD1_filetype,ierr)
		end if
		
		if(ierr/=0) THEN
			write(*,*), "Error, can't open the file SAVE"
			call MPI_FINALIZE(ierr)
			stop
		end if
	END SUBROUTINE CREATE_WRITE_FILES_MPI

	
	
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

