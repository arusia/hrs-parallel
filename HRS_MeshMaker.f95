!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
!
      SUBROUTINE MESHM(MC)
! 
         USE GenControl_Param
! 
!
!          .....................................................
!          .                                                   .
!          .  TOUGH2, MODULE MESHM, VERSION 1.0, MARCH 1991    .
!          .....................................................
!
!-----THIS IS THE EXECUTIVE ROUTINE FOR THE *MESHMAKER* MODULE.
!
!     IT IS CALLED WHEN DATA BLOCK "MESHM" IS PRESENT IN THE INPUT
!     FILE, AND WILL THEN PROCEED TO PERFORM INTERNAL MESH GENERATION.
!
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
! 
      CHARACTER VER*5,WORD*5
      LOGICAL EX
      DIMENSION VER(6)
      SAVE ICALL,VER
      DATA VER/'RZ2D ','RZ2DL','XYZ  ','PATTY','MINC ','     '/
      DATA ICALL/0/
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=> Main body of MESHM
!
!
      MC = 0
      ICALL=ICALL+1
      IF(ICALL.EQ.1) WRITE(11,899)
!
  899 FORMAT(/,'MESHM             1.0   24 MAY       1990',6X,&
     &         'EXECUTIVE ROUTINE FOR INTERNAL MESH GENERATION')
!
  100 CONTINUE
      READ (100,1),WORD
    1 FORMAT(A5)
      DO 2 K=1,6
         IF(WORD.EQ.VER(K)) GO TO(11,11,12,13,14,101),K
    2 CONTINUE
!
      PRINT 4,WORD
    4 FORMAT(' MESHMAKER HAS READ UNKNOWN BLOCK LABEL ',A5,'  ---  ',&
     &       'STOP EXECUTION')
      STOP
!
!
!
   11 CONTINUE
      PRINT 21
   21 format(/,' ',131('*'),&
     &       /,' *',20X,'MESHMAKER - RZ2D: ',&
     &                  'GENERATE 2-D R-Z MESH',70X,'*',&
     &       /,' ',131('*'))
!
      CALL RZ2D
      CALL WRZ2D(K)
      GO TO 100
!
!
!
   12 CONTINUE
      PRINT 22
   22 format(/,' ',131('*'),&
     &       /,' *',20X,'MESHMAKER - XYZ: GENERATE 1, 2, OR ',&
     &                  '3-D CARTESIAN MESH',56X,'*',&
     &       /,' ',131('*'))
!
      CALL GXYZ
      GOTO 100
!
   13 CONTINUE
      GOTO 100
!
!
!
   14 CONTINUE
      MC = MC+1
      PRINT 24
   24 format(/,' ',131('*'),&
     &       /,' *',18X,'MESHMAKER - MINC: GENERATE MULTIPLE ',&
     &                  'INTERACTING CONTINUA MESH FOR ',&
     &                  'FRACTURED MEDIUM',29X,'*',&
     &       /,' ',131('*')/)
!
!
!
      INQUIRE(FILE='MINC',EXIST=EX)
      IF(EX) GOTO 112
      PRINT 113
  113 FORMAT(' FILE *MINC* DOES NOT EXIST --- OPEN AS A NEW FILE')
      OPEN(10,FILE='MINC',STATUS='NEW')
      GOTO 120
!
  112 PRINT 114
  114 FORMAT(' FILE *MINC* EXISTS --- OPEN AS AN OLD FILE')
      OPEN(10,FILE='MINC',STATUS='OLD')
!
  120 CONTINUE
!
      CALL MIN!
!
  101 CONTINUE
      PRINT 102
  102 FORMAT( /,' ',131('*'),&
     &       //,' MESH GENERATION COMPLETE --- EXIT FROM ',&
     &          'MODULE *MESHMAKER*',/)
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of MESHM
!
!
      RETURN
      END
!
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
!
      BLOCK DATA MM_0
! 
         USE GenControl_Param
! 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
! 
      CHARACTER NA*1,WHE_x*4,TYPOS*5
      COMMON/N/NA(36)
      COMMON/CH/WHE_x,TYPOS(6)
      COMMON/W2I/NSKIP,NELEMT,NLAY
      COMMON/W2R/PI
! 
      DATA NA/'0','1','2','3','4','5','6','7','8','9',&
     &        'A','B','C','D','E','F','G','H','I','J','K','L','M',&
     &        'N','O','P','Q','R','S','T','U','V','W','X','Y','Z'/
      DATA TYPOS/'ONE-D','TWO-D','THRED','STANA','STANB','STANT'/
      DATA PI/3.14159265/
!      DATA MC/0/
! 
      END
!
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
!
      SUBROUTINE RZ2D
! 
         USE Basic_Param, ONLY: No_CEN
         USE GenControl_Param
! 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
! 
      CHARACTER SUB*5,WORD*5,NA*1
!
      COMMON/N/NA(36)
      COMMON/W1/V(2501),A(2501),D1(2501),D2(2501),RC(2501),H(99999)
      COMMON/W2I/NSKIP,NELEMT,NLAY
      COMMON/W2R/PI
! 
      DIMENSION SUB(4)
!
      SAVE ICALL,SUB
!
      DATA SUB/'RADII','EQUID','LOGAR','LAYER'/
      DATA ICALL/0/
!
      S(X)=X*(X**NLOG-1.)/(X-1.0d0)
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of RZ2D
!
!
      ICALL=ICALL+1
      IF(ICALL.EQ.1) WRITE(11,899)
  899 FORMAT('RZ2D              1.0   11 FEBRUARY  1992',6X,&
     &       'CALCULATE 2-D R-Z MESH FROM INPUT DATA')
!
      NRAD=0
      NEQU=0
      NLOG=0
      NRADC=0
      NEQUC=0
      NLOGC=0
      NINT=0
!
  204 READ (100,200),WORD
  200 FORMAT(A5)
!
      DO 201 K=1,4
      IF(WORD.EQ.SUB(K)) GOTO 202
  201 CONTINUE
!
      PRINT 203,WORD
  203 FORMAT(' HAVE READ UNKNOWN BLOCK LABEL ',A5,'  ---  STOP',&
     &       ' EXECUTION')
      STOP
!
  202 GOTO(211,212,213,214),K
!
  211 CONTINUE
!-----READ A SET OF INTERFACE RADII.
      READ (100,220),NRAD
  220 FORMAT(I5,5X,7E10.4)
!
      NINT1=NINT+1
      NINT=NINT+NRAD
      NRADC=NRADC+NRAD
      READ (100,221),(RC(I),I=NINT1,NINT)
  221 FORMAT(8E10.4)
      GOTO 204
!
  212 CONTINUE
!-----READ DATA FOR GENERATING SOME EQUIDISTANT INTERFACES.
!
      READ (100,220),NEQU,DR
      NINT1=NINT+1
      NINT=NINT+NEQU
      NEQUC=NEQUC+NEQU
      DO 222 I=NINT1,NINT
  222 RC(I)=RC(I-1)+DR
      GOTO 204
!
  213 CONTINUE
!-----READ DATA FOR GENERATING INTERFACES WITH LOGARITHMICALLY
!     INCREASING DISTANCE.
!
      READ (100,220),NLOG,RLOG,DR
      NLOGC=NLOGC+NLOG
      IF(DR.EQ.0.) DR=RC(NINT)-RC(NINT-1)
!
!
      PRINT 109
  109 FORMAT(/' ***** FIND APPROPRIATE FACTOR FOR',&
     X' INCREASING RADIAL DISTANCES *****')
!
      IX=0
      AM=(RLOG-RC(NINT))/DR
      AN=FLOAT(NLOG)
      XM=AM/AN
      IF(XM.EQ.1.) GOTO40
      IF(AM.GT.AN) THEN
      XL=1.
      XR=(AM+1.)**(1./AN)
      XR=MIN(XR,XM)
      ELSE
      XL=AM/(AM+1.)
      XL=MAX(XL,XM)
      XR=1.
      ENDIF
!     FIND PROPER INCREMENT FACTOR THROUGH ITERATIVE BISECTING
   41 XM=0.5*(XL+XR)
      IF(XR.GT.2.*XL) XM=SQRT(XL*XR)
      SMID=S(XM)
      IX=IX+1
!     PRINT 42,IX,XL,XR,XM,SMID,AM
   42 FORMAT(' AT ITERATION',I3,'  XL=',E20.14,'  XR=',&
     XE20.14,'  XM=',E12.6,'  SMID=',E12.6,'  AM=',E12.6)
      IF(IX.GE.1000) GOTO 43
      IF(XR-XL.LE.XL*1.E-10) GOTO40
      IF(SMID.LE.AM) XL=XM
      IF(SMID.GE.AM) XR=XM
      GOTO41
!
   40 RAT=XM
!
      PRINT 30,IX,RAT
   30 FORMAT(6X,' AFTER',I4,' ITERATIONS FOUND INCREMENT FACTOR',&
     X' RAT =',E14.8)
!
      DO 223 I=1,NLOG
      DR=DR*RAT
  223 RC(NINT+I)=RC(NINT+I-1)+DR
!
      NINT=NINT+NLOG
      GOTO 204
!
  214 CONTINUE
!-----COME HERE AFTER ALL NINT INTERFACES ARE ASSIGNED, AND READ
!     DATA ON LAYER THICKNESSES.
!
      READ (100,220),NLAY
      READ (100,221),(H(I),I=1,NLAY)
      DO219 I=2,NLAY
      IF(H(I).EQ.0.) H(I)=H(I-1)
  219 IF(H(I).LT.0.) H(I)=0.
      IF(H(1).LT.0.) H(1)=0.
!
!-----NOW ASSIGN ALL GEOMETRY DATA FOR A LAYER OF UNIT THICKNESS.
!
!=====PRINT ALL GEOMETRIC QUANTITIES=====
!
!
      PRINT 19
   19 FORMAT(//' * * * * * M E S H  G E O M E T R Y * * * * *',10X,&
     X'VOLUME AND AREA ARE GIVEN FOR HEIGHT = 1 METER'//&
     X' ELEM     REL           RCON           D1             D2     ',&
     X'       V              A'/)
!
      NELEMT=NINT-1
! 
! ----------------
! ........ KP - 2/11/92
! ........ Begin change to avoid "funny" multiply-add-related
! ........ behavior on IBM RISC6000
! ----------------
! 
!-KP  V(I)=PI*(RC(I+1)**2-RC(I)**2)
      DO 15 I=1,NELEMT
         A(I)  = 2.*PI*RC(I+1)
         REL   = SQRT(RC(I+1)*RC(I))
         D1(I) = REL-RC(I)
         D2(I) = RC(I+1)-REL
         v(i)  = pi*(rc(i+1)+rc(i))*(rc(i+1)-rc(i))
   15 CONTINUE
!
! 
! ----------------
! ........ KP - 2/11/92
! ........ End change 
! ----------------
! 
!
! 
! ----------------
! ........ GJM - 9/19/2000
! ........ Begin change to assign correct location for average gridblock pressure
! ----------------
! 
!
      DO I=1,NELEMT
!        REL = RC(I+1) - D(I)
         REL = SQRT(RC(I+1)*RC(I))
         PRINT 18, I,REL,RC(I+1),D1(I),D2(I),V(I),A(I)
      END DO
   18 FORMAT(' ',I4,6(2X,1PE12.5))
! 
! ----------------
! ........ GJM - 9/19/2000
! ........ End change 
! ----------------
! 
      REWIND 4
      PRINT 20
   20 FORMAT(/' WRITE FILE *MESH* FROM INPUT DATA')
! 
! - GJM: Begin modification for 8-character elements
! 
      IF(No_CEN.EQ.8) THEN
         WRITE(4,5003) NELEMT,NRADC,NEQUC,NLOGC,RC(1),RC(NELEMT+1)
      ELSE
         WRITE(4,3) NELEMT,NRADC,NEQUC,NLOGC,RC(1),RC(NELEMT+1)
      END IF
! 
 5003 FORMAT('ELEMEext2 --- ',4I5,F10.5,F10.3)
    3 FORMAT('ELEME --- ',4I5,F10.5,F10.3)
! 
!! - GJM: End modification for 8-character elements
! 
      RETURN
!
   43 CONTINUE
      PRINT 42,IX,XL,XR,XM,SMID,AM
      PRINT 44
   44 FORMAT(' CONVERGENCE FAILURE IN SUBROUTINE RZ2D'/&
     X       ' STOP EXECUTION')
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of RZ2D
!
!
      STOP
      END
!
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
!
      SUBROUTINE WRZ2D(K)
! 
         USE Basic_Param, ONLY: No_CEN
         USE GenControl_Param
! 
         USE Element_Arrays
! 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
! 
      CHARACTER NA*1,NII*1,NII1*1,NOV*1,NOV1*1,DOM*5
	  INTEGER :: MIX(8),MJX(6),XX,YY
! 
      COMMON/N/NA(36)
      COMMON/W1/V(2501),A(2501),D1(2501),D2(2501),RC(2501),H(99999)
      COMMON/W2I/NSKIP,NELEMT,NLAY
      COMMON/W2R/PI
! 
      SAVE ICALL
      DATA ICALL/0/
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=> Main body of WRZ2D
!
!
      ICALL=ICALL+1
      IF(ICALL.EQ.1) WRITE(11,899)
! 899 FORMAT(6X,'WRZ2D    1.0      26 MARCH     1991',6X,
  899 FORMAT('WRZ2D             2.0   20 September 2000',6X,&
     &       'WRITE DATA FOR 2-D R-Z MESH ON FILE *MESH*')
!
      NSKIP1=1
      NSKIP2=2
!
!      NII=' '
      IF(K.EQ.2) GOTO1
!
!-----COME HERE TO GENERATE MESH BY VERTICAL COLUMNS
!      DO 4 I= NSKIP1,NELEMT
!      MI = MOD(I,100)
      OUTER_LOOP: DO I8=NSKIP1-1,NELEMT-1
         MI  = MOD(I8,100)
         I   = I8+1
!
         IF(MOD(I,100) == 0) THEN
            II = I/100
         ELSE
            II = I/100+1
         END IF
         NII = NA(II)
! 
! ----------------
! ........ GJM - 9/19/2000
! ........ Begin change to assign correct location for average gridblock pressure
! ----------------
! 
!        REL = RC(I+1)-D(I)
         REL = SQRT(RC(I+1)*RC(I))
! 
! ----------------
! ........ GJM - 9/19/2000
! ........ End change 
! ----------------
! 
         ZJ  =-H(1)/2.0d0
! 
! - GJM: Begin modification for 8-character elements
! 
         IF(No_CEN.EQ.8) THEN
            DO J=1,NLAY
			   !Finding last 4-6 th place numbers of the radial naming 
			   MIX(3) = MOD((I-1)/10**2,10)+1
			   MIX(2) = MOD((I-1)/10**3,10)+1
			   MIX(1) = MOD((I-1)/10**4,10)+1
			   !Finding first 3 numbers/characters
			   MJX(2) = MOD((J-1),36)+1
			   MJX(1) = MOD((J-1)/36,36)+1
			   NOV = NA( MOD((J-1)/36**2,36)+11)
			   
               IF(J.GT.1) ZJ=ZJ-H(J)/2.-H(J-1)/2.0d0
               AIJ = 0.0d0
               IF(J.EQ.1) AIJ = AIJ+V(I)
               IF(J.EQ.NLAY) AIJ = AIJ+V(I)
               VIJ = V(I)*H(J)
               WRITE(4,5005) NOV,NA(MJX(1)),NA(MJX(2)),&
							& NA(MIX(1)),NA(MIX(2)),NA(MIX(3)),MI,&
							& VIJ,AIJ,REL,ZJ
            END DO 
         ELSE 
            DO J=1,NLAY
               MJ  = MOD(J-1,36)+1
               NOV = NA((J-1)/36+11)
               IF(J.GT.1) ZJ=ZJ-H(J)/2.-H(J-1)/2.0d0
               AIJ = 0.0d0
               IF(J.EQ.1) AIJ = AIJ+V(I)
               IF(J.EQ.NLAY) AIJ = AIJ+V(I)
               VIJ = V(I)*H(J)
               WRITE(4,5) NOV,NA(MJ),NII,MI,VIJ,AIJ,REL,ZJ
            END DO 
         END IF 
	 END DO OUTER_LOOP
!
    5 FORMAT(   3A1,I2.2,10X,'    1',2(1PE10.4),10X,1PE10.4,10X,1PE10.3)
 5005 FORMAT(6A1,1I2.2,7X,'    1',2(1PE10.4),10X,1PE10.4,10X,1PE10.3)
! 
!! - GJM: End modification for 8-character elements
! 
!
!     MJ RUNS FROM 1 TO 36 FOR CONSECUTIVE LAYERS.
!     NOV RUNS THROUGH A, B, C, ... FOR THE 1ST, 2ND, 3RD, ... SET
!     OF 36 LAYERS.
!
!-----CONVENTION FOR ELEMENT NAMES 
!     NOV = A, B, C, .... LABELS 1ST, 2ND, 3RD, ... SET OF 37 LAYERS.
!     NA = 0, 1, 2, 3, ... 9, A, B, C, ... LABELS LAYERS.
!     NII = 0, 1, 2, 3, ... AFTER 100, 200, 300, ... RADIAL ELEMENTS.
!     MI = 0, 1, 2, 3, ... 99; NUMBERS RADIAL ELEMENTS IN EXCESS OF
!                              FULL HUNDREDS.
!
      GOTO 2
!
    1 CONTINUE
!-----COME HERE TO GENERATE MESH BY HORIZONTAL LAYERS
      ZJ=-H(1)/2.
      DO 3 J=1,NLAY
			   
         MJ  = MOD(J-1,36)+1
         NOV = NA((J-1)/36+11)
		 !Finding first 4 numbers/characters 
		if(No_CEN==8) THEN
			   !Finding first 3 numbers/characters
			   MJX(2) = MOD((J-1),36)+1
			   MJX(1) = MOD((J-1)/36,36)+1
			   NOV = NA( MOD((J-1)/36**2,36)+11)
		end if
		
         IF(J > 1) ZJ=ZJ-H(J)/2.-H(J-1)/2.
!        DO 3 I = NSKIP1,NELEMT
         DO 3 I8 = NSKIP1-1,NELEMT-1
!           MI  = MOD(I,100)
            MI  = MOD(I8,100)
            I   = I8+1
!
            IF(MOD(I,100) == 0) THEN
               II = I/100
            ELSE
               II = I/100+1
            END IF
            NII = NA(II)
!           IF(II.GE.1) NII = NA(II)
! 
! ----------------
! ........ GJM - 9/19/2000
! ........ Begin change to assign correct location for average gridblock pressure
! ----------------
! 
!-GJM       REL = RC(I+1)-D(I)
            REL = SQRT(RC(I+1)*RC(I))
! 
! ----------------
! ........ GJM - 9/19/2000
! ........ End change
! ----------------
! 
            AIJ = 0.0
            IF(J.EQ.1) AIJ = AIJ+V(I)
            IF(J.EQ.NLAY) AIJ = AIJ+V(I)
            VIJ = V(I)*H(J)
! 
! - GJM: Begin modification for 8-character elements
! 
            IF(No_CEN.EQ.8) THEN
			 !Finding last 4 numbers of the radial naming in base 32 with initial number 1 instead of 0.
			   !Finding last 4-6 th place numbers of the radial naming 
			   MIX(3) = MOD((I-1)/10**2,10)+1
			   MIX(2) = MOD((I-1)/10**3,10)+1
			   MIX(1) = MOD((I-1)/10**4,10)+1
               WRITE(4,5005) NOV,NA(MJX(1)),NA(MJX(2)),&
							& NA(MIX(1)),NA(MIX(2)),NA(MIX(3)),MI,&
							& VIJ,AIJ,REL,ZJ
            ELSE
               WRITE(4,5) NOV,NA(MJ),NII,MI,VIJ,AIJ,REL,ZJ
            END IF
! 
! - GJM: End modification for 8-character elements
! 
    3 CONTINUE
!
!     MJ RUNS FROM 1 TO 35 FOR CONSECUTIVE LAYERS.
!     NOV RUNS THROUGH A, B, C, ... FOR THE 1ST, 2ND, 3RD, ... SET
!     OF 35 LAYERS.
!
    2 CONTINUE
!
      WRITE(4,6)
    6 FORMAT('     ')
      WRITE(4,7)
    7 FORMAT('CONNE')
!
!-----ASSIGN HORIZONTAL CONNECTIONS.
!
!     NII=' '
!     NII1=' '
!     DO 8 I=NSKIP2,NELEMT
      DO 8 I8=NSKIP2-1,NELEMT-1
!        MI = MOD(I,100)
         MI = MOD(I8,100)
         I  = I8+1
!        II = I/100
!        IF(II.GE.1) NII=NA(II)
!        NII=NA(II+1)

         IF(MOD(I,100) == 0) THEN
            II = I/100
         ELSE
            II = I/100+1
         END IF
         NII = NA(II)

!        MI1 = MOD(I-1,100)
         MI1 = MOD(I8-1,100)
!        II1 = (I-1)/100
!        IF(II1.GE.1) NII1=NA(II1)
!        NII1=NA(II1+1)

         IF(MOD(I8,100) == 0) THEN
            II1 = (I8)/100
         ELSE
            II1 = (I8)/100+1
         END IF
         NII1 = NA(II1)
! 
! - GJM: Begin modification for 8-character elements
! 

		  !Finding last 4-6 th place numbers of the radial naming 
		   MIX(3) = MOD((I-1)/10**2,10)+1
		   MIX(2) = MOD((I-1)/10**3,10)+1
		   MIX(1) = MOD((I-1)/10**4,10)+1
		   
		   MIX(6) = MOD((I8-1)/10**2,10)+1
		   MIX(5) = MOD((I8-1)/10**3,10)+1
		   MIX(4) = MOD((I8-1)/10**4,10)+1
         IF(No_CEN.EQ.8) THEN
            DO J=1,NLAY
			   !Finding first 3 numbers/characters
			   MJX(2) = MOD((J-1),36)+1
			   MJX(1) = MOD((J-1)/36,36)+1
			   NOV = NA( MOD((J-1)/36**2,36)+11)	   

			   
               AIJ = A(I-1)*H(J)
               WRITE(4,5009) NOV,NA(MJX(1)),NA(MJX(2)),NA(MIX(1)),NA(MIX(2)),NA(MIX(3)),MI,&
     &                       NOV,NA(MJX(1)),NA(MJX(2)),NA(MIX(4)),NA(MIX(5)),NA(MIX(6)),MI1,&
     &                             D2(I-1),D1(I),AIJ
            END DO
         ELSE
            DO J=1,NLAY
               MJ  = MOD(J-1,36)+1
               NOV = NA((J-1)/36+11)
               AIJ = A(I-1)*H(J)
               WRITE(4,9) NOV,NA(MJ),NII1,MI1,&
     &                    NOV,NA(MJ),NII,MI,&
     &                    D2(I-1),D1(I),AIJ
            END DO
         END IF
! 
 5009 FORMAT(6A1,1I2.2,6A1,1I2.2,13X,'1',3(1PE10.4))
    9 FORMAT(3A1,I2.2,3A1,I2.2,19X,'1',3(1PE10.4))
! 
! - GJM: End modification for 8-character elements
! 
    8 CONTINUE
!
!     MJ RUNS FROM 1 TO 36 FOR CONSECUTIVE LAYERS.
!     NOV RUNS THROUGH A, B, C, ... FOR THE 1ST, 2ND, 3RD, ... SET
!     OF 35 LAYERS.
!    
!
      IF(NLAY.LE.1) GOTO42
!
!-----NOW FOR THE VERTICAL CONNECTIONS.
!
!     NII=' '
!     DO 40 I=NSKIP1,NELEMT
      DO 40 I8=NSKIP1-1,NELEMT-1
!        MI  = MOD(I,100)
         MI  = MOD(I8,100)
         I   = I8+1
!
         IF(MOD(I,100) == 0) THEN
            II = I/100
         ELSE
            II = I/100+1
         END IF
         NII = NA(II)
!
!        IF(II.GE.1) NII=NA(II)
         AIJ = V(I)
! 
! - GJM: Begin modification for 8-character elements
! 
		  !Finding last 4-6 th place numbers of the radial naming 
		   MIX(3) = MOD((I-1)/10**2,10)+1
		   MIX(2) = MOD((I-1)/10**3,10)+1
		   MIX(1) = MOD((I-1)/10**4,10)+1
		
         IF(No_CEN.EQ.8) THEN
            DO J=2,NLAY
               J1   = J-1
			   
			   
			   !Finding first 3 numbers/characters 
			    MJX(2) = MOD((J-1),36)+1
			   MJX(1) = MOD((J-1)/36,36)+1
			   NOV = NA( MOD((J-1)/36**2,36)+11)	   

			    MJX(4) = MOD((J1-1),36)+1
			   MJX(3) = MOD((J1-1)/36,36)+1
			   NOV1 = NA( MOD((J1-1)/36**2,36)+11)	   

			   
               D1U  = H(J1)/2.0d0
               D2U  = H(J)/2.0d0
               WRITE(4,5041) NOV1,NA(MJX(3)),NA(MJX(4)),NA(MIX(1)),NA(MIX(2)),NA(MIX(3)),MI,&
     &                       NOV,NA(MJX(1)),NA(MJX(2)),NA(MIX(1)),NA(MIX(2)),NA(MIX(3)),MI,&
     &                             D1U,D2U,AIJ
            END DO
         ELSE
            DO J=2,NLAY
               J1   = J-1
               MJ   = MOD(J-1,36)+1
               NOV  = NA((J-1)/36+11)
               MJ1  = MOD(J1-1,36)+1
               NOV1 = NA((J1-1)/36+11)
               D1U  = H(J1)/2.0d0
               D2U  = H(J)/2.0d0
               WRITE(4,41) NOV1,NA(MJ1),NII,MI,&
     &                     NOV,NA(MJ),NII,MI,&
     &                     D1U,D2U,AIJ
            END DO
         END IF
! 
 5041 FORMAT(6A1,1I2.2,6A1,1I2.2,13X,'3',3(1PE10.4),'1.')
   41 FORMAT(3A1,I2.2,3A1,I2.2,19X,'3',3(1PE10.4),' 1.0e0')
! 
! - GJM: End modification for 8-character elements
! 
   40 CONTINUE
!
   42 CONTINUE
!
      WRITE(4,6)
!

	if(No_CEN == 8) THEN
		!Write Element Numbers of the CONNETIONS at the end of the file
		 WRITE(4,6002)      ! Write Ô+++  Ô at the end of MESH
	   
	   6002 FORMAT('+++  ')
	   6004 FORMAT(2I8)
		 !---For horizontal connections
		 SCOUNT=0
		 DO I8=NSKIP2-1,NELEMT-1
			DO J=1,NLAY
				if(K==1) THEN
					I=(I8-1)*NLAY + J
					II = I + NLAY
				else
					I = (J-1)*NELEMT + I8
					II = I+1
				end if
				WRITE(4,6004,advance="NO"),II,I
				SCOUNT = SCOUNT+1
				IF(SCOUNT==5) THEN
					WRITE(4,*)
					SCOUNT=0
				END IF
			END DO
		END DO
		!---For vertical connections
		DO  I8=NSKIP1,NELEMT
			DO J=2,NLAY
				if(K==1) THEN
					I=(I8-1)*NLAY + J
					II = I-1
				else
					I = (J-1)*NELEMT + I8
					II = I-NELEMT
				end if
				WRITE(4,6004,advance="NO"),II,I
				SCOUNT = SCOUNT+1
				IF(SCOUNT==5) THEN
					WRITE(4,*)
					SCOUNT=0
				END IF
			END DO
		END DO
	END IF
	 !--------------------------------------
      REWIND 4
      READ(4,43) DOM
!
      L=0
! 
! - GJM: Begin modification for 8-character elements
! 
      DO 44 I = NSKIP1,NELEMT
         DO 44 J = 1,NLAY
            L = L+1
            IF(No_CEN == 8) THEN
               READ(4,5043) elem(L)%name 
            ELSE
               READ(4,43) elem(L)%name(1:5)
            END IF
   44 CONTINUE
!
 5043 FORMAT(A8)
   43 FORMAT(A5)
! 
! - GJM: End modification for 8-character elements
! 
      CALL PRZ2D(K,NSKIP1,NELEMT,NLAY)
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of WRZ2D
!
!
      RETURN
      END
!
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
!
      SUBROUTINE PRZ2D(KK,NSKIP1,NELEMT,NLAY)
! 
         USE Basic_Param, ONLY: No_CEN
         USE GenControl_Param
! 
         USE Element_Arrays
! 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
! 
      SAVE ICALL
      DATA ICALL/0/
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=> Main body of PRZ2D
!
!
! 
!     FIND LOCATION OF ELEMENT NAME WITH R-INDEX I, Z-INDEX K.
!     MESH ORGANIZED "BY COLUMNS" (KK = 1)
!
      NIK1(I,K)=(I-1)*NLAY+K
!
!     MESH ORGANIZED "BY LAYERS" (KK = 2)
!
      NIK2(I,K)=(K-1)*(NELEMT-NSKIP1+1)+I
!
      ICALL=ICALL+1
      IF(ICALL.EQ.1) WRITE(11,899)
  899 FORMAT('PRZ2D             1.0   27 MARCH     1991',6X,&
     &       'MAKE STRUCTURED PRINTOUT OF 2-D R-Z MESH')
!
      NRZ = NELEMT*NLAY
      PRINT 31, NELEMT,NLAY,NRZ
   31 FORMAT(/,' ',131('*')/' *',20X,'2-D R-Z MESH WITH NR*NLAY = ',&
     &       I4,' *',I4,'  =',I4,' GRID BLOCKS',52X,'*'/' ',131('*'))
!
      PRINT 18,NLAY,NELEMT
   18 FORMAT(' *',129X,'*'/&
     &   ' *',20X,'THE MESH WILL BE PRINTED AS VERTICAL SLICES',66X,&
     &   '*'/' *',129X,'*'/&
     &   ' *',20X,'LAYERS GO FROM K = 1 TO K = NLAY =',I4,71X,'*'/&
     &   ' *',129X,'*'/&
     &   ' *',20X,'RADIAL GRID BLOCKS GO IN COLUMNS FROM I = 1 TO',&
     &   ' I = NR =',I4,50X,'*'/&
     &   ' *',129X,'*'/' ',131('*')/)
!
! 
! - GJM: Begin modification for 8-character elements
! 
      IF(No_CEN.EQ.8) THEN
         PRINT 5004
         DO K=1,NLAY
            IF(KK == 1) PRINT 5006, K,(elem(NIK1(I,K))%name,&
     &                                           I=NSKIP1,NELEMT)
            IF(KK == 2) PRINT 5006, K,(elem(NIK2(I,K))%name,&
     &                                           I=NSKIP1,NELEMT)
         END DO
      ELSE
         PRINT 4
         DO K=1,NLAY
            IF(KK == 1) PRINT 6, K,(elem(NIK1(I,K))%name(1:5),&
     &                                        I=NSKIP1,NELEMT)
            IF(KK == 2) PRINT 6, K,(elem(NIK2(I,K))%name(1:5),&
     &                                        I=NSKIP1,NELEMT)
         END DO
      END IF
! 
 5004 FORMAT('   COLUMN I =    1        2        3        4        ',&
     &       '5        6        7        8        9       10',&
     &       '       11       12'/' LAYERS')
    4 FORMAT('   COLUMN I =  1     2     3     4     5     6     ',&
     &       '7     8     9    10    11    12    13    14    15    ',&
     &       '16    17    18    19    20'/' LAYERS')
 5006 FORMAT('  K = ',I4,2X,12(1X,A8),/,(12X,12(1X,A8)),/,&
     &                (12X,12(1X,A8)),/,(12X,12(1X,A8)))
    6 FORMAT('  K = ',I4,2X,20(1X,A5)/(12X,20(1X,A5)))
! 
! - GJM: End modification for 8-character elements
! 
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of PRZ2D
!
!
      RETURN
      END
!
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
!
      SUBROUTINE GXYZ
! 
         USE Basic_Param, ONLY: No_CEN
         USE GenControl_Param
! 
         USE Element_Arrays
! 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
! 
!
!-----PROGRAM FOR GENERATING 3-D X-Y-Z MESHES
!
!     GRID BLOCK DIMENSIONS ARE DX, DY, AND DZ
!
      CHARACTER NA*1,NXYZ*2
      CHARACTER*5 DOM,ELNAME,ELNAM1,ONOMA
      COMMON/N/NA(36)
! 
      DIMENSION DX(2500),DY(2500),DZ(2500)
! 
! ----------------
! ........ GJM - 9/19/2000
! ........ Begin addition for optimal numbering to minimize bandwidth 
! ----------------
! 
      DIMENSION DLXYZ(2500,3),DDXYZ(3),RRR(3),NLXYZ(3)
! 
! ----------------
! ........ GJM - 9/19/2000
! ........ End addition for optimal numbering to minimize bandwidth 
! ----------------
! 
      DIMENSION D1(2),D2(2),D3(2)
!
      SAVE ICALL,NX,NY,NZ,PI
      DATA NX,NY,NZ/0,0,0/
      DATA PI/3.14159265/
      DATA ICALL/0/
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=> Main body of GXYZ
!
!
      ICALL=ICALL+1
      IF(ICALL.EQ.1) WRITE(11,899)
! 899 FORMAT(/6X,'GXYZ     1.0      18 MARCH     1991',6X,
  899 FORMAT(/,'GXYZ              2.0   18 September 2000',6X,&
     &         'GENERATE 1, 2, OR 3-D CARTESIAN MESH')
!
      DOM='    1'
      REWIND 4
!
!
!
      READ(100,5001) DEG
!
 5000 FORMAT(A2,3X,I5,E10.4)
 5001 FORMAT(8E10.4)
!
  500 READ(100,5000) NXYZ,NO,DEL
!
!
!
      SELECT CASE(NXYZ)
!
! -------
! ... X-increments
! -------
!
      CASE('NX')
!
         NX1 = NX + 1
         NX  = NX + NO
         IF(DEL /= 0.0d0) THEN
            DX(NX1:NX) = DEL
         ELSE
            READ(100,5001) (DX(I),I=NX1,NX)
         END IF
         GO TO 500
!
! -------
! ... Y-increments
! -------
!
      CASE('NY')
!
         NY1 = NY + 1
         NY  = NY + NO
         IF(DEL /= 0.0d0) THEN
            DY(NY1:NY) = DEL
         ELSE
            READ(100,5001) (DY(I),I=NY1,NY)
         END IF
         GO TO 500
!
! -------
! ... Z-increments
! -------
!
      CASE('NZ')
!
         NZ1 = NZ + 1
         NZ  = NZ + NO
         IF(DEL /= 0.0d0) THEN
            DZ(NZ1:NZ) = DEL
         ELSE
            READ(100,5001) (DZ(I),I=NZ1,NZ)
         END IF
         GO TO 500
!
! -------
! ... End of increment inputs
! -------
!
      CASE DEFAULT 
         CONTINUE
      END SELECT
!
!
!
      RAD  = PI*DEG/180.
      BET  = SIN(RAD)
      BETA = COS(RAD)
!
!-----MAKE PRINTOUT OF MESH SPECIFICATIONS.
!
      NN = NX*NY*NZ
!
      WRITE(6,40) NX,NY,NZ,NN,DEG
   40 FORMAT(/' GENERATE CARTESIAN MESH WITH NX*NY*NZ = ',&
     &I4,' *',I4,' *',I4,'  =  ',I5,'  GRID BLOCKS'/1X,131('*')//&
     &'   THE X-AXIS IS HORIZONTAL; THE Y-AXIS IS ROTATED BY ',E10.4,&
     &' DEGREES AGAINST THE HORIZONTAL'//&
     &'   THE GRID INCREMENTS ARE AS FOLLOWS')
      PRINT 41,NX,(NA(I),I=1,9),(DX(I),I=1,NX)
   41 FORMAT(//'   DELTA-X  (',I4,' INCREMENTS)'//13X,9(A1,10X),'10'//&
     &(10X,10(1X,E10.4)))
      PRINT 42,NY,(NA(I),I=1,9),(DY(I),I=1,NY)
   42 FORMAT(//'   DELTA-Y  (',I4,' INCREMENTS)'//13X,9(A1,10X),'10'//&
     &(10X,10(1X,E10.4)))
      PRINT 43,NZ,(NA(I),I=1,9),(DZ(I),I=1,NZ)
   43 FORMAT(//'   DELTA-Z  (',I4,' INCREMENTS)'//13X,9(A1,10X),'10'//&
     &(10X,10(1X,E10.4)))
      PRINT 44
   44 FORMAT(/' ',131('*'))
!
! 
! ----------------
! ........ GJM - 9/19/2000
! ........ Begin addition for optimal numbering to minimize bandwidth 
! ----------------
! 
!
      IF(NX >= NY .AND. NX >= NZ) THEN
!
         NLXYZ(3)      = NX
         DLXYZ(1:NX,3) = DX(1:NX)
!
         IF(NY >= NZ) THEN
!
            NLXYZ(2)      = NY
            DLXYZ(1:NY,2) = DY(1:NY)
!
            NLXYZ(1)      = NZ
            DLXYZ(1:NZ,1) = DZ(1:NZ)
!
            NNNUUU = 1
!
            GO TO 1000
!
         ELSE
!
            NLXYZ(2)      = NZ
            DLXYZ(1:NZ,2) = DZ(1:NZ)
!
            NLXYZ(1)      = NY
            DLXYZ(1:NY,1) = DY(1:NY)
!
            NNNUUU = 2
!
            GO TO 1000
!
         END IF
!
      END IF
!
!
!
      IF(NY >= NX .AND. NY >= NZ) THEN
!
         NLXYZ(3)      = NY
         DLXYZ(1:NY,3) = DY(1:NY)
!
         IF(NX >= NZ) THEN
!
            NLXYZ(2)      = NX
            DLXYZ(1:NX,2) = DX(1:NX)
!
            NLXYZ(1)      = NZ
            DLXYZ(1:NZ,1) = DZ(1:NZ)
!
            NNNUUU = 3
!
            GO TO 1000
!
         ELSE
!
            NLXYZ(2)      = NZ
            DLXYZ(1:NZ,2) = DZ(1:NZ)
!
            NLXYZ(1)      = NX
            DLXYZ(1:NX,1) = DX(1:NX)
!
            NNNUUU = 4
!
            GO TO 1000
!
         END IF
!
      END IF
!
!
!
      IF(NZ >= NX .AND. NZ >= NY) THEN
!
         NLXYZ(3)      = NZ
         DLXYZ(1:NZ,3) = DZ(1:NZ)
!
         IF(NX >= NY) THEN
!
            NLXYZ(2)      = NX
            DLXYZ(1:NX,2) = DX(1:NX)
!
            NLXYZ(1)      = NY
            DLXYZ(1:NY,1) = DY(1:NY)
!
            NNNUUU = 5
!
            GO TO 1000
!
         ELSE
!
            NLXYZ(2)      = NY
            DLXYZ(1:NY,2) = DY(1:NY)
!
            NLXYZ(1)      = NX
            DLXYZ(1:NX,1) = DX(1:NX)
!
            NNNUUU = 6
!
         END IF
!
      END IF
!
!
!
 1000 IF((NNNUUU == 1) .OR. (NNNUUU == 3)) THEN
         RRR(1) =-1.0d0
      ELSE
         RRR(1) = 1.0d0
      END IF
!
      IF((NNNUUU == 2) .OR. (NNNUUU == 4)) THEN
         RRR(2) =-1.0d0
      ELSE
         RRR(2) = 1.0d0
      END IF
!
      IF((NNNUUU == 5) .OR. (NNNUUU == 6)) THEN 
         RRR(3) =-1.0d0
      ELSE
         RRR(3) = 1.0d0
      END IF
!
! 
! ----------------
! ........ GJM - 9/19/2000
! ........ End addition for optimal numbering to minimize bandwidth 
! ----------------
! 
!
      ILOOP = 0
   30 CONTINUE
      ILOOP = ILOOP+1
!
! 
! - GJM: Begin modification for 8-character elements
! 
      IF(No_CEN.EQ.8) THEN
         IF(ILOOP.EQ.1) WRITE(4,5020)
      ELSE
         IF(ILOOP.EQ.1) WRITE(4,20)
      END IF
      IF(ILOOP.EQ.2) WRITE(4,21)
! 
 5020 FORMAT('ELEMEext2')
   20 FORMAT('ELEME')
   21 FORMAT('CONNE')
! 
! - GJM: End modification for 8-character elements
! 
!
! 
! ----------------
! ........ GJM - 9/19/2000
! ........ Begin addition  
! ........   (a) for optimal numbering to minimize bandwidth 
! ........   (b) to introduce diffenet numbering/naming system to allow
! ........       the use of the MINC facility for large grids
! ----------------
! 
!
      XI = RRR(3)*DLXYZ(1,3)/2.0d0
!
      DO 1 I=1,NLXYZ(3)
! 
         I1    = I+1
         D1(1) = DLXYZ(I,3)/2.0d0
         IF(I.LT.NLXYZ(3)) D1(2) = DLXYZ(I1,3)/2.0d0
         IF(I.GT.1) XI = XI+RRR(3)*DLXYZ(I,3)/2.0d0&
     &                     +RRR(3)*DLXYZ(I-1,3)/2.0d0
! 
         YJ = RRR(2)*DLXYZ(1,2)/2.0d0
         DO 1 J=1,NLXYZ(2)
! 
            J1    = J+1
            D2(1) = DLXYZ(J,2)/2.0d0
            IF(J.LT.NLXYZ(2)) D2(2) = DLXYZ(J1,2)/2.0d0
            IF(J.GT.1) YJ = YJ+RRR(2)*DLXYZ(J,2)/2.0d0&
     &                        +RRR(2)*DLXYZ(J-1,2)/2.0d0
! 
            ZK = RRR(1)*DLXYZ(1,1)/2.0d0
            DO 1 K=1,NLXYZ(1)
! 
               K1    = K+1
               D3(1) = DLXYZ(K,1)/2.0d0
               IF(K.LT.NLXYZ(1)) D3(2) = DLXYZ(K1,1)/2.0d0
               IF(K.GT.1) ZK = ZK+RRR(1)*DLXYZ(K,1)/2.0d0&
     &                           +RRR(1)*DLXYZ(K-1,1)/2.0d0
! 
               IF(ILOOP.EQ.2) GOTO 31
! 
               V = DLXYZ(I,3)*DLXYZ(J,2)*DLXYZ(K,1)
! 
               AIJ = 0.0d0
! 
               IF((NNNUUU == 1) .OR. (NNNUUU == 3)) THEN
                 IF(K == 1)  AIJ = AIJ + DLXYZ(J,2)*DLXYZ(I,3)
                 IF(K == NZ) AIJ = AIJ + DLXYZ(J,2)*DLXYZ(I,3)
               END IF
! 
               IF((NNNUUU == 2) .OR. (NNNUUU == 4)) THEN
                 IF(J == 1)  AIJ = AIJ + DLXYZ(K,1)*DLXYZ(I,3)
                 IF(J == NZ) AIJ = AIJ + DLXYZ(K,1)*DLXYZ(I,3)
               END IF
! 
               IF((NNNUUU == 5) .OR. (NNNUUU == 6)) THEN
                 IF(I == 1)  AIJ = AIJ + DLXYZ(K,1)*DLXYZ(J,2)
                 IF(I == NZ) AIJ = AIJ + DLXYZ(K,1)*DLXYZ(J,2)
               END IF
! 
! 
! 
               SELECT CASE(NNNUUU)
               CASE(1)
                  DDXYZ(1) = XI
                  DDXYZ(2) = YJ
                  DDXYZ(3) = ZK
               CASE(2)
                  DDXYZ(1) = XI
                  DDXYZ(2) = ZK
                  DDXYZ(3) = YJ
               CASE(3)
                  DDXYZ(1) = YJ
                  DDXYZ(2) = XI
                  DDXYZ(3) = ZK
               CASE(4)
                  DDXYZ(1) = ZK
                  DDXYZ(2) = XI
                  DDXYZ(3) = YJ
               CASE(5)
                  DDXYZ(1) = YJ
                  DDXYZ(2) = ZK
                  DDXYZ(3) = XI
               CASE(6)
                  DDXYZ(1) = ZK
                  DDXYZ(2) = YJ
                  DDXYZ(3) = XI
               END SELECT
! 
! 
               ELNAME = ONOMA(NLXYZ(1),NLXYZ(2),NLXYZ(3),i,j,k)
! 
! - GJM: Begin modification for 8-character elements
! 
               IF(No_CEN.EQ.8) THEN
                  WRITE(4,5010) 'ZZZ',ELNAME,DOM,V,AIJ,&
     &                           DDXYZ(1),DDXYZ(2),DDXYZ(3)
               ELSE
                  WRITE(4,10) ELNAME,DOM,V,AIJ,&
     &                        DDXYZ(1),DDXYZ(2),DDXYZ(3)
               END IF
! 
! - GJM: End modification for 8-character elements
! 
               GO TO 1

   31          CONTINUE
! 
               A12 = DLXYZ(J,2)*DLXYZ(K,1)
               A13 = DLXYZ(I,3)*DLXYZ(K,1)
               A23 = DLXYZ(I,3)*DLXYZ(J,2)
! 
               ELNAME = ONOMA(NLXYZ(1),NLXYZ(2),NLXYZ(3),i,j,k)
! 
               IF(I.LT.NLXYZ(3)) THEN 
                  ELNAM1 = ONOMA(NLXYZ(1),NLXYZ(2),NLXYZ(3),i+1,j,k)
! 
! - GJM: Begin modification for 8-character elements
! 
                  IF(No_CEN.EQ.8) THEN
                     IF((NNNUUU == 1) .OR. (NNNUUU == 2)) WRITE(4,5011)& 
     &                          'ZZZ',ELNAME,'ZZZ',ELNAM1,&
     &                           NA(2),D1(1),D1(2),A12
                     IF((NNNUUU == 3) .OR. (NNNUUU == 4)) WRITE(4,5011) &
     &                          'ZZZ',ELNAME,'ZZZ',ELNAM1,&
     &                           NA(3),D1(1),D1(2),A12,BET
                     IF((NNNUUU == 5) .OR. (NNNUUU == 6)) WRITE(4,5011)  &
     &                          'ZZZ',ELNAME,'ZZZ',ELNAM1,&
     &                           NA(4),D1(1),D1(2),A12,BETA
                  ELSE
                     IF((NNNUUU == 1) .OR. (NNNUUU == 2)) WRITE(4,11) &
     &                 ELNAME,ELNAM1,NA(2),D1(1),D1(2),A12
                     IF((NNNUUU == 3) .OR. (NNNUUU == 4)) WRITE(4,11) &
     &                 ELNAME,ELNAM1,NA(3),D1(1),D1(2),A12,BET
                     IF((NNNUUU == 5) .OR. (NNNUUU == 6)) WRITE(4,11) &
     &                 ELNAME,ELNAM1,NA(4),D1(1),D1(2),A12,BETA
                  END IF
! 
! - GJM: End modification for 8-character elements
! 
               END IF
! 
               IF(J.LT.NLXYZ(2)) THEN 
! 
                  ELNAM1 = ONOMA(NLXYZ(1),NLXYZ(2),NLXYZ(3),i,j+1,k)
! 
! 
! - GJM: Begin modification for 8-character elements
! 
                  IF(No_CEN.EQ.8) THEN
                     IF((NNNUUU == 3) .OR. (NNNUUU == 5)) WRITE(4,5011) &
     &                          'ZZZ',ELNAME,'ZZZ',ELNAM1,&
     &                           NA(2),D2(1),D2(2),A13
                     IF((NNNUUU == 1) .OR. (NNNUUU == 6)) WRITE(4,5011)  &
     &                          'ZZZ',ELNAME,'ZZZ',ELNAM1,&
     &                           NA(3),D2(1),D2(2),A13,BET
                     IF((NNNUUU == 2) .OR. (NNNUUU == 4)) WRITE(4,5011)  &
     &                          'ZZZ',ELNAME,'ZZZ',ELNAM1,&
     &                           NA(4),D2(1),D2(2),A13,BETA
                  ELSE
                     IF((NNNUUU == 3) .OR. (NNNUUU == 5)) WRITE(4,11) &
     &                  ELNAME,ELNAM1,NA(2),D2(1),D2(2),A13
                     IF((NNNUUU == 1) .OR. (NNNUUU == 6)) WRITE(4,11) &
     &                  ELNAME,ELNAM1,NA(3),D2(1),D2(2),A13,BET
                     IF((NNNUUU == 2) .OR. (NNNUUU == 4)) WRITE(4,11) &
     &                  ELNAME,ELNAM1,NA(4),D2(1),D2(2),A13,BETA
                  END IF
! 
! - GJM: End modification for 8-character elements
! 
               END IF
!
               IF(K.LT.NLXYZ(1)) THEN 
! 
                  ELNAM1 = ONOMA(NLXYZ(1),NLXYZ(2),NLXYZ(3),i,j,k+1)
! 
! - GJM: Begin modification for 8-character elements
! 
                  IF(No_CEN.EQ.8) THEN
                     IF((NNNUUU == 4) .OR. (NNNUUU == 6)) WRITE(4,5011) &
     &                          'ZZZ',ELNAME,'ZZZ',ELNAM1,&
     &                           NA(2),D3(1),D3(2),A23
                     IF((NNNUUU == 2) .OR. (NNNUUU == 5)) WRITE(4,5011) &
     &                          'ZZZ',ELNAME,'ZZZ',ELNAM1,&
     &                           NA(3),D3(1),D3(2),A23,BET
                     IF((NNNUUU == 1) .OR. (NNNUUU == 3)) WRITE(4,5011) &
     &                          'ZZZ',ELNAME,'ZZZ',ELNAM1,&
     &                           NA(4),D3(1),D3(2),A23,BETA
                  ELSE
                     IF((NNNUUU == 4) .OR. (NNNUUU == 6)) WRITE(4,11) &
     &                  ELNAME,ELNAM1,NA(2),D3(1),D3(2),A
                     IF((NNNUUU == 2) .OR. (NNNUUU == 5)) WRITE(4,11) &
     &                  ELNAME,ELNAM1,NA(3),D3(1),D3(2),A23,BET
                     IF((NNNUUU == 1) .OR. (NNNUUU == 3)) WRITE(4,11) &
     &                  ELNAME,ELNAM1,NA(4),D3(1),D3(2),A23,BETA
                  END IF
! 
! - GJM: End modification for 8-character elements
! 
               END IF
!
    1 CONTINUE
 5010 FORMAT(A3,A5,7X,A5,2E10.4,10X,3F10.3)
   10 FORMAT(A5,10X,A5,2E10.4,10X,3E10.4)
 5011 FORMAT(2(A3,A5),13X,A1,4E10.4)
   11 FORMAT(2(A5),19X,A1,4E10.4)
!
! 
! ----------------
! ........ GJM - 9/19/2000
! ........ End addition  
! ........   (a) for optimal numbering to minimize bandwidth 
! ........   (b) to introduce diffenet numbering/naming system to allow
! ........       the use of the MINC facility for large grids
! ----------------
! 
!
      WRITE(4,22)
   22 FORMAT('     ')
      IF(ILOOP.LT.2) GOTO 30
!
      REWIND 4
      READ(4,50) DOM
! 
      L=0
! 
! - GJM: Begin modification for 8-character elements
! 
      IF(No_CEN == 8) THEN
         DO 56 I=1,NX
            DO 56 J=1,NY
               DO 56 K=1,NZ
                  L = L+1
                  READ(4,5050) elem(L)%name
   56    CONTINUE
      ELSE
         DO 51 I=1,NX
            DO 51 J=1,NY
               DO 51 K=1,NZ
                  L = L+1
                  READ(4,50) elem(L)%name(1:5)
   51    CONTINUE
      END IF
! 
 5050 FORMAT(A8)
   50 FORMAT(A5)
! 
! - GJM: End modification for 8-character elements
! 
      CALL PCAR(NLXYZ(3),NLXYZ(2),NLXYZ(1),NX,NY,NZ,NNNUUU)
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of GXYZ
!
!
      RETURN
      END
!
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
!
      SUBROUTINE PCAR(NX,NY,NZ,NXA,NYA,NZA,NNNUUU)
! 
         USE Basic_Param, ONLY: No_CEN
         USE GenControl_Param
! 
         USE Element_Arrays
! 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
! 
!
!-----MAKE STRUCTURED PRINTOUT OF X-Y-Z GRID.
!
      CHARACTER*1 L1,L2,L3
      CHARACTER*2 M1,M2,M3
! 
      DIMENSION L1(6),L2(6),L3(6),M1(6),M2(6),M3(6)
!
      SAVE ICALL,L1,L2,L3,M1,M2,M3
!
      DATA ICALL/0/
      DATA L1/'K','J','K','I','J','I'/
      DATA L2/'J','K','I','K','I','J'/
      DATA L3/'I','I','J','J','K','K'/
      DATA M1/'NZ','NY','NZ','NX','NY','NX'/
      DATA M2/'NY','NZ','NX','NZ','NX','NY'/
      DATA M3/'NX','NX','NY','NY','NZ','NZ'/
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=> Main body of PCAR
!
!

!
!     FIND LOCATION OF ELEMENT NAME WITH X-INDEX I, Y-INDEX J,
!     AND Z-INDEX K
!
      NLOC(I,J,K)=(I-1)*NYA*NZA+(J-1)*NZA+K
!
      ICALL=ICALL+1
      IF(ICALL.EQ.1) WRITE(11,899)
! 899 FORMAT(6X,'PCAR     1.0      25 MARCH     1991',6X,
  899 FORMAT('PCAR              2.0   19 September 2000',6X,&
     &       'MAKE STRUCTURED PRINTOUT OF CARTESIAN MESH')
!
!
!
      PRINT 31, NXA,NYA,NZA
   31 format(/,' ',131('*'),&
     &       /,' *',20X,'CARTESIAN MESH WITH NX*NY*NZ = ',&
     &         I4,' *',I4,' *',I4,'  GRID BLOCKS',49X,'*',&
     &       /,' ',131('*'))
!
      N1 = NZ
      N2 = NY
      N3 = NX
!
      PRINT 18,L1(NNNUUU),L1(NNNUUU),M1(NNNUUU),N1
   18 FORMAT(' *',129X,'*'/&
     &' *',20X,'THE MESH WILL BE PRINTED AS SLICES FOR ',A1,&
     &' = 1 TO ',A1,' = ',A2,' =',I4,49X,'*')
!
      PRINT 19,L2(NNNUUU),L2(NNNUUU),M2(NNNUUU),N2
   19 FORMAT(' *',129X,'*'/&
     &' *',20X,'IN EACH MESH SLICE, ROWS WILL GO FROM  ',A1,&
     &' = 1 TO ',A1,' = ',A2,' =',I4,49X,'*')
!
      PRINT 20,L3(NNNUUU),L3(NNNUUU),M3(NNNUUU),N3
   20 FORMAT(' *',129X,'*'/&
     &' *',20X,'IN EACH ROW, COLUMNS WILL GO FROM      ',A1,&
     &' = 1 TO ',A1,' = ',A2,' =',I4,49X,'*'/' *',129X,'*')
!
! 
! - GJM: Begin modification for 8-character elements
!    MBK: Modifications begin here (Oct 4, 2004) 
! 
      IF(No_CEN.EQ.8) THEN
         DO 102 K=1,N1
!            
            PRINT 3,L1(NNNUUU),K
            PRINT 5004,L3(NNNUUU)
            DO 105 J=1,N2
! 
               GOTO(121,122,123,124,125,126),NNNUUU
! 
  121          PRINT 5006,L2(NNNUUU),J,(elem(NLOC(I,J,K))%name,I=1,N3)
               GOTO 105
! 
  122          PRINT 5006,L2(NNNUUU),J,(elem(NLOC(I,K,J))%name,I=1,N3)
               GOTO 105
! 
  123          PRINT 5006,L2(NNNUUU),J,(elem(NLOC(J,I,K))%name,I=1,N3)
               GOTO 105
! 
  124          PRINT 5006,L2(NNNUUU),J,(elem(NLOC(K,I,J))%name,I=1,N3)
               GOTO 105
! 
  125          PRINT 5006,L2(NNNUUU),J,(elem(NLOC(J,K,I))%name,I=1,N3)
               GOTO 105
! 
  126          PRINT 5006,L2(NNNUUU),J,(elem(NLOC(K,J,I))%name,I=1,N3)
               GOTO 105
! 
  105       CONTINUE
  102    CONTINUE
      ELSE
         DO 2 K=1,N1
            PRINT 3,L1(NNNUUU),K
            PRINT 4,L3(NNNUUU)
            DO 5 J=1,N2
! 
               GOTO(21,22,23,24,25,26),NNNUUU
! 
   21          PRINT 6,L2(NNNUUU),J,(ELEM(NLOC(I,J,K))%name(1:5),I=1,N3)
               GOTO 5
   22          PRINT 6,L2(NNNUUU),J,(ELEM(NLOC(I,K,J))%name(1:5),I=1,N3)
               GOTO 5
   23          PRINT 6,L2(NNNUUU),J,(ELEM(NLOC(J,I,K))%name(1:5),I=1,N3)
               GOTO 5
   24          PRINT 6,L2(NNNUUU),J,(ELEM(NLOC(K,I,J))%name(1:5),I=1,N3)
               GOTO 5
   25          PRINT 6,L2(NNNUUU),J,(ELEM(NLOC(J,K,I))%name(1:5),I=1,N3)
               GOTO 5
   26          PRINT 6,L2(NNNUUU),J,(ELEM(NLOC(K,J,I))%name(1:5),I=1,N3)
               GOTO 5
    5       CONTINUE
    2    CONTINUE
      END IF
!
!    MBK: Modifications end here (Oct 4, 2004)
! 
    3 FORMAT(' ',131('*')//' SLICE WITH ',A1,' =',I4/)
! 
 5004 FORMAT('   COLUMN ',A1,' =    1        2        3        4',&
     &       '        5        6        7        8        9       10',&
     &       '       11       12'/' LAYERS')
    4 FORMAT('   COLUMN ',A1,' =  1     2     3     4     5     6     ',&
     &       '7     8     9    10    11    12    13    14    15    ',&
     &       '16    17    18    19    20'/' ROWS')
 5006 FORMAT('  ',A1,' = ',I4,2X,12(1X,A8),/,(12X,12(1X,A8)),/,&
     &                     (12X,12(1X,A8)),/,(12X,12(1X,A8)))
    6 FORMAT('  ',A1,' = ',I4,2X,20(1X,A5)/(12X,20(1X,A5)))
! 
! - GJM: End modification for 8-character elements
! 
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of PCAR
!
!
      RETURN
      END
!
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
!
      SUBROUTINE MIN!
!
!-----THIS IS A MODIFIED AND ENHANCED VERSION OF PROGRAM GMINC ---
!
!-----THIS VERSION CAN ASSIGN VERTICAL CONNECTIONS BETWEEN MATRIX
!     CONTINUA (11-19-84).
!     FURTHER MODIFICATIONS (5-26-88):
!     (1) INCLUDE ELEMENT X,Y,Z-COORDINATES
!     (2) ELIMINATE "BOUNDARY ELEMENTS" FROM MINC-PROCEDURE
!
!***** GMINC WAS DEVELOPED BY KARSTEN PRUESS
!                          AT LAWRENCE BERKELEY LABORATORY. ************
!
!     THE PROGRAM GENERATES ONE-, TWO-, OR THREE-DIMENSIONAL MESHES
!     FOR FLOW SIMULATIONS IN FRACTURED POROUS MEDIA.
!
!     GMINC IMPLEMENTS THE METHOD OF
!                  MULTIPLE INTERACTING CONTINUA (MINC)
!     AS DEVELOPED BY PRUESS AND NARASIMHAN.
!
!     REFERENCES:
!
!     (1) K. PRUESS AND T.N. NARASIMHAN, A PRACTICAL METHOD FOR
!         MODELING FLUID AND HEAT FLOW IN FRACTURED POROUS MEDIA,
!         PAPER SPE-10509, PRESENTED AT THE SIXTH SPE-SYMPOSIUM ON
!         RESERVOIR SIMULATION, NEW ORLEANS, LA; (FEBRUARY 1982);
!         ALSO: SOCIETY OF PETROLEUM ENGINEERS JOURNAL, VOL. 25, NO.1,
!               PP. 14-26, 1985.
!
!     (2) K. PRUESS AND T.N. NARASIMHAN, ON FLUID RESERVES AND THE
!         PRODUCTION OF SUPERHEATED STEAM FROM FRACTURED, VAPOR-
!         DOMINATED GEOTHERMAL RESERVOIRS, J. GEOPHYS. RES. 87 (B11),
!         9329-9339, 1982.
!
!     (3) K. PRUESS AND K. KARASAKI, PROXIMITY FUNCTIONS FOR MODELING
!         FLUID AND HEAT FLOW IN RESERVOIRS WITH STOCHASTIC FRACTURE
!         DISTRIBUTIONS, PAPER PRESENTED AT EIGTH STANFORD WORKSHOP
!         ON GEOTHERMAL RESERVOIR ENGINEERING, STANFORD, CA.
!         (DECEMBER 1982).
!
!     (4) K. PRUESS, GMINC - A MESH GENERATOR FOR FLOW SIMULATIONS IN
!         FRACTURED RESERVOIRS, LAWRENCE BERKELEY LABORATORY REPORT
!         LBL-15227, 1983.
!
!***********************************************************************
!
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
!
      SAVE ICALL
      DATA ICALL/0/
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=> Main body of MIN!
!
!
      ICALL=ICALL+1
      IF(ICALL.EQ.1) WRITE(11,899)
  899 FORMAT(/,'MINC              1.0   22 JANUARY   1990',6X,&
     &         'EXECUTIVE ROUTINE FOR MAKING A "SECONDARY" ',&
     &         'FRACTURED-POROUS MEDIUM MESH')
!
!-----READ DATA ON MINC-PARTITIONING.
!
      CALL PART
!
!!!!! CALL TO GEOM INSERTED 11-19-84, TO OBTAIN ALL VOL(M) BEFORE
!     PROCESSING CONNECTIONS.
      CALL GEOM
!
!-----READ ELEMENT DATA FROM FILE *MESH* AND PROCESS SEQUENTIALLY.
      CALL MINCME
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of MIN!
!
!
      RETURN
      END
!
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
!
      SUBROUTINE PART
! 
         USE Basic_Param
         USE GenControl_Param
! 
         USE Element_Arrays
! 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
! 
      CHARACTER HB*1,WHE_x*4,TYPOS*5,NA*1,WORD*5
      CHARACTER DUAL*5
! 
      COMMON/MINCD_I/J,NVOL
      COMMON/MINCD_R/VOL(200),A(200),D(200)
      COMMON/PROXI_I/L
      COMMON/PROXI_R/PAR(7)
      COMMON/CH/WHE_x,TYPOS(6)
      COMMON/N/NA(36)
! 
      DIMENSION WORD(16),DUAL(2)
! 
      SAVE ICALL,HB,DUAL
      DATA HB/' '/
      DATA DUAL/'MMVER','MMALL'/
      DATA ICALL/0/
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=> Main body of PART
!
!
      ICALL=ICALL+1
      IF(ICALL.EQ.1) WRITE(11,899)
  899 FORMAT('PART              1.0   22 JANUARY   1990',6X,&
     &       'READ SPECIFICATIONS OF MINC-PARTITIONING FROM ',&
     &       'FILE *INPUT*')
!
      READ (100,5020),(WORD(I),I=1,16)
 5020 FORMAT(16A5)
      IF(WORD(1).EQ.'PART ') GOTO 920
!
      PRINT 901,WORD(1)
  901 FORMAT(' HAVE READ UNKNOWN BLOCK LABEL "',A5,'" IN MINC MODULE',&
     X' --- STOP EXECUTION ---')
      STOP
!
  920 CONTINUE
!
!****READ DATA FOR MULTIPLE INTERACTING CONTINUA.***********************
!
!***** *J* IS THE NUMBER OF MULTIPLE INTERACTING CONTINUA.
!*
!***** *NVOL* (.LE.J) IS THE NUMBER OF EXPLICITLY SPECIFIED VOLUME
!*     FRACTIONS.
!*
!***** *WHE_x* SPECIFIES WHETHER EXPLICITLY PROVIDED VOLUME FRACTIONS
!      ARE GIVEN STARTING AT THE OUTSIDE (FRACTURE) OR INSIDE (MATRIX).
!*
!***** *PAR* IS AN ARRAY WITH PARAMETERS FOR SPECIFYING FRACTURE
!*     DISTRIBUTIONS.
!
      DO902 L=1,6
      IF(WORD(2).EQ.TYPOS(L)) GOTO 903
  902 CONTINUE
      PRINT 904,WORD(2)
  904 FORMAT(' HAVE READ UNKNOWN PROXIMITY FUNCTION IDENTIFIER  *',A5,&
     &       '* ---   STOP EXECUTION')
      STOP
!
  903 CONTINUE
!-----INDEX *L* LABELS THE TYPE OF PROXIMITY FUNCTION SELECTED.
!
!
      READ (100,1),J,NVOL,WHE_x,(PAR(I),I=1,7)
    1 FORMAT(2I3,A4,7E10.4)
!
!-----READ A SET OF VOLUME FRACTIONS-----
      IF(WHE_x.EQ.'OUT ') READ (100,2),(VOL(M),M=1,NVOL)
      IF(WHE_x.EQ.'IN  ') READ (100,2),(VOL(J+1-M),M=1,NVOL)
    2 FORMAT(8E10.4)
!
!----- END OF MINC-DATA ------------------------------------------------
!
      IF((L.EQ.2 .OR. L.EQ.3) .AND. PAR(2).EQ.0.) PAR(2)=PAR(1)
      IF(L.EQ.3 .AND. PAR(3).EQ.0.) PAR(3)=PAR(2)
!
!-----IDENTIFY CHOICE MADE FOR GLOBAL MATRIX-MATRIX FLOW.
!
      DO802 MM=1,2
      IF(WORD(4).EQ.DUAL(MM)) GOTO 803
  802 CONTINUE
      MM=0
!
  803 CONTINUE
      PRINT 801,WORD(4)
  801 FORMAT(/' CHOICE OF MATRIX-MATRIX FLOW HANDLING: "',A5,'"')
      PRINT 804
  804 FORMAT(/'                       THE OPTIONS ARE: "     "',&
     X' (DEFAULT), NO GLOBAL MATRIX-MATRIX FLOW; GLOBAL FLOW ONLY',&
     X' THROUGH FRACTURES'/&
     X40X,'"MMVER", GLOBAL MATRIX-MATRIX FLOW IN VERTICAL DIRECTION',&
     X' ONLY'/&
     X40X,'"MMALL", GLOBAL MATRIX-MATRIX FLOW IN ALL DIRECTIONS'/)
      IF(MM.EQ.2.AND.J.NE.2) PRINT 806
  806 FORMAT(&
     X' !!!!! WARNING !!!!! THE "MMALL" OPTION SHOULD ONLY',&
     X' BE USED WITH TWO CONTINUA, WHERE IT AMOUNTS TO A DUAL-',&
     X'PERMEABILITY TREATMENT')
!
  805 CONTINUE
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of PART
!
!
      RETURN
      END
!
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
!
      SUBROUTINE GEOM
! 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
! 
      CHARACTER WHE_x*4,TYPOS*5,NA*1
! 
      DIMENSION X(200)
! 
      COMMON/MINCD_I/J,NVOL
      COMMON/MINCD_R/VOL(200),A(200),D(200)
      COMMON/PROXI_I/L
      COMMON/PROXI_R/PAR(7)
      COMMON/CH/WHE_x,TYPOS(6)
      COMMON/N/NA(36)
! 
      SAVE ICALL,DELTA
      DATA DELTA/1.E-8/
      DATA ICALL/0/
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=> Main body of GEOM
!
!
      ICALL=ICALL+1
      IF(ICALL.EQ.1) WRITE(11,899)
  899 FORMAT('GEOM              1.0    1 MAY       1991',6X,&
     &       'CALCULATE GEOMETRY PARAMETERS OF SECONDARY ',&
     &       '(FRACTURED-POROUS) MESH')
!
      IF(NVOL.GE.J) GOTO3
!
!-----COME HERE TO ASSIGN EQUAL VOLUMINA TO SUBDIVISIONS WHICH HAVE NOT
!     BEEN EXPLICITLY SPECIFIED-----
!
      VEX=0.0d0
! 
! ----------------
! ........ GJM - 9/19/2000
! ........ Begin correction 
! ........ Avoid problem of stalling when WHE_x does not have the desired value
! ----------------
! 
      DO 4 M=1,NVOL
         IF(WHE_x.EQ.'OUT ') THEN
            VEX=VEX+VOL(M)
         ELSE IF(WHE_x.EQ.'IN  ') THEN
            VEX=VEX+VOL(J+1-M)
         ELSE IF(WHE_x.EQ.'IN  ') THEN
            PRINT 8100, WHE_x
            STOP
         END IF
    4 CONTINUE
 8100 FORMAT(//,20('ERROR-'),//,T33,&
     &             '       S I M U L A T I O N   A B O R T E D',/,&
     &         T31,'The parameter WHE_x = ',a4,&
     &             ' is neither *OUT * nor *IN  *',&
     &       /,T33,'               CORRECT AND TRY AGAIN',     &
     &       //,20('ERROR-'))
! 
! ----------------
! ........ GJM - 9/19/2000
! ........ End correction 
! ----------------
! 
!     VEX IS THE TOTAL EXPLICITLY ASSIGNED VOLUME FRACTION.
!
      IF(VEX.GT.1.) GO TO10
      IF(VEX.EQ.1.) GO TO 3
!
      VF=(1.-VEX)/FLOAT(J-NVOL)
!-----VF IS THE VOLUME FRACTION FOR PARTITIONS WHICH ARE NOT
!     EXPLICITLY ASSIGNED.
      NVOL1=NVOL+1
      DO 5 M=NVOL1,J
         IF(WHE_x.EQ.'OUT ') THEN
            VOL(M)=VF
         ELSE
            VOL(J+1-M)=VF
         END IF
    5 CONTINUE
      GO TO 3
!
   10 CONTINUE
!-----COME HERE IF EXPLICITLY ASSIGNED VOLUMINA EXCEED 100%-----
      PRINT 11,VEX
   11 FORMAT(' PROGRAM STOPS BECAUSE TOTAL VOLUME VEX = ',&
     &       E12.5,' > 100%  ---  NEED TO CORRECT INPUT DATA')
      STOP
!
    3 CONTINUE
!
!-----NOW FIND DISTANCES FROM FRACTURES WHICH CORRESPOND TO
!     DESIRED VOLUME FRACTIONS.
!     INDEXING STARTS AT THE OUTSIDE; I.E. *1* IS THE OUTERMOST
!     VOLUME ELEMENT, AND *J* IS THE INNERMOST ONE.
!
!-----INITIALIZE TOTAL VOLUME FRACTION.
      TVOL=0.
!
!-----FIRST INTERFACE WILL BE AT FRACTURE FACE.
      X(1)=0.0d0
      D(1)=0.0d0
      A(1)=(1.0d0-VOL(1))*PROX(1.d-10)/1.d-10
!
!
!-----INITIALIZE SEARCH INTERVAL.
!
      XL = 0.0d0
      XR = VOL(2)/A(1)
!
!
      DO 30 M=2,J
!
!-----COMPUTE TOTAL FRACTION OF MATRIX VOLUME.
         TVOL = TVOL+VOL(M)/(1.-VOL(1))
         IF(M.EQ.J) TVOL = 1.-1.d-9
! 
         CALL INVER(TVOL,XMID,XL,XR)
! 
         X(M)=XMID
! 
         XMD  = XMID*DELTA
         A(M) = (1.d0-VOL(1))*(PROX(XMID+XMD)-PROX(XMID-XMD))&
     &                       /(2.0d0*XMD)
!
         D(M) = (X(M)-X(M-1))/2.0d0
!
!-----PUT LEFT END OF NEXT ITERATION INTERVAL AT PRESENT X.
         XL = XMID
!
   30 CONTINUE
!
!
!-----COME HERE TO COMPUTE A QUASI-STEADY VALUE FOR INNERMOST
!     NODAL DISTANCE.
!
      GOTO (41,42,43,44,45,46,47,48,49,50),L
!
   41 CONTINUE
!----- ONE-D CASE.
      D(J) = (PAR(1)-2.0d0*X(J-1))/6.
      GO TO 40
!
   42 CONTINUE
!----- TWO-D CASE.
      U    = PAR(1)-2.0d0*X(J-1)
      V    = PAR(2)-2.0d0*X(J-1)
      D(J) = U*V/(4.0d0*(U+V))
      GO TO 40
!
   43 CONTINUE
!----- THRED CASE.
      U    = PAR(1)-2.0d0*X(J-1)
      V    = PAR(2)-2.0d0*X(J-1)
      W    = PAR(3)-2.0d0*X(J-1)
      D(J) = 3.0d0*U*V*W/(1.0d1*(U*V+V*W+U*W))
      GO TO 40
!
   44 CONTINUE
   45 CONTINUE
   46 CONTINUE
   47 CONTINUE
   48 CONTINUE
   49 CONTINUE
   50 CONTINUE
      D(J) = (X(J)-X(J-1))/5.0d0
!
   40 CONTINUE
!
!-----PRINT OUT GEOMETRY DATA.
!
      PRINT 27
      PRINT 23
      PRINT 24
      PRINT 25,VOL(1),D(1)
      PRINT 26,A(1),X(1)
!
   23 FORMAT('  CONTINUUM     IDENTIFIER       VOLUME      NODAL ',&
     &       'DISTANCE      INTERFACE AREA   INTERFACE DISTANCE')
   24 FORMAT(84X,'FROM FRACTURES',/)
   25 FORMAT('  1-FRACTURES      * *    ',2(4X,E12.5))
   26 FORMAT(66X,E12.5,7X,E12.5)
   27 FORMAT(//' ==================== GEOMETRY DATA, NORMALIZED TO',&
     &         ' A DOMAIN OF UNIT VOLUME =========================',//)
!
      DO 100 M=2,J
         PRINT 101,M,NA(M),VOL(M),D(M)
         IF(M.NE.J) PRINT 102,A(M),X(M)
  100 CONTINUE
  101 FORMAT(' ',I2,'-MATRIX',9X,'*',A1,'*',8X,E12.5,4X,E12.5)
  102 FORMAT(66X,E12.5,7X,E12.5)
!
      PRINT 103
  103 FORMAT(/,1X,99('='))
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of GEOM
!
!
      RETURN
      END
!
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
!
      FUNCTION PROX(X)
!
!-----THE PROXIMITY FUNCTION PROX(X) REPRESENTS THE FRACTION OF
!     MATRIX VOLUME [VM=(1.-VOL(1))*V0 WITHIN A DOMAIN V0] WHICH
!     IS WITHIN A DISTANCE X FROM THE FRACTURES.
!
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
!
      COMMON/PROXI_I/L
      COMMON/PROXI_R/PAR(7)
      SAVE ICALL,A,B,C,D
!     NOW ASSIGN DATA FOR STANFORD LARGE RESERVOIR MODEL.
      DATA A,B,C,D/.263398,.190754,.2032,.191262/
      DATA ICALL/0/
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=> Main body of PROX
!
!
      ICALL=ICALL+1
      IF(ICALL.EQ.1) WRITE(11,899)
  899 FORMAT('PROX              1.0   22 JANUARY   1990',6X,&
     &       'CALCULATE PROXIMITY FUNCTIONS FOR DIFFERENT ',&
     &       'MATRIX BLOCK SHAPES')
!
      GOTO(1,2,3,4,4,4,1,1,1,1),L
!
    1 CONTINUE
!
!----- ONE-D CASE.
!
      PROX=2.*X/PAR(1)
      IF(X.GE.PAR(1)/2.) PROX=1.
      RETURN
    2 CONTINUE
!
!----- TWO-D CASE.
!     THE MATRIX BLOCKS HAVE THICKNESS OF PAR(1) AND PAR(2),
!     RESPECTIVELY, MEASURED PERPENDICULAR TO THE FRACTURES.
!     THE PROXIMITY FUNCTION IS VALID FOR ARBITRARY ANGLE
!     BETWEEN THE FRACTURE SETS.
!
      PROX = 2.*(PAR(1)+PAR(2))*X/(PAR(1)*PAR(2))&
     &      -4.*X*X/(PAR(1)*PAR(2))
      IF(X.GE.PAR(1)/2.0.OR. X.GE.PAR(2)/2.) PROX=1.
      RETURN
    3 CONTINUE
!
!----- THREE DIMENSIONAL CASE.
!
      U    = 2.*X/PAR(1)
      V    = 2.*X/PAR(2)
      W    = 2.*X/PAR(3)
      PROX = U*V*W-(U*V+U*W+V*W)+U+V+W
      IF(U.GE.1. .OR. V.GE.1. .OR. W.GE.1.) PROX=1.
      RETURN
    4 CONTINUE
!
!
!***** MATRIX OF STANFORD LARGE RESERVOIR MODEL *****
!
!
!     RECTANGULAR BLOCKS IN LAYERS B1,B2,M1,M2,T1.
      VR=8.*X**3-(8.*B+4.*A)*X**2+(4.*A*B+2.*B**2)*X
      IF(X.GE.B/2.) VR=A*B*B
!
!     TRIANGULAR BLOCKS IN LAYERS B1,B2,M1,M2,T1.
      VT =(6.+4.*SQRT(2.))*X**3&
     &    -(A*(6.+4.*SQRT(2.))/2.&
     &    +2.*B*(2.+SQRT(2.)))*X**2&
     &    +(A*B*(2.+SQRT(2.))+B*B)*X
      IF(X.GE.B/(2.+SQRT(2.))) VT=A*B*B/2.
!
!     RECTANGULAR BLOCKS IN LAYER T2.
      VRT2 = 8.*X**3-(8.*D+4.*C)*X**2+(4.*C*D+2.*D**2)*X
      IF(X.GE.D/2.) VRT2=C*D*D
!
!     TRIANGULAR BLOCKS IN LAYER T2.
      VTT2 =(6.+4.*SQRT(2.))*X**3&
     &      -(C*(6.+4.*SQRT(2.))/2.&
     &      +2.*D*(2.+SQRT(2.)))*X**2&
     &      +(C*D*(2.+SQRT(2.))+D*D)*X
      IF(X.GE.D/(2.+SQRT(2.))) VTT2=C*D*D/2.
!
      IF(L.EQ.4) GOTO 14
      IF(L.EQ.5) GOTO 15
      IF(L.EQ.6) GOTO 16
!
!***** NOW COMPUTE TOTAL MATRIX VOLUME WITHIN DISTANCE X.
   14 V = 5.*(5.*VR+4.*VT)+5.*VRT2+4.*VTT2
!
!-----AVERAGE PROXIMITY FUNCTION FOR ENTIRE ROCK LOADING.
!
      VTOT =35.*A*B**2+7.*C*D**2
!     VOLUME FRACTION.
      PROX = V/VTOT
      RETURN
!
   15 PROX = (5.*VR+4.*VT)/(7.*A*B*B)
!-----PROXIMITY FUNCTION FOR FIVE BOTTOM LAYERS.
      RETURN
!
   16 PROX = (5.*VRT2+4.*VTT2)/(7.*C*D*D)
!-----PROXIMITY FUNCTION FOR TOP LAYER.
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of PROX
!
!
      RETURN
!
!
      END
!
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
!
      SUBROUTINE INVER(F,X,XL,XR)
!
!===== THIS ROUTINE INVERTS THE PROXIMITY FUNCTION, TO GIVE A
!     DISTANCE *X* FROM FRACTURE FACES FOR A DESIRED FRACTION *F* OF
!     MATRIX VOLUME.
!
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
!
      SAVE ICALL,TOL
      DATA TOL/1.E-10/
      DATA ICALL/0/
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=> Main body of INVER
!
!
      ICALL=ICALL+1
      IF(ICALL.EQ.1) WRITE(11,899)
  899 FORMAT('INVER             1.0   22 JANUARY   1990',6X,&
     &       'INVERT A MONOTONIC FUNCTION THROUGH NESTED BISECTIONS')
!
!-----CHECK AND ADJUST UPPER LIMIT OF SEARCH INTERVAL.
!
   22 FR=PROX(XR)
      IF(FR.GT.F) GO TO 20
      XR=2.0d0*XR
      GO TO 22
!
!-----PERFORM ITERATIVE BISECTING, TO OBTAIN A SEQUENCE OF NESTED
!     INTERVALS CONTAINING THE DESIRED POINT, X.
!
   20 XMID=(XR+XL)/2.
      IF(XR-XL.LE.TOL*XR) GO TO 21
      FMID=PROX(XMID)
      IF(FMID.LE.F) XL=XMID
      IF(FMID.GE.F) XR=XMID
      GOTO 20
!
   21 CONTINUE
!
!-----COME HERE FOR CONVERGENCE.
!
      X = XMID
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of INVER
!
!
      RETURN
      END
!
!
!
!***************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***************************************************************************
!
!
!
      SUBROUTINE MINCME
! 
         USE Basic_Param
         USE GenControl_Param
! 
         USE Element_Arrays
         USE Connection_Arrays
! 
         IMPLICIT DOUBLE PRECISION (A-H,O-Z)
!
!=====THIS ROUTINE WORKS SEQUENTIALLY THROUGH THE ELEMENTS OF THE
!     PRIMARY MESH, ASSIGNING ALL SECONDARY ELEMENTS AND INTRA-BLOCK
!     CONNECTIONS.
!
! 
      CHARACTER HB*1,WHE_x*4,TYPOS*5,NA*1,DENT*5
      CHARACTER EL1*1,EL2*4,EM1*1,EM2*4,EL10*1
      CHARACTER ELE*5,EC1*5,EC2*5,ELREF*5
! 
      COMMON/MINCD_I/J,NVOL
      COMMON/MINCD_R/VOL(200),A(200),D(200)
      COMMON/PROXI_I/L
      COMMON/PROXI_R/PAR(7)
      COMMON/NP/NELP,NELAP,NCONP
      COMMON/CH/WHE_x,TYPOS(6)
      COMMON/N/NA(36)
! 
      DIMENSION DENT(16)
! 
! - GJM: Begin modification for 8-character elements
! 
      CHARACTER ELE_8*8,EREF_8*8,EC1_8*8,EC2_8*8
      CHARACTER EL1_3*3,EL2_5*5,EM1_3*3,EM2_5*5
! 
      CHARACTER MESHX*4
! 
! - GJM: End modification for 8-character elements
! 
      SAVE ICALL,HB
      DATA HB/' '/
      DATA ICALL/0/
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=> Main body of MINCME
!
!
      ICALL=ICALL+1
      IF(ICALL.EQ.1) WRITE(11,899)
  899 FORMAT('MINCME            1.0   14 FEBRUARY  1990',6X,&
     &       'PROCESS PRIMARY MESH FROM FILE *MESH*, AND ',&
     &       'WRITE SECONDARY MESH ON FILE *MINC*')
!
      REWIND 4
      REWIND 10
!
      PRINT 2
    2 FORMAT(/' READ PRIMARY MESH FROM FILE *MESH*')
!
!*****READ ELEMENT DATA FROM FILE *MESH*.*******************************
!
      READ(4,1) (DENT(I),I=1,16)
    1 FORMAT(16A5)
      IF(DENT(1).NE.'ELEME') THEN
         PRINT 4,DENT(1)
         STOP
      END IF
!
    4 FORMAT(' HAVE READ UNKNOWN BLOCK LABEL "',A5,'" ON FILE *MESH*',&
     &       ' --- STOP EXECUTION ---')
! 
! - GJM: Begin modification for 8-character elements
! 
      IF(DENT(2)(1:4).EQ.'ext2') THEN
         IF(No_CEN.NE.8) THEN
            PRINT 6000, MESHX,No_CEN
            STOP
         END IF
         MESHX = 'ext2'
         WRITE(10,5006)
      ELSE
         IF(No_CEN.NE.5) THEN
            PRINT 6000, MESHX,No_CEN
            STOP
         END IF
         WRITE(10,6)
      END IF
!
 5006 FORMAT('ELEMEext2')
    6 FORMAT('ELEME')
!
 6000 FORMAT(//,20('ERROR-'),//,T33,&
     &             '       S I M U L A T I O N   A B O R T E D',/,&
     &         T6,'The mesh type inicator MESHX = ',a4,&
     &            ' is in conflict with the # of characters in the',&
     &            ' element name No_CEN = ',i1,&
     &       /,T32,'               CORRECT AND TRY AGAIN',     &
     &       //,20('ERROR-'))
! 
! - GJM: End modification for 8-character elements
! 
!
      NEL  = 0
      NELA = 0
    9 CONTINUE
! 
! - GJM: Begin modification for 8-character elements
! 
      IF(MESHX.EQ.'ext2') THEN
         READ(4,5010) EL1_3,EL2_5,MA,VOLX,AHTX,X,Y,Z
         IF(EL1_3.EQ.'   '.AND.EL2_5.EQ.'     ') GO TO 40
      ELSE
         READ(4,10) EL1,EL2,MA,VOLX,AHTX,X,Y,Z
         IF(EL1.EQ.' '.AND.EL2.EQ.'    ') GO TO 40
      END IF
! 
 5010 FORMAT(A3,A5, 7X,I5,E10.4,E10.4,10X,3F10.3)
   10 FORMAT(A1,A4,10X,I5,E10.4,E10.4,10X,3E10.4)
! 
! - GJM: End modification for 8-character elements
! 
         NEL = NEL+1
         IF(VOLX.LE.0..AND.NELA.EQ.0) NELA = NEL-1
         IF(NEL.GT.NELA.AND.NELA.NE.0) GO TO 8
!
!-----COME HERE FOR ACTIVE ELEMENTS
!
         V   = VOL(1)*VOLX
!         MA1 = MA+1
         MA1 = (MA-1)*3+2
         AH  = AHTX
         IF(MM.EQ.1) AH = VOL(1)*AHTX
! 
! - GJM: Begin modification for 8-character elements
! 
         IF(MESHX.EQ.'ext2') THEN
            ELE_8 = 'F00'//EL2_5
            WRITE(10,5007) ELE_8,MA1,V,AH,X,Y,Z
         ELSE
            ELE = HB//EL2
            WRITE(10,7) ELE,MA1,V,AH,X,Y,Z
         END IF
! 
 5007 FORMAT(A8, 7X,I5,2E10.4,10X,3F10.3)
    7 FORMAT(A5,10X,I5,2E10.4,10X,3E10.4)
! 
! - GJM: End modification for 8-character elements
! 
! 
!     STORE FRACTURE ELEMENT NAME FOR LATER CROSS REFERENCING
! 
! ----------------
! ........ GJM - 9/19/2000
! ........ Begin correction 
! ........ Use ELEM1 and ELEM2 to avoid augmenting the array size
! ----------------
! 
         IF(MESHX == 'ext2') THEN
            elem(NEL)%name = ELE_8
         ELSE
            IF(NEL <= MNEL) THEN
               elem(NEL)%name = ELE
            ELSE IF((NEL > MNEL) .AND. (NEL <= (2*MNEL))) THEN
               elem(NEL-MNEL)%name = ELE
            ELSE IF((NEL > (2*MNEL)) .AND. (NEL <= (3*MNEL))) THEN
               elem(NEL-2*MNEL)%name = ELE
            ELSE
               PRINT 8100
               STOP
            END IF
         END IF
! 
! ----------------
! ........ GJM - 9/19/2000
! ........ End correction 
! ----------------
! 
!-----FOR EACH PRIMARY ELEMENT, ASSIGN *J* SECONDARY ELEMENTS.
! 
! 
! - GJM: Begin modification for 8-character elements
! 
         IF(MESHX.EQ.'ext2') THEN
            DO M=2,J
               V   = VOL(M)*VOLX
!               MA2 = MA+2
               MA2 = (MA-1)*3+3
               AH  = 0.
               IF(MM.EQ.1) AH = VOL(M)*AHTX
               IF(J.LT.11) THEN
                  WRITE(10,5017) 'M0',M-1,EL2_5,MA2,V,AH,X,Y,Z
               ELSE
                  WRITE(10,5018) 'M',M-1,EL2_5,MA2,V,AH,X,Y,Z
               END IF
            END DO
         ELSE
            DO M=2,J
               ELE = NA(M)//EL2
               V   = VOL(M)*VOLX
!               MA2 = MA+2
               MA2 = (MA-1)*3+3
               AH  = 0.
               IF(MM.EQ.1) AH = VOL(M)*AHTX
               WRITE(10,7) ELE,MA2,V,AH,X,Y,Z
            END DO
         END IF
! 
 5017 FORMAT(A2,I1,A5,7X,I5,2E10.4,10X,3F10.3)
 5018 FORMAT(A1,I2,A5,7X,I5,2E10.4,10X,3F10.3)
! 
! - GJM: End modification for 8-character elements
! 
!
         GO TO 9
    8    CONTINUE
!-----COME HERE FOR INACTIVE ELEMENTS. THEY WILL NOT BE SUBJECTED TO
!     THE MINC PROCEDURE, BUT WILL BE HANDLED AS A SINGLE CONTINUUM.
! 
! - GJM: Begin modification for 8-character elements
! 
         IF(MESHX.EQ.'ext2') THEN
            ELE_8 = EL1_3//EL2_5
            WRITE(10,5007) ELE_8,MA,VOLX,AHTX,X,Y,Z
         ELSE
            ELE = EL1//EL2
            WRITE(10,7) ELE,MA,VOLX,AHTX,X,Y,Z
         END IF
! 
! - GJM: End modification for 8-character elements
! 
!
         GO TO 9
!
   40 CONTINUE
 8100 FORMAT(//,20('ERROR-'),//,T33,&
     &             '       S I M U L A T I O N   A B O R T E D',/,&
     &         T29,'The array dimension MNEL is insufficient ',&
     &             'for the MINC needs',&
     &       /,T33,'               CORRECT AND TRY AGAIN',     &
     &       //,20('ERROR-'))
!
!
!
      WRITE(10,103)
      WRITE(10,104)
  103 FORMAT('     ')
  104 FORMAT('CONNE')
!
!-----NOW REDEFINE ELEMENT COUNTERS.
      NBC=0
      IF(NELA.NE.0) NBC  = NEL-NELA
      IF(NELA.EQ.0) NELA = NEL
!     PARAMETERS FOR PRIMARY MESH
      NELP  = NEL
      NELAP = NELA
!     PARAMETERS FOR SECONDARY MESH
      NELA = J*NELA
      NEL  = NELA+NB!
!
!****READ CONNECTION DATA.*********************************************
!
      N     = 0
      NCONP = 0
      READ(4,1) DENT(1)
      IF(DENT(1).EQ.'CONNE') GOTO 1200
      PRINT 1201,DENT(1)
 1201 FORMAT(' HAVE READ UNKNOWN BLOCK LABEL "',A5,'" ON FILE *MESH*',&
     &       ' --- STOP EXECUTION ---')
      STOP
!C
 1200 CONTINUE
! 
! - GJM: Begin modification for 8-character elements
! 
      IF(MESHX.EQ.'ext2') THEN
         READ(4,20) EL1_3,EL2_5,EM1_3,EM2_5,&
     &              ISOT,D1,D2,AREAX,BETAX
         IF(EL1_3.EQ.'   '.AND.EL2_5.EQ.'     ') GO TO 1400
         IF(EL1_3(1:1).EQ.'+') GO TO 1400
      ELSE
         READ(4,20) EL1,EL2,EM1,EM2,ISOT,D1,D2,AREAX,BETAX
         IF(EL1.EQ.' '.AND.EL2.EQ.'    ') GOTO 1400
         IF(EL1.EQ.'+') GO TO 1400
      END IF
! 
 5020 FORMAT(2(A3,A5), 9X,I5,4E10.4)
   20 FORMAT(2(A1,A4),15X,I5,4E10.4)
! 
! - GJM: End modification for 8-character elements
! 
      N     = N+1
      NCONP = NCONP+1
!
!-----NOW DETERMINE ACTIVITY STATUS OF ELEMENTS AT THIS CONNECTION.
!     ASSIGN THE "WOULD-BE" FRACTURE ELEMENTS, AND SEE WHETHER THEY
!     APPEAR IN THE LIST OF ACTIVE ELEMENTS
!
      IF(MESHX.EQ.'ext2') THEN
         EC1_8 = 'F00'//EL2_5
         EC2_8 = 'F00'//EM2_5
      ELSE
         EC1 = HB//EL2
         EC2 = HB//EM2
      END IF
!     INITIALIZE ACTIVITY INDEX
      IAC = 0
      N1  = NELAP+1
      N2  = NELAP+1
! 
! ----------------
! ........ GJM - 9/19/2000
! ........ Begin correction & modification 
! ........ Use ELEM1 and ELEM2 to avoid augmenting the array size
! ----------------
! 
      IF(MESHX == 'ext2') THEN
         DO 515 I=1,NELAP
            EREF_8 = elem(i)%name 
! 
            IF(EREF_8 /= EC1_8) GO TO 516
            IAC = IAC+1
            N1  = I
  516       IF(EREF_8 /= EC2_8) GO TO 517
            IAC = IAC+1
            N2  = I
  517       CONTINUE
            IF(IAC == 2) GO TO 18
  515    CONTINUE
         IF(N1.GT.NELAP) EC1_8 = EL1_3//EL2_5
         IF(N2.GT.NELAP) EC2_8 = EM1_3//EM2_5
      ELSE
         DO 15 I=1,NELAP
            IF(I <= MNEL) THEN
               ELREF = elem(i)%name(1:5)
            ELSE IF((I > MNEL) .AND. (I <= (2*MNEL))) THEN
               ELREF = elem(i-MNEL)%name(1:5) 
            ELSE IF((I > (2*MNEL)) .AND. (I <= (3*MNEL))) THEN
               ELREF = elem(i-2*MNEL)%name(1:5) 
            END IF
! 
            IF(ELREF /= EC1) GO TO 16
            IAC = IAC+1
            N1  = I
   16       IF(ELREF /= EC2) GO TO 17
            IAC = IAC+1
            N2  = I
   17       CONTINUE
            IF(IAC == 2) GO TO 18
   15    CONTINUE
         IF(N1 > NELAP) EC1 = EL1//EL2
         IF(N2 > NELAP) EC2 = EM1//EM2
      END IF
! 
! ----------------
! ........ GJM - 9/19/2000
! ........ End correction & modification 
! ----------------
! 
   18 CONTINUE
!
!-----GENERATE GLOBAL FRACTURE CONNECTION DATA.
!
      IF(MESHX.EQ.'ext2') THEN
         WRITE(10,5105) EC1_8,EC2_8,ISOT,D1,D2,AREAX,BETAX
      ELSE
         WRITE(10,105) EC1,EC2,ISOT,D1,D2,AREAX,BETAX
      END IF
! 
 5105 FORMAT(2A8, 9X,I5,4E10.4)
  105 FORMAT(2A5,15X,I5,4E10.4)
!
!
      IF(MM.EQ.0) GO TO 1200
!
!-----COME HERE FOR GLOBAL MATRIX-MATRIX FLOW CONNECTIONS.
!
      AB=ABS(BETAX)
      IF((AB.EQ.1.AND.MM.EQ.1).OR.MM.EQ.2) GOTO 33
      GOTO 1200
!
   33 CONTINUE
!
      IF(N1.GT.NELAP.AND.N2.GT.NELAP) GO TO 1200
!
!!!!! DO223 - LOOP INSERTED 11-19-84 TO ASSIGN GLOBAL CONNECTIONS
!     BETWEEN MATRIX CONTINUA.
!
! 
! - GJM: Begin modification for 8-character elements
! 
      IF(MESHX.EQ.'ext2') THEN
         DO M=2,J
            N = N+1
            IF(MM.EQ.1) AX = AREAX*VOL(M)
            IF(MM.EQ.2) AX = AREAX
            IF(N1.LE.NELAP) THEN
               IF(M.LT.11) THEN
                  EC1_8 = 'M0'//NA(M)//EL2_5
               ELSE
                  m_1   = m/10
                  m_2   = MOD(m,10)
                  EC1_8 = 'M'//NA(m_1)//NA(m_2)//EL2_5
               END IF
            END IF
            IF(N2.LE.NELAP) THEN
               IF(J.LT.11) THEN
                  EC2_8 = 'M0'//NA(M)//EM2_5
               ELSE
                  m_1   = m/10
                  m_2   = MOD(m,10)
                  EC2_8 = 'M'//NA(m_1)//NA(m_2)//EM2_5
               END IF
            END IF
            WRITE(10,5105) EC1_8,EC2_8,ISOT,D1,D2,AX,BETAX
         END DO
      ELSE
         DO M=2,J
            N = N+1
            IF(MM.EQ.1) AX = AREAX*VOL(M)
            IF(MM.EQ.2) AX = AREAX
            IF(N1.LE.NELAP) EC1 = NA(M)//EL2
            IF(N2.LE.NELAP) EC2 = NA(M)//EM2
            WRITE(10,105) EC1,EC2,ISOT,D1,D2,AX,BETAX
         END DO
      END IF
! 
! - GJM: End modification for 8-character elements
! 
!
      GOTO1200
!
!-----END OF CONNECTION DATA.-------------------------------------------
!
 1400 CONTINUE
      PRINT 5,NELP,NELAP,NCONP
    5 FORMAT('            THE PRIMARY MESH HAS',I5,' ELEMENTS (',I5,&
     &       ' ACTIVE) AND ',I6,&
     &       ' CONNECTIONS (INTERFACES) BETWEEN THEM')
!
!-----NOW ASSIGN INTRA-BLOCK CONNECTION DATA.
!     LOOP AGAIN OVER ELEMENTS
!
      REWIND 4
      READ(4,1) (DENT(I),I=1,16)
      I=0
! 
! - GJM: Begin modification for 8-character elements
! 
  110 IF(MESHX.EQ.'ext2') THEN
         READ(4,5010) EL1_3,EL2_5,MA,VOLX,AHTX
         IF(EL1_3.EQ.'   '.AND.EL2_5.EQ.'     ') GO TO 111
      ELSE
         READ(4,10) EL1,EL2,MA,VOLX,AHTX
         IF(EL1.EQ.' '.AND.EL2.EQ.'    ') GO TO 111
      END IF
! 
!! - GJM: End modification for 8-character elements
! 
      I = I+1
!
!-----FOR INACTIVE ELEMENTS, DO NOT ASSIGN INTRABLOCK CONNECTIONS
!
      IF(I.GT.NELAP) GO TO 111
!
!-----COME HERE TO ASSIGN INTRABLOCK CONNECTIONS FOR ACTIVE ELEMENTS
!     ONLY.
!
      EL10  = HB
      BETAX = 0.0d0
! 
! - GJM: Begin modification for 8-character elements
! 
      IF(MESHX.EQ.'ext2') THEN
         DO M=2,J
            N     = N+1
            AREAX = VOLX*A(M-1)
            IF(M.EQ.2) THEN
               WRITE(10,5102) 'F00',EL2_5,'M01',EL2_5,D(M-1),D(M),AREAX
            ELSE
               m_1a = (m-1)/10
               m_2a = MOD((m-1),10)
               m_1b = m/10
               m_2b = MOD(m,10)
               WRITE(10,5104) 'M',NA(m_1a),NA(m_2a),EL2_5,&
     &                        'M',NA(m_1b),NA(m_2b),EL2_5,&
     &                         D(M-1),D(M),AREAX
            END IF
         END DO
      ELSE
         DO M=2,J
            N     = N+1
            AREAX = VOLX*A(M-1)
            WRITE(10,102) EL10,EL2,NA(M),EL2,D(M-1),D(M),AREAX
            EL10  = NA(M)
         END DO
      END IF
! 
 5104 FORMAT(2(A1,A1,A1,A5),13X,'1',3E10.4)
 5102 FORMAT(2(A3,A5),13X,'1',3E10.4)
  102 FORMAT(2(A1,A4),19X,'1',3E10.4)
! 
! - GJM: Begin modification for 8-character elements
! 
      GO TO 110
!
  111 CONTINUE
      NCON=N
      PRINT 113,NEL,NELA,NCON
  113 FORMAT(/' WRITE SECONDARY MESH ON FILE *MINC*',/,&
     &        '          THE SECONDARY MESH HAS',I5,' ELEMENTS (',I5,&
     &        ' ACTIVE) AND ',I6,&
     &        ' CONNECTIONS (INTERFACES) BETWEEN THEM')
!
      WRITE(10,103)
      ENDFILE 10
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of MINCME
!
!
      RETURN
      END
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      CHARACTER*5 FUNCTION ONOMA(NLXYZ1,NLXYZ2,NLXYZ3,i,j,k)
!
!*********************************************************************
!*                                                                   *
!*!           FUNCTION FOR DETERMINING THE ELEMENT NAMES             *     
!*!                      CREATED BY MESHMAKER                        *     
!*!                Version 1.00, September 18, 2000                  *     
!*                                                                   *
!*********************************************************************
!
!
!
!*********************************************************************
!*                                                                   *
!*               C O M M O N    D E C L A R A T I O N S              *     
!*                                                                   *
!*********************************************************************
!
      COMMON/N/NA(36)
! 
      CHARACTER*1 NA,L1,L2,L3,L4,L5
! 
      SAVE ICALL
      DATA ICALL/0/
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=> Main body of ONOMA
!
!
      ICALL=ICALL+1
      IF(ICALL.EQ.1) WRITE(11,899)
  899 FORMAT(/,'ONOMA             1.0   18 September 2000',6X,&
     &         'FUNCTION RETURNING THE ELEMENT NAMES ',&
     &         'OF THE GRID CREATED BY MESHMAKER')
 
! 
!*********************************************************************
!*                                                                   *
!*                       DETERMINATION PROCESS                       *
!*                                                                   *
!*********************************************************************
! 
      IF(NLXYZ1.GT.1) THEN
         IF(NLXYZ1.GT.100) THEN
            nnn333 = 1+((k-1)/100)
            IF(nnn333.GT.36) THEN
               PRINT 8100
               STOP
            ELSE
               iii222 = (j-1)+(i-1)*NLXYZ2
               nnn222 = 1+MOD(iii222,36) 
               nnn111 = 1+(iii222/36)
            END IF
         ELSE
            iii333 = (j-1)+(i-1)*NLXYZ2
            nnn333 = 1+MOD(iii333,36) 
            iii222 = 1+(iii333/36)
!
            IF(iii222.EQ.36) THEN
               nnn222 = iii222
               nnn111 = nnn222/36
            ELSE IF(iii222.LT.36) THEN
               nnn222 = iii222
               nnn111 = 1+nnn222/36
            ELSE
               IF(MOD(iii222,36).NE.0) THEN
                  nnn222 = MOD(iii222,36)
                  nnn111 = 1+(iii222/36)
               ELSE
                  nnn222 = 36
                  nnn111 = iii222/36
               END IF
            END IF
!
         END IF
         mmm444 = mod(k-1,100)
         nnn444 = mmm444/10
         nnn555 = mod(mmm444,10)
      ELSE
         IF(NLXYZ2.NE.1) THEN
            IF(NLXYZ2.GT.100) THEN
               nnn333 = 1+((j-1)/100)
               IF(nnn333.GT.36) THEN
                  nnn333 = 1+MOD(nnn333,36)
                  iii333 = 1+nnn333/36
                  IF(iii333.GT.36) THEN
                     nnn222 = 1+MOD(iii333,36)
                     nnn111 = 1+nnn222/36
                  ELSE
                     nnn222 = iii333
                     nnn111 = 1
                  END IF
               ELSE
                  nnn222 = 1+MOD((i-1),36) 
                  nnn111 = 1+((i-1)/36)
               END IF
            ELSE
               nnn333 = 1+MOD((i-1),36) 
               nnn222 = 1+((i-1)/36)
               IF(nnn222.GT.36) THEN
                  nnn111 = 1+MOD(nnn222,36)
               ELSE
                  nnn111 = 1
               END IF
            END IF
            mmm444 = mod(j-1,100)
            nnn444 = mmm444/10
            nnn555 = mod(mmm444,10)
         ELSE
            IF(NLXYZ3.GT.100) THEN
               nnn333 = 1+((i-1)/100)
               IF(nnn333.GT.36) THEN
                  nnn333 = 1+MOD(nnn333,36)
                  iii333 = 1+nnn333/36
                  IF(iii333.GT.36) THEN
                     nnn222 = 1+MOD(iii333,36)
                     nnn111 = 1+nnn222/36
                  ELSE
                     nnn222 = iii333
                     nnn111 = 1
                  END IF
               ELSE
                  nnn222 = 1
                  nnn111 = 1
               END IF
            ELSE
               nnn333 = 1
               nnn222 = 1
               nnn111 = 1
            END IF
            mmm444 = mod(i-1,100)
            nnn444 = mmm444/10
            nnn555 = mod(mmm444,10)
         END IF
      END IF
! 
!***********************************************************************
!*                                                                     *
!*                     RETURNING THE onoma VALUE                       *
!*                                                                     *
!***********************************************************************
! 
      L1 = NA(nnn111+10) 
      L2 = NA(nnn222) 
      L3 = NA(nnn333) 
      L5 = NA(nnn555+1) 
      IF(nnn444.GE.1) THEN
         L4 = NA(nnn444+1) 
      ELSE
         L4 = '0'
      END IF
!
 1000 ONOMA = L1//L2//L3//L4//L5
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 8100 FORMAT(//,20('ERROR-'),//,T33,&
     &             '       S I M U L A T I O N   A B O R T E D',/,&
     &         T19,'The number of elements along the shortest ',&
     &             'dimension of a 3-D grid exceeds 3600',&
     &       /,T33,'               CORRECT AND TRY AGAIN',     &
     &       //,20('ERROR-'))
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=> End of ONOMA
!
!
 9999 RETURN
      END
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************


