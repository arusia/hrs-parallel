!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                                                                     >
!>       TFx_PorousMed_PP.f95: Code unit including the routines        >
!>        describing the porous media properties and processes         >
!>                                                                     >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      SUBROUTINE CapPres_Hyd(T,SL,SG,DW,Pc_GW,NMAT)
! 
! ...... Modules to be used 
! 
         USE PFMedProp_Arrays
! 
         USE H2O_Properties
		 USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*          ROUTINE FOR COMPUTING 3-PHASE CAPILLARY PRESSURES          *
!*        AS FUNCTION OF LIQUID SATURATION SL AND TEMPERATURE T        *
!*                                                                     *
!*                   Version 1.0 - January 12, 2004                    *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision input/output variables
! -------
! 
      REAL(KIND = 8), INTENT(IN)  :: T,SL,SG,DW
      REAL(KIND = 8), INTENT(OUT) :: Pc_GW
! 
! -------
! ... Double precision input/output variables
! -------
! 
      REAL(KIND = 8) :: m_inv,n_inv,Pc_00
! 
      REAL(KIND = 8) :: St_bar,Pc_1,Pc_2,Pc_3

 		  REAL(KIND = 8) :: sl_star
! 
! -------
! ... Integer input variables
! -------
! 
      INTEGER, INTENT(IN) :: nmat
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of CapPres_Hyd
!
!
      ICALL=ICALL+1                                                     
      IF(ICALL.EQ.1 .and. mpi_rank==0) WRITE(11,6000)                                      
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                   CAPILLARRY PRESSURE OPTIONS                       >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
	 
      CASE_CapPres: SELECT CASE (media(nmat)%NumF_CP)
! for problem 3 of international code comparison test
				CASE (2)    !BY RPS

				 m_inv =  1.0d0/0.46         !(1.0d0-1.0d0/media(nmat)%ParCP(2))
         n_inv =  0.46               !1.0d0/media(nmat)%ParCP(2)
         Pc_00 =  - 4000.0           !(1.0d0/8.0d0)*1e5 !!-DW*9.806d0/media(nmat)%ParCP(3)
					sl_star = (SL-0.14)/(1-0.14)
			
			  St_bar = MIN( MAX( (sl_star-media(nmat)%ParCP(1)+1.0d-4)&
     &                     /(1.0-media(nmat)%ParCP(1)),&
     &                      0.0d0),1.0d0)  

				!Pc_GW = Pc_00*((St_bar**(-m_inv)-1.0d0)**n_inv)
        Pc_GW = Pc_00*((Sl_star**(-m_inv)-1.0d0)**n_inv)
			if (Pc_GW <0.0)then
			Pc_GW = 0.0
			else if (Pc_GW >5.0e6)then
			Pc_GW = 5.0e6
		  endif  
!  
! 
! 
!***********************************************************************
!*                                                                     *
!*    3-phase capillary pressure functions of Parker et al. [1987]     *
!*                                                                     *
!***********************************************************************
! 
! 
      CASE (1,8)
!
         m_inv =  1.0d0/(1.0d0-1.0d0/media(nmat)%ParCP(2))
         n_inv =  1.0d0/media(nmat)%ParCP(2)
         Pc_00 = -DW*9.806d0/media(nmat)%ParCP(3)
!
! ...... Computation of the non-gas effective saturation
!
         St_bar = MIN( MAX( (SL-media(nmat)%ParCP(1)+1.0d-4)&
     &                     /(SL+SG-media(nmat)%ParCP(1)),&
     &                      0.0d0),1.0d0)                                    
!
! ...... Computation of the gas-water capillary pressures 
! ...... Scaling to account for effects of hydrate - Leverett J function 
!
         IF_SwVal: IF(St_bar <  1.0d0 .AND. St_bar >= 1.0d-1) THEN
!
                      Pc_GW = Pc_00*((St_bar**(-m_inv)-1.0d0)**n_inv)&
     &                             *SQRT(1.0d0/(SL+SG+1.0d-15))
!
                   ELSE IF(St_bar < 1.0d0 .AND. St_bar < 1.0d-1) THEN
!
                     Pc_1 = Pc_00*(1.00d-1**(-m_inv)-1.0d0)**n_inv
                     Pc_2 = Pc_00*(1.01d-1**(-m_inv)-1.0d0)**n_inv
                     Pc_3 = Pc_00*(0.99d-1**(-m_inv)-1.0d0)**n_inv
!
                     Pc_GW = (Pc_1-5.0d2*(Pc_2-Pc_3)*(1.0d-1-St_bar))&
     &                      *SQRT(1.0d0/(SL+SG+1.0d-15))
!
                  ELSE 
!
                     Pc_GW = 0.0d0
!
                  END IF IF_SwVal
! 
! 
!***********************************************************************
!*                                                                     *
!*                      Zero capillary pressure                        *
!*                                                                     *
!***********************************************************************
! 
! 
      CASE (9)
!
         Pc_GW = 0.0d0

! 
! 
!***********************************************************************
!*                                                                     *
!*        Default case: No capillary pressure has been defined         *
!*                                                                     *
!***********************************************************************
! 
! 
      CASE DEFAULT
!
! ...... Zero capillary pressure ---> Print warning message
!
         Pc_GW = 0.0d0
!
         IF(ICALL == 1) Print 6005, NMAT,media(nmat)%NumF_CP
!
! 
!
      END SELECT CASE_CapPres
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'CapPres_Hyd       1.0   12 January   2003',6X,  &      
     &         '3-phase capillary pressure as function of saturation ',&
     &       /,T48,'in a hydrate-bearing medium')                                                 
!
 6005 FORMAT(//,15('WARNING-'),//,&
     &         T7,'The input parameter NumF_CP(nmat =',i4,') = ',i2, &
     &            ' does not correspond to any of the ',&
     &            'available capillary pressure options',/,&
     &          T35,'The capillary pressures are set to zero by default',&
     &       //,T41,'S I M U L A T I O N   P R O C E E D I N G',&
     &       //,15('WARNING-'))
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of CapPres_Hyd
!
!
      RETURN
! 
      END SUBROUTINE CapPres_Hyd
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE RelPerm_Hyd(SL,SG,REPL,REPG,REPO,NMAT)                        
! 
! ...... Modules to be used 
! 
         USE PFMedProp_Arrays
		 USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*             ROUTINE FOR COMPUTING THE 3-PHASE RELATIVE              *
!*            PERMEABILITIES FOR LIQUID AND GASEOUS PHASES             *
!*                                                                     *
!*                   Version 1.0 - January 12, 2004                    *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision input/output variables
! -------
! 
      REAL(KIND = 8), INTENT(IN)  :: SL,SG
      REAL(KIND = 8), INTENT(OUT) :: REPL,REPG,REPO
! 
! -------
! ... Double precision input/output variables
! -------
! 
      REAL(KIND = 8) :: SM,AN,AM,AMINV,SWR,SGR,SS
      REAL(KIND = 8) :: SGBAR,SWBAR,STBAR,NEXP
      REAL(KIND = 8) :: Sw_irr,Sg_irr,Sw_max,xpn

! 
! -------
! ... Integer input variables
! -------
! 
      INTEGER, INTENT(IN) :: NMAT
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of RelPerm_Hyd
!
!
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)                                      
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                  RELATIVE PERMEABILITY OPTIONS                      >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      CASE_RelPer: SELECT CASE (media(nmat)%NumF_RP)
! 
! 
!***********************************************************************
!*                                                                     *
!*    Hydrate is immobile - All other phases are perfectly mobile      *
!*                                                                     *
!***********************************************************************
! 
! 
      CASE (5)
!
         REPL = 1.0d0                                                        
         REPG = 1.0d0                                                        
         REPO = 0.0d0                                                        
! 
! 
!***********************************************************************
!*                                                                     *
!*     Three-phase relative permeability functions of Stone [1970],    *
!*                     as modified by Aziz [1979]                      *
!*                                                                     *
!***********************************************************************
! 
! 
      CASE (2,6)
!
! ...... RP1 = Residual water saturation; RP2 = Residual oil saturation
! ...... RP3 = Residual gas saturation;   RP4 = Exponent for Kr
!
         SWR  = media(nmat)%ParRP(1)
         SGR  = media(nmat)%ParRP(3)
         NEXP = media(nmat)%ParRP(4)                                                   
!
! ...... Aqueous phase relative permeability
!
         IF_Aqu: IF(SL > SWR) THEN                  
            REPL = MIN( ((SL-SWR)&
     &                 /(1.0d0-SWR))**NEXP,1.0d0)
         ELSE
            REPL = 0.0d0
         END IF IF_Aqu
!
! ...... Gaseous phase relative permeability
!
         IF_Gas: IF(SG > SGR) THEN                  
            REPG = MIN((((SG-SGR)/(1.0d0-SGR))**NEXP),1.0d0)
         ELSE
            REPG = 0.0d0
         END IF IF_Gas
!
! ...... Hydrate phase relative permeability
!
         REPO = 0.0d0
! 
! 
!***********************************************************************
!*                                                                     *
!*   2-phase relative permeability functions of Van Genuchten [1980]   *
!*                                                                     *
!***********************************************************************
! 
! 
      CASE (7,-7)
!
! ----------
! ...... Set the max{Sw} and the van Genuchten exponent
! ----------
!
         Sw_irr  = media(nmat)%ParRP(2)     
         Sg_irr  = media(nmat)%ParRP(4)     
         Sw_max  = media(nmat)%ParRP(3)     
         xpn     = (1.-(1./media(nmat)%ParRP(1))) !m=1-1/n 

!
! ...... Adjusted liquid saturation
!
         SS = ( SL  - Sw_irr )/( Sw_max - Sw_irr )                        
!
! ----------
! ...... Compute relative permeabilities
! ----------
!
         IF_SLiq: IF(SL < Sw_max) THEN                                   
!
! ......... Aqueous phase relative permeability
!
            IF(SS > 0.0d0) THEN                                                  
               REPL = SQRT(SS)&
     &               *( 1.0d0 -( 1.0d0 - SS**( 1.0d0/xpn ))**xpn )**2
            ELSE
               REPL = 0.0d0 
            END IF                                                       
!
! ......... Gaseous phase relative permeability
!
            IF_GasRP: IF(media(nmat)%NumF_RP < 0) THEN
!
! ............ From the Parker et al. (1987) model; zero irreducible gas saturation
!
               IF((1.0d0 - SS) > 0.0d0) THEN                                                  
                  REPG = SQRT(1.0d0 - SS)&
     &                  *( ( 1.0d0 - SS**( 1.0d0/xpn ))**xpn )**2
               ELSE
                  REPG = 0.0d0 
               END IF                                                       
!
            ELSE
!
! ............ From the Van Genuchten [1980] model; zero irreducible gas saturation
!
               IF(Sg_irr <= 0.0d0) THEN
                  REPG = 1.0d0 - REPL  
               ELSE
!
! ............ From the Corey [1954] model; non-zero irreducible gas saturation
!
                  IF(SG <= Sg_irr) THEN
                     REPG = 0.0d0
                  ELSE
                     SS = MIN(  1.0d0, &
     &                          MAX(    0.0d0,  &
     &                                ( (SL - Sw_irr)&
     &                                 /(1.0d0 - Sw_irr - Sg_irr)&
     &                                )  &
     &                             )   &
     &                       )                       
                     REPG = (1.0d0-SS)*(1.0d0-SS)*(1.0d0-SS*SS)
                  END IF 
!
               END IF                                                
!
            END IF IF_GasRP                                                
!
         ELSE
            REPL = 1.0d0                                                        
            REPG = 0.0d0                                                        
         END IF IF_SLiq                                                      
! 
! 
!***********************************************************************
!*                                                                     *
!*     Three-phase relative permeability functions of Stone [1970],    *
!*                     as modified by Aziz [1979]                      *
!*                                                                     *
!***********************************************************************
! 
! 
      CASE (3,10)
!
! ...... RP1 = Residual water saturation; RP2 = Residual oil saturation
! ...... RP3 = Residual gas saturation;   RP4 = Exponent for Kr
!
         SWR  = media(nmat)%ParRP(1)
         SGR  = media(nmat)%ParRP(3)
         NEXP = media(nmat)%ParRP(4)                                                   
!
! ...... Aqueous phase relative permeability
!
         IF_Aqu1: IF(SL > media(nmat)%ParRP(1)) THEN                  
            REPL = MIN( ((SL-media(nmat)%ParRP(1))&
     &                 /(1.0d0-media(nmat)%ParRP(1)))**NEXP,1.0d0)
         ELSE
            REPL = 0.0d0
         END IF IF_Aqu1
!
! ...... Gaseous phase relative permeability
!
         IF_Gas1: IF(SG > SGR) THEN                  
            REPG = MIN((1.0d0-((1.0d0-SG-SWR)/(1.0d0-SWR))**NEXP),1.0d0)
         ELSE
            REPG = 0.0d0
         END IF IF_Gas1
!
! ...... Hydrate phase relative permeability
!
         REPO = 0.0d0
! 
! 
!***********************************************************************
!*                                                                     *
!*  3-phase relative permeability functions of Parker et al. [1987]    *
!*                                                                     *
!***********************************************************************
! 
! 
      CASE (1,9)
!
! ...... Basic parameters
!
         SM    = media(nmat)%ParRP(1)                                                     
         AN    = media(nmat)%ParRP(2)                                                     
         AM    = 1.0d0-1.0d0/AN                                                 
         AMINV = 1.0d0/AM                                                    
!
! ...... Computation of the gas reduced/effective saturations
!
         SGBAR = MIN(MAX((SG-1.0d-4)/(1.0d0-SM),0.0d0),1.0d0)                                    
!
! ...... Computation of the water reduced/effective saturations
!
         SWBAR = MIN(MAX((SL-SM+1.0d-4)/(1.0d0-SM),0.0d0),1.0d0)                                    
!
! ...... Computation of the liquid reduced/effective saturations
!
         STBAR = MIN(MAX( (SL+1.0d-4+(1.0d0-SL-SG)-SM)&
     &                   /(1.0d0-SM),0.0d0),1.0d0)                                    
!
! ...... Computation of the gas phase relative permability
!
         REPG = MIN(MAX( (DSQRT(SGBAR))&
     &                  *((1.0d0-STBAR**AMINV)**AM)&
     &                  *((1.0d0-STBAR**AMINV)**AM) &
     &                  /(1.0d0-SM),0.0d0),1.0d0)                                    
!
! ...... Computation of the aqueous phase relative permability
!
         REPL = MIN(MAX( (DSQRT(SWBAR))&
     &                  *(1.0d0-((1.0d0-SWBAR**AMINV)**AM))&
     &                  *(1.d0-((1.0d0-SWBAR**AMINV)**AM)),0.0d0),&
     &              1.0d0)                                    
!
! ...... Relative permability of hydrate
!
         REPO = 0.0d0                                    
! 
! 
!***********************************************************************
!*                                                                     *
!*       Default case: No relative permability has been defined        *
!*                                                                     *
!***********************************************************************
! 
! 
      CASE DEFAULT
!
! ...... Too important to proceed without ---> Abort simulation
!
         Print 6005, NMAT,media(nmat)%NumF_RP
         STOP                                                       
!
! 
!
      END SELECT CASE_RelPer
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'RelPerm_Hyd       1.0   12 January   2003',6X, &       
     &         '3-phase relative permeability as a function ',      &                     
     &         'of saturation ',&
     &       /,T48,'in a hydrate-bearing medium',/, &                                               
     &       47X,'IRP = 2 or 6 option ("STONE I") modified to ',     &
     &           'force KRN --> 0 as SN --> SNR')                                                 
!
 6005 FORMAT(//,20('ERROR-'),//,T43,&
     &             'S I M U L A T I O N   A B O R T E D',//,&
     &         T5,'The input parameter NumF_RP(nmat =',i4,') = ',i2, &
     &            ' does not correspond to any of the ',&
     &            'available relative permeability options',/,&
     &       /,T50,'CORRECT AND TRY AGAIN',     &
     &       //,20('ERROR-'))
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of RelPerm_Hyd
!
!
      RETURN
! 
      END SUBROUTINE RelPerm_Hyd
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE PERM_Interface(ISO,NMAT1,NMAT2,PRES1,PRES2,WT1,WT2,&
     &                          PermModif,pm_1,pm_2,PER1,PER2,PERI)
! 
! ...... Modules to be used 
! 
         USE PFMedProp_Arrays
		 USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************                                                                     *
!*           ROUTINE TO COMPUTE THE INTERFACE PERMEABILITY             *
!*                   Version 1.0 - August 18, 2003                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!

	
      IMPLICIT NONE
! 
! -------
! ... Double precision input/output variables
! -------
! 
      REAL(KIND = 8), INTENT(IN)  :: PRES1,PRES2,WT1,WT2,pm_1,pm_2
      REAL(KIND = 8), INTENT(OUT) :: PER1,PER2,PERI
! 
! -------
! ... Double precision input/output variables
! -------
! 
      REAL(KIND = 8) :: FK1,FK2,DPERI
! 
! -------
! ... Integer input variables
! -------
! 
      INTEGER, INTENT(IN) :: ISO,NMAT1,NMAT2
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0
! 
! -------
! ... Character variables
! -------
! 
      CHARACTER(LEN = 4) :: PermModif
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of PERM_Interface
!
!
      ICALL = ICALL+1
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)
!
! --------
! ... Intrinsic Permeabilities
! --------
!
      PER1 = media(nmat1)%Perm(iso)
      PER2 = media(nmat2)%Perm(iso)
! 
! --------
! ... Adjustment - Permeability modifiers  
! --------
! 
      IF(PermModif /= 'NONE') THEN
         PER1 = PER1*pm_1
         PER2 = PER2*pm_2
      END IF
! 
! --------
! ... Adjustment - Klinkenberg factors  
! --------
! 
      FK1  = media(nmat1)%Klink/PRES1
      FK2  = media(nmat2)%Klink/PRES2
! 
      PER1 = PER1*(1.0d0+FK1)
      PER2 = PER2*(1.0d0+FK2)
! 
! -------
! ... Harmonic weighting of intrinsic permeabilities          
! -------
! 
      DPERI = WT1*PER1+WT2*PER2
!
      IF_KHarW: IF(DPERI /= 0.0d0) THEN
                   PERI = PER1*PER2/DPERI
                ELSE
                   PERI = 0.0d0
                END IF IF_KHarW
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'PERM_Interface    1.0   18 August    2003',6X,&
     &         'Interface permeability')
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of PERM_Interface
!
!
      RETURN
!
      END SUBROUTINE PERM_Interface
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      REAL(KIND=8) FUNCTION Thrm_ConductGHydr(N1,N2,P1,P2,T1,T2,D1,D2,&
     &                                        S1,S2,S1O,S2O,S1i,S2i,&
     &                                        Dens1,Dens2,PS1,PS2,&
     &                                        WT1,WT2,NMAT1,NMAT2,&
     &                                        S_Molal,X1_mG,X2_mG,&
     &                                        MOP_10)
! 
! ...... Modules to be used 
! 
         USE Basic_Param
         USE Hydrate_Param
         USE RefRGas_Param
! 
         USE H2O_Properties
!
         USE Element_Arrays
         USE PFMedProp_Arrays
!
         USE RealGas_Properties
		 USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*      FUNCTION TO COMPUTE THE THERMAL CONDUCTIVITY OF A POROUS       *
!*   MEDIUM IMPREGNATED WITH VARIOUS PHASES IN A GAS HYDRATE PROBLEM   *
!*                                                                     *
!*                  Version 1.0 - September 9, 2003                    *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(2) :: G_MoleF
! 
! -------
! ... Double precision input variables
! -------
! 
      REAL(KIND = 8), INTENT(IN) :: T1,T2,D1,D2,WT1,WT2
      REAL(KIND = 8), INTENT(IN) :: X1_mG,X2_mG,S1,S2,S1O,S2O,S1i,S2i
      REAL(KIND = 8), INTENT(IN) :: P1,P2,PS1,PS2,Dens1,Dens2,S_Molal 
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: T1K,T2K,SG1,SG2
      REAL(KIND = 8) :: CON1h,CON2h,CON1rw,CON2rw
      REAL(KIND = 8) :: CON1g,CON2g,CON1i,CON2i
      REAL(KIND = 8) :: CON1,CON2,CONI,DCONI,SUM 
      REAL(KIND = 8) :: ZLiq,ZGas,rho_G,rho_L,X1mol_mG,X2mol_mG
! 
! -------
! ... Integer input variables
! -------
! 
      INTEGER, INTENT(IN) :: N1,N2,NMAT1,NMAT2,MOP_10
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0, i, ixx 
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=> Main body of Thrm_ConductGHydr
!
!
      ICALL = ICALL+1
      IF(ICALL == 1 .and. mpi_Rank==0) WRITE(11,6000)
!
! ... Temperature variables
!
      T1K = T1+273.15d0
      T2K = T2+273.15d0
! 
!***********************************************************************
!*                                                                     *
!*          Contribution of hydrate to thermal conductivity            *
!*                                                                     *
!***********************************************************************
! 
      IF(GH%N_ThC == 1) THEN
! 
! ----------
! ...... Temperature invariant  
! ----------
! 
         CON1h = elem(n1)%phi*S1O*GH%p_ThC(1)
         CON2h = elem(n2)%phi*S2O*GH%p_ThC(1)
      ELSE 
! 
! ----------
! ...... Temperature-dependent   
! ----------
! 
         SUM = GH%p_ThC(GH%N_ThC)
! 
         DO i=GH%N_ThC-1,1,-1
            SUM = SUM*T1+GH%p_ThC(i)
         END DO
! 
         CON1h = elem(n1)%phi*S1O*SUM
! 
         DO i=GH%N_ThC-1,1,-1
            SUM = SUM*T2+GH%p_ThC(i)
         END DO
! 
         CON2h = elem(n2)%phi*S2O*SUM
! 
      END IF
! 
!***********************************************************************
!*                                                                     *
!*             Determine if there is any ice in the system             *
!*                                                                     *
!***********************************************************************
! 
      IF(S1i >= 1.0d-12 ) THEN
         CON1i  = elem(n1)%phi*S1i*ThrmCond_Ice(T1)
      ELSE 
         CON1i = 0.0d0
      END IF
! 
      IF(S2i >= 1.0d-12 ) THEN
         CON2i  = elem(n2)%phi*S2i*ThrmCond_Ice(T2)
      ELSE 
         CON2i = 0.0d0
      END IF
! 
!***********************************************************************
!*                                                                     *
!*    Contribution of rock + aqueous phase to thermal conductivity     *
!*                                                                     *
!***********************************************************************
! 
      IF_KthRW: IF(MOP_10 == 0) THEN         ! Standard TOUGH2-option #1
! 
         CON1rw = media(nmat1)%KThrD&
     &           +SQRT(S1)*( media(nmat1)%KThrW&
     &                      -media(nmat1)%KThrD)
! 
         CON2rw = media(nmat2)%KThrD&
     &           +SQRT(S2)*( media(nmat2)%KThrW&
     &                      -media(nmat2)%KThrD)
! 
      ELSE IF(MOP_10 == 1) THEN              ! Standard TOUGH2-option #2
! 
         CON1rw = media(nmat1)%KThrD&
     &           +S1*( media(nmat1)%KThrW&
     &                -media(nmat1)%KThrD)
! 
         CON2rw = media(nmat2)%KThrD&
     &           +S2*( media(nmat2)%KThrW&
     &                -media(nmat2)%KThrD)
! 
      ELSE                                   ! Hydrate-specific option
! 
         CON1rw = media(nmat1)%KThrD&
     &           +S1*elem(n1)%phi*ThrmCond_LiqH2O(T1,P1,Dens1,PS1)
! 
         CON2rw = media(nmat2)%KThrD&
     &           +S2*elem(n2)%phi*ThrmCond_LiqH2O(T2,P2,Dens2,PS2)
! 
      END IF IF_KthRW           
! 
!***********************************************************************
!*                                                                     *
!*            Contribution of gas to thermal conductivity              *
!*                    [ignored for MOP(10) = 0,1]                      *
!*                                                                     *
!***********************************************************************
! 
      IF(MOP_10 > 2) THEN
! 
         SG1 = 1.0d0-S1-S1O-S1i
         SG2 = 1.0d0-S2-S2O-S2i
! 
! ...... First, the element N1 
! 
         X1mol_mG  =   X1_mG/GasR(id(1))%MolWt&
     &              /( X1_mG/GasR(id(1))%MolWt&
     &                +(1.0d0-X1_mG)*GasR(8)%MolWt)
! 
         G_MoleF(1) = X1mol_mG
         G_MoleF(2) = 1.0d0-X1mol_mG
! 
         CALL ZComp(P1,T1K,S_Molal,G_MoleF(1:2),&
     &                 ZLiq,ZGas,rho_G,rho_L,ixx)
! 
         CON1g = SG1*elem(n1)%phi*Gas_ThrmConduct(P1,T1K,ZGas)
! 
! ...... Then, the element N2 
! 
         X2mol_mG  =   X2_mG/GasR(id(1))%MolWt&
     &              /( X2_mG/GasR(id(1))%MolWt&
     &                +(1.0d0-X2_mG)*GasR(8)%MolWt)
! 
         G_MoleF(1) = X2mol_mG
         G_MoleF(2) = 1.0d0-X2mol_mG
! 
         CALL ZComp(P2,T2K,S_Molal,G_MoleF(1:2),&
     &                 ZLiq,ZGas,rho_G,rho_L,ixx)
! 
         CON2g = SG2*elem(n2)%phi*Gas_ThrmConduct(P2,T2K,ZGas)
! 
      ELSE
! 
         CON1g = 0.0d0
         CON2g = 0.0d0
! 
      END IF
! 
! 
!***********************************************************************
!*                                                                     *
!*                   COMPOSITE THERMAL CONDUCTIVITY                    *
!*                                                                     *
!***********************************************************************
! 
      CON1 = CON1h+CON1rw+CON1g+CON1i
      CON2 = CON2h+CON2rw+CON2g+CON2i
!
      DCONI = WT1*CON1+WT2*CON2
!c 
!c -------
!c! .. Harmonic weighting          
!c -------
!c 
      IF_ThCond: IF(DCONI /= 0.0d0) THEN
         CONI = CON1*CON2/DCONI
      ELSE
         IF(D1 == 0.0d0) THEN
            CONI = CON2
         ELSE IF(D2 == 0.0d0) THEN
            CONI = CON1
         ELSE 
            CONI = 0.0d0
         END IF
      END IF IF_ThCond
!
!
!
      Thrm_ConductGHydr = CONI
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'Thrm_ConductGHydr 1.0    9 September 2003',6X,&
     &         'Thermal conductivity of a hydrate-bearing porous ',&
     &         'medium')
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  END Thrm_ConductGHydr
!
!
      RETURN
!
      END FUNCTION Thrm_ConductGHydr
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      REAL(KIND=8) FUNCTION Thrm_ConductVOC(T1,T2,N1,N2,D1,D2,S1,S2,&
     &                                      S1O,S2O,NMAT1,NMAT2,&
     &                                      WT1,WT2,&
     &                                      TBR,TCRIT,MW_VOC,MOP_10)
! 
! ...... Modules to be used 
! 
         USE Basic_Param
!
         USE Element_Arrays
         USE PFMedProp_Arrays
		 USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*      FUNCTION TO COMPUTE THE THERMAL CONDUCTIVITY OF A POROUS       *
!*     MEDIUM IMPREGNATED WITH VARIOUS PHASES (VOC phase included)     *
!*                                                                     *
!*                  Version 1.0 - September 9, 2003                    *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision input variables
! -------
! 
      REAL(KIND = 8), INTENT(IN) :: T1,T2,D1,D2,WT1,WT2
      REAL(KIND = 8), INTENT(IN) :: S1,S2,S1O,S2O
      REAL(KIND = 8), INTENT(IN) :: TBR,TCRIT,MW_VOC
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: T1K,T2K,T1R,T2R,CONDO1,CONDO2
      REAL(KIND = 8) :: CON1o,CON2o,CON1rw,CON2rw,CON1,CON2 
      REAL(KIND = 8) :: CONI,DCONI 
! 
! -------
! ... Integer input variables
! -------
! 
      INTEGER, INTENT(IN) :: N1,N2,NMAT1,NMAT2,MOP_10
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=> Main body of Thrm_ConductVO!
!
!
      ICALL = ICALL+1
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)
!
! ... Temperature variables
!
      T1K = T1+273.15d0
      T2K = T2+273.15d0
      T1R = T1K/TCRIT
      T2R = T2K/TCRIT
! 
! -------
! ... NAPL-phase thermal conductivity          
! -------
! 
      CONDO1=( (1.11d0/DSQRT(MW_VOC))&
     &        *(3.0d0+2.0d1*(1.0d0-T1R)**6.667d-1)&
     &       )/(3.0d0+2.0d1*(1.0d0-TBR)**6.667d-1)
!
      CONDO2=( (1.11d0/DSQRT(MW_VOC))&
     &        *(3.0d0+2.0d1*(1.0d0-T2R)**6.667d-1)&
     &       )/(3.0d0+2.0d1*(1.0d0-TBR)**6.667d-1)
!
      CON1o = elem(n1)%phi*S1O*CONDO1
      CON2o = elem(n2)%phi*S2O*CONDO2
! 
! -------
! ... Rock + Aqueous phase thermal conductivity          
! -------
! 
      IF_KthRW: IF(MOP_10 == 0) THEN
                   CON1rw = media(nmat1)%KThrD&
     &                     +SQRT(S1)*( media(nmat1)%KThrW&
     &                                 -media(nmat1)%KThrD)
                   CON2rw = media(nmat2)%KThrD&
     &                     +SQRT(S2)*( media(nmat2)%KThrW&
     &                                 -media(nmat2)%KThrD)
                ELSE            
                   CON1rw = media(nmat1)%KThrD&
     &                     +S1*( media(nmat1)%KThrW&
     &                          -media(nmat1)%KThrD)
                   CON2rw = media(nmat2)%KThrD&
     &                     +S2*( media(nmat2)%KThrW&
     &                          -media(nmat2)%KThrD)
                END IF IF_KthRW           
! 
! -------
! ... Composite thermal conductivity          
! -------
! 
      CON1 = CON1o+CON1rw
      CON2 = CON2o+CON2rw
!
      DCONI = WT1*CON1+WT2*CON2
! 
! -------
! ... Harmonic weighting          
! -------
! 
      IF_ThCond: IF(DCONI /= 0.0d0) THEN
         CONI = CON1*CON2/DCONI
      ELSE
         IF(D1 == 0.0d0) THEN
            CONI = CON2
         ELSE IF(D2 == 0.0d0) THEN
            CONI = CON1
         ELSE 
            CONI = 0.0d0
         END IF
      END IF IF_ThCond
!
!
!
      Thrm_ConductVOC = CONI
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'Thrm_ConductVOC   1.0    9 September 2003',6X,&
     &         'Thermal conductivity of a VOC-bearing porous ',&
     &         'medium')
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  END Thrm_ConductVO!
!
!
      RETURN
!
      END FUNCTION Thrm_ConductVOC
!
!
!
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

