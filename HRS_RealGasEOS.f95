!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                                                                     >
!>        REAL GAS EQUATION OF STATE, PROPERTIES AND PROCESSES         >
!>                                                                     >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      MODULE Gas_DerivedType
! 
         SAVE
! 
! ----------
! ...... Derived-type variables     
! ----------
! 
         TYPE GasParV
            CHARACTER(LEN = 6) :: Name
            REAL(KIND = 8)     :: AtomDV
            REAL(KIND = 8)     :: TCrit,PCrit,VCrit,ZCrit
            REAL(KIND = 8)     :: Omega,MolWt,DMom
            REAL(KIND = 8)     :: A0,A1,A2,A3,A4
            REAL(KIND = 8)     :: Psi,A,B,T_B
            REAL(KIND = 8), DIMENSION(5) :: LogK  ! Log10(K) regression coefficients
            REAL(KIND = 8), DIMENSION(6) :: MVol  ! Molar volume regression coefficients
            REAL(KIND = 8), DIMENSION(6) :: SOCo  ! Salting-out coefficient
         END TYPE GasParV
! 
         TYPE GasPar2
            REAL(KIND = 8)     :: TCrit,PCrit,ZCrit,VCrit
            REAL(KIND = 8)     :: Omega,MolWt,DMom
         END TYPE GasPar2
! 
! 
      END MODULE Gas_DerivedType
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      MODULE RefRGas_Param
! 
         USE Gas_DerivedType
! 
         SAVE
! 
! ----------
! ...... Double precision parameters
! ----------
! 
         PARAMETER (Rgas = 8.314d3)
! 
! ----------
! ...... Integer parameters
! ----------
! 
         PARAMETER (Ncom = 9)
! 
! ----------
! ...... Derived-type parameters   
! 
! ...... NOTE: The Psi, A, B in the parameters
! ......    For CO2 : From Diamond and Atkinfiev (2003), Fluid Phase Equilib (in press) 
! ......    For rest: Table 1 and 2, Atkinfiev and Diamond, 2003, GCA (see above)
! 
! ...... NOTE: Salting-out coefficients
! ......    Langmuir (1997)  Aqueous Environmental Geochemistry, Prentice Hall, Table 4.5, p.144 
! ......    Modified because of s problem with the log form in the reference
! -------------
! 
! ----------
! 
         TYPE(GasParV), PARAMETER :: Methane = GasParV &
            ("CH4   ", 2.514d1,     &                                ! Name, AtomDV
              1.9056d+02,  4.600155d+06,  9.9000d-02,  2.880d-01,  & ! TCrit,PCrit,VCrit,ZCrit
              1.1000d-02,  1.604300d-02,  0.0000d+00,  4.568d+00,  & ! Omega,MolWt,DMom,A0  
             -8.9750d-03,  3.631000d-05, -3.4070d-08,  1.091d-11,  & ! A1,A2,A3,A4 
             -0.1131d+00, -1.184620d+01,  1.48615d01,  111.6d0,    & ! Psi,A,B,T_B
           (/-2.5764d+00, -1.3019d-02,    1.0924d-04, -2.9008d-07, & ! Log10(K):  0-350 C
              3.2774d-10 /),  &
           (/ 3.7076d+01,  2.1313d-03,   -2.8752d-05,  1.1936d-06,&  ! MolVol: 0-250 C 200 bar: SUPCRT92 + modified SLOP98
             -7.3216d-09,  1.8923d-11 /),&
           (/ 1.64818d-1, -1.40166d-3,    1.32360d-5, -4.85733d-8,&  ! Salting-out coefficient: From Cramer
             7.87967d-11, -5.52586d-14 /) )
! 
!    Alternative for CH4:  25-300 C 350 bar: Regression from Hnedkovsky, MVOL = ...
!    (/ 3.5821E+01, 5.8603E-02,-1.3091E-03, 1.9022E-05,-8.6735E-08, 1.4126E-10 /) 
! 
         TYPE(GasParV), PARAMETER :: Ethane = GasParV &
            ("C2H6  ", 4.566d1,                       &              ! Name, AtomDV
              3.0532d+02,  4.883865d+06,  1.4800d-01,  2.850d-01,  &
              9.9000d-02,  3.007000d-02,  0.0000d+00,  4.178d+00,   & 
             -4.4270d-03,  5.660000d-05, -6.6510d-08,  2.487d-11,&
             -0.6091d+00, -1.634820d+01,  2.00628d01,  184.6d0,&
           (/-2.1993d+00, -1.8015d-02,    1.4569d-04, -4.0442d-07,&  ! 0-350 C
              4.6619d-10 /),&
           (/ 4.6452d+01,  2.3782d-01,   -2.8260d-03,  2.0155d-05,&  ! 0-250 C 200 bar: SUPCRT92 + modified SLOP98  
             -7.1926d-08,  1.0645d-10 /),&
           (/ 1.64818d-1, -1.40166d-3,    1.32360d-5, -4.85733d-8, & ! From Cramer (1982) + Soreide & Whitson (1992)
             7.87967d-11, -5.52586d-14 /) ) 
! 
         TYPE(GasParV), PARAMETER :: Propane = GasParV &
            ("C3H8  ", 6.618d1,                         &             ! Name, AtomDV 
             3.6983d+02,  4.2455175d+06,  2.0300d-01,  2.810d-01,  &
              1.5200d-01,  4.4097000d-02,  0.0000d+00,  3.487d+00,  &  
              5.1310d-03,  6.0110000d-05, -7.8930d-08,  3.079d-11,&
             -1.1471d+00, -2.5387900d+01,  2.82616d01,  231.1d0,&
           (/-2.3150d+00, -2.0996d-02,     1.7215d-04, -4.8268d-07,&  ! 0-350 C
              2.0620d-10 /),   &
           (/ 6.1525d+01,  2.8840d-01,    -3.4270d-03,  2.4452d-05,&  ! 0-250 C 200 bar: SUPCRT92 + modified SLOP98  
             -8.7292d-08,  1.2927d-10 /),&
           (/ 0.00d+00,    0.00d+00,       0.00d+00,    0.000d+00,&   ! From Cramer (1982) + Soreide & Whitson (1992) 
              0.00d+00,    0.00d+00 /) ) 
!  
         TYPE(GasParV), PARAMETER :: H2Sulfide = GasParV &
            ("H2S   ", 2.752d1,                          &            ! Name, AtomDV  
              3.7340d+02,  8.9368650d+06,  9.8500d-02,  2.840d-01,  &
              9.0000d-02,  3.4082000d-02,  9.0000d-01,  3.259d+00,   & 
             -3.4380d-03,  1.3190000d-05, -1.3310d-08,  0.488d-11,&
             -0.2102d+00, -1.1230300d+01,  1.26104d01,  0.0d0,&
           (/-6.7372d-01, -1.4245d-02,     7.9816d-05, -1.9861d-07,&  ! 0-350 C
              5.5931d-10 /),   &
           (/ 3.3065d+01,  8.3733d-02,    -9.9447d-04,  6.9408d-06, & ! 0-250 C 200 bar: SUPCRT92 + modified SLOP98  
             -2.4299d-08,  3.4721d-11 /),&
           (/ 2.9050d-1,  -1.57400d-4,    -4.62000d+1,  5.70500d-1, & ! From Drummond (1981)
             -1.7770d-3,   0.00d+00   /) ) 
! 
!    Alternative for H2S:  25-350 C 350 bar: Regression from Hnedkovsky, MVOL = ...
!    (/ 3.3521E+01, 6.8759E-02,-7.8118E-04, 9.2859E-06,-3.8445E-08, 6.1362E-11 /) 
! 
         TYPE(GasParV), PARAMETER :: CarbonO2 = GasParV&
            ("CO2   ", 2.690d1,     &                                 ! Name, AtomDV 
              3.0412d+02,  7.3764600d+06,  9.4000d-02,  2.740d-01,&   ! TCrit,PCrit,VCrit,ZCrit
              2.2500d-01,  4.4010000d-02,  0.0000d-00,  3.539d+00, &   
              1.3560d-03,  1.5020000d-05, -2.3740d-08,  1.056d-11,&
             -0.0880d+00, -9.3134000d+00,  1.15477d01,  0.0d0,&
           (/-1.1161d+00, -1.6023d-02,     9.5730d-05, -2.3733d-07,&  ! 0-350 C
              2.3441d-10 /),  &
           (/ 2.9844d+01,  1.3722d-01,    -1.6281d-03,  1.0898d-05, & ! 0-250 C 200 bar: SUPCRT92 + modified SLOP98  
             -3.6679d-08,  4.8444d-11 /),&
           (/-1.0312d00,   1.28060d-3,     2.55900d+2,  4.44500d-1, & ! From Drummond (1981)
             -1.6060d-3,   0.00d+00   /) ) 
! 
!    Alternative 1 for CO2:  25-350 C 350 bar: Regression from Hnedkovsky, MVOL = ...
!    (/ 3.3101E+01, 6.1728E-03, 3.2205E-06, 6.3807E-06,-3.6252E-08, 6.8151E-11 /)
! 
!    Alternative 2 for CO2:  25-350 C 350 bar: Regression of J.E. Garcia, LBNL, 2001, MVOL = ...
!    (/ 3.7510E+01,-9.5850E-02,+8.7400E-04,-5.0440E-07,-4.4956E-19, 6.3527E-22 /) 
! 
         TYPE(GasParV), PARAMETER :: Nitrogen = GasParV &
            ("N2    ", 1.850d1,                         &             ! Name, AtomDV 
              1.2620d+02,  3.3943875d+06,  8.9500d-02,  2.900d-01,  &
              3.7000d-02,  2.8014000d-02,  0.0000d-00,  3.539d+00,   & 
             -0.2610d-03,  0.0070000d-05,  0.1570d-08, -0.099d-11, &
             -0.0320d+00, -1.1538000d+01,  1.46278d01,  0.0d0,&
           (/-2.9831d+00, -9.9098d-03,     8.5586d-05, -2.4931d-07,  &! 0-350 C
              3.0552d-10 /),  &
           (/ 3.0349d+01,  1.3953d-01,    -1.6592d-03,  1.2163d-05, &  ! 0-250 C 200 bar: SUPCRT92 + modified SLOP98  
             -4.4426d-08,  6.8446d-11 /),&
           (/ 8.68589d-2,  0.00d+00,       0.00d+00,    0.000d+00,&   ! = 0.2/Ln(10); test OK
              0.00d+00,    0.00d+00 /) ) 
! 
         TYPE(GasParV), PARAMETER :: Oxygen = GasParV &
            ("O2    ", 1.630d1,                       &               ! Name, AtomDV 
              1.5458d+02,  5.0459850d+06,  7.3400d-02,  2.880d-01,  &
              2.1000d-02,  3.1999000d-02,  0.0000d-00,  3.630d+00,   & 
             -1.7940d-03,  0.6580000d-05, -0.6010d-08,  0.179d-11, &
              0.0260d+00, -9.7540000d+00,  0.00000d00,  0.0d0,&
           (/-2.6661d+00, -1.1187d-02,     9.0527d-05, -2.6430d-07, & ! 0-350 C
              3.2814d-10 /),  &
           (/ 2.7708d+01,  1.3086d-01,    -1.5568d-03,  1.1635d-05,&  ! 0-250 C 200 bar: SUPCRT92 + modified SLOP98  
             -4.3171d-08,  6.8248d-11 /),&
           (/ 1.62180d-1, -1.16909d-3,     5.55185d-6, -8.75443d-9,  &! From Cramer (1982) 
             9.91567d-12,  0.00d+00   /) ) 
!       
         TYPE(GasParV), PARAMETER :: Water = GasParV&
            ("H2O   ", 1.310d1,                      &                ! Name, AtomDV 
              6.4714d+02,  2.2048320d+07,  5.6000d-02,  2.290d-01,  &
              3.4400d-01,  1.8015000d-02,  1.8000d-00,  4.395d+00,   & 
             -4.1860d-03,  1.4050000d-05, -1.5640d-08,  0.632d-11,& 
              0.0000d+00,  0.0000000d+00,  0.00000d00,  0.0d0,&
           (/ 0.00d+00,    0.00d+00,       0.00d+00,    0.000d+00,   &
              0.00d+00 /),  &
           (/ 0.00d+00,    0.00d+00,       0.00d+00,    0.000d+00,   &
              0.00d+00,    0.00d+00 /),&
           (/ 0.00d+00,    0.00d+00,       0.00d+00,    0.000d+00,  & ! Not needed 
              0.00d+00,    0.00d+00 /) ) 
!        
         TYPE(GasParV), PARAMETER :: Ethanol = GasParV &
            ("C2H5OH", 5.177d1,                         &             ! Name, AtomDV  
              5.1264d+02,  6.383475d+06,   1.670d-01,   2.480d-01,  &
              5.5650d-01,  3.204200d-02,   1.700d-00,   4.714d+00,   & 
             -6.9860d-03,  4.211000d-05,  -4.443d-08,   1.535d-11, &
              0.0000d+00,  0.0000000d+00,  0.00000d0,   0.0d0,&
           (/ 0.00d+00,    0.00d+00,       0.00d+00,    0.000d+00,   &
              0.00d+00 /),  &
           (/ 0.00d+00,    0.00d+00,       0.00d+00,    0.000d+00,   &
              0.00d+00,    0.00d+00 /),&
           (/ 0.00d+00,    0.00d+00,       0.00d+00,    0.000d+00, &  ! Not needed 
              0.00d+00,    0.00d+00 /) )         
! 
! ----------
! ...... Derived-type parameter arrays     
! ----------
! 
         TYPE(GasParV), PARAMETER, DIMENSION(Ncom) :: GasR = &
          (/  Methane,  Ethane,  Propane,  H2Sulfide,  CarbonO2,&
              Nitrogen, Oxygen,  Water,    Ethanol&
          /)
! 
! ----------
! ...... Derived-type arrays     
! ----------
! 
         TYPE(GasPar2), DIMENSION(Ncom) :: Gas
! 
! ----------
! ...... Double precision arrays
! ----------
! 
         REAL(KIND = 8), DIMENSION(Ncom) :: RK,Y,TR,Phi_F,ActCo
         REAL(KIND = 8), DIMENSION(Ncom) :: AxAL,BB,ALF
!
         REAL(KIND = 8), DIMENSION(Ncom,Ncom) :: Kij_SC,Kij_PC,Kij_RC
         REAL(KIND = 8), DIMENSION(Ncom,Ncom) :: Kij_S,Kij_P,Kij_R
         REAL(KIND = 8), DIMENSION(Ncom,Ncom) :: G_1p,G_2p,Ex_a
! 
! ----------
! ...... Integer arrays
! ----------
! 
         INTEGER, DIMENSION(Ncom) :: ID
! 
! ----------
! ...... Double precision variables
! ----------
! 
         REAL(KIND = 8) :: AUP,BUP,AxAL_m
! 
! ----------
! ...... Integer variables
! ----------
! 
         INTEGER :: NumCom,iH2O,iH2S
! 
! ----------
! ...... Character variables
! ----------
! 
         CHARACTER(LEN = 3) :: EOS_Type
         CHARACTER(LEN = 1) :: Phase
! 
! 
      END MODULE RefRGas_Param
!
!
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
! 
!
      MODULE RealGas_Properties
! 
         PRIVATE
! 
         PUBLIC :: RGasSetup,Zcomp,Gas_Fugacity,Gas_Solubility,&
     	             Gas_Viscosity,Gas_ThrmConduct,&
                  Gas_BinDiffusLoP,Gas_BinDiffusHiP,&
                  HUS_Departure,HU_IdealGas,HUS_IdealGas,&
                  HEAT_GasSolution,HenryS_Law,HenryS_K
!
!
! 
         CONTAINS
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE RGasSetup(N_Com,Cname,EquTyp,PhaseF)
! 
         USE RefRGas_Param
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*         Providing reference to the built-in gas parameters          *     
!*                                                                     *
!*                    Version 1.00 - 8 April, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
      USE MPI_PARAM
      IMPLICIT NONE
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: N_Com
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: i,j
! 
! -------
! ... Character arrays
! -------
! 
      CHARACTER(LEN = 6), DIMENSION(N_Com), INTENT(IN) :: Cname
! 
! -------
! ... Character variables
! -------
! 
      CHARACTER(LEN = 3), INTENT(IN) :: EquTyp
      CHARACTER(LEN = 1), INTENT(IN) :: PhaseF
! 
      SAVE ICALL
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=> Main body of RGasSetup
! 
! 
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)                                      
! 
! -------
! ... Define the module-specific parameters
! -------
! 
      NumCom   = N_Com      
      EOS_Type = EquTyp
      Phase    = PhaseF
! 
! -------
! ... Check if requested phase is available
! -------
! 
      IF_Phase: IF(Phase /= 'L' .AND. Phase /= 'G') THEN  ! If unavailable phase, ...
         WRITE(2,6005) Phase                              ! ... write a message, and ... 
         STOP                                             ! ... abort the simulation
      END IF IF_Phase
! 
!***********************************************************************      
!*                                                                     *
!*                       Select the EOS to use                         *
!*                                                                     *
!***********************************************************************      
!
      IF_SelectEOS: IF(EOS_Type.EQ.'SRK') THEN   ! Soave-Redlich-Kwong
         CALL K_SRK
      ELSE IF(EOS_Type(1:2).EQ.'PR') THEN        ! Peng-Robinson
         CALL K_PR
      ELSE IF(EOS_Type(1:2).EQ.'RK') THEN        ! Redlich-Kwong
         CALL K_RK
      ELSE                                     ! If unavailable EOS, ...
         WRITE (2,6010) EOS_Type                 ! ... write a message, and ...
         STOP                                  ! ... abort the simulation
      END IF IF_SelectEOS
! 
!***********************************************************************      
!*                                                                     *
!*          Determine the indices for communication with the           *
!*                    built-in parameter data base                     *
!*                                                                     *
!***********************************************************************      
!
      iH2O = 0   ! Initializations
      iH2S = 0
! 
! 
! 
      DO j=1,NumCom
! 
! ----------
! ...... Determining the ID indices       
! ----------
! 
         IF(Cname(j)(1:3) == 'CH4') THEN
            ID(j) = 1
         ELSE IF(Cname(j)(1:3) == 'C2H') THEN
            ID(j) = 2
         ELSE IF(Cname(j)(1:3) == 'C3H') THEN
            ID(j) = 3
         ELSE IF(Cname(j)(1:3) == 'H2S') THEN
            ID(j) = 4
            iH2S  = 1
         ELSE IF(Cname(j)(1:3) == 'CO2') THEN
            ID(j) = 5
         ELSE IF(Cname(j)(1:2) == 'N2') THEN
            ID(j) = 6
         ELSE IF(Cname(j)(1:2) == 'O2') THEN
            ID(j) = 7
         ELSE IF(Cname(j)(1:3) == 'H2O') THEN
            ID(j) = 8
            iH2O  = 1
         ELSE IF(Cname(j) == 'C2H3OH') THEN
            ID(j) = 9
         ELSE 
            WRITE(2,6015) Cname(j)
            STOP
         END IF
! 
! ----------
! ...... Mapping the mixture parameters to the reference ones      
! ----------
! 
         Gas(j)%Tcrit = GasR(ID(j))%Tcrit        
         Gas(j)%Pcrit = GasR(ID(j))%Pcrit        
         Gas(j)%Vcrit = GasR(ID(j))%Vcrit       
         Gas(j)%Zcrit = GasR(ID(j))%Zcrit 
!      
         Gas(j)%Omega = GasR(ID(j))%Omega       
         Gas(j)%MolWt = GasR(ID(j))%MolWt       
         Gas(j)%DMom  = GasR(ID(j))%DMom       
!      
      END DO
! 
!***********************************************************************      
!*                                                                     *
!*         Mapping the EOS coefficients to the reference ones          *
!*                                                                     *
!***********************************************************************      
!
      DO_Outer: DO i=1,NumCom
! 
         DO_Inner: DO j=1,NumCom
! 
            IF(EOS_Type.EQ.'SRK') THEN
               Kij_SC(i,j) = Kij_S(ID(i),ID(j))
            ELSE IF(EOS_Type(1:2).EQ.'PR') THEN
               Kij_PC(i,j) = Kij_P(ID(i),ID(j))
            ELSE 
               Kij_RC(i,j) = Kij_R(ID(i),ID(j))
            END IF 
! 
         END DO DO_Inner
! 
      END DO DO_Outer
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'RGasSetup         1.0    8 April     2004',6X, &
              'Mapping to the built-in reference ',    &
              'parameter data set for real gas behavior') 
!
 6005 FORMAT(//,20('ERROR-'),//, &
              T20,'R U N   A B O R T E D',/,&
              T20,'The phase indicator in parameter PHASE = ',a1,/,&
              T20,'It must be either L (Liquid), or G (Gas) ',//,&
              T20,'CORRECT AND TRY AGAIN',//,     &
               20('ERROR-'))
 6010 FORMAT(T5,'!!!!!   The type of EOS ',&
               ' equation EOS_Type = ',a3,&
               ' is not an available option   !!!!!',//,&
            T5,'====>   THE RUN WAS ABORTED - CORRECT AND TRY AGAIN')
 6015 FORMAT(T5,'!!!!!   The component ',a3,&
               ' is not an available option   !!!!!',//,&
            T5,'====>   THE RUN WAS ABORTED - CORRECT AND TRY AGAIN')


! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of RGasSetup
! 
! 
      RETURN
!      
      END SUBROUTINE RGasSetup
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE Zcomp(PP,TT,S_Molal,G_MoleF, &
                      ZLiq,ZGas,rho_G,rho_L,ixx)
! 
! ... Modules to be used 
! 
         USE RefRGas_Param, ONLY: NumCom
! 
         USE RefRGas_Param
! 
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*                  Computation of compressibility Z                   *     
!*                                                                     *
!*                    Version 1.00 - 9 April, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      USE MPI_PARAM
      IMPLICIT NONE
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(NumCom), INTENT(IN)  :: G_MoleF
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN)  :: PP,TT,S_Molal
      REAL(KIND = 8), INTENT(OUT) :: ZLiq,ZGas,rho_G,rho_L
! 
      REAL(KIND = 8) :: axx,GG_sum,Sum_J,TJI,Sum_K,TKI
      REAL(KIND = 8) :: ALO,BLO,AA0,AA1,AA2
      REAL(KIND = 8) :: Zij,Vij,Tij,Pij,Aij,XMN,XMX,SumT
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(OUT) :: ixx
! 
      INTEGER :: ICALL = 0
      INTEGER :: isol  = 0    ! Ensuring the use of standard formulation in SRK
! 
      INTEGER :: i,i1,j,j1,k,k1
! 
      SAVE ICALL,isol
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=> Main body of Zcomp
! 
! 
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .and. mpi_rank==0 .and. MPI_Rank==0) WRITE(11,6000)                                      
! 
! -------
! ... Initialization - CAREFUL! Whole array operation
! -------
! 
      Y(1:NumCom) = G_MoleF(1:NumCom)   
! 
!***********************************************************************      
!*                                                                     *
!*                  For the Soave-Redlich-Kwong EOS                    *
!*                                                                     *
!***********************************************************************      
!
      IF_EOS: IF(EOS_Type.EQ.'SRK') THEN
!
         DoNComp: DO i=1,NumCom
            RK(i)  = 4.8508d-1 &
                   +1.55171d0*Gas(i)%Omega&
                   -1.5613d-1*Gas(i)%Omega*Gas(i)%Omega
            TR(i)  = TT/Gas(i)%TCrit
            axx    = 1.0d0+RK(i)*(1.0d0-SQRT(TR(i)))
            ALF(i) = axx*axx
! 
            AxAL(i) = 4.2747d-1*ALF(i)*Rgas*Rgas &
                              *Gas(i)%TCrit*Gas(i)%TCrit&
                              /Gas(i)%PCrit
            BB(i)   = 8.664d-2*Rgas*Gas(i)%TCrit/Gas(i)%PCrit
         END DO DoNComp
!
! ----------
! ...... Alternative formulation
! ----------
!
         IF_Formulation: IF(isol == 9 .AND. NumCom > 1) THEN
!
            GG_sum = 0.0d0
!
            DO 20 i=1,NumCom
               i1  = ID(i)
!
               Sum_J = 0.0d0
               DO 12 j=1,NumCom
                  j1    = ID(j)
                  TJI   = G_1p(j1,i1)/TT+G_2p(j1,i1)
                  Sum_J = Sum_J &
                        +Y(j)*BB(j)*TJI*DEXP(-TJI*Ex_a(j1,i1))
   12          CONTINUE
!
               Sum_K = 0.0d0
               DO 15 k=1,NumCom
                  k1    = ID(k)
                  TKI   = G_1p(k1,i1)/TT+G_2p(k1,i1)
                  Sum_K = Sum_K&
                        +Y(K)*BB(K)*DEXP(-TKI*Ex_a(k1,i1))
   15          CONTINUE
!
               GG_sum = GG_sum+Y(I)*Sum_J/Sum_K
!
   20       CONTINUE
!
            AxAL_m = 0.0d0
            BLO    = 0.0d0
            ALO    = 0.0d0
            DO 25 i=1,NumCom
               BLO = BLO+Y(i)*BB(i)
               ALO = ALO+Y(i)*AxAL(i)/BB(i)
   25       CONTINUE
!
            AxAL_m = BLO*(ALO-(GG_sum/(LOG(2.0d0))))
!
            AUP = AxAL_m*PP/(Rgas*Rgas*TT*TT)
            BUP = BLO*PP/(Rgas*TT)
!            write(2,8801) 
 8801 format(T2,'Point 1 --->')
!
! ----------
! ...... Standard formulation
! ----------
!
         ELSE
!
! ......... Single component
!
            IF(NumCom.EQ.1) THEN
               AUP = AxAL(1)*PP/(Rgas*Rgas*TT*TT)
               BUP = BB(1)*PP/(Rgas*TT)
!
! ......... Mixture
!
            ELSE
               AxAL_m = 0.0d0
               BLO    = 0.0d0
               DO 100 I=1,NumCom
                  BLO = BLO+Y(I)*BB(I)
                  DO 50 J=1,NumCom
                     AxAL_m = AxAL_m &
                            +Y(I)*Y(J)&
                                 *(1.0d0-Kij_SC(I,J))&
                                 *SQRT(AxAL(I)*AxAL(J))
   50             CONTINUE
  100          CONTINUE
               AUP = AxAL_m*PP/(Rgas*Rgas*TT*TT)
               BUP = BLO*PP/(Rgas*TT)
            END IF
!            write(2,8802)
 8802 format(T2,'Point 2 --->')
!
         END IF IF_Formulation
!
! ----------
! ...... The parameters of the cubic equation
! ----------
!
         AA2 = -1.0d0
         AA1 =  AUP-BUP-BUP*BUP
         AA0 = -AUP*BUP
! 
!***********************************************************************      
!*                                                                     *
!*                     For the Peng-Robinson EOS                       *
!*                                                                     *
!***********************************************************************      
!
      ELSE IF(EOS_Type.EQ.'PR') THEN
! 
         DO 105 I=1,NumCom
!
            TR(i) = TT/Gas(i)%TCrit
!
! -------------
! ......... For liquid systems in the presence of H2O
! -------------
!
            IF(phase == 'L' .AND. iH2O == 1) THEN
! 
               IF(ID(i) <= 3) THEN
                  Kij_P(id(i),8) = PR_KW1(GasR(id(i))%Omega, &
                                         S_Molal,TR(i))
                  Kij_P(8,id(i)) = Kij_P(id(i),8)
               ELSE IF(ID(i) == 4) THEN
                  Kij_P(4,8) = PR_KW2(TR(i))
                  Kij_P(8,4) = Kij_P(4,8)
               ELSE IF(ID(i) == 5) THEN
                  Kij_P(5,8) = PR_KW3(S_Molal,TR(i))
                  Kij_P(8,5) = Kij_P(5,8)
               ELSE IF(ID(i) == 6 .OR. ID(i) == 7) THEN
                  Kij_P(ID(i),8) = PR_KW4(S_Molal,TR(i))
                  Kij_P(8,ID(i)) = Kij_P(ID(i),8)
               ELSE
                  CONTINUE
               END IF
! 
            END IF
!
! -------------
! ......... For gas systems in the presence of H2O and H2S
! -------------
!
            IF(   phase == 'G' .AND. iH2O  == 1 &
           .AND. iH2S  == 1   .AND. ID(i) == 4) THEN
               Kij_P(4,8) = 0.19031d0-0.05965d0*TR(4)
               Kij_P(8,4) = Kij_P(4,8)
            END IF
!
  105    CONTINUE
! 
! 
! 
         DO 110 I=1,NumCom
            ALF(I)  = A_PR(TR(i),S_Molal,Gas(i)%Omega,i)
            AxAL(I) = 4.5724d-1*ALF(I)*Rgas*Rgas &
                              *Gas(i)%TCrit*Gas(i)%TCrit&
                              /Gas(i)%PCrit
            BB(I)   = 7.78d-2*Rgas*Gas(i)%TCrit/Gas(i)%PCrit
  110    CONTINUE
! 
! 
! 
         IF(NumCom.EQ.1) THEN
! 
! ......... For single gases
! 
            AUP = AxAL(1)*PP/(Rgas*Rgas*TT*TT)
            BUP = BB(1)*PP/(Rgas*TT)
!
         ELSE
! 
! ......... For mixtures
! 
            AxAL_m = 0.0d0
            BLO    = 0.0d0
!
            DO 200 I=1,NumCom
               BLO = BLO+Y(I)*BB(I)
               DO 150 J=1,NumCom
                  AxAL_m = AxAL_m &
                         +Y(I)*Y(J)&
                              *(1.0d0-Kij_PC(I,j))&
                              *SQRT(AxAL(I)*AxAL(j))
  150          CONTINUE
  200       CONTINUE
!
            AUP = AxAL_m*PP/(Rgas*Rgas*TT*TT)
            BUP = BLO*PP/(Rgas*TT)
!
         END IF
!
! ----------
! ...... The parameters of the cubic equation
! ----------
!
         AA2 = -(1.0d0-BUP)
         AA1 =  AUP-3.0d0*BUP*BUP-2.0d0*BUP
         AA0 = -BUP*(AUP-BUP-BUP*BUP)
! 
!***********************************************************************      
!*                                                                     *
!*                     For the Redlich-Kwong EOS                       *
!*                                                                     *
!***********************************************************************      
!
      ELSE 
! 
         DO 210 I=1,NumCom
            AxAL(I) = 4.2748d-1*Rgas*Rgas &
                              *(Gas(i)%TCrit**2.5d0)&
                              / Gas(i)%PCrit
            BB(I)   = 8.664d-2*Rgas*Gas(i)%TCrit &
                                  /Gas(i)%PCrit
  210    CONTINUE
! 
! ----------
! ...... For single gases            
! ----------
! 
         IF(NumCom.EQ.1) THEN
            AUP = AxAL(1)*PP/(Rgas*Rgas*TT*TT*SQRT(TT))
            BUP = BB(1)*PP/(Rgas*TT)
! 
! ----------
! ...... For mixtures            
! ----------
! 
         ELSE
! 
            AxAL_m = 0.0d0
            BLO    = 0.0d0
! 
            DO 300 I=1,NumCom
! 
               BLO = BLO+Y(i)*BB(i)
! 
               DO 250 J=1,NumCom
! 
                  IF(i.EQ.j) THEN
                     AxAL_m = AxAL_m+Y(I)*Y(J)*AxAL(I)
                  ELSE
                     IF(EOS_Type(3:3).EQ.'a') THEN 
                        Zij = 5.0d-1*(Gas(i)%ZCrit+Gas(j)%ZCrit)
                        Vij = (5.0d-1*( Gas(i)%VCrit**(1.0d0/3.0d0)&
                                      +Gas(j)%VCrit**(1.0d0/3.0d0)&
                                    )&
                             )**3.0d0
                        Tij = (1.0d0-Kij_RC(i,j))*SQRT( Gas(i)%TCrit&
                                                      *Gas(j)%TCrit)
                        Pij = Rgas*Zij*Tij/Vij
                        Aij = 4.2748d-1*Rgas*Rgas &
                                      *Tij*Tij*SQRT(Tij)/Pij
                     ELSE
                        Aij = SQRT(AxAL(i)*AxAL(j))
                     END IF
                     AxAL_m = AxAL_m+Y(i)*Y(j)*Aij
! 
                  END IF
! 
  250          CONTINUE
! 
  300       CONTINUE
! 
            AUP = AxAL_m*PP/(Rgas*Rgas*TT*TT*SQRT(TT))
            BUP = BLO*PP/(Rgas*TT)
! 
         END IF
!
! ----------
! ...... The parameters of the cubic equation
! ----------
!
         AA2 = -1.0d0
         AA1 =  AUP-BUP-BUP*BUP
         AA0 = -AUP*BUP
! 
! 
! 
      END IF IF_EOS
! 
!***********************************************************************      
!*                                                                     *
!*                 SOLVE THE CUBIC EQUATION OF STATE                   *
!*                                                                     *
!***********************************************************************      
!
      ZLiq = 0.0d0  ! Initialization
      ZGas = 0.0d0
! 
      CALL CROOT(AA2,AA1,AA0,XMX,XMN,ixx)
! 
!      write(2,6001) XMX,XMN,ixx
 6001 FORMAT(t3,'XMAX=',1pe20.13,2x,'XMIN=',1pe20.13,2x,'ixx=',i3)
! 
! -------
! ... Determine the compressibility factor
! -------
! 
      IF(ixx == 3) THEN
         ZLiq = XMN
         ZGas = XMX
      ELSE
         ZLiq = XMX
         ZGas = XMX
      END IF
! 
! -------
! ... Compute the densities
! -------
! 
      SumT = SUM(Y(1:NumCom)*Gas(1:NumCom)%MolWt)
! 
      rho_G = 1.0d3*PP*SumT/(ZGas*Rgas*TT)
      rho_L = 1.0d3*PP*SumT/(ZLiq*Rgas*TT)
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'Zcomp             1.0    9 April     2004',6X, &
              'Computation of the compressibility factor Z of ',    &
              'a real gas') 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of Zcomp
! 
! 
      RETURN
!
      END SUBROUTINE Zcomp
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE Gas_Fugacity(PP,TT,ZZ,N_Com,FugCo)
! 
         USE RefRGas_Param
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*             Computation of fugacity coefficient PHI_F               *     
!*                                                                     *
!*                   Version 1.00 - 9 April, 2004                      *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      USE MPI_PARAM
      IMPLICIT NONE
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: N_Com
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: i,j
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(N_Com), INTENT(OUT) :: FugCo
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN) :: PP,TT,ZZ
! 
      REAL(KIND = 8) :: PHI0,Ai,Bi,AHAF
! 
      SAVE ICALL
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of Gas_Fugacity
! 
! 
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)                                      
! 
! -------
! ... Initialization - CAREFUL! Whole array operation
! -------
! 
      Phi_F = 0.0d0   
! 
      IF_EOS: IF(EOS_Type.EQ.'SRK') THEN
! 
!***********************************************************************      
!*                                                                     *
!*                  For the Soave-Redlich-Kwong EOS                    *
!*                                                                     *
!***********************************************************************      
!
         IF(NumCom == 1) THEN
! 
! ----------
! ...... For single gases            
! ----------
! 
            PHI0     = ZZ-1.0d0-LOG(ZZ-BUP)-(AUP/BUP)*LOG(1.0d0+BUP/ZZ)
            Phi_F(1) = DEXP(PHI0)
! 
         ELSE
! 
! ----------
! ...... For mixtures            
! ----------
! 
            DO 400 i=1,NumCom
! 
               Bi   = BB(i)*PP/Rgas/TT
               AHAF = 0.0d0
! 
               DO 350 j=1,NumCom
                  AHAF = AHAF &
                       +Y(j)*(1.0d0-Kij_SC(i,j))&
                            *(SQRT(AxAL(i)*AxAL(j)))
  350          CONTINUE
! 
               PHI0   = (Bi/BUP)*(ZZ-1.0d0)-LOG(ZZ-BUP) &
                      +(AUP/BUP)*(Bi/BUP-2.0d0*AHAF/AxAL_m)&
                                *LOG(1.0d0+BUP/ZZ)
               Phi_F(i) = DEXP(PHI0)
! 
  400       CONTINUE
! 
         END IF
! 
!***********************************************************************      
!*                                                                     *
!*                     For the Peng-Robinson EOS                       *
!*                                                                     *
!***********************************************************************      
!
      ELSE IF(EOS_Type.EQ.'PR') THEN
! 
! ----------
! ...... For single gases            
! ----------
! 
         IF(NumCom.EQ.1) THEN
            PHI0   = ZZ-1.0d0-LOG(ZZ-BUP) &
                     -(AUP/(BUP*SQRT(8.0d0)))&
                       *LOG((ZZ+2.414d0*BUP)/(ZZ-0.414d0*BUP))
            Phi_F(1) = DEXP(PHI0)
! 
! ----------
! ...... For mixtures            
! ----------
! 
         ELSE
! 
            DO 500 i=1,NumCom
! 
               Bi   = BB(i)*PP/Rgas/TT
               AHAF = 0.0d0
! 
               DO 450 J=1,NumCom
                  AHAF = AHAF &
                       +Y(j)*(1.0d0-Kij_PC(i,j))&
                            *(SQRT(AxAL(i)*AxAL(j)))
  450          CONTINUE
! 
               PHI0   = (Bi/BUP)*(ZZ-1.0d0)-LOG(ZZ-BUP) &
                      +(AUP/BUP/2.828d0)&
                         *(Bi/BUP-2.0d0*AHAF/AxAL_m)&
                         *LOG((ZZ+2.414d0*BUP)/(ZZ-0.414d0*BUP))
               Phi_F(i) = DEXP(PHI0)
! 
  500       CONTINUE
! 
         END IF
! 
!***********************************************************************      
!*                                                                     *
!*                     For the Redlich-Kwong EOS                       *
!*                                                                     *
!***********************************************************************      
!
      ELSE 
! 
! ----------
! ...... For single gases            
! ----------
! 
         IF(NumCom.EQ.1) THEN
! 
            PHI0     = ZZ-1.0d0-LOG(ZZ-BUP)-(AUP/BUP)*LOG(1.0d0+BUP/ZZ)
            Phi_F(1) = DEXP(PHI0)
! 
! ----------
! ...... For mixtures            
! ----------
! 
         ELSE
!
            DO 600 i=1,NumCom
               Ai     = AxAL(i)*PP/(Rgas*Rgas*TT*TT*SQRT(TT))
               Bi     = BB(i)*PP/Rgas/TT
               PHI0   = (Bi/BUP)*(ZZ-1.0d0)-LOG(ZZ-BUP) &
                      +(AUP/BUP)*(Bi/BUP-2.0d0*SQRT(Ai/AUP))&
                               *LOG(1.0d0+BUP/ZZ)
               Phi_F(i) = DEXP(PHI0)
  600       CONTINUE
!
         END IF
! 
! 
! 
      END IF IF_EOS
! 
! -------
! ... Preparing output values - CAREFUL! Whole array operation
! -------
! 
      FugCo(1:N_Com) = Phi_F(1:N_Com)   
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'Gas_Fugacity      1.0    9 April     2004',6X, &
              'Computation of the fugacity coefficient of ',   & 
              'a real fluid') 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of Gas_Fugacity
! 
! 
      RETURN
!      
      END SUBROUTINE Gas_Fugacity
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE Gas_Solubility(Pres,Temp,S_Molal,S_IonStr,N_Ions,Ntot, &
                               X_Mol,LogK)
! 
         USE RefRGas_Param
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*       Computation of solubility in aqueous phase for a given        *     
!*          pressure, temperature and gas phase composition            *
!*                                                                     *
!*      Solubilities are computed from equilibrium constants and       *
!*             activity coefficients calculated externally             *
!*                                                                     *
!*                     Version 1.0 - 10 April 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      USE MPI_PARAM
      IMPLICIT NONE
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(Ncom), INTENT(OUT) :: X_Mol  ! Mole fraction in the liquid phase
      REAL(KIND = 8), DIMENSION(Ncom), INTENT(OUT) :: LogK   ! = LOG10(K), K: equilibrium constant
! 
      REAL(KIND = 8), DIMENSION(Ncom) :: X_Molal             ! Molality of dissolved gas in aqueous phase
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN)  :: Pres, &     ! Pressure (Pa)
                                    Temp,  &   ! Temperature (K)
                                    S_Molal,&  ! Salt molality
                                    S_IonStr  ! Ionic strength of the solution
!                                              ! =  0.5 Sum (Ion_Molality * IonCharge**2)
!                                              !    For NaCl: S_Molal = S_IonStr
!
      REAL(KIND = 8) :: P_bar,gneut,gneut0,gneut2,S_Mol,TotX_Molal     
! 
! -------
! ... Integer arrays
! -------
! 
      INTEGER, PARAMETER, DIMENSION(Ncom) :: icor =  &
                                            (/ 1,1,1,0,0,0,1,0,0 /)  
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: Ntot    ! Number of components
      INTEGER, INTENT(IN) :: N_Ions  ! Number of ions of dissolved salt
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: n,ns,k,nsw
! 
      SAVE ICALL
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of Gas_Solubility
! 
! 
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)                                      
! 
! -------
! ... Initializations - CAREFUL! Whole array operations
! -------
! 
      X_Mol     = 0.0d0    ! Mole fraction in aqueous phase
      X_Molal   = 0.0d0    ! Molality in aqueous phase
      LogK      = 0.0d0    ! = LOG10(K), K: equilibrium constant 
!
! -------
! ... Compute the activity coefficients 
! -------
!
      CALL AqActivity(Pres,Temp,S_Molal,S_IonStr,Ntot)   
!
! -------
! ... Compute the equilibrium constants (for Liquid-Gas equilibrium)
! ... For the reaction  A(gas) <==> A(aq) we have  
! ...    logK = log(activity of Aaq) - log(fugacity of Agas)
! ...         = log(X_Molal*ActCo) - log(Pres*Y*Phi_F)
! -------
!
      CALL EqlbConst_Sol(Pres,Temp,Ntot,LogK)
! 
! **********************************************************************
! *                                                                    *
! *               Compute molalities and mole fractions                *
! *                                                                    *
! **********************************************************************
!
!
      gneut  = 1.0d0           ! Initializations
      gneut0 = 1.0d0
!
      P_bar  = Pres*1.0d-5     ! Pressure in bars     
!
! -------
! ... Iteration process - the outer loop 
! -------
!
      DO_AqMol: DO k = 1,100
!
! >>>>>>>>>>
! >>>>>>>>>>
! ...... The component loop 
! >>>>>>>>>>
! >>>>>>>>>>
!
         DO_NumCom: DO n=1,Ntot
!
            ns=id(n)
!
            IF_NoH2O: IF(ns /= 8) THEN  
!
! ............ Compute molalities
!
               IF(icor(ns) == 1) THEN 
                  X_Molal(n) = (10.0d0**LogK(n)) &        ! Equlibrium constant
                              *P_bar*Y(n)        &       ! Partial pressure component
                              *Phi_F(n)           &      ! Gas fugacity component
                              /(ActCo(n)*gneut/gneut0)  ! Activity (in L-H2O) component
               ELSE
                  X_Molal(n) = (10.0d0**LogK(n)) &        ! Equlibrium constant
                              *P_bar*Y(n)        &       ! Partial pressure component          
                              *Phi_F(n)           &      ! Gas fugacity component
                              /ActCo(n)                 ! Activity (in L-H2O) component
               ENDIF
!
            ELSE
               nsw = n     ! Determine the water index     
            ENDIF IF_NoH2O
!
         END DO DO_NumCom
!
! <<<<<<<<<<
! <<<<<<<<<<
! ...... End of the component loop 
! <<<<<<<<<<
! <<<<<<<<<<
!
         TotX_Molal = SUM(X_Molal)      ! The sum of all gas molalities
!
         gneut2 = 1.0d0/( 1.0d0 &
                        +(TotX_Molal+N_Ions*S_Molal)/55.50868d0)
!
         gneut0 = 1.0d0/( 1.0d0 &
                        +(TotX_Molal)/55.50868d0)
!
         IF_Converge: IF(ABS(gneut2-gneut) > 1.0d-9 ) THEN   ! If convergenec criterion not met, ...
            gneut = gneut2                                   ! ..., continue iterating, ...
         ELSE                                                ! ..., otherwise, ...
            EXIT DO_AqMol                                    ! ..., exit loop
         ENDIF IF_Converge
!
! ----------
! ...... End of the component loop 
! ----------
!
      END DO DO_AqMol
!
! -------
! ... Compute mole fractions of dissolved gases and salt
! ... CAREFUL! Whole array operation
! -------
!
      X_Mol = X_Molal/(TotX_Molal+N_Ions*S_Molal+55.50868d0)          
!
! -------
! ... Compute mole fractions of dissolved salt
! -------
!
      S_Mol = N_Ions*S_Molal/(TotX_Molal+N_Ions*S_Molal+55.50868d0)  
!
! -------
! ... Compute water molality and mole fraction
! -------
!
      X_Molal(nsw) = 55.50868d0                      ! H2O molality
      X_Mol(nsw)   = 1.0d0-SUM(X_Mol)-N_Ions*S_Mol   ! H2O mass fraction
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'GasSolubility     1.0   10 April     2004',6X, &
              'Computation of solubility in aqueous phase ',   &     
              'for a given P, T and gas phase composition') 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of Gas_Solubility
! 
! 
      RETURN
!
      END SUBROUTINE Gas_Solubility
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE AqActivity(Pres,Temp,S_Molal,S_IonStr,Ntot)
! 
         USE RefRGas_Param
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*      Computation of activity coefficients of dissolved gases        *     
!*                      in an aqueous solution                         *
!*                                                                     *
!*  Based on Aquater's Routine V1.5 beta  -  February 2004             *
!*  Uses salting-out coefficients of Cramer (1982) for O2              *
!*  Activity coefficient of Drummond (1981) for CO2 and H2S            *
!*  Salting-out coefficients of Cramer (1982) for CH4                  * 
!*  For heavier alkanes, alting-out coefficients of Cramer (1982)      * 
!*  for CH4, adjusted as suggested by Soreide and Whitson (1992)       *
!*                                                                     *
!*                    Version 1.0 - April 12, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      USE MPI_PARAM
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN)  :: Pres, &     ! Pressure (Pa)
                                    Temp,  &   ! Temperature (K)
                                    S_Molal,&  ! Salt molality
                                    S_IonStr  ! Ionic strength of the solution

! 
      REAL(KIND = 8) :: T_C,SO_Para     
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: Ntot  ! Number of components
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: n,ns
! 
      SAVE ICALL
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of AqActivity
! 
! 
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)                                      
! 
! ... Temperature in C  
! 
      T_C = Temp-273.16d0                                                               
! 
! -------
! ... Initialization of activity coefficients of dissolved gases in an aqueous solution
! -------
! 
      ActCo = 1.0d0        ! CAREFUL! Whole array operation
! 
! **********************************************************************
! *                                                                    *
! *                      In the absence of salt                        *
! *                                                                    *
! **********************************************************************
!
      IF(S_Molal == 0.0d0) RETURN   ! Return activity coefficients = 1.0
! 
! **********************************************************************
! *                                                                    *
! *                        The component loop                          *
! *                                                                    *
! **********************************************************************
!
      DO_NumCom: DO n=1,Ntot
!
         ns = id(n)
!
! ----------
! ...... The various options for computing the activity coefficients
! ----------
!
         CASE_ActCoef: SELECT CASE (ns)
!
         CASE(1,7)   ! Use polynomial regression of Ks=f(T)
!
            SO_Para = F_2(GasR(ns)%SOCo(1),&
     &                    GasR(ns)%SOCo(2),&
     &                    GasR(ns)%SOCo(3),&
     &                    GasR(ns)%SOCo(4),&
     &                    GasR(ns)%SOCo(5),&
     &                    GasR(ns)%SOCo(6),&
     &                    T_C)
!
            ActCo(n) = 10.0d0**(SO_Para*S_IonStr)
!
         CASE(2,3)  ! Use CH4 polynomial regression of Ks=f(T) + Soreide & Whitson
!
            SO_Para = F_2(GasR(1)%SOCo(1), &     ! Corresponds to that of CH4
     &                    GasR(1)%SOCo(2),&
     &                    GasR(1)%SOCo(3),&
     &                    GasR(1)%SOCo(4),&
     &                    GasR(1)%SOCo(5),&
     &                    GasR(1)%SOCo(6),&
     &                    T_C)
!
            SO_Para  = SO_Para                &  ! Adjust value
     &                +4.45d-4*( GasR(ns)%T_b  & 
     &                          -GasR(1)%T_b)   
!
            ActCo(n) = 10.0d0**(SO_Para*S_IonStr)
!
         CASE(4,5)   ! Use Drummond's equation 
!
            SO_Para = ( GasR(ns)%SOCo(1) &
     &                 +GasR(ns)%SOCo(2)*Temp&
     &                 +GasR(ns)%SOCo(3)/Temp&
     &                )*S_Molal&
     &               -( GasR(ns)%SOCo(4) &
     &                 +GasR(ns)%SOCo(5)*Temp&
     &                 +GasR(ns)%SOCo(6)*Pres/1.01325d5&
     &                )*S_IonStr/(1.0d0+S_IonStr)
!
            ActCo(n) = EXP(SO_Para)
!
         CASE(6,8,9) ! Use constant Ks
!
            ActCo(n) = 10.0d0**(GasR(ns)%SOCo(1)*S_IonStr)
!
         END SELECT CASE_ActCoef
!
      END DO DO_NumCom
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'AqActivity        1.0   12 April     2004',6X,&
     &         'Computation of activity coefficients of dissolved ',&
     &         'gases in an aqueous solution') 
! 
! 
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of main body of AqActivity
! 
! 
      RETURN
!
!
!***********************************************************************
!*                                                                     *
!*                        INTERNAL PROCEDURES                          *
!*                                                                     *
!***********************************************************************
!
!
      CONTAINS
! 
! ----------
! ...... Double precision functions
! ----------
! 
         REAL(KIND = 8) FUNCTION F_2(a,b,c,d,e,f,T)
! 
            REAL(KIND = 8) :: a,b,c,d,e,f,T      
!
            F_2 = a+(b+(c+(d+(e+f*T)*T)*T)*T)*T
!
         END FUNCTION F_2
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=> End of the F_2 Internal Function 
! 
! 
      END SUBROUTINE AqActivity
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE EqlbConst_Sol(Pres,Temp,Ntot,LogK)
! 
         USE RefRGas_Param
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*     Computation of equilibrium constants for a given pressure,      *     
!*               temperature and gas phase composition                 *
!*                                                                     *
!*   Equilibrium constants computed from Henry's Law constants at a    *
!*   given P,T, and are calculated using the equation of state from    *
!*    Atkinfiev and Diamond (2003) Geochimica et Cosmochimica Acta,    * 
!*               v.67, 613-627, Equations 15 and 17                    *
!*                                                                     *
!*                   Version 1.0 - February 18, 2004                   *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      USE MPI_PARAM
      IMPLICIT NONE
! 
! -------
! ... Double precision parameter arrays (Data from Spycher et al. [2003])
! -------
! 
      REAL(KIND = 8), PARAMETER, DIMENSION (5)  :: CO2_CoS =  &! Log(K) coefficients for 
     &   (/ -1.189d0, -1.304d-2, 5.446d-5, 0.d0, 0.0d0 /)     !    supercritical CO2
      REAL(KIND = 8), PARAMETER, DIMENSION (5)  :: CO2_CoL =  &! Log(K) coefficients for
     &   (/ -1.169d0, -1.368d-2, 5.380d-5, 0.d0, 0.0d0 /)     !  liquid CO2
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(Ncom), INTENT(OUT) :: LogK  ! = LOG10(K), K: equilibrium constant
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN) :: Pres, &   ! Pressure (Pa)
     &                              Temp     ! Temperature (K)
! 
      REAL(KIND = 8) :: Rcst,T_C,P_bar,Term,MolrVol,P_Ref      
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: Ntot  ! Number of components
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: n_Co2,n_H2O,n,ns
! 
      SAVE ICALL
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=> Main body of EqlbConst_Sol
! 
! 
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)                                      
!
      Rcst  = Rgas*1.0d-2     ! Convert Rgas into: bar cm^3/mol/K
      T_C   = Temp-273.16d0   ! Temperature in C                                                                 
      P_bar = Pres*1.0d-5     ! Pressure in bars     
! 
! -------
! ... Determine the refence pressure for the Poynting correction
! -------
! 
      IF_PRef: IF(T_C < 1.0d2) THEN
         P_Ref = 1.01325d0             ! = 1 bar for T_C < 100 C
      ELSE
         P_Ref = P_SatH2O(T_C)*1.0d-5  ! = the vapor pressure of water at T_C >= 100 C
      END IF IF_PRef
! 
      Term  = (P_bar-P_Ref)/(Rcst*Temp*2.303d0)
! 
! -------
! ... Initialization - CAREFUL! Whole array operation
! -------
! 
      LogK  = 0.0d0           
! 
! **********************************************************************
! *                                                                    *
! *                 Compute the equilibrium constants                  *
! *                                                                    *
! **********************************************************************
!
      n_Co2 = 0
      n_H2O = 0
!
! -------
! ... The component loop
! -------
!
      DO_NumCom: DO n=1,Ntot
!
         ns = id(n)
         IF(ns == 5) n_Co2 = n            ! Denotes presence of CO2
         IF(ns == 8) n_H2O = n            ! Denotes presence of H2O 
!
         MolrVol = F_2(GasR(ns)%MVol(1),&
     &                 GasR(ns)%MVol(2),&
     &                 GasR(ns)%MVol(3),&
     &                 GasR(ns)%MVol(4),&
     &                 GasR(ns)%MVol(5),&
     &                 GasR(ns)%MVol(6),&
     &                 T_C)
         LogK(n) = F_1(GasR(ns)%LogK(1),&
     &                 GasR(ns)%LogK(2),&
     &                 GasR(ns)%LogK(3),&
     &                 GasR(ns)%LogK(4),&
     &                 GasR(ns)%LogK(5),&
     &                 T_C)&
     &            -MolrVol*Term
!
      END DO DO_NumCom
! 
! **********************************************************************
! *                                                                    *
! *                  SPECIAL CASE: CO2 below 100 C                     *
! *      Using the more accurate data from Spycher et al. [2003]       *
! *                                                                    *
! **********************************************************************
!
      IF_TemLT100: IF(T_C <= 1.0d2 .AND. Ntot == 2 .AND.& 
     &                n_Co2 /= 0   .AND. n_H2O /= 0) THEN
! 
! ----------------
! ...... NOTE: Critical point will change if other gases are present;
! ......       In this case, use only supercritical data (good enough at T > 25 C)
! ......       Not implemented here
! ----------------
! 
         IF_CO2Liq: IF(T_C < 3.10d1) THEN   
! 
! -------------
! ......... For liquid CO2
! -------------
! 
            LogK(n) = F_1(CO2_CoL(1),CO2_CoL(2),CO2_CoL(3),&
     &                    CO2_CoL(4),CO2_CoL(5),T_C)&
     &               -3.26d1*Term
! 
         ELSE                               
! 
! -------------
! ......... For supercritical CO2
! -------------
! 
            LogK(n) = F_1(CO2_CoS(1),CO2_CoS(2),CO2_CoS(3),&
     &                    CO2_CoS(4),CO2_CoS(5),T_C)&
     &               -3.26d1*Term
! 
         END IF IF_CO2Liq
!
      END IF IF_TemLT100     
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'EqlbConst_Sol     1.0   12 April     2004',6X,&
     &         'Computation of solution equilibrium constants for ',&
     &         'a given P,T, and gas phase composition') 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=> Main body of EqlbConst_Sol
! 
! 
      RETURN
!
!
!***********************************************************************
!*                                                                     *
!*                        INTERNAL PROCEDURES                          *
!*                                                                     *
!***********************************************************************
!
!
      CONTAINS
! 
! ----------
! ...... Double precision function F_1
! ----------
! 
         REAL(KIND = 8) FUNCTION F_1(a,b,c,d,e,T)
! 
            REAL(KIND = 8) :: a,b,c,d,e,T      
!
            F_1 = a+(b+(c+(d+e*T)*T)*T)*T
!
         END FUNCTION F_1
! 
! ----------
! ...... Double precision function F_2
! ----------
! 
         REAL(KIND = 8) FUNCTION F_2(a,b,c,d,e,f,T)
! 
            REAL(KIND = 8) :: a,b,c,d,e,f,T      
!
            F_2 = a+(b+(c+(d+(e+f*T)*T)*T)*T)*T
!
         END FUNCTION F_2
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=> End of the Internal Functions 
! 
! 
      END SUBROUTINE EqlbConst_Sol
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      REAL(KIND = 8) FUNCTION P_SatH2O(T)
!
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*       FUNCTION TO COMPUTE THE SATURATED WATER VAPOR PRESSURE        *
!*             FOR A GIVEN TEMPERATURE 0 <=T <= 373.916 C              *     
!*                                                                     *
!*                   Version 1.0 - November 10, 2003                   *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
!
      USE MPI_PARAM
      IMPLICIT NONE
! 
! -------
! ... Double precision parameters
! -------
! 
      REAL(KIND = 8), PARAMETER :: A0 = -1.0575045810442507d2, &
     &                             A1 =  6.5256246146462046d2,&
     &                             A2 = -2.0295202273238559d3, &
     &                             A3 =  3.8505680776359373d3,&
     &                             A4 = -4.6193241945566390d3, &
     &                             A5 =  3.4343453745456216d3,&
     &                             A6 = -1.4482006395749671d3, &
     &                             A7 =  2.6531920643142292d2 
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN)  :: T  ! Temperature in C
! 
      REAL(KIND = 8) :: TC,XX
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0
! 
      SAVE ICALL
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of P_SatH2O
!
!
      TC    = (T+2.7316d2)/6.47096D2
! 
      XX    = A0+TC*(&
     &        A1+TC*(&
     &        A2+TC*(&
     &        A3+TC*(&
     &        A4+TC*(&
     &        A5+TC*(&
     &        A6+TC*A7))))))
! 
      P_SatH2O = 2.2064D7*EXP(XX)
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of P_SatH2O
!
!
      RETURN
!  
      END FUNCTION P_SatH2O
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      REAL(KIND = 8) FUNCTION Gas_Viscosity(PP,TT,ZZ)
!
         USE RefRGas_Param
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*       Computation of gas viscosity - Chung et al. (1984;1988)       *     
!*                                                                     *
!*                    Version 1.0 - April 11, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      USE MPI_PARAM
      IMPLICIT NONE
! 
! -------
! ... Double precision parameter arrays
! -------
! 
      REAL(KIND = 8), PARAMETER, DIMENSION(10) :: &
     &      aaa = (/ 6.3240d+00,  1.210d-03,  5.2830d+00,  6.623d+00,  &
     &               1.9745d+01, -1.900d+00,  2.4275d+01,  7.972d-01, &
     &              -2.3820d-01,  6.863d-02 /)
! 
      REAL(KIND = 8), PARAMETER, DIMENSION(10) :: &
     &      bbb = (/ 5.0412d+01, -1.1540d-03, 2.54209d+02,  3.8096d+01,  &
     &                 7.63d+00, -1.2537d+01,    3.45d+00,  1.1170d+00,   &
     &                 6.77d-02,  3.4790d-01 /)
! 
      REAL(KIND = 8), PARAMETER, DIMENSION(10) :: &
     &      ccc = (/ -5.1680d+01, -6.257d-03, -1.6848d+02, -8.464d+00, &
     &               -1.4354d+01,  4.985d+00, -1.1291d+01,  1.235d-02, &
     &               -8.1630d-01,  5.926d-01 /)
! 
      REAL(KIND = 8), PARAMETER, DIMENSION(10) :: &
     &      ddd = (/ 1.189d+03,  3.728d-02,  3.898d+03,  3.142d+01,   &
     &               3.153d+01, -1.815d+01,  6.935d+01, -4.117d+00,   &
     &               4.025d+00, -7.270d-01 /)
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(10) :: Ei
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN) :: PP,TT,ZZ
! 
      REAL(KIND = 8) :: sum_01,sum_02,sum_03,sum_04,sum_05,sum_06
      REAL(KIND = 8) :: rk_ij,Vc_i,Vc_j,sig_i,sig_j,sig_ij
      REAL(KIND = 8) :: EK_ij,WM_ij,w_ij   
      REAL(KIND = 8) :: sig_m,EK_m,T_stm,WM_m,W_m,RM4_m,RK_m  
      REAL(KIND = 8) :: Vc_m,Tc_m,RM4_r,Fc_m,Om_v,rho_0,yy_m,yy_c,G1_m   
      REAL(KIND = 8) :: G2_a,G2_b,G2_c,G2_m,eta_00,eta_ss,eta_sm 
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: i,j
! 
      SAVE ICALL
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of Gas_Viscosity
! 
! 
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)                                      
! 
! -------
! ... Initializations
! -------
! 
      sum_01 = 0.0d0    
      sum_02 = 0.0d0
      sum_03 = 0.0d0
      sum_04 = 0.0d0
      sum_05 = 0.0d0
      sum_06 = 0.0d0
! 
! **********************************************************************
! *                                                                    *
! *                      The component(s) loop(s)                      *
! *                                                                    *
! **********************************************************************
!
      DoLoop1: DO i=1,NumCom
!
         DoLoop2: DO j=1,NumCom
! 
            rk_ij  = 0.0d0
            IF(i == j) THEN
! 
               Vc_i   = 1.0d3*Gas(i)%VCrit
               sig_ij = 8.09d-1*(Vc_i**(1.0d0/3.0d0))
               EK_ij  = Gas(i)%TCrit/1.2593d0
               WM_ij  = 1.0d3*Gas(i)%MolWt
               w_ij   = Gas(i)%Omega
! 
               IF(id(i) == 8) rk_ij = 7.60d-2
               IF(id(i) == 9) rk_ij = 2.15d-1
! 
            ELSE
! 
               Vc_i   = 1.0d3*Gas(i)%VCrit
               Vc_j   = 1.0d3*Gas(j)%VCrit
               sig_i  = Vc_i**(1.0d0/3.0d0)
               sig_j  = Vc_j**(1.0d0/3.0d0)
               sig_ij = 8.09d-1*SQRT(sig_i*sig_j)
! 
               EK_ij  = SQRT(Gas(i)%TCrit*Gas(j)%TCrit)/1.2593d0
! 
               WM_ij  = 2.0d3*Gas(i)%MolWt*Gas(j)%MolWt&
     &                       /(Gas(i)%MolWt+Gas(j)%MolWt)
               w_ij   = 5.0d-1*(Gas(i)%Omega+Gas(j)%Omega)
! 
               IF((id(i) == 8 .AND. id(j) == 9) .OR.&
     &            (id(j) == 8 .AND. id(i) == 9)) rk_ij = 1.27828d-1
! 
            END IF
! 
! 
! 
            sum_01 = sum_01+Y(i)*Y(j)*sig_ij*sig_ij*sig_ij
            sum_02 = sum_02+Y(i)*Y(j)*EK_ij*sig_ij*sig_ij*sig_ij
            sum_03 = sum_03&
     &              +Y(i)*Y(j)*EK_ij*sig_ij*sig_ij*SQRT(WM_ij)
            sum_04 = sum_04&
     &              +Y(i)*Y(j)*w_ij*sig_ij*sig_ij*sig_ij
            sum_05 = sum_05&
     &              +Y(i)*Y(j)*Gas(i)%DMom*Gas(i)%DMom&
     &                        *Gas(j)%DMom*Gas(j)%DMom&
     &                        /(sig_ij*sig_ij*sig_ij)
            sum_06 = sum_06+Y(i)*Y(j)*rk_ij
! 
         END DO DoLoop2
! 
      END DO DoLoop1
! 
!
! 
      sig_m = sum_01**(1.0d0/3.0d0)
      EK_m  = sum_02/sum_01
      T_stm = TT/EK_m 
      WM_m  = sum_03*sum_03/(EK_m*EK_m*sum_01*sig_m) 
      W_m   = sum_04/sum_01 
      RM4_m = sum_01*sum_05 
      RK_m  = sum_06 
! 
      Vc_m  = sum_01/(8.09d-1*8.09d-1*8.09d-1)
      Tc_m  = 1.2593d0*EK_m
      RM4_r = 1.313d2*1.313d2*1.313d2*1.313d2*RM4_m&
     &               /(Vc_m*Vc_m*Tc_m*Tc_m)
      Fc_m  = 1.0d0-2.756d-1*W_m+5.9035d-2*RM4_r+RK_m
! 
      Om_v   = 1.16145d0*(T_stm**(-1.4874d-1))&
     &        +5.2487d-1*DEXP(-7.7320d-1*T_stm)&
     &        +2.16178d0*DEXP(-2.43787d0*T_stm)
! 
      rho_0  = 1.0d-3*PP/(ZZ*Rgas*TT)
      yy_m   = rho_0*Vc_m/6.0d0
      yy_c   = 1.0d0-yy_m
      G1_m   = (1.0d0-5.0d-1*yy_m)/(yy_c*yy_c*yy_c)
! 
! -------
! ... Compute the Ei parameters - CAREFUL! Whole aray operations
! -------
! 
      Ei = aaa+bbb*W_m+ccc*RM4_r+ddd*RK_m
! 
! -------
! ... Computation of the G2 parameters
! -------
! 
      G2_a   = Ei(1)*((1.0d0-DEXP(-Ei(4)*yy_m))/yy_m)
      G2_b   = G1_m*(Ei(2)*DEXP(Ei(5)*yy_m)+Ei(3))
      G2_c   = Ei(1)*Ei(4)+Ei(2)+Ei(3)
      G2_m   = (G2_a+G2_b)/G2_c
! 
! -------
! ... Computation of the 'eta' parameters
! -------
! 
      eta_00 = Ei(8)+(Ei(9)/T_stm)+(Ei(10)/(T_stm*T_stm))
      eta_ss = Ei(7)*yy_m*yy_m*G2_m*DEXP(eta_00)
! 
      eta_sm = SQRT(T_stm)*(Fc_m*((1.0d0/G2_m)+Ei(6)*yy_m))/Om_v&
     &        +eta_ss
! 
! -------
! ... Finally, the gas viscosity
! -------
! 
      Gas_Viscosity = 2.37865d-6*eta_sm*SQRT(WM_m*Tc_m)/(sig_m*sig_m)               
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'Gas_Viscosity     1.0   11 April     2004',6X,&
     &         'Viscosity of a real gas mixture ',&
     &         '- Method of Chung et al. (1984;1988)') 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of Gas_Viscosity
! 
! 
      RETURN
!
      END FUNCTION Gas_Viscosity
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      REAL(KIND = 8) FUNCTION Gas_ThrmConduct(PP,TT,ZZ)
!
         USE RefRGas_Param
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*        Computation of thermal conductivity of a gas mixture         * 
!*                 Method of Chung et al. (1984;1988)                  *     
!*                                                                     *
!*                    Version 1.0 - April 11, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      USE MPI_PARAM
      IMPLICIT NONE
! 
! -------
! ... Double precision parameter arrays
! -------
! 
      REAL(KIND = 8), PARAMETER, DIMENSION(7) :: &
     &      aaa = (/ 2.4166d+00, -5.0924d-01,  6.6107d+00,  1.4543d+01,  &
     &               7.9274d-01, -5.8634d+00,  9.1089d+01 /)
! 
      REAL(KIND = 8), PARAMETER, DIMENSION(7) :: &
     &      bbb = (/ 7.4824d-01, -1.5094d+00,  5.6207d+00, -8.9139d+00,  &
     &               8.2019d-01,  1.2801d+01,  1.2811d+02 /)
! 
      REAL(KIND = 8), PARAMETER, DIMENSION(7) :: &
     &      ccc = (/-9.1858d-01, -4.9991d+01,  6.4760d+01, -5.6379d+00, &
     &              -6.9369d-01,  9.5893d+00, -5.4217d+01 /)
! 
      REAL(KIND = 8), PARAMETER, DIMENSION(7) :: &
     &      ddd = (/ 1.2172d+02,  6.9983d+01,  2.7039d+01,  7.4344d+01,   &
     &               6.3173d+00,  6.5529d+01,  5.2381d+02 /)
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(7)    :: Bi
! 
      REAL(KIND = 8), DIMENSION(Ncom) :: Cv
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN) :: PP,TT,ZZ
! 
      REAL(KIND = 8) :: sum_01,sum_02,sum_03,sum_04,sum_05,sum_06
      REAL(KIND = 8) :: rk_ij,Vc_i,Vc_j,sig_i,sig_j,sig_ij
      REAL(KIND = 8) :: EK_ij,WM_ij,w_ij   
! 
      REAL(KIND = 8) :: sig_m,EK_m,T_stm,WM_m,W_m,RM4_m,RK_m  
      REAL(KIND = 8) :: Vc_m,Tc_m,RM4_r,Fc_m,Om_v,rho_0,yy_m,yy_c,G1_m   
      REAL(KIND = 8) :: G2_a,G2_b,G2_c,G2_m,C_vm,eta_m 
! 
      REAL(KIND = 8) :: WM_p,QQ,Z_Factor,alfa,beta,T_rm,Psi  
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: i,j
! 
! 
! 
      SAVE ICALL
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of Gas_ThrmConduct
! 
! 
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)                                      
! 
! -------
! ... Initializations
! -------
! 
      sum_01 = 0.0d0    
      sum_02 = 0.0d0
      sum_03 = 0.0d0
      sum_04 = 0.0d0
      sum_05 = 0.0d0
      sum_06 = 0.0d0
! 
! **********************************************************************
! *                                                                    *
! *                      The component(s) loop(s)                      *
! *                                                                    *
! **********************************************************************
!
      DoLoop1: DO i=1,NumCom
!
         DoLoop2: DO j=1,NumCom
! 
            rk_ij  = 0.0d0
            IF(i == j) THEN
! 
               Vc_i   = 1.0d3*Gas(i)%VCrit
               sig_ij = 8.09d-1*(Vc_i**(1.0d0/3.0d0))
               EK_ij  = Gas(i)%TCrit/1.2593d0
               WM_ij  = 1.0d3*Gas(i)%MolWt
               w_ij   = Gas(i)%Omega
! 
               IF(id(i) == 8) rk_ij = 7.60d-2
               IF(id(i) == 9) rk_ij = 2.15d-1
! 
            ELSE
! 
               Vc_i   = 1.0d3*Gas(i)%VCrit
               Vc_j   = 1.0d3*Gas(j)%VCrit
               sig_i  = Vc_i**(1.0d0/3.0d0)
               sig_j  = Vc_j**(1.0d0/3.0d0)
               sig_ij = 8.09d-1*SQRT(sig_i*sig_j)
! 
               EK_ij  = SQRT(Gas(i)%TCrit*Gas(j)%TCrit)/1.2593d0
! 
               WM_ij  = 2.0d3*Gas(i)%MolWt*Gas(j)%MolWt&
     &                       /(Gas(i)%MolWt+Gas(j)%MolWt)
               w_ij   = 5.0d-1*(Gas(i)%Omega+Gas(j)%Omega)
! 
               IF((id(i) == 8 .AND. id(j) == 9) .OR.&
     &            (id(j) == 8 .AND. id(i) == 9)) rk_ij = 1.27828d-1
! 
            END IF
! 
! 
! 
            sum_01 = sum_01+Y(i)*Y(j)*sig_ij*sig_ij*sig_ij
            sum_02 = sum_02+Y(i)*Y(j)*EK_ij*sig_ij*sig_ij*sig_ij
            sum_03 = sum_03&
     &              +Y(i)*Y(j)*EK_ij*sig_ij*sig_ij*SQRT(WM_ij)
            sum_04 = sum_04&
     &              +Y(i)*Y(j)*w_ij*sig_ij*sig_ij*sig_ij
            sum_05 = sum_05&
     &              +Y(i)*Y(j)*Gas(i)%DMom*Gas(i)%DMom&
     &                        *Gas(j)%DMom*Gas(j)%DMom&
     &                        /(sig_ij*sig_ij*sig_ij)
            sum_06 = sum_06+Y(i)*Y(j)*rk_ij
! 
         END DO DoLoop2
! 
      END DO DoLoop1
! 
!
! 
      sig_m = sum_01**(1.0d0/3.0d0)
      EK_m  = sum_02/sum_01
      T_stm = TT/EK_m 
      WM_m  = sum_03*sum_03/(EK_m*EK_m*sum_01*sig_m) 
      W_m   = sum_04/sum_01 
      RM4_m = sum_01*sum_05 
      RK_m  = sum_06 
! 
      Vc_m  = sum_01/(8.09d-1*8.09d-1*8.09d-1)
      Tc_m  = 1.2593d0*EK_m
      RM4_r = 1.313d2*1.313d2*1.313d2*1.313d2*RM4_m&
     &               /(Vc_m*Vc_m*Tc_m*Tc_m)
      Fc_m  = 1.0d0-2.756d-1*W_m+5.9035d-2*RM4_r+RK_m
! 
      Om_v   = 1.16145d0*(T_stm**(-1.4874d-1))&
     &        +5.2487d-1*DEXP(-7.7320d-1*T_stm)&
     &        +2.16178d0*DEXP(-2.43787d0*T_stm)
! 
      rho_0  = 1.0d-3*PP/(ZZ*Rgas*TT)
      yy_m   = rho_0*Vc_m/6.0d0
      yy_c   = 1.0d0-yy_m
      G1_m   = (1.0d0-5.0d-1*yy_m)/(yy_c*yy_c*yy_c)
! 
! -------
! ... Computate the low pressure viscosity
! -------
! 
      eta_m = (2.669d1*Fc_m*SQRT(WM_m*TT))&
     &       /(sig_m*sig_m*Om_v)
! 
! -------
! ... Compute the Bi parameters - CAREFUL! Whole aray operations
! -------
! 
      Bi = aaa+bbb*W_m+ccc*RM4_r+ddd*RK_m
! 
! -------
! ... Computation of the G2 parameters
! -------
! 
      G2_a   = Bi(1)*((1.0d0-DEXP(-Bi(4)*yy_m))/yy_m)
      G2_b   = G1_m*(Bi(2)*DEXP(Bi(5)*yy_m)+Bi(3))
      G2_c   = Bi(1)*Bi(4)+Bi(2)+Bi(3)
      G2_m   = (G2_a+G2_b)/G2_c
! 
! -------
! ... Computation of the remaining parameters
! -------
! 
      DO j=1,NumCom
! 
         Cv(j) = F_1(GasR(id(j))%A0, &             ! Individual Cv
     &               GasR(id(j))%A1,&
     &               GasR(id(j))%A2,&
     &               GasR(id(j))%A3,&
     &               GasR(id(j))%A4,&
     &               TT)-1.0d0
! 
      END DO
! 
      C_vm = 1.0d-3*Rgas&
     &             *SUM(Y(1:NumCom)*Cv(1:NumCom)) ! Mixture C_vm
! 
! -------
! ... Computation of the remaining parameters
! -------
! 
      WM_p = 1.0d-3*WM_m 
      QQ   = 3.586d-3*SQRT(Tc_m/WM_p)/(Vc_m**(2.0d0/3.0d0))
! 
      alfa = C_vm/8.314d0-1.5d0
      beta = 7.862d-1-7.109d-1*W_m+1.3168d0*W_m*W_m
! 
      T_rm     = TT/Tc_m
      Z_Factor = 2.0d0+1.05d1*T_rm*T_rm
! 
      Psi  = 1.0d0+alfa*( ( 2.15d-1&
     &                     +2.8288d-1*alfa&
     &                     -1.061d0*beta&
     &                     +2.6665d-1*Z_Factor)&
     &                   /( 6.366d-1&
     &                     +beta*Z_Factor&
     &                     +1.061*alfa*beta)&
     &                  )
! 
! -------
! ... Finally, the gas thermal conductivity
! -------
! 
      Gas_ThrmConduct = 3.12d-6*eta_m*Psi*(1.0d0/G2_m +Bi(6)*yy_m)/WM_p&
     &                 +QQ*Bi(7)*yy_m*yy_m*SQRT(T_rm)*G2_m             
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'Gas_ThrmConduct   1.0   11 April     2004',6X,&
     &         'Computation of the thermal conductivity of a real ',&
     &         'gas mixture - Method of Chung et al. (1984;1988)') 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of Gas_ThrmConduct
! 
! 
      RETURN
!
!
!***********************************************************************
!*                                                                     *
!*                        INTERNAL PROCEDURES                          *
!*                                                                     *
!***********************************************************************
!
!
      CONTAINS
! 
! ----------
! ...... Double precision function F_1
! ----------
! 
         REAL(KIND = 8) FUNCTION F_1(a0,a1,a2,a3,a4,T_k)
! 
            REAL(KIND = 8) :: a0,a1,a2,a3,a4,T_k  
!
            F_1 = a0+(a1+(a2+(a3+a4*T_k)*T_k)*T_k)*T_k
!
         END FUNCTION F_1
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=> End of the F_1 Internal Function 
! 
! 
      END FUNCTION Gas_ThrmConduct
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      REAL(KIND = 8) FUNCTION Gas_BinDiffusLoP(id1,id2,P,T_K)
!
         USE RefRGas_Param
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*       Computation of gas binary diffusivity at low pressures        *   
!*                   Method of Fuller et al. [1969]                    *   
!*                                                                     *
!*                     Version 1.0 - May 10, 2004                      *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      USE MPI_PARAM
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN) :: P,T_K
! 
      REAL(KIND = 8) :: MW_AB 
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: id1,id2
! 
      INTEGER :: ICALL = 0
! 
      SAVE ICALL
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of Gas_BinDiffusLoP
! 
! 
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)                                      
! 
! -------
! ... Compute the effective molecular weight of the mixture
! -------
! 
      MW_AB = 2.0d0/( (1.0d-3/GasR(id1)%MolWt)&
     &               +(1.0d-3/GasR(id2)%MolWt))
! 
! **********************************************************************
! *                                                                    *
! *   Compute the binary diffusivity in the gas phase - Low Pressure   *
! *                                                                    *
! **********************************************************************
!
      Gas_BinDiffusLoP =  1.43d-7*(T_K**1.75d0)&
     &                  /(1.0d-5*P*SQRT(MW_AB)&
     &                          *(&
     &                            ( (GasR(id1)%AtomDV)**(1.d0/3.d0)&
     &                             +(GasR(id2)%AtomDV)**(1.d0/3.d0)&
     &                            )**2&
     &                           )&
     &                   )
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'Gas_BinDiffusLoP  1.0   10 May       2004',6X,&
     &         'Binary diffusivity of a gas mixture at low ',&
     &         'pressures - Method of Fuller et al. [1969]') 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of Gas_BinDiffusLoP
! 
! 
      RETURN
!
      END FUNCTION Gas_BinDiffusLoP
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      REAL(KIND = 8) FUNCTION Gas_BinDiffusHiP(ix1,ix2,Y1,Y2,P,T_K)
!
         USE RefRGas_Param
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*       Computation of gas binary diffusivity at low pressures        *   
!*                   Method of Fuller et al. [1969]                    *   
!*                                                                     *
!*                     Version 1.0 - May 10, 2004                      *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      USE MPI_PARAM
      IMPLICIT NONE
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(Ncom) :: Y_inG
! 
      REAL(KIND = 8), DIMENSION(Ncom) :: Y_old
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN) :: Y1,Y2,P,T_K
! 
      REAL(KIND = 8) :: ZLiq,ZGas,rho_GL,rho_GH,rho_L,G_VisL,G_VisH
      REAL(KIND = 8) :: P_d,P_c,P_r,P_m,Omega_c,B_para,C_para
      REAL(KIND = 8) :: D_FactorHiP
! 
! -------
! ... Integer arrays
! -------
! 
      INTEGER, DIMENSION(Ncom) :: id_old
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: ix1,ix2  ! The gas numbers in the original sequence
! 
      INTEGER :: ICALL = 0, Num_old,ixx
! 
! -------
! ... Character arrays
! -------
! 
      CHARACTER(LEN = 6), DIMENSION(Ncom) :: GName 
      CHARACTER(LEN = 6), DIMENSION(2)    :: GasComp
! 
      SAVE ICALL
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of Gas_BinDiffusHiP
! 
! 
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)                                      
! 
! -------
! ... Save initial system properties - CAREFUL! Whole array operations
! -------
! 
      Num_old = NumCom
      Y_old   = Y
      id_old  = id
!
      GName(1:2) = GasR(id(1:2))%Name
! 
! -------
! ... Map into the old system     
! -------
! 
      GasComp(1) = GasR(id(ix1))%Name
      GasComp(2) = GasR(id(ix2))%Name
! 
! -------
! ... Call the set up subroutine     
! -------
! 
      CALL RGasSetup(2,GasComp(1:2),'PR ','G')
! 
! -------
! ... Assigning values to the "Y_inG" array (normalized mole fractions)     
! -------
! 
      Y_inG(1) = Y1/(Y1+Y2)      
      Y_inG(2) = 1.0d0-Y1      
! 
! **********************************************************************
! *                                                                    *
! *            Compute Low Pressure Viscosity and Density              *
! *                                                                    *
! **********************************************************************
!
      P_d = P*SUM(Y_inG)   ! Set the pressure for diffusion 
      P_m = 1.013d5        ! Set the low pressure
! 
! -------
! ... Compute gas density at TKel and a reference low pressure  P = 2.0e5 Pa     
! -------
! 
      CALL Zcomp(P_m,T_K,0.0d0,Y_inG,ZLiq,ZGas,rho_GL,rho_L,ixx)
! 
! -------
! ... Compute gas viscosity at TKel and a reference low pressure  P = 2.0e5 Pa     
! -------
! 
      G_VisL = Gas_Viscosity(P_m,T_K,ZGas)
! 
! **********************************************************************
! *                                                                    *
! *           Compute High Pressure Viscosity and Density              *
! *                                                                    *
! **********************************************************************
!
! 
! -------
! ... Compute gas density at TKel and the actual pressure P_d    
! -------
! 
      CALL Zcomp(P_d,T_K,0.0d0,Y_inG,ZLiq,ZGas,rho_GH,rho_L,ixx)
! 
! -------
! ... Compute gas viscosity at TKel and the actual pressure P_d     
! -------
! 
      G_VisH = Gas_Viscosity(P_d,T_K,ZGas)
! 
! **********************************************************************
! *                                                                    *
! *    Compute parameters of the Riazi and Whitson [1993] equation     *
! *                                                                    *
! **********************************************************************
! 
      Omega_c = Y_inG(1)*GasR(id(1))%Omega+Y_inG(2)*GasR(id(2))%Omega
      P_c     = Y_inG(1)*GasR(id(1))%PCrit+Y_inG(2)*GasR(id(2))%PCrit
      P_r     = P/P_c
!
      C_para  = -5.0d-2+1.0d-1*Omega_c
      B_para  = -2.7d-1-3.8d-1*Omega_c
! 
! **********************************************************************
! *                                                                    *
! *            Compute the diffusivity adjustment factor               *
! *                                                                    *
! **********************************************************************
! 
      D_FactorHiP = 1.07d0*(rho_GL/rho_GH) &           
     &                    *((G_VisH/G_VisL)**(B_para+C_para*P_r)) 
! 
! **********************************************************************
! *                                                                    *
! *           Compute the high-pressure binary disffusivity            *
! *                                                                    *
! **********************************************************************
! 
      Gas_BinDiffusHiP = D_FactorHiP&
     &                  *Gas_BinDiffusLoP(id(1),id(2),P_m,T_K)      
! 
! **********************************************************************
! *                                                                    *
! *                    Restore initial settings                        *
! *                                                                    *
! **********************************************************************
! 
      NumCom  = Num_old 
      Y       = Y_old 
      id      = id_old
!
      CALL RGasSetup(NumCom,GName(1:NumCom),'PR ','G')
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'Gas_BinDiffusHiP  1.0   10 May       2004',6X,&
     &         'Binary diffusivity of a gas mixture at high ',&
     &         'pressures - Methods of Fuller et al. [1969] ', &
     &         'and Riazi and Whitson [1993]') 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of Gas_BinDiffusHiP
! 
! 
      RETURN
!
      END FUNCTION Gas_BinDiffusHiP
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE HUS_Departure(TT,ZZ,DHd,DUd,DSd)
! 
         USE RefRGas_Param
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*            Computation of enthalpy and entropy departures           *     
!*                                                                     *
!*                    Version 1.0 - April 11, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      USE MPI_PARAM
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN)  :: TT,ZZ
      REAL(KIND = 8), INTENT(OUT) :: DHd,DUd,DSd
! 
      REAL(KIND = 8) :: Di,D_m
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: i,j
! 
      SAVE ICALL
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of HUS_Departure
! 
! 
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)                                      
! 
!***********************************************************************      
!*                                                                     *
!*                  For the Soave-Redlich-Kwong EOS                    *
!*                                                                     *
!***********************************************************************      
!
      IF_EOS: IF(EOS_Type.EQ.'SRK') THEN
! 
         IF(NumCom.EQ.1) THEN
! 
! ----------
! ...... For single gases            
! ----------
! 
            Di  = RK(1)*AxAL(1)*SQRT(TR(1)/ALF(1))
! 
            DUd = Rgas*TT*(AUP/BUP)*(1.0d0+Di/AxAL(1))*LOG(1.0d0+BUP/ZZ)
! 
            DHd = DUd+Rgas*TT*(1.0d0-ZZ)
! 
            DSd = Rgas*( &
     &                  -LOG(ZZ-BUP)&
     &                  +(AUP*Di/(BUP*AxAL(1)))*LOG(1.0d0+BUP/ZZ)&
     &                 )
! 
! ----------
! ...... For mixtures            
! ----------
! 
         ELSE
! 
            D_m = 0.0d0
            DO 700 I=1,NumCom
               DO 650 J=1,NumCom
                  D_m = D_m&
     &                 +RK(j)*Y(i)*Y(j)*(1.0d0-Kij_SC(i,j))&
     &                       *SQRT(&
     &                              AxAL(i)*AxAL(j)*TR(j)/ALF(j)&
     &                             )
  650          CONTINUE
  700       CONTINUE
! 
            DUd = Rgas*TT*(AUP/BUP)*(1.0d0+D_m/AxAL_m)*LOG(1.0d0+BUP/ZZ)
! 
            DHd = DUd+Rgas*TT*(1.0d0-ZZ)
! 
            DSd = Rgas*(&
     &                  -LOG(ZZ-BUP)&
     &                  +(AUP*D_m/(BUP*AxAL_m))*LOG(1.0d0+BUP/ZZ)&
     &                 )
! 
         END IF
! 
!***********************************************************************      
!*                                                                     *
!*                     For the Peng-Robinson EOS                       *
!*                                                                     *
!***********************************************************************      
!
      ELSE IF(EOS_Type.EQ.'PR') THEN
! 
! ----------
! ...... For single gases            
! ----------
! 
         IF(NumCom.EQ.1) THEN
! 
            IF(ID(1).EQ.8.AND.TR(1).LT.7.225d-1) THEN      
               Di = RK(1)*AxAL(1)/SQRT(ALF(1))
            ELSE
               Di = RK(1)*AxAL(1)*SQRT(TR(1)/ALF(1))
            END IF
! 
            DUd = Rgas*TT*(AUP/BUP/2.828d0)*(1.0d0+Di/AxAL(1))&
     &                   *LOG((ZZ+2.414d0*BUP)/(ZZ-0.414d0*BUP))
! 
            DHd = DUd+Rgas*TT*(1.0d0-ZZ)
! 
            DSd = Rgas*(&
     &                  -LOG(ZZ-BUP)&
     &                  +(AUP*Di/(2.828d0*BUP*AxAL(1)))&
     &                       *LOG((ZZ+2.414d0*BUP)/(ZZ-0.414d0*BUP))&
     &                 )
! 
! ----------
! ...... For mixtures            
! ----------
! 
         ELSE
! 
            D_m = 0.0d0
            DO 800 i=1,NumCom
! 
               DO 750 j=1,NumCom
                  IF(ID(j).EQ.8.AND.TR(j).LT.7.225d-1) THEN      
                     Di = AxAL(i)*AxAL(j)/ALF(j)
                  ELSE
                     Di = AxAL(i)*AxAL(j)*TR(j)/ALF(j)
                  END IF
                  D_m = D_m&
     &                 +RK(j)*Y(i)*Y(j)*(1.0d0-Kij_PC(I,J))&
     &                       *SQRT(Di)
  750          CONTINUE
! 
  800       CONTINUE
! 
            DUd = Rgas*TT*(AUP/BUP/2.828d0)*(1.0d0+D_m/AxAL_m)&
     &                   *LOG((ZZ+2.414d0*BUP)/(ZZ-0.414d0*BUP))
! 
            DHd = DUd+Rgas*TT*(1.0d0-ZZ)
! 
            DSd = Rgas*(&
     &                  -LOG(ZZ-BUP)&
     &                  +(AUP*D_m/(2.828d0*BUP*AxAL_m))&
     &                       *LOG((ZZ+2.414d0*BUP)/(ZZ-0.414d0*BUP))&
     &                 )
! 
         END IF
! 
!***********************************************************************      
!*                                                                     *
!*                     For the Redlich-Kwong EOS                       *
!*                                                                     *
!***********************************************************************      
!
      ELSE 
!
         DUd = Rgas*TT*1.5d0*(AUP/BUP)*LOG(1.0d0+BUP/ZZ)
!
         DHd = DUd+Rgas*TT*(1.0d0-ZZ)
!
         DSd = Rgas*(&
     &               -LOG(ZZ-BUP)&
     &               +(AUP/2.0d0/BUP)*LOG(1.0d0+BUP/ZZ)&
     &              )
! 
! 
! 
      END IF IF_EOS
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'HS_Departure      1.0   11 April     2004',6X,&
     &         'Computation of the enthalpy, internal energy and ',&
     &         'entropy departures of real gases') 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of HUS_Departure
! 
! 
      RETURN
!
      END SUBROUTINE HUS_Departure
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE HU_IdealGas(n,T0,T1,DHi,DUi)
! 
         USE RefRGas_Param
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*           Computation of ideal enthalpy and entropy changes         *     
!*                                                                     *
!*                    Version 1.0 - March 17, 2001                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
!
      USE MPI_PARAM
      IMPLICIT NONE
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(Ncom) :: Y_in
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN)  :: T0,T1
      REAL(KIND = 8), INTENT(OUT) :: DHi,DUi
! 
      REAL(KIND = 8) :: T0_2,T0_3,T0_4,T0_5
      REAL(KIND = 8) :: T1_2,T1_3,T1_4,T1_5,Hcom,Ucom
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: n
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: i,ix,i_bgn,i_end
! 
      SAVE ICALL
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of HU_IdealGas
! 
! 
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .and. MPI_RANK==0) WRITE(11,6000)                                      
! 
! -------
! ... Initializations
! -------
! 
      DHi = 0.0d0
      DUi = 0.0d0
! 
      T0_2 = T0*T0
      T0_3 = T0_2*T0
      T0_4 = T0_3*T0
      T0_5 = T0_4*T0
! 
      T1_2 = T1*T1
      T1_3 = T1_2*T1
      T1_4 = T1_3*T1
      T1_5 = T1_4*T1
! 
! -------
! ... Allow computations for both individual gases and gas mixtures
! -------
! 
      IF(n == 0) THEN
         i_bgn = 1
         i_end = NumCom
         Y_in  = Y         ! CAREFUL: Whole array operation
      ELSE
         i_bgn   = n
         i_end   = n
         Y_in(n) = 1.0d0
      END IF
! 
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*    COMPUTE ENTHALPY AND ENTROPY CHANGES BASED ON IDEAL BEHAVIOR     *
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      DO i=i_bgn,i_end
! 
         ix = ID(i)
! 
         Hcom = GasR(ix)%A0*(T1-T0)&
     &         +GasR(ix)%A1*(T1_2-T0_2)/2.0d0&
     &         +GasR(ix)%A2*(T1_3-T0_3)/3.0d0&
     &         +GasR(ix)%A3*(T1_4-T0_4)/4.0d0&
     &         +GasR(ix)%A4*(T1_5-T0_5)/5.0d0
         DHi  = DHi+Hcom*Y_in(i)
! 
         Ucom = Hcom - 1.0d0
         DUi  = DUi+Ucom*Y_in(i)
! 
      END DO
! 
! 
! 
        DHi = DHi*Rgas      
        DUi = DUi*Rgas      
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'HU_IdealGas       1.0   11 April     2004',6X,&
     &         'Computation of the ideal enthalpy and internal energy ',&
     &         'of ideal gases') 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of HU_IdealGas
! 
! 
      RETURN
!
      END SUBROUTINE HU_IdealGas
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE HUS_IdealGas(n,T0,T1,P0,P1,DHi,DUi,DSi)
! 
         USE RefRGas_Param
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*           Computation of ideal enthalpy and entropy changes         *     
!*                                                                     *
!*                    Version 1.0 - March 17, 2001                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
!
      USE MPI_PARAM
      IMPLICIT NONE
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(Ncom) :: Y_in
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN)  :: T0,T1,P0,P1
      REAL(KIND = 8), INTENT(OUT) :: DHi,DUi,DSi
! 
      REAL(KIND = 8) :: T0_2,T0_3,T0_4,T0_5
      REAL(KIND = 8) :: T1_2,T1_3,T1_4,T1_5,Hcom,Ucom,Scom
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: n
! 
      INTEGER :: ICALL = 0
! 
      INTEGER :: i,ix,i_bgn,i_end
! 
      SAVE ICALL
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of HUS_IdealGas
! 
! 
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)                                      
! 
! -------
! ... Initializations
! -------
! 
      DHi = 0.0d0
      DSi = 0.0d0
! 
      T0_2 = T0*T0
      T0_3 = T0_2*T0
      T0_4 = T0_3*T0
      T0_5 = T0_4*T0
! 
      T1_2 = T1*T1
      T1_3 = T1_2*T1
      T1_4 = T1_3*T1
      T1_5 = T1_4*T1
! 
! -------
! ... Allow computations for both individual gases and gas mixtures
! -------
! 
      IF(n == 0) THEN
         i_bgn = 1
         i_end = NumCom
         Y_in  = Y         ! CAREFUL: Whole array operation
      ELSE
         i_bgn   = n
         i_end   = n
         Y_in(n) = 1.0d0
      END IF
! 
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*    COMPUTE ENTHALPY AND ENTROPY CHANGES BASED ON IDEAL BEHAVIOR     *
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      DO i=1,NumCom
! 
         ix = ID(i)
! 
         Hcom = GasR(ix)%A0*(T1-T0)&
     &         +GasR(ix)%A1*(T1_2-T0_2)/2.0d0&
     &         +GasR(ix)%A2*(T1_3-T0_3)/3.0d0&
     &         +GasR(ix)%A3*(T1_4-T0_4)/4.0d0&
     &         +GasR(ix)%A4*(T1_5-T0_5)/5.0d0
         DHi  = DHi+Hcom*Y_in(i)
! 
         Ucom = Hcom-1.0d0
         DUi  = DUi+Ucom*Y_in(i)
! 
         Scom = GasR(ix)%A0*LOG(T1/T0)&
     &         +GasR(ix)%A1*(T1-T0)&
     &         +GasR(ix)%A2*(T1_2-T0_2)/2.0d0&
     &         +GasR(ix)%A3*(T1_3-T0_3)/3.0d0&
     &         +GasR(ix)%A4*(T1_4-T0_4)/4.0d0
         DSi  = DSi+Scom*Y_in(i)
! 
      END DO
! 
! 
! 
        DHi = DHi*Rgas      
        DUi = DUi*Rgas      
        DSi = Rgas*(DSi-LOG(P1/P0))  
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'HUS_IdealGas      1.0   11 April     2004',6X,&
     &         'Computation of the ideal enthalpy, internal energy ',&
     &         'and entropy of ideal gases') 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of HUS_IdealGas
! 
! 
      RETURN
!
      END SUBROUTINE HUS_IdealGas
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
      SUBROUTINE K_SRK
! 
         USE RefRGas_Param
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*        SETTING THE INTERACTION COEFFICIENTS IN THE SRK EOS          *     
!*                                                                     *
!*                   Version 1.00 - April 9, 2001                      *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      USE MPI_PARAM
      IMPLICIT NONE
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0
! 
      SAVE 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of K_SRK
! 
! 
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)                                      
! 
! -------
! ... Initializations - CAREFUL! Whole array operations
! -------
! 
      G_1p = 0.0d0    
      G_2p = 0.0d0
      Ex_a = 0.0d0
! 
!***********************************************************************      
!*                                                                     *
!*                       KIJ VALUES FOR METHANE                        *
!*                                                                     *
!***********************************************************************      
!
        Kij_S(1,1) = 0.0d0
!
! ===>  From Walas (Table 1.12)
! 
        Kij_S(1,2) = 0.0d0
        Kij_S(1,3) = 0.0d0
        Kij_S(1,4) = 0.0850d0
        Kij_S(1,5) = 0.0973d0
        Kij_S(1,6) = 0.0319d0
        Kij_S(1,7) = 0.0319d0
!
        Kij_S(2,1) = Kij_S(1,2)
        Kij_S(3,1) = Kij_S(1,3)
        Kij_S(4,1) = Kij_S(1,4)
        Kij_S(5,1) = Kij_S(1,5)
        Kij_S(6,1) = Kij_S(1,6)
        Kij_S(7,1) = Kij_S(1,7)
!
! ===>  ??? Guess, Using RK parameters - B
!
        Kij_S(1,8) = 3.4d-1
        Kij_S(1,9) = 1.3d-1

        Kij_S(8,1) = Kij_S(1,8)
        Kij_S(9,1) = Kij_S(1,9)   ! <=== ??? Guess - E
!
! ===>  G_1p, G_2p Values - Pedersen et al., 2001
!
        G_1p(1,8) = 4.559d3
        G_2p(1,8) =-6.54d0

        G_1p(8,1) =-5.705d2
        G_2p(8,1) = 3.46d0
      
        Ex_a(1,8) = 1.5d-1
        Ex_a(8,1) = Ex_a(1,8)
! 
!***********************************************************************      
!*                                                                     *
!*                       KIJ VALUES FOR ETHANE                         *
!*                                                                     *
!***********************************************************************      
!
        Kij_S(2,2) = 0.0d0
!
! ===>  From Walas (Table 1.12) 
!
        Kij_S(2,3) = 0.0d0
        Kij_S(2,4) = 0.0829d0
        Kij_S(2,5) = 0.1346d0
        Kij_S(2,6) = 0.0388d0
        Kij_S(2,7) = 0.0388d0

        Kij_S(3,2) = Kij_S(2,3)
        Kij_S(4,2) = Kij_S(2,4)
        Kij_S(5,2) = Kij_S(2,5)
        Kij_S(6,2) = Kij_S(2,6)
        Kij_S(7,2) = Kij_S(2,7)
!
! ===>  ??? Guess, Using RK parameters - B
!
        Kij_S(2,8) = 3.7d-1
        Kij_S(2,9) = 1.2d-1

        Kij_S(8,2) = Kij_S(2,8)
        Kij_S(9,2) = Kij_S(2,9)   ! <=== ??? Guess - E
!
! ===>  G_1p, G_2p Values - Pedersen et al., 2001
!
        G_1p(2,8) = 3.640d3
        G_2p(2,8) =-2.14d0

        G_1p(8,2) =-5.043d2
        G_2p(8,2) = 8.0d-1
      
        Ex_a(2,8) = 9.0d-2
        Ex_a(8,2) = Ex_a(2,8)
! 
!***********************************************************************      
!*                                                                     *
!*                      KIJ VALUES FOR PROPANE                         *
!*                                                                     *
!***********************************************************************      
!
        Kij_S(3,3) = 0.0d0
!
! ===>  From Walas (Table 1.12) 
!
        Kij_S(3,4) = 0.0831d0
        Kij_S(3,5) = 0.1018d0
        Kij_S(3,6) = 0.0807d0
        Kij_S(3,7) = 0.0807d0
!
        Kij_S(4,3) = Kij_S(3,4)
        Kij_S(5,3) = Kij_S(3,5)
        Kij_S(6,3) = Kij_S(3,6)
        Kij_S(7,3) = Kij_S(3,7)
!
! ===>  ??? Guess, Using RK parameters - B
!
        Kij_S(3,8) = 4.0d-1
        Kij_S(3,9) = 1.2d-1

        Kij_S(8,3) = Kij_S(3,8)
        Kij_S(9,3) = Kij_S(3,9)   ! <=== ??? Guess - E
!
! ===>  G_1p, G_2p Values - Pedersen et al., 2001
!
        G_1p(3,8) = 3.517d3
        G_2p(3,8) =-9.7d-2

        G_1p(8,3) =-1.584d3
        G_2p(8,3) =-4.42d-1
      
        Ex_a(3,8) = 7.0d-2
        Ex_a(8,3) = Ex_a(3,8)
! 
!***********************************************************************      
!*                                                                     *
!*                 KIJ VALUES FOR HYDROGEN SULFIDE                     *
!*                                                                     *
!***********************************************************************      
!
        Kij_S(4,4) = 0.0d0
!
! ===>  From Walas (Table 1.12) 
!
        Kij_S(4,5) = 0.1018d0
        Kij_S(4,6) = 0.140d0
        Kij_S(4,7) = 0.140d0

        Kij_S(5,4) = Kij_S(4,5)
        Kij_S(6,4) = Kij_S(4,6)
        Kij_S(7,4) = Kij_S(4,7)
!
! ===>  ??? Guess, Using RK parameters - B
!
        Kij_S(4,8) = 0.15d0
        Kij_S(4,9) = 0.10d0

        Kij_S(8,4) = Kij_S(4,8)
        Kij_S(9,4) = Kij_S(4,9)   ! <=== ??? Guess - E
!
! ===>  G_1p, G_2p Values - Pedersen et al., 2001
!
        G_1p(4,8) = 0.0d0
        G_2p(4,8) = 0.0d0

        G_1p(8,4) = 0.0d0
        G_2p(8,4) = 0.0d0
      
        Ex_a(4,8) = 0.0d-2
        Ex_a(8,4) = Ex_a(4,8)
! 
!***********************************************************************      
!*                                                                     *
!*                  KIJ VALUES FOR CARBON DIOXIDE                      *
!*                                                                     *
!***********************************************************************      
!
        Kij_S(5,5) = 0.0d0
!
! ===>  From Walas (Table 1.12) 
!
        Kij_S(5,6) =-0.022d0
        Kij_S(5,7) =-0.022d0

        Kij_S(6,5) = Kij_S(5,6)
        Kij_S(7,5) = Kij_S(5,7)
!
! ===>  ??? Guess, Using RK parameters - B
!
        Kij_S(5,8) = 1.4d-1
        Kij_S(5,9) = 1.00d-2

        Kij_S(8,5) = Kij_S(5,8)
        Kij_S(9,5) = Kij_S(5,9)   ! <=== ??? Guess - E
!
! ===>  G_1p, G_2p Values - Pedersen et al., 2001
!
        G_1p(5,8) = 3.932d3
        G_2p(5,8) =-5.89d-2

        G_1p(8,5) =-4.127d3
        G_2p(8,5) = 8.9d0
      
        Ex_a(5,8) = 3.0d-2
        Ex_a(8,5) = Ex_a(5,8)
! 
!***********************************************************************      
!*                                                                     *
!*                     KIJ VALUES FOR NITROGEN                         *
!*                                                                     *
!***********************************************************************      
!
        Kij_S(6,6) = 0.0d0
!
! ===>  From Walas (Table 1.12) 
!
        Kij_S(6,7) = 0.0d0
        Kij_S(7,6) = Kij_S(6,7)
!
! ===>  ??? Guess, Using RK parameters - B
!
        Kij_S(6,8) = 0.300d0
        Kij_S(6,9) = 0.050d0

        Kij_S(8,6) = Kij_S(6,8)
        Kij_S(9,6) = Kij_S(6,9)   ! <=== ??? Guess - E
!
! ===>  G_1p, G_2p Values - Pedersen et al., 2001
!
        G_1p(6,8) = 4.643d3
        G_2p(6,8) =-2.10d0

        G_1p(8,6) =-6.45d1
        G_2p(8,6) =-1.05d0
      
        Ex_a(6,8) = 8.0d-2
        Ex_a(8,6) = Ex_a(6,8)
! 
!***********************************************************************      
!*                                                                     *
!*                      KIJ VALUES FOR OXYGEN                          *
!*                                                                     *
!***********************************************************************      
!
        Kij_S(7,7) = 0.0d0
!
!-------------
! ..... Asssuming same as N2 
! ===>  ??? Guess, Using RK parameters - B
!-------------
!
        Kij_S(7,8) = 0.300d0
        Kij_S(7,9) = 0.050d0

        Kij_S(8,7) = Kij_S(7,8)
        Kij_S(9,7) = Kij_S(7,9)   ! <=== ??? Guess - E
!
! ===>  G_1p, G_2p Values - Pedersen et al., 2001
!
        G_1p(7,8) = 4.643d3
        G_2p(7,8) =-2.10d0

        G_1p(8,7) =-6.45d1
        G_2p(8,7) =-1.05d0
      
        Ex_a(7,8) = 8.0d-2
        Ex_a(8,7) = Ex_a(7,8)
! 
!***********************************************************************      
!*                                                                     *
!*                       KIJ VALUES FOR WATER                          *
!*                                                                     *
!***********************************************************************      
!
        Kij_S(8,8) = 0.0d0
! 
! ===>  ??? Guess, Using RK parameters - B
! 
        Kij_S(8,9) = 1.5d-1
        Kij_S(9,8) = Kij_S(8,9)   ! <=== ??? Guess - E
! 
!***********************************************************************      
!*                                                                     *
!*                      KIJ VALUES FOR ETHANOL                         *
!*                                                                     *
!***********************************************************************      
!
        Kij_S(9,9) = 0.0d0
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'K_SRK             1.0    9 April     2004',6X,&
     &         'Setting the interaction coefficients in the ',    &
     &         'Soave-Redlich-Kwong EOS ') 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of K_SRK
! 
! 
      RETURN
!
      END SUBROUTINE K_SRK
!
!
!
!*************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!*************************************************************************
!
!
      SUBROUTINE K_PR
! 
         USE RefRGas_Param
!
!*************************************************************************
!*************************************************************************
!*                                                                       *
!*          SETTING THE INTERACTION COEFFICIENTS IN THE PR EOS           *     
!*                                                                       *
!*                    Version 1.00 - April 9 , 2001                      *     
!*                                                                       *
!*************************************************************************
!*************************************************************************
!
      USE MPI_PARAM
      IMPLICIT NONE
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0
! 
      SAVE 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of K_P
! 
! 
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)                                      
! 
!***********************************************************************      
!*                                                                     *
!*                       KIJ VALUES FOR METHANE                        *
!*                                                                     *
!***********************************************************************      
!
        Kij_P(1,1) = 0.0d0  
!
! ===>  From Walas (Table 1.13)
!
        Kij_P(1,2) = 0.0d0
        Kij_P(1,3) = 0.0d0
!
        Kij_P(2,1) = Kij_P(1,2)
        Kij_P(3,1) = Kij_P(1,3)
!
! ===>  ??? SRK, From Walas (Table 1.12) - B
!
        Kij_P(1,4) = 0.0850d0
        Kij_P(4,1) = Kij_P(1,4)   ! <=== ??? SRK - E
!
! ===>  From Walas (Table 1.13) 
!
        Kij_P(1,5) = 0.1500d0
        Kij_P(1,6) = 0.1200d0
        Kij_P(1,7) = 0.1200d0
!
        Kij_P(5,1) = Kij_P(1,5)
        Kij_P(6,1) = Kij_P(1,6)
        Kij_P(7,1) = Kij_P(1,7)
!
! ===>  From Soreide and Whitson (1992), Table 5
!
        Kij_P(1,8) = 0.4850d0
        Kij_P(8,1) = Kij_P(1,8)
!
! ===>  ??? Guess - B
!
        Kij_P(1,9) = 0.10d0
        Kij_P(9,1) = Kij_P(1,9)   ! <=== ??? Guess - E
! 
!***********************************************************************      
!*                                                                     *
!*                       KIJ VALUES FOR ETHANE                         *
!*                                                                     *
!***********************************************************************      
!
        Kij_P(2,2) = 0.0d0     
!
! ===>  From Walas (Table 1.13)
!
        Kij_P(2,3) = 0.01d0
        Kij_P(3,2) = Kij_P(2,3)
!
! ===>  ??? SRK, From Walas (Table 1.12) - B
!
        Kij_P(2,4) = 0.0829d0
        Kij_P(4,2) = Kij_P(2,4)   ! ===> ??? SRK - E
!
! ===>  From Walas (Table 1.13) 
!
        Kij_P(2,5) = 0.1500d0
        Kij_P(2,6) = 0.1200d0
        Kij_P(2,7) = 0.1200d0
!
        Kij_P(5,2) = Kij_P(2,5)
        Kij_P(6,2) = Kij_P(2,6)
        Kij_P(7,2) = Kij_P(2,7)
!
! ===>  From Soreide and Whitson (1992), Table 5
!
        Kij_P(2,8) = 0.4920d0
        Kij_P(8,2) = Kij_P(2,8)
!
! ===>  ??? Guess - B
!
        Kij_P(2,9) = 0.10d0
        Kij_P(9,2) = Kij_P(2,9)   ! <=== ??? Guess - E
! 
!***********************************************************************      
!*                                                                     *
!*                      KIJ VALUES FOR PROPANE                         *
!*                                                                     *
!***********************************************************************      
!
        Kij_P(3,3) = 0.0d0
!
! ===>  ??? SRK, From Walas (Table 1.12) - B
!
        Kij_P(3,4) = 0.0831d0
        Kij_P(4,3) = Kij_P(3,4)   ! <=== ??? SRK - E
!
! ===>  From Walas (Table 1.13) 
!
        Kij_P(3,5) = 0.1500d0
        Kij_P(3,6) = 0.1200d0
        Kij_P(3,7) = 0.1200d0
!
        Kij_P(5,3) = Kij_P(3,5)
        Kij_P(6,3) = Kij_P(3,6)
        Kij_P(7,3) = Kij_P(3,7)
!
! ===>  From Soreide and Whitson (1992), Table 5
!
        Kij_P(3,8) = 0.5525d0
        Kij_P(8,3) = Kij_P(3,8)

! ===>  ??? Guess - B
        Kij_P(3,9) = 0.10d0
        Kij_P(9,3) = Kij_P(3,9)   ! <=== ??? Guess - E
! 
!***********************************************************************      
!*                                                                     *
!*                 KIJ VALUES FOR HYDROGEN SULFIDE                     *
!*                                                                     *
!***********************************************************************      
!
        Kij_P(4,4) = 0.0d0
! 
! ===>  ??? SRK, From Walas (Table 1.12) - B
! 
        Kij_P(4,5) = 0.1020d0
        Kij_P(4,6) = 0.140d0
        Kij_P(4,7) = 0.140d0
! 
        Kij_P(5,4) = Kij_P(4,5)
        Kij_P(6,4) = Kij_P(4,6)
        Kij_P(7,4) = Kij_P(4,7)   ! <=== ??? SRK, From Walas (Table 1.12) - E
! 
! ===>  From Soreide and Whitson (1992), Eq.17 (modified internally) 
! 
        Kij_P(4,8) = 0.19031d0
        Kij_P(8,4) = Kij_P(4,8)
! 
! ===>  ??? Guess - B
! 
        Kij_P(4,9) = 0.15d0
        Kij_P(9,4) = Kij_P(4,9)   ! <=== ??? Guess - E
! 
!***********************************************************************      
!*                                                                     *
!*                  KIJ VALUES FOR CARBON DIOXIDE                      *
!*                                                                     *
!***********************************************************************      
!
        Kij_P(5,5) = 0.0d0
! 
! ===>  ??? Guess - B
! 
        Kij_P(5,6) = 0.0d0
        Kij_P(5,7) = 0.0d0
! 
        Kij_P(6,5) = Kij_P(5,6)
        Kij_P(7,5) = Kij_P(5,7)   ! <=== ??? Guess - E
! 
! ===>  From Soreide and Whitson (1992), Table 5
! 
        Kij_P(5,8) = 0.1896d0
        Kij_P(8,5) = Kij_P(5,8)
! 
! ===>  ??? Guess - B
! 
        Kij_P(5,9) = 0.00d0
        Kij_P(9,5) = Kij_P(5,9)   ! <=== ??? Guess - E
! 
!***********************************************************************      
!*                                                                     *
!*                     KIJ VALUES FOR NITROGEN                         *
!*                                                                     *
!***********************************************************************      
!
        Kij_P(6,6) = 0.0d0
        Kij_P(6,7) = 0.0d0
! 
! ===>  From Soreide and Whitson (1992), Table 5
! 
        Kij_P(6,8) = 0.4778d0
        Kij_P(8,6) = Kij_P(6,8)
! 
! ===>  ??? Guess - B
! 
        Kij_P(6,9) = 0.00d0
        Kij_P(9,6) = Kij_P(6,9)   ! <=== ??? Guess - E
! 
!***********************************************************************      
!*                                                                     *
!*                      KIJ VALUES FOR OXYGEN                          *
!*                                                                     *
!***********************************************************************      
!
!...... Asssuming same as N2 
!
        Kij_P(7,7) = 0.0d0

        Kij_P(7,8) = 0.4778d0
        Kij_P(7,9) = 0.00d0

        Kij_P(8,7)  = Kij_P(7,8)
        Kij_P(9,7)  = Kij_P(7,9)
! 
!***********************************************************************      
!*                                                                     *
!*                       KIJ VALUES FOR WATER                          *
!*                                                                     *
!***********************************************************************      
!
        Kij_P(8,8) = 0.0d0
! 
! ===> v??? Guess - B
! 
        Kij_P(8,9) = 1.5d-1   ! <=== ??? Guess - E
! 
!***********************************************************************      
!*                                                                     *
!*                      KIJ VALUES FOR ETHANOL                         *
!*                                                                     *
!***********************************************************************      
!
        Kij_P(9,9) = 0.0d0
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'K_PR              1.0    9 April     2004',6X,&
     &         'Setting the interaction coefficients in the ', &   
     &         'Peng-Robinson EOS') 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of K_PR
! 
! 
      RETURN
!
      END SUBROUTINE K_PR
!
!
!
!*************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!*************************************************************************
!
!
      SUBROUTINE K_RK
! 
         USE RefRGas_Param
!
!*************************************************************************
!*************************************************************************
!*                                                                       *
!*          SETTING THE INTERACTION COEFFICIENTS IN THE RK EOS           *     
!*                                                                       *
!*                     Version 1.00 - April 9, 2001                      *     
!*                                                                       *
!*************************************************************************
!*************************************************************************
!
      USE MPI_PARAM
      IMPLICIT NONE
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0
! 
      SAVE 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of K_RK
! 
! 
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)                                      
! 
!***********************************************************************      
!*                                                                     *
!*                       KIJ VALUES FOR METHANE                        *
!*                                                                     *
!***********************************************************************      
!
        Kij_R(1,1) = 0.0d0
!
! ===>  From Walas (Table E.3) and Tsonopoulos (1974, 1980)
!
        Kij_R(1,2) = 1.0d-2
        Kij_R(1,3) = 2.0d-2
        Kij_R(1,4) = 5.0d-2
        Kij_R(1,5) = 5.0d-2
        Kij_R(1,6) = 3.0d-2
        Kij_R(1,7) = 3.0d-2
        Kij_R(1,8) = 3.4d-1
        Kij_R(1,9) = 1.3d-1
!
        Kij_R(2,1) = Kij_R(1,2)
        Kij_R(3,1) = Kij_R(1,3)
        Kij_R(4,1) = Kij_R(1,4)
        Kij_R(5,1) = Kij_R(1,5)
        Kij_R(6,1) = Kij_R(1,6)
        Kij_R(7,1) = Kij_R(1,7)
        Kij_R(8,1) = Kij_R(1,8)
        Kij_R(9,1) = Kij_R(1,9)
! 
!***********************************************************************      
!*                                                                     *
!*                       KIJ VALUES FOR ETHANE                         *
!*                                                                     *
!***********************************************************************      
!
        Kij_R(2,2) = 0.0d0
!
! ===>  From Walas (Table E.3) and Tsonopoulos (1974, 1980)
!
        Kij_R(2,3) = 0.0d0
        Kij_R(2,4) = 6.00d-2
        Kij_R(2,5) = 0.8000d-1
        Kij_R(2,6) = 5.0000d-2
        Kij_R(2,7) = 5.0000d-2
        Kij_R(2,8) = 3.7d-1
        Kij_R(2,9) = 1.2d-1
!
        Kij_R(3,2) = Kij_R(2,3)
        Kij_R(4,2) = Kij_R(2,4)
        Kij_R(5,2) = Kij_R(2,5)
        Kij_R(6,2) = Kij_R(2,6)
        Kij_R(7,2) = Kij_R(2,7)
        Kij_R(8,2) = Kij_R(2,8)
        Kij_R(9,2) = Kij_R(2,9)
! 
!***********************************************************************      
!*                                                                     *
!*                      KIJ VALUES FOR PROPANE                         *
!*                                                                     *
!***********************************************************************      
!
        Kij_R(3,3) = 0.0d0
!
! ===>  From Walas (Table E.3) and Tsonopoulos (1974, 1980)
!
        Kij_R(3,4) = 0.080d0
        Kij_R(3,5) = 0.1100d0
        Kij_R(3,6) = 0.0900d0
        Kij_R(3,7) = 0.0900d0
        Kij_R(3,8) = 4.0d-1
        Kij_R(3,9) = 1.2d-1
!
        Kij_R(4,3) = Kij_R(3,4)
        Kij_R(5,3) = Kij_R(3,5)
        Kij_R(6,3) = Kij_R(3,6)
        Kij_R(7,3) = Kij_R(3,7)
        Kij_R(8,3) = Kij_R(3,8)
        Kij_R(9,3) = Kij_R(3,9)
! 
!***********************************************************************      
!*                                                                     *
!*                 KIJ VALUES FOR HYDROGEN SULFIDE                     *
!*                                                                     *
!***********************************************************************      
!
        Kij_R(4,4) = 0.0d0
!
! ===>  From Walas (Table E.3) and Tsonopoulos (1974, 1980)
!
        Kij_R(4,5) = 8.0d-2
        Kij_R(5,4) = Kij_R(4,5)
!
! ===>  ??? From SRK - B
!
        Kij_R(4,6) = 0.140d0
        Kij_R(4,7) = 0.140d0

        Kij_R(6,4) = Kij_R(4,6)
        Kij_R(7,4) = Kij_R(4,7)   ! <=== ??? SRK - E
!
! ===>  ??? Guess - From Soreide & Wilson (1992), Eq. 17
!
        Kij_R(4,8) = 0.15d0
        Kij_R(8,4) = Kij_R(4,8)   ! <=== ??? Guess - SRK - E
!
! ===>  ??? Guess - B
!
        Kij_R(4,9) = 0.10d0
        Kij_R(9,4) = Kij_R(4,9)   ! <=== ??? Guess - E
! 
!***********************************************************************      
!*                                                                     *
!*                  KIJ VALUES FOR CARBON DIOXIDE                      *
!*                                                                     *
!***********************************************************************      
!
        Kij_R(5,5) = 0.0d0
!
! ===>  ??? Guess - B
!
        Kij_R(5,6) = 0.00d0
        Kij_R(5,7) = 0.00d0
!
        Kij_R(6,5) = Kij_R(5,6)
        Kij_R(7,5) = Kij_R(5,7)   ! <=== ??? Guess - E
!
! ===>  From Walas (Table E.3) and Tsonopoulos (1974, 1980)
!
        Kij_R(5,8) = 1.4d-1
        Kij_R(5,9) = 1.00d-2
!
        Kij_R(8,5) = Kij_R(5,8)
        Kij_R(9,5) = Kij_R(5,9)
! 
!***********************************************************************      
!*                                                                     *
!*                     KIJ VALUES FOR NITROGEN                         *
!*                                                                     *
!***********************************************************************      
!
        Kij_R(6,6) = 0.0d0
!
! ===>  ??? Guess - B
!
        Kij_R(6,7) = 0.0d0
        Kij_R(7,6) = Kij_R(6,7)   ! <=== ??? Guess - E
!
! ===>  From Tsonopoulos (1974)
!
        Kij_R(6,8) = 0.300d0
        Kij_R(6,9) = 0.050d0

        Kij_R(8,6) = Kij_R(6,8)
        Kij_R(9,6) = Kij_R(6,9)
! 
!***********************************************************************      
!*                                                                     *
!*                      KIJ VALUES FOR OXYGEN                          *
!*                                                                     *
!***********************************************************************      
!
        Kij_R(7,7) = 0.0d0
!
!-------------
! ..... Asssuming same as N2 
!-------------
!
        Kij_R(7,8) = 0.300d0
        Kij_R(7,9) = 0.050d0

        Kij_R(8,7) = Kij_R(7,8)
        Kij_R(9,7) = Kij_R(7,9)
! 
!***********************************************************************      
!*                                                                     *
!*                       KIJ VALUES FOR WATER                          *
!*                                                                     *
!***********************************************************************      
!
        Kij_R(8,8) = 0.0d0
!
! ===>  ??? Guess - B
!
        Kij_R(8,9) = 1.5d-1
        Kij_R(9,8) = Kij_R(8,9)   ! <=== ??? Guess - E
! 
!***********************************************************************      
!*                                                                     *
!*                      KIJ VALUES FOR ETHANOL                         *
!*                                                                     *
!***********************************************************************      
!
        Kij_R(9,9) = 0.0d0
 
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'K_SR              1.0    9 April     2004',6X,&
     &         'Setting the interaction coefficients in the ', &   
     &         'Redlich-Kwong EOS') 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of K_RK
! 
! 
      RETURN
!
      END SUBROUTINE K_RK
!
!
!
!*************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!*************************************************************************
!
!
      REAL(KIND = 8) FUNCTION PR_KW1(w_x,c_x,tr_x)
!
!*************************************************************************
!*************************************************************************
!*                                                                       *
!*         COMPUTE Kij FOR WATER + METHANE, ETHANE, PROPANE              *     
!*         From Soreide and Whitson (1992), Eq.12 + Table 2              *
!*                                                                       *
!*                   Version 1.00 - April 9, 2004                        *     
!*                                                                       *
!*************************************************************************
!*************************************************************************
!
      USE MPI_PARAM
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN) :: w_x,c_x,tr_x
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0
! 
      SAVE ICALL
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of PR_KW1
! 
! 
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)                                      
! 
! 
! 
      PR_KW1  = ( &
     &           1.1120d0-1.7360d0*(w_x**(-1.0d-1))&
     &          )       &
     &           *(&
     &             1.0d0+(4.7863d-13*w_x*w_x*w_x*w_x)*c_x&
     &            )&
     &         +tr_x*(1.1001d0+8.36d-1*w_x)&
     &              *(&
     &                1.0d0+1.438d-2*c_x&
     &               )&
     &         +tr_x*tr_x*((-1.5742d-1)-1.0988d0*w_x)&
     &              *(&
     &                1.0d0+2.1547d-3*c_x&
     &               )
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'PR_KW1            1.0    9 April     2004',6X,&
     &         'Adjusting Kij for Water + Methane, Ethane, ',  &  
     &         'Propane - Peng-Robinson EOS') 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of PR_KW1
! 
! 
      RETURN
!
      END FUNCTION PR_KW1
!
!
!
!*************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!*************************************************************************
!
!
      REAL(KIND = 8) FUNCTION PR_KW2(tr_x)
!
!*************************************************************************
!*************************************************************************
!*                                                                       *
!*                    COMPUTE Kij FOR WATER + H2S                        *     
!*         From Soreide and Whitson (1992), Eq.12 + Table 2              *
!*                                                                       *
!*                   Version 1.00 - April 9, 2004                        *     
!*                                                                       *
!*************************************************************************
!*************************************************************************
!
	  USE MPI_PARAM
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN) :: tr_x
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0
! 
      SAVE ICALL
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of PR_KW2
! 
! 
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)                                      
! 
! 
! 
      PR_KW2  = -2.0441d-1+2.3426d-1*tr_x
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'PR_KW2            1.0    9 April     2004',6X,&
     &         'Adjusting Kij for Water + H2S ',    &
     &         '- Peng-Robinson EOS') 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=> End of PR_KW2
! 
! 
      RETURN
!
      END FUNCTION PR_KW2
!
!
!
!*************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!*************************************************************************
!
!
      REAL(KIND = 8) FUNCTION PR_KW3(c_x,tr_x)
!
!*************************************************************************
!*************************************************************************
!*                                                                       *
!*                COMPUTE Kij FOR WATER + CARBON DIOXIDE                 *     
!*          From Soreide and Whitson (1992), Eq.12 + Table 2             *
!*                                                                       *
!*                   Version 1.00 - April 9, 2004                        *     
!*                                                                       *
!*************************************************************************
!*************************************************************************
!
	  USE MPI_PARAM
	  
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN) :: c_x,tr_x
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0
! 
      SAVE ICALL
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of PR_KW3
! 
! 
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)                                      
! 
! 
! 
      PR_KW3  = -3.1092d-1*(&
     &                      1.0d0+1.5587d-1*c_x**7.505d-1&
     &                     )&
     &          +2.3580d-1*(&
     &                      1.0d0+1.7837d-1*c_x**9.79d-1&
     &                     )*tr_x&
     &          -2.12566d+1*DEXP(-6.7222d0*tr_x-c_x)
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'PR_KW3            1.0    9 April     2004',6X,&
     &         'Adjusting Kij for Water + CO2 ',    &
     &         '- Peng-Robinson EOS') 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=> End of PR_KW3
! 
! 
      RETURN
!
      END FUNCTION PR_KW3
!
!
!
!*************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!*************************************************************************
!
!
      REAL(KIND = 8) FUNCTION PR_KW4(c_x,tr_x)
!
	  USE MPI_PARAM
!*************************************************************************
!*************************************************************************
!*                                                                       *
!*              COMPUTE Kij FOR WATER + OXYGEN, NITROGEN                 *     
!*          From Soreide and Whitson (1992), Eq.12 + Table 2             *
!*                                                                       *
!*                   Version 1.00 - April 9, 2004                        *     
!*                                                                       *
!*************************************************************************
!*************************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN) :: c_x,tr_x
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: ICALL = 0
! 
      SAVE ICALL
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of PR_KW4
! 
! 
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)                                      
! 
! 
! 
      PR_KW4  = -1.70235d0*(&
     &                      1.0d0+2.5587d-1*c_x**7.50d-1&
     &                     )&
     &          +4.4338d-1*(&
     &                      1.0d0+8.126d-2*c_x**7.50d-1&
     &                     )*tr_x
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'PR_KW4            1.0    9 April     2004',6X,&
     &         'Adjusting Kij for Water + Oxygen, Nitrogen ',  &  
     &         '- Peng-Robinson EOS') 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of PR_KW4
! 
! 
      RETURN
!
      END FUNCTION PR_KW4
!
!
!
!*************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!*************************************************************************
!
!
!
      REAL(KIND = 8) FUNCTION A_PR(TR_x,CS_x,WC_x,i)
! 
         USE RefRGas_Param, ONLY: RK,ID
		 USE MPI_PARAM
!
!*************************************************************************
!*************************************************************************
!*                                                                       *
!*              COMPUTE alfa OF THE PENG_ROBINSON EQUATION               *     
!*                                                                       *
!*                    Version 1.00 - April 4, 2004                       *     
!*                                                                       *
!*************************************************************************
!*************************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN) :: TR_x,CS_x,WC_x
! 
      REAL(KIND = 8) :: ax_0
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: i
! 
      INTEGER :: ICALL = 0
! 
      SAVE ICALL
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of A_PR
! 
! 
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)                                      
! 
! 
! 
!        IF (ID(i).EQ.8.AND.TR_x.LT.7.225d-1) THEN
        IF (ID(i).EQ.8) THEN
! 
! -------------
! ........ Aqueous systems           
! -------------
! 
           ax_0  = 1.0d0&
     &            +4.53d-1*(1.0d0-TR_x*(1.0d0-1.03d-2*(CS_x**1.1d0)))&
     &            +3.4d-3*((1.0d0/(TR_x*TR_x*TR_x))-1.0d0)
           RK(i) = ( 4.53d-1*(1.0d0-1.03d-2*(CS_x**1.1d0))*TR_x&
     &              +1.02d-2*(1.0d0/(TR_x*TR_x*TR_x)))*2.0d0
           A_PR  = ax_0*ax_0
! 
        ELSE
! 
! -------------
! ......... Non-aqueous systems            
! -------------
! 
            RK(i) = 3.7464d-1+1.54226d0*WC_x-2.6992d-1*WC_x*WC_x
            A_PR  = (1.0d0+RK(i)*(1.0d0-SQRT(TR_x)))&
     &             *(1.0d0+RK(i)*(1.0d0-SQRT(TR_x)))
! 
        END IF
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'A_PR              1.0    4 April     2004',6X,&
     &         'Compute "alfa" of the Peng-Robinson EOS of real gases') 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of A_PR
! 
! 
      RETURN
!
      END FUNCTION A_PR
!
!
!
!*************************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!*************************************************************************
!
!
      SUBROUTINE CROOT(A2,A1,A0,XMX,XMN,ixx)
!

	  USE MPI_PARAM
!*************************************************************************
!*************************************************************************
!*                                                                       *
!*                     ROOTS OF THE CUBIC EQUATION                       *     
!*                        X^3+A2*X^2+A1*X+A0=0                           *     
!*                                                                       *
!*                    Version 1.00 - March 24, 2004                      *     
!*                                                                       *
!*************************************************************************
!*************************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision parameters
! -------
! 
      REAL(KIND = 8), PARAMETER :: PI=3.14159265358979324D0
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN)  :: A2,A1,A0
      REAL(KIND = 8), INTENT(OUT) :: XMX,XMN
! 
      REAL(KIND = 8) :: G,H,GH,S,TT,XA,X1,X2,X3,THETA
      REAL(KIND = 8) :: X1mn,X2mn,X3mn
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(OUT) :: ixx
! 
      INTEGER :: ICALL = 0
! 
      SAVE ICALL
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of CROOT
! 
! 
      ICALL = ICALL+1                                                     
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)                                      
! 
! -------
! ... Intermediate variables
! -------
! 
      G  =  A1-(A2*A2/3.0d0)
      H  = (2.0d0*A2*A2*A2-9.0d0*A2*A1+2.7d1*A0)/2.7d1
      GH = (G*G*G)/2.7d1+(H*H)/4.0d0
!
!
!
      IF_NoRoots: IF (GH > 0.0d0) THEN
! 
! -------------
! ...... Single Real Root           
! -------------
! 
         S  = -5.0d-1*H+SQRT(GH)
         TT = -5.0d-1*H-SQRT(GH)
! 
         IF (S >= 0.0d0) THEN
            S = S**(1.0d0/3.0d0)
         ELSE
            S = -((-S)**(1.0d0/3.0d0))
         END IF
! 
         IF (TT >= 0.0d0) THEN
            TT = TT**(1.0d0/3.0d0)
         ELSE
            TT = -((-TT)**(1.0d0/3.0d0))
         END IF
! 
         XMX = S+TT-A2/3.0d0
         XMN = XMX
         ixx = 1
! 
      ELSE
! 
! -------------
! ...... Three Real Roots           
!c -------------
!c 
         XA    = -5.0d-1*H/SQRT(-(G*G*G/2.7d1))
         THETA = (PI/2.0d0-ATAN(XA/(SQRT(1.0d0-XA*XA))))/3.0d0
! 
         X1 = 2.0d0*SQRT(-G/3.0d0)*COS(THETA)
         X2 = 2.0d0*SQRT(-G/3.0d0)*COS(THETA+2.0d0*PI/3.0d0)
         X3 = 2.0d0*SQRT(-G/3.0d0)*COS(THETA+4.0d0*PI/3.0d0)
! 
         XMX  = MAX(X1,X2,X3)-A2/3.0d0
! 
         X1mn = X1-A2/3.0d0
         X2mn = X2-A2/3.0d0
         X3mn = X3-A2/3.0d0
! 
         IF(X1mn < 0.0d0) X1mn = 1.0d10
         IF(X2mn < 0.0d0) X2mn = 1.0d10
         IF(X3mn < 0.0d0) X3mn = 1.0d10
! 
         XMN  = MIN(X1mn,X2mn,X3mn)
         ixx  = 3
! 
      END IF IF_NoRoots
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'CROOT             1.0   24 March     2004',6X,&
     &         'Determining the real roots of a cubic equation') 
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of CROOT
! 
! 
      RETURN
!
      END SUBROUTINE CROOT
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      REAL(KIND = 8) FUNCTION HenryS_K(Gas_id,TX,MoleF_salt,&
     &                                 F_SaltEffect)
! 
! ...... Modules to be used 
! 
         USE RefRGas_Param
		 USE MPI_PARAM
! 
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*        Henry's law to calculate a gas bubbling pressure or          *     
!*  the gas solubility (dissolved mass fraction in the aqueous phase)  *     
!*                                                                     *
!*                    Version 1.0 - March 24, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision parameters
! -------
! 
      REAL(KIND = 8), PARAMETER :: MW_H2O  = 1.8016d1
      REAL(KIND = 8), PARAMETER :: MW_salt = 5.8448d1
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: TX          ! Temperature in C
      REAL(KIND = 8) :: HK_GinH2O   ! Henry's "constant" (Pa)
      REAL(KIND = 8) :: MoleF_salt  ! Salt mole fraction in the aqueous phase
!                              
      REAL(KIND = 8) :: XG,T2,T3,T4,MW_Gas
      REAL(KIND = 8) :: RKHW,RKHO2,SOGAS,SOO2
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: Gas_id
! 
      INTEGER :: ICALL = 0
! 
! -------
! ... Character variables
! -------
! 
      CHARACTER(LEN = 6) :: GasX       ! Gas chemical type
! 
! -------
! ... LOGICAL variables
! -------
! 
      LOGICAL :: F_SaltEffect   ! Flag indicating if salt effects ... 
!                               ! ... are to be accounted for
! 
      SAVE ICALL
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of HenryS_Law
! 
! 
      ICALL=ICALL+1
      IF(ICALL == 1 .and. MPI_RANK==0) WRITE(11,6000)
! 
! -------
! ... Initial computations
! -------
! 
      IF(Gas_id == 0) THEN
         GasX   = 'AIR'
         MW_Gas = 2.896d1
      ELSE
         GasX   = GasR(Gas_id)%name
         MW_Gas = 1.0d3*GasR(Gas_id)%MolWt
      END IF
! 
      XG = 0.0d0   ! A simplification - Ignore the dissolved gas mass fraction
!                  ! Otherwise, there may be a need to iterate
!
      IF(F_SaltEffect .EQV. .FALSE.) MoleF_salt = 0.0d0 ! The effect of salt is ignored
! 
! -------
! ... Temperature parameters
! -------
! 
      T2 = TX*TX
      T3 = T2*TX
      T4 = T2*T2
! 
! **********************************************************************
! *                                                                    *
! *          Evaluate Preliminary Parameters in Henry's Law            *
! *                                                                    *
! **********************************************************************
!
      SELECT_Gas: SELECT CASE(TRIM(ADJUSTL(GasX)))
!
! >>>>>>>
! >>>>>>>
! ... AIR: D'AMORE And Truesdell [1988], Cramer [1982], Cygan [1991]
! >>>>>>>
! >>>>>>>
!
      CASE('AIR')
!
         RKHW = 1.01325D5*( 51372.6D0&
     &                     +1586.03D+0*TX&
     &                     -5.93780D+0*T2&
     &                     -6.98282D-2*T3&
     &                     +5.10330D-4*T4&
     &                     -1.21388D-6*T2*T3&
     &                     +1.00041D-9*T3*T3&
     &                    )
!
         RKHO2 = 1.0D5*( 26234.0D0&
     &                  +610.628D+0*TX&
     &                  +7.00732D+0*T2&
     &                  -1.39299D-1*T3&
     &                  +7.13850D-4*T4&
     &                  -1.54216D-6*T2*T3&
     &                  +1.23190D-9*T3*T3&
     &                 )
! 
! ----------
! ...... Determine salt effects
! ----------
! 
         IF_Salt1: IF(F_SaltEffect .EQV. .TRUE.) THEN
!
            SOGAS =  0.183369D0&
     &              -2.36905D-03*TX&
     &              +2.42438D-05*T2&
     &              -7.30134D-08*T3&
     &              +8.58723D-11*T4
!
            SOO2  =  0.16218D0&
     &              -1.16909D-03*TX&
     &              +5.55185D-06*T2&
     &              -8.75443D-09*T3&
     &              +9.91567D-12*T4
!
         ELSE
!
            SOGAS = 0.0D0
            SOO2  = 0.0D0
!
         END IF IF_Salt1
!
!
         HK_GinH2O =  0.79D0*RKHW*10.0D0**(MoleF_salt*SOGAS)&
     &                +0.21D0*RKHO2*10.0D0**(MoleF_salt*SOO2)
!
! >>>>>>>
! >>>>>>>
! ... CO2: Cramer [1982]
! >>>>>>>
! >>>>>>>
!
      CASE('CO2')
!
         RKHW = 1.0d5*( 783.666D0&
     &                 +1.96025D+01*TX&
     &                 +8.20574D-01*T2&
     &                 -7.40674D-03*T3&
     &                 +2.18380D-05*T4&
     &                 -2.20999D-08*T2*T3&
     &                )
! 
! ----------
! ...... Determine salt effects
! ----------
! 
         IF_Salt2: IF(F_SaltEffect .EQV. .TRUE.) THEN
            SOGAS =  1.19784D-01&
     &              -7.17823D-04*TX&
     &              +4.93854D-06*T2&
     &              -1.03826D-08*T3&
     &              +1.08233D-11*T4
         ELSE
            SOGAS = 0.0D0
         END IF IF_Salt2
! 
         HK_GinH2O = RKHW*10.0D0**(MoleF_salt*SOGAS)
!
! >>>>>>>
! >>>>>>>
! ... CH4: Cramer [1982]
! >>>>>>>
! >>>>>>>
!
      CASE('CH4')
!
         RKHW = 1.0D5*( 24582.4D0 &
     &                 +6.71091D+02*TX&
     &                 +6.87067D+00*T2&
     &                 -1.773079D-1*T3&
     &                 +1.09652D-03*T4&
     &                 -3.19599D-06*T2*T3&
     &                 +4.46172D-09*T3*T3&
     &                 -2.40294D-12*T4*T3&
     &                )
! 
! ----------
! ...... Determine salt effects
! ----------
! 
         IF_Salt3: IF(F_SaltEffect .EQV. .TRUE.) THEN
            SOGAS =  1.64818D-01&
     &              -1.40166D-03*TX&
     &              +1.32360D-05*T2&
     &              -4.85733D-08*T3&
     &              +7.87967D-11*T4&
     &              -5.52586D-14*TX*T4
         ELSE
            SOGAS = 0.0D0
         END IF IF_Salt3
! 
         HK_GinH2O = RKHW*10.0D0**(MoleF_salt*SOGAS)
!
! >>>>>>>
! >>>>>>>
! ... H2: D'Amore and Truesdell [1988]
! >>>>>>>
! >>>>>>>
!
      CASE('H2')
!
         RKHW = 1.01325D5*( 57106.3D0&
     &                     +7.61981D+02*TX&
     &                     -8.55167D+00*T2&
     &                     +1.20762D-03*T3&
     &                     +2.85520D-04*T4&
     &                     -1.42066D-06*T2*T3&
     &                     +2.81250D-09*T3*T3&
     &                     -2.04787D-12*T4*T3&
     &                    ) 
!
         SOGAS = 0.0D0
!
         HK_GinH2O = RKHW*10.0D0**(MoleF_salt*SOGAS)
!
! >>>>>>>
! >>>>>>>
! ... N2: D'Amore and Truesdell [1988], Cygan [1991]
! >>>>>>>
! >>>>>>>
!
      CASE('N2')
!
         RKHW = 1.01325D5*( 51372.6D0&
     &                     +1.58603D+3*TX&
     &                     -5.93780D+0*T2&
     &                     -6.98282D-2*T3&
     &                     +5.10330D-4*T4&
     &                     -1.21388D-6*T2*T3&
     &                     +1.00041D-9*T3*T3&
     &                    ) 
! 
! ----------
! ...... Determine salt effects
! ----------
! 
         IF_Salt4: IF(F_SaltEffect .EQV. .TRUE.) THEN
            SOGAS =  1.83369D-01&
     &              -2.36905D-03*TX&
     &              +2.42438D-05*T2&
     &              -7.30134D-08*T3&
     &              +8.58723D-11*T4
         ELSE
            SOGAS = 0.0D0
         END IF IF_Salt4
! 
         HK_GinH2O = RKHW*10.0D0**(MoleF_salt*SOGAS)
!
! >>>>>>>
! >>>>>>>
! ... End of the SELECT_Gas construct
! >>>>>>>
! >>>>>>>
!
      END SELECT SELECT_Gas
! 
! **********************************************************************
! *                                                                    *
! *                 Determine Desired Parameters                       *
! *                                                                    *
! **********************************************************************
!
      HenryS_K = HK_GinH2O
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'HenryS_K          1.0   24 March     2004',6X,&
     &         "Calculate the temperature-dependent ",&
     &         "Henry\D5s constant for the dissolution of a ",/,&
     &       T48,"non-condensible gas in water")
!
 6010 FORMAT(//,20('ERROR-'),//,&
     &       T2,' The input and output options',&
     &          ' in subroutine "HenryS_Law" are inconsistent',//,&
     &       T2,'!!!!!!!    THE EXECUTION WAS STOPPED    !!!!!!!',&
     &       //,20('ERROR-'))
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of HenryS_K 
! 
! 
      RETURN
!
      END FUNCTION HenryS_K
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      REAL(KIND = 8) FUNCTION HEAT_GasSolution(Gas_id,TX,&
     &                                         MoleF_salt,&
     &                                         HK_GinH2O,&
     &                                         HK_O2inH2O)
! 
! ...... Modules to be used 
! 
         USE RefRGas_Param
		 USE MPI_PARAM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*        Heat of solution of non-condensible gases into water         *     
!*         as a function of temperature and salt concentration         *     
!*                                                                     *
!*                    Version 1.0 - March 24, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN)  :: TX          ! Temperature in C
      REAL(KIND = 8), INTENT(IN)  :: MoleF_salt  ! Salt mole fraction ...
!                                                ! ... in the aqueous phase 
      REAL(KIND = 8), INTENT(IN)  :: HK_GinH2O   ! Henry's constant in H2O
      REAL(KIND = 8), INTENT(IN)  :: HK_O2inH2O  ! O2 Henry's constant in H2O ...
!                                                ! ... (For air only)                              
!                              
      REAL(KIND = 8) :: T2,T3,T4,MW_Gas
      REAL(KIND = 8) :: DKHWDT,DSOGAS,DKHO2,DSOO2,DKHDT
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: Gas_id
! 
      INTEGER :: ICALL = 0
! 
! -------
! ... Character variables
! -------
! 
      CHARACTER(LEN = 6) :: GasX  ! Gas chemical type
! 
! 
      SAVE ICALL
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of HEAT_GasSolution
! 
! 
      ICALL = ICALL+1
      IF(ICALL == 1 .and. mpi_rank==0) WRITE(11,6000)
! 
! -------
! ... Initial computations
! -------
! 
      IF(Gas_id == 0) THEN
         GasX   = 'AIR'
         MW_Gas = 2.896d1
      ELSE
         GasX   = GasR(Gas_id)%name
         MW_Gas = 1.0d3*GasR(Gas_id)%MolWt
      END IF
! 
      T2 = TX*TX
      T3 = T2*TX
      T4 = T2*T2
! 
! **********************************************************************
! *                                                                    *
! *            Evaluate Solution Heat for Various Gases                *
! *                                                                    *
! **********************************************************************
!
      SELECT_Gas: SELECT CASE(TRIM(ADJUSTL(GasX)))
!
! >>>>>>>
! >>>>>>>
! ... AIR: D'Amore and Truesdell [1988], Cramer [1982], Cygan [1991]
! >>>>>>>
! >>>>>>>
!
      CASE('AIR')
!
         DKHWDT = 1.01325d5*( 1586.03d0 &
     &                       -2.0d0*5.93780d00*TX&
     &                       -3.0d0*6.98282d-2*T2&
     &                       +4.0d0*5.10330d-4*T3&
     &                       -5.0d0*1.21388d-6*T4&
     &                       +6.0d0*1.00041d-9*T2*T3&
     &                      )
!
         DSOGAS = -2.36905d-3&
     &            +2.d0*2.42438d-05*TX&
     &            -3.d0*7.30134d-08*T2&
     &            +4.d0*8.58723d-11*T3
!
         DKHO2  = 1.0d5*( 6.10628d2&
     &                   +2.0d0*7.00732d00*TX&
     &                   -3.0d0*1.39299d-1*T2&
     &                   +4.0d0*7.13850d-4*T3&
     &                   -5.0d0*1.54216d-6*T4&
     &                   +6.0d0*1.23190d-9*T2*T3&
     &                  )
!
         DSOO2  = -1.16909d-3&
     &            +2.0d0*5.55185d-06*TX&
     &            -3.0d0*8.75443d-09*T2&
     &            +4.0d0*9.91567d-12*T3
!
         DKHWDT = DKHWDT/HK_GinH2O&
     &           +LOG(10.d0)*MoleF_salt*DSOGAS
!
         DKHO2  = DKHO2/HK_O2inH2O&
     &           +LOG(10.d0)*MoleF_salt*DSOO2
!
         DKHDT  = 0.79d0*DKHWDT&
     &           +0.21d0*DKHO2
!
! >>>>>>>
! >>>>>>>
! ... CO2: Cramer [1982]
! >>>>>>>
! >>>>>>>
!
      CASE('CO2')
!
         DKHWDT = 1.0d5*( 19.6025d0 &
     &                   +2.0d0*0.8205740d0*TX&
     &                   -3.0d0*7.40674d-03*T2&
     &                   +4.0d0*2.18380d-05*TX*T2&
     &                   -5.0d0*2.20999d-08*T4&
     &                  )
!
         DSOGAS = -7.17823d-4&
     &            +2.0d0*4.93854d-06*TX&
     &            -3.0d0*1.03826d-08*T2&
     &            +4.0d0*1.08233d-11*TX*T2
!
         DKHDT  = DKHWDT/HK_GinH2O&
     &           +LOG(10.0d0)*MoleF_salt*DSOGAS
!
! >>>>>>>
! >>>>>>>
! ... CH4: Cramer [1982]
! >>>>>>>
! >>>>>>>
!
      CASE('CH4')
!
         DKHWDT = 1.0d5*( 671.091D0&
     &                   +2.0D0*6.87067D+00*TX&
     &                   -3.0D0*1.73079D-01*T2&
     &                   +4.0D0*1.09652D-03*T3&
     &                   -5.0D0*3.19599D-06*T4&
     &                   +6.0D0*4.46172D-09*T2*T3&
     &                   -7.0D0*2.40294D-12*T4*T2&
     &                  ) 
!
         DSOGAS = -1.40166D-3&
     &            +2.0D0*1.32360D-05*TX&
     &            -3.0D0*4.85733D-08*T2&
     &            +4.0D0*7.87967D-11*T3&
     &            -5.0D0*5.52586D-14*T4
!
         DKHDT  = DKHWDT/HK_GinH2O&
     &           +LOG(10.0D0)*MoleF_salt*DSOGAS
!
! >>>>>>>
! >>>>>>>
! ... H2: D'Amore and Truesdell [1988]
! >>>>>>>
! >>>>>>>
!
      CASE('H2')
!
         DKHWDT = 1.01325D5*( 761.981D0&
     &                       -2.0D0*8.55167D+00*TX&
     &                       +3.0D0*1.20762D-03*T2&
     &                       +4.0D0*2.85520D-04*T3&
     &                       -5.0D0*1.42066D-06*T4&
     &                       +6.0D0*2.81250D-09*T2*T3&
     &                       -7.0D0*2.04787D-12*T4*T2&
     &                      )
!
         DSOGAS = 0.0D0
!
         DKHDT  = DKHWDT/HK_GinH2O&
     &           +LOG(10.0D0)*MoleF_salt*DSOGAS
!
! >>>>>>>
! >>>>>>>
! ... N2: D'Amore and Truesdell [1988], Cygan [1991]
! >>>>>>>
! >>>>>>>
!
      CASE('N2')
!
         DKHWDT =  1586.03D0&
     &            -2.0D0*5.93780D+0*TX&
     &            -3.0D0*6.98282D-2*T2&
     &            +4.0D0*5.10330D-4*T3&
     &            -5.0D0*1.21388D-6*T4&
     &            +6.0D0*1.00041D-9*T2*T3
!
         DKHWDT = DKHWDT*1.01325D5
!
         DSOGAS = -2.36905D-3&
     &            +2.0D0*2.42438D-05*TX&
     &            -3.0D0*7.30134D-08*T2&
     &            +4.0D0*8.58723D-11*T3
!
         DKHDT  = DKHWDT/HK_GinH2O&
     &           +LOG(10.0D0)*MoleF_salt*DSOGAS
!
! >>>>>>>
! >>>>>>>
! ... End of the SELECT_Gas construct
! >>>>>>>
! >>>>>>>
!
      END SELECT SELECT_Gas
! 
! **********************************************************************
! *                                                                    *
! *             Finalize computation (Himmelblau [1959])               *
! *                                                                    *
! **********************************************************************
!
      Heat_GasSolution =   &                    ! Heat of solution (J/kg)
     &                   -RGas*(TX+273.15D0)&
     &                        *(TX+273.15D0)&
     &                        *DKHDT/MW_Gas
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'HEAT_GasSolution  1.0   24 March     2004',6X,&
     &         'Heat of solution of non-condensible gas into water as ',&
     &         'a function of ',&
     &       /,T48,'temperature and salt mole fraction')
! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of HEAT_GasSolution 
! 
! 
      RETURN
!
      END FUNCTION Heat_GasSolution
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
      END MODULE RealGas_Properties
!
!
!
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
