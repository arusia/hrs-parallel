!This contains option to have domain decomposition within a single process through call to METIS
     MODULE MPI_VAR
		IMPLICIT NONE
		save
		integer :: rank,w_size	
	END MODULE MPI_VAR
    MODULE PART_VAR
	
	
		IMPLICIT NONE
		SAVE
		
		INTEGER,POINTER:: ERRAY(:,:),part_list(:)
	
	END MODULE PART_VAR
	
	MODULE COARSE_M
	REAL(kind=8),allocatable,dimension(:,:) :: CO
	END MODULE COARSE_M
	
	MODULE Matrix_Solvers_par
! 
	
         PUBLIC

	 TYPE :: BUFFEL
		real(kind=8),dimension(:), pointer :: A
		integer, dimension(:), pointer :: IA
		integer, dimension(:), pointer :: JA
		integer, dimension(:), pointer :: update_p
		integer, dimension(:), pointer :: update_g
	END TYPE BUFFEL

	  
	TYPE :: MAPEL
		integer :: update_l !Local index of the parent process
		integer :: IA  !Local index of current process
		type(MAPEL),pointer :: next
	END TYPE MAPEL
	
	TYPE :: HeadMAPEL
		TYPE(MAPEL),pointer :: Ende
		TYPE(MAPEL),pointer :: next
	END TYPE HeadMAPEL
	
	TYPE :: EXEL
		integer :: loc
		integer :: sloc
		type(Exel),pointer :: next
	END TYPE Exel
	
	TYPE :: EXT
		integer :: loc
		integer :: update_p
		type(EXT),pointer :: next
	END TYPE EXT
	
	TYPE :: HEADEX
		TYPE(EXEL),pointer:: endp
		TYPE(EXEL),pointer:: ende
		TYPE(EXEL),pointer :: curr
		TYPE(EXEL),pointer :: next
	END TYPE HEADEX
	
	TYPE :: HDEXT
		TYPE(EXT),pointer:: ende
		TYPE(EXT),pointer :: next
	END TYPE HDEXT
	
	INTEGER :: CALLE=0
! 
! ----------
! ...... Iterative Solvers (Preconditioned Conjugate Gradient)
! ----------
! 
         PUBLIC :: DSLUCS_WP_PAR,DSLUCS_SW_PAR,DSLUCS_TEMP,DSLUCS_SW_PAR_HYB
! 
! ----------
! ...... Direct Solvers (LU Decomposition)
! ----------
! 
         !PUBLIC :: DGBTRF,DGBTRS
!
!
! 
	SAVE CALLE
	
         CONTAINS
		 
	  SUBROUTINE DSLUCS_WP_PAR(NI,NT,B,X,NELT,IA,JA,A,TOL,ITMAX,ITER,ERR,&
     &                  IERR,IUNIT,RWORK,LENW,IWORK,LENIW,UPDATE_P,update_l )
!---------------------------------------------------------
!This the parallel version of DSLUCS. Changed on 102616. Author - Aman Rusia
!--------------------------------------------------------
!NL - Number of elements in internal set
!NT - Number of elements in total = internal + update set
!B - Right hand side of the system - processor dependent size (NT)
!X - It is the final solution - processor dependent size (NT). X should be guess solution 
!NELT - size of A, IA and JA. Assign NELT_MAX+1 to them
!IA,JA,A - irn ,icn,co distributed
!    &                  CLOSUR,NMAXIT,ITERU,ERR,IERR,&
!   &                  IUNIT
!RWORK - size LENW.  Work array. Originally LENW = max(1000+mnz+38*MAXNUM_ELEM * NEQ, 9*MaxNum_Conx+1000) where mnz = max size of CO = NEQ*NEQ*(MaxNum_elements+2*MaxNum_conx). New LENW = 1000+mnz+38*MAXNUM_ELEM * NEQ, 9*MaxNum_Conx+1000
!IWORK - size LENIW. jvect. Work array. Originally LENIW = 100+mnz+5*MAXNUM_ELEM*NEQ. IWORK(1:10) stores Upper,Lower triangular matrices and Diagonal inverse of preconditioner, and their column and row information
!new the above sizes are changed according to new MAXNUM_ELEM = MAXNUM_ELEM/size. Note that MaxNum_Conx should remain intact since it could be fully inside a processor.
!RWORK also stores values of elements in L, U and Dinv after preconditioning. IWORK stores their respective row and column indices
!IWORK not needed in non preconditioned dslucs
	 use MPI_VAR
	 IMPLICIT REAL(KIND = 8)(A-H,O-Z)
	 include 'mpif.h'
!
      INTEGER :: NI, NT, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITMAX, ITER
      INTEGER :: IERR, IUNIT, LENW, IWORK(LENIW), LENIW
      REAL(KIND = 8) :: B(NT), X(NT), A(NELT), TOL, ERR, RWORK(LENW)
      EXTERNAL ::  DSLUI
	 ! EXTERNAL :: DSMV_PAR
	  INTEGER :: UPDATE_P(*),update_l(*)
!
      INTEGER, PARAMETER :: LOCRB=1, LOCIB=11
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSLUCS
!
!
	 call MPI_COMM_RANK ( MPI_COMM_WORLD,rank,IERR)
	 call MPI_COMM_SIZE ( MPI_COMM_WORLD, w_size, IERR)
      ISYM = 0
      ITOL = 2
!
      IERR = 0
      IF( NI.LT.1 .OR. NELT.LT.1 ) THEN
         IERR = 3
         RETURN
      ENDIF
      CALL DS2Y( NI, NELT, JA, IA, A, ISYM, IUNIT ) !In parallel version, IA and JA have been swapped. No other change inside
    
!--------212         
 
      LOCL   = LOCRB      !Starting location in RWORK
      LOCDIN = LOCL + 0
      LOCUU  = LOCDIN + 0
      LOCR   = LOCUU + 0
      LOCR0  = LOCR + NT
      LOCP   = LOCR0 + NT
      LOCQ   = LOCP + NT
      LOCU   = LOCQ + NT
      LOCV1  = LOCU + NT
      LOCV2  = LOCV1 + NT
      LOCW   = LOCV2 + NT

     ! CALL DCHKW( 'DSLUCS', LOCIW, LENIW, 0, LENW, IERR, ITER, ERR ) !Check if LOCW and LOCIW exceed the limit or not. LOCW has been set as zero in non preconditioned version
      IF( IERR.NE.0 ) RETURN
!
!

      CALL DCGS_WP(NI, NT, B, X, NELT, IA, JA, A, ISYM, DSMV_PAR,&
          DSLUI_PAR, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT,&
          RWORK(LOCR), RWORK(LOCR0), RWORK(LOCP),&
          RWORK(LOCQ), RWORK(LOCU), RWORK(LOCV1),&
          RWORK(LOCV2), RWORK, IWORK, update_l,UPDATE_P )
		  
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DSLUCS
!
!
      RETURN
      END SUBROUTINE DSLUCS_WP_PAR
!
    SUBROUTINE DCGS_WP(NI, NT, B, X, NELT, IA, JA, A, ISYM, MATVEC,&
     &     MSOLVE, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, &
     &     R, R0, P, Q, U, V1, V2, RWORK, IWORK,update_l,UPDATE_P)
!
!!N, B, X, NELT, IA, JA, A, ISYM, DSMV,&           !!!ITERATION BASICALLY STARTS AFTER THIS. Algorithm after here.
 !         DSLUI, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT,&
 !         RWORK(LOCR), RWORK(LOCR0), RWORK(LOCP),&
 !         RWORK(LOCQ), RWORK(LOCU), RWORK(LOCV1),&
  !        RWORK(LOCV2), RWORK, IWORK
!
	use MPI_VAR
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
	  
      INTEGER :: NI, NT, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITMAX
      INTEGER :: ITER, IERR, IUNIT, IWORK(*)
      REAL(KIND = 8) :: B(NT), X(NT), A(NELT), TOL, ERR, R(NT), R0(NT), P(NT)
      REAL(KIND = 8) :: Q(NT), U(NT), V1(NT), V2(NT), RWORK(*)
!
      REAL(KIND = 8) :: DMACH(5)
	  INTEGER ::update_l(*),UPDATE_P(*)
	  
      DATA DMACH(3) / 1.1101827117665D-16 /

      EXTERNAL MATVEC, MSOLVE
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DCGS
!
!
	
      ITER = 0
      IERR = 0
!
      IF( NI < 1 ) THEN
         IERR = 3
         RETURN
      ENDIF
!
      TOLMIN = 5.0d2*DMACH(3)
      IF( TOL < TOLMIN ) THEN
         TOL  = TOLMIN
         IERR = 4
      ENDIF
!  

      
      CALL MATVEC(NI, NT, X, R, NELT, IA, JA, A, ISYM,UPDATE_P,update_l) !DSMV Matvec computes AX -> R. MSOLVE solves the system MR = V1, where V1 is residual AX - R from previous step, M = LDU - the ldu decomposition from preconditioning.
!  
     
 !if(rank==0) print *, R(1:NI), rank
      V1(1:NI)  = R(1:NI) - B(1:NI)
!         
     ! CALL MSOLVE(N, V1, R, NELT, IA, JA, A, ISYM, RWORK, IWORK)!DSLUI - Solves for R in LDU.R = B1 Directly parallelizable
!        

	  R(1:NI) = V1(1:NI)
      ISDCGS = 0
      ITOL   = 2
      IF(ITER == 0) THEN
         !CALL MSOLVE(N, B, V2, NELT, IA, JA, A, ISYM, RWORK, IWORK)
		 V2(1:NI) = B(1:NI)
         BNRM = SQRT(DOT_PROD_PAR(V2,V2,NI))
      ENDIF
!         
      ERR = SQRT(DOT_PROD_PAR(R,R,NI))/BNRM
!     


	if(rank==0) then
      IF(IUNIT /= 0) THEN
         IF( ITER == 0 ) THEN
            WRITE(IUNIT,1000) NT, ITOL
         ENDIF
         WRITE(IUNIT,1010) ITER, ERR, AK, BK
      ENDIF
	end if
	
      IF(ERR <= TOL) ISDCGS = 1
!         
      IF(ISDCGS /= 0) GO TO 200
!
!
      FUZZ = DMACH(3)**2
!
      R0(1:NI) = R(1:NI)
!
      RHONM1 = 1.0D0
!         
!         
      DO K=1,ITMAX
         ITER = K
!
         RHON = DOT_PROD_PAR(R0,R,NI)
!
         IF( ABS(RHONM1) < FUZZ ) GO TO 998
!
         BK = RHON/RHONM1
!

         IF( ITER.EQ.1 ) THEN
            U(1:NI) = R(1:NI)
            P(1:NI) = R(1:NI)
         ELSE
            U(1:NI)  = R(1:NI) + BK*Q(1:NI)
            V1(1:NI) = Q(1:NI) + BK*P(1:NI)
            P(1:NI)  = U(1:NI) + BK*V1(1:NI)
         ENDIF
!         
         CALL MATVEC(NI, NT, P, V2, NELT, IA, JA, A, ISYM,UPDATE_P,update_l)
         !CALL MSOLVE(N, V2, V1, NELT, IA, JA, A, ISYM, RWORK, IWORK)
!         
 
		 V1(1:NI) = V2(1:NI)
         SIGMA = DOT_PROD_PAR(R0,V1,NI)

         IF( ABS(SIGMA) < FUZZ ) GO TO 999
!         
         AK = RHON/SIGMA
         AKM = -AK
!         
         Q(1:NI)  = U(1:NI) + AKM*V1(1:NI)
!         
         V1(1:NI) = U(1:NI) + Q(1:NI)
!         
         X(1:NI)  = X(1:NI) + AKM*V1(1:NI)
!                      
         CALL MATVEC(NI, NT, V1, V2, NELT, IA, JA, A, ISYM,UPDATE_P,update_l)
        ! CALL MSOLVE(N, V2, V1, NELT, IA, JA, A, ISYM, RWORK, IWORK)
		
		 V1 = V2
         R(1:NI) = R(1:NI) + AKM*V1(1:NI)
!         
         ISDCGS = 0
         ITOL   = 2
!         
         ERR = SQRT(DOT_PROD_PAR(R,R,NI))/BNRM
!        
		 if(rank==0) then
			 IF(IUNIT /= 0) THEN
				WRITE(IUNIT,1010) ITER, ERR, AK, BK
			 ENDIF
		 end if
		 
         IF(ERR <= TOL) ISDCGS = 1
!         
         IF(ISDCGS /= 0) GO TO 200
!
         RHONM1 = RHON
	  END DO
 1000 FORMAT('BiConjugate Gradient Squared without Preconditioning for ',&
          'N, ITOL = ',I5, I5,&
          /' ITER','   Error Estimate','            Alpha',&
          '             Beta')
 1010 FORMAT(1X,I4,1X,E16.7,1X,E16.7,1X,E16.7)
!         
      ITER = ITMAX + 1
      IERR = 2
 200  RETURN
!
 998  IERR = 5
      RETURN
!
 999  IERR = 6
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DCGS
!
!
      RETURN
      END SUBROUTINE DCGS_WP
!

   SUBROUTINE DS2Y(N, NELT, IA, JA, A, ISYM,IUNIT )
!Parallel Version written by Aman Rusia Date 102616
!
!
use MPI_VAR
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      INTEGER N, NELT, IA(NELT), JA(NELT), ISYM
      REAL(KIND = 8) A(NELT)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DS2Y
!
!	
	
      IF( JA(N+1).EQ.NELT+1 ) RETURN
      CALL QS2I1D( JA, IA, A, NELT, 1,IUNIT )
      JA(1) = 1
      DO 20 ICOL = 1, N-1
         DO 10 J = JA(ICOL)+1, NELT
            IF( JA(J).NE.ICOL ) THEN
               JA(ICOL+1) = J
               GOTO 20
            ENDIF
 10      CONTINUE
 20   CONTINUE
      JA(N+1) = NELT+1
!         
      JA(N+2) = 0
!
      DO 70 ICOL = 1, N
         IBGN = JA(ICOL)
         IEND = JA(ICOL+1)-1
         DO 30 I = IBGN, IEND
            IF( IA(I).EQ.ICOL ) THEN
               ITEMP = IA(I)
               IA(I) = IA(IBGN)
               IA(IBGN) = ITEMP
               TEMP = A(I)
               A(I) = A(IBGN)
               A(IBGN) = TEMP
               GOTO 40
            ENDIF
 30      CONTINUE
 40      IBGN = IBGN + 1
         IF( IBGN.LT.IEND ) THEN
            DO 60 I = IBGN, IEND
               DO 50 J = I+1, IEND
                  IF( IA(I).GT.IA(J) ) THEN
                     ITEMP = IA(I)
                     IA(I) = IA(J)
                     IA(J) = ITEMP
                     TEMP = A(I)
                     A(I) = A(J)
                     A(J) = TEMP
                  ENDIF
 50            CONTINUE
 60         CONTINUE
         ENDIF
 70   CONTINUE
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DS2Y
!
!
      RETURN
      END SUBROUTINE DS2Y
!
!
!
   SUBROUTINE QS2I1D( IA, JA, A, N, KFLAG, IUNIT )
!
!
!
	use MPI_VAR
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
	
	  
      DIMENSION IL(21),IU(21)
      INTEGER  :: N,NN,IA(N),JA(N),IT,IIT,JT,JJT
      REAL(KIND = 8) A(N), TA, TTA
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of QS2I1D
!
!
	
      NN = N
      IF (NN.LT.1) THEN
	 
	  if(rank==0) WRITE(IUNIT,6100) 
 6100 FORMAT(/,'QS2I1D- the number of values to', &
     &         ' be sorted was NOT POSITIVE.')
         RETURN
      ENDIF
      IF( N.EQ.1 ) RETURN
      KK = IABS(KFLAG)
      IF ( KK.NE.1 ) THEN
         if(rank==0) WRITE(IUNIT,6101) 
 6101 FORMAT(/,'QS2I1D- the sort control parameter, k, ',&
     &         'was not 1 OR -1.')
         RETURN
      ENDIF
!
      IF( KFLAG.LT.1 ) THEN
         DO 20 I=1,NN
            IA(I) = -IA(I)
 20      CONTINUE
      ENDIF
!
      M = 1
      I = 1
      J = NN
      R = 3.75D-1
 210  IF( R.LE.5.898437D-1 ) THEN
         R = R + 3.90625D-2
      ELSE
         R = R-2.1875D-1
      ENDIF
 225  K = I
!
!
      IJ = I + IDINT( DBLE(J-I)*R )
      IT = IA(IJ)
      JT = JA(IJ)
      TA = A(IJ)
!
!
      IF( IA(I).GT.IT ) THEN
         IA(IJ) = IA(I)
         IA(I)  = IT
         IT     = IA(IJ)
         JA(IJ) = JA(I)
         JA(I)  = JT
         JT     = JA(IJ)
         A(IJ)  = A(I)
         A(I)   = TA
         TA     = A(IJ)
      ENDIF
      L=J
!                           
!
      IF( IA(J).LT.IT ) THEN
         IA(IJ) = IA(J)
         IA(J)  = IT
         IT     = IA(IJ)
         JA(IJ) = JA(J)
         JA(J)  = JT
         JT     = JA(IJ)
         A(IJ)  = A(J)
         A(J)   = TA
         TA     = A(IJ)
!
!
         IF ( IA(I).GT.IT ) THEN
            IA(IJ) = IA(I)
            IA(I)  = IT
            IT     = IA(IJ)
            JA(IJ) = JA(I)
            JA(I)  = JT
            JT     = JA(IJ)
            A(IJ)  = A(I)
            A(I)   = TA
            TA     = A(IJ)
         ENDIF
      ENDIF
!
!
  240 L=L-1
      IF( IA(L).GT.IT ) GO TO 240
!
!
  245 K=K+1
      IF( IA(K).LT.IT ) GO TO 245
!
!
      IF( K.LE.L ) THEN
         IIT   = IA(L)
         IA(L) = IA(K)
         IA(K) = IIT
         JJT   = JA(L)
         JA(L) = JA(K)
         JA(K) = JJT
         TTA   = A(L)
         A(L)  = A(K)
         A(K)  = TTA
         GOTO 240
      ENDIF
!
!
      IF( L-I.GT.J-K ) THEN
         IL(M) = I
         IU(M) = L
         I = K
         M = M+1
      ELSE
         IL(M) = K
         IU(M) = J
         J = L
         M = M+1
      ENDIF
      GO TO 260
!
!                                  
  255 M = M-1
      IF( M.EQ.0 ) GO TO 300
      I = IL(M)
      J = IU(M)
  260 IF( J-I.GE.1 ) GO TO 225
      IF( I.EQ.J ) GO TO 255
      IF( I.EQ.1 ) GO TO 210
      I = I-1
  265 I = I+1
      IF( I.EQ.J ) GO TO 255
      IT = IA(I+1)
      JT = JA(I+1)
      TA =  A(I+1)
      IF( IA(I).LE.IT ) GO TO 265
      K=I
  270 IA(K+1) = IA(K)
      JA(K+1) = JA(K)
      A(K+1)  =  A(K)
      K = K-1
      IF( IT.LT.IA(K) ) GO TO 270
      IA(K+1) = IT
      JA(K+1) = JT
      A(K+1)  = TA
      GO TO 265
!
!
  300 IF( KFLAG.LT.1 ) THEN
         DO 310 I=1,NN
            IA(I) = -IA(I)
 310     CONTINUE
      ENDIF
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of QS2I1D
!
!
      RETURN
      END SUBROUTINE QS2I1D
!
!


      SUBROUTINE DCHKW( NAME, LOCIW, LENIW, LOCW, LENW,&
     &     IERR, ITER, ERR )
!
!
!
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      CHARACTER(LEN=*)::NAME
      CHARACTER, dimension(72) ::MESG
      INTEGER LOCIW, LENIW, LOCW, LENW, IERR, ITER
      REAL(KIND = 8) ERR
!
      REAL(KIND = 8) DMACH(5)
      DATA DMACH(2) / 1.79769313486231D+308 /
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DCHKW
!
!
      IERR = 0
      IF(LOCIW.GT.LENIW) THEN
         IERR = 1
         ITER = 0
         ERR = DMACH(2)
         MESG = NAME//': INTEGER work array too short. '//&
             ' IWORK needs i1: have allocated i2.'
      ENDIF
!
      IF(LOCW.GT.LENW) THEN
         IERR = 1
         ITER = 0
         ERR = DMACH(2)
         MESG = NAME//': REAL work array too short. '//&
             ' RWORK needs i1: have allocated i2.'
      ENDIF
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DCHKW
!
!
      RETURN
      END SUBROUTINE DCHKW
!
!
SUBROUTINE DSMV_PAR( NI, NT, X, Y, NELT, IA, JA, A, ISYM, UPDATE_P,update_l )
!DSMV = MATVEC
!Parallel version of the dot product happens after update
!
	use MPI_VAR
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      INTEGER :: NI,NT, NELT, IA(NELT), JA(NELT), ISYM
	  INTEGER, dimension(*) :: update_p,update_l
      REAL(KIND = 8) :: A(NELT), X(NT), Y(NT)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSMV
!
!
	!print *, "HE"
	
	!print *, size(update_p), size(update_l), 'asdas',rank
	call update(X,NI,NT,IA,JA,update_p,update_l)
	
      Y(1:NT) = 0.0d0     ! CAREFUL! Whole array operations
!
      DO irow = 1, NI
         do j = IA(irow),IA(irow+1)-1
	 ! if(rank==0 .And. irow==49) print *, Y(irow),A(j),X(JA(j)),JA(j)
			Y(irow) = Y(irow)+A(j)*X(JA(j))
		 end do
      END DO
	 ! if(rank==0) print *, "_-_________"
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DSMV
!
!
      RETURN
      END SUBROUTINE DSMV_PAR
!
!
!
FUNCTION DOT_PROD_PAR(V1,V2,NI) result(prod)
	use MPI_VAR
	implicit none
	include 'mpif.h'
	
	REAL (KIND=8), dimension(*) :: V1,V2
	REAL(KIND=8) :: prod,prod_part
	INTEGER :: NI, FLAG          !NI is size of V1 and V2.
	INTEGER :: i,ierr
	!DO local product
	prod_part = DOT_PRODUCT(V1(1:NI),V2(1:NI))
	
	!Combine local product and distribute to all
	call MPI_ALLREDUCE(prod_part, prod, 1, MPI_REAL8, MPI_SUM,&
        MPI_COMM_WORLD, ierr)
		
END FUNCTION DOT_PROD_PAR

subroutine update(X,NI,NT,IA,JA,update_p,update_l)
!PLEASE ENSURE THAT update_l is non repeating. An element should appear only once.
!Warning: check the size of stats allocated, maynot be enough	
	use MPI_VAR
	implicit none
	include 'mpif.h'
	
	REAL (KIND=8), dimension(*) :: X
	integer,dimension(*) :: IA,JA
	integer :: NI,NT, ierr, update_p(*), update_l(*)
	integer :: i,j, l
	integer:: temp(0:w_size-1)
	integer, dimension (:,:),allocatable :: stats
	integer, dimension((NT-NI)+(NT-NI)*w_size) :: request
	
	allocate(stats(MPI_STATUS_SIZE,(NT-NI)+(NT-NI)*w_size))
	
	
	l=1
	!print *, NI,NT,X(1:NI), "RANK"
	do i = 1, NI
		!Post the send requests
		!Loop over IA (each internal element) to find the external connections
		!Ensuring the element is sent to a neighboring processor only once
		temp = 0
		
		do j = IA(i+1)-1,IA(i),-1
			if(JA(j)<=NI) exit
			if(temp(update_p(JA(j)-NI))==0) then
				call MPI_ISEND(X(i),1,&
					MPI_REAL8,update_p(JA(j)-NI),i,MPI_COMM_WORLD,request(l), ierr)
				temp(update_p(JA(j)-NI))=1
				l=l+1
				!print *, rank, i, JA(j)-NI
			end if
		end do
		!Post the receive requests in reverse order
		if(i<=NT-NI)then
			call MPI_IRECV(X(NT-i+1),1,&
					MPI_REAL8,update_p(NT-NI-i+1),update_l(NT-NI-i+1),MPI_COMM_WORLD,request(l), ierr )
			l=l+1
		end if
	end do
	
	if(l>1) call MPI_Waitall(l-1,request,stats,ierr)
					
	deallocate(stats)
end subroutine update

subroutine Initialize_Exmap(head,IA,JA,A,update_p,update_l,NELT,NI,NT,COUNTS)


	use mpi_var
	implicit none
	type(HEADEX) :: head(0:w_size-1)
	type(Exel),pointer :: point
	integer :: IA(*),JA(*),NELT,NI,NT,update_p(*),update_l(*)
	real(kind=8) :: A(*)
	integer :: i,j, temp(0:w_size-1),COUNTS(0:w_size-1)
	
	do i=0,w_size-1
		NULLIFY(head(i)%next)
		NULLIFY(head(i)%curr)
		NULLIFY(head(i)%endp)
		NULLIFY(head(i)%ende)	
	end do
	COUNTS = 0
	do i = 1, NI
		!Loop over IA (each internal element) to find the external connections
		!Ensuring the element is sent to a neighboring processor only once
		temp = 0
		
		do j = IA(i+1)-1,IA(i),-1
			
			if(JA(j)<=NI) exit
			if(temp(update_p(JA(j)-NI))==0) then
				temp(update_p(JA(j)-NI))=1
				point=>head(update_p(JA(j)-NI))%endp
				if(.NOT. associated(point)) then
					allocate(head(update_p(JA(j)-NI))%endp)
					head(update_p(JA(j)-NI))%next=>head(update_p(JA(j)-NI))%endp
					point=>head(update_p(JA(j)-NI))%endp		
				else
					allocate(point%next)
					point=>point%next
				end if
				nullify(point%next)
				point%loc=i
				head(update_p(JA(j)-NI))%endp=>point
				COUNTS(update_p(JA(j)-NI)) = COUNTS(update_p(JA(j)-NI))+IA(i+1)-IA(i)
			end if
		end do
	end do
	do i=0,w_size-1
		head(i)%ende=>head(i)%endp
		head(i)%curr=>head(i)%next
	end do
end subroutine Initialize_Exmap

subroutine Free_Exmap(head)
	use mpi_var
	implicit none
	type(HEADEX) :: head(0:w_size-1)
	type(Exel), pointer :: point,temp
	integer :: i
	
	do i=0,w_size-1
		
		point => head(i)%next
		nullify(head(i)%next)
		nullify(head(i)%endp)
		nullify(head(i)%ende)
		nullify(head(i)%curr)
		
		do while(associated(point))
			temp => point%next 
			nullify(point%next)
			deallocate(point)
			nullify(point)
			point => temp
		end do
	end do
	
end subroutine Free_Exmap

subroutine update_map(X,NI,NT,update_p,update_l,head)
	use MPI_VAR
	implicit none
	include 'mpif.h'
	
	REAL (KIND=8), dimension(*) :: X
	integer :: NI,NT, ierr, update_p(*), update_l(*)
	integer :: i,j, l,lp
	integer :: stats(MPI_STATUS_SIZE,4*w_size)
	integer, dimension((NT-NI)+(NT-NI)*w_size) :: request,outarray
	type(HEADEX) :: HEAD(0:w_size-1)
	type(EXEL),pointer :: point 	
	integer :: calle=1,flag,outcount
	REAL (KIND=8), ALLOCATABLE :: SEND(:,:),RECV(:,:)
	INTEGER, ALLOCATABLE :: SENDI(:,:),RECVI(:,:),ms(:)
	INTEGER :: max_size
	logical :: flaglogic(4*w_size)
	
	!Finding max size to be sent to any proc
	max_size=-1
	l=0
	DO i =0,w_size-1
		point=>HEAD(i)%next
		do while(associated(point))
			l=l+1
			point=>point%next
		end do
		max_size = max(max_size,l)
	END DO
	allocate(SEND(max_size,0:w_size-1))
	allocate(SENDI(max_size,0:w_size-1))
	
	allocate(ms(0:w_size-1))
	!Finding max size to be recieved from any proc
	ms(0:w_size-1) = 0
	Do i = 1, NT-NI
		ms(update_p(i)) = ms(update_p(i)) + 1
	END DO
	allocate(RECV(maxval(ms(0:w_size-1)),0:w_size-1))
	allocate(RECVI(maxval(ms(0:w_size-1)),0:w_size-1))

	
	l=1
	do i=0,w_size-1
		
		!Post recv request
		if(ms(i) > 0) THEN
			call MPI_IRECV(RECV(1:ms(i),i),ms(i),&
					MPI_REAL8,i,0,MPI_COMM_WORLD,request(l), ierr )
			l=l+1
			call MPI_IRECV(RECVI(1:ms(i),i),ms(i),&
					MPI_INT,i,1,MPI_COMM_WORLD,request(l), ierr )
			l=l+1
		END IF
		
	end do
	lp=l
	do i = 0, w_size-1
		!Post the send requests
		j=0
		point=>HEAD(i)%next
		do while(associated(point))
			j=j+1
			SEND(j,i) = X(point%loc)
			SENDI(j,i) = point%loc
			point=>point%next
		end do
		if(j>0) then
			call MPI_ISEND(SEND(1:j,i),j,&
				MPI_REAL8,i,0,MPI_COMM_WORLD,request(l), ierr)
			l=l+1
			call MPI_ISEND(SENDI(1:j,i),j,&
				MPI_INT,i,1,MPI_COMM_WORLD,request(l), ierr)
			l=l+1
		end if
	end do
	
	if(l-1>size(request)) then
		if (rank==0) print *, "SIZE of request and status is less in update_map. ERROR!"
		stop
	end if
	if(l>1) call MPI_Waitall(l-1,request,MPI_STATUSES_IGNORE,ierr)
		
	DO i=1,NT-NI
		DO J=1,MS(update_p(i))
			IF(RECVI(J,update_p(i))==update_l(i)) THEN
				X(NI+i) = RECV(J,update_p(i))
				EXIT
			END IF
		END DO
		if(J>MS(update_p(i))) THEN
			print *, "ERROR COMM 889XX"
			STOP
		End if
	END DO
	
	deallocate(SEND)
	deallocate(SENDI)
	deallocate(RECV)
	deallocate(RECVI)
	deallocate(ms)
	calle = calle+1
end subroutine update_map

subroutine add_mapel(loc,proc,updatel,NMAP)
	use mpi_var
	implicit none
	TYPE(HeadMAPEL) :: NMAP(0:w_size-1)
	TYPE(MAPEL),pointer :: curr_mapel
	integer :: loc,proc,updatel,updatep
	
		Curr_mapel => NMAP(proc)%ende
		if(.NOT. associated(curr_mapel)) then
			allocate(NMAP(proc)%ende)
			Curr_mapel=> NMAP(proc)%ende
			NMAP(proc)%next=> NMAP(proc)%ende
		else
			allocate(curr_mapel%next)
			curr_mapel => curr_mapel%next
			NMAP(proc)%ende=>curr_mapel
		end if
		
		nullify(curr_mapel%next)
		curr_mapel%update_l = updatel
		curr_mapel%IA = loc
end subroutine add_mapel

subroutine freeext(HEADEXT)
	use mpi_var
	implicit none
	type(HDEXT) :: HEADEXT(0:w_size-1)
	type(EXT),pointer :: point,temp
	integer :: i
	
	do i=0,w_size-1
		point=>HEADEXT(i)%next
		nullify(HEADEXT(i)%next)
		do while (associated(point))
			temp => point%next 
			nullify(point%next)
			deallocate(point)
			nullify(point)
			point => temp
		end do
	end do
end subroutine freeext




subroutine SCHWZ_PRC(NI,NT,NI_P,NELT,A,IA,JA,update_l,update_p,overlap,MAAS,LOCIB,&
					& head,heads,COUNTS,IWORK,RWORK,IUNIT,LENW,LENIW,update_g,global,REORDER,REORDERT,RE)
	!CHECK SIZE OF REQUEST AND STATS
	use mpi_var
	implicit none
	include 'mpif.h'
	
	integer,intent(in) :: NI,NT,NELT,MAAS,IUNIT,RE
	integer, intent(in) :: overlap !Overlap needed for schwarz preconditioner
	integer, intent(in) :: IA(*),JA(*),LOCIB,LENW,LENIW
	real(kind=8),intent(in) :: A(*)
	real(kind=8) :: AA(MAAS),RWORK(*)
	integer :: IAA(MAAS),JAA(MAAS),update_l(*),update_p(*),IWORK(*)
	integer,dimension(0:w_size-1) :: COUNTS,COUNTR,COUNTRE,COUNTE,FLAG_RE
	integer :: ni_p, nt_p, NELT_P
	type(BUFFEL),dimension(0:w_size-1) :: SBUFF, EXSBUFF,EXRBUFF     !Send buffer
	type(HeadMAPEL) :: NMAP(0:w_size-1)
	type(MAPEL), pointer :: curr_mapel
	integer :: i,ii,j,k,l,ierr,ucount,gcount,loc,proc,endloc,NT_PC
	integer:: temp(0:w_size-1),flag(0:w_size-1)
	integer:: updaten_l(MAAS),updaten_p(MAAS),updaten_g(MAAS)
	integer, dimension (MPI_STATUS_SIZE,20*w_size) :: stats
	logical, dimension (20*w_size) :: flagstatus
	integer, dimension(20*w_size) :: request
	type(HEADEX) :: head(0:w_size-1),heads(0:w_size-1)
	type(HDEXT) :: headext(0:w_size-1)
	type(EXEL),pointer:: tpoint,point,point2
	TYPE(EXT),pointer :: pointx
	integer :: NL,NU,LOCIL,LOCJL,LOCIU,LOCJU,LOCL,LOCDIN,LOCUU,LOCIW,LOCNC,LOCNR
	REAL(KIND=8) :: CO_ROW(0:w_size-1)
	
	real(kind=8) :: TempA
	integer :: TempJA,TNELT
	
	INTEGER :: update_g(MAAS),global(NI), REORDER(MAAS),REORDERT(MAAS)

	real(kind=8),allocatable :: AWP(:)
	INTEGER,allocatable :: IWP(:),JWP(:),VECT(:)
	integer :: vstart,vend
	
	!Nicolaides coarse grid
	CO_ROW=0.0
	do i=1,NI
		do j=IA(i),IA(i+1)-1
			if(JA(J)<=NI) THEN
				CO_ROW(rank)=CO_ROW(rank)+A(J)
			else
				CO_ROW(update_p(JA(J)-NI))=CO_ROW(update_p(JA(J)-NI))+A(J)
			end if
		end do
	end do
	
	CALL COARSE_LU(CO_ROW(0:w_size-1))
		
	do i=1,IA(NI+1)-1 !loop till NELT
		AA(i) = A(i)
		JAA(i) = JA(i)
	end do
	IAA(1:NI+1) = IA(1:NI+1)
	nt_p = nt
	ni_p = ni
	NELT_P = NELT

	updaten_p(1:nt_p-ni_p) = update_p(1:nt_p-ni_p)
	updaten_l(1:nt_p-ni_p) = update_l(1:nt_p-ni_p)
	updaten_g(1:nt_p-ni_p) = update_g(1:nt_p-ni_p)

	gcount = nt_p-ni_p

	do i=0,w_size-1
		NULLIFY(NMAP(i)%next)
		NULLIFY(NMAP(i)%ende)
	end do
	COUNTE=0
	!Forming map for current update boundary elements against their process number and storing their local numbers
		do i=1,nt_p-ni_p
			call add_mapel(ni_p+i,updaten_p(i),updaten_l(i),NMAP)
		end do
	flag=0
	where(counts>0) flag=1
	!Iterating for each next overlap
	do ii=1,overlap
	
		!Forming map for new external boundary 
			do i=0,w_size-1
				NULLIFY(headext(i)%next)
				NULLIFY(headext(i)%ende)	
			end do
			if(ii>1) then
			
				COUNTS=0
				flag=0
				COUNTE=0
				do i=0,w_size-1
				
					point=>HEAD(i)%curr
					tpoint=>HEAD(i)%ende
					if(associated(tpoint)) endloc=tpoint%loc
					do while(associated(point))
						
						loc=point%loc
						!if(rank==1 .and. i==0) print *,loc,JAA(IAA(loc)),JAA(IAA(loc+1)-1)
						do j=IAA(loc)+1,IAA(loc+1)-1
							if(JAA(j)<=NI) then 
								point2=>head(i)%next
								do while(associated(point2))
									if(point2%loc==JAA(j)) exit
									point2=>point2%next
								end do
								if(associated(point2)) cycle
								allocate(head(i)%ende%next)
								point2=>head(i)%ende%next
								point2%loc=JAA(j)
								NULLIFY(point2%next)
								if(flag(i)/=1) THEN
									HEAD(i)%curr=>point2
									flag(i)=1
								END IF
								head(i)%ende=>point2
								COUNTS(i)=COUNTS(i)+IAA(JAA(j)+1)-IAA(JAA(j))
							else
							
								if(update_p(JAA(j)-NI)==i) cycle
								pointx=>headext(update_p(JAA(j)-NI))%next
								if(associated(pointx)) then
									do while(associated(pointx))
										if(pointx%loc==update_l(JAA(j)-NI) .and. pointx%update_p==i) exit
										pointx=>pointx%next
									end do
									if(associated(pointx)) cycle
									allocate(headext(update_p(JAA(j)-NI))%ende%next)
									pointx=>headext(update_p(JAA(j)-NI))%ende%next
								else
									allocate(headext(update_p(JAA(j)-NI))%next)
									pointx=>headext(update_p(JAA(j)-NI))%next
									headext(update_p(JAA(j)-NI))%ende=>pointx
								end if
								pointx%loc=update_l(JAA(j)-NI)
								pointx%update_p=i
								NULLIFY(pointx%next)
								headext(update_p(JAA(j)-NI))%ende=>pointx
								COUNTE(update_p(JAA(j)-NI))=COUNTE(update_p(JAA(j)-NI))+1	
								end if
							
						end do
						if(point%loc==endloc) exit
						point=>point%next
						
					end do
				end do
				
			end if
			
			if(ii>1) then
				l=1
				COUNTRE = 0
				!Sending counte and recieving it from other processes as countre
				do i=0,w_size-1
						CALL MPI_ISEND(COUNTE(i),1,MPI_INTEGER,i,0,MPI_COMM_WORLD,request(l),ierr)
						l=l+1
				
						call MPI_IRECV(COUNTRE(i),1,&
							MPI_INTEGER,i,0,MPI_COMM_WORLD,request(l), ierr )
						l=l+1
				end do
				
					
				if(l>1) call MPI_Waitall(l-1,request,stats,ierr)
			
				l=1
				do i=0,w_size-1
					if(counte(i)/=0) then
						allocate(EXSBUFF(i)%IA(2*counte(i)),STAT=IERR)	
						if(IERR/=0) print *, "MEMORY ALLOCATION ERROR AT 88XY"
						pointx=>HEADEXT(i)%next
						counte(i)=0
						
						do while(associated(pointx))
							EXSBUFF(i)%IA(counte(i)+1) = pointx%loc
							EXSBUFF(i)%IA(counte(i)+2) = pointx%update_p
							counte(i)=counte(i)+2
							pointx=>pointx%next
						end do
						
						counte(i)=counte(i)/2
						call MPI_ISEND(EXSBUFF(i)%IA(1:2*counte(i)),2*counte(i),&
						MPI_INT,i,1,MPI_COMM_WORLD,request(l), ierr)
						if(ierr/=MPI_SUCCESS) print *,RANK,"MPI_ISEND ERROR",IERR 
						l=l+1
					end if
				end do
				
				do i=0,w_size-1
					if(countre(i)/=0) then
						allocate(EXRBUFF(i)%IA(2*countre(i)),STAT=IERR)	
						if(IERR/=0) print *, "MEMORY ALLOCATION ERROR AT 88XZ"
						call MPI_IRECV(EXRBUFF(i)%IA(1:2*countre(i)),2*countre(i),&
							MPI_INT,i,1,MPI_COMM_WORLD,request(l), ierr )
						l=l+1
					end if
				end do
				if(l>1) call MPI_Waitall(l-1,request,stats,ierr)
			
				
				do i=0,w_size-1
					if(COUNTRE(i)==0) cycle
					do j=1,COUNTRE(i)
						loc=EXRBUFF(i)%IA(2*j-1)
						proc=EXRBUFF(i)%IA(2*j)
						point2=>head(proc)%next
						if(associated(point2)) then
							do while(associated(point2))
								if(point2%loc==loc) exit
								point2=>point2%next
							end do
							if(associated(point2)) THEN	
								cycle
							end if
							allocate(head(proc)%ende%next)
							point2=>head(proc)%ende%next
						else
							allocate(head(proc)%ende)
							head(proc)%next=>head(proc)%ende
							head(proc)%curr=>head(proc)%ende
							point2=>head(proc)%ende
							flag(proc)=1
						end if
						point2%loc=loc
						NULLIFY(point2%next)
						if(flag(proc)/=1) then
							!We have to reassign HEAD(proc)%curr
							head(proc)%curr=>point2
							flag(proc)=1
						end if
						head(proc)%ende=>point2
						COUNTS(proc)=COUNTS(proc)+IAA(loc+1)-IAA(loc)
					end do
				end do
				
			end if
			
			COUNTR=0
			l=1
			do i=0,w_size-1
			
				!if(flag(i)==0) cycle
				
				call MPI_ISEND(counts(i),1,&
				MPI_INTEGER,i,2,MPI_COMM_WORLD,request(l), ierr)
				l=l+1
				
				call MPI_IRECV(countr(i),1,&
					MPI_INTEGER,i,2,MPI_COMM_WORLD,request(l), ierr )
				l=l+1
				
			end do
			if(l>1) call MPI_Waitall(l-1,request,stats,ierr)
			

		!Check if enough memory for receiving is present
			TNELT = NELT_P
			do i=0,w_size-1
				TNELT = TNELT+countr(i)
			end do
			if(TNELT>MAAS) then
				write(*,*), "AT PROCESS",rank,"NOT ENOUGH MEMORY TO &
				RECEIVE IN SCHWZ_PRC, AT OVERLAP ITERATION", ii,"AVAILABLE",MAAS,"NEEDED",TNELT
				stop
				return
			end if
	
		!Allocate then loop to form A, IA, JA, and update_p to be sent to neighboring process
			
		do i=0,w_size-1
				if(COUNTS(i)==0) cycle
				allocate(SBUFF(i)%A(counts(i)))
				allocate(SBUFF(i)%IA(counts(i)))
				allocate(SBUFF(i)%JA(counts(i)))
				allocate(SBUFF(i)%update_p(counts(i)))
				allocate(SBUFF(i)%update_g(counts(i)))
		end do
			COUNTS = 0
			
			do i=0,w_size-1
				point=>head(i)%curr
				do while(associated(point))
					do k=IA(point%loc),IA(point%loc+1)-1,1
						SBUFF(i)%A(counts(i)+1) = A(k)
						if(point%loc<=NI) then
							SBUFF(i)%IA(counts(i)+1) = point%loc
						else
							SBUFF(i)%IA(counts(i)+1) = update_l(point%loc-NI)
						end if
						
						if(JA(k)<=NI) then		
							SBUFF(i)%JA(counts(i)+1) = JA(k)
							SBUFF(i)%update_p(counts(i)+1) = rank
							SBUFF(i)%update_g(counts(i)+1) = global(JA(k))
						else
							SBUFF(i)%JA(counts(i)+1) = update_l(JA(k)-NI)
							SBUFF(i)%update_p(counts(i)+1) = update_p(JA(k)-NI)
							SBUFF(i)%update_g(counts(i)+1) = update_g(JA(k)-NI)
						end if
						COUNTS(i) = COUNTS(i) + 1
					end do
					point=>point%next
				end do
			end do
		
		
			l=1
			do i=0,w_size-1
				if(COUNTS(i)/=0) THEN
				
				
					call MPI_ISEND(SBUFF(i)%A,counts(i),&
					MPI_REAL8,i,3,MPI_COMM_WORLD,request(l), ierr)
					l=l+1
					call MPI_ISEND(SBUFF(i)%IA,counts(i),&
					MPI_INTEGER,i,4,MPI_COMM_WORLD,request(l), ierr)
					l=l+1
					call MPI_ISEND(SBUFF(i)%JA,counts(i),&
					MPI_INTEGER,i,5,MPI_COMM_WORLD,request(l), ierr)
					l=l+1
					call MPI_ISEND(SBUFF(i)%update_p,counts(i),&
					MPI_INTEGER,i,6,MPI_COMM_WORLD,request(l), ierr)
					l=l+1
					call MPI_ISEND(SBUFF(i)%update_g,counts(i),&
					MPI_INTEGER,i,7,MPI_COMM_WORLD,request(l), ierr)
					l=l+1
				END IF
				
				IF(COUNTR(i)/=0) THEN
					call MPI_IRECV(AA(NELT_P+1 : NELT_P +COUNTR(i)),COUNTR(i),&
						MPI_REAL8,i,3,MPI_COMM_WORLD,request(l), ierr )
					l=l+1
					call MPI_IRECV(IAA(NELT_P+1 : NELT_P +COUNTR(i)),COUNTR(i),&
						MPI_INTEGER,i,4,MPI_COMM_WORLD,request(l), ierr )
					l=l+1
					call MPI_IRECV(JAA(NELT_P+1 : NELT_P +COUNTR(i)),COUNTR(i),&
						MPI_INTEGER,i,5,MPI_COMM_WORLD,request(l), ierr )
					l=l+1
					call MPI_IRECV(updaten_p(NELT_P+1 : NELT_P +COUNTR(i)),COUNTR(i),&
						MPI_INTEGER,i,6,MPI_COMM_WORLD,request(l), ierr )
					l=l+1
					call MPI_IRECV(updaten_g(NELT_P+1 : NELT_P +COUNTR(i)),COUNTR(i),&
						MPI_INTEGER,i,7,MPI_COMM_WORLD,request(l), ierr )
					l=l+1
					NELT_P = NELT_P + countr(i)
				END IF
			end do	
			if(l>1) call MPI_Waitall(l-1,request,stats,ierr)
		
			do i=0,w_size-1
				if(COUNTS(i)==0) cycle
				deallocate(SBUFF(i)%A)
				deallocate(SBUFF(i)%IA)
				deallocate(SBUFF(i)%JA)
				deallocate(SBUFF(i)%update_p)
				deallocate(SBUFF(i)%update_g)
			end do
			
			if(ii>1) then
				do i=0,w_size-1
					if(counte(i)/=0) then
						deallocate(EXSBUFF(i)%IA)
					end if
					if(countre(i)/=0) then
						deallocate(EXRBUFF(i)%IA)
					end if
				end do
			end if
			
		!Modifying IA and JA of the newly received elements to have the local numbering of current process
			ucount = 1
			NT_PC=NT_P
			l = IAA(NI_P+1)
			FLAG_RE = 0
			!if(rank==1) print *, IAA(l+1:l+counts(0))
			
			!if(rank==1) print *, JAA(l+1:l+counts(0))
			
			!if(rank==1) print *, updaten_p(l+1:l+counts(0))
			
			do i=0,w_size-1
				k=1
				do j=l+1, l+countr(i)
					if(j<=l+countr(i)-1 .and. IAA(j)==IAA(j-1)) then
						k=k+1
					else
						IAA(j-k:j-1)=find_loc(IAA(j-1),i,NT_PC,NMAP)
						if(IAA(j-1)==NT_PC+1) THEN
							print *,"ERROR 232 ","RANK",rank,"Element",IAA(j-1),"FROM proc",i
							stop
						end if
						k=1
					end if
					if(updaten_p(j-1) /= rank) then
						loc = find_loc(JAA(j-1),updaten_p(j-1),NT_PC,NMAP)
						if(updaten_p(j-1)/=i) FLAG_RE(i) = FLAG_RE(i)+1
						if(loc==NT_PC+1) then
							updaten_l(ucount) = JAA(j-1)
							updaten_p(ucount) = updaten_p(j-1)
							updaten_g(ucount) = updaten_g(j-1)
							ucount = ucount+1
							NT_PC=NT_PC+1
							call add_mapel(NT_PC,updaten_p(ucount-1),updaten_l(ucount-1),NMAP)
						end if
						JAA(j-1)=loc
					end if	
				end do			
				l = countr(i) + l
			end do

			

			update_l(gcount+1:gcount+ucount-1) = updaten_l(1:ucount-1)
			update_p(gcount+1:gcount+ucount-1) = updaten_p(1:ucount-1)
			update_g(gcount+1:gcount+ucount-1) = updaten_g(1:ucount-1)
			gcount = gcount+ucount-1
			
		!Sorting out the newly incorporated elements
		    l=IAA(NI_P+1)
			
			if(NELT_P>=l) call QS2I1D(IAA(l:NELT_P),JAA(l:NELT_P), &
					AA(l:NELT_P), NELT_P-l+1, 1, IUNIT )
				
			j= NI_P+1
			IAA(j+1) = IAA(j)
			k=1
			do i = IAA(NI_P+1)+1,NELT_P+1
				if ( i<=NELT_P .and. IAA(i)==IAA(i-1)) then
					IAA(j+1) = IAA(j+1) + 1
					k=k+1
					cycle
				end if
				IAA(j+1) = IAA(j+1) + 1
				j=j+1	
				k=1
				IAA(j+1) = IAA(j)
			end do	
			IAA(NT_P+1)=NELT_P+1
			
			
			!Sorting within each row
			do i=NI_P+1,NT_P
				!First element to be diagonal element
				do j=IAA(i),IAA(i+1)-1
					if(JAA(j) == i) then
						TempA = AA(j)
						AA(j) = AA(IAA(i))
						AA(IAA(i)) = TempA
						
						TempJA = JAA(j)
						JAA(j) = JAA(IAA(i))
						JAA(IAA(i)) = TempJA
						exit
					end if
				end do
				
				!Subsequent sorting
				do j=IAA(i)+1,IAA(i+1)-2
					do k=j+1,IAA(i+1)-1
						if(JAA(k) < JAA(j)) then
							tempJA = JAA(k)
							JAA(k) = JAA(j)
							JAA(j) = tempJA
							
							TempA = AA(k)
							AA(k) = AA(j)
							AA(j) = TempA
						end if
					end do
				end do
			end do
					
! if(calle==1 ) then
		! print *,"RANK",rank, NI_P,NT_P,NELT_P
		! call MPI_BARRIER(MPI_COMM_WORLD,ierr)		
! stop	
! end if 
			NI_P = NT_P
			NT_P = NT_P + ucount-1
			
			call freeext(HEADEXT)
				! if(ii==2) then
			! 	 *, "RANK",rank,"COUNTS",counts,"COUNTR",countr
				! print *, ""
				! call MPI_BARRIER(MPI_COMM_WORLD,ierr)
				! stop
			! end if	
			
	end do
 
!----------SECTION STARTING REORDERING SEQUENCE------------------
	allocate(AWP(NELT_P))
	allocate(IWP(NI_P+1))
	allocate(JWP(NELT_P))
	
	IF(RE==1) THEN
		vstart = minval((/ global(1:NI),update_g(1:NI_P-NI)/))
		vend = 	maxval((/ global(1:NI),update_g(1:NI_P-NI)/))
		allocate(VECT(vstart:vend))
		
		VECT = 0
		do i=1,NI
			VECT(global(i)) = i
		end do
		do i=1,NI_P-NI
			VECT(update_g(i)) = NI+i
		end do
		!------------------Forming Reordered Matrix---------------------------------------
		j=0
		do i=vstart,vend
			if(VECT(i)<1) CYCLE
			J = J+1
			REORDER(j)=VECT(i)
		end do
		REORDER(NI_P+1:NT_P) = (/ (i, i=NI_P+1,NT_P)  /)
	ELSE
		REORDER(1:NT_P) = (/ (i, i=1,NT_P)  /)
	END IF
	!--------------Finding new AA-IAA-JAA in AWP-IWP-JWP
	IWP(1) = 1
	NELT_P = 0
	do i=1,NI_P
		j=REORDER(i)
		AWP(NELT_P+1:NELT_P + IAA(j+1)-iAA(j)) = AA(IAA(j):IAA(J+1)-1)
		JWP(NELT_P+1:NELT_P + IAA(j+1)-iAA(j)) = JAA(IAA(j):IAA(J+1)-1)
		IWP(i+1)=IWP(i) + IAA(j+1)-IAA(j)
		NELT_P = NELT_P + IAA(j+1)-IAA(j)
	END DO

	!Finding transpose of Reorder
	REORDERT(REORDER(1:NT_P)) = (/ (i,i=1,NT_P) /)
	!Applying reordering sequence to the COLUMNS of sparse compressed matrix (AA-IAA-JAA)
	
	IF(RE==1) THEN
		DO I=1,NI_P
			JWP(IWP(I)) = I
			DO J= IWP(I)+1,IWP(I+1)-1
				JWP(J)=REORDERT(JWP(J))
			END DO
		END DO
	
		!------------SORTING-------------------
		
		!Sorting within each row
		do i=1,NI_P
			!Sorting non-digonal elements
			do j=IWP(i)+1,IWP(i+1)-2
				do k=j+1,IWP(i+1)-1
					if(JWP(k) < JWP(j)) then
						tempJA = JWP(k)
						JWP(k) = JWP(j)
						JWP(j) = tempJA
						
						TempA = AWP(k)
						AWP(k) = AWP(j)
						AWP(j) = TempA
					end if
				end do
			end do
		end do
	END IF
			
	!----------------
!---------------------END of reordering-------------------------------------------
	
	!Find NL and NU
	NL=0
	NU=0
	
	 
	do i=1,NI_P
		do j=IWP(i)+1,IWP(i+1)-1,1
			if(JWP(j)>NI_P) EXIT
			if(JWP(j)>i) then
				NU=NU+1
			else
				NL=NL+1
			end if
		end do
	end do
	
	!Assign position in Rwork array
	  LOCL   = IWORK(10) + NELT_P      
      LOCDIN = LOCL + NELT_P
      LOCUU  = LOCDIN + NELT_P
	!Assign position in iwork array
	  LOCIL = LOCIB !Starting location in IWORK
      LOCJL = LOCIL + NI_P+1
      LOCIU = LOCJL + NL
      LOCJU = LOCIU + NU
      LOCNR = LOCJU + NI_P+1	!LOCNR = NROW in DSILUS
      LOCNC = LOCNR + NI_P		!LOCNC = NCOL in DSILUS
      LOCIW = LOCNC + NI_P
	
	  IWORK(1) = LOCIL
      IWORK(2) = LOCJL
      IWORK(3) = LOCIU
      IWORK(4) = LOCJU
      IWORK(5) = LOCL
      IWORK(6) = LOCDIN
      IWORK(7) = LOCUU
      IWORK(9) = LOCIW
	  
	!Checking allocation size
	
	if(LOCIW>LENIW .OR. LOCUU+NELT_P>LENW) then
		write(*,*), "AT PROCESS",rank,"NOT ENOUGH MEMORY TO CARRY OUT ILU forward elimination"	
		print *, "ABORTING LOC 88SZXX"
		STOP
	end if
	
	!!Forming L, D and U after the forward elimination of ILU(0)
		!if(rank==0 ) then
	!print *, NELT_P, NI_P,NT_P,NELT, NI, NT, rank
	!print *,rank,"RANK A",A(1:NELT)
	!print *,rank,"RANK A",JA(1:NELT)
	!print *,rank,"RANK A",IA(1:NI+1)
	!print *,rank,"RANK A",AA(1:NELT_P)
	!print *,rank,"RANK A",JAA(1:NELT_P)
	!print *,rank,"RANK A",IAA(1:NI_P+1)
	!print *,rank,"RANK A",IAA(NELT+1:NELT_P)
	!stop
	!end if
	 
! if(calle==1) then
 !print *, IAA(1:NI_P+1),"NELT_P",NELT_P, rank
 ! if(rank==0) print *, NI_P, update_p(NI_P-NI), update_l(NI_P-NI), "RRA" ,rank
 ! call MPI_BARRIER(MPI_COMM_WORLD,ierr)
 ! stop
 ! end if
call  DSILUS_SCHW(NI_P,NELT_P,IWP,JWP,AWP,NL,IWORK(LOCIL),IWORK(LOCJL),&
     &                  RWORK(LOCL),RWORK(LOCDIN),NU,IWORK(LOCIU),IWORK(LOCJU),RWORK(LOCUU),IWORK(LOCNR),IWORK(LOCNC))
	 
	call deallocate_nmap(NMAP)
	
	
	
	do i=0,w_size-1
		HEADS(i)%next=>HEAD(i)%next
		HEADS(i)%endp=>HEAD(i)%endp
		HEADS(i)%ende=>HEAD(i)%ende
		
		if(associated(HEAD(i)%endp)) then
			if(associated(HEAD(i)%endp%next)) then	
				HEADS(i)%curr=>HEAD(i)%endp%next		
				HEADS(i)%ende%next=>HEADS(i)%next
				NULLIFY(HEAD(i)%endp%next)
			else
				HEADS(i)%curr=>HEADS(i)%next
			end if
			HEADS(i)%next=>HEADS(i)%curr
		else
			HEADS(i)%curr=>HEAD(i)%next
			NULLIFY(HEAD(i)%next)
		end if
		
	end do
	! if(rank==0) then
	! do i=0,w_size-1
		! if(i==1) print *, HEAD(i)%endp%loc, "@@"
		! point=>HEAD(i)%next
		! do while(associated(point))
			! print *, point%loc
			! point=>point%next
		! end do
	! end do
	! end if
	! call MPI_BARRIER(MPI_COMM_WORLD,ierr)
	! stop
	deallocate(IWP)
	deallocate(AWP)
	deallocate(JWP)
	if(RE==1) deallocate(VECT)
end subroutine SCHWZ_PRC


subroutine deallocate_nmap(NMAP)
	use mpi_var
	implicit none
	
	type(HeadMAPEL) :: NMAP(0:w_size-1)
	type(MAPEL),pointer :: head,temp
	integer :: i
	
	do i=0,w_size-1
		
		head => NMAP(i)%next
		nullify(NMAP(i)%next)
		nullify(NMAP(i)%ende)
		do while(associated(head))
			temp => head%next 
			nullify(head%next)
			deallocate(head)
			nullify(head)
			head => temp
		end do
	end do
end subroutine deallocate_nmap


integer function find_loc(L_num,proc,N,NMAP)
	use mpi_var
	implicit none
	integer :: L_num,proc,N
	type(HeadMAPEL) :: NMAP(0:w_size-1)
	type(MAPEL),pointer :: curr_el
	
	curr_el => NMAP(proc)%next
	do while(associated(curr_el))
		if(curr_el%update_l /= L_num) then
			curr_el =>curr_el%next
			cycle
		end if
		find_loc = curr_el%IA
		return
	end do
		find_loc = N+1
end function find_loc


SUBROUTINE DSILUS_SCHW(N,NELT,IA,JA,A,NL,IL,JL,&
     &                  L,DINV,NU,IU,JU,U,NROW,NCOL)
!DSILUS_SCHW DIFFERES FROM DSILUS in that first DSILUS is in column SLAP and secondly SCHW results in extra elements.. 
!..due to overlapping. That we have to filter in this subroutine
!!Parallel version of incomplete LDU preconditioner changed by Aman Rusia on 160616
!!N - size of system (originally), NELT, IA, JA, A, NL, IWORK(LOCIL),&
  !        & IWORK(LOCJL), RWORK(LOCL), RWORK(LOCDIN), NU, IWORK(LOCIU),&
  !       & IWORK(LOCJU), RWORK(LOCU), IWORK(LOCNR), IWORK(LOCNC)
!NROW - size 1:N. Since U is stored in SLAP row format. Nrow(i) is number of upper elements in ith column
!NCOL - size 1:N. Since L is stored in SLAP column format. Ncol(i) is number of lower elements in ith column
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      INTEGER N, NELT,IA(NELT),JA(NELT),NL,IL(NL),JL(NL)
      INTEGER NU, IU(NU), JU(NU), NROW(N), NCOL(N)			!Although the size of IL and IU are set as NL and NU, there is enough space as provided in DSLUCS. (check!)
      REAL(KIND = 8) A(NELT), L(NL), DINV(N), U(NU)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSILUS
!
!
      NROW(1:N) = 0
      NCOL(1:N) = 0
!

      DO IROW = 1, N
         IBGN = IA(IROW)+1
         IEND = IA(IROW+1)-1
         IF( IBGN.LE.IEND ) THEN
            DO  I = IBGN, IEND
				IF((JA(I) > N) ) CYCLE
               IF( (JA(I).GT.IROW)) THEN
                  NCOL(JA(I)) = NCOL(JA(I)) + 1
               ELSE
                  NROW(IROW) = NROW(IROW) + 1
               ENDIF
			END DO
         ENDIF
	  END DO
	  
!Forming JU - column of SLAP column format in U. and IL - row of SLAP row format in L. 1st element of U and L are always 0.
      JU(1) = 1
      IL(1) = 1
      DO 40 ICOL = 1, N
         IL(ICOL+1) = IL(ICOL) + NROW(ICOL)
         JU(ICOL+1) = JU(ICOL) + NCOL(ICOL)
         NROW(ICOL) = IL(ICOL)
         NCOL(ICOL) = JU(ICOL)
 40   CONTINUE
 
!Copying values into IU, U, JL and L. Note no diagonal elements are stored in either L or U        
      DO 60 IROW = 1, N
         DINV(IROW) = A(IA(IROW))
         IBGN = IA(IROW)+1
         IEND = IA(IROW+1)-1
         IF( IBGN.LE.IEND ) THEN
            DO 50 I = IBGN, IEND
               ICOL = JA(I)
			   IF((JA(I) > N)) CYCLE
               IF( (IROW.LT.ICOL) ) THEN
                  IU(NCOL(ICOL)) = IROW
                  U(NCOL(ICOL)) = A(I)
                  NCOL(ICOL) = NCOL(ICOL) + 1
               ELSE
                  JL(NROW(IROW)) = ICOL
                  L(NROW(IROW)) = A(I)
                  NROW(IROW) = NROW(IROW) + 1
               ENDIF
 50         CONTINUE
         ENDIF
 60   CONTINUE
	  
!Sorting of row and column number. So the appear incremently in each format.
      DO 110 K = 2, N
         JBGN = JU(K)
         JEND = JU(K+1)-1
         IF( JBGN.LT.JEND ) THEN !Do the sorting of row numbers in i increment order for each column.
            DO 80 J = JBGN, JEND-1
               DO 70 I = J+1, JEND
                  IF( IU(J).GT.IU(I) ) THEN
                     ITEMP = IU(J)
                     IU(J) = IU(I)
                     IU(I) = ITEMP
                     TEMP = U(J)
                     U(J) = U(I)
                     U(I) = TEMP
                  ENDIF
 70            CONTINUE
 80         CONTINUE
         ENDIF
         IBGN = IL(K)
         IEND = IL(K+1)-1
         IF( IBGN.LT.IEND ) THEN !Do the sorting of column numbers in each row.
            DO 100 I = IBGN, IEND-1
               DO 90 J = I+1, IEND
                  IF( JL(I).GT.JL(J) ) THEN
                     JTEMP = JU(I)
                     JU(I) = JU(J)
                     JU(J) = JTEMP
                     TEMP = L(I)
                     L(I) = L(J)
                     L(J) = TEMP
                  ENDIF
 90            CONTINUE
 100        CONTINUE
         ENDIF
 110  CONTINUE
 
!Carrying out forward Sweep
      DO 300 I=2,N  
!Changing lower elements         
         INDX1 = IL(I)
         INDX2 = IL(I+1) - 1
         IF(INDX1 .GT. INDX2) GO TO 200 !No lower elements in the row
         DO 190 INDX=INDX1,INDX2		!Column loop for a row in L
            IF(INDX .EQ. INDX1) GO TO 180 !only one lower element
            INDXR1 = INDX1				
            INDXR2 = INDX - 1
            INDXC1 = JU(JL(INDX))    !JU(JL(INDX)) is just the index of the first element in the same column as INDX present in U
            INDXC2 = JU(JL(INDX)+1) - 1 !Lower most element in a column of U where INDXC1 is the top most.
            IF(INDXC1 .GT. INDXC2) GO TO 180 !No elements in U in the given column 
 160        KR = JL(INDXR1) !Column offset of first element in the current row.
 170        KC = IU(INDXC1) !Row offset of first element in the current column @U
            IF(KR .GT. KC) THEN
               INDXC1 = INDXC1 + 1	
               IF(INDXC1 .LE. INDXC2) GO TO 170
            ELSE IF(KR .LT. KC) THEN
               INDXR1 = INDXR1 + 1
               IF(INDXR1 .LE. INDXR2) GO TO 160
            ELSE IF(KR .EQ. KC) THEN
               L(INDX) = L(INDX) - L(INDXR1)*DINV(KC)*U(INDXC1) !go through transformations. NOTE - EXTRA DINV(KC) because U is also divided by DINV - since this is LDU factorization and not LU.
               INDXR1 = INDXR1 + 1
               INDXC1 = INDXC1 + 1
               IF(INDXR1 .LE. INDXR2 .AND. INDXC1 .LE. INDXC2) GO TO 160
            END IF
 180        L(INDX) = L(INDX)/DINV(JL(INDX)) !Final value stored.
 190     CONTINUE
!Changing upper elements         
 200     INDX1 = JU(I)
         INDX2 = JU(I+1) - 1
         IF(INDX1 .GT. INDX2) GO TO 260
         DO 250 INDX=INDX1,INDX2
            IF(INDX .EQ. INDX1) GO TO 240
            INDXC1 = INDX1
            INDXC2 = INDX - 1
            INDXR1 = IL(IU(INDX))
            INDXR2 = IL(IU(INDX)+1) - 1
            IF(INDXR1 .GT. INDXR2) GO TO 240
 210        KR = JL(INDXR1)
 220        KC = IU(INDXC1)
            IF(KR .GT. KC) THEN
               INDXC1 = INDXC1 + 1
               IF(INDXC1 .LE. INDXC2) GO TO 220
            ELSE IF(KR .LT. KC) THEN
               INDXR1 = INDXR1 + 1
               IF(INDXR1 .LE. INDXR2) GO TO 210
            ELSE IF(KR .EQ. KC) THEN
               U(INDX) = U(INDX) - L(INDXR1)*DINV(KC)*U(INDXC1)
               INDXR1 = INDXR1 + 1
               INDXC1 = INDXC1 + 1
               IF(INDXR1 .LE. INDXR2 .AND. INDXC1 .LE. INDXC2) GO TO 210
            END IF
 240        U(INDX) = U(INDX)/DINV(IU(INDX))
 250     CONTINUE
!Changing diagonal elements  
 260     INDXR1 = IL(I)
         INDXR2 = IL(I+1) - 1
         IF(INDXR1 .GT. INDXR2) GO TO 300
         INDXC1 = JU(I)
         INDXC2 = JU(I+1) - 1
         IF(INDXC1 .GT. INDXC2) GO TO 300
 270     KR = JL(INDXR1)
 280     KC = IU(INDXC1)
         IF(KR .GT. KC) THEN
            INDXC1 = INDXC1 + 1
            IF(INDXC1 .LE. INDXC2) GO TO 280
         ELSE IF(KR .LT. KC) THEN
            INDXR1 = INDXR1 + 1
            IF(INDXR1 .LE. INDXR2) GO TO 270
         ELSE IF(KR .EQ. KC) THEN
            DINV(I) = DINV(I) - L(INDXR1)*DINV(KC)*U(INDXC1)
            INDXR1 = INDXR1 + 1
            INDXC1 = INDXC1 + 1
            IF(INDXR1 .LE. INDXR2 .AND. INDXC1 .LE. INDXC2) GO TO 270
         END IF
!         
 300  CONTINUE
!         
      DINV(1:N) = 1.0d0/DINV(1:N)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DSILUS
!
!
      RETURN
      END SUBROUTINE DSILUS_SCHW
!
!

  SUBROUTINE DSLUCS_SW_PAR(NI,NT,B,X,NELT,IA,JA,A,TOL,ITMAX,ITER,ERR,&
     &                  IERR,IUNIT,RWORK,LENW,IWORK,LENIW,UPDATE_P,update_l,&
						overlap,MAAS,update_g,global,RE)
!---------------------------------------------------------
!This the parallel version of DSLUCS. Changed on 102616. Author - Aman Rusia
!--------------------------------------------------------
!NL - Number of elements in internal set
!NT - Number of elements in total = internal + update set
!B - Right hand side of the system - processor dependent size (NT)
!X - It is the final solution - processor dependent size (NT). X should be guess solution 
!NELT - size of A, IA and JA. Assign NELT_MAX+1 to them
!IA,JA,A - irn ,icn,co distributed
!    &                  CLOSUR,NMAXIT,ITERU,ERR,IERR,&
!   &                  IUNIT
!RWORK - size LENW.  Work array. Originally LENW = max(1000+mnz+38*MAXNUM_ELEM * NEQ, 9*MaxNum_Conx+1000) where mnz = max size of CO = NEQ*NEQ*(MaxNum_elements+2*MaxNum_conx). New LENW = 1000+mnz+38*MAXNUM_ELEM * NEQ, 9*MaxNum_Conx+1000
!IWORK - size LENIW. jvect. Work array. Originally LENIW = 100+mnz+5*MAXNUM_ELEM*NEQ. IWORK(1:10) stores Upper,Lower triangular matrices and Diagonal inverse of preconditioner, and their column and row information
!new the above sizes are changed according to new MAXNUM_ELEM = MAXNUM_ELEM/size. Note that MaxNum_Conx should remain intact since it could be fully inside a processor.
!RWORK also stores values of elements in L, U and Dinv after preconditioning. IWORK stores their respective row and column indices
!IWORK not needed in non preconditioned dslucs
	 use MPI_VAR
	 use COARSE_M
	 IMPLICIT REAL(KIND = 8)(A-H,O-Z)
	 include 'mpif.h'
!
      INTEGER :: NI, NT, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITMAX, ITER, NI_P,NT_P,NELT_P,MAAS
      INTEGER :: IERR, IUNIT, LENW, IWORK(LENIW), LENIW,RE
      REAL(KIND = 8) :: B(NT), X(NT), A(NELT), TOL, ERR, RWORK(LENW)
    !  EXTERNAL :: DSMV_PAR_MAPDSMV_PAR_MAP, DSLUI_PAR
	  INTEGER :: UPDATE_P(MAAS),update_l(MAAS),overlap
	  INTEGER :: update_g(MAAS),global(NI),REORDER(MAAS),REORDERT(MAAS)
! 
      INTEGER, PARAMETER :: LOCRB=1, LOCIB=11
	  TYPE(HEADEX),allocatable :: head(:),heads(:)
	  TYPE(EXEL),pointer :: point
	  INTEGER, allocatable :: COUNTS(:)
	  DOUBLE PRECISION :: T1,T2,T3,TSCHWZ=0
	  
	  SAVE TSCHWZ
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSLUCS
!
!
	CALLE=CALLE+1

	 call MPI_COMM_RANK ( MPI_COMM_WORLD,rank,IERR)
	 call MPI_COMM_SIZE ( MPI_COMM_WORLD, w_size, IERR)
	 allocate(head(0:w_size-1))
	 allocate(heads(0:w_size-1))
	 allocate(COUNTS(0:w_size-1))
	
      ISYM = 0
      ITOL = 2
!
      IERR = 0
      IF( NI.LT.1 .OR. NELT.LT.1 ) THEN
         IERR = 3 
         RETURN
      ENDIF
	  
      CALL DS2Y( NI, NELT, JA, IA, A, ISYM, IUNIT ) !In parallel version, IA and JA have been swapped. No other change inside
    
!--------212         
 
    
      LOCR   = LOCRB
      LOCR0  = LOCR + NT
      LOCP   = LOCR0 + NT
      LOCQ   = LOCP + NT
      LOCU   = LOCQ + NT
      LOCV1  = LOCU + NT
      LOCV2  = LOCV1 + NT
	  LOCW   = LOCV2 + NT
!
	
      IWORK(10) = LOCW
!
      IF( IERR.NE.0 ) RETURN
!
!
	  call Initialize_Exmap(head,IA,JA,A,update_p,update_l,NELT,NI,NT,COUNTS)

	  T1 = MPI_WTIME()
      call SCHWZ_PRC(NI,NT,NI_P,NELT,A,IA,JA,update_l,update_p,overlap,&
					MAAS,LOCIB,head,HEADS,COUNTS,IWORK,RWORK,IUNIT,LENW,LENIW,&
					update_g,global,REORDER,REORDERT,RE)
	  T2 = MPI_WTIME() 
	  
	  TSCHWZ = TSCHWZ + T2-T1
	 
      CALL DCGS_SW_PAR(NI,NT,NI_P, B, X, NELT, IA, JA, A, ISYM, DSMV_PAR_MAP,&
          DSLUI_PAR_REORDER_COARSE, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT,&
          RWORK(LOCR), RWORK(LOCR0), RWORK(LOCP),&
          RWORK(LOCQ), RWORK(LOCU), RWORK(LOCV1),&
          RWORK(LOCV2), RWORK, IWORK, update_l,UPDATE_P,HEAD,HEADS,overlap,REORDER,REORDERT,RE)
		
	
	  if(rank==0) write(IUNIT,*), "Time to setup SCHWARZ preconditioning",TSCHWZ		
      call Free_Exmap(heads)
	  
	  DO I=1,NI
			IF(ISNAN(X(I))) then		!REMOVE THIS SECTION
				print *, I,CALLE
				stop
			END IF
	  END DO
	  deallocate(head)
	  
	  deallocate(heads)
	  deallocate(COUNTS)
	  if(rank==0) deallocate(CO)
	    
	  
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DSLUCS
!
!
      RETURN
      END SUBROUTINE DSLUCS_SW_PAR
!
    SUBROUTINE DCGS_SW_PAR(NI,NT,NI_P, B, X, NELT, IA, JA, A, ISYM, MATVEC,&
     &     MSOLVE, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, &
     &     R, R0, P, Q, U, V1, V2, RWORK, IWORK,update_l,UPDATE_P,HEAD,HEADS,overlap,REORDER,REORDERT,RE)
!
!!N, B, X, NELT, IA, JA, A, ISYM, DSMV,&           !!!ITERATION BASICALLY STARTS AFTER THIS. Algorithm after here.
 !         DSLUI, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT,&
 !         RWORK(LOCR), RWORK(LOCR0), RWORK(LOCP),&
 !         RWORK(LOCQ), RWORK(LOCU), RWORK(LOCV1),&
  !        RWORK(LOCV2), RWORK, IWORK
!
	use MPI_VAR
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
	  include 'mpif.h'
      INTEGER NI, NT,NI_P, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITMAX
      INTEGER ITER, IERR, IUNIT, IWORK(*),RE
      REAL(KIND = 8) B(NT), X(NT), A(NELT), TOL, ERR, R(NT), R0(NT), P(NT)
      REAL(KIND = 8) Q(NT), U(NT), V1(NT), V2(NT), RWORK(*)
!
	  DOUBLE PRECISION :: Tilu=0,Tmatvat=0,t1,t2,TDOTP=0
      REAL(KIND = 8) DMACH(5)
	  INTEGER ::update_l(*),UPDATE_P(*),overlap,REORDER(*),REORDERT(*)
	  TYPE(HEADEX) :: HEAD(*),HEADS(*)
      DATA DMACH(3) / 1.1101827117665D-16 /
	  SAVE Tilu,Tmatvat,TDOTP
      EXTERNAL MATVEC, MSOLVE
	  
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DCGS
!
!
	
      ITER = 0
      IERR = 0
!
      IF( NI < 1 ) THEN
         IERR = 3
         RETURN
      ENDIF
!
      TOLMIN = 5.0d2*DMACH(3)
      IF( TOL < TOLMIN ) THEN
         TOL  = TOLMIN
         IERR = 4
      ENDIF
!  
 
      T1 =MPI_WTIME()
      CALL MATVEC(NI, NT, X, R, NELT, IA, JA, A, ISYM,UPDATE_P,update_l,head) !DSMV Matvec computes AX -> R. MSOLVE solves the system MR = V1, where V1 is residual AX - R from previous step, M = LDU - the ldu decomposition from preconditioning.
!  

	
      V1(1:NI)  = R(1:NI) - B(1:NI)
!      
      T2 = 	MPI_WTIME()
      CALL MSOLVE(NI,NT,NI_P, V1, R, NELT, IA, JA, A, ISYM, RWORK, IWORK,HEAD,HEADS,overlap,update_l,update_p,REORDER,REORDERT)!DSLUI - Solves for R in LDU.R = B1 Directly parallelizable
!       

	  Tilu = Tilu + MPI_WTIME() - T2
	  Tmatvat = Tmatvat + T2-T1
			
      ISDCGS = 0
      ITOL   = 2
      IF(ITER == 0) THEN
		
		T2 = 	MPI_WTIME()
         CALL MSOLVE(NI,NT,NI_P, B, V2, NELT, IA, JA, A, ISYM, RWORK, IWORK,HEAD,HEADS,overlap,update_l,update_p,REORDER,REORDERT)
	Tilu = Tilu + MPI_WTIME() - T2
		T2 = MPI_WTIME()
		BNRM = SQRT(DOT_PROD_PAR(V2,V2,NI))
		TDOTP = TDOTP+MPI_WTIME()-T2
      ENDIF
!         

		T2 = MPI_WTIME()
      ERR = SQRT(DOT_PROD_PAR(R,R,NI))/BNRM
	  
		TDOTP = TDOTP+MPI_WTIME()-T2
!     
 

	if(rank==0) then
      IF(IUNIT /= 0) THEN
         IF( ITER == 0 ) THEN
            IF(RE==1) THEN
				WRITE(IUNIT,1000) NT, ITOL,overlap
			ELSE
				WRITE(IUNIT,1001) NT, ITOL,overlap
			END IF
         ENDIF
         WRITE(IUNIT,1010) ITER, ERR, AK, BK
      ENDIF
	end if
	
      IF(ERR <= TOL) ISDCGS = 1
!         
      IF(ISDCGS /= 0) GO TO 200
!
!
      FUZZ = DMACH(3)**2
!
      R0(1:NI) = R(1:NI)
!
      RHONM1 = 1.0D0
!         
!         
      DO K=1,ITMAX
         ITER = K
!
		T2 = MPI_WTIME()
         RHON = DOT_PROD_PAR(R0,R,NI)
		 		TDOTP = TDOTP+MPI_WTIME()-T2

!

         IF( ABS(RHONM1) < FUZZ ) GO TO 998
!
         BK = RHON/RHONM1
!

         IF( ITER.EQ.1 ) THEN
            U(1:NI) = R(1:NI)
            P(1:NI) = R(1:NI)
         ELSE
            U(1:NI)  = R(1:NI) + BK*Q(1:NI)
            V1(1:NI) = Q(1:NI) + BK*P(1:NI)
            P(1:NI)  = U(1:NI) + BK*V1(1:NI)
         ENDIF
!         
T1 = MPI_WTIME()
         CALL MATVEC(NI, NT, P, V2, NELT, IA, JA, A, ISYM,UPDATE_P,update_l,head)
		 T2 = MPI_WTIME()
         CALL MSOLVE(NI,NT,NI_P, V2, V1, NELT, IA, JA, A, ISYM, RWORK, IWORK,HEAD,HEADS,overlap,&
					update_l,update_p,REORDER,REORDERT)
!         
 		 
	  Tilu = Tilu + MPI_WTIME() - T2
	  Tmatvat = Tmatvat + T2-T1
		T2 = MPI_WTIME()
         SIGMA = DOT_PROD_PAR(R0,V1,NI)
		TDOTP = TDOTP+MPI_WTIME()-T2


         IF( ABS(SIGMA) < FUZZ ) GO TO 999
!         
         AK = RHON/SIGMA
         AKM = -AK
!         
         Q(1:NI)  = U(1:NI) + AKM*V1(1:NI)
!         
         V1(1:NI) = U(1:NI) + Q(1:NI)
!         
         X(1:NI)  = X(1:NI) + AKM*V1(1:NI)
!           
           T1 = MPI_WTIME()
         CALL MATVEC(NI, NT, V1, V2, NELT, IA, JA, A, ISYM,UPDATE_P,update_l,head)
		 T2 = MPI_WTIME()
         CALL MSOLVE(NI,NT,NI_P, V2, V1, NELT, IA, JA, A, ISYM, RWORK, IWORK,HEAD,HEADS,&
				overlap,update_l,update_p,REORDER,REORDERT)
		
	  Tilu = Tilu + MPI_WTIME() - T2
	  Tmatvat = Tmatvat + T2-T1
	
         R(1:NI) = R(1:NI) + AKM*V1(1:NI)
!         

         ISDCGS = 0
         ITOL   = 2
!         
		T2 = MPI_WTIME()
         ERR = SQRT(DOT_PROD_PAR(R,R,NI))/BNRM
		 		TDOTP = TDOTP+MPI_WTIME()-T2

!        
		 if(rank==0) then
			 IF(IUNIT /= 0) THEN
				WRITE(IUNIT,1010) ITER, ERR, AK, BK
			 ENDIF
		 end if
		 
         IF(ERR <= TOL) ISDCGS = 1
!         
 
         IF(ISDCGS /= 0) GO TO 200
!

         RHONM1 = RHON

	  END DO
 1000 FORMAT('BiConjugate Gradient Squared Preconditioned with Schwarz Method for ',&
          'N, ITOL = , OVERLAP = , This is new reoder',I8, I5,I5&
          /' ITER','   Error Estimate','            Alpha',&
          '             Beta')
 1001 FORMAT('BiConjugate Gradient Squared Preconditioned with Schwarz Method for ',&
          'N, ITOL = , OVERLAP = , without reordering',I8, I5,I5&
          /' ITER','   Error Estimate','            Alpha',&
          '             Beta')
 1010 FORMAT(1X,I4,1X,E16.7,1X,E16.7,1X,E16.7)
!         
      ITER = ITMAX + 1
      IERR = 2
200 	  if(rank==0) write(IUNIT,*),"TIME FOR MATVAT", tmatvat,"Time for ILU",tilu,"TIME for DOTP",TDOTP
		RETURN
!
 998  IERR = 5
	  if(rank==0) write(IUNIT,*),"TIME FOR MATVAT", tmatvat,"Time for ILU",tilu,"TIME for DOTP",TDOTP
      RETURN
!
 999  IERR = 6
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DCGS
!
!
	  if(rank==0) write(IUNIT,*),"TIME FOR MATVAT", tmatvat,"Time for ILU",tilu,"TIME for DOTP",TDOTP
      RETURN
      END SUBROUTINE DCGS_SW_PAR
	  

      SUBROUTINE DSLUI_PAR(NI,NT,NI_P, B, Xt, NELT, IA, JA, A, ISYM, RWORK, IWORK,HEAD,HEADS,overlap,update_l,update_p)
!
! !DSLUI returns in R the solution of system LDU.R = V1 where LDU = M
!N, V1, R, NELT, IA, JA, A, ISYM, RWORK, IWORK
!Parallel DSLUI is compatible with SLAP column format of L in LDU
!This subroutine is directly parallelizable
	  use MPI_VAR
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      INTEGER NI,NT,NI_P, NELT, IA(NELT), JA(NELT), ISYM, IWORK(*),overlap,update_p(*),update_l(*)
      REAL(KIND = 8) B(NI), Xt(NI), A(NELT), RWORK(*), X(NI_P)
	  TYPE(HEADEX) :: HEAD(0:w_size-1),HEADS(0:w_size-1)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSLUI
!
!

      LOCIL  = IWORK(1)
      LOCJL  = IWORK(2)
      LOCIU  = IWORK(3)
      LOCJU  = IWORK(4)
      LOCL   = IWORK(5)
      LOCDIN = IWORK(6)
      LOCU   = IWORK(7)
!        
 
      X(1:NI) = B(1:NI)  ! CAREFUL! Whole array operations
	  
	  if(overlap/=0) call update_map(X,NI,NI_P,update_p,update_l,heads)
! 
 
      DO 30 IROW = 2, NI_P
         JBGN = IWORK(LOCIL+IROW-1)
         JEND = IWORK(LOCIL+IROW)-1
         IF( JBGN.LE.JEND ) THEN
            DO 20 J = JBGN, JEND
               X(IROW) = X(IROW)-RWORK(LOCL+J-1)*X(IWORK(LOCJL+J-1))
 20         CONTINUE
         ENDIF
 30   CONTINUE
   
      X(1:NI_P) = X(1:NI_P)*RWORK(LOCDIN:LOCDIN+NI_P-1)  ! CAREFUL! Whole array operations
!         
      DO 60 ICOL = NI_P, 2, -1
         JBGN = IWORK(LOCJU+ICOL-1)
         JEND = IWORK(LOCJU+ICOL)-1
         IF( JBGN.LE.JEND ) THEN
!          X(IWORK(LOCIU-1+JBGN:LOCIU-1+JEND)) 
!     &  = X(IWORK(LOCIU-1+JBGN:LOCIU-1+JEND))
!     &   -RWORK(LOCU-1+JBGN:LOCU-1+JEND)*X(ICOL)
            DO 50 J = JBGN, JEND
               JJUU    = IWORK(LOCIU+J-1)
               X(JJUU) = X(JJUU)-RWORK(LOCU+J-1)*X(ICOL) !Note: U is already divided by the diagonal of the same row.
 50         CONTINUE
         ENDIF
 60   CONTINUE
 
	  
	Xt(1:NI) = X(1:NI)
	 
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DSLUI
!
!
      RETURN
      END SUBROUTINE DSLUI_PAR
	 

      SUBROUTINE DSLUI_PAR_REORDER_COARSE(NI,NT,NI_P, B, Xt, NELT, IA, JA, A, ISYM, RWORK, IWORK,HEAD,HEADS,overlap,&
						update_l,update_p,REORDER,REORDERT)
!
! !DSLUI returns in R the solution of system LDU.R = V1 where LDU = M
!N, V1, R, NELT, IA, JA, A, ISYM, RWORK, IWORK
!Parallel DSLUI is compatible with SLAP column format of L in LDU
!This subroutine is directly parallelizable
	  use MPI_VAR
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      INTEGER NI,NT,NI_P, NELT, IA(NELT), JA(NELT), ISYM, IWORK(*),overlap,update_p(*),update_l(*),REORDER(*),REORDERT(*)
      REAL(KIND = 8) B(NI), Xt(NI), A(NELT), RWORK(*), X(NI_P+NT-NI),SWAP(NI_P),X_CO(NT)		!SWAP NEEDED FOR REORDER
	  TYPE(HEADEX) :: HEAD(0:w_size-1),HEADS(0:w_size-1)
	   REAL(KIND = 8) :: CO_B,CO_X(0:w_size-1)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSLUI
!
!

      LOCIL  = IWORK(1)
      LOCJL  = IWORK(2)
      LOCIU  = IWORK(3)
      LOCJU  = IWORK(4)
      LOCL   = IWORK(5)
      LOCDIN = IWORK(6)
      LOCU   = IWORK(7)
!        
	  X = 0.0
      X(1:NI) = B(1:NI)  ! CAREFUL! Whole array operations

	if(w_size>1) then
		!Applying coarse correction
		CO_B = sum(B(1:NI))
		call coarse_correct(CO_B,CO_X(0:w_size-1))
		!Find the new solution
		X_CO(1:NI) = CO_X(rank)
		X_CO(NI+1:NT) = CO_X(update_p(1:NT-NI))
		!Find the residual with new solution
		do i=1,NI
			do j=IA(i),IA(i+1)-1
				X(i) = X(i)-A(j)*X_CO(JA(j))
			end do
		end do
	end if
	
	
	  !Communicate the new residual at the interface
	  if(overlap/=0) call update_map(X,NI,NI_P,update_p,update_l,heads)
! 
	  !-----REORDERING new X---------
	  SWAP(1:NI_P) = X(REORDER(1:NI_P))
	  X(1:NI_P) = SWAP(1:NI_P)

	  !-------------------------------
      DO 30 IROW = 2, NI_P
         JBGN = IWORK(LOCIL+IROW-1)
         JEND = IWORK(LOCIL+IROW)-1
         IF( JBGN.LE.JEND ) THEN
            DO 20 J = JBGN, JEND
               X(IROW) = X(IROW)-RWORK(LOCL+J-1)*X(IWORK(LOCJL+J-1))
 20         CONTINUE
         ENDIF
 30   CONTINUE
   
      X(1:NI_P) = X(1:NI_P)*RWORK(LOCDIN:LOCDIN+NI_P-1)  ! CAREFUL! Whole array operations
!         
      DO 60 ICOL = NI_P, 2, -1
         JBGN = IWORK(LOCJU+ICOL-1)
         JEND = IWORK(LOCJU+ICOL)-1
         IF( JBGN.LE.JEND ) THEN
!          X(IWORK(LOCIU-1+JBGN:LOCIU-1+JEND)) 
!     &  = X(IWORK(LOCIU-1+JBGN:LOCIU-1+JEND))
!     &   -RWORK(LOCU-1+JBGN:LOCU-1+JEND)*X(ICOL)
            DO 50 J = JBGN, JEND
               JJUU    = IWORK(LOCIU+J-1)
               X(JJUU) = X(JJUU)-RWORK(LOCU+J-1)*X(ICOL) !Note: U is already divided by the diagonal of the same row.
 50         CONTINUE
         ENDIF
 60   CONTINUE
 
	  !-----REORDERING transpose solution (back to original)---------
	  SWAP(1:NI_P) = X(REORDERT(1:NI_P))
	  X(1:NI_P) = SWAP(1:NI_P)
		
	!Copying the solution
	Xt(1:NI) =  X(1:NI)
	
	if(w_size==1) return
	
	
	!Multiplying by Q
	SWAP=0.0
	do i=1,NI
		do j=IA(i),IA(i+1)-1
			SWAP(i) = SWAP(i)-A(j)*X(JA(j))
		end do
	end do
	CO_B = sum(SWAP(1:NI))
	call coarse_correct(CO_B,CO_X(0:w_size-1))
	Xt(1:NI) =  Xt(1:NI) - CO_X(rank)

	!Final Solution
	Xt(1:NI) = X_CO(1:NI) + Xt(1:NI)
	 
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DSLUI
!
!
      RETURN
      END SUBROUTINE DSLUI_PAR_REORDER_COARSE
	  
	  
      SUBROUTINE DSLUI_PAR_REORDER(NI,NI_P, B, Xt, NELT, IA, JA, A, ISYM, RWORK, IWORK,HEAD,HEADS,overlap,&
						update_l,update_p,REORDER,REORDERT)
!
! !DSLUI returns in R the solution of system LDU.R = V1 where LDU = M
!N, V1, R, NELT, IA, JA, A, ISYM, RWORK, IWORK
!Parallel DSLUI is compatible with SLAP column format of L in LDU
!This subroutine is directly parallelizable
	  use MPI_VAR
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      INTEGER NI,NI_P, NELT, IA(NELT), JA(NELT), ISYM, IWORK(*),overlap,update_p(*),update_l(*),REORDER(*),REORDERT(*)
      REAL(KIND = 8) B(NI), Xt(NI), A(NELT), RWORK(*), X(NI_P),SWAP(NI_P)		!SWAP NEEDED FOR REORDER
	  TYPE(HEADEX) :: HEAD(0:w_size-1),HEADS(0:w_size-1)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSLUI
!
!

      LOCIL  = IWORK(1)
      LOCJL  = IWORK(2)
      LOCIU  = IWORK(3)
      LOCJU  = IWORK(4)
      LOCL   = IWORK(5)
      LOCDIN = IWORK(6)
      LOCU   = IWORK(7)
!        
      X(1:NI) = B(1:NI)  ! CAREFUL! Whole array operations
	  
	  if(overlap/=0) call update_map(X,NI,NI_P,update_p,update_l,heads)
! 
	  !-----REORDERING new X---------
	  SWAP(1:NI_P) = X(REORDER(1:NI_P))
	  X(1:NI_P) = SWAP(1:NI_P)

	  !-------------------------------
      DO 30 IROW = 2, NI_P
         JBGN = IWORK(LOCIL+IROW-1)
         JEND = IWORK(LOCIL+IROW)-1
         IF( JBGN.LE.JEND ) THEN
            DO 20 J = JBGN, JEND
               X(IROW) = X(IROW)-RWORK(LOCL+J-1)*X(IWORK(LOCJL+J-1))
 20         CONTINUE
         ENDIF
 30   CONTINUE
   
      X(1:NI_P) = X(1:NI_P)*RWORK(LOCDIN:LOCDIN+NI_P-1)  ! CAREFUL! Whole array operations
!         
      DO 60 ICOL = NI_P, 2, -1
         JBGN = IWORK(LOCJU+ICOL-1)
         JEND = IWORK(LOCJU+ICOL)-1
         IF( JBGN.LE.JEND ) THEN
!          X(IWORK(LOCIU-1+JBGN:LOCIU-1+JEND)) 
!     &  = X(IWORK(LOCIU-1+JBGN:LOCIU-1+JEND))
!     &   -RWORK(LOCU-1+JBGN:LOCU-1+JEND)*X(ICOL)
            DO 50 J = JBGN, JEND
               JJUU    = IWORK(LOCIU+J-1)
               X(JJUU) = X(JJUU)-RWORK(LOCU+J-1)*X(ICOL) !Note: U is already divided by the diagonal of the same row.
 50         CONTINUE
         ENDIF
 60   CONTINUE
 
	  !-----REORDERING transpose solution (back to original)---------
	  SWAP(1:NI_P) = X(REORDERT(1:NI_P))
	  X(1:NI_P) = SWAP(1:NI_P)
		
	!Copying the solution
	Xt(1:NI) = X(1:NI)
	 
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DSLUI
!
!
      RETURN
      END SUBROUTINE DSLUI_PAR_REORDER
	 

	subroutine COARSE_LU(CO_ROW)
	use MPI_VAR
	use MPI
	USE COARSE_M
	implicit none
	real (kind=8) :: CO_ROW(1:w_size),CO_T(w_size,w_size)
	integer :: ierr,i,request(2*w_size),j
	integer, dimension (MPI_STATUS_SIZE,2*w_size) :: stats

	!communicate and receive in process 1, all the rows 
	if (rank==0) THEN
		allocate(CO(w_size,w_size))
		CO=0.0
		do i=1,w_size-1
			call MPI_IRECV(CO(1:w_size,i+1),w_size,&
				MPI_REAL8,i,1,MPI_COMM_WORLD,request(i), ierr)
		end do
		
		CO(1:w_size,1) = CO_ROW(1:w_size)
		
		call MPI_Waitall((w_size-1),request,stats,ierr)
		
		!Transpose
		CO_T = CO(1:w_size,1:w_size)
		do i=1,w_size
			CO(i,1:w_size) = CO_T(1:w_size,i)
		end do
		
		!Find the LU decomposition
		do i=2,w_size
			do j=1,i-1
				!Find the factorization
				CO(i,j) = CO(i,j)/CO(j,j)
				!Subtract everywhere
				CO(i,j+1:w_size) = CO(i,j+1:w_size) - CO(i,j)*CO(j,j+1:w_size)
			end do
		end do
	else
	
		call MPI_ISEND(CO_ROW(1:w_size),w_size,&
			MPI_REAL8,0,1,MPI_COMM_WORLD,request(1), ierr)
		
		call MPI_Waitall(1,request,stats,ierr)
		
	end if
	
	call mPI_BARRIER(MPI_COMM_WORLD,IERR)
	end subroutine COARSE_LU
	 
	subroutine coarse_correct(CO_B,CO_X)
		use MPI_VAR
		use MPI
		USE COARSE_M
		implicit none
		integer :: ierr,i,request(2*w_size),j
		REAL(Kind=8) ::CO_B,CO_X(1:w_size),B(w_size)
		
	    call MPI_GATHER(CO_B,1,MPI_REAL8,B,1,MPI_REAL8,0,MPI_COMM_WORLD,IERR)
		!communicate and receive in process 1 RHS
		if (rank==0) THEN
			!do i=1,w_size-1
		!		call MPI_IRECV(B(i+1),1,&
		!			MPI_REAL8,i,1,MPI_COMM_WORLD,request(i), ierr)
		!	end do
		!	
		!	B(1) = CO_B
		!	
		!	call MPI_Waitall((w_size-1),request,MPI_STATUSES_IGNORE,ierr)
			
			!Find the solution
			!Find L inverse
			do i=2,w_size
				do j=1,i-1 
					B(i) = B(i)-CO(i,j)*B(j)
				end do
			end do
			!Find U inverse
			do i=w_size,1,-1
				do j=i+1,w_size 
					B(i) = B(i)-CO(i,j)*B(j)
				end do
				B(i) = B(i)/CO(i,i)
			end do
			!send the solution
			!do i=1,w_size-1
			!	call MPI_ISEND(B(i+1),1,&
			!		MPI_REAL8,i,2,MPI_COMM_WORLD,request(i), ierr)
			!end do
			
			CO_X(1:w_size) = B(1:w_size)
			
			!call MPI_Waitall((w_size-1),request,MPI_STATUSES_IGNORE,ierr) 
		else
			!call MPI_ISEND(CO_B,1,&
			!	MPI_REAL8,0,1,MPI_COMM_WORLD,request(1), ierr)
			
			!call MPI_IRECV(CO_X,1,&
			!	MPI_REAL8,0,2,MPI_COMM_WORLD,request(2), ierr)
				
		!	call MPI_Waitall(1,request,MPI_STATUSES_IGNORE,ierr)
			
		end if
	
	call MPI_BCAST(CO_X,w_size,MPI_REAL8,0,MPI_COMM_WORLD,IERR)
	end subroutine coarse_correct
	
	
	
	  SUBROUTINE DSMV_PAR_MAP( NI, NT, X, Y, NELT, IA, JA, A, ISYM, UPDATE_P,update_l,head)
!DSMV = MATVEC
!Parallel version of the Matrix Vector Product happens after update
!
	use MPI_VAR
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      INTEGER :: NI,NT, NELT, IA(NELT), JA(NELT), ISYM
	  INTEGER:: update_p(*),update_l(*)
      REAL(KIND = 8) :: A(NELT), X(NT), Y(NT)
	  TYPE(HEADEX) :: head(0:w_size-1)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSMV
!
	!print *, "HE"
	call update_map(X,NI,NT,update_p,update_l,head)

      Y(1:NT) = 0.0d0     ! CAREFUL! Whole array operations
!
      DO irow = 1, NI
         do j = IA(irow),IA(irow+1)-1
	 ! if(rank==0 .And. irow==49) print *, Y(irow),A(j),X(JA(j)),JA(j)
			Y(irow) = Y(irow)+A(j)*X(JA(j))
		 end do
      END DO
	       ! if(calle==2) then
  ! print *, A(1:NELT), rank
 ! stop
 ! end if
  
	 ! if(rank==0) print *, "_-_________"
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DSMV
!
!
	
      RETURN
      END SUBROUTINE DSMV_PAR_MAP

	  

!
  SUBROUTINE DSLUCS_SW_PAR_HYB(NI,NT,B,X,NELT,IA,JA,A,TOL,ITMAX,ITER,ERR,&
     &                  IERR,IUNIT,RWORK,LENW,IWORK,LENIW,UPDATE_P,update_l,&
						overlap,MAAS,N_SUB,overlap_loc)
!---------------------------------------------------------
!This the parallel version of DSLUCS. Changed on 102616. Author - Aman Rusia
!--------------------------------------------------------
!NL - Number of elements in internal set
!NT - Number of elements in total = internal + update set
!B - Right hand side of the system - processor dependent size (NT)
!X - It is the final solution - processor dependent size (NT). X should be guess solution 
!NELT - size of A, IA and JA. Assign NELT_MAX+1 to them
!IA,JA,A - irn ,icn,co distributed
!    &                  CLOSUR,NMAXIT,ITERU,ERR,IERR,&
!   &                  IUNIT
!RWORK - size LENW.  Work array. Originally LENW = max(1000+mnz+38*MAXNUM_ELEM * NEQ, 9*MaxNum_Conx+1000) where mnz = max size of CO = NEQ*NEQ*(MaxNum_elements+2*MaxNum_conx). New LENW = 1000+mnz+38*MAXNUM_ELEM * NEQ, 9*MaxNum_Conx+1000
!IWORK - size LENIW. jvect. Work array. Originally LENIW = 100+mnz+5*MAXNUM_ELEM*NEQ. IWORK(1:10) stores Upper,Lower triangular matrices and Diagonal inverse of preconditioner, and their column and row information
!new the above sizes are changed according to new MAXNUM_ELEM = MAXNUM_ELEM/size. Note that MaxNum_Conx should remain intact since it could be fully inside a processor.
!RWORK also stores values of elements in L, U and Dinv after preconditioning. IWORK stores their respective row and column indices
!IWORK not needed in non preconditioned dslucs
	 use MPI_VAR
	 use part_var
	 IMPLICIT REAL(KIND = 8)(A-H,O-Z)
	 include 'mpif.h'
!
      INTEGER :: NI, NT, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITMAX, ITER, NI_P,NT_P,NELT_P,MAAS
      INTEGER :: IERR, IUNIT, LENW, IWORK(LENIW), LENIW
      REAL(KIND = 8) :: B(NT), X(NT), A(NELT), TOL, ERR, RWORK(LENW)
    !  EXTERNAL :: DSMV_PAR_MAP, DSLUI_PAR
	  INTEGER :: UPDATE_P(MAAS),update_l(MAAS),overlap,overlap_loc,N_SUB
	  INTEGER :: NI_PL(N_SUB)
!
      INTEGER, PARAMETER :: LOCRB=1, LOCIB=11
	  TYPE(HEADEX),allocatable :: head(:),heads(:)
	  TYPE(EXEL),pointer :: point
	  INTEGER, allocatable :: COUNTS(:)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSLUCS
!
!
	CALLE=CALLE+1
	
	 call MPI_COMM_RANK ( MPI_COMM_WORLD,rank,IERR)
	 call MPI_COMM_SIZE ( MPI_COMM_WORLD, w_size, IERR)
	 allocate(head(0:w_size-1))
	 allocate(heads(0:w_size-1))
	 allocate(COUNTS(0:w_size-1))
	 
	
	 !Find the partition of the domain
		if(CALLE==1) THEN
			allocate(part_list(NI_P))
		end if
		
      ISYM = 0
      ITOL = 2
!
      IERR = 0
      IF( NI.LT.1 .OR. NELT.LT.1 ) THEN
         IERR = 3
         RETURN
      ENDIF
      CALL DS2Y( NI, NELT, JA, IA, A, ISYM, IUNIT ) !In parallel version, IA and JA have been swapped. No other change inside
    
!--------212         
 
    
      LOCR   = LOCRB
      LOCR0  = LOCR + NT
      LOCP   = LOCR0 + NT
      LOCQ   = LOCP + NT
      LOCU   = LOCQ + NT
      LOCV1  = LOCU + NT
      LOCV2  = LOCV1 + NT
	  LOCW   = LOCV2 + NT
!
	
      IWORK(10) = LOCW
!
      IF( IERR.NE.0 ) RETURN
!
!
	  call Initialize_Exmap(head,IA,JA,A,update_p,update_l,NELT,NI,NT,COUNTS)

	
      call SCHWZ_PRC_HYB(NI,NT,NI_P,NELT,A,IA,JA,update_l,update_p,overlap,&
					MAAS,LOCIB,head,HEADS,COUNTS,IWORK,RWORK,IUNIT,LENW,LENIW,&
					& N_SUB,overlap_loc,NI_PL)
		 
      CALL DCGS_SW_PAR_HYB(NI,NT,NI_P, B, X, NELT, IA, JA, A, ISYM, DSMV_PAR_MAP,&
          DSLUI_PAR_HYB, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT,&
          RWORK(LOCR), RWORK(LOCR0), RWORK(LOCP),&
          RWORK(LOCQ), RWORK(LOCU), RWORK(LOCV1),&
          RWORK(LOCV2), RWORK, IWORK, update_l,UPDATE_P,HEAD,HEADS,overlap&
			&,N_SUB,NI_PL,overlap_loc)
	  
	  deallocate(ERRAY)
      call Free_Exmap(heads)
	  
	  deallocate(head)
	  
	  deallocate(heads)
	  deallocate(COUNTS)
	  
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DSLUCS
!
!
      RETURN
      END SUBROUTINE DSLUCS_SW_PAR_HYB
!
   

      SUBROUTINE DSLUI_PAR_HYB(NI,NI_P, B, Xt, NELT, IA, JA, A, ISYM, RWORK, IWORK,HEAD,HEADS,overlap,update_l,update_p,&
								&N_SUB,NI_PL)
!
! !DSLUI returns in R the solution of system LDU.R = V1 where LDU = M
!N, V1, R, NELT, IA, JA, A, ISYM, RWORK, IWORK
!Parallel DSLUI is compatible with SLAP column format of L in LDU
!This subroutine is directly parallelizable
	  use MPI_VAR
	  use part_var
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      INTEGER  :: NI,NI_P, NELT, IA(NELT), JA(NELT), ISYM, IWORK(*),overlap,update_p(*),update_l(*)
      REAL(KIND = 8) :: B(NI), Xt(NI), A(NELT), RWORK(*), X(NI_P,N_SUB)
	  TYPE(HEADEX) :: HEAD(0:w_size-1),HEADS(0:w_size-1)
	  INTEGER,intent(in):: N_SUB,NI_PL(*)
	  INTEGER :: TEMP(N_SUB),NELT_P,NU,NL
	  INTEGER :: ICALL=0
	  SAVE ICALL
	  
	  ICALL=ICALL+1
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSLUI
!
!
      Xt(1:NI) = B(1:NI)  
	  
	  if(overlap/=0) call update_map(Xt,NI,NI_P,update_p,update_l,heads)
	  
	  DO I=1,N_SUB
		DO J=1,NI_PL(I)	
			X(J,I) = Xt(ERRAY(J,I))
		END DO
	  END DO
	 
	  
! 
	  NU = (IWORK(3)-IWORK(2))/N_SUB
	  NL = (IWORK(4)-IWORK(3))/N_SUB
	  NELT_P = (IWORK(6)-IWORK(5))/N_SUB
! 
	  DO I=1,N_SUB
		  LOCIL  = IWORK(1)+(I-1)*(NI_P+1)
		  LOCJL  = IWORK(2)+(I-1)*NU
		  LOCIU  = IWORK(3)+(I-1)*NL
		  LOCJU  = IWORK(4)+(I-1)*(NI_P+1)
		  LOCL   = IWORK(5)+(I-1)*NELT_P
		  LOCDIN = IWORK(6)+(I-1)*NI_P
		  LOCU   = IWORK(7)+(I-1)*NELT_P
		 
		  DO IROW = 2, NI_PL(I)
			 JBGN = IWORK(LOCIL+IROW-1)
			 JEND = IWORK(LOCIL+IROW)-1
			 DO J = JBGN, JEND
				X(IROW,I) = X(IROW,I)-RWORK(LOCL+J-1)*X(IWORK(LOCJL+J-1),I)
			 END DO
		  END DO
	 
		  X(1:NI_PL(I),I) = X(1:NI_PL(I),I)*RWORK(LOCDIN:LOCDIN+NI_PL(I)-1)  ! CAREFUL! Whole array operations
	!         
	
		  DO ICOL = NI_PL(I), 2, -1
			 JBGN = IWORK(LOCJU+ICOL-1)
			 JEND = IWORK(LOCJU+ICOL)-1
			 IF( JBGN.LE.JEND ) THEN
				DO J = JBGN, JEND
				   JJUU    = IWORK(LOCIU+J-1)
				   X(JJUU,I) = X(JJUU,I)-RWORK(LOCU+J-1)*X(ICOL,I) !Note: U is already divided by the diagonal of the same row.
				END DO
			 ENDIF
		   END DO
		   
	   END DO
	TEMP(1:N_SUB)=0
	
	DO I=1,NI
		TEMP(PART_LIST(I))=TEMP(PART_LIST(I))+1
		Xt(I) = X(TEMP(part_list(I)),part_list(I))
	END DO
	
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DSLUI
!
!
      RETURN
END SUBROUTINE DSLUI_PAR_HYB
	  
	  subroutine SCHWZ_PRC_HYB(NI,NT,NI_P,NELT,A,IA,JA,update_l,update_p,overlap,MAAS,LOCIB,&
					& head,heads,COUNTS,IWORK,RWORK,IUNIT,LENW,LENIW,&
					& N_SUB,overlap_loc,NI_PL)
	!CHECK SIZE OF REQUEST AND STATS
	use mpi_var
	use part_var
	implicit none
	include 'mpif.h'
	
	integer,intent(in) :: NI,NT,NELT,MAAS,IUNIT
	integer, intent(in) :: overlap !Overlap needed for schwarz preconditioner
	integer, intent(in) :: IA(*),JA(*),LOCIB,LENW,LENIW
	real(kind=8),intent(in) :: A(*)
	real(kind=8) :: AA(MAAS),RWORK(*)
	integer :: IAA(MAAS),JAA(MAAS),update_l(*),update_p(*),IWORK(*)
	integer,dimension(0:w_size-1) :: COUNTS,COUNTR,COUNTRE,COUNTE
	integer :: ni_p, nt_p, NELT_P
	type(BUFFEL),dimension(0:w_size-1) :: SBUFF, EXSBUFF,EXRBUFF     !Send buffer
	type(HeadMAPEL) :: NMAP(0:w_size-1)
	type(MAPEL), pointer :: curr_mapel
	integer :: i,ii,j,k,l,ierr,ucount,gcount,loc,proc,endloc,NT_PC
	integer:: temp(0:w_size-1),flag(MAAS)
	integer:: updaten_l(MAAS),updaten_p(MAAS)
	integer, dimension (MPI_STATUS_SIZE,8*w_size) :: stats
	integer, dimension(8*w_size) :: request
	type(HEADEX) :: head(0:w_size-1),heads(0:w_size-1)
	type(HDEXT) :: headext(0:w_size-1)
	type(EXEL),pointer:: tpoint,point,point2
	TYPE(EXT),pointer :: pointx
	integer :: NL,NU,LOCIL,LOCJL,LOCIU,LOCJU,LOCL,LOCDIN,LOCUU,LOCIW,LOCNC,LOCNR,LOCW
	
	real(kind=8) :: TempA
	integer :: TempJA,TNELT
	
	integer,intent(in) :: N_SUB,overlap_loc
	real(kind=8) :: AAA(NELT)
	integer :: IAAA(NELT+1),JAAA(NELT)
	integer :: NI_PL(1:N_SUB),TNI_PL,TNI_PL_O,NL_L,NU_L,NELT_PL
	integer,allocatable :: LNSUB(:),TPART(:),TLNSUB(:)
	INTEGER :: ICALL=0
	SAVE ICALL
	
	ICALL = ICALL+1
	do i=1,IA(NI+1) !loop till NELT+1
		AA(i) = A(i)
		IAA(i) = IA(i)
		JAA(i) = JA(i)
	end do
	nt_p = nt
	ni_p = ni
	NELT_P = NELT
	updaten_p(1:nt_p-ni_p) = update_p(1:nt_p-ni_p)
	updaten_l(1:nt_p-ni_p) = update_l(1:nt_p-ni_p)
	gcount = nt_p-ni_p

	do i=0,w_size-1
		NULLIFY(NMAP(i)%next)
		NULLIFY(NMAP(i)%ende)
	end do
	COUNTE=0
	!Forming map for current update boundary elements against their process number and storing their local numbers
		do i=1,nt_p-ni_p
			call add_mapel(ni_p+i,updaten_p(i),updaten_l(i),NMAP)
		end do
	flag=0
	
	!Iterating for each next overlap
	do ii=1,overlap
	
		!Forming map for new external boundary 
			do i=0,w_size-1
				NULLIFY(headext(i)%next)
				NULLIFY(headext(i)%ende)	
			end do
			if(ii>1) then
				COUNTS=0
				COUNTE=0
				do i=0,w_size-1
				
					point=>HEAD(i)%curr
					tpoint=>HEAD(i)%ende
					if(associated(tpoint)) endloc=tpoint%loc
					do while(associated(point))
						
						loc=point%loc
						!if(rank==1 .and. i==0) print *,loc,JAA(IAA(loc)),JAA(IAA(loc+1)-1)
						do j=IAA(loc)+1,IAA(loc+1)-1
							if(JAA(j)<=NI) then 
								point2=>head(i)%next
								do while(associated(point2))
									if(point2%loc==JAA(j)) exit
									point2=>point2%next
								end do
								if(associated(point2)) cycle
								allocate(head(i)%ende%next)
								point2=>head(i)%ende%next
								point2%loc=JAA(j)
								NULLIFY(point2%next)
								head(i)%ende=>point2
								COUNTS(i)=COUNTS(i)+IAA(JAA(j)+1)-IAA(JAA(j))
							else
							
								if(update_p(JAA(j)-NI)==i) cycle
								pointx=>headext(update_p(JAA(j)-NI))%next
								if(associated(pointx)) then
									do while(associated(pointx))
										if(pointx%loc==update_l(JAA(j)-NI)) exit
										pointx=>pointx%next
									end do
									if(associated(pointx)) cycle
									allocate(headext(update_p(JAA(j)-NI))%ende%next)
									pointx=>headext(update_p(JAA(j)-NI))%ende%next
								else
									allocate(headext(update_p(JAA(j)-NI))%next)
									pointx=>headext(update_p(JAA(j)-NI))%next
									headext(update_p(JAA(j)-NI))%ende=>pointx
								end if
								pointx%loc=update_l(JAA(j)-NI)
								pointx%update_p=i
								NULLIFY(pointx%next)
								headext(update_p(JAA(j)-NI))%ende=>pointx
								COUNTE(i)=COUNTE(i)+1
							end if
							
						end do
						if(point%loc==endloc) exit
						point=>point%next
						
					end do
						if(associated(tpoint)) HEAD(i)%curr=>tpoint%next
				end do
				
			end if
			
		!Counting size of the chunk to be sent to neighboring processes
			! COUNTS = 0
			! do i=1,NI_P
				! temp = 0
				! do j=IAA(i+1)-1,IAA(i),-1
					! if(JAA(j)<=NI_P ) exit
					! if(temp(updaten_p(JAA(j)-NI_P)) == 1) cycle
					! COUNTS(updaten_p(JAA(j)-NI_P)) = COUNTS(updaten_p(JAA(j)-NI_P)) + IAA(i+1) - IAA(i)
					! temp(updaten_p(JAA(j)-NI_P)) = 1
				! end do
			!end do
			! if(ii>1) then
				! do i=0:w_size-1
					! point=>curr(i)
					! do while(associated(point))
						! point=>point%next
					! end do
				! end do
			! end if
		!Sending and receiving the count
	

			
			l=1
			if(ii>1) then
				do i=0,w_size-1
					if(counte(i)/=0) then
					
						allocate(EXSBUFF(i)%IA(2*counte(i)))
						pointx=>HEADEXT(i)%next
						counte(i)=0
						
						do while(associated(pointx))
							EXSBUFF(i)%IA(counte(i)+1) = pointx%loc
							EXSBUFF(i)%IA(counte(i)+2) = pointx%update_p
							counte(i)=counte(i)+2
							pointx=>pointx%next
						end do
						
						counte(i)=counte(i)/2
						call MPI_ISEND(EXSBUFF(i)%IA(1:2*counte(i)),2*counte(i),&
						MPI_INTEGER,i,1,MPI_COMM_WORLD,request(l), ierr)
						l=l+1
						
					end if
				end do
				
				do i=0,w_size-1
					if(countre(i)/=0) then
						allocate(EXRBUFF(i)%IA(2*countre(i)))	
						call MPI_IRECV(EXRBUFF(i)%IA(1:2*countre(i)),2*countre(i),&
							MPI_INTEGER,i,1,MPI_COMM_WORLD,request(l), ierr )
						l=l+1
					end if
				end do
				if(l>1) call MPI_Waitall(l-1,request,stats,ierr)
			
			
				do i=0,w_size-1
					if(COUNTRE(i)==0) cycle
					do j=1,COUNTRE(i)
						loc=EXRBUFF(i)%IA(2*j-1)
						proc=EXRBUFF(i)%IA(2*j)
						point2=>head(proc)%next
						if(associated(point2)) then
							do while(associated(point2))
								if(point2%loc==loc) exit
								point2=>point2%next
							end do
							if(associated(point2)) cycle
							allocate(head(proc)%ende%next)
							point2=>head(proc)%ende%next
						else
							allocate(head(proc)%ende)
							head(proc)%next=>head(proc)%ende
							head(proc)%curr=>head(proc)%ende
							point2=>head(proc)%ende
						end if
						point2%loc=loc
						NULLIFY(point2%next)
						head(proc)%ende=>point2
						COUNTS(proc)=COUNTS(proc)+IAA(loc+1)-IAA(loc)
					end do
				end do
				
			end if
			COUNTR=0
			l=1
			do i=0,w_size-1
			
				if(COUNTS(i)==0) cycle
				
				call MPI_ISEND(counts(i),1,&
				MPI_INTEGER,i,0,MPI_COMM_WORLD,request(l), ierr)
				l=l+1
				
				call MPI_IRECV(countr(i),1,&
					MPI_INTEGER,i,0,MPI_COMM_WORLD,request(l), ierr )
				l=l+1
				
			end do
			if(l>1) call MPI_Waitall(l-1,request,stats,ierr)
			
		! if(rank==0 .and. ii==2) then
		! do i = 0, w_size-1
			! point=>HEAD(i)%next
			! do while(associated(point))
			 ! print *, "SENDING",point%loc, "BY RANK", rank, "TO", i
				! point=>point%next
			! end do
			
		! end do
		! print *,"@@@@----------------"
		
		! do i=1,NT_P-NI
			! print *, "RECEVING",update_l(i),"BY",rank,"FROM",update_p(i)
		! end do
	! stop
	! end if
		!Check if enough memory for receiving is present
			TNELT = NELT_P
			do i=0,w_size-1
				TNELT = TNELT+countr(i)
			end do
			if(TNELT>MAAS) then
				write(*,*), "AT PROCESS",rank,"NOT ENOUGH MEMORY TO &
				RECEIVE IN SCHWZ_PRC, AT OVERLAP ITERATION", ii,"AVAILABLE",MAAS,"NEEDED",TNELT
				stop
				return
			end if
	
		!Allocate then loop to form A, IA, JA, and update_p to be sent to neighboring process
			
			
		do i=0,w_size-1
				if(COUNTS(i)==0) cycle
				allocate(SBUFF(i)%A(counts(i)))
				allocate(SBUFF(i)%IA(counts(i)))
				allocate(SBUFF(i)%JA(counts(i)))
				allocate(SBUFF(i)%update_p(counts(i)))
			end do
			COUNTS = 0
			
			do i=0,w_size-1
				point=>head(i)%curr
				do while(associated(point))
					do k=IA(point%loc),IA(point%loc+1)-1,1
						SBUFF(i)%A(counts(i)+1) = A(k)
						if(point%loc<=NI) then
							SBUFF(i)%IA(counts(i)+1) = point%loc
						else
							SBUFF(i)%IA(counts(i)+1) = update_l(point%loc-NI)
						end if
						
						if(JA(k)<=NI) then		
							SBUFF(i)%JA(counts(i)+1) = JA(k)
							SBUFF(i)%update_p(counts(i)+1) = rank
						else
							SBUFF(i)%JA(counts(i)+1) = update_l(JA(k)-NI)
							SBUFF(i)%update_p(counts(i)+1) = update_p(JA(k)-NI)
						end if
						COUNTS(i) = COUNTS(i) + 1
					end do
					point=>point%next
				end do
			end do
		
			
			l=1
			do i=0,w_size-1
				if(COUNTS(i)==0) cycle
				
			
				call MPI_ISEND(SBUFF(i)%A,counts(i),&
				MPI_REAL8,i,0,MPI_COMM_WORLD,request(l), ierr)
				l=l+1
				call MPI_ISEND(SBUFF(i)%IA,counts(i),&
				MPI_INTEGER,i,1,MPI_COMM_WORLD,request(l), ierr)
				l=l+1
				call MPI_ISEND(SBUFF(i)%JA,counts(i),&
				MPI_INTEGER,i,2,MPI_COMM_WORLD,request(l), ierr)
				l=l+1
				call MPI_ISEND(SBUFF(i)%update_p,counts(i),&
				MPI_INTEGER,i,3,MPI_COMM_WORLD,request(l), ierr)
				l=l+1
				
				call MPI_IRECV(AA(NELT_P+1 : NELT_P +COUNTR(i)),COUNTR(i),&
					MPI_REAL8,i,0,MPI_COMM_WORLD,request(l), ierr )
				l=l+1
				call MPI_IRECV(IAA(NELT_P+1 : NELT_P +COUNTR(i)),COUNTR(i),&
					MPI_INTEGER,i,1,MPI_COMM_WORLD,request(l), ierr )
				l=l+1
				call MPI_IRECV(JAA(NELT_P+1 : NELT_P +COUNTR(i)),COUNTR(i),&
					MPI_INTEGER,i,2,MPI_COMM_WORLD,request(l), ierr )
				l=l+1
				call MPI_IRECV(updaten_p(NELT_P+1 : NELT_P +COUNTR(i)),COUNTR(i),&
					MPI_INTEGER,i,3,MPI_COMM_WORLD,request(l), ierr )
				l=l+1
				NELT_P = NELT_P + countr(i)
			end do	
			if(l>1) call MPI_Waitall(l-1,request,stats,ierr)
				
			
				
		
			do i=0,w_size-1
				if(COUNTS(i)==0) cycle
				deallocate(SBUFF(i)%A)
				deallocate(SBUFF(i)%IA)
				deallocate(SBUFF(i)%JA)
				deallocate(SBUFF(i)%update_p)
			end do
			
			if(ii>1) then
				do i=0,w_size-1
					if(counte(i)/=0) then
						deallocate(EXSBUFF(i)%IA)
					end if
					if(countre(i)/=0) then
						deallocate(EXRBUFF(i)%IA)
					end if
				end do
			end if
			
		!Modifying IA and JA of the newly received elements to have the local numbering of current process
			ucount = 1
			NT_PC=NT_P
			l = IAA(NI_P+1)
			COUNTRE = 0
			!if(rank==1) print *, IAA(l+1:l+counts(0))
			
			!if(rank==1) print *, JAA(l+1:l+counts(0))
			
			!if(rank==1) print *, updaten_p(l+1:l+counts(0))
			
			do i=0,w_size-1
				k=1
				do j=l+1, l+countr(i)
					if(j<=l+countr(i)-1 .and. IAA(j)==IAA(j-1)) then
						k=k+1
					else
						IAA(j-k:j-1)=find_loc(IAA(j-1),i,NT_PC,NMAP)
						if(IAA(j-1)==NT_PC+1) print *,"ERROR 232"
						k=1
					end if
					
					if(updaten_p(j-1) /= rank) then
						loc = find_loc(JAA(j-1),updaten_p(j-1),NT_PC,NMAP)
						if(updaten_p(j-1)/=i) COUNTRE(i) = COUNTRE(i)+1
						if(loc==NT_PC+1) then
							updaten_l(ucount) = JAA(j-1)
							updaten_p(ucount) = updaten_p(j-1)
							ucount = ucount+1
							NT_PC=NT_PC+1
							call add_mapel(NT_PC,updaten_p(ucount-1),updaten_l(ucount-1),NMAP)
						end if
						JAA(j-1)=loc
					end if	
				end do			
				l = countr(i) + l
			end do
			update_l(gcount+1:gcount+ucount-1) = updaten_l(1:ucount-1)
			update_p(gcount+1:gcount+ucount-1) = updaten_p(1:ucount-1)
			gcount = gcount+ucount-1
			
		!Sorting out the newly incorporated elements
		    l=IAA(NI_P+1)
			
			if(NELT_P>=l) call QS2I1D(IAA(l:NELT_P),JAA(l:NELT_P), &
					AA(l:NELT_P), NELT_P-l+1, 1, IUNIT )
				
			j= NI_P+1
			IAA(j+1) = IAA(j)
			k=1
			do i = IAA(NI_P+1)+1,NELT_P+1
				if ( i<=NELT_P .and. IAA(i)==IAA(i-1)) then
					IAA(j+1) = IAA(j+1) + 1
					k=k+1
					cycle
				end if
				IAA(j+1) = IAA(j+1) + 1
				j=j+1	
				k=1
				IAA(j+1) = IAA(j)
			end do	
			IAA(NT_P+1)=NELT_P+1
			
			
			!Sorting within each row
			do i=NI_P+1,NT_P
				!First element to be diagonal element
				do j=IAA(i),IAA(i+1)-1
					if(JAA(j) == i) then
						TempA = AA(j)
						AA(j) = AA(IAA(i))
						AA(IAA(i)) = TempA
						
						TempJA = JAA(j)
						JAA(j) = JAA(IAA(i))
						JAA(IAA(i)) = TempJA
						exit
					end if
				end do
				
				!Subsequent sorting
				do j=IAA(i)+1,IAA(i+1)-2
					do k=j+1,IAA(i+1)-1
						if(JAA(k) < JAA(j)) then
							tempJA = JAA(k)
							JAA(k) = JAA(j)
							JAA(j) = tempJA
							
							TempA = AA(k)
							AA(k) = AA(j)
							AA(j) = TempA
						end if
					end do
				end do
			end do
					
! if(calle==1 ) then
		! print *,"RANK",rank, NI_P,NT_P,NELT_P
		! call MPI_BARRIER(MPI_COMM_WORLD,ierr)		
! stop	
! end if 
			NI_P = NT_P
			NT_P = NT_P + ucount-1
			
			call freeext(HEADEXT)
				! if(ii==2) then
			! 	 *, "RANK",rank,"COUNTS",counts,"COUNTR",countr
				! print *, ""
				! call MPI_BARRIER(MPI_COMM_WORLD,ierr)
				! stop
			! end if	
			
	end do
	!Find NL and NU
	NL=0
	NU=0
	
	 
	do i=1,NI_P
		do j=IAA(i)+1,IAA(i+1)-1,1
			if(JAA(j)>NI_P) EXIT
			if(JAA(j)>i) then
				NU=NU+1
			else
				NL=NL+1
			end if
		end do
	end do
	
	!Assign position in Rwork array
	  LOCL   = IWORK(10)   
      LOCDIN = LOCL + N_SUB*NELT_P
      LOCUU  = LOCDIN + N_SUB*NI_P
	  LOCW = LOCUU + N_SUB*NELT_P
	!Assign position in iwork array
	  LOCIL = LOCIB !Starting location in IWORK
      LOCJL = LOCIL + N_SUB*(NI_P+1)
      LOCIU = LOCJL + N_SUB*NU
      LOCJU = LOCIU + N_SUB*NL
      LOCNR = LOCJU + N_SUB*(NI_P+1)	!LOCNR = NROW in DSILUS
      LOCNC = LOCNR + N_SUB*NI_P		!LOCNC = NCOL in DSILUS
      LOCIW = LOCNC + N_SUB*NI_P
	
	  IWORK(1) = LOCIL
      IWORK(2) = LOCJL
      IWORK(3) = LOCIU
      IWORK(4) = LOCJU
      IWORK(5) = LOCL
      IWORK(6) = LOCDIN
      IWORK(7) = LOCUU
      IWORK(9) = LOCIW
	  
	!Checking allocation size
	
	if(LOCIW>LENIW .OR. LOCW>LENW) then
		write(*,*), "AT PROCESS",rank,"NOT ENOUGH MEMORY TO CARRY OUT ILU forward elimination"	
		print *, "ABORTING"
		stop
	end if

	!Finding partition
	if(ICALL==1) then
		if(N_SUB/=1) THEN
			call domain_partition(IAA,JAA,NELT_P,NI_P,N_SUB,part_list)
			part_list=part_list + 1   !Warning: whole array operation
			ELSE
			part_list=1
		end if
	end if
    !Allocate arrays needed for local DD
	allocate(LNSUB(NI_P))
	allocate(TPART(NI_P))
	allocate(ERRAY(NI_P,N_SUB))
	allocate(TLNSUB(NI_P))
	
	
	!Iterate to form the local subdomain numbers 
	NI_PL(1:N_sub) =0 
	
	DO I=1,NI_P
		NI_PL(part_list(I))=NI_PL(part_list(I))+1
		LNSUB(I)=NI_PL(part_list(I))
	END DO
	DO I=1,N_SUB
		TPART(1:NI_P) = part_list(1:NI_P)
		TLNSUB(1:NI_P)=LNSUB(1:NI_P)
		TNI_PL=0
		DO J=1,NI_P
			IF(TPART(J) .NE. I) CYCLE
			TNI_PL=TNI_PL+1
			ERRAY(TNI_PL,I) = J
		END DO
		TNI_PL_O = TNI_PL
		
	!Change TPART and TLNSUB to form overlap
		IF(N_SUB>1) THEN
		DO J=1,overlap_loc
			TNI_PL_O = TNI_PL
			DO K=1,NI_P
				if(TPART(K)==I .AND. TLNSUB(K)<=TNI_PL_O) THEN
					DO L=IAA(K)+1,IAA(K+1)-1
						IF(TPART(JAA(L)) .NE. I) THEN
							TPART(JAA(L)) = I
							TLNSUB(JAA(L))=TNI_PL+1
							TNI_PL=TNI_PL+1
							ERRAY(TNI_PL,I) = JAA(L)
						END IF
					END DO
				END IF
			END DO
		END DO
		END IF
	!Form AAA,IAAA,JAAA for subdomain I, and find TNI_PL=Number of elements in Ith subdomain after overlap
		IAAA=0 	  !WARNING: WHOLE ARRAY OPERATION
		IAAA(1)=1 
		NELT_PL=0
		DO J=1,TNI_PL
			IAAA(J+1)=IAAA(J)+1
			NELT_PL=NELT_PL+1
			L=ERRAY(J,I)
			AAA(NELT_PL) = AA(IAA(L))
			JAAA(NELT_PL) = J
			DO K=IAA(L)+1,IAA(L+1)-1
				IF(TPART(JAA(K)) .NE. I) CYCLE
				IAAA(J+1)=IAAA(J+1)+1
				NELT_PL=NELT_PL+1
				AAA(NELT_PL) = AA(K)
				JAAA(NELT_PL) = TLNSUB(JAA(K))
			END DO
		END DO
		
	!Sort within each IAAA(K):IAAA(K+1)-1
		DO J=1,TNI_PL
			DO K=IAAA(J)+1,IAAA(J+1)-2
				DO L=K+1,IAAA(J+1)-1
					IF(JAAA(L)>=JAAA(K)) CYCLE
					tempA = AAA(K)
					AAA(K) = AAA(L)
					AAA(L) = tempA
					
					tempJA = JAAA(K)
					JAAA(K) = JAAA(L)
					JAAA(L) = tempJA
				END DO
			END DO
		END DO
		
	!Find local NL and NU: NL_L and NU_L
		NL_L=0
		NU_L=0
		 
		do J=1,TNI_PL
			do K=IAAA(J)+1,IAAA(J+1)-1,1
				if(JAAA(K)>J) then
					NU_L=NU_L+1
				else
					NL_L=NL_L+1
				end if
			end do
		end do
	!CALL ILU preconditioner on the subdomain
		
		call  DSILUS_SCHW(TNI_PL,NELT_PL,IAAA,JAAA,AAA,NL_L,IWORK(LOCIL+(I-1)*(NI_P+1)),IWORK(LOCJL+(I-1)*NL),&
	 &                  RWORK(LOCL+(I-1)*NELT_P),RWORK(LOCDIN+(I-1)*NI_P),NU_L,IWORK(LOCIU+(I-1)*NU),&
						IWORK(LOCJU+(I-1)*(NI_P+1)),RWORK(LOCUU+(I-1)*NELT_P),IWORK(LOCNR+(I-1)*NI_P),IWORK(LOCNC+(I-1)*NI_P))
		NI_PL(I)=TNI_PL
	
	END DO
	
	 call deallocate_nmap(NMAP)
	
	
	do i=0,w_size-1
		HEADS(i)%next=>HEAD(i)%next
		HEADS(i)%endp=>HEAD(i)%endp
		HEADS(i)%ende=>HEAD(i)%ende
		
		if(associated(HEAD(i)%endp)) then
			if(associated(HEAD(i)%endp%next)) then	
				HEADS(i)%curr=>HEAD(i)%endp%next		
				HEADS(i)%ende%next=>HEADS(i)%next
				NULLIFY(HEAD(i)%endp%next)
			else
				HEADS(i)%curr=>HEADS(i)%next
			end if
			HEADS(i)%next=>HEADS(i)%curr
		else
			HEADS(i)%curr=>HEAD(i)%next
			NULLIFY(HEAD(i)%next)
		end if
		
	end do
	! if(rank==0) then
	! do i=0,w_size-1
		! if(i==1) print *, HEAD(i)%endp%loc, "@@"
		! point=>HEAD(i)%next
		! do while(associated(point))
			! print *, point%loc
			! point=>point%next
		! end do
	! end do
	! end if
	! call MPI_BARRIER(MPI_COMM_WORLD,ierr)
	! stop
	
end subroutine SCHWZ_PRC_HYB

SUBROUTINE DCGS_SW_PAR_HYB(NI,NT,NI_P, B, X, NELT, IA, JA, A, ISYM, MATVEC,&
     &     MSOLVE, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, &
     &     R, R0, P, Q, U, V1, V2, RWORK, IWORK,update_l,UPDATE_P,HEAD,HEADS,overlap&
	 &	  ,N_SUB,NI_PL,overlap_loc)
!
!!N, B, X, NELT, IA, JA, A, ISYM, DSMV,&           !!!ITERATION BASICALLY STARTS AFTER THIS. Algorithm after here.
 !         DSLUI, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT,&
 !         RWORK(LOCR), RWORK(LOCR0), RWORK(LOCP),&
 !         RWORK(LOCQ), RWORK(LOCU), RWORK(LOCV1),&
  !        RWORK(LOCV2), RWORK, IWORK
!
	use MPI_VAR
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
	  include 'mpif.h'
      INTEGER NI, NT,NI_P, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITMAX
      INTEGER ITER, IERR, IUNIT, IWORK(*)
      REAL(KIND = 8) B(NT), X(NT), A(NELT), TOL, ERR, R(NT), R0(NT), P(NT)
      REAL(KIND = 8) Q(NT), U(NT), V1(NT), V2(NT), RWORK(*)
!
      REAL(KIND = 8) DMACH(5)
	  INTEGER ::update_l(*),UPDATE_P(*),overlap
	  TYPE(HEADEX) :: HEAD(*),HEADS(*)
	  INTEGER :: NI_PL(*),N_SUB,overlap_loc
      DATA DMACH(3) / 1.1101827117665D-16 /

      EXTERNAL MATVEC, MSOLVE
	  
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DCGS
!
!
      ITER = 0
      IERR = 0
!
      IF( NI < 1 ) THEN
         IERR = 3
         RETURN
      ENDIF
!
      TOLMIN = 5.0d2*DMACH(3)
      IF( TOL < TOLMIN ) THEN
         TOL  = TOLMIN
         IERR = 4
      ENDIF
!  
 
	
      
      CALL MATVEC(NI, NT, X, R, NELT, IA, JA, A, ISYM,UPDATE_P,update_l,head) !DSMV Matvec computes AX -> R. MSOLVE solves the system MR = V1, where V1 is residual AX - R from previous step, M = LDU - the ldu decomposition from preconditioning.
!  
      V1(1:NI)  = R(1:NI) - B(1:NI)
!      
      CALL MSOLVE(NI,NI_P, V1, R, NELT, IA, JA, A, ISYM, RWORK, IWORK,HEAD,HEADS,overlap,update_l,update_p,&
						&N_SUB,NI_PL)	!DSLUI - Solves for R in LDU.R = B1 Directly parallelizable
!      
		
      ISDCGS = 0
      ITOL   = 2
      IF(ITER == 0) THEN
         CALL MSOLVE(NI,NI_P, B, V2, NELT, IA, JA, A, ISYM, RWORK, IWORK,HEAD,HEADS,overlap,update_l,update_p,&
						&N_SUB,NI_PL)

         BNRM = SQRT(DOT_PROD_PAR(V2,V2,NI))
      ENDIF
!         
      ERR = SQRT(DOT_PROD_PAR(R,R,NI))/BNRM
!     
	
	if(rank==0) then
      IF(IUNIT /= 0) THEN
         IF( ITER == 0 ) THEN
            WRITE(IUNIT,1000) N_SUB,w_size,overlap_loc,NT, ITOL
         ENDIF
         WRITE(IUNIT,1010) ITER, ERR, AK, BK
      ENDIF
	end if
	
      IF(ERR <= TOL) ISDCGS = 1
!         
      IF(ISDCGS /= 0) GO TO 200
!
!
      FUZZ = DMACH(3)**2
!
      R0(1:NI) = R(1:NI)
!
      RHONM1 = 1.0D0
!         
!         
      DO K=1,ITMAX
         ITER = K
!
         RHON = DOT_PROD_PAR(R0,R,NI)
!

         IF( ABS(RHONM1) < FUZZ ) GO TO 998
!
         BK = RHON/RHONM1
!

         IF( ITER.EQ.1 ) THEN
            U(1:NI) = R(1:NI)
            P(1:NI) = R(1:NI)
         ELSE
            U(1:NI)  = R(1:NI) + BK*Q(1:NI)
            V1(1:NI) = Q(1:NI) + BK*P(1:NI)
            P(1:NI)  = U(1:NI) + BK*V1(1:NI)
         ENDIF
!         
         CALL MATVEC(NI, NT, P, V2, NELT, IA, JA, A, ISYM,UPDATE_P,update_l,head)
         CALL MSOLVE(NI,NI_P, V2, V1, NELT, IA, JA, A, ISYM, RWORK, IWORK,HEAD,HEADS,overlap,update_l,update_p,&
						&N_SUB,NI_PL)
!         
 		 
         SIGMA = DOT_PROD_PAR(R0,V1,NI)

         IF( ABS(SIGMA) < FUZZ ) GO TO 999
!         
         AK = RHON/SIGMA
         AKM = -AK
!         
         Q(1:NI)  = U(1:NI) + AKM*V1(1:NI)
!         
         V1(1:NI) = U(1:NI) + Q(1:NI)
!         
         X(1:NI)  = X(1:NI) + AKM*V1(1:NI)
!           
           
         CALL MATVEC(NI, NT, V1, V2, NELT, IA, JA, A, ISYM,UPDATE_P,update_l,head)
         CALL MSOLVE(NI,NI_P, V2, V1, NELT, IA, JA, A, ISYM, RWORK, IWORK,HEAD,HEADS,overlap,update_l,update_p,&
						&N_SUB,NI_PL)
		
	
         R(1:NI) = R(1:NI) + AKM*V1(1:NI)
!         

         ISDCGS = 0
         ITOL   = 2
!         
         ERR = SQRT(DOT_PROD_PAR(R,R,NI))/BNRM
!        
		 if(rank==0) then
			 IF(IUNIT /= 0) THEN
				WRITE(IUNIT,1010) ITER, ERR, AK, BK
			 ENDIF
		 end if
		 
         IF(ERR <= TOL) ISDCGS = 1
!         
 
         IF(ISDCGS /= 0) GO TO 200
!

         RHONM1 = RHON

	  END DO
 1000 FORMAT('BiConjugate Gradient Squared Preconditioned with Hybdrid Schwarz Method  ',&
			&'Number of local subdomains per process, number of process and overlap:',3I5,&
          'N, ITOL = ',I5, I5,&
          /' ITER','   Error Estimate','            Alpha',&
          '             Beta')
 1010 FORMAT(1X,I4,1X,E16.7,1X,E16.7,1X,E16.7)
!         
      ITER = ITMAX + 1
      IERR = 2
 200  RETURN
!
 998  IERR = 5
      RETURN
!
 999  IERR = 6
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DCGS
!
!
      RETURN
END SUBROUTINE DCGS_SW_PAR_HYB
	  

SUBROUTINE DRIVER_HYB(N,B,X,NELT,IA,JA,A,TOL,ITMAX,ITER,ERR,&
     &                  IERR,IUNIT,RWORK,LENW,IWORK,LENIW,N_SUB,overlap_loc)
	  use MPI_VAR
	 IMPLICIT REAL(KIND = 8)(A-H,O-Z)
	 include 'mpif.h'
!
	  INTEGER,INTENT(IN) :: N
      INTEGER :: NI, NT, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITMAX, ITER
      INTEGER :: IERR, IUNIT, LENW, IWORK(LENIW), LENIW,MAAS,N_SUB
      REAL(KIND = 8) :: B(N), X(N), A(NELT), TOL, ERR, RWORK(LENW)
      EXTERNAL ::  DSLUI
	 ! EXTERNAL :: DSMV_PAR
	  INTEGER :: UPDATE_P(1),update_l(1),overlap,overlap_loc,TITER,ICALL=0
	  SAVE TITER,ICALL
!	
	 NI=N
	 NT=N
	 MAAS=NELT+1
	 overlap=0
	 update_p=-1
	 update_l=-1
	 call DSLUCS_SW_PAR_HYB(NI,NT,B,X,NELT,IA,JA,A,TOL,ITMAX,ITER,ERR,&
    &                  IERR,IUNIT,RWORK,LENW,IWORK,LENIW,UPDATE_P,update_l,&
					overlap,MAAS,N_SUB,overlap_loc)
		
	 ICALL=ICALL+1
	 TITER=TITER+ITER
	 write(IUNIT,*) , "AVERAGE NUMBER OF ITERATIONS FOR CONVERGENCE: ",TITER/ICALL,"Number of calls",ICALL
	 !print *, X(1:N)
	! stop
END SUBROUTINE DRIVER_HYB


SUBROUTINE DRIVER_WP(N,B,X,NELT,IA,JA,A,TOL,ITMAX,ITER,ERR,&
     &                  IERR,IUNIT,RWORK,LENW,IWORK,LENIW)
	  use MPI_VAR
	 IMPLICIT REAL(KIND = 8)(A-H,O-Z)
	 include 'mpif.h'
!
	  INTEGER,INTENT(IN) :: N
      INTEGER :: NI, NT, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITMAX, ITER
      INTEGER :: IERR, IUNIT, LENW, IWORK(LENIW), LENIW,MAAS
      REAL(KIND = 8) :: B(N), X(N), A(NELT), TOL, ERR, RWORK(LENW)
      EXTERNAL ::  DSLUI
	 ! EXTERNAL :: DSMV_PAR
	  INTEGER :: UPDATE_P(1),update_l(1),overlap,TITER,ICALL=0
	  SAVE TITER,ICALL
!
	
	 NI=N
	 NT=N
	 MAAS=NELT+1
	 overlap=0
	 update_p=-1
	 update_l=-1
	 call DSLUCS_WP_PAR(NI,NT,B,X,NELT,IA,JA,A,TOL,ITMAX,ITER,ERR,&
     &                  IERR,IUNIT,RWORK,LENW,IWORK,LENIW,UPDATE_P,update_l)!,&
						!overlap,MAAS)
	 
	 ICALL=ICALL+1
	 TITER=TITER+ITER
	 write(IUNIT,*) , "AVERAGE NUMBER OF ITERATIONS FOR CONVERGENCE: ",TITER/ICALL
	
END SUBROUTINE DRIVER_WP
	
 

! SUBROUTINE DRIVER_SCHU(N,B,X,NELT,IA,JA,A,TOL,ITMAX,ITER,ERR,&
     ! &                  IERR,IUNIT,RWORK,LENW,IWORK,LENIW,N_SUB,overlap_loc)
	  ! use MPI_VAR
	  ! use schur_seq
	 ! IMPLICIT REAL(KIND = 8)(A-H,O-Z)
	 ! include 'mpif.h'
! !
	  ! INTEGER,INTENT(IN) :: N
      ! INTEGER :: NI, NT, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITMAX, ITER
      ! INTEGER :: IERR, IUNIT, LENW, IWORK(LENIW), LENIW,MAAS,N_SUB
      ! REAL(KIND = 8) :: B(N), X(N), A(NELT), TOL, ERR, RWORK(LENW)
      ! EXTERNAL ::  DSLUI
	 ! ! EXTERNAL :: DSMV_PAR
	  ! INTEGER :: UPDATE_P(1),update_l(1),overlap,overlap_loc,TITER,ICALL=0
	  ! DOUBLE PRECISION :: t1,t2
	  ! SAVE TITER,ICALL
! !	
	 ! ICALL=ICALL+1
	 
	 ! NI=N
	 ! NT=N
	 ! MAAS=NELT+1
	 ! overlap=0
	 ! update_p=-1
	 ! update_l=-1
	! t1 = MPI_WTIME()
	! call DSLUCS_SCP_INEX(N,B,X,NELT,IA,JA,A,TOL,ITMAX,ITER,ERR,&
    ! &                  IERR,IUNIT,RWORK,LENW,IWORK,LENIW,N_SUB)
	! print *,"TIME FOR SOLVING USING SCHU PREC",MPI_WTIME()-t1
	
	 ! TITER=TITER+ITER
	 ! write(IUNIT,*) , "AVERAGE NUMBER OF ITERATIONS FOR CONVERGENCE: ",TITER/ICALL,"Number of calls",ICALL
	 ! !print *, X(1:N)
	! ! stop
! END SUBROUTINE DRIVER_SCHU



SUBROUTINE domain_partition(IA,JA,NELT,N,N_SUB,part_list)

IMPLICIT NONE
INTEGER :: IA(*),JA(*),NELT,N,N_SUB,part_list(*)
INTEGER :: xadj(0:N),adjncy(0:NELT-1),NELT_N
INTEGER :: I,J
INTEGER :: ncon=1
integer, pointer :: vwget=>NULL(), vsize=>NULL() !weights and size of vertices
integer, pointer :: adjwget=>NULL() !Weights of edges
real, pointer :: tpwgets=>NULL(),ubvec=>NULL()
integer,pointer :: mopts=>NULL() !OPTIONS 
integer,pointer  ::  objval=>NULL() !Stores the edge-cut or the total comm. Volume of the partitioning solution
integer :: part(0:N-1),error
CHARACTER(LEN=14) :: filenm
LOGICAL ::EX
INTEGER :: ICALL=0

SAVE ICALL

ICALL = ICALL+1

 open(821,FILE="CSR")
 !Converting to C-indexed CSR arrays according to METIS by also removing connection to self
  xadj(0)=IA(1)-1
  NELT_N=0
  DO I=1,N
	adjncy(NELT_N:NELT_N+IA(I+1)-2-IA(I)) = JA(IA(I)+1:IA(I+1)-1)-1
	NELT_N=NELT_N+IA(I+1)-1-IA(I)
	 xadj(I)=xadj(I-1)+IA(I+1)-1-IA(I)
  END DO
  
  !Writing to the METIS input file, N, and number of unique connections (1-2 and 2-1 connections are counted as one)
  write(821,*)  N,(NELT_N)/2
	  
  DO I=1,N
	write(821,*),adjncy(Xadj(I-1):Xadj(I)-1)+1
  END DO
  
  write(filenm,*),N_SUB
  filenm = trim(adjustl(filenm))
  
  error = system('gpmetis CSR '//filenm)
  if(error/=0) then
	print *, "ERROR in METIS partitioning algorithm at 77821"
	stop
  end if  
  filenm = "CSR.part."//filenm
  
  INQUIRE(FILE=filenm,EXIST = EX)
  IF(.NOT. EX) then
	print *, "NEED PARTITIONING DATA. ABORT!!!!!!"
	stop
  end if
  
 open(751,FILE=filenm)

 READ(751,*),part_list(1:N)
 close(751)
 
END SUBROUTINE domain_partition



	END MODULE Matrix_Solvers_par
