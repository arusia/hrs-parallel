HRS_run: HRS_MPI.o LinEqSolver_par.o schur_prec_saad.o HRS_RealGasEOS.o HRS_Hydrate_PP.o HRS_AllocateMem.o HRS_Main.o HRS_H2O_PP.o HRS_PorousMed_PP.o HRS_HydrateEOS.o HRS_Executive.o HRS_Inputs.o HRS_MeshMaker.o
	mpif90 -o $@ $^

HRS_MPI.o: HRS_MPI.f95
	mpif90 -c HRS_MPI.f95

LinEqSolver_par.o: HRS_MPI.f95 LinEqSolver_par.f90
	mpif90 -c LinEqSolver_par.f90
	
schur_prec_saad.o: LinEqSolver_par.f90 schur_prec_saad.f90
	mpif90 -c schur_prec_saad.f90

HRS_RealGasEOS.o: HRS_MPI.f95 HRS_RealGasEOS.f95
	mpif90 -c HRS_RealGasEOS.f95

HRS_Hydrate_PP.o: HRS_MPI.f95 HRS_Hydrate_PP.f95
	mpif90 -c HRS_Hydrate_PP.f95

HRS_AllocateMem.o: HRS_MPI.f95 HRS_RealGasEOS.f95 HRS_Hydrate_PP.f95 HRS_AllocateMem.f95
	mpif90 -c HRS_AllocateMem.f95

HRS_Main.o: HRS_MPI.f95 HRS_RealGasEOS.f95 HRS_Hydrate_PP.f95 HRS_AllocateMem.f95 HRS_Main.f95
	mpif90 -c HRS_Main.f95

HRS_H2O_PP.o: HRS_Main.f95 HRS_H2O_PP.f95
	mpif90 -c HRS_H2O_PP.f95

HRS_PorousMed_PP.o: HRS_Main.f95 HRS_PorousMed_PP.f95
	mpif90 -c HRS_PorousMed_PP.f95

HRS_HydrateEOS.o: HRS_HydrateEOS.f95
	mpif90 -c HRS_HydrateEOS.f95
	
HRS_Inputs.o: HRS_Main.f95 HRS_Inputs.f95
	mpif90 -c HRS_Inputs.f95
	
HRS_Executive.o: HRS_Main.f95 HRS_HydrateEOS.f95 HRS_Executive.f95
	mpif90 -c HRS_Executive.f95
	
HRS_MeshMaker.o: HRS_Main.f95 HRS_MeshMaker.f95
	mpif90 -c HRS_MeshMaker.f95  
	
.PHONY: clean

clean:
	rm  ./*.o
	rm ./*.mod
