 module schur_var
	use Matrix_Solvers_par, only: HEADEX
	real (kind=8),dimension(:),allocatable :: AA,E
	integer,dimension(:),allocatable :: JAA,IAA,color,EI,EJ,update_l
	integer :: NII,NIB,NELT_AA,P
	integer, parameter :: FILL=5
	integer :: LOCSI,LOCSR,SLENIW,SLENW,NELT_max,NELT_S
	TYPE(HEADEX),allocatable:: HEAD_schu(:)
 end module schur_var
 
 module schur
 
	use Matrix_Solvers_par
 contains
 
 SUBROUTINE DSLUCS_SC_PAR(NI,NT,B,X,NELT,IA,JA,A,TOL,ITMAX,ITER,ERR,&
     &                  IERR,IUNIT,RWORK,LENW,IWORK,LENIW,UPDATE_P,UPDATE_G)
!---------------------------------------------------------
!This the parallel version of DSLUCS. Changed on 102616. Author - Aman Rusia
!--------------------------------------------------------
!NL - Number of elements in internal set
!NT - Number of elements in total = internal + update set
!B - Right hand side of the system - processor dependent size (NT)
!X - It is the final solution - processor dependent size (NT). X should be guess solution 
!NELT - size of A, IA and JA. Assign NELT_MAX+1 to them
!IA,JA,A - irn ,icn,co distributed
!    &                  CLOSUR,NMAXIT,ITERU,ERR,IERR,&
!   &                  IUNIT
!RWORK - size LENW.  Work array. Originally LENW = max(1000+mnz+38*MAXNUM_ELEM * NEQ, 9*MaxNum_Conx+1000) where mnz = max size of CO = NEQ*NEQ*(MaxNum_elements+2*MaxNum_conx). New LENW = 1000+mnz+38*MAXNUM_ELEM * NEQ, 9*MaxNum_Conx+1000
!IWORK - size LENIW. jvect. Work array. Originally LENIW = 100+mnz+5*MAXNUM_ELEM*NEQ. IWORK(1:10) stores Upper,Lower triangular matrices and Diagonal inverse of preconditioner, and their column and row information
!new the above sizes are changed according to new MAXNUM_ELEM = MAXNUM_ELEM/size. Note that MaxNum_Conx should remain intact since it could be fully inside a processor.
!RWORK also stores values of elements in L, U and Dinv after preconditioning. IWORK stores their respective row and column indices
!IWORK not needed in non preconditioned dslucs
	 use MPI_VAR
	 use schur_var
	 IMPLICIT REAL(KIND = 8)(A-H,O-Z)
	 include 'mpif.h'
!
      INTEGER :: NI, NT, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITMAX, ITER
      INTEGER :: IERR, IUNIT, LENW, IWORK(LENIW), LENIW
      REAL(KIND = 8) :: B(NT), X(NT), A(NELT), TOL, ERR, RWORK(LENW)
    !  EXTERNAL :: DSMV_PAR_MAP, DSLUI_PAR
	  INTEGER :: UPDATE_P(*),UPDATE_G(*),overlap
	  
!
      INTEGER, PARAMETER :: LOCRB=1, LOCIB=11
	  TYPE(HEADEX),allocatable :: head(:),heads(:)
	  TYPE(EXEL),pointer :: point
	  INTEGER, allocatable :: COUNTS(:)
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSLUCS
!
!

	CALLE=CALLE+1
	 call MPI_COMM_RANK ( MPI_COMM_WORLD,rank,IERR)
	 call MPI_COMM_SIZE ( MPI_COMM_WORLD, w_size, IERR)
	 
	 allocate(head(0:w_size-1))
	 allocate(COUNTS(0:w_size-1))
	 
      ISYM = 0
      ITOL = 2
!
      IERR = 0
      IF( NI.LT.1 .OR. NELT.LT.1 ) THEN
         IERR = 3
         RETURN
      ENDIF
      CALL DS2Y( NI, NELT, JA, IA, A, ISYM, IUNIT ) !In parallel version, IA and JA have been swapped. No other change inside
    
!--------212         
    
      LOCR   = LOCRB
      LOCR0  = LOCR + NT
      LOCP   = LOCR0 + NT
      LOCQ   = LOCP + NT
      LOCU   = LOCQ + NT
      LOCV1  = LOCU + NT
      LOCV2  = LOCV1 + NT
	  LOCW   = LOCV2 + NT
!
	
      IWORK(10) = LOCW
!
      IF( IERR.NE.0 ) RETURN
!
		
		
		call Initialize_Exmap(head,IA,JA,A,update_p,update_g,NELT,NI,NT,COUNTS)

        call schur_prec(NI,NT,NELT,A,IA,JA,update_g,update_p,&
					LOCIB,IWORK,RWORK,IUNIT,LENW,LENIW)
!END OF SCHUR COMPLEMENT INITIALIZATION-----------------------------------------------

		
      CALL DCGS_SU_PAR(NI,NT, B, X, NELT, IA, JA, A, ISYM, DSMV_PAR_MAP,&
          SCHUR_MSOLVE, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT,&
          RWORK(LOCR), RWORK(LOCR0), RWORK(LOCP),&
          RWORK(LOCQ), RWORK(LOCU), RWORK(LOCV1),&
          RWORK(LOCV2), RWORK, IWORK, UPDATE_G,UPDATE_P,HEAD,COUNTS)
	  
	  !DEALLOCATE AA,AB,BA,BB and corrosponding IA,JA vectors
		deallocate(AA)
		deallocate(JAA)
		deallocate(IAA)
		
		deallocate(E)
		deallocate(EI)
		deallocate(EJ)
		deallocate(color)

	    call Free_Exmap(head)
	    deallocate(head)
	    deallocate(COUNTS)
		deallocate(update_l)
		deallocate(HEAD_schu)
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DSLUCS
!
!
      RETURN
      END SUBROUTINE DSLUCS_SC_PAR


subroutine schur_prec(NI,NT,NELT,A,IA,JA,update_g,update_p,LOCIB,&
					& IWORK,RWORK,IUNIT,LENW,LENIW)
	use mpi_var
	use schur_var
	implicit none
	include 'mpif.h'
	
	integer,intent(in) :: NI,NT,NELT,IUNIT
	integer, intent(in) :: IA(*),JA(*),LOCIB,LENW,LENIW
	real(kind=8),intent(in) :: A(*)
	real(kind=8) :: RWORK(*),temp,FUZZ=1e-22,TOL=1e-12
	integer :: update_g(*),update_p(*),IWORK(*)
	integer :: i,ii,j,k,l,ierr,tempj,poss,ISYM,max_row,NTB
	integer :: NL,NU,LOCIL,LOCJL,LOCIU,LOCJU,LOCL,LOCD,LOCUU,LOCIW,LOCNC,LOCNR,LOCW
	integer :: CALLE =0
	SAVE CALLE
	CALLE = CALLE + 1

  !Iterate over each element to color positive and negative
	 NII=0
	 NIB=0
	allocate(COLOR(NI))
	 do i=1,NI
		if(JA(IA(i+1)-1)>NI) then
			NIB=NIB+1
			color(i)=-NIB
		else
			NII=NII+1
			color(i)=NII
		end if
	 end do
	!Allocate AA, IAA and JAA
	
	allocate(AA(NELT))
	allocate(JAA(NELT))
	allocate(IAA(NI+1))
	
	max_row = maxval(IA(2:NI+1)-IA(1:NI))
	
	allocate(EI(NIB+1))
	allocate(E(max_row*NIB))
	allocate(EJ(max_row*NIB))
	!Iterate over each element to define AA,AB,BA and BB
	IAA(1)=1
	EI(1)=1
	do i=1,NI
		if(color(i)<0) CYCLE
		IAA(color(i)+1)=IAA(color(i))
		do j=IA(i),IA(i+1)-1
			if(color(JA(j))>0) then
				IAA(color(i)+1)=IAA(color(i)+1)+1
				JAA(IAA(color(i)+1)-1) = color(JA(j))
				AA(IAA(color(i)+1)-1) = A(j)
			else
				IAA(color(i)+1)=IAA(color(i)+1)+1
				JAA(IAA(color(i)+1)-1) = NII-color(JA(j))
				AA(IAA(color(i)+1)-1) = A(j)
			end if
		end do
	end do
	do i=1,NI
		if(COLOR(I)>0) CYCLE
		IAA(NII-color(i)+1)=IAA(NII-color(i))
		EI(-color(i)+1)=EI(-color(i))
		do j=IA(i),IA(i+1)-1
			if(JA(J)>NI) THEN
				EI(-color(i)+1)=EI(-color(i)+1)+1
				EJ(EI(-color(i)+1)-1) = JA(j)-NI
				E(EI(-color(i)+1)-1) = A(j)
				cycle
			end if
			if(color(JA(j))>0) then
				IAA(NII-color(i)+1)=IAA(NII-color(i)+1)+1
				JAA(IAA(NII-color(i)+1)-1) = color(JA(j))
				AA(IAA(NII-color(i)+1)-1) = A(j)
			else
				IAA(NII-color(i)+1)=IAA(NII-color(i)+1)+1
				JAA(IAA(NII-color(i)+1)-1) = NII-color(JA(j))
				AA(IAA(NII-color(i)+1)-1) = A(j)
			end if
		end do
	end do
	
	!----------SORTING
	DO L=1,NI
		!Doing bubble sort for each element--It is already ensured that first element is diagonal, so leave that
		DO J=IAA(L)+1,IAA(L+1)-2
			poss = J
			DO K=J+1,IAA(L+1)-1
				IF(JAA(poss) .LE. JAA(K)) CYCLE
				poss = K
			END DO
			IF(poss==J) CYCLE
			!Exchange Jth position with smallest element
			tempj = JAA(J)
			temp = AA(J)
			AA(J) = AA(poss) 
			JAA(J) = JAA(poss)
			AA(poss) = temp
			JAA(poss) = tempj
		END DO
	END DO
	
	NL=0
	NU=0
	do i=1,NI
		do j=IAA(i)+1,IAA(i+1)-1,1
			if(JAA(j)>i) then
				NU=NU+1
			else
				NL=NL+1
			end if
		end do
	end do
	
	
	NELT_AA = IAA(NI+1)-1
	
	!-----Assigning P to be fill in + max_row
	P = FILL + max_row/2
	!-----Find location in RWORK and IWORK for each subdomain to store 
	!RWORK and IWORK length for SCHUR COMPLEMENT preconditoining 
	  NTB = NIB + NT-NI
	  SLENW =  7*NTB + (NIB+2*NIB*P) +100
	  SLENIW = 11 + (2*(NIB+1)+2*NIB*P) +100

	!Assign position in Rwork array
	  NELT_max = NI*P
	  LOCL   = IWORK(10)   
	  LOCD = LOCL + NELT_max
	  LOCUU  = LOCD + NI
	  LOCSR = LOCUU + NELT_max
	  LOCW = LOCSR + SLENW
	!Assign position in iwork array
	  LOCIL = LOCIB !Starting location in IWORK
	  LOCJL = LOCIL + (NI+1)
	  LOCIU = LOCJL + (NELT_max)
	  LOCJU = LOCIU + (NI+1)
	  LOCNR = LOCJU + (NELT_max)	!LOCNR = NROW in DSILUS
	  LOCNC = LOCNR + NI		!LOCNC = NCOL in DSILUS
	  LOCSI = LOCNC + NI
	  LOCIW = LOCSI + SLENIW
	  
	!
	
	  IWORK(1) = LOCIL
      IWORK(2) = LOCJL
      IWORK(3) = LOCIU
      IWORK(4) = LOCJU
      IWORK(5) = LOCL
      IWORK(6) = LOCD
      IWORK(7) = LOCUU
      IWORK(9) = LOCIW
	  
	!Checking allocation size
	if(LOCIW>LENIW .OR. LOCW>LENW) then
		write(*,*), "3311 NOT ENOUGH MEMORY TO CARRY OUT ILU forward elimination in rank", rank	
		print *, "ABORTING"
		stop
	end if
	!Form ILU of AA
	!Form ILU for internal array AA
	call  ILUT(NI,NELT_AA,IAA,JAA,AA,NL,IWORK(LOCIL),IWORK(LOCJL),&
	 &            RWORK(LOCL),RWORK(LOCD),NU,IWORK(LOCIU),&
	 IWORK(LOCJU),RWORK(LOCUU),IWORK(LOCNR),IWORK(LOCNC),TOL,P,NI)
	
	! print *, RWORK(LOCUU+IWORK(LOCIU:LOCIU+NI-1)-1)
	
	!Extracting schur complement preconditioner from ILU decomposition of AA.
	IWORK(LOCSI+9) = 1+7*NTB
	call SC_Extract(NI,RWORK(LOCSR),IWORK(LOCSI),SLENW,SLENIW,&
		&			IWORK(LOCIL),IWORK(LOCJL),&
	 &            RWORK(LOCL),RWORK(LOCD),IWORK(LOCIU),&
				IWORK(LOCJU),RWORK(LOCUU),NIB)
	
	
	allocate(update_l(NIB))
	allocate(HEAD_schu(0:w_size-1))

	do i=1,NI
	if(color(i)<0) then
		update_l(-color(i))=i
	end if
	end do

	call Initialize_Exmap_schu(HEAD_schu,EI,EJ,E,update_p,update_g,NIB,update_l)

	end subroutine schur_prec


	SUBROUTINE SC_Extract(N,RWORK,IWORK,LENW,LENIW,IL,JL,L,D,IU,JU,U,NB)
	
	USE SCHUR_VAR , only: NELT_S,P
	IMPLICIT NONE
	
	INTEGER :: IWORK(*),LENW,LENIW,IL(*),JL(*),IU(*),JU(*),N,NB
	REAL(KIND=8) :: RWORK(*),L(*),D(*),U(*)
	INTEGER :: I,J,K
	INTEGER :: LOCIL,LOCJL,LOCL,LOCIU,LOCJU,LOCU,LOCD,LOCIW,LOCW
	INTEGER :: CALLI=0
	SAVE CALLI
	CALLI = CALLI+1

	  NELT_S = NB*P
	  LOCIL = 11 !Starting location in IWORK
	  LOCJL = LOCIL + (NB+1)
	  LOCIU = LOCJL + (NELT_S)
	  LOCJU = LOCIU + (NELT_S)
	  LOCIW = LOCJU + (NB+1)
	  
	  LOCL   = IWORK(10)  
	  LOCD = LOCL + NELT_S
	  LOCU  = LOCD + NB
	  LOCW = LOCU + NELT_S
	  IWORK(1) = LOCIL
	  IWORK(2) = LOCJL
	  IWORK(3) = LOCIU
	  IWORK(4) = LOCJU
	  IWORK(5) = LOCL
	  IWORK(6) = LOCD
	  IWORK(7) = LOCU
	  IWORK(8) = LOCW
	  IWORK(9) = LOCIW
		  
		if(LOCIW >LENIW .OR. LOCW>LENW) then
			write(*,*), "331X NOT ENOUGH MEMORY TO Extract SCHUR preconditioner"
			print *, "ABORTING"
			stop
		end if
	
    IWORK(LOCIL) = 1
	DO I = 1,NB
		IWORK(LOCIL+I) = IWORK(LOCIL+I-1)
		DO J = IL(N-NB+I),IL(N-NB+I+1)-1
			IF(JL(J)<=N-NB) CYCLE
			K = IWORK(LOCIL+I)
			IF(K-IWORK(LOCIL)+1>NELT_S) THEN
				print *, "SIZE OVERSHOOT AT 889"
				stop
			END IF
			IWORK(LOCJL+K-1) = JL(J)-(N-NB)
			RWORK(LOCL+K-1) = L(J)
			IWORK(LOCIL+I)=IWORK(LOCIL+I)+1
		END DO
	END DO
	
	
    IWORK(LOCIU) = 1
	DO I = 1,NB
		IWORK(LOCIU+I) = IWORK(LOCIU+I-1)
		DO J = IU(N-NB+I),IU(N-NB+I+1)-1
			IF(JU(J)<=N-NB) CYCLE
			K = IWORK(LOCIU+I)
			IF(K-IWORK(LOCIU)+1>NELT_S) THEN
				print *, "SIZE OVERSHOOT AT 890"
				stop
			END IF
			IWORK(LOCJU+K-1) = JU(J)-(N-NB)
			RWORK(LOCU+K-1) = U(J)
			IWORK(LOCIU+I)=IWORK(LOCIU+I)+1
		END DO
	END DO
	END SUBROUTINE SC_Extract
	
  
	  
	  SUBROUTINE SCHUR_MSOLVE(NI,NT, B, X, NELT, IA, JA, A, ISYM, RWORK, IWORK,update_g,update_p)
	  use MPI_VAR 
	  use schur_var
	  USE MPI
      IMPLICIT NONE
      INTEGER NI,NT, NELT, IA(NELT), JA(NELT), ISYM, IWORK(*),overlap,update_p(*),update_g(*)
      REAL(KIND = 8) :: B(NI), A(NELT), RWORK(*), X(NI), B_I(NI),B_B(NIB+NT-NI), Y(NIB+NT-NI)
	  REAL(KIND=8) :: XI(NI),XB(NIB+NT-NI)
	  INTEGER :: SITMAX=3,SITER,NT_B,IERR,i,j,k
	  REAL (kind=8) :: STOL=1e-6,ERR
	  INTEGER, allocatable :: SIWORK(:)
	  real (kind=8), allocatable :: SRWORK(:)
	  INTEGER :: CALLI=0
	  DOUBLE PRECISION :: t1=0.0,t2=0.0,t3=0.0,t4=0.0,t5=0.0
	  SAVE CALLI,t3,t4,t5
	  CALLI =CALLI+1
	 
	 t1 = mPI_WTIME()
	  Do i=1,NI
		if(color(i)<0) then
			B_B(-color(i))=B(i)
		else
			B_I(color(i))=B(i)
		end if
	  end do
	
	 
	 CALL LUI(NI,(/B_i(1:NII),B_b(1:NIB)/),XI,&
					RWORK,IWORK)
					
	 Y(1:NIB) = XI(NII+1:NII+NIB)
	 t2 = MPI_WTIME()
	 t3 = t3 + t2-t1
	 !-----------------
	 
	 NT_B=NIB+NT-NI
	  
	  do i=1,NI
		if(color(i)<0) then
			update_l(-color(i))=i
			XB(-color(i)) = 0.0
		end if
	  end do
	  !XB(1:NIB)=Y(1:NIB)
	      
	  call DSLUCS_WP_SCH(NIB,NT_B,Y,XB,STOL,SITMAX,SITER,ERR,&
	 &                  IERR,0,RWORK(LOCSR),SLENW,IWORK(LOCSI),&
					SLENIW,UPDATE_P,UPDATE_G,HEAD_schu)
	t1 = mPI_WTIME()
	t4 = t4 + t1-t2
	 
	if(rank==0) print *,"SITER:",SITER," CALLI:", CALLI," IERR:", IERR, "Residual:",ERR
	  		
   if(IERR==2) THEN
	!	print *, "-------------------IERR ==2 WARNING--------------"
	!	stop
   END IF	
   
   !--------Communicating external boundary solution_-----
	 
	call update_map_schu(Xb,NIB,NIB+NT-NI,update_p,update_g,HEAD_schu)
   !--------Solving for internal variables
	DO J=1,NIB
		DO K=EI(J),EI(J+1)-1
			B_b(J) = B_b(J) - E(K)*Xb(EJ(K)+NIB)
		END DO
	END DO
	
	CALL LUI(NI,(/B_i(1:NII)	,B_b(1:NIB)/),XI,&
			RWORK,IWORK)
	
	 DO I=1,NI
		if(color(i)<0) then
			X(i) = XI(NII-color(i))
		else
			X(i) = XI(color(i))
		end if
	 END DO
	 
	 t2 = MPI_WTIME()
	 t5 = t5 + t2- t1
 END SUBROUTINE SCHUR_MSOLVE
	  
	  
	  SUBROUTINE DSMVR( N,M, X, Y, NELT, IA, JA, A, ISYM )
	  
!Computing AX->Y. A is of size nxm and x is of size mx1
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      INTEGER N, NELT, IA(NELT), JA(NELT), ISYM
      REAL(KIND = 8) A(NELT), X(M), Y(N)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSMV
!
!
      Y(1:N) = 0.0d0     ! CAREFUL! Whole array operations
!
      DO IROW = 1, N
         DO j = IA(irow),IA(irow+1)-1
			Y(IROW) = Y(IROW) + A(j)*X(JA(j))
		 END DO
      END DO
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DSMV
!
!
      RETURN
      END SUBROUTINE DSMVR
!

SUBROUTINE DSILUS_ROW(N,NELT,IA,JA,A,NL,IL,JL,&
     &                  L,DINV,NU,IU,JU,U,NROW,NCOL)
!!Parallel version of incomplete LDU preconditioner changed by Aman Rusia on 160616
!!N - size of system (originally), NELT, IA, JA, A, NL, IWORK(LOCIL),&
  !        & IWORK(LOCJL), RWORK(LOCL), RWORK(LOCDIN), NU, IWORK(LOCIU),&
  !       & IWORK(LOCJU), RWORK(LOCU), IWORK(LOCNR), IWORK(LOCNC)
!NROW - size 1:N. Since U is stored in SLAP row format. Nrow(i) is number of upper elements in ith column
!NCOL - size 1:N. Since L is stored in SLAP column format. Ncol(i) is number of lower elements in ith column
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      INTEGER N, NELT,IA(NELT),JA(NELT),NL,IL(NL),JL(NL)
      INTEGER NU, IU(NU), JU(NU), NROW(N), NCOL(N)			!Although the size of IL and IU are set as NL and NU, there is enough space as provided in DSLUCS. (check!)
      REAL(KIND = 8) A(NELT), L(NL), DINV(N), U(NU)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSILUS
!
!
      NROW(1:N) = 0
      NCOL(1:N) = 0
!

      DO IROW = 1, N
         IBGN = IA(IROW)+1
         IEND = IA(IROW+1)-1
         IF( IBGN.LE.IEND ) THEN
            DO  I = IBGN, IEND
				IF((JA(I) > N) ) CYCLE
               IF( (JA(I).GT.IROW)) THEN
                  NCOL(JA(I)) = NCOL(JA(I)) + 1
               ELSE
                  NROW(IROW) = NROW(IROW) + 1
               ENDIF
			END DO
         ENDIF
	  END DO
	  
!Forming JU - column of SLAP column format in U. and IL - row of SLAP row format in L. 1st element of U and L are always 0.
      JU(1) = 1
      IL(1) = 1
      DO 40 ICOL = 1, N
         IL(ICOL+1) = IL(ICOL) + NROW(ICOL)
         JU(ICOL+1) = JU(ICOL) + NCOL(ICOL)
         NROW(ICOL) = IL(ICOL)
         NCOL(ICOL) = JU(ICOL)
 40   CONTINUE
 
!Copying values into IU, U, JL and L. Note no diagonal elements are stored in either L or U        
      DO 60 IROW = 1, N
         DINV(IROW) = A(IA(IROW))
         IBGN = IA(IROW)+1
         IEND = IA(IROW+1)-1
         IF( IBGN.LE.IEND ) THEN
            DO 50 I = IBGN, IEND
               ICOL = JA(I)
			   IF((JA(I) > N)) CYCLE
               IF( (IROW.LT.ICOL) ) THEN
                  IU(NCOL(ICOL)) = IROW
                  U(NCOL(ICOL)) = A(I)
                  NCOL(ICOL) = NCOL(ICOL) + 1
               ELSE
                  JL(NROW(IROW)) = ICOL
                  L(NROW(IROW)) = A(I)
                  NROW(IROW) = NROW(IROW) + 1
               ENDIF
 50         CONTINUE
         ENDIF
 60   CONTINUE
	  
!Sorting of row and column number. So the appear incremently in each format.
      DO 110 K = 2, N
         JBGN = JU(K)
         JEND = JU(K+1)-1
         IF( JBGN.LT.JEND ) THEN !Do the sorting of row numbers in i increment order for each column.
            DO 80 J = JBGN, JEND-1
               DO 70 I = J+1, JEND
                  IF( IU(J).GT.IU(I) ) THEN
                     ITEMP = IU(J)
                     IU(J) = IU(I)
                     IU(I) = ITEMP
                     TEMP = U(J)
                     U(J) = U(I)
                     U(I) = TEMP
                  ENDIF
 70            CONTINUE
 80         CONTINUE
         ENDIF
         IBGN = IL(K)
         IEND = IL(K+1)-1
         IF( IBGN.LT.IEND ) THEN !Do the sorting of column numbers in each row.
            DO 100 I = IBGN, IEND-1
               DO 90 J = I+1, IEND
                  IF( JL(I).GT.JL(J) ) THEN
                     JTEMP = JU(I)
                     JU(I) = JU(J)
                     JU(J) = JTEMP
                     TEMP = L(I)
                     L(I) = L(J)
                     L(J) = TEMP
                  ENDIF
 90            CONTINUE
 100        CONTINUE
         ENDIF
 110  CONTINUE
 
!Carrying out forward Sweep
      DO 300 I=2,N  
!Changing lower elements         
         INDX1 = IL(I)
         INDX2 = IL(I+1) - 1
         IF(INDX1 .GT. INDX2) GO TO 200 !No lower elements in the row
         DO 190 INDX=INDX1,INDX2		!Column loop for a row in L
            IF(INDX .EQ. INDX1) GO TO 180 !only one lower element
            INDXR1 = INDX1				
            INDXR2 = INDX - 1
            INDXC1 = JU(JL(INDX))    !JU(JL(INDX)) is just the index of the first element in the same column as INDX present in U
            INDXC2 = JU(JL(INDX)+1) - 1 !Lower most element in a column of U where INDXC1 is the top most.
            IF(INDXC1 .GT. INDXC2) GO TO 180 !No elements in U in the given column 
 160        KR = JL(INDXR1) !Column offset of first element in the current row.
 170        KC = IU(INDXC1) !Row offset of first element in the current column @U
            IF(KR .GT. KC) THEN
               INDXC1 = INDXC1 + 1	
               IF(INDXC1 .LE. INDXC2) GO TO 170
            ELSE IF(KR .LT. KC) THEN
               INDXR1 = INDXR1 + 1
               IF(INDXR1 .LE. INDXR2) GO TO 160
            ELSE IF(KR .EQ. KC) THEN
               L(INDX) = L(INDX) - L(INDXR1)*DINV(KC)*U(INDXC1) !go through transformations. NOTE - EXTRA DINV(KC) because U is also divided by DINV - since this is LDU factorization and not LU.
               INDXR1 = INDXR1 + 1
               INDXC1 = INDXC1 + 1
               IF(INDXR1 .LE. INDXR2 .AND. INDXC1 .LE. INDXC2) GO TO 160
            END IF
 180        L(INDX) = L(INDX)/DINV(JL(INDX)) !Final value stored.
 190     CONTINUE
!Changing upper elements         
 200     INDX1 = JU(I)
         INDX2 = JU(I+1) - 1
         IF(INDX1 .GT. INDX2) GO TO 260
         DO 250 INDX=INDX1,INDX2
            IF(INDX .EQ. INDX1) GO TO 240
            INDXC1 = INDX1
            INDXC2 = INDX - 1
            INDXR1 = IL(IU(INDX))
            INDXR2 = IL(IU(INDX)+1) - 1
            IF(INDXR1 .GT. INDXR2) GO TO 240
 210        KR = JL(INDXR1)
 220        KC = IU(INDXC1)
            IF(KR .GT. KC) THEN
               INDXC1 = INDXC1 + 1
               IF(INDXC1 .LE. INDXC2) GO TO 220
            ELSE IF(KR .LT. KC) THEN
               INDXR1 = INDXR1 + 1
               IF(INDXR1 .LE. INDXR2) GO TO 210
            ELSE IF(KR .EQ. KC) THEN
               U(INDX) = U(INDX) - L(INDXR1)*DINV(KC)*U(INDXC1)
               INDXR1 = INDXR1 + 1
               INDXC1 = INDXC1 + 1
               IF(INDXR1 .LE. INDXR2 .AND. INDXC1 .LE. INDXC2) GO TO 210
            END IF
 240        U(INDX) = U(INDX)/DINV(IU(INDX))
 250     CONTINUE
!Changing diagonal elements  
 260     INDXR1 = IL(I)
         INDXR2 = IL(I+1) - 1
         IF(INDXR1 .GT. INDXR2) GO TO 300
         INDXC1 = JU(I)
         INDXC2 = JU(I+1) - 1
         IF(INDXC1 .GT. INDXC2) GO TO 300
 270     KR = JL(INDXR1)
 280     KC = IU(INDXC1)
         IF(KR .GT. KC) THEN
            INDXC1 = INDXC1 + 1
            IF(INDXC1 .LE. INDXC2) GO TO 280
         ELSE IF(KR .LT. KC) THEN
            INDXR1 = INDXR1 + 1
            IF(INDXR1 .LE. INDXR2) GO TO 270
         ELSE IF(KR .EQ. KC) THEN
            DINV(I) = DINV(I) - L(INDXR1)*DINV(KC)*U(INDXC1)
            INDXR1 = INDXR1 + 1
            INDXC1 = INDXC1 + 1
            IF(INDXR1 .LE. INDXR2 .AND. INDXC1 .LE. INDXC2) GO TO 270
         END IF
!         
 300  CONTINUE
!         
      DINV(1:N) = 1.0d0/DINV(1:N)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DSILUS
!
!
      RETURN
      END SUBROUTINE DSILUS_ROW
!
!
 SUBROUTINE DCGS_SU_PAR(NI,NT, B, X, NELT, IA, JA, A, ISYM, MATVEC,&
     &     MSOLVE, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, &
     &     R, R0, P, Q, U, V1, V2, RWORK, IWORK,UPDATE_G,UPDATE_P,HEAD,COUNTS)
!
!!N, B, X, NELT, IA, JA, A, ISYM, DSMV,&           !!!ITERATION BASICALLY STARTS AFTER THIS. Algorithm after here.
 !         DSLUI, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT,&
 !         RWORK(LOCR), RWORK(LOCR0), RWORK(LOCP),&
 !         RWORK(LOCQ), RWORK(LOCU), RWORK(LOCV1),&
  !        RWORK(LOCV2), RWORK, IWORK
!
	use MPI_VAR
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
	  include 'mpif.h'
      INTEGER NI, NT, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITMAX
      INTEGER ITER, IERR, IUNIT, IWORK(*)
      REAL(KIND = 8) B(NT), X(NT), A(NELT), TOL, ERR, R(NT), R0(NT), P(NT)
      REAL(KIND = 8) Q(NT), U(NT), V1(NT), V2(NT), RWORK(*)
!
      REAL(KIND = 8) DMACH(5)
	  INTEGER ::UPDATE_G(*),UPDATE_P(*),COUNTS(*)
      DATA DMACH(3) / 1.1101827117665D-16 /
	  TYPE(HEADEX) :: HEAD(*)
	  INTEGER :: ICALL = 0
      EXTERNAL MATVEC, MSOLVE
	  SAVE ICALL
	  
	  ICALL = ICALL+1
	  
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DCGS
!
!
	
      ITER = 0
      IERR = 0
!
      IF( NI < 1 ) THEN
         IERR = 3
         RETURN
      ENDIF
!
      TOLMIN = 5.0d2*DMACH(3)
      IF( TOL < TOLMIN ) THEN
         TOL  = TOLMIN
         IERR = 4
      ENDIF
!  
 
	
	 
      
      CALL MATVEC(NI, NT, X, R, NELT, IA, JA, A, ISYM,UPDATE_P,update_g,head) !DSMV Matvec computes AX -> R. MSOLVE solves the system MR = V1, where V1 is residual AX - R from previous step, M = LDU - the ldu decomposition from preconditioning.
!  

      V1(1:NI)  = R(1:NI) - B(1:NI)
	
		  CALL MSOLVE(NI,NT, V1, R, NELT, IA, JA, A, ISYM, RWORK, IWORK,update_g,update_p)!DSLUI - Solves for R in LDU.R = B1 Directly parallelizable
!   
!IF(ICALL==2) print *, rank,' R---',R(1:NI)
!IF(ICALL==2) stop
      ISDCGS = 0
      ITOL   = 2
      IF(ITER == 0) THEN
	
         CALL MSOLVE(NI,NT, B, V2, NELT, IA, JA, A, ISYM, RWORK, IWORK,update_g,update_p)

         BNRM = SQRT(DOT_PROD_PAR(V2,V2,NI))
      ENDIF
!         
      ERR = SQRT(DOT_PROD_PAR(R,R,NI))/BNRM
!     


	if(rank==0) then
      IF(IUNIT /= 0) THEN
         IF( ITER == 0 ) THEN
            WRITE(IUNIT,1000) NT, ITOL
         ENDIF
         WRITE(IUNIT,1010) ITER, ERR, AK, BK
      ENDIF
	end if
	
      IF(ERR <= TOL) ISDCGS = 1
!         
      IF(ISDCGS /= 0) GO TO 200
!
!
      FUZZ = DMACH(3)**2
!
      R0(1:NI) = R(1:NI)
!
      RHONM1 = 1.0D0
!         
!         
      DO K=1,ITMAX
         ITER = K
!
         RHON = DOT_PROD_PAR(R0,R,NI)
!
         IF( ABS(RHONM1) < FUZZ ) GO TO 998
!
         BK = RHON/RHONM1
!

         IF( ITER.EQ.1 ) THEN
            U(1:NI) = R(1:NI)
            P(1:NI) = R(1:NI)
         ELSE
            U(1:NI)  = R(1:NI) + BK*Q(1:NI)
            V1(1:NI) = Q(1:NI) + BK*P(1:NI)
            P(1:NI)  = U(1:NI) + BK*V1(1:NI)
         ENDIF
!         
         CALL MATVEC(NI, NT, P, V2, NELT, IA, JA, A, ISYM,UPDATE_P,update_g,head)
         CALL MSOLVE(NI,NT, V2, V1, NELT, IA, JA, A, ISYM, RWORK, IWORK,update_g,update_p)
!         
!IF(ICALL==2) print *, rank,' V1','---',V1(1:NI)

         SIGMA = DOT_PROD_PAR(R0,V1,NI)

         IF( ABS(SIGMA) < FUZZ ) GO TO 999
!         
         AK = RHON/SIGMA
         AKM = -AK
!         
         Q(1:NI)  = U(1:NI) + AKM*V1(1:NI)
!         
         V1(1:NI) = U(1:NI) + Q(1:NI)
!         
         X(1:NI)  = X(1:NI) + AKM*V1(1:NI)
!           
           
         CALL MATVEC(NI, NT, V1, V2, NELT, IA, JA, A, ISYM,UPDATE_P,update_g,head)
         CALL MSOLVE(NI,NT, V2, V1, NELT, IA, JA, A, ISYM, RWORK, IWORK,update_g,update_p)
		
!IF(ICALL==2) print *, rank,' V1','---',V1(1:NI)
!IF(ICALL==2) stop
	
         R(1:NI) = R(1:NI) + AKM*V1(1:NI)
!         

         ISDCGS = 0
         ITOL   = 2
!         
         ERR = SQRT(DOT_PROD_PAR(R,R,NI))/BNRM
!        
		 if(rank==0) then
			 IF(IUNIT /= 0) THEN
				WRITE(IUNIT,1010) ITER, ERR, AK, BK
			 ENDIF
		 end if
		 
         IF(ERR <= TOL) ISDCGS = 1
!         

         IF(ISDCGS /= 0) GO TO 200
!

         RHONM1 = RHON
		 
	  END DO
 1000 FORMAT('BiConjugate Gradient Squared Preconditioned with SCHUR COMPLEMENT METHOD for ',&
          'N, ITOL = ',I5, I5,&
          /' ITER','   Error Estimate','            Alpha',&
          '             Beta')
 1010 FORMAT(1X,I4,1X,E16.7,1X,E16.7,1X,E16.7)
!         
      ITER = ITMAX + 1
      IERR = 2
 200  RETURN
!
 998  IERR = 5
      RETURN
!
 999  IERR = 6
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DCGS
!
!
      RETURN
      END SUBROUTINE DCGS_SU_PAR
	  
	  
	  subroutine Initialize_Exmap_schu(head,IA,JA,A,update_p,update_g,NIB,update_l)


	use mpi_var
	implicit none
	type(HEADEX) :: head(0:w_size-1)
	type(Exel),pointer :: point
	integer :: IA(*),JA(*),NIB,update_p(*),update_g(*),update_l(*)
	real(kind=8) :: A(*)
	integer :: i,j, temp(0:w_size-1)
	
	do i=0,w_size-1
		NULLIFY(head(i)%next)
		NULLIFY(head(i)%curr)
		NULLIFY(head(i)%endp)
		NULLIFY(head(i)%ende)	
	end do
	do i = 1, NIB
		!Loop over IA (each internal element) to find the external connections
		!Ensuring the element is sent to a neighboring processor only once
		temp = 0
		
		do j = IA(i),IA(i+1)-1
			if(temp(update_p(JA(j)))==0) then
				temp(update_p(JA(j)))=1
				point=>head(update_p(JA(j)))%endp
				if(.NOT. associated(point)) then
					allocate(head(update_p(JA(j)))%endp)
					head(update_p(JA(j)))%next=>head(update_p(JA(j)))%endp
					point=>head(update_p(JA(j)))%endp		
				else
					allocate(point%next)
					point=>point%next
				end if
				nullify(point%next)
				point%loc=update_l(i)
				point%sloc=i
				head(update_p(JA(j)))%endp=>point
			end if
		end do
	end do
	do i=0,w_size-1
		head(i)%ende=>head(i)%endp
		head(i)%curr=>head(i)%next
	end do
end subroutine Initialize_Exmap_schu

  SUBROUTINE DSLUCS_WP_SCH(NI,NT,B,X,TOL,ITMAX,ITER,ERR,&
     &                  IERR,IUNIT,RWORK,LENW,IWORK,LENIW,UPDATE_P,UPDATE_G,HEAD)
!---------------------------------------------------------
!This the parallel version of DSLUCS. Changed on 102616. Author - Aman Rusia
!--------------------------------------------------------
!NL - Number of elements in internal set
!NT - Number of elements in total = internal + update set
!B - Right hand side of the system - processor dependent size (NT)
!X - It is the final solution - processor dependent size (NT). X should be guess solution 
!NELT - size of A, IA and JA. Assign NELT_MAX+1 to them
!IA,JA,A - irn ,icn,co distributed
!    &                  CLOSUR,NMAXIT,ITERU,ERR,IERR,&
!   &                  IUNIT
!RWORK - size LENW.  Work array. Originally LENW = max(1000+mnz+38*MAXNUM_ELEM * NEQ, 9*MaxNum_Conx+1000) where mnz = max size of CO = NEQ*NEQ*(MaxNum_elements+2*MaxNum_conx). New LENW = 1000+mnz+38*MAXNUM_ELEM * NEQ, 9*MaxNum_Conx+1000
!IWORK - size LENIW. jvect. Work array. Originally LENIW = 100+mnz+5*MAXNUM_ELEM*NEQ. IWORK(1:10) stores Upper,Lower triangular matrices and Diagonal inverse of preconditioner, and their column and row information
!new the above sizes are changed according to new MAXNUM_ELEM = MAXNUM_ELEM/size. Note that MaxNum_Conx should remain intact since it could be fully inside a processor.
!RWORK also stores values of elements in L, U and Dinv after preconditioning. IWORK stores their respective row and column indices
!IWORK not needed in non preconditioned dslucs
	 use MPI_VAR
	 IMPLICIT REAL(KIND = 8)(A-H,O-Z)
	 include 'mpif.h'
!
      INTEGER :: NI, NT, ISYM, ITOL, ITMAX, ITER
      INTEGER :: IERR, IUNIT, LENW, IWORK(LENIW), LENIW
      REAL(KIND = 8) :: B(NT), X(NT), TOL, ERR, RWORK(LENW)
	 ! EXTERNAL :: DSMV_PAR
	  INTEGER :: UPDATE_P(*),UPDATE_G(*)
	  TYPE(HEADEX) :: HEAD(*)
	  DOUBLE PRECISION :: t1,t2=0.0
	  INTEGER :: ICALL=0
	  SAVE t2,ICALL
	  ICALL=ICALL+1
!
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSLUCS
!
!
	 call MPI_COMM_RANK ( MPI_COMM_WORLD,rank,IERR)
	 call MPI_COMM_SIZE ( MPI_COMM_WORLD, w_size, IERR)
      ISYM = 0
      ITOL = 2
!
      IERR = 0
      IF( NI.LT.1) THEN
         IERR = 3
         RETURN
      ENDIF
     
!--------212         
 
      LOCR   = 1
      LOCR0  = LOCR + NT
      LOCP   = LOCR0 + NT
      LOCQ   = LOCP + NT
      LOCU   = LOCQ + NT
      LOCV1  = LOCU + NT
      LOCV2  = LOCV1 + NT
      LOCW   = LOCV2 + NT

     ! CALL DCHKW( 'DSLUCS', LOCIW, LENIW, 0, LENW, IERR, ITER, ERR ) !Check if LOCW and LOCIW exceed the limit or not. LOCW has been set as zero in non preconditioned version
      IF( IERR.NE.0 ) RETURN
!
!
	 t1 = MPI_WTIME()
      CALL DCGS_WP_SCH(NI, NT, B, X, ISYM, DSMV_PAR_MAP_schu,&
           ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT,&
          RWORK(LOCR), RWORK(LOCR0), RWORK(LOCP),&
          RWORK(LOCQ), RWORK(LOCU), RWORK(LOCV1),&
          RWORK(LOCV2), RWORK, IWORK, UPDATE_G,UPDATE_P,HEAD )
	 t2 = t2+MPI_WTIME()-t1
		
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DSLUCS
!
!
      RETURN
      END SUBROUTINE DSLUCS_WP_SCH
	  
	  SUBROUTINE DCGS_WP_SCH(NI, NT, B, X, ISYM, MATVEC,&
     &      ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, &
     &     R, R0, P, Q, U, V1, V2, RWORK, IWORK,UPDATE_G,UPDATE_P,HEAD)
!
!!N, B, X, NELT, IA, JA, A, ISYM, DSMV,&           !!!ITERATION BASICALLY STARTS AFTER THIS. Algorithm after here.
 !         DSLUI, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT,&
 !         RWORK(LOCR), RWORK(LOCR0), RWORK(LOCP),&
 !         RWORK(LOCQ), RWORK(LOCU), RWORK(LOCV1),&
  !        RWORK(LOCV2), RWORK, IWORK
!
	use MPI_VAR
	USE MPI
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
	  
      INTEGER :: NI, NT,  ISYM, ITOL, ITMAX
      INTEGER :: ITER, IERR, IUNIT, IWORK(*)
      REAL(KIND = 8) :: B(NT), X(NT),  TOL, ERR, R(NT), R0(NT), P(NT)
      REAL(KIND = 8) :: Q(NT), U(NT), V1(NT), V2(NT), RWORK(*)
!
      REAL(KIND = 8) :: DMACH(5)
	  INTEGER ::UPDATE_G(*),UPDATE_P(*)
	  TYPE(HEADEX) :: HEAD(*)
	  DOUBLE PRECISION :: t1=0.0,t2=0.0,t3=0.0,t4=0.0,t5=0.0

      DATA DMACH(3) / 1.1101827117665D-16 /

      EXTERNAL MATVEC, MSOLVE
	  SAVE t2
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DCGS
!
!
	
      ITER = 0
      IERR = 0
!
      IF( NI < 1 ) THEN
         IERR = 3
         RETURN
      ENDIF
!
      TOLMIN = 5.0d2*DMACH(3)
      IF( TOL < TOLMIN ) THEN
         TOL  = TOLMIN
         IERR = 4
      ENDIF
!  
 
      CALL MATVEC(NI, NT, X, R, NELT, IA, JA, A, ISYM,UPDATE_P,update_g,head,RWORK,IWORK) !DSMV Matvec computes AX -> R. MSOLVE solves the system MR = V1, where V1 is residual AX - R from previous step, M = LDU - the ldu decomposition from preconditioning.
!  
      V1(1:NI)  = R(1:NI) - B(1:NI)
!        
     ! CALL MSOLVE(N, V1, R, NELT, IA, JA, A, ISYM, RWORK, IWORK)!DSLUI - Solves for R in LDU.R = B1 Directly parallelizable
!        

	  R(1:NI) = V1(1:NI)
      ISDCGS = 0
      ITOL   = 2
      IF(ITER == 0) THEN
         !CALL MSOLVE(N, B, V2, NELT, IA, JA, A, ISYM, RWORK, IWORK)
		 V2(1:NI) = B(1:NI)
		 t1=MPI_WTIME()
         BNRM = SQRT(DOT_PROD_PAR(V2,V2,NI))
		 t2 = t2+MPI_WTIME() - t1
      ENDIF
!         
		 t1=MPI_WTIME()
      ERR = SQRT(DOT_PROD_PAR(R,R,NI))/BNRM
		 t2 = t2+MPI_WTIME() - t1
!     

!------------Richardson's iteration--remove---

      DO K=1,ITMAX
		ITER = K
		X(1:NI) = X(1:NI) - R(1:NI)
		CALL MATVEC(NI, NT, R, V1, NELT, IA, JA, A, ISYM,UPDATE_P,update_g,head,RWORK,IWORK) !DSMV Matvec computes AX -> R. MSOLVE solves the system MR = V1, where V1 is residual AX - R from previous step, M = LDU - the ldu decomposition from preconditioning.
		R(1:NI) = R(1:NI) - V1(1:NI)
		ERR = SQRT(DOT_PROD_PAR(R,R,NI))/BNRM
		 IF(ERR <= TOL) ISDCGS = 1
!         
		IF(ISDCGS /= 0) GO TO 200
	  END DO
      GO TO 313
!-----------------------------

	if(rank==0) then
      IF(IUNIT /= 0) THEN
         IF( ITER == 0 ) THEN
            WRITE(IUNIT,1000) NT, ITOL
         ENDIF
         WRITE(IUNIT,1010) ITER, ERR, AK, BK
      ENDIF
	end if
	
      IF(ERR <= TOL) ISDCGS = 1
!         
      IF(ISDCGS /= 0) GO TO 200
!
!
      FUZZ = DMACH(3)**2
!
      R0(1:NI) = R(1:NI)
!
      RHONM1 = 1.0D0
!         
!         
      DO K=1,ITMAX
         ITER = K
!
		 t1=MPI_WTIME()
         RHON = DOT_PROD_PAR(R0,R,NI)
		 t2 = t2 + MPI_WTIME() - t1
!
		
         !IF( ABS(RHONM1) < FUZZ ) GO TO 998
!
         BK = RHON/RHONM1
!

         IF( ITER.EQ.1 ) THEN
            U(1:NI) = R(1:NI)
            P(1:NI) = R(1:NI)
         ELSE
            U(1:NI)  = R(1:NI) + BK*Q(1:NI)
            V1(1:NI) = Q(1:NI) + BK*P(1:NI)
            P(1:NI)  = U(1:NI) + BK*V1(1:NI)
         ENDIF
!         
         CALL MATVEC(NI, NT, P, V2, NELT, IA, JA, A, ISYM,UPDATE_P,update_g,head,RWORK,IWORK)
      
		!CALL MSOLVE(N, V2, V1, NELT, IA, JA, A, ISYM, RWORK, IWORK)
!         
 
		 V1(1:NI) = V2(1:NI)
		 
		 t1=MPI_WTIME()
         SIGMA = DOT_PROD_PAR(R0,V1,NI)
		 t2 = t2 + MPI_WTIME() - t1

         !IF( ABS(SIGMA) < FUZZ ) GO TO 999
!         
         AK = RHON/SIGMA
         AKM = -AK
!         
         Q(1:NI)  = U(1:NI) + AKM*V1(1:NI)
!         
         V1(1:NI) = U(1:NI) + Q(1:NI)
!         
         X(1:NI)  = X(1:NI) + AKM*V1(1:NI)
!                      
         CALL MATVEC(NI, NT, V1, V2, NELT, IA, JA, A, ISYM,UPDATE_P,update_g,head,RWORK,IWORK)
        ! CALL MSOLVE(N, V2, V1, NELT, IA, JA, A, ISYM, RWORK, IWORK)
		
		 V1 = V2
         R(1:NI) = R(1:NI) + AKM*V1(1:NI)
!         
	!	  CALL MATVEC(NI, NT, X, R, NELT, IA, JA, A, ISYM,UPDATE_P,update_g,head,RWORK,IWORK)
	!	 R(1:NI) = R(1:NI)-B(1:NI)
         ISDCGS = 0
         ITOL   = 2
!         
		 t1=MPI_WTIME()
         ERR = SQRT(DOT_PROD_PAR(R,R,NI))/BNRM
		 t2 = t2 + MPI_WTIME() - t1
!        
		 if(rank==0) then
			 IF(IUNIT /= 0) THEN
				WRITE(IUNIT,1010) ITER, ERR, AK, BK
			 ENDIF
		 end if
		 
         IF(ERR <= TOL) ISDCGS = 1
!         
         IF(ISDCGS /= 0) GO TO 200
!
         RHONM1 = RHON
	  END DO
 1000 FORMAT('BiConjugate Gradient Squared without Preconditioning for ',&
          'N, ITOL = ',I5, I5,&
          /' ITER','   Error Estimate','            Alpha',&
          '             Beta')
 1010 FORMAT(1X,I4,1X,E16.7,1X,E16.7,1X,E16.7)
!         
 313  ITER = ITMAX + 1
      IERR = 2
 200  RETURN
!
 998  IERR = 5
      RETURN
!
 999  IERR = 6
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DCGS
!
!
      RETURN
      END SUBROUTINE DCGS_WP_SCH
	
	  
	  SUBROUTINE DCGSTB_WP_SCH(NI, NT, B, X, ISYM, MATVEC,&
     &      ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, &
     &     R, R0, P, Q, U, V1, V2, RWORK, IWORK,UPDATE_G,UPDATE_P,HEAD)
!
!!N, B, X, NELT, IA, JA, A, ISYM, DSMV,&           !!!ITERATION BASICALLY STARTS AFTER THIS. Algorithm after here.
 !         DSLUI, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT,&
 !         RWORK(LOCR), RWORK(LOCR0), RWORK(LOCP),&
 !         RWORK(LOCQ), RWORK(LOCU), RWORK(LOCV1),&
  !        RWORK(LOCV2), RWORK, IWORK
!
	use MPI_VAR
	USE MPI
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
	  
      INTEGER :: NI, NT,  ISYM, ITOL, ITMAX
      INTEGER :: ITER, IERR, IUNIT, IWORK(*)
      REAL(KIND = 8) :: B(NT), X(NT),  TOL, ERR, R(NT), R0(NT), P(NT)
      REAL(KIND = 8) :: Q(NT), U(NT), V1(NT), V2(NT), RWORK(*)
!
      REAL(KIND = 8) :: DMACH(5)
	  INTEGER ::UPDATE_G(*),UPDATE_P(*)
	  TYPE(HEADEX) :: HEAD(*)
	  DOUBLE PRECISION :: t1=0.0,t2=0.0,t3=0.0,t4=0.0,t5=0.0

      DATA DMACH(3) / 1.1101827117665D-16 /

      EXTERNAL MATVEC, MSOLVE
	  SAVE t2
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DCGS
!
!
	
     
      ITER = 0
      IERR = 0
!
      IF( NI.LT.1 ) THEN
         IERR = 3
         RETURN
      ENDIF
      TOLMIN = 5.0d2*DMACH(3)
      IF(TOL.LT.TOLMIN) THEN
         TOL = TOLMIN
         IERR = 4
      ENDIF
!         
	 R(1:NI) = B(1:NI)
!
      ERR1 = 0.0d0
	  ERR1 = DOT_PROD_PAR(X,X,NI)
!
!
!
      IF(err1.NE.0.0d0) THEN
         CALL MATVEC(NI, NT, X, R, NELT, IA, JA, A, ISYM,UPDATE_P,update_g,head,RWORK,IWORK)
!
         ERR2 = 0.0D0
	  	 R(1:NI)  = B(1:NI)-R(1:NI)
		 ERR2  = DOT_PROD_PAR(R,R,NI)
         ERR2 = SQRT(ERR2)
		 IBCGST=0
         IF(ERR2.LE.TOL) IBCGST = 1
         IF(IBCGST.NE.0) GO TO 200
      END IF
!
!
!
      RHONM1 = 1.0D0
      ALPHA  = 1.0D0
      OMEGA  = 1.0D0
      BETA   = 0.0d0
!
!
!         
      IBCGST = 0
      ITOL   = 2
      BNRM   = 0.0d0
	  
      BNRM = DOT_PROD_PAR(B,B,NI)
      BNRM = SQRT(BNRM)
!
      ERR = 0.0D0
      ERR = DOT_PROD_PAR(R,R,NI)
      ERR = SQRT(ERR)/BNRM
!         
      IF(IUNIT.NE.0) THEN
         IF(ITER.EQ.0) THEN
            if(rank==0) WRITE(IUNIT,1000) NI, ITOL
         ENDIF
         if(rank==0) WRITE(IUNIT,1010) ITER, ERR, Alpha, BETA, OMEGA
      ENDIF
      IF(ERR.LE.TOL) IBCGST = 1
!         
      IF(IBCGST.NE.0) GO TO 200
!
!
      FUZZ = DMACH(3)**2
      DO 14 I = 1, NI
         R0(I) = R(I)
   14 CONTINUE
!         
!
!         
      DO 100 K=1,ITMAX
         ITER = K
!
         DDOT = 0.0d0
		 DDOT = DOT_PROD_PAR(R0,R,NI)
         RHON = DDOT

         BETA = (RHON/RHONM1)*(ALPHA/OMEGA)
!
         IF( ITER.EQ.1 ) THEN
            DO 20 I = 1, NI
               P(I)  = R(I)
   20       CONTINUE
         ELSE
            DO 25 I = 1, NI
               U(I) = P(I)-OMEGA*V1(I)
   25       CONTINUE
            DO 26 I = 1, NI
               P(I) = R(I)+BETA*U(I)
   26       CONTINUE
         END IF
!         
         !CALL MSOLVE(N, P, V2, NELT, IA, JA, A, ISYM, RWORK, IWORK)
		 V2(1:NI) = P(1:NI)
         CALL MATVEC(NI, NT, V2, V1, NELT, IA, JA, A, ISYM,UPDATE_P,update_g,head,RWORK,IWORK)

         DDOT = 0.0D0
		 DDOT = DOT_PROD_PAR(R0,V1,NI)
         SIGMA = DDOT

         ALPHA = RHON/SIGMA
!
         DO 35 I=1,NI
            Q(I) = R(I)-ALPHA*V1(I)
   35    CONTINUE         
!
         ERR = 0.0d0
		 ERR = DOT_PROD_PAR(Q,Q,NI)
         ERR = SQRT(ERR)/BNRM
!         
         IF(ERR.LE.TOL) IBCGST = 1
!
         DO 42 I5=1,NI
            X(I5) = X(I5)+ALPHA*V2(I5)
   42    CONTINUE
!         
         IF(IBCGST.NE.0) THEN 
            IF(IUNIT.NE.0) THEN
               if(rank==0) WRITE(IUNIT,1010) ITER, ERR, Alpha, BETA, OMEGA
            ENDIF
            GO TO 200
         END IF       
!                      
         !CALL MSOLVE(N, Q, V2, NELT, IA, JA, A, ISYM, RWORK, IWORK)
		 V2(1:NI) = Q(1:NI)
         CALL MATVEC(NI, NT, V2, U, NELT, IA, JA, A, ISYM,UPDATE_P,update_g,head,RWORK,IWORK)
!
         SUM1=0.0D0 
         SUM2=0.0D0 
         SUM1 = DOT_PROD_PAR(U,Q,NI)
         SUM2 = DOT_PROD_PAR(U,U,NI)
         OMEGA = SUM1/SUM2

          DO 60 I = 1,NI
             X(I) = X(I)+OMEGA*V2(I)
   60     CONTINUE
!
         DO 65 I = 1,NI
            R(I) = Q(I) - OMEGA*U(I)
   65    CONTINUE
!         
         IBCGST = 0
         ITOL   = 2
         ERR    = 0.0D0
         ERR = DOT_PROD_PAR(R,R,NI)
         ERR = SQRT(ERR)/BNRM
!         
         IF(IUNIT .NE. 0) THEN
            if(rank==0) WRITE(IUNIT,1010) ITER, ERR, Alpha, BETA, OMEGA
         ENDIF
         IF(ERR .LE. TOL) IBCGST = 1
!         
         IF(IBCGST.NE.0) GO TO 200
!
         RHONM1 = RHON
 100  CONTINUE
 1000 FORMAT(' Preconditioned BiConjugate Gradient Stabilized for ',&
          'N, ITOL = ',I5, I5,&
          /' ITER','   Error Estimate','            Alpha',&
          '             Beta','             Omega')
 1010 FORMAT(1X,I4,1X,1pE16.7,1X,1pE16.7,1X,1pE16.7,1X,1pE16.7)
!         
      ITER = ITMAX + 1
      IERR = 2
 200  RETURN
!
 998  IERR = 5
      RETURN
!
 999  IERR = 6
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DCGSTB
!
!
      RETURN
      END SUBROUTINE DCGSTB_WP_SCH
	 
	 
	  subroutine update_map_schu(X,NI,NT,update_p,update_g,head)
	use MPI_VAR
	implicit none
	include 'mpif.h'
	
	
	REAL (KIND=8), dimension(*) :: X
	integer :: NI,NT, ierr, update_p(*), update_g(*)
	integer :: i,j, l,lp
	integer :: stats(MPI_STATUS_SIZE,4*w_size)
	integer, dimension((NT-NI)+(NT-NI)*w_size) :: request,outarray
	type(HEADEX) :: HEAD(0:w_size-1)
	type(EXEL),pointer :: point 	
	integer :: calle=1,flag,outcount
	REAL (KIND=8), ALLOCATABLE :: SEND(:,:),RECV(:,:)
	INTEGER, ALLOCATABLE :: SENDI(:,:),RECVI(:,:),ms(:)
	INTEGER :: max_size
	logical :: flaglogic(4*w_size)
	SAVE CALLE 
	
	
	
	!Finding max size to be sent to any proc
	max_size=-1
	l=0
	DO i =0,w_size-1
		point=>HEAD(i)%next
		do while(associated(point))
			l=l+1
			point=>point%next
		end do
		max_size = max(max_size,l)
	END DO
	allocate(SEND(max_size,0:w_size-1))
	allocate(SENDI(max_size,0:w_size-1))
	
	allocate(ms(0:w_size-1))
	!Finding max size to be recieved from any proc
	ms(0:w_size-1) = 0
	Do i = 1, NT-NI
		ms(update_p(i)) = ms(update_p(i)) + 1
	END DO
	allocate(RECV(maxval(ms(0:w_size-1)),0:w_size-1))
	allocate(RECVI(maxval(ms(0:w_size-1)),0:w_size-1))

	
	l=1
	do i=0,w_size-1
		
		!Post recv request
		if(ms(i) > 0) THEN
			call MPI_IRECV(RECV(1:ms(i),i),ms(i),&
					MPI_REAL8,i,0,MPI_COMM_WORLD,request(l), ierr )
			l=l+1
			call MPI_IRECV(RECVI(1:ms(i),i),ms(i),&
					MPI_INT,i,1,MPI_COMM_WORLD,request(l), ierr )
			l=l+1
		END IF
		
	end do
	lp=l
	do i = 0, w_size-1
		!Post the send requests
		j=0
		point=>HEAD(i)%next
		do while(associated(point))
			j=j+1
			SEND(j,i) = X(point%sloc)
			SENDI(j,i) = point%loc
			point=>point%next
		end do
		if(j>0) then
			call MPI_ISEND(SEND(1:j,i),j,&
				MPI_REAL8,i,0,MPI_COMM_WORLD,request(l), ierr)
			l=l+1
			call MPI_ISEND(SENDI(1:j,i),j,&
				MPI_INT,i,1,MPI_COMM_WORLD,request(l), ierr)
			l=l+1
		end if
	end do
	
	
	if(l-1>size(request)) then
		if (rank==0) print *, "SIZE of request and status is less in update_map. ERROR!"
		stop
	end if
	if(l>1) call MPI_Waitall(l-1,request,MPI_STATUS_IGNORE,ierr)
		
	DO i=1,NT-NI
		DO J=1,MS(update_p(i))
			IF(RECVI(J,update_p(i))==update_g(i)) THEN
				X(NI+i) = RECV(J,update_p(i))
				EXIT
			END IF
		END DO
		if(J>MS(update_p(i))) THEN
			print *, "ERROR COMM 889XX"
			STOP
		End if
	END DO
	
	deallocate(SEND)
	deallocate(SENDI)
	deallocate(RECV)
	deallocate(RECVI)
	deallocate(ms)
	calle = calle+1
end subroutine update_map_schu


 SUBROUTINE DSMV_PAR_MAP_schu( NI, NT, X, Y, NELT, IA, JA, A, ISYM, UPDATE_P,update_g,head,RWORK,IWORK)
!DSMV = MATVEC
!Parallel version of the Matrix Vector Product happens after update
!
	use MPI_VAR
	USE SCHUR_VAR, ONLY:E,EJ,EI
	USE MPI
      IMPLICIT NONE
      INTEGER :: NI,NT, NELT, IA(NELT), JA(NELT), ISYM,I,J,K
	  INTEGER:: update_p(*),update_g(*),IWORK(*)
      REAL(KIND = 8) :: A(NELT), X(NT), Y(NT),B_b(NT),RWORK(*)
	  TYPE(HEADEX) :: head(0:w_size-1)
	  INTEGER :: ICALL=0
	  DOUBLE PRECISION :: t1=0.0,t2=0.0,t3=0.0,t4=0.0,t5=0.0
	  SAVE ICALL,t1,t2,t3
	  ICALL = ICALL+1
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSMV
!
	t1 = MPI_WTIME()
	call update_map_schu(X,NI,NT,update_p,update_g,head)
	t2 = MPI_WTIME()
    Y(1:NT) = 0.0d0     ! CAREFUL! Whole array operations
	
	DO J=1,NI
		DO K=EI(J),EI(J+1)-1
			Y(J) = Y(J) + E(K)*X(EJ(K)+NI)
		END DO
	END DO
	CALL LUIS(NI,Y,B_b,RWORK,IWORK)	!Return solution in B_b
	
	!Final addition I*X + S-1*Eij*X 
	Y = X + B_b
	t3 = MPI_WTIME()
	t4 = t4 + t2-t1
	t5 = t5 + t3-t2
	  
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DSMV
!
!
	
      RETURN
      END SUBROUTINE DSMV_PAR_MAP_schu



SUBROUTINE LUI(N,B,X,RWORK,IWORK)
	USE SCHUR_VAR, ONLY: NELT_max
	USE mpi_var
	USE MPI
	IMPLICIT NONE
	INTEGER :: N,IWORK(*)
	REAL(KIND=8) :: B(*),X(*),RWORK(*)
	INTEGER :: LOCIL,LOCJL,LOCL,LOCIU,LOCJU,LOCU,LOCD
	INTEGER :: IROW,JBGN,JEND,J,ICOL,JJUU
    INTEGER :: ICALL=0
	SAVE ICALL
	ICALL = ICALL+1
      LOCIL  = IWORK(1)
      LOCJL  = IWORK(2)
      LOCIU  = IWORK(3)
      LOCJU  = IWORK(4)
      LOCL   = IWORK(5)
      LOCD = IWORK(6)
      LOCU   = IWORK(7)
!         
      X(1:N) = B(1:N)  ! CAREFUL! Whole array operations
!         
!         
      DO IROW = 2, N
         JBGN = IWORK(LOCIL+IROW-1)
         JEND = IWORK(LOCIL+IROW)-1
         IF( JBGN.LE.JEND ) THEN
            DO  J = JBGN, JEND
               X(IROW) = X(IROW)-RWORK(LOCL+J-1)*X(IWORK(LOCJL+J-1))
			END DO 
         ENDIF
	  END DO
      DO ICOL = N, 1, -1
          JBGN = IWORK(LOCIU+ICOL-1)
         JEND = IWORK(LOCIU+ICOL)-1
         IF( JBGN.LE.JEND ) THEN
            DO J = JBGN+1, JEND
			   JJUU = IWORK(LOCJU+J-1)
               X(ICOL) = X(ICOL)-RWORK(LOCU+J-1)*X(JJUU) !Note: U is already divided by the diagonal of the same row.
			END DO
         ENDIF
		 X(ICOL)=X(ICOL)/RWORK(LOCU+JBGN-1)
	  END DO
END SUBROUTINE LUI


SUBROUTINE LUIS(N,B,X,RWORK,IWORK)

	USE SCHUR_VAR, ONLY: NELT_S
	IMPLICIT NONE
	INTEGER :: N,IWORK(*)
	REAL(KIND=8) :: B(*),X(*),RWORK(*)
	INTEGER :: LOCIL,LOCJL,LOCL,LOCIU,LOCJU,LOCU,LOCD
	INTEGER :: IROW,JBGN,JEND,J,ICOL,JJUU

      LOCIL  = IWORK(1)
      LOCJL  = IWORK(2)
      LOCIU  = IWORK(3)
      LOCJU  = IWORK(4)
      LOCL   = IWORK(5)
      LOCD   = IWORK(6)
      LOCU   = IWORK(7)
!         
      X(1:N) = B(1:N)  ! CAREFUL! Whole array operations
!        
      DO IROW = 2, N
         JBGN = IWORK(LOCIL+IROW-1)
         JEND = IWORK(LOCIL+IROW)-1
         IF( JBGN.LE.JEND ) THEN
            DO  J = JBGN, JEND
               X(IROW) = X(IROW)-RWORK(LOCL+J-1)*X(IWORK(LOCJL+J-1))
			END DO 
         ENDIF
	  END DO
!         

      
      DO ICOL = N, 1, -1
          JBGN = IWORK(LOCIU+ICOL-1)
         JEND = IWORK(LOCIU+ICOL)-1
         IF( JBGN.LE.JEND ) THEN
            DO J = JBGN+1, JEND
			   JJUU = IWORK(LOCJU+J-1)
               X(ICOL) = X(ICOL)-RWORK(LOCU+J-1)*X(JJUU) !Note: U is already divided by the diagonal of the same row.
			END DO
         ENDIF
		 X(ICOL)=X(ICOL)/RWORK(LOCU+JBGN-1)
	  END DO
	
END SUBROUTINE LUIS

	


SUBROUTINE ILUT(N,NELT,IA,JA,A,NL,IL,JL,&
     &                  L,W,NU,IU,JU,U,IW,JW,TOL,P,NI)
!!Parallel version of incomplete LDU preconditioner changed by Aman Rusia on 160616
!!N - size of system (originally), NELT, IA, JA, A, NL, IWORK(LOCIL),&
  !        & IWORK(LOCJL), RWORK(LOCL), RWORK(LOCDIN), NU, IWORK(LOCIU),&
  !       & IWORK(LOCJU), RWORK(LOCU), IWORK(LOCNR), IWORK(LOCNC)
!NROW - size 1:N. Since U is stored in SLAP row format. Nrow(i) is number of upper elements in ith column
!NCOL - size 1:N. Since L is stored in SLAP column format. Ncol(i) is number of lower elements in ith column
       USE MPI
	   USE MPI_VAR
	   IMPLICIT NONE
      INTEGER :: N, NELT,IA(N+1),JA(NELT),NL,IL(N+1),JL(NL),P,NI
      INTEGER :: NU, IU(N+1), JU(NU),pattern(N)			!Although the size of IL and IU are set as NL and NU, there is enough space as provided in DSLUCS. (check!)
      REAL(KIND = 8)  :: A(NELT), L(NL), U(NU), W(N), TOL,tempW
	  INTEGER :: IROW,ICOL, IW(N),JW(N),ICOUNT,JCOUNT,NELT_L,NELT_U
	  INTEGER :: EN,ST,I,J,K,LN,poss,temp
	  REAL (KIND=8) :: norm(1:N)
	  DOUBLE PRECISION :: t1=0.0,t2=0.0,t3=0.0,t4=0.0,t5=0.0,t6=0.0,t7,t8=0.0
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSILUT
!
!
   NELT_L=0
   NELT_U=0
   IL(1)=1
   IU(1)=1
   
   !--------Finding norm of each column of A----
   norm = 0.0
   do i=1,N
	 do j=IA(i),IA(i+1)-1
		norm(i) = norm(i)+A(j)*A(j)
	 end do
   end do
   norm = sqrt(norm)
   !---------------------------------------
   t1 = MPI_WTIME()
   !Warnign: WHOLE ARRAY OPERATIONS AHEAD
    W=0.0
	IW=0
	JW=0
	PATTERN=0
  DO I = 1, N
		t2 = MPI_WTIME()
		ICOUNT=0
		JCOUNT=0
		DO J=IA(I)+1,IA(I+1)-1
			if(JA(J)>I) exit
			ICOUNT = ICOUNT+1
			W(ICOUNT)=A(J)
			JW(ICOUNT)=JA(J)
			IW(JA(J)) = ICOUNT
			PATTERN(JA(J)) = 1
		END DO
		
			JCOUNT = JCOUNT+1
			W(JCOUNT+I-1)=A(IA(I))
			JW(JCOUNT+I-1)=JA(IA(I))
			IW(I) = JCOUNT+I-1
			PATTERN(I) = 1
		DO K=J,IA(I+1)-1
			JCOUNT = JCOUNT+1
			W(JCOUNT+I-1)=A(K)
			JW(JCOUNT+I-1)=JA(K)
			IW(JA(K)) = JCOUNT+I-1
			PATTERN(JA(K)) = 1
		END DO
		J=1
		
		DO WHILE (J<I)
			if(J>ICOUNT) EXIT		!All lower elements exhausted
			W(J) = W(J)/U(IU(JW(J)))
			IF(abs(W(J))<TOL*norm(i) .and. pattern(JW(J))==0) THEN
				!DROP THIS ELEMENT
				IW(JW(J))=0
				DO K=J,ICOUNT-1
					W(K)=W(K+1)
					JW(K)=JW(K+1)
					IW(JW(K))=K
				END DO
				ICOUNT=ICOUNT-1
				CYCLE
			END IF
		
			DO K=IU(JW(J))+1,IU(JW(J)+1)-1
				IF(IW(JU(K))==0 .and. ( I>NI .OR. JU(K)>NI ) .and. abs(W(J)*U(K))>TOL*norm(i)) THEN !Fill in element
					IF(JU(K)>=I) THEN
						poss=-1
						do LN=I+JCOUNT-1,I,-1
							if(JW(LN)<JU(K)) then
								poss=LN+1
								exit
							end if
							W(LN+1)=W(LN)
							JW(LN+1)=JW(LN)
							IW(JW(LN))=LN+1
						end do
						if(poss==-1) poss=I+JCOUNT
						JCOUNT = JCOUNT+1
						W(poss)=-W(J)*U(K)
						JW(poss) = JU(K)
						IW(JU(K)) = poss
					ELSE
						poss=-1
						DO LN=ICOUNT,J,-1
							if(JW(LN)<JU(K)) then
								poss=LN+1
								exit
							end if
							W(LN+1)=W(LN)
							JW(LN+1)=JW(LN)
							IW(JW(LN)) = LN+1
						END DO
						if(poss==-1) poss=ICOUNT+1
						ICOUNT=ICOUNT+1
						W(poss)=-W(J)*U(K)
						JW(poss) = JU(K)
						IW(JU(K)) = poss
						
					END IF
				ELSE		!Existing element
					if(IW(JU(K))/=0) W(IW(JU(K))) = W(IW(JU(K))) - W(J)*U(K) !If check is important
				END IF 
				
			
			END DO
			J=J+1
		END DO
		
		t3 = MPI_WTIME()
		!--------------------Adjusting pattern elements in front for L---------
		K=0			!K counts number of elements in the pattern for L
		DO J=1,ICOUNT
			IF(PATTERN(JW(J))==0) CYCLE
			IF(J==K+1) THEN
				K=K+1
				CYCLE
			END IF
			K = K+1
			!Else swap K and J elements for W and JW
			temp = JW(J)
			JW(J) = JW(K)
			JW(K) = temp
			
			tempW = W(J)
			W(J) = W(K)
			W(K) = tempW
		END DO
		!-------------------------------------------------
		!Sort L elements
		if(ICOUNT>1) call qsort (W(K+1:ICOUNT),JW(K+1:ICOUNT),1,ICOUNT-K,1)
		
		!--------------------Adjusting pattern elements in front for U---------
		K=I			!K stores last position of elements in the pattern for U
		 DO J=I+1,I+JCOUNT-1
			 IF(PATTERN(JW(J))==0) CYCLE
			 IF(J==K+1) THEN
				 K=K+1
				 CYCLE
			 END IF
			 K = K+1
			 !Else swap K and J elements for W and JW
			 temp = JW(J)
			 JW(J) = JW(K)
			 JW(K) = temp
			
			 tempW = W(J)
			 W(J) = W(K)
			 W(K) = tempW
		 END DO
		
		!Make PATTERN and IW arrays zero for next iteration
		DO J=1,ICOUNT
			IW(JW(J))=0
			PATTERN(JW(J))=0
		END DO
		DO J=I,I+JCOUNT-1
			IW(JW(J))=0
			PATTERN(JW(J))=0
		END DO
		
		if(JCOUNT>2) call qsort (W(K+1:I+JCOUNT-1),JW(K+1:I+JCOUNT-1),1,I+JCOUNT-1-K,1) 
		
		!if(I==576) stop
!  write(499,*),JCOUNT,"---", W(I+1:I+JCOUNT-1)
!  write(499,*), JW(I+1:I+JCOUNT-1)
		!Insert first P elements in L and U each
		if(ICOUNT<P) THEN
			ST=1
			EN=ICOUNT
		else if(P<=I-1) THEN
			ST = 1
			EN = P
		else
			ST = 1
			EN = I-1
		END IF
		
		L(NELT_L+1:NELT_L+EN-ST+1) = W(ST:EN)
		IL(I+1) = IL(I)+EN-ST+1
		JL(NELT_L+1:NELT_L+EN-ST+1) = JW(ST:EN)
		NELT_L = NELT_L+EN-ST+1
		IF(NELT_L > (P)*N) THEN
			PRINT *, "MEMORY SCARCITY AT 778B"
			stop
		END IF
			
		U(NELT_U+1) = W(I)
		JU(NELT_U+1) = I
		IU(I+1)=IU(I)+1
		NELT_U = NELT_U+1
		
		if(JCOUNT<P) THEN
			ST=I+1
			EN=JCOUNT+I-1
		else if(I+P-1 <= N) THEN
			ST = I+1
			EN = I+P-1
		else
			ST = I+1
			EN = N
		END IF
		
		U(NELT_U+1:NELT_U+EN-ST+1) = W(ST:EN)
		IU(I+1) = IU(I+1)+EN-ST+1
		JU(NELT_U+1:NELT_U+EN-ST+1) = JW(ST:EN)
		NELT_U = NELT_U+EN-ST+1
		
		!CHECK SIZE
		IF(NELT_U > (P+1)*N) THEN
			PRINT *, "MEMORY SCARCITY AT 778A"
			stop
		END IF
		
		t4 = MPI_WTIME()
		!Sort each row of L and U
		call LUSORT(L(IL(I):IL(I+1)-1),JL(IL(I):IL(I+1)-1),IL(I+1)-IL(I))
	
		call LUSORT(U(IU(I):IU(I+1)-1),JU(IU(I):IU(I+1)-1),IU(I+1)-IU(I))
		
		t7 = MPI_WTIME()
		t5 = t5 + t3-t2
		t6 = t6 + t4-t3
		t8 = t8 + t7-t4
		!if(rank==7) print *, t5, t6,t8, I
  END DO
  !write(499,*), L(IL(1):IL(10)-1)
  !write(499,*), JL(IL(1):IL(10)-1)
  !write(499,*), U(IU(1):IU(10)-1)
  !write(499,*), JU(IU(1):IU(10)-1)
 END SUBROUTINE ILUT
!
recursive subroutine qsort(A,IA, first, last,direction)
  implicit none
  real (kind=8) ::  A(:), x, t
  integer :: first, last,IA(:),ti
  integer :: i, j,direction
 
  if (first>=last) return	
  x = a( (first+last) / 2 )
  i = first
  j = last
  do
     do while ((abs(a(i)) > abs(x) .and. direction == 1) .or. (abs(a(i)) < abs(x) .and. direction == 0))
        i=i+1
     end do
     do while ((abs(x) > abs(a(j)) .and. direction == 1) .or. (abs(x) < abs(a(j)) .and. direction == 0))
        j=j-1
     end do
     if (i >= j) exit
     t = a(i);  a(i) = a(j);  a(j) = t
     ti = ia(i);  ia(i) = ia(j);  ia(j) = ti
     i=i+1
     j=j-1
  end do
  if (first < i-1) call qsort(a, ia,first, i-1,direction)
  if (j+1 < last)  call qsort(a, ia,j+1, last,direction)
end subroutine qsort


subroutine LUSORT(A,JA,N)
implicit none
real (kind=8) :: A(N),tempA
integer :: JA(N),N,I,J,minv,temp


DO I=1,N-1
	minv = I
	DO J=I+1,N
		if(JA(J)<JA(minv)) minv=J
	END DO
	!Exchange
	temp=JA(minv)
	JA(minv)=JA(I)
	JA(I)=temp
	
	tempA = A(minv)
	A(minv) = A(I)
	A(I) = tempA
END DO
end subroutine LUSORT
	 	
	  
	 end module schur
