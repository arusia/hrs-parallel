!Rewriting schur complement in non-explicit form for the SC matrix, preconditioned local ILU with external setup
module SCHUR_SEQ_VAR
IMPLICIT NONE
INTEGER :: NI_max,NELT_max,max_row,NTI,NTB,NB_max,schur_overlap=1,LOCSI,LOCSR,SLENIW,SLENW
INTEGER,dimension(:),allocatable :: COLOR,N_PL,NI,NB,CNI,CNB  !,NI_PL(:)
INTEGER,dimension(:),allocatable :: IAA,JAA,IAB,JAB,IBA,JBA,IBB,JBB,ERRAY(:,:),part_list
REAL(KIND=8),dimension(:),allocatable :: AA,BB,AB,BA
end module

MODULE schur_seq

PUBLIC :: DSLUCS_SCP_INEX
CONTAINS
   SUBROUTINE DSLUCS_SCP_INEX(N,B,X,NELT,IA,JA,A,TOL,ITMAX,ITER,ERR,&
     &                  IERR,IUNIT,RWORK,LENW,IWORK,LENIW,N_SUB)
!
!
!	
	  USE SCHUR_SEQ_VAR
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
	  INCLUDE 'mpif.h'
!
      INTEGER N, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITMAX, ITER
      INTEGER IERR, IUNIT, LENW, IWORK(LENIW), LENIW,N_SUB
      REAL(KIND = 8) B(N), X(N), A(NELT), TOL, ERR, RWORK(LENW)
	  DOUBLE PRECISION :: time_t1,time_t2,time_i,time_set,time_sol
     
!
      INTEGER, PARAMETER :: LOCRB=1, LOCIB=11
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSLUCS
!
!
	 time_i=MPI_WTIME()
      ISYM = 0
      ITOL = 2
!
      IERR = 0
      IF( N.LT.1 .OR. NELT.LT.1 ) THEN
         IERR = 3
         RETURN
      ENDIF
      CALL DS2Y( N, NELT, JA, IA, A, ISYM, IUNIT )
!
      
      LOCR   = LOCRB
      LOCR0  = LOCR + N
      LOCP   = LOCR0 + N
      LOCQ   = LOCP + N
      LOCU   = LOCQ + N
      LOCV1  = LOCU + N
      LOCV2  = LOCV1 + N
	  LOCW   = LOCV2 + N
!
	
      IWORK(10) = LOCW
	  
!N,NELT,IA,JA,A,IUNIT,RWORK,IWORK,LENW,LENIW,N_SUB
	 time_t1 = MPI_WTIME()
      CALL SCHUR_INEX( N,NELT,IA,JA,A,IUNIT,RWORK,IWORK,LENW,LENIW,LOCIB,N_SUB )
	 time_set= MPI_WTIME()
	 time_set = time_set - time_t1
	 !print *, "TIME FOR SETTING UP SCHUR PREC",time_set
!         
      CALL DCGS_SCH(N, B, X, NELT, IA, JA, A, ISYM, DSMV_ROW,&
          SCHUR_MSOLVE_SEQ, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT,&
          RWORK(LOCR), RWORK(LOCR0), RWORK(LOCP),&
          RWORK(LOCQ), RWORK(LOCU), RWORK(LOCV1),&
          RWORK(LOCV2), RWORK, IWORK,N_SUB )
		  
		  
	deallocate(AA)
	deallocate(JAA)
	deallocate(IAA)
	
	deallocate(BB)
	deallocate(JBB)
	deallocate(IBB)
	
	deallocate(AB)
	deallocate(JAB)
	deallocate(IAB)
	
	deallocate(BA)
	deallocate(JBA)
	deallocate(IBA)
	
	deallocate(ERRAY)
	deallocate(COLOR)
	deallocate(N_PL)
	deallocate(NI)
	deallocate(NB)
	deallocate(CNI)
	deallocate(CNB)
	!deallocate(NI_PL)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DSLUCS
!
!
      RETURN
      END SUBROUTINE DSLUCS_SCP_INEX

SUBROUTINE SCHUR_INEX(N,NELT,IA,JA,A,IUNIT,RWORK,IWORK,LENW,LENIW,LOCIB,N_SUB)
	USE SCHUR_SEQ_VAR
	IMPLICIT NONE
	include 'mpif.h'
	integer,intent(in) :: N,NELT,IUNIT,N_SUB
	integer, intent(in) :: IA(N+1),JA(NELT)
	real(kind=8) :: A(NELT),RWORK(LENW),FUZZ=1e-22,temp
	INTEGER :: IWORK(LENIW)
	INTEGER :: i,j,k,l,m,TN_PL,OFFST_B,OFFST_A,ISYM,JJ,KK
	INTEGER :: NELT_AA,NELT_BA,NELT_BB
	integer :: LENIW,LENW,ii,ij,poss,tempj
	integer :: NL,NU,LOCIL,LOCJL,LOCIU,LOCJU,LOCIB,&
				LOCL,LOCDIN,LOCUU,LOCIW,LOCNC,LOCNR,LOCW
	DOUBLE PRECISION::t1,t2,t3,t4,t5
	INTEGER :: ICALL=0
	SAVE ICALL
	ICALL=ICALL+1
	!-----Partition the mesh into different number of subdomains
	
	t1 = MPI_WTIME()
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	IF(ICALL==1) allocate(part_list(N))
	allocate(ERRAY(N,N_SUB))
	allocate(COLOR(N))
	allocate(N_PL(N_sub))
	allocate(NI(N_sub))
	allocate(NB(N_sub))
	allocate(CNI(N_sub+1))
	allocate(CNB(N_sub+1))
	
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	!Find the partition of the domain
	if(ICALL==1) THEN
		if(N_SUB/=1) THEN
			call domain_partition(IA,JA,NELT,N,N_SUB,part_list)		
			part_list(1:N)=part_list(1:N)+1  !WARNING: Whole array operation
		ELSE
			part_list(1:N)=1
		END IF
	end if
	!Find number of nodes in each subdomain
	N_PL(1:N_sub) =0 
	
	DO I=1,N
		N_PL(part_list(I))=N_PL(part_list(I))+1
	END DO
	
	
	!-----Color the elements according to internal or boundary
	NI(1:N_SUB)=0
	NB(1:N_SUB)=0
	COLOR(1:N)=0
	
	DO I=1,N
		DO J=IA(I)+1,IA(I+1)-1
			IF(part_list(JA(J))==part_list(I)) CYCLE
			EXIT
		END DO
		IF(J==IA(I+1)) THEN
			NI(part_list(I))=NI(part_list(I))+1
			COLOR(I)=NI(part_list(I))
		ELSE
			NB(part_list(I))=NB(part_list(I))+1
			COLOR(I)=-1*NB(part_list(I))
		END IF
	END DO
	
	CNI(1)=1
	CNB(1)=1
	DO I=2,N_sub+1
		CNI(I)=NI(I-1)+CNI(I-1)
		CNB(I)=NB(I-1)+CNB(I-1)
	END DO
	NTI=CNI(N_sub+1)-1
	NTB=CNB(N_sub+1)-1
	
    !Find required maximum sizes
	NI_max = maxval(NI)
	NB_max = maxval(NB)
	max_row=1
	Do I=1,N
		max_row =max(max_row,IA(I+1)-IA(I))
	END DO
	NELT_max = NI_max*max_row
	
	!-----Allocate memory for AA,AB,BA and BB
	allocate(AA(NELT_max))
	allocate(JAA(NELT_max))
	allocate(IAA(NI_max+1))
	
	allocate(BB(max_row*NTB))
	allocate(JBB(max_row*NTB))
	allocate(IBB(NTB+1))
	
	allocate(AB(NTI*max_row))
	allocate(JAB(NTI*max_row))
	allocate(IAB(NTI+1))
	
	allocate(BA(NTB*max_row))
	allocate(JBA(NTB*max_row))
	allocate(IBA(NTB+1))
	
	!-----Find location in RWORK and IWORK for each subdomain to store 
	
	!RWORK and IWORK length for SCHUR COMPLEMENT preconditoining 
	  SLENW =  7*NTB + 1*(NELT*2+N) +100
	  SLENIW = 11 + 1*(NELT+4*(N+1)) 
	!Assign position in Rwork array
	  LOCL   = IWORK(10)   
      LOCDIN = LOCL + N_SUB*NELT_max
      LOCUU  = LOCDIN + N_SUB*NI_max
	  LOCSR = LOCUU + N_SUB*NELT_max
	  LOCW = LOCSR + SLENW
	!Assign position in iwork array
	  LOCIL = LOCIB !Starting location in IWORK
      LOCJL = LOCIL + N_SUB*(NI_max+1)
      LOCIU = LOCJL + N_SUB*(NELT_max/2)
      LOCJU = LOCIU + N_SUB*(NELT_max/2)
      LOCNR = LOCJU + N_SUB*(NI_max+1)	!LOCNR = NROW in DSILUS
      LOCNC = LOCNR + NI_max		!LOCNC = NCOL in DSILUS
      LOCSI = LOCNC + NI_max
	  LOCIW = LOCSI + SLENIW
	  
	  IWORK(1) = LOCIL
      IWORK(2) = LOCJL
      IWORK(3) = LOCIU
      IWORK(4) = LOCJU
      IWORK(5) = LOCL
      IWORK(6) = LOCDIN
      IWORK(7) = LOCUU
      IWORK(9) = LOCIW
	  
	!Checking allocation size
	
	if(LOCIW>LENIW .OR. LOCW>LENW) then
		write(*,*), "3311 NOT ENOUGH MEMORY TO CARRY OUT ILU forward elimination ",LOCIW,LENIW,LOCW,LENW	
		print *, "ABORTING"
		stop
	end if
	!-----Iterate over each subdomain and form AA,AB,BA,BB
	IBB(1:NTB+1)=0
	IBA(1:NTB+1)=0
	
	IBB(1)=1
	IBA(1)=1
	t2 = mpi_wtime()
	IAB(1:NTI+1)=0
	IAB(1)=1
	DO I=1,N_sub
		
		IAA(1:NI_max+1)=0
		IAA(1)=1
		
		!--------Initialize local to global number index ERRAY
		TN_PL=0
		DO J=1,N
			IF(part_list(J) .NE. I) CYCLE
			TN_PL=TN_PL+1
			ERRAY(TN_PL,I) = J
		END DO
		DO J=1,N_PL(I)
		
			L=ERRAY(J,I)
			OFFST_B=CNB(I)-1
			OFFST_A=CNI(I)-1
			
			IF( COLOR(L)<0) THEN 
			!The node in question is boundary node
				IBB(OFFST_B-color(L)+1)=IBB(OFFST_B-color(L))
				IBA(OFFST_B-color(L)+1)=IBA(OFFST_B-color(L))
				do K=IA(L),IA(L+1)-1
				
					if(color(JA(K))>0) then
						IF(IBA(OFFST_B-color(L)+1) > NTB*max_row) THEN
							print *, "Size overshoot in IAB 435"
							stop
						END IF
						IBA(OFFST_B-color(L)+1)=IBA(OFFST_B-color(L)+1)+1
						JBA(IBA(OFFST_B-color(L)+1)-1) = color(JA(K))
						BA(IBA(OFFST_B-color(L)+1)-1) = A(K)
					else
						IF(IBB(OFFST_B-color(L)+1)> NTB*max_row) THEN
							print *, "Size overshoot in IAB 440"
							stop
						END IF
						IBB(OFFST_B-color(L)+1)=IBB(OFFST_B-color(L)+1)+1
						JBB(IBB(OFFST_B-color(L)+1)-1) =  CNB(part_list(JA(K)))-color(JA(K))-1
					!	print *, '-8-8',color(L),CNB(part_list(JA(K)))-color(JA(K))-1,color(JA(K)),part_list(JA(K)),JA(K),K,L
						BB(IBB(OFFST_B-color(L)+1)-1) = A(K)
					end if
				end do
			ELSE
			!The node in question is internal node
				IAA(color(L)+1)=IAA(color(L))
				IAB(OFFST_A+color(L)+1)=IAB(OFFST_A+color(L))
				do K=IA(L),IA(L+1)-1		
					if(color(JA(K))>0) then
					
						IF(IAA(color(L)+1) > NELT_max) THEN
							print *, "Size overshoot in IAA 445"
							stop
						END IF
						IAA(color(L)+1)=IAA(color(L)+1)+1
						JAA(IAA(color(L)+1)-1) = color(JA(K))
						AA(IAA(color(L)+1)-1) = A(K)
						
					else
						IF(IAB(OFFST_A+color(L)+1) > NTI*max_row) THEN
							print *, "Size overshoot in IAB 450"
							stop
						END IF
						IAB(OFFST_A+color(L)+1)=IAB(OFFST_A+color(L)+1)+1
						JAB(IAB(OFFST_A+color(L)+1)-1) = -color(JA(K))
						AB(IAB(OFFST_A+color(L)+1)-1) = A(K)
					end if
				
				end do
			END IF
		END DO
		! print *,'A--------------'
		! print *, IA(1:N+1)
		! print *, JA(1:IA(N+1)-1)
		! print *, A(1:IA(N+1)-1)
		
		! print *,'AB--------------'
		! print *, IAB(1:NI(I))
		! print *, JAB(1:IAB(NI(I)+1)-1)
		! print *, AB(1:IAB(NI(I)+1)-1)
		
		! print *,'AA--------------'
		! print *, IAA(1:NI(I))
		! print *, JAA(1:IAA(NI(I)+1)-1)
		! print *, AA(1:IAA(NI(I)+1)-1)
		
		NL=0
		NU=0
		do J=1,NI(I)
			do K=IAA(J)+1,IAA(J+1)-1,1
				if(JAA(K)>J) then
					NU=NU+1
				else
					NL=NL+1
				end if
			end do
		end do
		NELT_AA = IAA(NI(I)+1)-1
		NELT_BA = IBA(OFFST_B+NB(I)+1)-IBA(OFFST_B+1)
		
		
		!Form ILU for internal array AA
		call  DSILUS_ROW(NI(I),NELT_AA,IAA,JAA,AA,NL,IWORK(LOCIL+(I-1)*(NI_max+1)),IWORK(LOCJL+(I-1)*(NELT_max/2)),&
		 &            RWORK(LOCL+(I-1)*(NELT_max)),RWORK(LOCDIN+(I-1)*(NI_max)),NU,IWORK(LOCIU+(I-1)*(NELT_max/2)),&
		 IWORK(LOCJU+(I-1)*(NI_max+1)),RWORK(LOCUU+(I-1)*(NELT_max)),IWORK(LOCNR),IWORK(LOCNC))
		
	
	END DO

	!Before leaving, sort the BB,JBB,IBB matrix
	DO I=1,NTB
		!Doing bubble sort for each element--It is already ensured that first element is diagonal, so leave that
		DO J=IBB(I)+1,IBB(I+1)-2
			poss = J
			DO K=J+1,IBB(I+1)-1
				IF(JBB(poss) .LE. JBB(K)) CYCLE
				poss = K
			END DO
			IF(poss==J) CYCLE
			!Exchange Jth position with smallest element
			tempj = JBB(J)
			temp = BB(J)
			BB(J) = BB(poss) 
			JBB(J) = JBB(poss)
			BB(poss) = temp
			JBB(poss) = tempj
		END DO
	END DO
	
	NELT_BB=IBB(NTB+1)-1
	!Find NL and NU
	NU=0
	NL=0
	DO I=1,NTB
		DO J=IBB(I)+1,IBB(I+1)-1
			IF(JBB(J)>I) THEN
				NU=NU+1
			ELSE
				NL=NL+1
			END IF
		END DO
	END DO
	  LOCIL = 11
      LOCJL = LOCIL + NTB+1
      LOCIU = LOCJL + NL
      LOCJU = LOCIU + NU
      LOCNR = LOCJU + NTB+1
      LOCNC = LOCNR + NTB
      LOCIW = LOCNC + NTB
!
	  
      LOCL   = 1
      LOCDIN = LOCL + NL
      LOCUU  = LOCDIN + NTB
	  LOCW = LOCUU + NU
	  IWORK(LOCSI-1+10)=LOCW
	  
      IWORK(LOCSI-1+1) = LOCIL
      IWORK(LOCSI-1+2) = LOCJL
      IWORK(LOCSI-1+3) = LOCIU
      IWORK(LOCSI-1+4) = LOCJU
      IWORK(LOCSI-1+5) = LOCL
      IWORK(LOCSI-1+6) = LOCDIN
      IWORK(LOCSI-1+7) = LOCUU
      IWORK(LOCSI-1+9) = LOCIW
	call  DSILUS_ROW(NTB,NELT_BB,IBB,JBB,BB,NL,IWORK(LOCSI-1+LOCIL),IWORK(LOCSI-1+LOCJL),&
     &                  RWORK(LOCSR+LOCL-1),RWORK(LOCSR-1+LOCDIN),NU,&
			IWORK(LOCSI-1+LOCIU),IWORK(LOCSI-1+LOCJU),RWORK(LOCSR-1+LOCUU),IWORK(LOCSI-1+LOCNR),IWORK(LOCSI-1+LOCNC))
	

END SUBROUTINE SCHUR_INEX 	
	
	
subroutine SCHUR_MSOLVE_SEQ(N, B, X, NELT, IA, JA, A, ISYM, RWORK, IWORK,N_sub)
	USE SCHUR_SEQ_VAR
	!USE Matrix_Solvers
	IMPLICIT NONE
	INCLUDE 'mpif.h'
	INTEGER :: N,NELT,ISYM,JA(NELT),IA(N+1),IWORK(*),N_sub
	REAL (KIND=8) :: X(N),A(NELT),RWORK(*),B(N)
	REAL(KIND=8),allocatable :: Y(:)!,SRWORK(:)
	!INTEGER,allocatable :: SIWORK(:)
	REAL (KIND=8) :: B_i(1:NTI),B_b(1:NTB),Xb(1:NTB),XI(NTI)
	REAL(KIND=8) :: STOL=1e-9,ERR
	INTEGER :: SITMAX=500,SITER=0,IERR=0,ii,ij
	INTEGER :: OFFST_A,OFFST_B
	INTEGER :: I,J,K,L,M,LENW,LENIW,NELT_BB
	INTEGER :: IERROR(1:100)
	INTEGER :: CALLI=0
	SAVE CALLI
	DOUBLE PRECISION :: t1,t2,t3
	
	CALLI=CALLI+1
	t1 = MPI_WTIME()
	! Form B_i and B_b------------------------------
	allocate(Y(NTB))
	
	DO I=1,N_SUB
		 OFFST_A=CNI(I)-1
		 OFFST_B =CNB(I)-1
		 
		 DO J=1,N_PL(I)
			K=ERRAY(J,I)
			IF(COLOR(K)<0) THEN
				B_b(OFFST_B-COLOR(K))=B(K)
				Xb(OFFST_B-COLOR(K))=X(K)
			ELSE
				B_i(OFFST_A+COLOR(K))=B(K)
			END IF
		 END DO
		 call DSLUI_SCH(NI(I), B_i(OFFST_A+1:OFFST_A+NI(I)), XI(OFFST_A+1:OFFST_A+NI(I)), &
								NELT,IA,JA,A, ISYM, RWORK, IWORK,I,NI_max,NELT_max)
		
		 ! stop
		 call DSMVR_SCH( NB(I),NI(I), XI(OFFST_A+1:OFFST_A+NI(I)), Y(OFFST_B+1:OFFST_B+NB(I)), &
						IBA(NTB+1)-1, IBA, JBA, BA, ISYM,OFFST_B)	
					
		 Y(OFFST_B+1:OFFST_B+NB(I)) = B_b(OFFST_B+1:OFFST_B+NB(I))-Y(OFFST_B+1:OFFST_B+NB(I))
		
	END DO
	

	
	NELT_BB=IBB(NTB+1)-1
	!Solve schur complement system-
	!print *,'NIGNINI--------------'
	!print *, XB(1:NTB)
	t2 = MPI_WTIME()
	
	!print *, "SETTING UP RHS time",t2-t1
	call DSLUCS_WP_SCSQ(NTB,Y,Xb,NELT_BB,IBB,JBB,BB,STOL,SITMAX,SITER,ERR,&
     &                  IERR,0,RWORK(LOCSR),SLENW,IWORK(LOCSI),SLENIW,N_SUB,&
						N,NELT,IA,JA,A,RWORK,IWORK)
	t3 = MPI_WTIME()
	
	!print *, "SC matrix solver time",t3-t2
	!Xb1(1:NTB) = Yb(1:NTB)
	!if(IERR/=0) print *,"WARNING: IERR NOT ZERO AT DSLUCS_WP_SCSQ"," and IERR:",IERR
	 
	!if(CALLI==17) print *, Xb(1:100)
	print *,"SITER:",SITER," CALLI:", CALLI," IERR:", IERR
!	if(CALLI==17) stop
	! if(maxval(abs(Y(1:NTB)))>1e-20) then
	!	 print *,'Xb',Xb(1:Ntb)
	!	 stop
	!end if 
	
	!Solve internal system to get Fi--------------
	DO I=1,N_SUB
		OFFST_A=CNI(I)-1
		OFFST_B =CNB(I)-1
		call DSMVR_SCH(NI(I),NB(I),XB(OFFST_B+1:OFFST_B+NB(I)),XI(OFFST_A+1:OFFST_A+NI(I)),&
					IAB(NTI+1)-1,IAB,JAB,AB,ISYM,OFFST_A)
		
		XI(OFFST_A+1:OFFST_A+NI(I)) = B_I(OFFST_A+1:OFFST_A+NI(I))-XI(OFFST_A+1:OFFST_A+NI(I))
		
		call DSLUI_SCH(NI(I), XI(OFFST_A+1:OFFST_A+NI(I)), XI(OFFST_A+1:OFFST_A+NI(I)),&
							NELT,IA,JA,A, ISYM, RWORK, IWORK,I,NI_max,NELT_max)
		
		
		DO J=1,N_PL(I)
			K=ERRAY(J,I)
			IF(COLOR(K)<0)  THEN
				X(K) = XB(OFFST_B-COLOR(K))
			ELSE
				X(K) = XI(OFFST_A+COLOR(K))
			END IF
		 END DO
		 
	END DO
	
end subroutine SCHUR_MSOLVE_SEQ





    SUBROUTINE DSLUCS_WP_SCSQ(N,B,X,NELT,IA,JA,A,TOL,ITMAX,ITER,ERR,&
     &                  IERR,IUNIT,RWORK,LENW,IWORK,LENIW,N_SUB,&
						N_A,NELT_A,IA_A,JA_A,A_A,RTWORK,ITWORK)
!
!
!
	  USE SCHUR_SEQ_VAR
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
!
      INTEGER :: N, NELT, IA(N+1), JA(NELT),IA_A(N_A+1),JA_A(NELT_A),ISYM,&
				ITOL, ITMAX, ITER,N_A,NELT_A, NL,NU
      INTEGER :: IERR, IUNIT, LENW, IWORK(*),ITWORK(*),LENIW,N_SUB
      REAL(KIND = 8) :: B(N), X(N), A(NELT),A_A(NELT_A), TOL, ERR, RWORK(*),RTWORK(*)
!
      INTEGER, PARAMETER :: LOCRB=1, LOCIB=11
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSLUCS
!
!

      ISYM = 0
      ITOL = 2
!
      IERR = 0
      IF( N.LT.1 .OR. NELT.LT.1 ) THEN
         IERR = 3
         RETURN
      ENDIF 
      CALL DS2Y( N, NELT, JA, IA, A, ISYM, IUNIT )
!
!        
  
!	
	
	!Assign position in Rwork array
	!Assign position in iwork array
	  
      LOCR   = IWORK(10) 
      LOCR0  = LOCR + N
      LOCP   = LOCR0 + N
      LOCQ   = LOCP + N
      LOCU   = LOCQ + N
      LOCV1  = LOCU + N
      LOCV2  = LOCV1 + N
      LOCW   = LOCV2 + N
	  
!
	  
	  if(IWORK(9)>LENIW .OR. LOCW>LENW) then
		print *, "ABORTING: NOT ENOUGH MEMORY AT LOCATION Y2231--preconditioning SC matrix"
		stop
	  end if
	
		
      CALL DCGS_WP_SCSQ(N, B, X, NELT, IA, JA, A, ISYM, DSMV_SCHUR_INEX,&
          DSLUI_ANY, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT,&
          RWORK(LOCR), RWORK(LOCR0), RWORK(LOCP),&
          RWORK(LOCQ), RWORK(LOCU), RWORK(LOCV1),&
          RWORK(LOCV2), RWORK, IWORK,N_SUB,N_A,RTWORK,ITWORK)
		 
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DSLUCS
!
!
      RETURN
      END SUBROUTINE DSLUCS_WP_SCSQ
!
!


SUBROUTINE DSMV_SCHUR_INEX(NTB, X, Y, NELT_BB, IBB, JBB, BB, N_SUB,RWORK,IWORK)
!OUTPUT IS IN Y
	USE SCHUR_SEQ_VAR, NTBB => NTB, IBBB => IBB, JBBB => JBB, BBB => BB
	IMPLICIT NONE
	INTEGER :: NTB,NELT_BB,N_SUB
	INTEGER :: ISYM,I,J,OFFST_A,OFFST_B,IBB(NTB+1),JBB(NELT_BB),IWORK(*)
	REAL(KIND=8),allocatable :: XI(:),B_i(:)
	REAL(KIND=8) :: Y(NTB),X(NTB),B_b(NTB),BB(NELT_BB),RWORK(*)
	
	allocate(XI(NTI))
	allocate(B_i(NTI))
	DO I=1,N_SUB
		!Defining some variables
		OFFST_B=CNB(I)-1
		OFFST_A=CNI(I)-1
		!compute matrix vector product AB*X
		
		call DSMVR_SCH(NI(I),NB(I),X(OFFST_B+1:OFFST_B+NB(I)),XI(OFFST_A+1:OFFST_A+NI(I)),&
						IAB(NTI+1)-1,IAB,JAB,AB,ISYM,OFFST_A)
		!Compute preconditioning operation inv(AA)*(XI)
		call DSLUI_SCH(NI(I), XI(OFFST_A+1:OFFST_A+NI(I)), B_i(OFFST_A+1:OFFST_A+NI(I)), &
									0,(/0/),(/0/),BB(1), ISYM, RWORK, IWORK,I,NI_max,NELT_max)
			
		!Compute matvat product BA*B_i
		 call DSMVR_SCH( NB(I),NI(I), B_i(OFFST_A+1:OFFST_A+NI(I)), Y(OFFST_B+1:OFFST_B+NB(I)), &
							IBA(NTB+1)-1, IBA, JBA, BA, ISYM,OFFST_B)	
	END DO
	!Compute matvat product BB*X
	CALL DSMV_ROW(NTB, X, B_b, NELT_BB, IBB, JBB, BB, ISYM )
	
	!Final subtraction 
	Y = B_b - Y
	
	deallocate(XI)
	deallocate(B_i)
END SUBROUTINE DSMV_SCHUR_INEX
	
 SUBROUTINE DCGS_WP_SCSQ(N, B, X, NELT, IA, JA, A, ISYM, MATVEC,MSOLVE,&
     &      ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, &
     &     R, R0, P, Q, U, V1, V2, RWORK, IWORK,N_SUB,N_A,RTWORK,ITWORK)
!
!
!
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
!
      INTEGER :: N, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITMAX
      INTEGER :: ITER, IERR, IUNIT, IWORK(*),N_SUB,N_A,ITWORK(*)
      REAL(KIND = 8) :: B(N), X(N), A(NELT), TOL, ERR, R(N), R0(N), P(N)
      REAL(KIND = 8) :: Q(N), U(N), V1(N), V2(N), RWORK(*),RTWORK(*)
!
      REAL(KIND = 8) :: DMACH(5)
      DATA DMACH(3) / 1.1101827117665D-16 /

!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DCGS
!
!
      ITER = 0
      IERR = 0
!
      IF( N < 1 ) THEN
         IERR = 3
         RETURN
      ENDIF
!
      TOLMIN = 5.0d2*DMACH(3)
      IF( TOL < TOLMIN ) THEN
         TOL  = TOLMIN
         IERR = 4
      ENDIF
!         
	
      CALL MATVEC(N, X, R, NELT, IA, JA, A, N_SUB,RTWORK,ITWORK)
!         
      V1(1:N)  = R(1:N) - B(1:N)
!         
	  CALL MSOLVE(N, V1, R, NELT, IA, JA, A, ISYM, RWORK, IWORK,N_SUB,N_A)
!        
      ISDCGS = 0
      ITOL   = 2
      IF(ITER == 0) THEN
		CALL MSOLVE(N, B, V2, NELT, IA, JA, A, ISYM, RWORK, IWORK,N_SUB,N_A)
         BNRM = SQRT(DOT_PRODUCT(V2(1:N),V2(1:N)))
      ENDIF
!         
      ERR = SQRT(DOT_PRODUCT(R(1:N),R(1:N)))/BNRM
!         
      IF(IUNIT /= 0) THEN
         IF( ITER == 0 ) THEN
            WRITE(IUNIT,1000) N, ITOL
         ENDIF
         WRITE(IUNIT,1010) ITER, ERR, AK, BK
      ENDIF
      IF(ERR <= TOL) ISDCGS = 1
!         
      IF(ISDCGS /= 0) GO TO 200
!
!
      FUZZ = DMACH(3)**2
!
      R0(1:N) = R(1:N)
!
      RHONM1 = 1.0D0
!         
!         
      DO 100 K=1,ITMAX
         ITER = K
!
         RHON = DOT_PRODUCT(R0(1:N),R(1:N))
!
         !IF( ABS(RHONM1) < FUZZ ) GO TO 998
!
         BK = RHON/RHONM1
!
         IF( ITER.EQ.1 ) THEN
            U(1:N) = R(1:N)
            P(1:N) = R(1:N)
         ELSE
            U(1:N)  = R(1:N) + BK*Q(1:N)
            V1(1:N) = Q(1:N) + BK*P(1:N)
            P(1:N)  = U(1:N) + BK*V1(1:N)
         ENDIF
!         
         CALL MATVEC(N, P, V2, NELT, IA, JA, A, N_SUB,RTWORK,ITWORK)
         CALL MSOLVE(N, V2, V1, NELT, IA, JA, A, ISYM, RWORK, IWORK,N_SUB,N_A)
!         
	
         SIGMA = DOT_PRODUCT(R0(1:N),V1(1:N))

         !IF( ABS(SIGMA) < FUZZ ) GO TO 999
!         
         AK = RHON/SIGMA
         AKM = -AK
!         
         Q(1:N)  = U(1:N) + AKM*V1(1:N)
!         
         V1(1:N) = U(1:N) + Q(1:N)
!         
         X(1:N)  = X(1:N) + AKM*V1(1:N)
!                      
         CALL MATVEC(N, V1, V2, NELT, IA, JA, A, N_SUB,RTWORK,ITWORK)
         CALL MSOLVE(N, V2, V1, NELT, IA, JA, A, ISYM, RWORK, IWORK,N_SUB,N_A)
		 
         R(1:N) = R(1:N) + AKM*V1(1:N)
	   ! CALL MATVEC(N,X,V1,NELT,IA,JA,A,ISYM)
       !  V1(1:N) = V1(1:N) - B(1:N)
       !  CALL MSOLVE(N, V1, R, NELT, IA, JA, A, ISYM, RWORK, IWORK,N_SUB,N_A)

	! print *, 'XXX', X(1:N)
         ISDCGS = 0
         ITOL   = 2
!         
         ERR = SQRT(DOT_PRODUCT(R(1:N),R(1:N)))/BNRM
!         
         IF(IUNIT /= 0) THEN
            WRITE(IUNIT,1010) ITER, ERR, AK, BK
         ENDIF
         IF(ERR <= TOL) ISDCGS = 1
!         
         IF(ISDCGS /= 0) GO TO 200
!
         RHONM1 = RHON
 100  CONTINUE
 1000 FORMAT(' Preconditioned BiConjugate Gradient Squared for ',&
          'N, ITOL = ',I5, I5,&
          /' ITER','   Error Estimate','            Alpha',&
          '             Beta')
 1010 FORMAT(1X,I4,1X,E16.7,1X,E16.7,1X,E16.7)
!         
      ITER = ITMAX + 1
      IERR = 2
 200  RETURN
!
 998  IERR = 5
      RETURN
!
 999  IERR = 6
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DCGS
!
!
      RETURN
      END SUBROUTINE DCGS_WP_SCSQ
	  
SUBROUTINE TEMP_J(N, X, Y, NELT, IA, JA, A, ISYM, RWORK, IWORK,N_SUB,N_A) 	
!Temporary jacobi preconditioning . Y is output
	INTEGER :: N,NELT
	INTEGER :: IA(N+1),JA(NELT),IWORK(*),N_SUB,N_A,ISYM
	REAL(KIND=8) :: A(NELT),RWORK(*),X(N),Y(N)
	INTEGER :: I
	
	DO I=1,N
		Y(I) = X(I)/A(IA(I))
	END DO
END SUBROUTINE TEMP_J	





	SUBROUTINE domain_partition(IA,JA,NELT,N,N_SUB,part_list)

IMPLICIT NONE
INTEGER :: IA(*),JA(*),NELT,N,N_SUB,part_list(*)
INTEGER :: xadj(0:N),adjncy(0:NELT-1),NELT_N
INTEGER :: I,J
INTEGER :: ncon=1
integer, pointer :: vwget=>NULL(), vsize=>NULL() !weights and size of vertices
integer, pointer :: adjwget=>NULL() !Weights of edges
real, pointer :: tpwgets=>NULL(),ubvec=>NULL()
integer,pointer :: mopts=>NULL() !OPTIONS 
integer,pointer  ::  objval=>NULL() !Stores the edge-cut or the total comm. Volume of the partitioning solution
integer :: part(0:N-1),error
CHARACTER(LEN=14) :: filenm
LOGICAL ::EX
INTEGER :: ICALL=0

SAVE ICALL

ICALL = ICALL+1

 open(821,FILE="CSR")
 !Converting to C-indexed CSR arrays according to METIS by also removing connection to self
  xadj(0)=IA(1)-1
  NELT_N=0
  DO I=1,N
	adjncy(NELT_N:NELT_N+IA(I+1)-2-IA(I)) = JA(IA(I)+1:IA(I+1)-1)-1
	NELT_N=NELT_N+IA(I+1)-1-IA(I)
	 xadj(I)=xadj(I-1)+IA(I+1)-1-IA(I)
  END DO
  
  !Writing to the METIS input file, N, and number of unique connections (1-2 and 2-1 connections are counted as one)
  write(821,*)  N,(NELT_N)/2
	  
  DO I=1,N
	write(821,*),adjncy(Xadj(I-1):Xadj(I)-1)+1
  END DO
  
  write(filenm,*),N_SUB
  filenm = trim(adjustl(filenm))
  
  error = system('gpmetis CSR '//filenm)
  if(error/=0) then
	print *, "ERROR in METIS partitioning algorithm at 77821"
	stop
  end if  
  filenm = "CSR.part."//filenm
  
  INQUIRE(FILE=filenm,EXIST = EX)
  IF(.NOT. EX) then
	print *, "NEED PARTITIONING DATA. ABORT!!!!!!"
	stop
  end if
  
 open(751,FILE=filenm)

 READ(751,*),part_list(1:N)
 close(751)
 
END SUBROUTINE domain_partition

  
  
      SUBROUTINE DSMV_ROW( N, X, Y, NELT, IA, JA, A, ISYM )
!
!
!
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      INTEGER N, NELT, IA(NELT), JA(NELT), ISYM
      REAL(KIND = 8) A(NELT), X(N), Y(N)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSMV
!
!
      Y(1:N) = 0.0d0     ! CAREFUL! Whole array operations
!
      DO IROW = 1, N
         DO j = IA(IROW),IA(IROW+1)-1 
			Y(IROW) = Y(IROW)+A(j)*X(JA(J))
		 END DO
      END DO
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DSMV
!
!
      RETURN
      END SUBROUTINE DSMV_ROW
!


	 SUBROUTINE DS2Y(N, NELT, IA, JA, A, ISYM,IUNIT )
!Parallel Version written by Aman Rusia Date 102616
!
!
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      INTEGER N, NELT, IA(NELT), JA(NELT), ISYM
      REAL(KIND = 8) A(NELT)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DS2Y
!
!	
      IF( JA(N+1).EQ.NELT+1 ) RETURN
      CALL QS2I1D( JA, IA, A, NELT, 1,IUNIT )
      JA(1) = 1
      DO 20 ICOL = 1, N-1
         DO 10 J = JA(ICOL)+1, NELT
            IF( JA(J).NE.ICOL ) THEN
               JA(ICOL+1) = J
               GOTO 20
            ENDIF
 10      CONTINUE
 20   CONTINUE
      JA(N+1) = NELT+1
!         
      JA(N+2) = 0
!
      DO 70 ICOL = 1, N
         IBGN = JA(ICOL)
         IEND = JA(ICOL+1)-1
         DO 30 I = IBGN, IEND
            IF( IA(I).EQ.ICOL ) THEN
               ITEMP = IA(I)
               IA(I) = IA(IBGN)
               IA(IBGN) = ITEMP
               TEMP = A(I)
               A(I) = A(IBGN)
               A(IBGN) = TEMP
               GOTO 40
            ENDIF
 30      CONTINUE
 40      IBGN = IBGN + 1
         IF( IBGN.LT.IEND ) THEN
            DO 60 I = IBGN, IEND
               DO 50 J = I+1, IEND
                  IF( IA(I).GT.IA(J) ) THEN
                     ITEMP = IA(I)
                     IA(I) = IA(J)
                     IA(J) = ITEMP
                     TEMP = A(I)
                     A(I) = A(J)
                     A(J) = TEMP
                  ENDIF
 50            CONTINUE
 60         CONTINUE
         ENDIF
 70   CONTINUE
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DS2Y
!
!
      RETURN
      END SUBROUTINE DS2Y
!
 SUBROUTINE QS2I1D( IA, JA, A, N, KFLAG, IUNIT )
!
!
!
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
	
	  
      DIMENSION IL(21),IU(21)
      INTEGER  :: N,NN,IA(N),JA(N),IT,IIT,JT,JJT
      REAL(KIND = 8) A(N), TA, TTA
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of QS2I1D
!
!
	
      NN = N
      IF (NN.LT.1) THEN
	 
 6100 FORMAT(/,'QS2I1D- the number of values to', &
     &         ' be sorted was NOT POSITIVE.')
         RETURN
      ENDIF
      IF( N.EQ.1 ) RETURN
      KK = IABS(KFLAG)
      IF ( KK.NE.1 ) THEN
 6101 FORMAT(/,'QS2I1D- the sort control parameter, k, ',&
     &         'was not 1 OR -1.')
         RETURN
      ENDIF
!
      IF( KFLAG.LT.1 ) THEN
         DO 20 I=1,NN
            IA(I) = -IA(I)
 20      CONTINUE
      ENDIF
!
      M = 1
      I = 1
      J = NN
      R = 3.75D-1
 210  IF( R.LE.5.898437D-1 ) THEN
         R = R + 3.90625D-2
      ELSE
         R = R-2.1875D-1
      ENDIF
 225  K = I
!
!
      IJ = I + IDINT( DBLE(J-I)*R )
      IT = IA(IJ)
      JT = JA(IJ)
      TA = A(IJ)
!
!
      IF( IA(I).GT.IT ) THEN
         IA(IJ) = IA(I)
         IA(I)  = IT
         IT     = IA(IJ)
         JA(IJ) = JA(I)
         JA(I)  = JT
         JT     = JA(IJ)
         A(IJ)  = A(I)
         A(I)   = TA
         TA     = A(IJ)
      ENDIF
      L=J
!                           
!
      IF( IA(J).LT.IT ) THEN
         IA(IJ) = IA(J)
         IA(J)  = IT
         IT     = IA(IJ)
         JA(IJ) = JA(J)
         JA(J)  = JT
         JT     = JA(IJ)
         A(IJ)  = A(J)
         A(J)   = TA
         TA     = A(IJ)
!
!
         IF ( IA(I).GT.IT ) THEN
            IA(IJ) = IA(I)
            IA(I)  = IT
            IT     = IA(IJ)
            JA(IJ) = JA(I)
            JA(I)  = JT
            JT     = JA(IJ)
            A(IJ)  = A(I)
            A(I)   = TA
            TA     = A(IJ)
         ENDIF
      ENDIF
!
!
  240 L=L-1
      IF( IA(L).GT.IT ) GO TO 240
!
!
  245 K=K+1
      IF( IA(K).LT.IT ) GO TO 245
!
!
      IF( K.LE.L ) THEN
         IIT   = IA(L)
         IA(L) = IA(K)
         IA(K) = IIT
         JJT   = JA(L)
         JA(L) = JA(K)
         JA(K) = JJT
         TTA   = A(L)
         A(L)  = A(K)
         A(K)  = TTA
         GOTO 240
      ENDIF
!
!
      IF( L-I.GT.J-K ) THEN
         IL(M) = I
         IU(M) = L
         I = K
         M = M+1
      ELSE
         IL(M) = K
         IU(M) = J
         J = L
         M = M+1
      ENDIF
      GO TO 260
!
!                                  
  255 M = M-1
      IF( M.EQ.0 ) GO TO 300
      I = IL(M)
      J = IU(M)
  260 IF( J-I.GE.1 ) GO TO 225
      IF( I.EQ.J ) GO TO 255
      IF( I.EQ.1 ) GO TO 210
      I = I-1
  265 I = I+1
      IF( I.EQ.J ) GO TO 255
      IT = IA(I+1)
      JT = JA(I+1)
      TA =  A(I+1)
      IF( IA(I).LE.IT ) GO TO 265
      K=I
  270 IA(K+1) = IA(K)
      JA(K+1) = JA(K)
      A(K+1)  =  A(K)
      K = K-1
      IF( IT.LT.IA(K) ) GO TO 270
      IA(K+1) = IT
      JA(K+1) = JT
      A(K+1)  = TA
      GO TO 265
!
!
  300 IF( KFLAG.LT.1 ) THEN
         DO 310 I=1,NN
            IA(I) = -IA(I)
 310     CONTINUE
      ENDIF
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of QS2I1D
!
!
      RETURN
      END SUBROUTINE QS2I1D
!
!  
  
SUBROUTINE DSILUS_ROW(N,NELT,IA,JA,A,NL,IL,JL,&
     &                  L,DINV,NU,IU,JU,U,NROW,NCOL)
!!Parallel version of incomplete LDU preconditioner changed by Aman Rusia on 160616
!!N - size of system (originally), NELT, IA, JA, A, NL, IWORK(LOCIL),&
  !        & IWORK(LOCJL), RWORK(LOCL), RWORK(LOCDIN), NU, IWORK(LOCIU),&
  !       & IWORK(LOCJU), RWORK(LOCU), IWORK(LOCNR), IWORK(LOCNC)
!NROW - size 1:N. Since U is stored in SLAP row format. Nrow(i) is number of upper elements in ith column
!NCOL - size 1:N. Since L is stored in SLAP column format. Ncol(i) is number of lower elements in ith column
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      INTEGER N, NELT,IA(NELT),JA(NELT),NL,IL(NL),JL(NL)
      INTEGER NU, IU(NU), JU(NU), NROW(N), NCOL(N)			!Although the size of IL and IU are set as NL and NU, there is enough space as provided in DSLUCS. (check!)
      REAL(KIND = 8) A(NELT), L(NL), DINV(N), U(NU)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSILUS
!
!
      NROW(1:N) = 0
      NCOL(1:N) = 0
!

      DO IROW = 1, N
         IBGN = IA(IROW)+1
         IEND = IA(IROW+1)-1
         IF( IBGN.LE.IEND ) THEN
            DO  I = IBGN, IEND
				IF((JA(I) > N) ) CYCLE
               IF( (JA(I).GT.IROW)) THEN
                  NCOL(JA(I)) = NCOL(JA(I)) + 1
               ELSE
                  NROW(IROW) = NROW(IROW) + 1
               ENDIF
			END DO
         ENDIF
	  END DO
	  
!Forming JU - column of SLAP column format in U. and IL - row of SLAP row format in L. 1st element of U and L are always 0.
      JU(1) = 1
      IL(1) = 1
      DO 40 ICOL = 1, N
         IL(ICOL+1) = IL(ICOL) + NROW(ICOL)
         JU(ICOL+1) = JU(ICOL) + NCOL(ICOL)
         NROW(ICOL) = IL(ICOL)
         NCOL(ICOL) = JU(ICOL)
 40   CONTINUE
 
!Copying values into IU, U, JL and L. Note no diagonal elements are stored in either L or U        
      DO 60 IROW = 1, N
         DINV(IROW) = A(IA(IROW))
         IBGN = IA(IROW)+1
         IEND = IA(IROW+1)-1
         IF( IBGN.LE.IEND ) THEN
            DO 50 I = IBGN, IEND
               ICOL = JA(I)
			   IF((JA(I) > N)) CYCLE
               IF( (IROW.LT.ICOL) ) THEN
                  IU(NCOL(ICOL)) = IROW
                  U(NCOL(ICOL)) = A(I)
                  NCOL(ICOL) = NCOL(ICOL) + 1
               ELSE
                  JL(NROW(IROW)) = ICOL
                  L(NROW(IROW)) = A(I)
                  NROW(IROW) = NROW(IROW) + 1
               ENDIF
 50         CONTINUE
         ENDIF
 60   CONTINUE
!Sorting of row and column number. So the appear incremently in each format.
      DO 110 K = 2, N
         JBGN = JU(K)
         JEND = JU(K+1)-1
         IF( JBGN.LT.JEND ) THEN !Do the sorting of row numbers in i increment order for each column.
            DO 80 J = JBGN, JEND-1
               DO 70 I = J+1, JEND
                  IF( IU(J).GT.IU(I) ) THEN
                     ITEMP = IU(J)
                     IU(J) = IU(I)
                     IU(I) = ITEMP
                     TEMP = U(J)
                     U(J) = U(I)
                     U(I) = TEMP
                  ENDIF
 70            CONTINUE
 80         CONTINUE
         ENDIF
         IBGN = IL(K)
         IEND = IL(K+1)-1
         IF( IBGN.LT.IEND ) THEN !Do the sorting of column numbers in each row.
            DO 100 I = IBGN, IEND-1
               DO 90 J = I+1, IEND
                  IF( JL(I).GT.JL(J) ) THEN
                     JTEMP = JU(I)
                     JU(I) = JU(J)
                     JU(J) = JTEMP
                     TEMP = L(I)
                     L(I) = L(J)
                     L(J) = TEMP
                  ENDIF
 90            CONTINUE
 100        CONTINUE
         ENDIF
 110  CONTINUE
 
!Carrying out forward Sweep
      DO 300 I=2,N  
!Changing lower elements         
         INDX1 = IL(I)
         INDX2 = IL(I+1) - 1
         IF(INDX1 .GT. INDX2) GO TO 200 !No lower elements in the row
         DO 190 INDX=INDX1,INDX2		!Column loop for a row in L
            IF(INDX .EQ. INDX1) GO TO 180 !only one lower element
            INDXR1 = INDX1				
            INDXR2 = INDX - 1
            INDXC1 = JU(JL(INDX))    !JU(JL(INDX)) is just the index of the first element in the same column as INDX present in U
            INDXC2 = JU(JL(INDX)+1) - 1 !Lower most element in a column of U where INDXC1 is the top most.
            IF(INDXC1 .GT. INDXC2) GO TO 180 !No elements in U in the given column 
 160        KR = JL(INDXR1) !Column offset of first element in the current row.
 170        KC = IU(INDXC1) !Row offset of first element in the current column @U
            IF(KR .GT. KC) THEN
               INDXC1 = INDXC1 + 1	
               IF(INDXC1 .LE. INDXC2) GO TO 170
            ELSE IF(KR .LT. KC) THEN
               INDXR1 = INDXR1 + 1
               IF(INDXR1 .LE. INDXR2) GO TO 160
            ELSE IF(KR .EQ. KC) THEN
               L(INDX) = L(INDX) - L(INDXR1)*DINV(KC)*U(INDXC1) !go through transformations. NOTE - EXTRA DINV(KC) because U is also divided by DINV - since this is LDU factorization and not LU.
               INDXR1 = INDXR1 + 1
               INDXC1 = INDXC1 + 1
               IF(INDXR1 .LE. INDXR2 .AND. INDXC1 .LE. INDXC2) GO TO 160
            END IF
 180        L(INDX) = L(INDX)/DINV(JL(INDX)) !Final value stored.
 190     CONTINUE
!Changing upper elements         
 200     INDX1 = JU(I)
         INDX2 = JU(I+1) - 1
         IF(INDX1 .GT. INDX2) GO TO 260
         DO 250 INDX=INDX1,INDX2
            IF(INDX .EQ. INDX1) GO TO 240
            INDXC1 = INDX1
            INDXC2 = INDX - 1
            INDXR1 = IL(IU(INDX))
            INDXR2 = IL(IU(INDX)+1) - 1
            IF(INDXR1 .GT. INDXR2) GO TO 240
 210        KR = JL(INDXR1)
 220        KC = IU(INDXC1)
            IF(KR .GT. KC) THEN
               INDXC1 = INDXC1 + 1
               IF(INDXC1 .LE. INDXC2) GO TO 220
            ELSE IF(KR .LT. KC) THEN
               INDXR1 = INDXR1 + 1
               IF(INDXR1 .LE. INDXR2) GO TO 210
            ELSE IF(KR .EQ. KC) THEN
               U(INDX) = U(INDX) - L(INDXR1)*DINV(KC)*U(INDXC1)
               INDXR1 = INDXR1 + 1
               INDXC1 = INDXC1 + 1
               IF(INDXR1 .LE. INDXR2 .AND. INDXC1 .LE. INDXC2) GO TO 210
            END IF
 240        U(INDX) = U(INDX)/DINV(IU(INDX))
 250     CONTINUE
!Changing diagonal elements  
 260     INDXR1 = IL(I)
         INDXR2 = IL(I+1) - 1
         IF(INDXR1 .GT. INDXR2) GO TO 300
         INDXC1 = JU(I)
         INDXC2 = JU(I+1) - 1
         IF(INDXC1 .GT. INDXC2) GO TO 300
 270     KR = JL(INDXR1)
 280     KC = IU(INDXC1)
         IF(KR .GT. KC) THEN
            INDXC1 = INDXC1 + 1
            IF(INDXC1 .LE. INDXC2) GO TO 280
         ELSE IF(KR .LT. KC) THEN
            INDXR1 = INDXR1 + 1
            IF(INDXR1 .LE. INDXR2) GO TO 270
         ELSE IF(KR .EQ. KC) THEN
            DINV(I) = DINV(I) - L(INDXR1)*DINV(KC)*U(INDXC1)
            INDXR1 = INDXR1 + 1
            INDXC1 = INDXC1 + 1
            IF(INDXR1 .LE. INDXR2 .AND. INDXC1 .LE. INDXC2) GO TO 270
         END IF
!         
 300  CONTINUE
!         
      DINV(1:N) = 1.0d0/DINV(1:N)
	  
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DSILUS
!
!
      RETURN
      END SUBROUTINE DSILUS_ROW
!


 SUBROUTINE DSLUI_SCH(N, B, X, NELT, IA, JA, A, ISYM, RWORK, IWORK,I,NI_max,NELT_max)
!
! !DSLUI returns in R the solution of system LDU.R = V1 where LDU = M
!N, V1, R, NELT, IA, JA, A, ISYM, RWORK, IWORK
!Parallel DSLUI is compatible with SLAP column format of L in LDU
!This subroutine is directly parallelizable

      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      INTEGER :: N, NELT, IA(NELT), JA(NELT), ISYM, IWORK(*),ICALL=0
      REAL(KIND = 8) :: B(N), X(N), A(NELT), RWORK(*)
	  SAVE ICALL
	  ICALL = ICALL+1
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSLUI
!
!
      LOCIL  = IWORK(1)+(I-1)*(NI_max+1)
      LOCJL  = IWORK(2)+(I-1)*(NELT_max/2)
      LOCIU  = IWORK(3)+(I-1)*(NELT_max/2)
      LOCJU  = IWORK(4)+(I-1)*(NI_max+1)
      LOCL   = IWORK(5)+(I-1)*(NELT_max)
      LOCDIN = IWORK(6)+(I-1)*(NI_max)
      LOCU   = IWORK(7)+(I-1)*(NELT_max)
!         
      X(1:N) = B(1:N)  ! CAREFUL! Whole array operations
!         
      DO 30 IROW = 2, N
         JBGN = IWORK(LOCIL+IROW-1)
         JEND = IWORK(LOCIL+IROW)-1
         IF( JBGN.LE.JEND ) THEN
            DO 20 J = JBGN, JEND
               X(IROW) = X(IROW)-RWORK(LOCL+J-1)*X(IWORK(LOCJL+J-1))
 20         CONTINUE
         ENDIF
 30   CONTINUE
         
      X(1:N) = X(1:N)*RWORK(LOCDIN:LOCDIN+N-1)  ! CAREFUL! Whole array operations
!         
      DO 60 ICOL = N, 2, -1
         JBGN = IWORK(LOCJU+ICOL-1)
         JEND = IWORK(LOCJU+ICOL)-1
         IF( JBGN.LE.JEND ) THEN
!          X(IWORK(LOCIU-1+JBGN:LOCIU-1+JEND)) 
!     &  = X(IWORK(LOCIU-1+JBGN:LOCIU-1+JEND))
!     &   -RWORK(LOCU-1+JBGN:LOCU-1+JEND)*X(ICOL)
            DO 50 J = JBGN, JEND
               JJUU    = IWORK(LOCIU+J-1)
               X(JJUU) = X(JJUU)-RWORK(LOCU+J-1)*X(ICOL) !Note: U is already divided by the diagonal of the same row.
 50         CONTINUE
         ENDIF
 60   CONTINUE
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DSLUI
!
!
      RETURN
      END SUBROUTINE DSLUI_SCH
	  
	  
	  
	  	  SUBROUTINE DSMVR_SCH( N,M, X, Y, NELT, IA, JA, A, ISYM,IFFST)
	  
!Computing AX->Y. A is of size nxm and x is of size mx1
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      INTEGER  :: N, NELT, IA(NELT), JA(NELT), ISYM, ICALL=0
      REAL(KIND = 8) :: A(NELT), X(M), Y(N)
	  SAVE ICALL
	  
	  ICALL=ICALL+1
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSMV
!
!
      Y(1:N) = 0.0d0     ! CAREFUL! Whole array operations
!
	
      DO IROW = 1, N
         DO j = IA(IFFST+irow),IA(IFFST+irow+1)-1
			Y(IROW) = Y(IROW) + A(j)*X(JA(j))
		 END DO
      END DO
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DSMV
!
!
      RETURN
      END SUBROUTINE DSMVR_SCH
!


SUBROUTINE DCGS_SCH(N, B, X, NELT, IA, JA, A, ISYM, MATVEC,&
     &     MSOLVE, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, &
     &     R, R0, P, Q, U, V1, V2, RWORK, IWORK,N_SUB)
!
!
!
      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
	  INCLUDE 'mpif.h'
!
      INTEGER N, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITMAX
      INTEGER ITER, IERR, IUNIT, IWORK(*),N_SUB
      REAL(KIND = 8) B(N), X(N), A(NELT), TOL, ERR, R(N), R0(N), P(N)
      REAL(KIND = 8) Q(N), U(N), V1(N), V2(N), RWORK(*)
!
	  DOUBLE PRECISION:: time_t1,time_sol
      REAL(KIND = 8) DMACH(5)
      DATA DMACH(3) / 1.1101827117665D-16 /
	  INTEGER :: ICALL=0
	  SAVE ICALL
	  
	  ICALL = ICALL+1

!      EXTERNAL MATVEC, MSOLVE
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DCGS
!
!
      ITER = 0
      IERR = 0
!
      IF( N < 1 ) THEN
         IERR = 3
         RETURN
      ENDIF
!
      TOLMIN = 5.0d2*DMACH(3)
      IF( TOL < TOLMIN ) THEN
         TOL  = TOLMIN
         IERR = 4
      ENDIF
!         
		
      CALL MATVEC(N, X, R, NELT, IA, JA, A, ISYM)
!         
      V1(1:N)  = R(1:N) - B(1:N)
!         
	  time_t1 = MPI_WTIME()
      CALL MSOLVE(N, V1, R, NELT, IA, JA, A, ISYM, RWORK, IWORK,N_SUB)
	  time_sol = MPI_WTIME()  - time_t1
	 ! print *, "TIME FOR SOLVING ONE PRECONDITIONING STEP",time_sol
	!  IF(ICALL==2) print *,"R",R(1:N)
	!  IF(ICALL==2) stop
!        
      ISDCGS = 0
      ITOL   = 2
      IF(ITER == 0) THEN
         CALL MSOLVE(N, B, V2, NELT, IA, JA, A, ISYM, RWORK, IWORK,N_SUB)
         BNRM = SQRT(DOT_PRODUCT(V2(1:N),V2(1:N)))
      ENDIF
!         
      ERR = SQRT(DOT_PRODUCT(R(1:N),R(1:N)))/BNRM
!         

      IF(IUNIT /= 0) THEN
         IF( ITER == 0 ) THEN
            WRITE(IUNIT,1000) N_SUB,N, ITOL
         ENDIF
         WRITE(IUNIT,1010) ITER, ERR, AK, BK
      ENDIF
	  
      IF(ERR <= TOL) ISDCGS = 1
!         
      IF(ISDCGS /= 0) GO TO 200
!
!
      FUZZ = DMACH(3)**2
!
      R0(1:N) = R(1:N)
!
      RHONM1 = 1.0D0
!         
!         
      DO 100 K=1,ITMAX
         ITER = K
!
         RHON = DOT_PRODUCT(R0(1:N),R(1:N))
!
         IF( ABS(RHONM1) < FUZZ ) GO TO 998
!
         BK = RHON/RHONM1
!
         IF( ITER.EQ.1 ) THEN
            U(1:N) = R(1:N)
            P(1:N) = R(1:N)
         ELSE
            U(1:N)  = R(1:N) + BK*Q(1:N)
            V1(1:N) = Q(1:N) + BK*P(1:N)
            P(1:N)  = U(1:N) + BK*V1(1:N)
         ENDIF
!         

         CALL MATVEC(N, P, V2, NELT, IA, JA, A, ISYM)
         CALL MSOLVE(N, V2, V1, NELT, IA, JA, A, ISYM, RWORK, IWORK,N_SUB)
		 ! print *,"V1 check",V1(1:N)
	
!         
         SIGMA = DOT_PRODUCT(R0(1:N),V1(1:N))

         IF( ABS(SIGMA) < FUZZ ) GO TO 999
!         
         AK = RHON/SIGMA
         AKM = -AK
!         
         Q(1:N)  = U(1:N) + AKM*V1(1:N)
!         
         V1(1:N) = U(1:N) + Q(1:N)
!         
         X(1:N)  = X(1:N) + AKM*V1(1:N)
!                      
         CALL MATVEC(N, V1, V2, NELT, IA, JA, A, ISYM)
         CALL MSOLVE(N, V2, V1, NELT, IA, JA, A, ISYM, RWORK, IWORK,N_SUB)
         R(1:N) = R(1:N) + AKM*V1(1:N)
!         
         ISDCGS = 0
         ITOL   = 2
!         
         ERR = SQRT(DOT_PRODUCT(R(1:N),R(1:N)))/BNRM
!         
         IF(IUNIT /= 0) THEN
            WRITE(IUNIT,1010) ITER, ERR, AK, BK
         ENDIF
         IF(ERR <= TOL) ISDCGS = 1
!         
         IF(ISDCGS /= 0) GO TO 200
!
         RHONM1 = RHON
 100  CONTINUE
 1000 FORMAT(' BiConjugate Gradient Squared preconditioned with Schur complement method&
			with number of subdomains',I4,' for ',&
          'N, ITOL = ',I5, I5,&
          /' ITER','   Error Estimate','            Alpha',&
          '             Beta')
 1010 FORMAT(1X,I4,1X,E16.7,1X,E16.7,1X,E16.7)
!         
      ITER = ITMAX + 1
      IERR = 2
 200  RETURN
!
 998  IERR = 5
      RETURN
!
 999  IERR = 6
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DCGS
!
!
      RETURN
      END SUBROUTINE DCGS_SCH

	  
	   SUBROUTINE DSLUI_ANY(N, B, X, NELT, IA, JA, A, ISYM, RWORK, IWORK)
!
! !DSLUI returns in R the solution of system LDU.R = V1 where LDU = M
!N, V1, R, NELT, IA, JA, A, ISYM, RWORK, IWORK
!Parallel DSLUI is compatible with SLAP column format of L in LDU
!This subroutine is directly parallelizable

      IMPLICIT REAL(KIND = 8)(A-H,O-Z)
      INTEGER N, NELT, IA(NELT), JA(NELT), ISYM, IWORK(*)
      REAL(KIND = 8) B(N), X(N), A(NELT), RWORK(*)
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of DSLUI
!
!
      LOCIL  = IWORK(1)
      LOCJL  = IWORK(2)
      LOCIU  = IWORK(3)
      LOCJU  = IWORK(4)
      LOCL   = IWORK(5)
      LOCDIN = IWORK(6)
      LOCU   = IWORK(7)
!         
      X(1:N) = B(1:N)  ! CAREFUL! Whole array operations
!         
      DO 30 IROW = 2, N
         JBGN = IWORK(LOCIL+IROW-1)
         JEND = IWORK(LOCIL+IROW)-1
         IF( JBGN.LE.JEND ) THEN
            DO 20 J = JBGN, JEND
               X(IROW) = X(IROW)-RWORK(LOCL+J-1)*X(IWORK(LOCJL+J-1))
 20         CONTINUE
         ENDIF
 30   CONTINUE
         
      X(1:N) = X(1:N)*RWORK(LOCDIN:LOCDIN+N-1)  ! CAREFUL! Whole array operations
!         
      DO 60 ICOL = N, 2, -1
         JBGN = IWORK(LOCJU+ICOL-1)
         JEND = IWORK(LOCJU+ICOL)-1
         IF( JBGN.LE.JEND ) THEN
!          X(IWORK(LOCIU-1+JBGN:LOCIU-1+JEND)) 
!     &  = X(IWORK(LOCIU-1+JBGN:LOCIU-1+JEND))
!     &   -RWORK(LOCU-1+JBGN:LOCU-1+JEND)*X(ICOL)
            DO 50 J = JBGN, JEND
               JJUU    = IWORK(LOCIU+J-1)
               X(JJUU) = X(JJUU)-RWORK(LOCU+J-1)*X(ICOL) !Note: U is already divided by the diagonal of the same row.
 50         CONTINUE
         ENDIF
 60   CONTINUE
!
!
!=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of DSLUI
!
!
      RETURN
      END SUBROUTINE DSLUI_ANY


	  end module schur_seq
  